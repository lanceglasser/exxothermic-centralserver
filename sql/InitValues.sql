
insert into user_role values (1,0,now(),'Administrator',now(),'Administrator'),(2,0,now(),'Integrator Owner Company',now(),'Integrator Owner Company'),
(3,0,now(),'Owner Company',now(),'Owner Company'),(4,0,now(),'account',now(),'account');

insert into user_user(id,created_date,role_user_id) values (1,now(),4) ;

insert into company_catalog_type values (0, 0 ," ",true,"Bar");
insert into company_catalog_type values (1, 0 ," ",true,"Hotel");
insert into company_catalog_sub_type values (0,0,"Bar lounge",true,"Bar lounge",0);
insert into company_catalog_sub_type values (1,0," ",true,"Lounge",1);
insert into company_catalog_sub_type values (2,0," ",true,"Gym",1);
insert into company_catalog_sub_type values (3,0," ",true,"Other",1);
insert into country values (1,0,"United States of America","USA");
insert into country values (2,0,"Costa Rica","cr");
insert into state values (1,0,1,"Texas"),(2,0,1,"Illinois"),(3,0,1,"Virginia");
insert into city values (1,0,'Dallas',1),(5,0,'Austin',1),(2,0,'Chicago',2),(3,0,'Springfield',2),(6,0,'Norfolk',3),(7,0,'Richmond',3);
insert into box_status values (0,"Connected-Not Registered");
insert into box_status values (1,"Connected");
insert into  company_company (id,name,enable,created_by_id,created_date,last_update,owner_id,city_id) values (1,"exxothermic",false,1,now(),now(),1,1);
insert into company_location  (id,administrator_id,company_id,created_date,last_update,enable,type_id,city_id,created_by_id)
values (1,1,1,now(),now(),false,1,1,1);




/*Test accounts*/
insert into user_user(email, first_name,last_name, password,role_user_id) values('test@test.com','test','test',"123",3);
update user_user set created_date=now(), last_login=now(), last_update=now();

insert into  company_company (id,name,enable,created_by_id,created_date,last_update,owner_id,city_id) values (2,"cecropia",true,1,now(),now(),2,1);
insert into company_location  (name,administrator_id,company_id,created_date,last_update,enable,type_id,city_id,created_by_id)
values ("test",2,2,now(),now(),false,1,1,1), ("test2",2,2,now(),now(),false,1,1,1);


insert into box_box(created_date,update_date,location_id,status_id,name,serial) values (now(),now(),1,0,"","123"),(now(),now(),1,0,"","abc"),(now(),now(),1,0,"","zxc"),(now(),now(),1,1,"","123456");




