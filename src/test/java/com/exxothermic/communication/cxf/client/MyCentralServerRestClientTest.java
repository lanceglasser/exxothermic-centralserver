package com.exxothermic.communication.cxf.client;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.exxothermic.communication.mycentralserver.cxf.client.MyCentralServerRestClient;

//TODO Implements Test for MyCentralServerRestClientTest

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"file:src/main/webapp/WEB-INF/spring/root-context.xml", "file:src/main/webapp/WEB-INF/spring/application-context.xml"})
public class MyCentralServerRestClientTest {

	@Autowired
	private MyCentralServerRestClient myCentralServerRestClient;
	
	@Test
	public void testConnectDevice() {
		myCentralServerRestClient.connectDevice("008", "1236.4");
		assertNotNull("");
	}

	@Test
	public void testDisconectDevice() {
		//fail("Not yet implemented");
		assertNotNull("");
	}

	@Test
	public void testRetrieveCustomButtons() {
		//fail("Not yet implemented");
		assertNotNull("");
	}

	@Test
	public void testUpdateLastConnectionFromDevice() {
		//fail("Not yet implemented");
		assertNotNull("");
	}

}
