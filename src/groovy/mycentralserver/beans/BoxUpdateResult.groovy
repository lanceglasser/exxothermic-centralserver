package mycentralserver.beans

class BoxUpdateResult {
	
	protected String serial;
	protected String description;
	
	protected boolean status;
	
	public BoxUpdateResult(String serial, String description, boolean status){
		this.serial = serial;
		this.description = description;
		this.status = status;
	}
	
	/**
	 * Returns the serial of the Object
	 * @return	The serial of the Object
	 */
	public String getSerial(){
		return this.serial;
	}
	
	/**
	 * Set the serial of the object
	 * @param serial	Serial to be set
	 * @return			The Object
	 */
	public BoxUpdateResult setSerial(String serial){
		this.serial = serial;
		return this;
	}
	
	/**
	 * Returns the description of the Object
	 * @return	The description of the Object
	 */
	public String getDescription(){
		return this.description;
	}
	
	/**
	 * Set the description of the object
	 * @param description	Description to be set
	 * @return				The Object
	 */
	public BoxUpdateResult setDescription(String description){
		this.description = description;
		return this;
	}
	
	/**
	 * Returns the status of the Object
	 * @return	The statusof the Object
	 */
	public boolean getStatus(){
		return this.status;
	}
	
	/**
	 * Set the status of the object
	 * @param status	Status to be set
	 * @return			The Object
	 */
	public BoxUpdateResult setStatus(boolean status){
		this.status = status;
		return this;
	}
}
