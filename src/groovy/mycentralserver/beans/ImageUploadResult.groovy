package mycentralserver.beans

import java.awt.image.BufferedImage;

import mycentralserver.MessageErrorService;

import org.springframework.aop.aspectj.RuntimeTestWalker.ThisInstanceOfResidueTestVisitor;
import org.codehaus.groovy.grails.web.context.ServletContextHolder as SCH;
import org.codehaus.groovy.grails.web.servlet.GrailsApplicationAttributes as GA;

class ImageUploadResult {
	
	protected String fileName;
	protected String imageUrl;
	protected String path;
	protected BufferedImage tempImage;
		
	def message;
		
	/**
	 * Constructor with only the action name as parameter
	 * @param actionName	Action Name, just for reference
	 */
	public ImageUploadResult(String fileName, String url, String path, BufferedImage image){
		this.fileName = fileName;
		this.imageUrl = url;
		this.path = path;
		this.tempImage = image;
		def ctx = SCH.servletContext.getAttribute(GA.APPLICATION_CONTEXT);
		message = ctx.message;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	/**
	 * @return the imageUrl
	 */
	public String getImageUrl() {
		return imageUrl;
	}

	/**
	 * @param imageUrl the imageUrl to set
	 */
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @return the tempImage
	 */
	public BufferedImage getTempImage() {
		return tempImage;
	}

	/**
	 * @param tempImage the tempImage to set
	 */
	public void setTempImage(BufferedImage tempImage) {
		this.tempImage = tempImage;
	}
		
}
