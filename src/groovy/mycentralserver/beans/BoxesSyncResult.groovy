package mycentralserver.beans

import mycentralserver.MessageErrorService;

import org.springframework.aop.aspectj.RuntimeTestWalker.ThisInstanceOfResidueTestVisitor;
import org.codehaus.groovy.grails.web.context.ServletContextHolder as SCH;
import org.codehaus.groovy.grails.web.servlet.GrailsApplicationAttributes as GA;

class BoxesSyncResult {
	
	protected List<BoxUpdateResult> boxUpdatesList;
	protected String actionName;
	
	protected int errorCounter;
	protected int successCounter;
	
	def messageErrorDescriptionHash;
	def message;
		
	/**
	 * Constructor with only the action name as parameter
	 * @param actionName	Action Name, just for reference
	 */
	public BoxesSyncResult(String actionName){
		this.boxUpdatesList = new ArrayList<BoxUpdateResult>();
		this.actionName = actionName;
		this.errorCounter = 0;
		this.successCounter = 0;
		MessageErrorService messageErrorService = new MessageErrorService();
		messageErrorDescriptionHash = messageErrorService.getMessageErrorDescription();
		def ctx = SCH.servletContext.getAttribute(GA.APPLICATION_CONTEXT);
		message = ctx.message;
	}
	
	/**
	 * Returns the number of objects with error
	 * @return	Counter of objects with error
	 */
	public int getErrorCounter(){
		return this.errorCounter;
	}
	
	/**
	 * Increase the number of objects with error
	 * @return	The updated counter
	 */
	protected int increaseError(){
		this.errorCounter++;
		return this.errorCounter;
	}
	
	/**
	 * Returns the number of objects with success
	 * @return	Counter of objects with success
	 */
	public int getSuccessCounter(){
		return this.successCounter;
	}
	
	/**
	 * Returns the number of all objects
	 * @return	Counter of all objects
	 */
	public int getTotalCounter(){
		return this.successCounter + this.errorCounter;
	}
	
	/**
	 * Increase the number of objects with success
	 * @return	The updated counter
	 */
	protected int increaseSuccess(){
		this.successCounter++;
		return this.successCounter;
	}
	
	/**
	 * Read the response from Communication and add the results according to their status
	 * @param result	Response from Communication
	 */
	public void addBoxResult(result){
		if(result != null){
			for(br in result.data){
				BoxUpdateResult boxResult = new BoxUpdateResult(br.serialNumber, br.errorCode, br.successful);
				this.boxUpdatesList.add(boxResult);
				if(br.successful){
					//If the box update return success
					this.increaseSuccess();
				} else {
					//If the box update returns error
					this.increaseError();
				}
			}
		}
	}
	
	/**
	 * Returns the list of results by box
	 * @return	List with the results
	 */
	public List<BoxUpdateResult> getBoxUpdatesList(){
		return this.boxUpdatesList;
	}
}
