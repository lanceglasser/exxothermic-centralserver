
/** this method create the message container for the success messages and the error messages.
 *  The containers are destroyed when the user press the button close ( the "x")
 * 
 * 
 * @param sucessfulMessage
 * @param errorMessage
 */
function createMessagesContainersForSuccessAndFail(sucessfulMessage, errorMessage) {
	 createMessageSuccesfulContainer(sucessfulMessage);
	 createMessageFailContainer(errorMessage);
}

/** this methid create the message successful container
 * 
 * @param sucessfulMessage
 */
function createMessageSuccesfulContainer(sucessfulMessage) {
	createMessageContainer("containerSuccessMessage", "ready", "alert-sucessMessage", "alert alert-success" , sucessfulMessage );
}

/** this method create the message fail container
 * 
 * @param failMessage
 */
function createMessageFailContainer(failMessage) {
	createMessageContainer("containerFailMessage", "error", "alert-errorMessage", "alert alert-error", failMessage );
}

/** this method creates generic message container, for display error
 * 
 * @param divPrincipalContainerToFind div which contains the divContainer, this is the place where the function add the html elements.
 * @param divContainer this is the div's name which contains the element close button and the message
 * @param classString this is the css to apply.
 * @param message this is the message to display
 */
function createMessageContainer(divPrincipalContainerToFind, divContainer, divContainerAlertMessage , classString, message ) {
	var divContainerWithMessage = $("#"+divContainer); //"#ready" , "#error"
	  
	  if( !existTheElement(divContainerWithMessage)) {
		  var successMessage = $("#"+divPrincipalContainerToFind);
	  
		  var divAlert = $("<div></div>");	  
		  divAlert.addClass(classString);
		  divAlert.attr("id", divContainer);
		  
		  var buttonClose = $("<a></a>");	  
		  buttonClose.addClass("close");
		  buttonClose.attr("data-dismiss", "alert");
		  buttonClose.html("&#215;");
		  
		  var divMessage = $("<div></div>");
		  divMessage.attr("id", divContainerAlertMessage);		  
		  divMessage.html(message);
		  
		  var divChannel = $("<div></div>");	 
		  divChannel.addClass("channels");
		  
		  divAlert.append(buttonClose);
		  divAlert.append(divMessage)
		  divAlert.append(divChannel);
		
		  successMessage.append(divAlert);
	  } // END if(  readyDiv == null)
}

function existTheElement(elem) {
	return (elem.length > 0);
} 
