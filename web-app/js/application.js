  
  
if (typeof jQuery !== 'undefined') {
	
	(function($) {
		$.ajaxSetup({ cache: false });
		$('#spinner').ajaxStart(function() {
			$(this).fadeIn();
		}).ajaxStop(function() {
			$(this).fadeOut();
		});
		
		//Fill a select list with options using an array of values as the data source
		//@param {String, Object} selectElement Reference to the select list to be modified, either the selector string, or the jQuery object itself
		//@param {Object} values An array of option values to use to fill the select list. May be an array of strings, or an array of hashes (associative arrays).
		//@param {String} [valueKey] If values is an array of hashes, this is the hashkey to the value parameter for the option element
		//@param {String} [textKey] If values is an array of hashes, this is the hashkey to the text parameter for the option element
		//@param {String} [defaultValue] The default value to select in the select list
		//@remark This function will remove any existing items in the select list
		//@remark If the values attribute is an array, then the options will use the array values for both the text and value.
		//@return the select element 

		$.fn.setSelectOptions= function( values, valueKey, textKey, defaultValue) {
		 
		 selectElement=$(this[0]);
		 selectElement.empty();

		 if (typeof (values) == 'object') {

		     if (values.length) {

		         var type = typeof (values[0]);
		         var html = "";

		         if (type == 'object') {
		             // values is array of hashes
		             var optionElement = null;

		             $.each(values, function () {
		                 html += '<option value="' + this[valueKey] + '">' + this[textKey] + '</option>';                    
		             });

		         } else {
		             // array of strings
		             $.each(values, function () {
		                 var value = this.toString();
		                 html += '<option value="' + value + '">' + value + '</option>';                    
		             });
		         }

		         selectElement.append(html);
		     }

		     // select the defaultValue is one was passed in
		     if (typeof defaultValue != 'undefined') {
		         selectElement.children('option[value="' + defaultValue + '"]').attr('selected', 'selected');
		     }

		 }

		 return selectElement;

		}
		
		console.debug("It's going to set the only numbers event...");
		
		$(".onlyNumbers").keypress(function(event) {
		      console.debug("only number keypress...");
			  // Backspace, tab, enter, end, home, left, right
			  // We don't support the del key in Opera because del == . == 46.
			  var controlKeys = [8, 9, 13, 35, 36, 37, 39];
			  // IE doesn't support indexOf
			  var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
			  // Some browsers just don't raise events for control keys. Easy.
			  // e.g. Safari backspace.
			  if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
			      (48 <= event.which && event.which <= 57) || // Always 1 through 9
			      //(48 == event.which && $(this).attr("value")) || // No 0 first digit
			      isControlKey) { // Opera assigns values for control keys.
			    return;
			  } else {
			    event.preventDefault();
			  }
			});
		
		var isValidPassword = function(obj){
			var str = obj.value;
			if(str.indexOf('#') > -1 || str.indexOf('$') > -1 || str.indexOf('%') > -1 || str.indexOf('_') > -1 || str.indexOf('@') > -1 ||
			   str.indexOf('!') > -1 || str.indexOf('?') > -1 || str.indexOf('-') > -1) {
				if(str.length < 6) {			        
			        return false;
			    }
			    re = /[0-9]/;
			    if(!re.test(str)) {
			       return false;
			    }
			    re = /[A-Za-z]/;
			    if(!re.test(str)) {
			      return false;
			    }
				return true;
			} else {
				return false;
			}
		};
				
		$("#password").change(function(event){
			event.target.setCustomValidity((isValidPassword(this)===false) ? validationDetail : '');
			$("#confirmPassword").trigger("change");
		});
		
		$("#newPassword").change(function(event){
			event.target.setCustomValidity((isValidPassword(this)===false) ? validationDetail : '');
			 
			$("#confirmPassword").trigger("change");
			
		});
		
		$("#confirmPassword").change(function(event){

			var pass = "";
			
			if (document.getElementById("password")){
				pass=document.getElementById("password").value;
			}
		
			if (document.getElementById("newPassword")){
				pass=document.getElementById("newPassword").value;
			}
			
			if(event.target.value!=pass){			
				event.target.setCustomValidity(confirmationFails);	
			}else{
				event.target.setCustomValidity('');
			}
		});
	})(jQuery);
}
