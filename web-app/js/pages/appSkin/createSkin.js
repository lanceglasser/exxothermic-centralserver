(function($){
  $(document).ready(function()  {
 
	  $("#menuTextColor").change( function() {
		
		  var textColor = this.value;
		  
		  var myhex = textColor.replace(/[^0-9a-fA-F]/g, '');
		  this.value = "#" + myhex;
		  
	  });
	  
	  $("#headerTextColor").change( function() {
			
		  var textColor = this.value;
		  
		  var myhex = textColor.replace(/[^0-9a-fA-F]/g, '');
		  this.value = "#" + myhex;
		  
	  });
	  
	  $("#backgroundColor").change( function() {
			
		  var textColor = this.value;
		  
		  var myhex = textColor.replace(/[^0-9a-fA-F]/g, '');
		  this.value = "#" + myhex;
		  
	  });


  });
})(jQuery);