(function($){
  $(document).ready(function()  {
	  
	  var stopEvent = true;
	  var target;
	  	  
  
	  var initializeEvents = function(){
		  
		  $(".changePassword").click(function(e){
			  target = e.target;
			  
			  if (stopEvent){
				  e.preventDefault();			  
				  $( "#confirm-reset-dialog" ).dialog("open");			  
			  }
		  });
		  
	  };
	  
	  $("#confirm-reset-dialog" ).dialog({
	      height: 140,
	      modal: true,
	      autoOpen: false,
	      buttons: [ {
	    	  		text: okButton, 
	    	  		click: function() {
	    	  			$( this ).dialog( "close" );	
	    	  			 stopEvent = false;	    	  			 
	    	  			 target.click();
	    	  		}
	      		   },
	    	  	{ text: cancelButton,	
		    	  click: function() {
		    		  $( this ).dialog( "close" );		    		 
		    	  }
	    	  }
	      ]
	    });
	  
	  initializeEvents();
  });
})(jQuery);