(function($){
  $(document).ready(function()  {
	  
	  var stopEvent = true;
	  var target;
	  	  
	  var initializeEvents = function(){
		  $(".delete").click(function(e){
			  
			  target = e.target || e.srcElement;
			  		  
			  if (stopEvent){
				  e.preventDefault();	  
				  $( "#confirm-retire-dialog" ).dialog({ height: 175 }).dialog("open");
			  } else {
				  stopEvent = true;
				  window.location.href=target;
			  }
		  });

		  $(".reset").click(function(e){
			  target = e.target || e.srcElement;
			  
			  if (stopEvent){
				  e.preventDefault();			  
				  $( "#confirm-reset-dialog" ).dialog({ height: 175 }).dialog("open");			  
			  } else {
				  stopEvent = true;
				  window.location.href=target;
			  }
		  });		  
	  };
	  
	  $("#confirm-reset-dialog" ).dialog({
	      height: 140,
	      width: 400,
	      modal: true,
	      autoOpen: false,
	      buttons: [ {
	    	  		text: okButton, 
	    	  		click: function() {
	    	  			$( this ).dialog( "close" );	
	    	  			 stopEvent = false;	    	  			 
	    	  			$(target).trigger("click");
	    	  		}
	      		   },
	    	  	{ text: cancelButton,	
		    	  click: function() {
		    		  $( this ).dialog( "close" );		    		 
		    	  }
	    	  }
	      ]
	    });
	  
	  $("#confirm-retire-dialog" ).dialog({
	      height: 140,
	      width: 400,
	      modal: true,
	      autoOpen: false,
	      buttons: [ {
	    	  		text: okButton, 
	    	  		click: function() {
	    	  			$( this ).dialog( "close" );
	    	  			 stopEvent = false;
	    	  			 $(target).trigger("click");
	    	  		}
	      		   },
	    	  	{ text: cancelButton,	
		    	  click: function() {
		    		  $( this ).dialog( "close" );		    		 
		    	  }
	    	  }
	      ]
	    });
	  
	  initializeEvents();
  });
})(jQuery);