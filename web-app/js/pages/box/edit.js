 
(function($){
		
  $(document).ready(function(){
	  $("#process, #ready, #error").hide();
	  
	  
		
	  $("#dialogPlaceholder").dialog({
	        autoOpen: false,	        	      
	        width: 'auto',
	        height: 'auto',
	        modal: true,
	        resizable: false,	 
	        animate: true,
	        title: 'upload image',
	        autoReposition: true,    	        
	        close: function(){
	            $("#dialogPlaceholder").html('');
	        }
	    });
	  
	  $("button[name='trigger_btn']").click(function(e) {
		  	e.preventDefault();
		    var id= $(this).attr('id'); 		
	        $.ajax({
	            url: baseURL + '/box/uploadImagePage?id='+ id,
	            success: function(data){
	                $("#dialogPlaceholder").html(data);
	                $("#dialogPlaceholder").dialog("open");
	            }
	        });
	    });
	  
	  $("button[name='upload_large_image_btn']").click(function(e) {
		  	e.preventDefault();
		    var id= $(this).attr('id'); 		
	        $.ajax({
	            url: baseURL + '/box/uploadLargeImagePage?id='+ id,
	            success: function(data){
	                $("#dialogPlaceholder").html(data);
	                $("#dialogPlaceholder").dialog("open");
	            }
	        });
	    });
	  
	  $("input[name='paBox']").change(function(){
		  $.ajaxSetup({
			  async: true
		  });
		  var serial=$("input[name='serialNumber']").val();
		  var pa=$("input[name='paBox']").is(':checked');
		  var url="../isvalidpa?serial="+serial+"&pa="+pa;		  
		  $.getJSON(url, function(data)
				  {
			  		switch(data.status)
			  		{
			  			case 0:	
			  				break;
			  			case 1:	
			  				bootbox.alert(data.results);
			  				$('input[type="submit"], a.btn.small.primary input[name="paBox"]').attr('disabled', true);
			  				$("a.btn.small.primary").unbind('click');
			  				$("#channelscontent").hide();
			  				break;
			  			case 2:	
			  				$('input[name=paBox]').prop('checked', true);
			  				bootbox.alert(data.results);
			  				
			  				break;
			  			case 3:	
			  				$('input[name=paBox]').prop('checked', true);
			  				bootbox.alert(data.results);
			  				break;
			  			case 4:	
			  				bootbox.confirm(data.results, function(result) {
			  				   if(!result)
			  				   {
			  					 $('input[name=paBox]').prop('checked', false);
			  				   }
			  				}); 
			  				break;
			  		}
				  });
		  
	  });
	  
	  
	 // $("a.btn.small.primary").click(function(e){
	  $("#btnSyncronize").click(function(e){ 
	  
		  e.preventDefault();
		  
		  var sucessfulMessage = $("#sucessfulMessage");
		  var failMessage = $("#failMessage");
		  var serial = $("input[name='serialNumber']").val();
		  
		  createMessagesContainersForSuccessAndFail(sucessfulMessage.val(), failMessage.val());
		  
		  $("#process").show();
		  var error=new Array();
		  var ready=new Array();
		  $("#ready, #error").hide();
		  
		  $.ajaxSetup({
			  			async: false
		  			});
		  		  	 
				  
		  $("input[name='channel']").each(function(index,val)
		  {
			 
			  var id 	 = $(val).val();
			  var rowId  =  $("tr#"+id+ " input[name='rowId']").val();
			  var name 	 = $("tr#"+id+ " input[name='label']").val();
			  var oldName = $("tr#"+id+ " input[name='oldlabel']").val();
			  var pa 	 = $("tr#"+id+ " input[name='paChannel']").is(':checked');
			  var oldPA =  castStrToBool($("tr#"+id+ " input[name='oldpa']").val());	
			  var imageURL 	 = $("tr#"+id+ " input[name='imageURL']").val();
			  var oldImageURL =  $("tr#"+id+ " input[name='oldImageURL']").val();	
			  var description 	 = $("tr#"+id+ " textArea[name='description']").val();
			  var oldDescription =  $("tr#"+id+ " input[name='oldDescription']").val();	
			  var largeImageURL 	 = $("tr#"+id+ " input[name='largeImageURL']").val();
			  var oldLargeImageURL =  $("tr#"+id+ " input[name='oldLargeImageURL']").val();	
			  
			    
			  var url 	= $("a[name='urlLinkToSyncronize']").attr("href")+ "?serial=" + serial + 
			  																"&channelNumber=" + id + 
			  																"&channelLabel=" + name + 
			  																"&isPA=" + pa +
			  																"&imageURL=" + imageURL +
			  																"&description=" + description +
			  																"&largeImageURL=" + largeImageURL;
			  
			  if(name.length == 0){
				  error.push(rowId + " - " +  emptyChannel);
			  }else{
			  
				  if(!(name == oldName) || !(pa == oldPA) || !(imageURL  == oldImageURL) || !(description  == oldDescription)
						   || !(largeImageURL  == oldLargeImageURL))
				  {
					  $.getJSON(url, function(data)
					  {	})
					  	.done(function( data ) {
					  		
					  		if(data.successful)
							  {
								  ready.push(rowId);
								  $("tr#"+id+ " input[name='oldlabel']").val(name);
								  $("tr#"+id+ " input[name='oldpa']").val(pa);
								  
								  if(data.message != null) {
									  var html = $("#alert-sucessMessage").html();
									  $("#alert-sucessMessage").html(data.message + html);
								  }
							  }
							  else
							  {																
								   if(data.message != null) {
										  error.push(rowId + "-" + data.message);
								   }else{
										error.push(rowId);
								   }
							  }
					  		
						})
						.fail(function( jqxhr, textStatus, error ) {
							//var err = textStatus + ', ' + error;
							//alert( "Request Failed: " + err);
						});
				  }
			  }
		  });					
			   
		  if(ready.length>0)
		  {
			  $("#ready").show();
			  var response="<ul>";
			  for (var i=0;i<ready.length;i++)
			  {
				 response+="<li>"+ready[i]+"</li>\n"; 
			  }
			  $("#ready .channels").html(response);
		  }	  
		 
		  
		  if(error.length>0)
		  {
			  $("#error").show();
			  var response="<ul>";
			  for (var i=0;i<error.length;i++)
			  {
				 response+="<li>"+error[i]+"</li>\n"; 
			  }
			  $("#error .channels").html(response);
		  }
		  $("#process").hide("slow");
	  });
  });  
  
  	function castStrToBool(str){
  		var valueInLoweCase = str.toLowerCase();
  		
	    if ((valueInLoweCase =='false') || (valueInLoweCase == "false")){
	       return false;
	    } else if ( (valueInLoweCase=='true') || (valueInLoweCase == "true")){
	       return true;
	    } else {
	       return undefined;
	    }
	}
})(jQuery);