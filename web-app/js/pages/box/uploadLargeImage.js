var jcrop_api,  boundx,  boundy, $preview, $pcnt , $pimg, xsize , ysize;
var BOX_MAX_WIDTH=450;
var BOX_MAX_HEIGHT=350;

function updatePreview( c)
{
	 if (parseInt(c.w) > 0)
	  {
		var rx = 320 / c.w;
	    var ry = 125 / c.h;
	    
	    $pimg.css({
	      width: Math.round(rx * boundx) + 'px',
	      height: Math.round(ry * boundy) + 'px',
	      marginLeft: '-' + Math.round(rx * c.x) + 'px',
	      marginTop: '-' + Math.round(ry * c.y) + 'px'
	    });
	    
	    $('#x').val(c.x);
	    $('#y').val(c.y);   
	    $('#w').val(c.w);
	    $('#h').val(c.h);
	  }  
};
    
function fileSelectHandler() {
	// get selected file
    var oFile = $('#file')[0].files[0];		
    var errorDiv = document.getElementById('errorDiv');
    // preview element
    var oImage = document.getElementById('target');
    var oImagePreview = document.getElementById('preview');
   
    //hide error section
    errorDiv.style.display = 'none'; 
 
    // check for image type (jpg and png are allowed)
    var rFilter = /^(image\/jpeg|image\/png)$/i;
    if (! rFilter.test(oFile.type)) {
    	errorDiv.innerHTML = '<ul><li>' + invalidImageError + '</li></ul>';
       	errorDiv.style.display = 'block';
    	return;
    }
 
    // check for file size
    if (oFile.size > 300 * 1024) {
        errorDiv.innerHTML = '<ul><li>' + tooBigImageError + '</li></ul>';
    	errorDiv.style.display = 'block';
    	return;
    }
     var oReader = new FileReader();
	 oReader.onload = function(e) {		  
	        // e.target.result contains the DataURL which we can use as a source of the image
    	
    	var newImage = new Image();
    	newImage.onload = function() {
    		  
    		  if(this.width > 800 || this.width < 640 || this.height < 240){
    			  errorDiv.innerHTML = '<ul><li>' + dimensionsImageError + '</li></ul>';
    		      errorDiv.style.display = 'block';
    		      return;
    		  }else{    			    			
    			
				oImagePreview.src = oImage.src=this.src;
    			
				var res_box_width,res_box_height;
    			
    			if(this.width > BOX_MAX_WIDTH){
    			 
    				res_box_width = BOX_MAX_WIDTH;
    				res_box_height = (BOX_MAX_WIDTH / this.width) * this.height;
    				
    				if (res_box_height > BOX_MAX_HEIGHT){
    				 
    					res_box_height = BOX_MAX_HEIGHT;
        				res_box_width = (BOX_MAX_HEIGHT / this.height) * this.width;            				
    				}
    			}else {
    				if(this.height > BOX_MAX_HEIGHT){
    				 
    					res_box_height = BOX_MAX_HEIGHT;
        				res_box_width = (BOX_MAX_HEIGHT / this.height) * this.width;        
    				}
    			}
    			
    			    			
    			oImagePreview.onload = function () {    			  
		  	        $('#preview').css("width",  (newImage.width *  320 ) / 640  +'px');
		  	        $('#preview').css("height", (newImage.height * 125 ) / 250  +'px');
		  	        
    			};
        			        			
	  	        oImage.onload = function () { // onload event handler	  	   	        
	  	        	$('#demo').hide().fadeIn('fast');
	  	        	$('#preview-pane').hide().fadeIn('fast');
	  	        	
	  	        	//update dialog width
		  	  		$('#dialogPlaceholder').css('height', res_box_height + 240 + 'px');
		  	      	$('#dialogPlaceholder').css('width',  res_box_width + 550 + 'px');	 
	  	          	 		  	        		
		  	        // destroy Jcrop if it is existed
		  	        if (typeof jcrop_api != 'undefined') {
		  	                jcrop_api.destroy();
		  	                jcrop_api = null;	  	             
		            }
		  	          
		  	        $('#target').css("width", oImagePreview.width +'px');
		  	        $('#target').css("height",oImagePreview.height +'px');
		  	         
		  	         // initialize Jcrop
	                $('#target').Jcrop({                                    	         	                    
	                    boxWidth: 450,
	                    boxHeight: 350,
	                    bgColor: 'white',	                    
	                    minSize: [640, 250],
	                    maxSize: [640, 250],
	                    onChange: updatePreview,
	                    onSelect: updatePreview,
	                    setSelect: [0, 0, 640, 250],
	                }, function(){		  
	                    // use the Jcrop API to get the real image size
	                    var bounds = this.getBounds();
	                    boundx = bounds[0];
	                    boundy = bounds[1];	 	                   
	                    // Store the Jcrop API in the jcrop_api variable
	                    jcrop_api = this;
	                          
	                    // Move the preview into the jcrop container for css positioning
	                    $preview.appendTo(jcrop_api.ui.holder);	  	  	          	
	                });		  	           	               
	  	        };
    		 }
    	};        	
    	newImage.src=e.target.result;
    };  	 
	    // read selected file as DataURL
	    oReader.readAsDataURL(oFile);
}


function showSpinner() {	
    $('div#ajaxFlowWait').show();
    if(document.getElementById('errorDiv')){
    	document.getElementById('errorDiv').style.display = 'none';
    }
}

function hideSpinner() {
    $('div#ajaxFlowWait').hide();
    if(document.getElementById('errorDiv')){
    	document.getElementById('errorDiv').style.display = 'none';
    }
}

(function($){
	  $(document).ready(function()  {
		// Grab some information about the preview pane
	    $preview = $('#preview-pane'),
	    $pcnt = $('#preview-pane .preview-container'),
	    $pimg = $('#preview-pane .preview-container img');
	    xsize = $pcnt.width();
	    ysize = $pcnt.height();
	    
	    $('#myUpload').submit(function(e){	    	
			e.preventDefault();
			
			var formData = new FormData($(this)[0]);
			
			 $.ajax({type:'POST',
	            data: formData ,
	            url: baseURL + '/box/uploadFile?type=large', 
	            async: false,
	            cache: false,
	            contentType: false,
	            processData: false,
	            success: function(data) {
	            	var channelNumber = data.channelNumber;	  	          	
	                $('#largeImageDemo_' + channelNumber).show();	
	                
	                $("tr#"+channelNumber+ " input[name='largeImageURL']").val(data.largeImageURL);
	                
	                var oImage = document.getElementById('largeImageTarget_'+channelNumber);
	                oImage.src = data.largeImageURL;
	                
	                hideSpinner();
	                $("#dialogPlaceholder").dialog("close");
	            },
	            error: function(xhr, textStatus, error){
	               //whatever you need to do here - output errors etc.
	              var errorDiv = document.getElementById('errorDiv');
	              hideSpinner();
	              errorDiv.innerHTML = '<ul><li>' + textStatus + '</li></ul>';
	          	  errorDiv.style.display = 'block';
	                           
	            }
			 });
	    });
	    	
	  });
})(jQuery);