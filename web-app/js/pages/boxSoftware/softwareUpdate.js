var pageUpdated = false;

(function($){
 
  
  $(document).ready(function() {
	    
	var ajax_call = function() {
		
	   if(!pageUpdated) {
			var serial=$("input[name='serialNumber']").val();
		  //your jQuery ajax code
			$.ajax({type:'POST',
	            data: "" ,
	            url: baseURL + '/boxSoftware/asReconnected?serialNumber='+serial, 
	            async: false,
	            cache: false,
	            contentType: false,
	            processData: false,
	            success: function(data) {    	            	
	            	if( data.message == doneMessage){	            	
		            	 var messageDiv = document.getElementById('messageUpdate');
		            	 messageDiv.innerHTML = boxUpdatedMessage;
		            	 
		            	 var statusTD 	 = $(".status_"+serial);
		            	 statusTD.html(doneMessage);
		            	 
		            	 pageUpdated = true;
	            	}
	            }	            
			 });
	   }
		
	};

	var interval = 1000 * 60 * 1; // where X is your every X minutes

	setInterval(ajax_call, interval);
		
  }); // $(document).ready  
  
})(jQuery);
