(function($){
	
  var code; 	
  $(document).ready(function(){
	  	  
	  code = $( "#type").find(":selected").attr("id");
	  $("#type,#country,#state ").select2();
	  
	  
	  if( code =='other' || code=='waitingRoomSpecify')	  
	  {
		  $("#controlCompanyOtherType").show();
		  $("#otherType").attr("required","required");
		  
	  }
	  else
	  {
		  $("#controlCompanyOtherType").hide();
		  $("#otherType").removeAttr("required");
	  }
	  	  
	  $("#type").change(function(){
		  code = $( "#type").find(":selected").attr("id");
		  if( code=='other' || code=='waitingRoomSpecify')
		  {
			  $("#controlCompanyOtherType").show("slow");
			  $("#otherType").attr("required","required");
			  
		  }
		  else
		 {
			  $("#controlCompanyOtherType").hide();
			  $("#otherType").removeAttr("required");
		 }
	  });
	  
	  $("#webSite").change(function(){
		  if(this.value.substr(0,7) != 'http://'){
			  this.value = 'http://' + this.value;
			}
			if(this.value.substr(this.value.length-1, 1) != '/'){
				this.value = this.value + '/';
			}
	  });
	  
	  $("#maxOccupancy, #numberOfTv, #numberOfTvWithExxothermicDevice").change(function () { 
		    this.value = this.value.replace(/[^\d]/g,'');
		});
	    
	   
	  $("#country").change(function(){		 
		  $.getJSON(loadStatesByCountryUrl + "?id="+$(this).val(),function(data){
			  try
			  {
				  if(data.states.length > 0 ){
					  $("#stateNameDiv").hide();
					  $("#stateDiv").show();
					  //remove the plugin select2
					  $("#state").select2("destroy");
					  //add the data
					  $("#state").setSelectOptions(data.states,"id","name");
					  //restart the pluggin select2
					  $("#state").select2();
					  //Trigger to change the City select box
					  $("#state").change();
				  }else{
					  $("#stateNameDiv").show();
					  $("#stateDiv").hide();
				  }
			  }
			  catch(Exception){}
			 
		  });
	  });
	  	  	
  });
})(jQuery);