(function($){
  $(document).ready(function()  {
	  
	  var stopEvent = true;
	  var target;
	  
	  
	  $('table').tablesorter({

		    // customize header HTML
		    onRenderHeader: function(index) {
		        // the span wrapper is added by default
		        this.wrapInner('<span class="icons"></span>');
		    }

		});
	  
	  var initializeEvents = function(){
		  
		  $(".changePassword").click(function(e){
			  target = e.target || e.srcElement;
			  
			  if (stopEvent){
				  e.preventDefault();			  
				  $( "#confirm-reset-dialog" ).dialog("open");			  
			  } else {
				  stopEvent = true;
				  window.location.href=target;
			  }
		  });
		  
	  };
	  
	  $("#confirm-reset-dialog" ).dialog({
	      height: 140,
	      modal: true,
	      autoOpen: false,
	      buttons: [ {
	    	  		text: okButton, 
	    	  		click: function() {
	    	  			$( this ).dialog( "close" );	
	    	  			 stopEvent = false;	    	  			 
	    	  			$(target).trigger("click");
	    	  		}
	      		   },
	    	  	{ text: cancelButton,	
		    	  click: function() {
		    		  $( this ).dialog( "close" );		    		 
		    	  }
	    	  }
	      ]
	    });
	  
	  initializeEvents();
  });
})(jQuery);