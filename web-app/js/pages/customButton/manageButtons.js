(function($){
  $(document).ready(function()  {
	  
	  var initializeEvents = function(){
		  		  
		  $.propHooks.checked = {
				  set: function(elem, value, name) {
				    var ret = (elem[ name ] = value);
				    $(elem).trigger("change");
				    return ret;
				  }
				};
		  
		  var workingWithCompanyCB = false;
		  var workingWithLocationCB = false;
		  
		  $(".cbCompany").change(function(e){
			  if(!workingWithLocationCB){
				// $(this).closest('fieldset').find(':checkbox').prop('checked', this.checked);
				  workingWithCompanyCB = true;
				  e.preventDefault();
				  if ($(this).is(':checked')) {
					  //$("#locationContainer_" + $(this).attr('companyId')).slideUp(500);
					  $(".cb_" + $(this).attr('companyId')).prop('checked', true);
				  } else {
					  //$("#locationContainer_" + $(this).attr('companyId')).slideDown(500);
					  $(".cb_" + $(this).attr('companyId')).prop('checked', false);
				  } 
				  workingWithCompanyCB = false;
			  }
		  });
		  
		  $(".cbLocation").change(function(e){
			  if(!workingWithCompanyCB){
				  workingWithLocationCB = true;
				  e.preventDefault();
				  if ($(this).is(':checked')) {
					  var len = $(".cb_" + $(this).attr('companyId')).length;
					  var chk = $('.cb_' + $(this).attr('companyId') + ':checked').length;
					  if (len == chk){//All of them are checked						  
						  $("#allCompany_" + $(this).attr('companyId')).prop('checked', true);
						  //$("#locationContainer_" + $(this).attr('companyId')).slideUp(500);
					  }
				  } else {					  
					  $("#allCompany_" + $(this).attr('companyId')).prop('checked', false);
				  }
				  
				  workingWithLocationCB = false;
			  }
		  });
		  
		  var selectedLocationsIds = selectedLocations.split(",");
		  
		  $("#resetButton").click(function(e){
			  e.preventDefault();
			  //workingWithLocationCB = true;
			  $(".cbLocation").prop('checked', false);
			  for(i=0; i < selectedLocationsIds.length; i++){
				  $("#location_" + selectedLocationsIds[i]).prop('checked', true);
			  }
			  //workingWithLocationCB = false;
		  });
		  
		  for(i=0; i < selectedLocationsIds.length; i++){
			  $("#location_" + selectedLocationsIds[i]).prop('checked', true);
		  }
		  
		  $(".minus").click(function(e){
			  e.preventDefault();
			  $("#locationContainer_" + $(this).attr('companyId')).slideUp();
			  $(this).hide();
			  $("#plus_"+ $(this).attr('companyId')).show();
		  });
		  
		  $(".plus").click(function(e){
			  e.preventDefault();
			  $("#locationContainer_" + $(this).attr('companyId')).slideDown();
			  $(this).hide();
			  $("#minus_"+ $(this).attr('companyId')).show();
		  });
	  };
	  
	  initializeEvents();
  });
})(jQuery);