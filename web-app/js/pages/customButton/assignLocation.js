(function($){
  
  $(document).ready(function()  {
	  
	  $("#buttons").change(function(e){
		  var id = $("#buttons").find(":selected").val();
		  $("#customButtonId").val(id);
	  });
	  
	  $("#buttons").select2();	
	  
	  if(defaultButtonId !== 0 ){
		  $('#buttons').val(defaultButtonId);
	  }
	  
	  $("#buttons").trigger('change');
	  
	  $("#createForm").submit(function(e){		 
		  var locationsIds = "";
		  $('.cbLocation:checked').each(function () {
			  locationsIds += (locationsIds === "")? $(this).attr("locationId"):"," + $(this).attr("locationId");
			});
		  $("#locationsToSave").val(locationsIds);
	  });
	  
	  
	  
	 /* $("#btnSyncronize").click(function(e){
		  var id = $("#buttons").find(":selected").val();
		  var url 	=  "syncronizeCustomButtons" + "?id=" + id;  //$("a[name='urlLinkToSyncronize']").attr("href")+ "?id=" + id; 
		  var i = 0;
		  var error = new Array();
		  var ready = new Array();
		  
		  $.getJSON(url, function(data)
		  {	  
			  if(data.successful)
			  {
				  ready.push(id)
				  //$("tr#"+id+ " input[name='oldlabel']").val(name);
				  //$("tr#"+id+ " input[name='oldpa']").prop('checked', pa);
			  }
			  else
			  {
				  error.push(id)
			  }			  			
		  });
	  });*/
	  
	  /*function getCustomButtonId() {
		  var id = $("#buttons").find(":selected").val();
		  return id;
	  }*/	  
	
  });
   
})(jQuery);