/***
 @file      exxo_app_skin_list.js
 @brief     General functions for the App Skin list pages
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Content = {
	init : function() {
	   
	   Exxo.UI.vars['stopEvent'] = true;
	   Exxo.UI.vars['target'] = true;
	   //alert(Exxo.UI.vars['stopEvent']);
	   	      
		//$('#table-list').dataTable();
		
		$(".delete").click(function(e){
		   Exxo.UI.vars['target'] = e.target;
           
           if (Exxo.UI.vars['stopEvent']){
               e.preventDefault();             
               $( "#confirm-delete-dialog" ).dialog("open");            
           }
       });
		
		$("#confirm-delete-dialog" ).dialog({
	          height: 180,
	          width: 360,
	          modal: true,
	          autoOpen: false,
	          buttons: [ {
	                    text: Exxo.UI.translates['okButton'], 
	                    click: function() {
	                        $( this ).dialog( "close" );    
	                        Exxo.UI.vars['stopEvent'] = false;                      
	                        Exxo.UI.vars['target'].click();
	                    }
	                   },
	                { text: Exxo.UI.translates['cancelButton'],   
	                  click: function() {
	                      $( this ).dialog( "close" );                   
	                  }
	              }
	          ]
	        });
	}
};