(function($){
  $(document).ready(function()  {
	  
	  var initializeEvents = function(){
		  
		  $(".viewLocation-disable").click(function(e){
			  e.preventDefault();			  
			  //alert(disableMessage);
			  $( "#content-disable-dialog" ).dialog("open");
		  });
		  
	  };
	  
	  $( "#content-disable-dialog" ).dialog({
	      height: 140,
	      modal: true,
	      autoOpen: false
	    });
	  
	  initializeEvents();
  });
})(jQuery);

