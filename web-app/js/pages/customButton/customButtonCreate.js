var jcrop_api,  boundx,  boundy, $preview, $pcnt , $pimg, xsize , ysize;


function updatePreview(c)
{
   if (parseInt(c.w) > 0)
  {
	var rx = 150 / c.w;
    var ry = 120 / c.h;

    $pimg.css({
      width: Math.round(rx * boundx) + 'px',
      height: Math.round(ry * boundy) + 'px',
      marginLeft: '-' + Math.round(rx * c.x) + 'px',
      marginTop: '-' + Math.round(ry * c.y) + 'px'
    });
    
    $('#x').val(c.x);
    $('#y').val(c.y);   
    $('#w').val(c.w);
    $('#h').val(c.h);
  }  
};


function fileSelectHandler() {
		// get selected file
	    var oFile = $('#file')[0].files[0];		
	    var errorDiv = document.getElementById('errorDiv');
	    // preview element
	    var oImage = document.getElementById('target');
	    var oImagePreview = document.getElementById('preview');
	   
	    //hide error section
	    errorDiv.style.display = 'none'; 
	 
	    // check for image type (jpg and png are allowed)
	    var rFilter = /^(image\/jpeg|image\/png)$/i;
	    if (! rFilter.test(oFile.type)) {
	    	errorDiv.innerHTML = '<ul><li>' + invalidImageError + '</li></ul>';
	       	errorDiv.style.display = 'block';
	    	return;
	    }
	 
	    // check for file size
	    if (oFile.size > 300 * 1024) {
	        errorDiv.innerHTML = '<ul><li>' + tooBigImageError + '</li></ul>';
	    	errorDiv.style.display = 'block';
	    	return;
	    }
	      	 
  	    // prepare HTML5 FileReader
  	    var oReader = new FileReader();
        oReader.onload = function(e) {		  
  	        // e.target.result contains the DataURL which we can use as a source of the image
        	
        	var newImage = new Image();
        	newImage.onload = function() {
        		  
        		  if(this.width > 900 || this.width < 750 || this.height < 600){
        			  errorDiv.innerHTML = '<ul><li>' + dimensionsImageError + '</li></ul>';
        		      errorDiv.style.display = 'block';
        		      return;
        		  }else{
        			
        			oImagePreview.src = oImage.src =   this.src;    	  	        
        
	    	  	    oImagePreview.onload = function () {	    	  	    	 
	      			    $('#preview').css("width",  (newImage.width *  150 ) / 750  +'px');
	  		  	        $('#preview').css("height", (newImage.height * 120 ) / 600  +'px');
	      			};
	      			
	      			$('#target').toggleClass("jc-demo-box", true);
	      			
    	  	        oImage.onload = function () { // onload event handler
    	  	        	$('#preview-pane').hide().fadeIn('fast');
    	  	        	$('#demo').hide().fadeIn('fast');
    	  	        	    	  	        		
    		  	        // destroy Jcrop if it is existed
    		  	        if (typeof jcrop_api != 'undefined') {
    		  	                jcrop_api.destroy();
    		  	                jcrop_api = null;	  	             
    		            }
    		  	         
    		  	        $('#target').css("width", newImage.width +'px');
    		  	        $('#target').css("height",newImage.height +'px');
    		  	         
    		  	         // initialize Jcrop
    	                $('#target').Jcrop({                                    
    	                    aspectRatio: 5/4,     	  
    	                    boxWidth: 300,   
    	                    boxHeight: 240,
    	                    bgColor: 'white',	                    
    	                    minSize: [750, 600],
    	                    maxSize: [750, 600],
    	                    onChange: updatePreview,
    	                    onSelect: updatePreview,
    	                    setSelect: [0, 0, 750, 600]
    	                }, function(){		  
    	                    // use the Jcrop API to get the real image size
    	                    var bounds = this.getBounds();
    	                    boundx = bounds[0];
    	                    boundy = bounds[1];
    	 
    	                    // Store the Jcrop API in the jcrop_api variable
    	                    jcrop_api = this;
    	                    // Move the preview into the jcrop container for css positioning
    	                    $preview.appendTo(jcrop_api.ui.holder);
    	                });		  	               	                
    	  	        };    	  	       
        		 }
        	};        	
        	newImage.src=e.target.result;
        };  	 
  	    // read selected file as DataURL
  	    oReader.readAsDataURL(oFile);
  }

(function($){
  $(document).ready(function()  {
	// Grab some information about the preview pane
    $preview = $('#preview-pane'),
    $pcnt = $('#preview-pane .preview-container'),
    $pimg = $('#preview-pane .preview-container img');
    xsize = $pcnt.width();
    ysize = $pcnt.height();
  });
})(jQuery);