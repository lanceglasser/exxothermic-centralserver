(function($){
	
  var code;
  $(document).ready(function(){
	  $("#type,#country,#state").select2();  
	  
	  $("#commercialIntegrator").select2({	      
	      allowClear: true
	  });
	    
	  code = $( "#type").find(":selected").attr("id");
	  	  
	  if( code =='other' || code=='waitingRoomSpecify')	  
	  {
		  $("#controlCompanyOtherType").show();
		  $("#otherType").attr("required","required");
		  
	  }
	  else
	  {
		  $("#controlCompanyOtherType").hide();
		  $("#otherType").removeAttr("required");
		  $("#otherType").attr("value","");
	  }
	  
	  $("#type").change(function(){
		  code = $( "#type").find(":selected").attr("id");
		  
		  if( code=='other' || code=='waitingRoomSpecify')
		  {
			  $("#controlCompanyOtherType").show();
			  $("#otherType").attr("required","required");
			  
		  }
		  else
		 {
			  $("#controlCompanyOtherType").hide();
			  $("#otherType").removeAttr("required");
			  $("#otherType").attr("value","");
		 }
	  });
	   
	  
	  
	  $("#country").change(function(){		  
		  $.getJSON(loadStatesByCountryUrl + "?id="+$(this).val(),function(data){
			  try
			  {
				  if(data.states.length > 0 ){
					  $("#stateNameDiv").hide();
					  $("#stateDiv").show();
					  //remove the plugin select2
					  $("#state").select2("destroy");
					  //add the data
					  $("#state").setSelectOptions(data.states,"id","name");
					  //restart the pluggin select2
					  $("#state").select2();
					  //Trigger to change the City select box
					  $("#state").change();
				  }else{
					  $("#stateNameDiv").show();
					  $("#stateDiv").hide();
				  }
			  }
			  catch(Exception){}
			 
		  });
	  });
	  
	  $("#maxOccupancy, #numberOfTv, #numberOfTvWithExxothermicDevice").keyup(function () { 
		    this.value = this.value.replace(/[^0-9\.]/g,'');
		    
		});
  });
})(jQuery);