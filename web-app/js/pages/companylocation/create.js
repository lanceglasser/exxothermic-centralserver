
var map = null;
var geocoder = null;
//var infowindow = new google.maps.InfoWindow();
var marker;
var flaglat = false;
var flaglng = false;


function initialize(lat, lng) {
    if (GBrowserIsCompatible()) {
      map = new GMap2(document.getElementById("map-canvas"));
      map.setCenter(new GLatLng(37.4419, -122.1419), 1);
      map.setUIToDefault();
      geocoder = new GClientGeocoder();
      
      if(lat !== -1){
         setPointByCoordinates(lat, lng);
      } else {
         //Exxo.Location.Create.updateGoogleMapPosition();
      }
    }
  }

function setPointByCoordinates(lat, lng){
   if (geocoder && map) {
      var point = new GLatLng(lat, lng);
      map.setCenter(point, 15);
      marker = new GMarker(point, {draggable: true});
      map.addOverlay(new GMarker(point));
      marker.openInfoWindowHtml(marker.getLatLng().toUrlValue(6));
      GEvent.addListener(marker, "dragend", function() {
         marker.openInfoWindowHtml(marker.getLatLng().toUrlValue(6));
         setCoordinates(marker.getLatLng().lat(), marker.getLatLng().lng());
       });
      GEvent.addListener(marker, "click", function() {
         marker.openInfoWindowHtml(marker.getLatLng().toUrlValue(6));
       });
      GEvent.trigger(marker, "click");
   }
}

function showAddress(address) {
   if (geocoder && map) {
      if(marker){
         marker.closeInfoWindow();
      }
      map.clearOverlays();
      map.getDefaultUI();
      
      if(address !== null && address !== ""){         
         geocoder.getLatLng(
           address,
           function(point) {
             if (!point) {
               //alert(address + " not found");
               console.debug("Address not found.");
               setCoordinates(-1, -1);
             } else {          
               map.setCenter(point, 15);
               marker = new GMarker(point, {draggable: true});
               setCoordinates(marker.getLatLng().lat(), marker.getLatLng().lng());
               map.addOverlay(marker);
               
               GEvent.addListener(marker, "dragend", function() {
                 marker.openInfoWindowHtml(marker.getLatLng().toUrlValue(6));
                 setCoordinates(marker.getLatLng().lat(), marker.getLatLng().lng());
               });
               GEvent.addListener(marker, "click", function() {
                 marker.openInfoWindowHtml(marker.getLatLng().toUrlValue(6));
               });
               GEvent.trigger(marker, "click");
             }
           }
         );
    }
   }
}

function setCoordinates(lat, lng){
   $("#latitude").val(lat);
   $("#longitude").val(lng);
}


/* Latitute and Longitude
function initialize() {
  geocoder = new google.maps.Geocoder();
  var latlng = new google.maps.LatLng(26.09471,-29.982826);
  var mapOptions = {
    zoom: 1,
    center: latlng,
    mapTypeId: 'roadmap'
  };
  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
}


function showLatLng(latlng) {
	  var input = latlng;
	  var latlngStr = input.split(',', 2);
	  var lat = parseFloat(latlngStr[0]);
	  var lng = parseFloat(latlngStr[1]);
	  var latlng = new google.maps.LatLng(lat, lng);
	  
	  geocoder.geocode({'latLng': latlng}, function(results, status) {
	    if (status == google.maps.GeocoderStatus.OK) {
	      if (results[1]) {
	        marker = new google.maps.Marker({
	            position: latlng,
	            map: map
	        });
	    	map.setZoom(12);
	        infowindow.setContent(results[1].formatted_address);
	        infowindow.open(map, marker);
	        } else {
	        alert('No results found');
	      }
	    } else {
	      alert('Geocoder failed due to: ' + status);
	    }
	  });	  
	  flaglat= false;
	  flaglng= false;
	}
*/

	      

