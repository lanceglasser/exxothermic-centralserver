(function($){

if (typeof console == "undefined" || typeof console.log == "undefined" || typeof console.debug == "undefined") var console = { log: function() {}, debug: function() {} }; 

$(document).ready(function() {
	Cecropia.Company.Create.initComponents();
});


})(jQuery);

var isEventGeneratedFromCheckboxUseCompanyInfo = false;
var defaultCountryId;
var defaultTypeEstablishmentId;
var defaultStateId;
var code;

var Cecropia = {
	Company : {
		Create : {
			initComponents: function(e){		

				$.ajaxSetup({
					   statusCode: {
					     401: function () {
					    	 window.location = baseURL + '/login';
					     }
					   }
					});

				
				$("#wizardForm").submit(function(e){
					e.preventDefault();
					 $.ajax({type:'POST',
						   data:'_eventId_continue=1&'+$("#wizardForm").serialize(), 								  
						   url: baseURL + '/wizard/pages', 
						   success:function(data) {							   
							   $("#wizardPage").html(data);
	                         },
						   error:function(XMLHttpRequest,textStatus,errorThrown){							   
					            alert(errorThrown);
							   	hideSpinner();
							   	null;
						 }
					  });
					 
					 return false;
				   });
				 
				$('.questionToolTip').tooltipster({
	                position: 'right',
	                maxWidth: 210 
	              });
				
				 
				$("#type,#country,#state,#typeOfCompany").select2();
				$("#location").select2();
			  	  
				   
				code = $( "#type").find(":selected").attr("id");
				  if(code=='other' || code=='waitingRoomSpecify')
				  {
					  $("#controlCompanyOtherType").show();
					  $("#otherType").attr("required","required");
					  
				  }
				  else
				  {
					  $("#controlCompanyOtherType").hide();
					  $("#otherType").removeAttr("required");
				  }
				  
				  $("#type").change(function(){
					  code = $( "#type").find(":selected").attr("id");
					  
					  if( code=='other' || code=='waitingRoomSpecify')
					  {
						  $("#controlCompanyOtherType").show();
						  $("#otherType").attr("required","required");						 
					  }
					  else
					 {
						  $("#controlCompanyOtherType").hide();
						  $("#otherType").removeAttr("required");
					 }
				  });
				  				  
				  
				  $("#webSite").change(function(){
					  $(this).val($.trim($(this).val()));
					  
					  var value = $(this).val();			
					  if ( value.length > 0){	
						  if(this.value.substr(0,7) != 'http://'){
							  this.value = 'http://' + this.value;
						  }
						  if(this.value.substr(this.value.length-1, 1) != '/'){
							  this.value = this.value + '/';
						  }
					  }
				  });
				    
				  $("#country").change(function(){							  
					  $.getJSON(loadStatesByCountryUrl + "&id="+$(this).val(),function(data){
						  try
						  {
							  if(data.states.length > 0 ){
								  $("#stateNameDiv").hide();
								  $("#stateDiv").show();
								  //remove the plugin select2
								  $("#state").select2("destroy");
								  //add the data
								  $("#state").setSelectOptions(data.states,"id","name");
								  //restart the pluggin select2
								  $("#state").select2();
								  //Trigger to change the City select box
								  $("#state").change();
							  }else{
								  $("#stateNameDiv").show();
								  $("#stateDiv").hide();
							  }
						  }
						  catch(Exception){}
						 
					  });
				  });
				  
				  $(".onlyNumbers").keypress(function(event) {
					  // Backspace, tab, enter, end, home, left, right
					  // We don't support the del key in Opera because del == . == 46.
					  var controlKeys = [8, 9, 13, 35, 36, 37, 39];
					  // IE doesn't support indexOf
					  var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
					  // Some browsers just don't raise events for control keys. Easy.
					  // e.g. Safari backspace.
					  if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
					      (48 <= event.which && event.which <= 57) || // Always 1 through 9
					      //(48 == event.which && $(this).attr("value")) || // No 0 first digit
					      isControlKey) { // Opera assigns values for control keys.
					    return;
					  } else {
					    event.preventDefault();
					  }
					});							
				}
			}
		},
 Location : {
	Create : {
		initComponents: function(){		
			defaultStateId  = $("#state").val();
			defaultTypeEstablishmentId = $("#type").val();
			defaultCountryId = $("#country").val();			 
			 
			$.ajaxSetup({
				   statusCode: {
				     401: function () {
				    	 window.location = baseURL + '/login';
				     }
				   }
				});

			
			$("#wizardForm").submit(function(e){
				e.preventDefault();
				 $.ajax({type:'POST',
					   data:'_eventId_continue=1&'+$("#wizardForm").serialize(), 								  
					   url: baseURL + '/wizard/pages', 
					   success:function(data) {							   
						   $("#wizardPage").html(data);
                         },
					   error:function(XMLHttpRequest,textStatus,errorThrown){							   
				            alert(errorThrown);
						   	hideSpinner();
						   	null;
					 }
				  });
				 
				 return false;
			   });
			
			$('.questionToolTip').tooltipster({
                position: 'right',
                maxWidth: 210 
              });
			
			$("#type,#country,#state,#company").select2();  
		  	  
			
			$("#commercialIntegrator").select2({	      
		        allowClear: true
		    });
			
			code = $( "#type").find(":selected").attr("id");
			
			if( code =='other' || code=='waitingRoomSpecify')	  			
			{
				  $("#controlCompanyOtherType").show();
				  $("#otherType").attr("required","required");
				  
			  }
			  else
			  {
				  $("#controlCompanyOtherType").hide();
				  $("#otherType").removeAttr("required");
			  }
			  
			  $("#type").change(function(){
				  
				  code = $( "#type").find(":selected").attr("id");				  
				  if( code=='other' || code=='waitingRoomSpecify')				  
				  {
					  $("#controlCompanyOtherType").show();
					  $("#otherType").attr("required","required");
					  
				  }
				  else
				 {
					  $("#controlCompanyOtherType").hide();
					  $("#otherType").removeAttr("required");
				 }
			  });
			  
			  $("#webSite").change(function(){
				  $(this).val($.trim($(this).val()));
				  
				  var value = $(this).val();			
				  if ( value.length > 0){	
					  if(this.value.substr(0,7) != 'http://'){
						  this.value = 'http://' + this.value;
					  }
					  if(this.value.substr(this.value.length-1, 1) != '/'){
						  this.value = this.value + '/';
					  }
				  }
			  });
			    
			  $("#country").change(function(){	
				  var isChecked = $("#useCompanyinformationCheckbox").prop("checked");
				  if(isChecked == "checked" || isChecked == true) {
					  	//do nothing
				  }else{
					  $.getJSON(loadStatesByCountryUrl + "&id="+$(this).val(),function(data){
						  try
						  {
							  if(data.states.length > 0 ){
								  $("#stateNameDiv").hide();
								  $("#stateDiv").show();
								  //remove the plugin select2
								  $("#state").select2("destroy");
								  //add the data
								  $("#state").setSelectOptions(data.states,"id","name");
								  //restart the pluggin select2
								  
								  $("#state").select2();
							  }else{
								  $("#stateNameDiv").show();
								  $("#stateDiv").hide();
							  }
						  
						  }
						  catch(Exception){}					  		  
					  });
				   }
			  });
			  
			  $(".onlyNumbers").keypress(function(event) {
				  // Backspace, tab, enter, end, home, left, right
				  // We don't support the del key in Opera because del == . == 46.
				  var controlKeys = [8, 9, 13, 35, 36, 37, 39];
				  // IE doesn't support indexOf
				  var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
				  // Some browsers just don't raise events for control keys. Easy.
				  // e.g. Safari backspace.
				  if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
				      (48 <= event.which && event.which <= 57) || // Always 1 through 9
				      //(48 == event.which && $(this).attr("value")) || // No 0 first digit
				      isControlKey) { // Opera assigns values for control keys.
				    return;
				  } else {
				    event.preventDefault();
				  }
				});
			  

			   $("#maxOccupancy, #numberOfTv, #numberOfTvWithExxothermicDevice").change(function () { 
				    this.value = this.value.replace(/[^\d]/g,'');
				}); 
			  
			  $("#company").change(function(){	  
				  $("#useCompanyinformationCheckbox").change();
			  });
			  
			  // Ajax to use company information
			  $("#useCompanyinformationCheckbox").change(function(){
				  
				  isEventGeneratedFromCheckboxUseCompanyInfo = true;
				  var isChecked = $("#useCompanyinformationCheckbox").prop("checked");
				  
				   if(isChecked == "checked" || isChecked == true) {
					   $.getJSON(getCompanyInfoURL + "&companyId="+$("#company").val(), function(data){
						  
						   try				  
						  {
							  
							  //Address
							  $("#address").val(data.address); 
							  $('#address').attr("readonly", true);
							  
							  //city
							  $("#city").val(data.city);
							  $('#city').attr("readonly", true);
							  		
							  //type
							  if(data.typeId != null) {
								  $("#type").val(data.typeId).trigger("change");//change the value and update the display
								  $("#type").select2("disable", true);		
								  $("#otherType").val(data.other);
								  $('#otherType').attr("readonly", true);
							  }
							  
							  //state
							  if(data.states.length > 0 ){
								  $("#stateNameDiv").hide();
								  $("#stateDiv").show();
								  //state
								  //remove the plugin select2
								  $("#state").select2("destroy");
								  //add the data
								   $("#state").setSelectOptions(data.states,"id","name");
								  //select the option
								  //restart the pluggin select2
								  $("#state").select2();							 
								  $("#state").val(data.stateId).trigger("change");
								  $("#state").select2("disable", true);	
							  }else{
								  $("#stateNameDiv").show();
								  $("#stateDiv").hide();
								  $("#stateName").val(data.stateName);
								  $('#stateName').attr("readonly", true);
							  }
								  									   
							   //country
							   $("#country").val(data.countryId).trigger("change");
							   $("#country").select2("disable", true);
							  
						  }
						  catch(Exception){	}			 
					   });
				   } else {
					   isEventGeneratedFromCheckboxUseCompanyInfo = false;
					   $("#stateDiv").show();
					   $("#stateNameDiv").hide();	
					   $('#address').attr("readonly", false);
					   $('#address').val("");
					   $("#country").select2("enable", false);
					   $("#country").val(defaultCountryId).trigger("change");
					   $('#city').val("");
					   $('#city').attr("readonly", false);
					   $("#stateName").val("");
					   $('#stateName').attr("readonly", false);
					   $("#type").select2("enable", false); 
					   $("#type").val(defaultTypeEstablishmentId).trigger("change");
					   $("#otherType").val("");
					   $('#otherType').attr("readonly", false);
					   $("#state").select2();
					   $("#state").select2("enable", false);
					   $("#state").val(defaultStateId).trigger("change");
				   }
			  });
			  
			   
			}
		}
	}
};
