/*! ExXothermic Dashboard Widget
 * ©2015 - ExXothermic
 */

/**
 * @summary     Dashboard Widget
 * @description General Widget Object to be Use at the Dashboards; includes hide, show,
 * 				drag and drop and customization options between others.
 * @version     0.0.1
 * @file        jquery.exxo.widget.js
 * @author      Cecropia Solutions
 * @contact     www.cecropiasolutions.com
 * @copyright   Copyright 2015 - ExXothermic
 *
 */

(function($) {
	$.fn.extend({
		
		exxoWidget : function(options) {

			var defaults = {
				columnSize:				4,
				minHeight:				'200px',
				referenceIcon:			'',
				referenceUrl:			'#',
				helpMsg:				'',
				showDashboardTitleRef:	true,
				enableConfigWindow:		false,
				enableAutoRefresh:		false,
				autoRefresh:			false,
				allowRemove:			true
			};

			// Extend your options with the default values
			var options = $.extend(defaults, options);
			
			function generateHeader(title, referenceIcon, referenceUrl, 
					helpMsg, showDashboardTitleRef, status, options){
				var html = '<header>' 
					+ '<div class="widget-menu">'
						+ '<div class="widgetMenuIcon">' 
						+ '</div>'
						+ '<div class="widget-menu-actions">';
				
				switch(referenceIcon){
					case "list":
						//html += '<a href="' + referenceUrl + '" class="widgetList opIcons" title="' + Exxo.UI.translates['WidgetListIcon'] + '"></a>'; 
						html += '<a href="' + referenceUrl + '" class="widgetList">' + Exxo.UI.translates['WidgetListIcon'] + '</a>'
						break;
					default:
						break;
				}
								
				if(options.enableConfigWindow){
					html   += '<a class="widgetConfig">' + Exxo.UI.translates['WidgetConfigIcon'] + '</a>';
				}
				
				html   += '<a class="widgetRefresh">' + Exxo.UI.translates['WidgetRefreshIcon'] + '</a>';
				
				/*
				if(status === "min"){
					html += '<a href="#" class="widgetMin" style="display: none;">' + Exxo.UI.translates['WidgetMinIcon'] + '</a>'
					+ '<a href="#" class="widgetMax">' + Exxo.UI.translates['WidgetMaxIcon'] + '</a>';
				} else {
					html += '<a href="#" class="widgetMin">' + Exxo.UI.translates['WidgetMinIcon'] + '</a>'
					+ '<a href="#" class="widgetMax" style="display: none;">' + Exxo.UI.translates['WidgetMaxIcon'] + '</a>';
				}
				*/		
				
				if(options.allowRemove){
					html += '<a href="#" class="widgetDelete">' + Exxo.UI.translates['remove'] + '</a>';
				}
				
				html += '</div>'
					+ '</div>'
					+ title;
				
				if(showDashboardTitleRef) html += ' ' + Exxo.UI.translates['dashboard-titles-ref'];
				
				/*
				html += '<a href="" class="widgetDelete opIcons" title="' + Exxo.UI.translates['remove'] + '"> </a>'
					+ '<a href="" class="widgetMin opIcons" title="' + Exxo.UI.translates['WidgetMinIcon'] + '"></a>'
					+ '<a href="" class="widgetMax opIcons" style="display: none;" title="' + Exxo.UI.translates['WidgetMaxIcon'] + '"> </a>'
					;
				*/
				
				if(helpMsg !== ""){
					//html += '<a href="#" class="widget-info-icon" title="' + helpMsg + '"></a>'
					html += '<a href="#" class="widgetInfo opIcons" title="' + helpMsg + '"></a>'
				}
				
				//html += '<a href="" class="widgetRefresh opIcons" title="' + Exxo.UI.translates['WidgetRefreshIcon'] + '"></a>' 
				html += '</header>';
				
				return html;
			}

			return this.each(function() {
				var o = options;

				var widget = $(this);
				
				var html = 
					generateHeader((widget.attr('widget-title')? widget.attr('widget-title'):''), 
							o.referenceIcon, o.referenceUrl, o.helpMsg, o.showDashboardTitleRef, widget.attr("status"), o)
					+ '<div class="widget-internal-content"' + (widget.attr("status")=="min"? ' style="display:none;"':'') + '>'
					+ widget.html()
					+ '</div>';
				
				widget.html(html);
			});
		}
	});
})(jQuery);