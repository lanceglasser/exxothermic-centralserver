/***
 @file      exxo_validation.js
 @brief     General functions for the Validation of some Form fields
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Validation = {
    NO_ERROR:           0,
    ERROR_LENGHT:       -10,
    ERROR_PASS_CONFIRM: -20,
    ERROR_INVALID_INPUT: -30,
    EXXTRACTORS_USER_PASSWORD: 1,
    cleanInputErrors: function(usernameId, passId, confirmId){
    	var inputPass = document.getElementById(passId);
        var inputConfirm = document.getElementById(confirmId);
        inputPass.setCustomValidity('');
        inputConfirm.setCustomValidity('');
        var input = document.getElementById(usernameId);
        input.setCustomValidity('');
    },
    validatePasswordWithConfirmation: function(passId, confirmId, type){
       if(type === Exxo.Validation.EXXTRACTORS_USER_PASSWORD){
          //Validates the password for the ExXtractors Users
          var value = $("#"+passId).val();
          value = value.trim();
          var inputPass = document.getElementById(passId);
          var inputConfirm = document.getElementById(confirmId);
          inputPass.setCustomValidity('');
          inputConfirm.setCustomValidity('');
          if(value.length < 4 || value.length > 8){
             //inputPass.setCustomValidity(Exxo.UI.translates['passwordError']);
             Exxo.Validation.showError(Exxo.UI.translates['passwordError']);
             $("#"+passId).focus();
             return Exxo.Validation.ERROR_LENGHT;
             
          } else {
             //Checks confirmation
             if(value !== $("#"+confirmId).val()){
                $("#"+confirmId).focus();
                //inputConfirm.setCustomValidity(Exxo.UI.translates['confirmError']);
                Exxo.Validation.showError(Exxo.UI.translates['confirmError']);
                return Exxo.Validation.ERROR_PASS_CONFIRM;
             }
          }
       }
       return Exxo.Validation.NO_ERROR;
    },
    validateUsername: function(usernameId){
       var value = $("#"+usernameId).val();
       value = value.trim();
       console.debug("Validate input: " + value);
       var input = document.getElementById(usernameId);
       input.setCustomValidity('');
       var resultCode = Exxo.Validation.NO_ERROR;
       if(value.length < 4 || value.length > 16){
          resultCode = Exxo.Validation.ERROR_LENGHT;
       } else {
    	   //Validates only letters and numbers
    	   if(!Exxo.Validation.alphanumeric(value)){
    		   resultCode = Exxo.Validation.ERROR_INVALID_INPUT
    	   }
       }
       if(resultCode !== Exxo.Validation.NO_ERROR){
    	   //input.setCustomValidity(Exxo.UI.translates['usernameError']);
           Exxo.Validation.showError(Exxo.UI.translates['usernameError']);
           $("#"+usernameId).focus();
       }
       console.debug("resultCode: " + resultCode);
       return resultCode;
    },
    hideError: function(){
       $("#divError").hide();
    },
    showError: function(msg){
       $("#errorMessage").html(msg);
       $("#divError").show();
    },
    alphanumeric: function(text){
    	var lettersAndNumbers = /^[0-9a-zA-Z]+$/;  
    	return text.match(lettersAndNumbers);
    	//See more at: http://www.w3resource.com/javascript/form/letters-numbers-field.php#sthash.QHZJIjqo.dpuf
    },
    isValidVersionLabel: function(obj) {
      var versionLabelRE = /^\d+(\.\d+(\.\d+)?)?$/
      return obj.match(versionLabelRE);
    }
};
