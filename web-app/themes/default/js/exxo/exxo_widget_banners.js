/***
 @file      exxo_widget_banners.js
 @brief     Functions for the Banners List Widget
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.WidgetsBanners = {
	initBannersListWidget : function() {

		$("#banners-widget").exxoWidget({
			helpMsg : Exxo.UI.translates['widget-banners-list-info-msg'],
			referenceIcon : 'list',
			referenceUrl : Exxo.UI.urls['banners-list']
		});

		Exxo.UI.vars["widgetBannersTable"] = $('#widgetBannersTable')
				.dataTable(
						{

							"pagingType" : "full", // More info at: https://datatables.net/examples/basic_init/alt_pagination.html
							"lengthMenu" : [ [ 5 ], [ 5 ] ], // More info at: https://datatables.net/examples/advanced_init/length_menu.html
							"processing" : true,
							"serverSide" : true,
							"ajax" : {
								"url" : Exxo.UI.urls["widget-get-data-url"],
								"data" : function(d) {
									d.object = 'banner';
									d.companyId = Exxo.UI.vars["company-id"];
									d.locationId = Exxo.UI.vars["location-id"];
								}
							},
							"columnDefs" : [
									{
										"targets" : [ 0 ],
										"render" : function(data, type, row) {
											if(Exxo.UI.vars["can-edit-banners"]){
												return '<a href="'
												+ Exxo.UI.urls["banners-edit-url"]
												+ row[3] + '">' + row[0]
												+ '</a>';
											} else {
												return row[0];
											}											
										}
									},
									{
										"targets" : [ 2 ],
										"render" : function(data, type, row) {
											return '<div class="state '
													+ (row[2] ? 'stateGreen'
															: 'stateGray')
													+ '"></div>';
										}
									},
									{
										"targets" : [ 1 ],
										"render" : function(data, type, row) {
											if (row[1] == "" || 
												row[1] == "null" ||
												row[1] == undefined) {
												return "-";
											} else {
												return '<img src="'
													+ row[1] + '" width="64px" height="28px"/>';
											}
										}
									} ]
						});
	}
};