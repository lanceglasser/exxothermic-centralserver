/***
 @file      exxo_box_unregister.js
 @brief     Custom functions and events for the Box UnRegistration page.
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Box = {
    Unregister : {
    	filterByText: function(box, text) {
    		text = text.toUpperCase();
    		return box.serial.toUpperCase().indexOf(text) > -1 || 
    				box.name.toUpperCase().indexOf(text) > -1 || 
    				box.venue.toUpperCase().indexOf(text) > -1;
    	},
       init : function() {
          $("#box,#location").select2();
          Exxo.Tables.init();
          
          $("#textToSearch").keyup(function(){
        	  $("#boxes-table > tbody").html("");
        	  var boxes = Exxo.UI.vars["current-boxes"];
        	  var textToSearch = $(this).val();
        	  var counter = 0;
        	  var rowClass = "";
        	  for(var i=0; i<boxes.length; i++) {
        		  var box = boxes[i];
        		  if(Exxo.Box.Unregister.filterByText(box, textToSearch)) {
        			  rowClass = (counter % 2 === 0)? 'even':'odd';
        			  var lastSeenColumn = "<td>";
        			  if(box.connected){
        				  lastSeenColumn += '<i class="state stateConnected"></i>';
        				  lastSeenColumn += '<span class="greenText" title="' + box.statusDescription + '">';
        				  lastSeenColumn += box.lastSeen;
        				  lastSeenColumn += '</span>';
        			  } else {
        				  lastSeenColumn += '<i class="state stateDisconnected"></i>';
        				  lastSeenColumn += '<span class="redText" title="' + box.statusDescription + '">';
        				  lastSeenColumn += box.lastSeen;
        				  lastSeenColumn += '</span>';
        			  }
        			  lastSeenColumn += "</td>";
					   
        			  var newRowContent = '<tr class="' + rowClass + '">' +
							'<td>' + '<input type="checkbox" name="boxes" id="boxes" value="' 
								+ box.id + '"> </td>' +
							'<td> ' + box.serial + '</td>' +
							'<td> ' + box.name + '</td>' +
							'<td> ' + box.model + '</td>' +
							'<td> ' + box.venue + '</td>'+
							lastSeenColumn +
							'</tr>';
        			  $("#boxes-table tbody").append(newRowContent);
        			  counter++;
        		  }
        	  }
          });
          
          $("#location").change(function(e) {
        	  Exxo.showSpinner();
        	  $("#boxes-table > tbody").html("");
        	  var boxes = Exxo.UI.vars["all-boxes"];
        	  var locationId = $(this).val();
        	  var textToSearch = $("#textToSearch").val();
        	  var counter = 0;
        	  var rowClass = "";
        	  var currentBoxes = new Array();
        	  for(var i=0; i<boxes.length; i++){
        		  var box = boxes[i];
        		  if(locationId == 0 || locationId == box.venueid) {
        			  currentBoxes.push(box);
        			  // Validate text
        			  if(Exxo.Box.Unregister.filterByText(box, textToSearch)) {
        				  rowClass = (counter % 2 === 0)? 'even':'odd';
            			  var newRowContent = '<tr class="' + rowClass + '">' +
    							'<td>' + '<input type="checkbox" name="boxes" id="boxes" value="' 
    								+ box.id + '"> </td>' +
    							'<td> ' + box.serial + '</td>' +
    							'<td> ' + box.name + '</td>' +
    							'<td> ' + box.model + '</td>' +
    							'<td> ' + box.venue + '</td>'+													
    							'</tr>';
            			  $("#boxes-table tbody").append(newRowContent);
            			  counter++;
        			  }
        		  }
        	  }
        	  Exxo.UI.vars["current-boxes"] = currentBoxes;
        	  Exxo.hideSpinner();
          });
       }
    }
};