/***
 @file      exxo_employee_asign.js
 @brief     Custom functions and events for the Employee asign page
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Employee = {
    Asign : {
       init : function() {
          $("#user").select2();
          $("#user").trigger('change');
       }
    }
};