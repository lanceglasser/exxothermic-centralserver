/***
 @file      exxo_help_center.js
 @brief     Specific functions for the Help Center Window
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.HelpCenter = {
	init : function() {
		$(".helpCategoryLink").click(function(e){
			$(".helpCategoryLink").removeClass("selected");
			$(this).addClass("selected");
		});
		
		$("#"+Exxo.UI.vars["cat"]+"Link").addClass("selected");
		
		$("#initialLoadLink").click(function(e){
			$("#help-container").show();
		});
		$("#initialLoadLink").click();
		$("#help-container").click();
	},
	initHelpPage : function(){		
		$(".subcat-link").unbind().click(function(e){
			e.preventDefault();
			var ref = $(this).attr('ref');
			$("#subcat-list-"+ref).toggle();
			$(this).find("span").toggleClass("open");
			return false;
		});
		
		$(".help-link").click(function(e){
			e.preventDefault();
			var template = $(this).attr('template');
			var cat = $(this).attr('cat');
			Exxo.HelpCenter.loadHelpPage(cat, template);
			return false;
		});
	},
	loadHelpPage: function(cat, template){
		jQuery.ajax(
			{
			type:'POST',
			data:{'template': template, 'cat': cat}, 
			url:Exxo.UI.urls["show-help-page-url"],
			success:function(data,textStatus){
				jQuery('#main-help-container').html(data);
			},
			error:function(XMLHttpRequest,textStatus,errorThrown){}
			});
			
		//Exxo.HelpCenter.initHelpPage();
		return false;
	}
};