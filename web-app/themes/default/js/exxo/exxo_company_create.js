/***
 @file      exxo_company_create.js
 @brief     Custom functions and events for the Company Create Page.
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Company = {
    Create : {
       defaultTimezone: 'UTC',
       init : function() {
          //Init Images Functions
          Exxo.Images.init();
          
          $("#updateForm,#wizardForm").submit(function(e){
        	  Exxo.showSpinner();
          });
          
          Exxo.UI.vars['code'] = $( "#type").find(":selected").attr("id");
          
          $("#type,#country,#state,#typeOfCompany,#timezone,#levelOfService,#companyIntegrator").select2();
          
          $('#timezone').val(Exxo.Company.Create.defaultTimezone);
          $('#timezone').change();
          if( Exxo.UI.vars['code'] =='other' || Exxo.UI.vars['code'] == 'waitingRoomSpecify')  
          {
              $("#controlCompanyOtherType").show();
              $("#otherType").attr("required","required");
              
          }
          else
          {
              $("#controlCompanyOtherType").hide();
              $("#otherType").removeAttr("required");
          }
          
          $("#type").change(function(){
              Exxo.UI.vars['code'] = $( "#type").find(":selected").attr("id");
              if( Exxo.UI.vars['code']=='other' || Exxo.UI.vars['code']=='waitingRoomSpecify')
              {
                  $("#controlCompanyOtherType").show();
                  $("#otherType").attr("required","required");
                  
              }
              else
             {
                  $("#controlCompanyOtherType").hide();
                  $("#otherType").removeAttr("required");
             }
          });
          
          $("#webSite").change(function(){
              $(this).val($.trim($(this).val()));
              
              var value = $(this).val();            
              if ( value.length > 0){   
                  if(this.value.substr(0,7) != 'http://' && this.value.substr(0,8) != 'https://'){
                      this.value = 'http://' + this.value;
                  }
                  if(this.value.substr(this.value.length-1, 1) != '/'){
                      this.value = this.value + '/';
                  }
              }
          });
            
          $("#country").change(function(){
              //$.getJSON(loadStatesByCountryUrl + "../common/statebycountry?shortName="+$(this).val(),function(data){
             var url = Exxo.UI.urls["loadStatesByCountryUrl"];
             if(Exxo.Utils.Urls.getUrlParam(url, 'execution') == null){
                url += "?id="+$(this).val();
             } else {
                url += "&id="+$(this).val();
             }
             
             Exxo.UI.setInputsEventsByCountry($(this).val());
             
              $.getJSON(url,function(data){
                  try
                  {
                      if(data.states.length > 0 ){
                          $("#stateNameDiv").hide();
                          $("#stateDiv").show();
                          //remove the plugin select2
                          $("#state").select2("destroy");
                          //add the data
                          $("#state").setSelectOptions(data.states,"id","name");
                          //restart the pluggin select2
                          $("#state").select2();
                          //Trigger to change the City select box
                          $("#state").change();
                      }else{
                          $("#stateNameDiv").show();
                          $("#stateDiv").hide();
                      }
                  }
                  catch(Exception){}
                 
              });
          });
          
          Exxo.UI.setInputsEventsByCountry($("#country").val());
       }
    }
};