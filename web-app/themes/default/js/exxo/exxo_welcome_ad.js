/***
 @file      exxo_welcome_ad
 @brief     Custom functions and events for the creation and edit of the Welcome Ads
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.WelcomeAd = {
   Create : {
      jcrop_api_1: null,
      jcrop_api_2: null,
      init : function() {
         if(Exxo.Images != undefined)Exxo.Images.init();
         if(Exxo.Tables != undefined)Exxo.Tables.init();
         
         $(".view-imageHelp").click(function(e){
  			e.preventDefault();
  			//console.debug("Must view file: " + $(this).attr("href"));
  			
  			$( "#imageHelp-viewer" ).dialog("open");
  			 			
  			// adds to the url tab1 to set tab1 as selected
  			var url=String(window.location); 			 			
  			if(url.search("#tab")==-1)
  			{	
  					window.location.href = url + "#tab1"
  			}
  			
  		});
  		
  		$("#imageHelp-viewer" ).dialog({
  	          height: 500,
  	          width: 600,
  	          modal: true,
  	          autoOpen: false,
  	          buttons: [ 
  	                { text: Exxo.UI.translates['closeButton'],  
  	                  class:  "dialog-close",
  	                  click: function() {
  	                      //$( this ).dialog( "close" );                   
  	                  }
  	              }
  	          ]
  	        });
  		
  		var buttons = $('.ui-dialog-buttonset').children('button');
  		// remove default color class that overwrite custom css
  		buttons.removeClass('ui-state-default'); 		
  		$( ".dialog-close" ).off(); // remove event handler that adds an hover class // tried to remove only the event, but didn't work
  		$( ".dialog-close" ).click(
  				function() {
  					$("#imageHelp-viewer" ).dialog( "close" );
  				}	
  		);
         
         //Show the div according to the initial Welcome Type
         $("#" + Exxo.UI.vars["welcomeType"] + "-SELECT-DIV").removeClass("hide");
         
         $("#welcomeType").select2();
         
         $("#welcomeType").change(
               function(e){
                  //Hide/Show Container according if value is Image or Video
                  $(".welcome-file").addClass('hide'); //Hide all the divs
                  $("#" + $(this).val() + "-SELECT-DIV").removeClass("hide"); //Show the one according to the value
                  if($(this).val() == "VIDEO") {
                	  if($("#skipEnable").is(':checked')) {
                		  $("#skipTime").removeAttr("readOnly");
                	  } else {
                		  $("#skipTime").attr("readOnly", "readOnly");
                	  }
                  } else { //Image Type
                	  $("#skipTime").removeAttr("readOnly");
                  }
               }
         );
         
         $("#skipEnable").change(function(e){
        	 if($("#welcomeType").val() == "VIDEO") {
        		 if($(this).is(':checked')) {
	           		  $("#skipTime").removeAttr("readOnly");
	           	  } else {
	           		  $("#skipTime").attr("readOnly", "readOnly");
	           	  }
        	 }
         });
         
         $("#welcomeType").change();
                  
         $("#createForm").submit(function(e) {
            Exxo.showSpinner();
            var ids = "";
            $(".cbLocation").each(function(){
               if ($(this).is(':checked')) {
                  console.debug("This location is selected: " + $(this).attr("locationId"));
                  ids += (ids == "")? $(this).attr("locationId"):","+$(this).attr("locationId"); 
               }
            });
            $("#locationsToSave").val(ids);
         });
                  
         /*$(".imgUpload").change(function(e){
            var width = $(this).attr('maxWidth');
            var height = $(this).attr('maxHeight');
            var maxSize = $(this).attr('maxSize');
            Exxo.Utils.Files.validateImageFileExactSize($(this), width, height, maxSize,
                  Exxo.Skin.Create.successImageSelection, Exxo.Skin.Create.errorImageSelection)
         });*/
         
         $("#welcomeVideoUrl").change(function(e){
            //var result = Exxo.Utils.Files.validateVideoFile($(this));
            if(Exxo.Utils.Files.validateVideoFile($(this))){//valid
               $("#welcomeVideoUrl-updated").val(1);
               $('#fakeupload' + $(this).attr('ref')).val($(this).val().split(/(\\|\/)/g).pop());
            } else {
               $("#welcomeVideoUrl-updated").val(0);
               $('#fakeupload' + $(this).attr('ref')).val("");
               e.preventDefault();
            }
         });
         
         $("#fakeupload").val(Exxo.UI.vars["videoFileName"]);
         
           
         /* Video preview removed because browsers incompatibility
          *  
         $("#view-video").click(function(e){
            console.debug("view video click on welcome ad file.");
            Exxo.WelcomeAd.Create.openVideoPlayer();
            return false;
         });
         
         $("#video-container").html($("#video").html());
         $("#video").html("");
         */
         
         /** Delete Options Start **/
         Exxo.UI.vars['stopEvent'] = true;
 		 Exxo.UI.vars['target'] = true;
 		
         $(".delete").click(function(e) {
        	 if (Exxo.UI.isSafari()) {
        		 Exxo.UI.vars['target'] = $(this);
        	 } else {
        		 Exxo.UI.vars['target'] = e.target;
        	 }

 			if (Exxo.UI.vars['stopEvent']) {
 				e.preventDefault();
 				$("#confirm-delete-dialog").dialog("open");
 			} else {
 				Exxo.showSpinner();
 				if (Exxo.UI.isSafari()) {
 					location.href = $(this).attr("href");
 				}
 			}
 		});

 		$("#confirm-delete-dialog").dialog({
 			height : 180,
 			width : 360,
 			modal : true,
 			autoOpen : false,
 			buttons : [ {
 				text : Exxo.UI.translates['okButton'],
 				click : function() {
 					$(this).dialog("close");
 					Exxo.UI.vars['stopEvent'] = false;
 					Exxo.UI.vars['target'].click();
 				}
 			}, {
 				text : Exxo.UI.translates['cancelButton'],
 				click : function() {
 					$(this).dialog("close");
 				}
 			} ]
 		});
 		/** Delete Options End **/
 		
 		/* Enable/Disable Autogeneration of Dialog Image for Tablets */
        $("#genImgForTablets").change(function(e){
        	if(!$(this).is(":checked")) {
        		//If uncheck must give the option to upload a specific image
        		$("#tabletImageObject").show();
        		$("#waGenExampleDiv").hide();
	        } else {
	        	//If check, hide the upload component and shows help message
	        	$("#tabletImageObject").hide();
	        	$("#waGenExampleDiv").show();
	        }
         });
        
        $("#genImgForTablets").trigger('change');
        
      },
      openVideoPlayer: function(){
         console.debug("open video player on welcome ad file.");
         $("body").click(function(e){
            Exxo.WelcomeAd.Create.closeVideoPlayer();
         });
         $("#video-player").show(40);
         $(".video-js")[0].player.play();
      },
      closeVideoPlayer: function() {
         $(".video-js")[0].player.pause();
         $("#video-player").hide();
         $("body").unbind( "click" );
      }
   }
};