/***
 @file      exxo_images_upload.js
 @brief     General functions to handle the image crop and upload
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Images = {
      init : function() {
         $(".imgUploadSelector").change(function(e){
            var minWidth = $(this).attr('minWidth');
            var minHeight = $(this).attr('minHeight');
            var maxWidth = $(this).attr('maxWidth');
            var maxHeight = $(this).attr('maxHeight');
            var maxSize = $(this).attr('maxSize');
            Exxo.Images.hideCropComponents($(this));
            Exxo.Utils.Files.validateImageFile($(this), minWidth, minHeight, maxWidth, maxHeight, maxSize,
                  Exxo.Images.successImageSelection, Exxo.Images.errorImageSelection);
         });
      },
      successImageSelection: function(file){
         $("#"+file.attr('id') + "-updated").val(1);
         $('#fakeupload' + file.attr('ref')).val(file.val().split(/(\\|\/)/g).pop());
      }, 
      errorImageSelection: function(file){
         file.val("");
         $('#fakeupload' + file.attr('ref')).val("");
         $("#"+file.attr('id') + "-updated").val(0);
      },
      resetComponent: function(file){
    	  if(file === undefined || file.attr('cropImage') !== "true") return; //Only execute when must crop the image
          file.val("");
          $('#fakeupload' + file.attr('ref')).val("");
          $("#"+file.attr('id') + "-updated").val(0);
          
          var ref = (file.attr('ref') != null && file.attr('ref') != "undefined")? file.attr('ref'):"";
          var errorDiv = document.getElementById("errorDiv"+ref);
          // Hide error section
          errorDiv.style.display = 'none';
          errorDiv.innerHTML = '';
          
          Exxo.Images.hideCropComponents(file);
      },
      hideCropComponents: function(file){
         if(file.attr('cropImage') !== "true") return; //Only execute when must crop the image
         //Get the reference of the component
         var ref = (file.attr('ref') != null && file.attr('ref') != "undefined")? file.attr('ref'):"";
         var componentId = "" + file.attr('id');
         $('#demo' + ref).hide();
         $('#target' + ref).remove();
         $('#preview-pane-' + componentId).hide();
         $('#preview-pane-' + componentId).appendTo($('#demo'+ ref));
      },
      updatePreview: function( c, componentId, demoId, demoWidth, demoHeight, demoId2, demoWidth2, demoHeight2,
    		  demoId3, demoWidth3, demoHeight3){
         if (parseInt(c.w) > 0) {
        	var finalWidth = Exxo.UI.vars['jcrop_api-' + componentId + "-w"];
        	var finalHeight = Exxo.UI.vars['jcrop_api-' + componentId + "-h"];
        	
        	var previewW = 150;
        	var previewH = 200;
        	var aspectRadio = finalWidth / finalHeight;
        	
        	if(finalWidth >= previewW){
        		previewH = previewW * finalHeight / finalWidth;
        	}
        	
            $pimg = $('#preview-pane-' + componentId + ' .preview-container img');
            var rx = previewW / finalWidth;
            var ry = previewH / finalHeight;
            
            var img_height = Exxo.UI.vars['jcrop_api-' + componentId + "-boundy"];
            var img_width = Exxo.UI.vars['jcrop_api-' + componentId + "-boundx"];
            
            //console.debug("Images [w: " + img_width + ", h: " + img_height + "]");
        	//console.debug("[rx:" + rx + ", ry:" + ry + "]");
        	//console.debug("Pimg CSS [w: " + Math.round(rx * img_width) + ", h: " + Math.round(ry * img_height) + "]");
        	//console.debug("Pimg CSS [left: " + Math.round(rx * c.x) + ", Top: " + Math.round(ry * c.y) + "]");
            $pimg.css({
               width: Math.round(rx * img_width) + 'px',
               height: Math.round(ry * img_height) + 'px',
               marginLeft: '-' + Math.round(rx * c.x) + 'px',
               marginTop: '-' + Math.round(ry * c.y) + 'px'
            });
            $pimg = $('#preview-pane-' + componentId + ' .preview-container').css({
                width: Math.round(previewW) + 'px',
                height: Math.round(previewH) + 'px'
            });
            
            $('#x-' + componentId).val((c.x < 0)? 0:c.x);
            $('#y-' + componentId).val((c.y < 0)? 0:c.y);
            $('#w-' + componentId).val(finalWidth);
            $('#h-' + componentId).val(finalHeight);
            
            //Show cropped image at the preview example if must
            if(demoId !== "" && demoWidth != 0 && demoHeight != 0){
            	
                rx = demoWidth / finalWidth;
                ry = demoHeight / finalHeight;
                $pimg = $('#' + demoId);
                $pimg.css({
                    width: Math.round(rx * img_width) + 'px',
                    height: Math.round(ry * img_height) + 'px',
                	display: 	'inline',
                    marginLeft: '-' + Math.round(rx * c.x) + 'px',
                    marginTop: '-' + Math.round(ry * c.y) + 'px'
                 });
            }
            
            //Show cropped image at the preview example if must
            if(demoId2 !== "" && demoWidth2 != 0 && demoHeight2 != 0){            	
                rx = demoWidth2 / finalWidth;
                ry = demoHeight2 / finalHeight;
                $pimg2 = $('#' + demoId2);
                $pimg2.css({
                    width: Math.round(rx * img_width) + 'px',
                    height: Math.round(ry * img_height) + 'px',
                	display: 	'inline',
                    marginLeft: '-' + Math.round(rx * c.x) + 'px',
                    marginTop: '-' + Math.round(ry * c.y) + 'px'
                 });
            }
            
            //Show cropped image at the preview example if must
            if(demoId3 !== "" && demoWidth3 != 0 && demoHeight3 != 0){            	
                rx = demoWidth3 / finalWidth;
                ry = demoHeight3 / finalHeight;
                $pimg2 = $('#' + demoId3);
                $pimg2.css({
                    width: Math.round(rx * img_width) + 'px',
                    height: Math.round(ry * img_height) + 'px',
                	display: 	'inline',
                    marginLeft: '-' + Math.round(rx * c.x) + 'px',
                    marginTop: '-' + Math.round(ry * c.y) + 'px'
                 });
            }
         }
         //Do not remove until confirme that the preview is working fine
         /*$pimg = $('#preview-pane-' + componentId + ' .preview-container img');
         if (parseInt(c.w) > 0) {            
            $pimg.css({
              width:        Math.round( (Exxo.UI.vars['jcrop_api-' + componentId + "-boundx"] / c.w) * 150) + "px",
              height:       Math.round( (Exxo.UI.vars['jcrop_api-' + componentId + "-boundy"] / c.h) * 150) + "px",
              marginLeft:   '-' + Math.round( ((Exxo.UI.vars['jcrop_api-' + componentId + "-boundx"] / c.w) * 150) / (Exxo.UI.vars['jcrop_api-' + componentId + "-boundx"] / c.x) ) + 'px',
              marginTop:    '-' + Math.round( ((Exxo.UI.vars['jcrop_api-' + componentId + "-boundy"] / c.h) * 150) / (Exxo.UI.vars['jcrop_api-' + componentId + "-boundy"] / c.y) ) + 'px'
            });
            console.debug("Coords -> x: " + c.x + ", y: " + c.y + ", w: " + c.w + ", h: " + c.h);
            $('#x-' + componentId).val((c.x < 0)? 0:c.x);
            $('#y-' + componentId).val((c.y < 0)? 0:c.y);
            $('#w-' + componentId).val(c.w);
            $('#h-' + componentId).val(c.h);
         }*/
      },
      initCrop: function(file, image, minWidth, minHeight, maxWidth, maxHeight){
         if(file.attr('cropImage') !== "true") return; //Only execute when must crop the image
         //Get the reference of the component
         var ref = (file.attr('ref') != null && file.attr('ref') != "undefined")? file.attr('ref'):"";
         var componentId = "" + file.attr('id');
         
         if(minWidth == null && minWidth !== 'undefined'){
            minWidth = 5;
         }
         if(minHeight == null && minHeight !== 'undefined'){
            minHeight = 5;
         }
         if(maxWidth == null && maxWidth !== 'undefined'){
            maxWidth = image.width;
         }
         if(maxHeight == null && maxHeight !== 'undefined'){
            maxHeight = image.height;
         }
                  
       //Refactor minWidth and minHeight if required
         var maxTargetWidth = 300;
         if(image.width < 300){
        	 maxTargetWidth = image.width; 
         }
         /*if(minWidth > 300){
            minHeight = minHeight * maxTargetWidth / minWidth;
            minWidth = maxTargetWidth;
         }*/
         
         var timg = $('<img id="target' + ref + '">'); //Equivalent: $(document.createElement('img'))
         timg.css({
            width:          maxTargetWidth + 'px',
            height:         'auto',
            padding:        '5px',
            border:         '1px #bbb solid'
         });
         timg.appendTo('#targetImgDiv'+ref);
         
         var targetImage = document.getElementById('target' + ref);
         var oImagePreview = document.getElementById('preview' + ref);
         
         /**/
         oImagePreview.src = targetImage.src=image.src;
                        
         oImagePreview.onload = function () {
            //console.debug("preview w: " + (image.width *  150 / minWidth)  +'px, h: ' + (image.height * 150 / minHeight)  +'px');
            //$('#preview' + ref).css("width",  (image.width *  150 ) / minWidth +'px');
            //$('#preview' + ref).css("height", (image.height * 150 ) / minHeight  +'px');            
         };
         
         //Check if the component is associated with a demo component
         var demoId = file.attr("demoId");
         var demoWidth = 0;
         var demoHeight = 0;
         if(demoId !== ""){
        	 var imageDemoExample = document.getElementById(demoId);
        	 demoWidth = file.attr("demoWidth");
             demoHeight = file.attr("demoHeight");
        	 if(file.attr("update1") != undefined){
        		 if(file.attr("update1") == 0){
        			 // There is a demo reference but for any reason must not update the demo
        			 // In this case should also clean the demo        			 
                     imageDemoExample.src = "";
        		 } else {
                     imageDemoExample.src = image.src;                     
        		 }
        	 } else { //Do normal stuff
                 imageDemoExample.src = image.src;
        	 }
         }
         
         var demoId2 = file.attr("d2id");
         var demoWidth2 = 0;
         var demoHeight2 = 0;
         if(demoId2 != undefined && demoId2 !== ""){
        	 var imageDemoExample2 = document.getElementById(demoId2);
        	 demoWidth2 = file.attr("d2w");
             demoHeight2 = file.attr("d2h");
        	 if(file.attr("update2") != undefined){
        		 if(file.attr("update2") == 0){
        			 // There is a demo reference but for any reason must not update the demo
        			 // In this case should also clean the demo        			 
                     imageDemoExample2.src = "";
        		 } else {
                     imageDemoExample2.src = image.src;                     
        		 }
        	 } else { //Do normal stuff
                 imageDemoExample2.src = image.src;
        	 }
         }
         
         var demoId3 = file.attr("d3id");
         var demoWidth3 = 0;
         var demoHeight3 = 0;
         console.debug("demoId3: " + demoId3);
         if(demoId3 != undefined && demoId3 !== ""){
        	 var imageDemoExample3 = document.getElementById(demoId3);
        	 demoWidth3 = file.attr("d3w");
             demoHeight3 = file.attr("d3h");
        	 if(file.attr("update3") != undefined){
        		 if(file.attr("update3") == 0){
        			 // There is a demo reference but for any reason must not update the demo
        			 // In this case should also clean the demo        			 
                     imageDemoExample3.src = "";
        		 } else {
                     imageDemoExample3.src = image.src;                     
        		 }
        	 } else { //Do normal stuff
                 imageDemoExample3.src = image.src;
        	 }
         }
         
         targetImage.onload = function () { // onload event handler
    
            $('#demo' + ref).show();
            $('#preview-pane-' + componentId).show();
                                            
            // destroy Jcrop if it is existed
            if (Exxo.UI.vars['jcrop_api-'+componentId] != null && typeof Exxo.UI.vars['jcrop_api-'+componentId] != 'undefined') {
               Exxo.UI.vars['jcrop_api-'+componentId].destroy();
               Exxo.UI.vars['jcrop_api-'+componentId] = null;                    
            }
            
            //$('#target' + ref).css("width", oImagePreview.width +'px');
            //$('#target' + ref).css("height",oImagePreview.height +'px');
     
            //console.debug("MinWidth: " + minWidth + ", minHeight: " + minHeight + ", w: " + image.width + ", h: " + image.height);
            //initialize Jcrop
            $('#target' + ref).Jcrop({
               //boxWidth:    minWidth,
               //boxHeight:   minHeight,
               bgColor:     'white',
               minSize:     [minWidth, minHeight],
               //minSize:     [100, 100],
               maxSize:     [minWidth, minHeight],
               trueSize:    [image.width, image.height],
               onChange:    function(coords){Exxo.Images.updatePreview(coords, componentId, demoId, demoWidth, demoHeight, 
            		   								demoId2, demoWidth2, demoHeight2,
            		   								demoId3, demoWidth3, demoHeight3);},
               onSelect:    function(coords){Exxo.Images.updatePreview(coords, componentId, demoId, demoWidth, demoHeight, 
            		   								demoId2, demoWidth2, demoHeight2,
            		   								demoId3, demoWidth3, demoHeight3);},
               setSelect:   [0, 0, minWidth, minHeight],
               bgOpacity:   .4,
               allowResize: false,
               allowSelect:	false
               //aspectRatio: maxWidth / maxHeight
            }, function(){        
               // use the Jcrop API to get the real image size
               var bounds = this.getBounds();
               Exxo.UI.vars['jcrop_api-' + componentId + "-boundx"] = bounds[0];
               Exxo.UI.vars['jcrop_api-' + componentId + "-boundy"] = bounds[1];
               Exxo.UI.vars['jcrop_api-' + componentId + "-w"] = minWidth;
               Exxo.UI.vars['jcrop_api-' + componentId + "-h"] = minHeight;
               // Store the Jcrop API in the jcrop_api variable
               Exxo.UI.vars['jcrop_api-'+componentId] = this;
               
               this.setSelect([ 0, 0, minWidth, minHeight ]);
               
               // Move the preview into the jcrop container for css positioning
               $('#preview-pane-' + componentId).appendTo(Exxo.UI.vars['jcrop_api-'+componentId].ui.holder);
            });                                    
         };
      }
};