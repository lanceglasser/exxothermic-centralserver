/***
 @file      exxo_widget_welcome_ad.js
 @brief     Functions for the Welcome Ad List Widget
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.WidgetsWelcomeAd = {
	initWelcomeAdsListWidget : function() {

		$("#welcomeList-widget").exxoWidget({
			helpMsg : Exxo.UI.translates['widget-wa-list-info-msg'],
			referenceIcon : 'list',
			referenceUrl : Exxo.UI.urls['welcome-ad-list']
		});

		Exxo.UI.vars["widgetWelcomeAdsTable"] = $('#widgetWelcomeListTable')
				.dataTable(
						{

							"pagingType" : "full", // More info at: https://datatables.net/examples/basic_init/alt_pagination.html
							"lengthMenu" : [ [ 5 ], [ 5 ] ], // More info at: https://datatables.net/examples/advanced_init/length_menu.html
							"processing" : true,
							"serverSide" : true,
							"ajax" : {
								"url" : Exxo.UI.urls["widget-get-data-url"],
								"data" : function(d) {
									d.object = 'welcomeAd';
									d.companyId = Exxo.UI.vars["company-id"];
									d.locationId = Exxo.UI.vars["location-id"];
								}
							},
							"columnDefs" : [
									{
										"targets" : [ 0 ],
										"render" : function(data, type, row) {
											return '<a href="'
													+ Exxo.UI.urls["wa-edit-url"]
													+ row[3] + '">' + row[0]
													+ '</a>';
										}
									},
									{
										"targets" : [ 2 ],
										"render" : function(data, type, row) {
											if (row[1] == "VIDEO") {
												if (row[4] == "" || 
													row[4] == "null" ||
													row[4] == undefined) {
													return "-";
												} else {
													return '<a href="'
														+ row[4]
														+ '" target="_blank">'
														+ Exxo.UI.translates['download']
														+ '</a>';
												}												
											} else {
												if (row[2] == "" || 
													row[2] == "null" ||
													row[2] == undefined) {
													return "-";
												} else {
													return '<img src="'
													+ row[2]
													+ '" width="35px" height="35px"/>';
												}
											}											
										}
									},
									{
										"targets" : [ 1 ],
										"render" : function(data, type, row) {
											if (row[1] == "VIDEO") {
												return Exxo.UI.translates['widget-welcome-ad-type-video'];
											} else {
												return Exxo.UI.translates['widget-welcome-ad-type-image'];
											}
										}
									} ]
						});
	}
};