var Exxo = Exxo || {};

Exxo.Wizzard = {
      initComponents: function(e){  
         
         $.ajaxSetup({
                statusCode: {
                  401: function () {
                      window.location = baseURL + '/login';
                  }
                }
             });             

         $("#wizardForm").submit(function(e){
             e.preventDefault();
             Exxo.showSpinner();
              $.ajax({type:'POST',
                    data:'_eventId_continue=1&'+$("#wizardForm").serialize(),                                  
                    url: baseURL + '/wizard/pages', 
                    success:function(data) {                            
                        $("#wizardPage").html(data);
                        Exxo.hideSpinner();
                      },
                    error:function(XMLHttpRequest,textStatus,errorThrown){                              
                         alert(errorThrown);
                         Exxo.hideSpinner();
                         null;
                  }
               });
              
              return false;
            });
                                 
         }
}