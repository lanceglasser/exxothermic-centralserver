/***
 @file      exxo_main.js
 @brief     Handles the load of required js fields by module and includes general functions.
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 @note      The Exxo.module variable must be set for each page and the init functions execute corresponding init.
 @todo      Add modal messages, alert, confirm, others.
 */

if (typeof console == "undefined" || typeof console.log == "undefined" || typeof console.debug == "undefined") var console = { log: function() {}, debug: function() {} };
if (typeof jQuery !== 'undefined') {
	console.debug("JQuery found!!!");
	(function($) {
		$('#spinner').ajaxStart(function() {
			$(this).fadeIn();
		}).ajaxStop(function() {
			$(this).fadeOut();
		});
	})(jQuery);
} else {
	console.debug("JQuery not found!!!");
}
$(document).ready(function() {
	Exxo.init();
});

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

var Exxo = {
		module : "",
		imagesURL : "",
        assetsURL : "",
		isIe: false,
		session : {},
		logout:function(url){
			location.href= url;
		},
		init : function() {
			Exxo.UI.vars['is.loading.page'] = true;
			var module = Exxo.module;
            console.debug("Exxo.module: " + Exxo.module);
            
            $("label, .questionToolTip").click(function(e){

            	e.preventDefault();
                return false;
             });
            
        	/****accordion menu****/
            $(".goo-collapsible > li > a").on("click", function(e){
                
            	if(!$(this).hasClass("active")) {
            		
                  // hide any open menus and remove all other classes
            		$(".goo-collapsible li ul").slideUp(350);
            		$(".goo-collapsible li a").removeClass("active");
                  
            		// open our new menu and add the open class
            		$(this).next("ul").slideDown(350);
            		$(this).addClass("active");
            		
            	}else if($(this).hasClass("active")) {
            		
            		$(this).removeClass("active");
            		$(this).next("ul").slideUp(350);
            	}
            });
        	/*****************end of accordion menu***************/
            
            Exxo.UI.init();
			if (module) {
			    if(module !== "login" && module !== "register"){
			       Exxo.Dashboard.init();
			    }
				switch (module) {
                case "login":
                    Exxo.Login.init(); break;
                case "register":
                    Exxo.Register.init(); break;
                case "index":
                   Exxo.UserDashboard.init();
                   break;
                case "employee-list":
                    Exxo.Tables.init();
                    Exxo.Employee.init();
                    break;
                case "employee":                    
                    Exxo.EmployeeForm.init();
                    break;
                case "page-list":
                    Exxo.Tables.init();
                    break;
                case "employee-asign":
                    Exxo.Employee.Asign.init();
                    break;
                case "company-asign-integrator":
                    Exxo.Company.AsignIntegrator.init();
                    break;
                case "company-create":
                    Exxo.Company.Create.init();
                    break;
                case "integrator-create":
                    Exxo.Integrator.Create.init();
                    break;
                case "box-manufacturer-edit":
                    Exxo.BoxManufacturer.Edit.init();
                    break;
                case "location-list":
                    Exxo.Tables.init();
                    break;   
                case "location-show":
                    Exxo.Location.Show.init();
                    Exxo.Metrics.Charts.init();
                    break;
                case "location-create":
                    Exxo.Location.Create.init();
                    break;   
                case "box-list":
                    Exxo.Tables.init();
                    Exxo.Box.List.init();
                    break;
                case "box-list-server":
                    Exxo.Box.List.init();
                    break;
                case "box-block":
                    Exxo.Box.Block.init();
                    break; 
                case "content-list":                	
                	Exxo.Content.init();
                    Exxo.Tables.init();
                    break;
                case "box-register":
                    Exxo.Box.Register.init();
                    break;
                case "box-edit":
                    Exxo.Box.Edit.init();
                    break;
                case "box-unregister":
                    Exxo.Box.Unregister.init();
                    break;
                case "button-assign-location":
                    Exxo.Button.Assign.init();
                    break;
                case "custom-button-create":
                    Exxo.Button.Create.init();
                    break;
                case "custom-button-edit":
                    Exxo.Button.Create.initForEdit();
                    break;
                case "box-software-update":
                    Exxo.Box.Software.Update.init();
                    break;
                case "user-create":
                    Exxo.User.Create.init();
                    break;
                case "document-create":
                	Exxo.Document.Create.init();
                    break;
                case "app-skin-list":
                    Exxo.Tables.init();
                    Exxo.AppSkin.init();
                    break;
                case "skin-create":
                    Exxo.Skin.Create.initCreate();
                    Exxo.Tables.init();
                    break;
                case "skin-associate":
                    Exxo.Skin.Create.init();
                    Exxo.Tables.init();
                    break;
                case "exxtractors-users":
                    Exxo.ExxtractorsUsers.init();
                    break;
                case "welcome-ad":
                    Exxo.WelcomeAd.Create.init();
                    break;
                case "affiliate-establishments":
                    Exxo.Affiliates.Establishments.init();
                    break;
                case "affiliate-create":
                    Exxo.Affiliate.Create.init();
                    break;
			    case "affiliate-list":
				    Exxo.Tables.init();
				    Exxo.Affiliate.init();
				    break;
                case "affiliate-edit":
                    Exxo.Tables.init();
                    break;
                case "exxtractor-user-create":
                	Exxo.ExxtractorsUsers.createInit();
                	break;
                case "exxtractor-user-edit":
                	Exxo.ExxtractorsUsers.editInit();
                	break;
                case "users-list":
                    Exxo.Tables.init();
                    break;
                case "box-metrics-report":
                	Exxo.Metrics.Report.init();
                	break;
                case "box-metrics-tables":
                	Exxo.Metrics.Tables.init();
                	break;
                case "box-metrics-charts":
                	Exxo.Metrics.Charts.init();
                	Exxo.Metrics.Charts.extraInit();
                	break;
                case "software-upgrade-job":
                	Exxo.Software.Job.init();
                	break;
                case "box-upgrade-job-list":
                	Exxo.Software.Job.list();
                	break;
                case "box-upgrade-job-show":
                	Exxo.Software.Job.show();
                	break;
                case "wa-assign":
                	Exxo.Assign.Location.init();
                	break;
                case "configuration-edit":
                	Exxo.Configurations.Edit.init();
                	break;
                case "devices-list": Exxo.Tables.init(); Exxo.Content.initConfirmDeleteOptions(); break;
                case "change-company-owner": Exxo.Tables.init(); Exxo.Company.Owners.init(); break;
                case "content-show":
                	Exxo.Button.Create.displayScheduleInfoFromShow();
                	break;
                default:
					break;
				}
			}
			Exxo.UI.vars['is.loading.page'] = false;
		},
        simplePost : function(url, values, successFunction) {
            Exxo.post(url, values, successFunction, function(x, t, m) {
                //ToDo: Add error function for this request
            }, false);
        },
        post : function(url, values, successFunction, errorFunction, showSpinner) {
            var request = $.ajax({
                url: url,
                type: "POST",
                data: values,
                dataType: "json",
                timeout: 300000
            });

            request.done(successFunction);

            request.fail(errorFunction);
        },
        ajaxCall : function(url, params, succedFunction, errorFunction, showSpinner) {
            var request = $.ajax({
                url: url,
                type: "POST",
                data: params,
                dataType: "json"
            });

            request.done(succedFunction);

            request.fail(errorFunction);
        },
        showInfoMessage : function(message, showAlert, divId, showDiv) {
            if ( showAlert ) {
            	new Messi(message, {title: 'Info', modal: true, titleClass: 'info', buttons: [{id: 0, label: Exxo.UI.translates['closeButton'], val: 'X'}]});
            }
            if ( showDiv ) {
                $("#" + divId).html(message);
            }
        },
        showErrorMessage : function(message, showAlert, divId, showDiv) {
            if ( showAlert ) {
            	new Messi(message, {title: 'Error', modal: true, titleClass: 'error', buttons: [{id: 0, label: Exxo.UI.translates['closeButton'], val: 'X'}]});
            }
            if ( showDiv ) {
                $("#" + divId).html(message);
            }
        },
        showConfirmationQuestion : function(message) {
            return confirm(message);
        },
        showSpinner : function() {
           $('div#ajaxFlowWait').show(40);
           //document.getElementById('errorDiv').style.display = 'none';
        },
        hideSpinner : function() {
           $('div#ajaxFlowWait').hide();
           //document.getElementById('errorDiv').style.display = 'none';
        },
        isValidPassword : function(obj){
           var str = obj.value;
           if(str.indexOf('#') > -1 || str.indexOf('$') > -1 || str.indexOf('%') > -1 || str.indexOf('_') > -1 || str.indexOf('@') > -1 ||
              str.indexOf('!') > -1 || str.indexOf('?') > -1 || str.indexOf('-') > -1) {
               if(str.length < 6 || str.length > 50) {
                   return false;
               }
               re = /[0-9]/;
               if(!re.test(str)) {
                  return false;
               }
               re = /[A-Za-z]/;
               if(!re.test(str)) {
                 return false;
               }
               return true;
           } else {
               return false;
           }
        },
        openHelpWindow: function(tag, template){
        	//console.debug("OpenHelpWindow: " + tag);
        	//tag = "widgets";
        	//template = "widgets";
        	$.ajax({
                url: Exxo.UI.urls['baseURL'] + '/home/showHelp?main=true&cat=' + tag + "&template="+template,
                success: function(data){
              	  	Exxo.hideSpinner();
                    $("#helpDialogPlaceHolder").html(data);
                    Exxo.HelpCenter.init();
                    $("#helpDialogPlaceHolder").dialog("open");
                }
            });
        },
		UI : {
			translates : {},
			urls : {},
            vars : {},
			intervals : {},
			isSafari: function(){
				return (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1);
			},
			isIE: function(){				
				var ua = window.navigator.userAgent;
		        var msie = ua.indexOf("MSIE ");

		        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer, return version number
		            return true;
		        else
		            return false;
		        
			},
			init : function() {
				/**/
				$(".goo-collapsible u, .goo-collapsible li, .goo-collapsible a, .goo-collapsible li a").click(function(e){
					console.debug("Event click: " + e.which);
					if (e.which != 1) { //Other than left click
						e.preventDefault();
						e.stopPropagatioin();
						return false;
					}
				});
				/*$(".back-button").click(function(e){
					if (document.referrer != "") {			
						var backResult = window.history.back();
						if(backResult != undefined){
							e.preventDefault();
							e.stopPropagation();
						}
					}
				});*/
   			    /*$('.questionToolTip').tooltipster({
                     position : 'right',
                     maxWidth : 210
                 });*/
				
				//Set default Values for Tables
				if(Exxo.UI.vars["rowsPerPage"] === undefined){
					Exxo.UI.vars["rowsPerPage"] = 10;
					Exxo.UI.vars["firstRecord"] = 0;
					Exxo.UI.vars["tableFilter"] = "";
					Exxo.UI.vars["columnOrder"] = 0;
					Exxo.UI.vars["orderDir"] 	= "asc";
				}
				
				Exxo.UI.vars["defaultTableRowsPerPage"] = 10;
				Exxo.UI.initLocales();
				
			},
			initLocales: function(){
				//Fix the color input selectors styles for IE and Safari
				if(Exxo.UI.isIE()){
					$(".color .add-on-fix").css("top", "7px");
				} else if(Exxo.UI.isSafari()){
					$(".color .add-on-fix").css("top", "7px");
				}
				
				$("a.close").click(function(e){
					e.preventDefault();
					$(this).parent().hide();
				});
			    if(Exxo.module !== "login"){
			    
			    	// Init the Dialog Window for the Help Center
			    	if($("#helpDialogPlaceHolder") !== undefined){
						$("#helpDialogPlaceHolder").dialog({
			                autoOpen: false,                      
			                width: 1030,
			                height: 665,
			                modal: true,
			                resizable: true,    
			                animate: true,
			                autoReposition: true,               
			                close: function(){
			                    $("#helpDialogPlaceHolder").html('');
			                }
			            });
					}
			    	
			    	// Init the Dialog Window for the Help Center
			    	if($("#addWidgetDialogPlaceHolder") !== undefined){
						$("#addWidgetDialogPlaceHolder").dialog({
			                autoOpen: false,                      
			                width: 800,
			                height: 600,
			                modal: true,
			                resizable: true,    
			                animate: true,
			                autoReposition: true,               
			                close: function(){
			                    $("#addWidgetDialogPlaceHolder").html('');
			                }
			            });
					}
			    	
			       //Init the tabs in the page
			       $('.tab-section').hide();
			       $('#tabs a').bind('click', function(e){
			           $('#tabs a.current').removeClass('current');
			           $('.tab-section:visible').hide();
			           $(this.hash).show();
			           $(this).addClass('current');
			           e.preventDefault();
			       }).filter(':first').click();
			       
			       $("#password").change(function(event){
			            event.target.setCustomValidity((Exxo.isValidPassword(this)===false) ? Exxo.UI.vars['validationDetail'] : '');
	                    $("#confirmPassword").trigger("change");
	                });
			    }
                $("#newPassword").change(function(event){
                    event.target.setCustomValidity((Exxo.isValidPassword(this)===false) ? Exxo.UI.vars['validationDetail'] : '');
                    $("#confirmPassword").trigger("change");

                });

                $("#confirmPassword").change(function(event){
                    var pass = "";

                    if (document.getElementById("password")){
                        pass=document.getElementById("password").value;
                    }

                    if (document.getElementById("newPassword")){
                        pass=document.getElementById("newPassword").value;
                    }

                    if(event.target.value!=pass){
                        event.target.setCustomValidity(Exxo.UI.vars['confirmationFails']);
                    }else{
                        event.target.setCustomValidity('');
                    }
                });

			    Exxo.UI.setOnlyNumbersEvent();
		        
			},
			isNumeric: function(str){
			    return /^\d+$/.test(str);
			},
			setOnlyNumbersEvent: function(){
			   $(".onlyNumbers").unbind();
			   //Remove not number characters from the value if exists			   
			   if(Exxo.UI.vars['is.loading.page'] == undefined || !Exxo.UI.vars['is.loading.page'] ){
				   $( ".onlyNumbers" ).each(function( index ) {
				      var value = $(this).val();
				      var newValue = "";
				      for (var i = 0, len = value.length; i < len; i++) {
				         if(Exxo.UI.isNumeric(value[i])){
				            newValue += value[i];
				         }
				       }
				      $(this).val(newValue);
				   });
			   }			   
			   
			   $(".onlyNumbers").keypress(function(event) {
                  // Backspace, tab, enter, end, home, left, right
                  // We don't support the del key in Opera because del == . == 46.
                  var controlKeys = [8, 9, 13, 35, 36, 37, 39];
                  // IE doesn't support indexOf
                  var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
                  // Some browsers just don't raise events for control keys. Easy.
                  // e.g. Safari backspace.
                  if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
                      (48 <= event.which && event.which <= 57) || // Always 1 through 9
                      //(48 == event.which && $(this).attr("value")) || // No 0 first digit
                      isControlKey) { // Opera assigns values for control keys.
                    return;
                  } else {
                    event.preventDefault();
                  }
                });
			   $('.onlyNumbers').bind('paste', function () {
			   		var self = $(this);
			   		var orig = self.val();
			   		setTimeout(function () {
			   			var removedText = self.val().replace(/\D+/g, '');
				        self.val(removedText)
				    });
				});
			   
			   $("#lowercaseOnly").keyup(function(e){
				   this.value = this.value.toLowerCase();
			   });
			},
			setInputsEventsByCountry: function(country){
			   country = country * 1;
			   var canada = 2;
			   var unitedStates = 1;
			   var australia = 18;
			   
			   switch(country){
			      case canada:
			         $(".onlyNumbers").unbind();
			         Exxo.UI.setMinimumZipCode(5);
			         break;
			      case australia:
			    	 Exxo.UI.setOnlyNumbersEvent();
			    	 Exxo.UI.setMinimumZipCode(4);
			    	 break; 
			      default:
			         Exxo.UI.setOnlyNumbersEvent();
			      	 Exxo.UI.setMinimumZipCode(5);
			         break;
			   }
			},
			setMinimumZipCode: function(min){
				$("#zipCode").attr("pattern", ".{" + min + ",9}");
				var title = Exxo.UI.translates['zipcode-title-msg'];
				title = title.replace("X1", min);
				title = title.replace('X2', "9");
				$("#zipCode").attr("title", title);
			}
		}
	};
