/***
 @file      exxo_box_register_software.js
 @brief     Custom functions and events for the Box Registration page.
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Box = {
    Register : {
       init : function() {
    	   $('.list-group.checked-list-box .list-group-item').each(function () {
    	        
    	        // Settings
    	        var $widget = $(this),
    	            $checkbox = $('<input type="checkbox" class="hidden" />'),
    	            color = ($widget.data('color') ? $widget.data('color') : "primary"),
    	            style = ($widget.data('style') == "button" ? "btn-" : "list-group-item-"),
    	            settings = {
    	                on: {
    	                    icon: 'glyphicon glyphicon-check'
    	                },
    	                off: {
    	                    icon: 'glyphicon glyphicon-unchecked'
    	                }
    	            };
    	            
    	        $widget.css('cursor', 'pointer')
    	        $widget.append($checkbox);

    	        // Event Handlers
    	        $widget.on('click', function () {
    	            $checkbox.prop('checked', !$checkbox.is(':checked'));
    	            $checkbox.triggerHandler('change');
    	            updateDisplay();
    	        });
    	        $checkbox.on('change', function () {
    	            updateDisplay();
    	        });
    	          

    	        // Actions
    	        function updateDisplay() {
    	            var isChecked = $checkbox.is(':checked');

    	            // Set the button's state
    	            $widget.data('state', (isChecked) ? "on" : "off");

    	            // Set the button's icon
    	            $widget.find('.state-icon')
    	                .removeClass()
    	                .addClass('state-icon ' + settings[$widget.data('state')].icon);

    	            // Update the button's color
    	            if (isChecked) {
    	                $widget.addClass(style + color + ' active');
    	            } else {
    	                $widget.removeClass(style + color + ' active');
    	            }
    	        }

    	        // Initialization
    	        function init() {
    	            
    	            if ($widget.data('checked') == true) {
    	                $checkbox.prop('checked', !$checkbox.is(':checked'));
    	            }
    	            
    	            updateDisplay();

    	            // Inject the icon if applicable
    	            if ($widget.find('.state-icon').length == 0) {
    	                $widget.prepend('<span class="state-icon ' + settings[$widget.data('state')].icon + '"></span>');
    	            }
    	        }
    	        init();
    	    });
    	    
    	    $('#get-checked-data').on('click', function(event) {
    	        event.preventDefault(); 
    	        var checkedItems = {}, counter = 0;
    	        $("#check-list-box li.active").each(function(idx, li) {
    	            checkedItems[counter] = $(li).text();
    	            counter++;
    	        });
    	        $('#display-json').html(JSON.stringify(checkedItems, null, '\t'));
    	    });
    	    
    	   /**/
    	   $("#imageType").select2();
    	   $("#imageType").change(function(e){
    	      console.debug("The value is: " + $(this).val());
    	   });
    	   $("#affiliate").select2();
    	   
    	   $("#file").change(function(e){
               //var result = Exxo.Utils.Files.validateVideoFile($(this));
               if(Exxo.Utils.Files.validateSoftwareVersionFile($(this))){//valid
                  //$('#fakeupload').val($(this).val().replace('C:\\fakepath\\', ''));
             	  $("#file-updated").val(1);
             	  var ref = ($(this).attr('ref') != null && $(this).attr('ref') != "undefined")? $(this).attr('ref'):"";
                  $('#fakeupload' + ref).val($(this).val().split(/(\\|\/)/g).pop());
               } else {
             	 $("#file-updated").val(0);
                  e.preventDefault();
                  return false;
               }
            });
           
           $('#fakeupload').val(Exxo.UI.vars['filename']);
           
           $("#editSoftwareUpdateForm, #uploadSoftwareUpdateForm").submit(function(e){
        	   var architectures = "";
        	   $('.architecture.active').each(function(i, obj) {
        		   architectures += (architectures === "")? "["+$(obj).val()+"]":",[" + $(obj).val()+"]";
        	   });
        	   
        	   if(architectures === ""){
        		   //Must select at least 1 architecture
        		   Exxo.showErrorMessage(Exxo.UI.translates['architectures.min.error'], true, "", false);
        		   e.preventDefault();
            	   e.stopPropagation();
            	   return false;
        	   } else {
        		   $("#associatedArchitectures").val(architectures);
        		   var labelValue = $("#versionSoftwareLabel").val();
        		   if (!Exxo.Validation.isValidVersionLabel(labelValue)) {
        			   $('#versionSoftwareLabel').get(0).setCustomValidity(Exxo.UI.translates['invalid.version.label']);
        			   e.preventDefault();
                	   e.stopPropagation();
                	   return false;
        		   }
        	   }
           });
       }
    }
};






