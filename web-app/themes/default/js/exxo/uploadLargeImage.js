(function($){
	  $(document).ready(function()  {
	     Exxo.Images.init();
	    
	    $('#myUpload').submit(function(e){	    	
			e.preventDefault();
			//hideSpinner();
			var formData = new FormData($(this)[0]);
			console.debug("-> " + $("#file-updated").val() + " <--> " + ($("#file-updated").val() == 0));
			if($("#file-updated").val() == 0){
				//There was an error
                var errorDiv = document.getElementById('errorDiv');
                hideSpinner();
                errorDiv.innerHTML = '<ul><li>' + Exxo.UI.translates['select-image-before-save'] + '</li></ul>';
                errorDiv.style.display = 'block';
			} else {
				 showSpinner();
				 $.ajax({type:'POST',
		            data: formData ,
		            url: Exxo.UI.urls['baseURL'] + '/box/uploadFile?type=large', 
		            async: true,
		            cache: false,
		            contentType: false,
		            processData: false,
		            success: function(data) {
		               hideSpinner();
		               if(data.error){
		                  //There was an error
		                  var errorDiv = document.getElementById('errorDiv');
		                  hideSpinner();
		                  errorDiv.innerHTML = '<ul><li>' + data.textStatus + '</li></ul>';
		                  errorDiv.style.display = 'block';
		               } else {//There's no error
		                  var channelNumber = data.channelNumber;                  
		                    $('#largeImageDemo_' + channelNumber).show();   
		                    
		                    $("tr#"+channelNumber+ " input[name='largeImageURL']").val(data.largeImageURL);
		                    
		                    var oImage = document.getElementById('largeImageTarget_'+channelNumber);
		                    oImage.src = data.largeImageURL;
		                    
		                    $('#largeImageTarget_'+channelNumber).show();
		                    
		                    $("#dialogPlaceholder").dialog("close");
		               }
		            	
		            },
		            error: function(xhr, textStatus, error){
		               console.debug("->TextStatus: " + textStatus + ", error: " + error);
		               //whatever you need to do here - output errors etc.
		              var errorDiv = document.getElementById('errorDiv');
		              hideSpinner();
		              errorDiv.innerHTML = '<ul><li>' + textStatus + '</li></ul>';
		          	  errorDiv.style.display = 'block';
		                           
		            }
				 });
			}
			
	    });
	    	
	  });
})(jQuery);

function showSpinner(){
   $('#ajaxFlowWaitImg').show(40);
   Exxo.showSpinner();
}
function hideSpinner(){
   $('#ajaxFlowWaitImg').hide();
   Exxo.hideSpinner();
}