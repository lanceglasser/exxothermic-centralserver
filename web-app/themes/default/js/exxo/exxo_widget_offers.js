/***
 @file      exxo_widget_offers.js
 @brief     Functions for the Offers List Widget
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.WidgetsOffers = {
	initOffersListWidget : function() {

		$("#offers-widget").exxoWidget({
			helpMsg : Exxo.UI.translates['widget-offers-list-info-msg'],
			referenceIcon : 'list',
			referenceUrl : Exxo.UI.urls['offers-list']
		});

		Exxo.UI.vars["widgetOffersTable"] = $('#widgetOffersTable')
				.dataTable(
						{

							"pagingType" : "full", // More info at: https://datatables.net/examples/basic_init/alt_pagination.html
							"lengthMenu" : [ [ 5 ], [ 5 ] ], // More info at: https://datatables.net/examples/advanced_init/length_menu.html
							"processing" : true,
							"serverSide" : true,
							"ajax" : {
								"url" : Exxo.UI.urls["widget-get-data-url"],
								"data" : function(d) {
									d.object = 'offer';
									d.companyId = Exxo.UI.vars["company-id"];
									d.locationId = Exxo.UI.vars["location-id"];
								}
							},
							"columnDefs" : [
									{
										"targets" : [ 0 ],
										"render" : function(data, type, row) {
											if(Exxo.UI.vars["can-edit-offers"]){
												return '<a href="'
												+ Exxo.UI.urls["offers-edit-url"]
												+ row[3] + '">' + row[0]
												+ '</a>';
											} else {
												return row[0];
											}
											
										}
									},
									{
										"targets" : [ 2 ],
										"render" : function(data, type, row) {
											return '<div class="state '
													+ (row[2] ? 'stateGreen'
															: 'stateGray')
													+ '"></div>';
											return data;
										}
									},
									{
										"targets" : [ 1 ],
										"render" : function(data, type, row) {
											if (row[1] == "" || 
												row[1] == "null" ||
												row[1] == undefined) {
												return "-";
											} else {
												return '<img src="'
													+ row[1] + '" width="45px" height="28px"/>';
											}
										}
									} ]
						});
	}
};