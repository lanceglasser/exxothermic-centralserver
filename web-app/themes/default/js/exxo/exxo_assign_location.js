/***
 @file      exxo_assign_location.js
 @brief     Custom functions and events for the association of Locations to a Object
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Assign = {
	Location : {
		firstSelection : true,
		clearScrean : function() {
			if (!Exxo.Assign.Location.firstSelection) {
				$(".alert").hide();
				$("#syncResultContainer").hide();
			}
			Exxo.Assign.Location.firstSelection = false;
			Exxo.showSpinner();
		},
		init : function() {
			Exxo.Tables.init();

			$("#activeSkin").select2();
			$("#activeSkin").trigger('change');
			
			$("#createForm").submit(function(e) {
	            Exxo.showSpinner();
	            var ids = "";
	            $(".cbLocation").each(function(){
	               if ($(this).is(':checked')) {
	                  ids += (ids == "")? $(this).attr("locationId"):","+$(this).attr("locationId"); 
	               }
	            });
	            $("#locationsToSave").val(ids);
	         });

		}
	}
};