/*******************************************************************************
 * @file exxo_box_edit.js
 * @brief Custom functions and events for the Box Edition Page.
 * @author Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Box = {
	Edit : {
		init : function() {
			$(".numberspinner").each(function( index ) {
				var $this = $( this );
				var min = $this.attr("min");
				var max = $this.attr("max");
				var step = $this.attr("step");
				var value = $this.attr("value");
				console.debug(min + ", " + max + ", " + step + ", " + value);
				$this.TouchSpin({
	                min: min,
	                max: max,
	                step: step,
	                decimals: 0,
	                initval: value,
	                boostat: 5,
	                maxboostedstep: 10
	            });
			});
			
			$("#process, #ready, #error").hide();

			$("#voipMark,#voipPriority").select2();
			$(".config-select-input").select2();

			$("#pa").change(function(e) {
				if ( $(this).is(':checked') ) {
					$("#_pa").val(1);
				} else {
					$("#_pa").val(0);
				}
			});

			$("#dialogPlaceholder").dialog({
				autoOpen : false,
				width : 800,
				height : 600,
				modal : true,
				resizable : false,
				animate : true,
				autoReposition : true,
				close : function() {
					$("#dialogPlaceholder").html('');
				}
			});

			$(".btn-upload-image").click(
					function(e) {
						e.preventDefault();
						var id = $(this).attr('channel-id');
						Exxo.showSpinner();
						$.ajax({
							url : Exxo.UI.urls['baseURL']
									+ '/box/uploadImagePage?id=' + id,
							success : function(data) {
								Exxo.hideSpinner();
								$("#dialogPlaceholder").html(data);
								$("#dialogPlaceholder").dialog("open");
							}
						});
					});

			$(".btn-upload-large-image").click(
					function(e) {
						e.preventDefault();
						var id = $(this).attr('channel-id');
						Exxo.showSpinner();
						$.ajax({
							url : Exxo.UI.urls['baseURL']
									+ '/box/uploadLargeImagePage?id=' + id,
							success : function(data) {
								Exxo.hideSpinner();
								$("#dialogPlaceholder").html(data);
								$("#dialogPlaceholder").dialog("open");
							}
						});
					});

			Exxo.UI.vars['stopEvent'] = true;
			Exxo.UI.vars['target'] = true;
			$(".btn-del-image")
					.click(
							function(e) {
								e.preventDefault();

								if (Exxo.UI.isSafari()) {
									Exxo.UI.vars['target'] = $(this);
								} else {
									Exxo.UI.vars['target'] = e.target;
								}

								if (Exxo.UI.vars['stopEvent']) {
									$("#confirm-delete-dialog").dialog("open");
								} else {

									var id = $(this).attr('channel-id');
									var type = $(this).attr('image-type');
									var url = $(this).attr('href');
									var channelNumber = $(this).attr(
											'channel-number');
									if (type == "small") {
										$(
												"tr#"
														+ channelNumber
														+ " input[name='imageURL']")
												.val("");
									} else {
										$(
												"tr#"
														+ channelNumber
														+ " input[name='largeImageURL']")
												.val("");
									}

									Exxo.showSpinner();

									$
											.ajax({
												type : 'POST',
												data : 'id=' + id + '&t='
														+ type,
												url : url,
												success : function(data) {
													Exxo.hideSpinner();
													Exxo.UI.vars['stopEvent'] = true;
													if (data.error === false) {
														if (type === "large") {
															$(
																	"#largeImageTarget_"
																			+ channelNumber)
																	.hide();
															$(
																	"#largeImageTarget_"
																			+ channelNumber)
																	.attr(
																			"src",
																			"");
														} else {
															$(
																	"#target_"
																			+ channelNumber)
																	.hide();
															$(
																	"#target_"
																			+ channelNumber)
																	.attr(
																			"src",
																			"");
														}
													} else {
														// There was an error
														Exxo
																.showErrorMessage(
																		Exxo.UI.translates['channel-image-remove-error'],
																		true,
																		null,
																		false);
													}
												},
												error : function(
														XMLHttpRequest,
														textStatus, errorThrown) {
													Exxo.hideSpinner();
													Exxo.UI.vars['stopEvent'] = true;
													Exxo
															.showErrorMessage(
																	Exxo.UI.translates['channel-image-remove-error'],
																	true, null,
																	false);
												}
											});
								}
								return false;
							});
			$("#confirm-delete-dialog").dialog({
				height : 180,
				width : 360,
				modal : true,
				autoOpen : false,
				buttons : [ {
					text : Exxo.UI.translates['okButton'],
					click : function() {
						$(this).dialog("close");
						Exxo.UI.vars['stopEvent'] = false;
						Exxo.UI.vars['target'].click();
					}
				}, {
					text : Exxo.UI.translates['cancelButton'],
					click : function() {
						$(this).dialog("close");
					}
				} ]
			});

			$("input[name='paBox']")
					.change(
							function() {
								$.ajaxSetup({
									async : true
								});
								var serial = Exxo.UI.vars['serial-number'];
								var pa = $("input[name='paBox']")
										.is(':checked');
								var url = "../isvalidpa?serial=" + serial
										+ "&pa=" + pa;
								$
										.getJSON(
												url,
												function(data) {
													switch (data.status) {
													case 0:
														break;
													case 1:
														bootbox
																.alert(data.results);
														$(
																'input[type="submit"], a.btn.small.primary input[name="paBox"]')
																.attr(
																		'disabled',
																		true);
														$("a.btn.small.primary")
																.unbind('click');
														$("#channelscontent")
																.hide();
														break;
													case 2:
														$('input[name=paBox]')
																.prop(
																		'checked',
																		true);
														bootbox
																.alert(data.results);

														break;
													case 3:
														$('input[name=paBox]')
																.prop(
																		'checked',
																		true);
														bootbox
																.alert(data.results);
														break;
													case 4:
														bootbox
																.confirm(
																		data.results,
																		function(
																				result) {
																			if (!result) {
																				$(
																						'input[name=paBox]')
																						.prop(
																								'checked',
																								false);
																			}
																		});
														break;
													}
												});

							});

			$("#audioCardGeneration").change(function(e){
				var val = $(this).val();
				if( val == "1") { //First generation; hide jumper configuration
					$("#jumperConfigurationContainer").hide();
					$("#jumperConfiguration").val(0); //Set to None
					$("#jumperConfiguration").change(); //Trigger the event for select2 update
				} else {
					$("#jumperConfigurationContainer").show();
				}
			});
			
			Exxo.UI.vars['PREVIOUS-AUDIO_INPUT_MODE'] = $("#CFG-AUDIO_INPUT_MODE").val();
			
			$("#form-box-config").submit(function(e) {
				if ($("#CFG-AUDIO_INPUT_MODE").val() != Exxo.UI.vars['PREVIOUS-AUDIO_INPUT_MODE']) {
					if(!Exxo.showConfirmationQuestion(Exxo.UI.translates['confirm.audio.input.change'])) {
						e.preventDefault();
						e.stopPropagation();
						return false;
					}
				}
			});

			// $("a.btn.small.primary").click(function(e){
			$("#btnSyncronize").click(
				function(e) {
					e.preventDefault();
					Exxo.showSpinner();
	
					var sucessfulMessage = $("#sucessfulMessage");
					var failMessage = $("#failMessage");
					var serial = Exxo.UI.vars['serial-number'];
					createMessagesContainersForSuccessAndFail(sucessfulMessage.val(), failMessage.val());
	
					$("#process").show();
					var error = new Array();
					var ready = new Array();
					$("#ready, #error").hide();
	
					$.ajaxSetup({
						async : false
					});
	
					var updatingChannelsCounter = 0;
					var totalUpdatedChannels = 0;
					Exxo.UI.vars['errorHeaders'] = [];
					var rowIndex = 0;
					$("input[name='channel']").each(
						function(index, val) {
							var id = $(val).val();
							rowIndex++;
							var name = $("tr#" + id + " input[name='label']").val();
							var oldName = $("tr#" + id + " input[name='oldlabel']").val();
							var pa = $("tr#" + id + " input[name='paChannel']").is(':checked');
							var oldPA = false;
							var imageURL = $("tr#" + id + " input[name='imageURL']").val();
							var oldImageURL = $("tr#" + id + " input[name='oldImageURL']").val();
							var description = $("tr#" + id + " textArea[name='description']").val();
							var oldDescription = $("tr#" + id + " input[name='oldDescription']").val();
							var largeImageURL = $("tr#" + id + " input[name='largeImageURL']").val();
							var oldLargeImageURL = $("tr#" + id + " input[name='oldLargeImageURL']").val();
							var gain = $("tr#" + id + " input[name='gain']").val();
							var oldGain = $("tr#" + id + " input[name='oldGain']").val();
							var enableForApp = $("tr#" + id + " input[name='enableForApp']").is(':checked');
							var oldEnableForApp = Exxo.Box.castStrToBool($("tr#"+ id+ " input[name='oldEnableForApp']").val());
							var delay = $("tr#" + id + " input[name='delay']").val();
							var oldDelay = $("tr#" + id + " input[name='oldDelay']").val();
							var url = $(
									"a[name='urlLinkToSyncronize']")
									.attr("href")
									+ "?serial=" + serial
									+ "&channelNumber=" + id
									+ "&channelLabel=" + name
									+ "&isPA=" + pa
									+ "&imageURL=" + imageURL
									+ "&description=" + description
									+ "&largeImageURL=" + largeImageURL
									+ "&gain=" + gain
									+ "&enableForApp=" + enableForApp
									+ "&delay=" + delay;

							if (name.length == 0) {
								var errorList = Exxo.UI.vars['errorHeaders'][Exxo.UI.translates['emptyChannel']];
								var channelsList = (errorList == undefined) ? "" : errorList + ", ";
								channelsList += "Row " + rowIndex;
								Exxo.UI.vars['errorHeaders'][Exxo.UI.translates['emptyChannel']] = channelsList;
								var errorIndex = $.inArray(Exxo.UI.translates['emptyChannel'], error);
								if (errorIndex < 0) {
									error.push(Exxo.UI.translates['emptyChannel']);
								}
							} else {
								if (!(name == oldName)
										|| !(imageURL == oldImageURL)
										|| !(description == oldDescription)
										|| !(largeImageURL == oldLargeImageURL)
										|| !(gain == oldGain)
										|| !(enableForApp == oldEnableForApp)
										|| !(delay == oldDelay)) {
									updatingChannelsCounter++;
									totalUpdatedChannels++;
									$.getJSON(url, function(data) {})
										.done(
											function(data) {
												if (data.successful) {
													ready.push(name);
													$("tr#"+id+" input[name='oldlabel']").val(name);
													$("tr#"+id+" input[name='oldpa']").val(pa);
													$("tr#"+id+" input[name='oldImageURL']").val(imageURL);
													$("tr#"+id+" input[name='oldDescription']").val(description);
													$("tr#"+id+" input[name='oldLargeImageURL']").val(largeImageURL);
													$("tr#"+id+" input[name='oldGain']").val(gain);
													$("tr#"+id+" input[name='oldEnableForApp']").val(enableForApp);
													$("tr#"+id+" input[name='oldDelay']").val(delay);
												} else {
													if (data.message == null) {
														data.message = Exxo.UI.translates['unknown.error'];
													}
													var errorList = Exxo.UI.vars['errorHeaders'][data.message];
													var channelsList = (errorList == undefined) ? ""
															: Exxo.UI.vars['errorHeaders'][data.message]
																	+ ", ";
													channelsList += name;
													Exxo.UI.vars['errorHeaders'][data.message] = channelsList;
													var errorIndex = $.inArray(data.message,error);
													if (errorIndex < 0) {
														error.push(data.message);
													}
												}
												updatingChannelsCounter--;
											})
											.fail(
												function(jqxhr, textStatus, error) {
													updatingChannelsCounter--;
												});
								}
							}
						});
					if (totalUpdatedChannels == 0
							&& error.length == 0) {
						// Nothing to update
						$("#ready .channels")
								.html(
										Exxo.UI.translates['no-channels-to-update']);
						$("#ready").show();
					} else {
						if (ready.length > 0) {
							var response = "";
							for (var i = 0; i < ready.length; i++) {
								response += (response == "") ? ready[i]
										: ", " + ready[i];
							}
							$("#ready .channels").html("[" + response + "]");
							$("#ready").show();
						}
	
						if (error.length > 0) {
							var response = "<ul>";
							for (var i = 0; i < error.length; i++) {
								response += "<li>["
										+ Exxo.UI.vars['errorHeaders'][error[i]]
										+ "] : " + error[i]
										+ "</li>\n";
								Exxo.UI.vars[Exxo.UI.translates['regex']] = "";
								Exxo.UI.vars[Exxo.UI.translates['regexDesc']] = "";
							}
							$("#error .channels").html(response);
							$("#error").show();
						}
					}
	
					$("#process").hide("slow");
					Exxo.hideSpinner();
				});// End of $("input[name='channel']").each

			// Buster Level Components
			var range_all_sliders = {
				'min' : [ 0 ],
				'max' : [ 4 ]
			};

			$(".gainSelect").each(function(index) {
				var startValue = $(this).attr('level');
				var slider = this;
				noUiSlider.create(slider, {
					start: startValue,
					step: 1,
					range: range_all_sliders,
					pips: {
						mode : 'values',
						values : [ 0, 1, 2, 3, 4 ],
						density : 0,
						stepped : true
					},
					format: wNumb({
						decimals: 0
					})
				});
				slider.noUiSlider.on('set', function(values, handle) {
					var value = values[handle];
					$("#gain-" + $(slider).attr('rel')).val(value);
				});
				if(!Exxo.UI.vars['is-connected']) {
					slider.setAttribute('disabled', true);
				}
			});
			
			$(".send-action-btn").click(function(e){
				var action = $(this).attr("btn-action");
				var url = $(this).attr("href");
				Exxo.Box.Edit.sendActionRequest(action, url);
				e.preventDefault();
				e.stopPropagation();
				return false;
			});
		},
		sendActionRequest : function(action, url) {
			Exxo.showSpinner();
			$.ajax({type:'POST',
	            data: {	serial: Exxo.UI.vars['serial-number'],
	            		requestAction: action},
	            url: url, 
	            async: true,
	            cache: false,
	            success: function(data) {
	               Exxo.hideSpinner();
	               if(data.error){
	                  //There was an error
	                  Exxo.showErrorMessage(data.msg, true, '', false);
	               } else {//There's no error
	            	  Exxo.showInfoMessage(data.msg, true, '', false);
	               }
	            },
	            error: function(xhr, textStatus, error){
	              Exxo.showErrorMessage("Unexpected error; please try again", true, '', false);
	              Exxo.hideSpinner();
	            }
			 });
		}
	},
	castStrToBool : function(str) {
		var valueInLoweCase = str.toLowerCase();

		if ((valueInLoweCase == 'false') || (valueInLoweCase == "false")) {
			return false;
		} else if ((valueInLoweCase == 'true') || (valueInLoweCase == "true")) {
			return true;
		} else {
			return undefined;
		}
	}
};