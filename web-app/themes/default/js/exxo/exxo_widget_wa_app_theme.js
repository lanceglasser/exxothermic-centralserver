/***
 @file      exxo_widget_location.js
 @brief     Fuctions for the Location Widget
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.WidgetsWaAppTheme = {
	initWaAppThemeWidget: function(){
			$( "#waAppTheme-widget" ).exxoWidget({
				helpMsg:	Exxo.UI.translates['widget-shortcuts-info-msg'],
				showDashboardTitleRef:	false
			});
			Exxo.WidgetsWaAppTheme.loadWaAppThemeInfo();
	},
	loadWaAppThemeInfo: function(){
		if(Exxo.UI.vars["location-id"] != 0){
			$("#welcome-ad-info,#app-theme-info").hide();
			$("#waAppTheme-loading").show();
	        $.ajax({type:'POST',
	              data:'object=locationStyle&locationId=' + Exxo.UI.vars["location-id"],                                  
	              url: Exxo.UI.urls["widget-get-detailed-data-url"],
	              success:function(data) {
	            	  console.debug("*** wa appTheme Info: " + data);
	            	  console.debug(data);
	            	  Exxo.UI.vars["locationId"] = data.locationId;
	            	  if(data.locationId !== 0) { //Company was found
	            		  var fields = new Array("wName", "skipEnable", "skipTime","welcomeType", "smallImageUrl","videoUrl", // welcome ad fields 
	            				  "aName", "primaryColor", "secondaryColor", "title","backgroundImageUrl");
	            		  
	            		  if(data["wName"] !== ""){
	            			  //welcomeTypeCode
	            			  if(data["welcomeTypeCode"] == "IMAGE" && 
	            					  data["smallImageUrl"] != ""){
	            				  $("#wa-video-preview-msg").hide();
	            				  $("#smallImageUrl").attr("src", data["smallImageUrl"]);
	            				  $("#smallImageUrl").show();
	            			  } else {
	            				  $("#smallImageUrl").hide();
	            				  $("#wa-video-preview-msg").show();
	            			  }
	            			  $("#wName").css("font-weight", "bold");
	            			  $("#wName").html(data["wName"]);
		            		  $("#skipEnable").html(data["skipEnable"]);
		            		  $("#skipTime").html(data["skipTime"]);
		            		  $("#welcomeType").html(data["welcomeType"]);
		            		  
		            		  $("#wa-info-container").show();
	            		  } else { //Not Welcome Ad associate
	            			  $("#wa-info-container").hide();
	            			  $("#wName").css("font-weight", "normal");
	            			  $("#wName").html("Will use the default depending of venue server");
	            		  }
	            		  
	            		  
	            		  //
	            		  $("#aName").html(data["aName"]);
	            		  $("#title").html(data["title"]);
	            		  
	            		  if(data["backgroundImageUrl"] == ""){
	            			  $("#backgroundImageUrl").hide();
	            			  $("#aps-video-preview-msg").show();
	            		  } else {
	            			  $("#aps-video-preview-msg").hide();
	            			  $("#backgroundImageUrl").attr("src", data["backgroundImageUrl"]);
	            			  $("#backgroundImageUrl").show();
	            		  }
	            		  
	            		  $("#primaryColor").css("background-color",data["primaryColor"]);
	            		  $("#secondaryColor").css("background-color",data["secondaryColor"]);
	            		   
	            		  $("#waAppTheme-loading").hide();
	            		  $("#welcome-ad-info,#app-theme-info").show();
	            	  } else {
	            		  $("#waAppTheme-loading").hide();
	            		  console.debug("Should hide the Widget");
	            	  }
	                 //Exxo.hideSpinner();
	                 if(data.error == false) {//There was an error
	                	 $("#waAppTheme-loading").hide();
	                 }	
	              },
	              error:function(XMLHttpRequest,textStatus,errorThrown){
	                   console.debug("Unexpected error getting theme info: " + errorThrown);
	                   $("#waAppTheme-loading").hide();
	              }
	         });
		}
	} // End of loadWaAppThemeInfo
};