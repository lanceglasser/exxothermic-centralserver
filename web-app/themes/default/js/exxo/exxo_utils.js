/***
 @file      exxo_utils.js
 @brief     General functions for the Selection and Upload of Files
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Utils = {
    Urls: {
       getUrlParam: function(url, name){
          name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
          var regexS = "[\\?&]"+name+"=([^&#]*)";
          var regex = new RegExp( regexS );
          var results = regex.exec( url );
          if( results == null )
            return null;
          else
            return results[1];
       }
    },
	Files: {
	   checkFile: function(file){
	      var file_list = file.files;
	      var oFile = file[0];
	      
	      var fileExtension = oFile.name.split('.')[oFile.name.split('.').length - 1].toLowerCase();
          var iConvert = (oFile.size / 1024).toFixed(2);

          txt = "File type : " +fileExtension + "\n";
          if(oFile.size > (1024 * 1024)){
              txt += "Size: " + (oFile.size / (1024*1024)).toFixed(2) + " MB \n";
          } else {
             txt += "Size: " + (oFile.size / 1024).toFixed(2) + " KB \n";
          }
          alert(txt);
          /*
	      for (var i = 0, file; file = file_list[i]; i++) {
	          var fileExtension = file.name.split('.')[file.name.split('.').length - 1].toLowerCase();
	          var iConvert = (file.size / 1024).toFixed(2);

	          txt = "File type : " +fileExtension + "\n";
	          if(file.size > (1024 * 1024)){
	              txt += "Size: " + (file.size / (1024*1024)).toFixed(2) + " MB \n";
	          } else {
	          txt += "Size: " + (file.size / 1024).toFixed(2) + " KB \n";
	          }
	          alert(txt);
	      }*/
	   },
	   validateImageFileExactSize : function(file, width, height, maxSize, onSuccessFunction, onErrorFunction ){
	      Exxo.Utils.Files.validateImageFile(file, width*1, height*1, width*1, height*1, maxSize, onSuccessFunction, onErrorFunction);
	   },
	   validateImageFile : function(file, minWidth, minHeight, maxWidth, maxHeight, maxSize,
	         onSuccessFunction, onErrorFunction ){
	      //Exxo.Utils.Files.checkFile(file);
	      var ref = (file.attr('ref') != null && file.attr('ref') != "undefined")? file.attr('ref'):"";
          var oFile = file[0].files[0];
          var errorDiv = document.getElementById("errorDiv"+ref);
          // Hide error section
          errorDiv.style.display = 'none';
          errorDiv.innerHTML = '';
          
          // Check for image type (jpg and png are allowed)
          var rFilter = /^(image\/jpeg|image\/png)$/i;
          if ( oFile == undefined || !rFilter.test(oFile.type) || oFile.name.indexOf("jpeg") > -1) {
             errorDiv.innerHTML = '<ul><li>' + Exxo.UI.translates[Exxo.Constants.ERROR_FILE_INVALID] + '</li></ul>';
             errorDiv.style.display = 'block';
             onErrorFunction(file);
             return false;
          }
          
          // Check for file size
          //console.debug("Validating size " + oFile.size + " with " + maxSize);
          if (oFile.size > maxSize) {
             errorDiv.innerHTML = '<ul><li>' + file.attr('fileSizeError') + '</li></ul>';
             errorDiv.style.display = 'block';
             onErrorFunction(file);
             return false;
          }
          
          try{
        	  //It's not Using Safari
    		  // prepare HTML5 FileReader
              var oReader = new FileReader();
              oReader.onload = function(e) {        
                 // e.target.result contains the DataURL which we can use as a source of the image
       
                 var newImage = new Image();
                 
                 newImage.onload = function() {
                    
                    /*console.debug("Validating image inside with min: [" + minWidth + "-"+minHeight + "] and max: ["
                          + maxWidth + "-" + maxHeight+"]");*/
                    
                    if(this.width > maxWidth || this.width < minWidth || this.height < minHeight || this.height > maxHeight){
                       errorDiv.innerHTML = '<ul><li>' + file.attr('sizeError') + '</li></ul>';
                       errorDiv.style.display = 'block';
                       onErrorFunction(file);
                       return;
                    } else {
                       //oImagePreview.src = oImage.src=this.src;
                       //$('#' + inputId).val(file.val().split(/(\\|\/)/g).pop());
                       onSuccessFunction(file);
                       Exxo.Images.initCrop(file, this, minWidth, minHeight, maxWidth, maxHeight);
                    }
                 };
                 newImage.src=e.target.result;
              };
              // Read selected file as DataURL
              oReader.readAsDataURL(oFile);
          }catch(error){
        	  //Do not suppor FileReader; probably it's Using Safari
        	  //if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {}
        	  onSuccessFunction(file);
          }
          
          
          return true;
       },
	   validateVideoFile: function(file){
	      var ref = (file.attr('ref') != null && file.attr('ref') != "undefined")? file.attr('ref'):"";
	      var oFile = file[0].files[0];
	      var errorDiv = document.getElementById("errorDiv"+ref);
	      // Hide error section
	      errorDiv.style.display = 'none'; 
	      
	      // Check for video type (avi and mp4 are allowed)
	      var rFilter = /^(video\/264|video\/mp4)$/i;
	      if (! rFilter.test(oFile.type)) {
	         errorDiv.innerHTML = '<ul><li>' + Exxo.UI.translates[Exxo.Constants.ERROR_FILE_INVALID] + '</li></ul>';
	         errorDiv.style.display = 'block';
	         return false;
	      }
	      
	      var maxSize = Exxo.Constants.getValue(
	            Exxo.Constants.VIDEO_MAX_SIZE_KEY, Exxo.Constants.VIDEO_MAX_SIZE_DEFAULT);
	      
	      // Check for file size
	      if (oFile.size > maxSize) {
	         errorDiv.innerHTML = '<ul><li>' + Exxo.UI.translates[Exxo.Constants.ERROR_FILE_SIZE] + '</li></ul>';
	         errorDiv.style.display = 'block';
	         return false;
	      }
	      
	      return true;
	   },
	   validateDocumentFile: function(file){
	      var ref = (file.attr('ref') != null && file.attr('ref') != "undefined")? file.attr('ref'):"";
	      var oFile = file[0].files[0];
	      var errorDiv = document.getElementById("errorDiv"+ref);
	      // Hide error section
	      errorDiv.style.display = 'none'; 
	      
	      // Check for video type (avi and mp4 are allowed)
	      var rFilter = /^(application\/pdf)$/i;
	      if (! rFilter.test(oFile.type)) {
	    	 console.debug("File Type: " + oFile.type);
	         errorDiv.innerHTML = '<ul><li>' + Exxo.UI.translates[Exxo.Constants.ERROR_FILE_INVALID] + '</li></ul>';
	         errorDiv.style.display = 'block';
	         return false;
	      }
	      
	      var maxSize = Exxo.Constants.getValue(
	            Exxo.Constants.DOCUMENT_MAX_SIZE_KEY, Exxo.Constants.DOCUMENT_MAX_SIZE_DEFAULT);
	      
	      // Check for file size
	      if (oFile.size > maxSize) {
	    	 console.debug("File Size: " + oFile.size + "; maxSize: " + maxSize);
	         errorDiv.innerHTML = '<ul><li>' + Exxo.UI.translates[Exxo.Constants.ERROR_FILE_SIZE] + '</li></ul>';
	         errorDiv.style.display = 'block';
	         return false;
	      }
	      
	      return true;
	   },
	   validateSoftwareVersionFile: function(file){
	      var ref = (file.attr('ref') != null && file.attr('ref') != "undefined")? file.attr('ref'):"";
	      var oFile = file[0].files[0];
	      var errorDiv = document.getElementById("errorDiv"+ref);
	      // Hide error section
	      errorDiv.style.display = 'none';
	      // application/gzip 
	      var gzipFilter = /^(application\/gzip)$/i;
	      // application/x-gzip 
	      var xgzipFilter = /^(application\/x-gzip)$/i;
	      if ( gzipFilter.test(oFile.type) || xgzipFilter.test(oFile.type)) {
	    	 return true;
	      } else {
	    	 console.debug("Software version file type: " + oFile.type);
	         errorDiv.innerHTML = '<ul><li>' + Exxo.UI.translates[Exxo.Constants.ERROR_FILE_INVALID] + '</li></ul>';
	         errorDiv.style.display = 'block';
	         return false; 
	      }
	   }
	}
};