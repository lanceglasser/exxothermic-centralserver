/*******************************************************************************
 * @file exxo_widget_exxtractors.js
 * @brief Functions for the Exxtractors List Widget
 * @author Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.WidgetsExxtractors = {
	initExxtractorsListWidget : function() {

		$("#exxtractors-widget").exxoWidget({
			helpMsg : Exxo.UI.translates['widget-exxtractors-list-info-msg'],
			referenceIcon : 'list',
			referenceUrl : Exxo.UI.urls['exxtractors-list']
		});

		Exxo.UI.vars["widgetExxtractorsTable"] = $('#widgetExxtractorsTable')
				.dataTable(
						{
							"pagingType" : "full", // More info at:
													// https://datatables.net/examples/basic_init/alt_pagination.html
							"lengthMenu" : [ [ 5 ], [ 5 ] ], // More info at:
																// https://datatables.net/examples/advanced_init/length_menu.html
							"processing" : true,
							"serverSide" : true,
							"ajax" : {
								"url" : Exxo.UI.urls["widget-get-data-url"],
								"data" : function(d) {
									d.object = 'exxtractor';
									d.companyId = Exxo.UI.vars["company-id"];
									d.locationId = Exxo.UI.vars["location-id"];
								}
							},
							"columnDefs" : [     
									{
										"targets" : [ 1 ],
										"render" : function(data, type, row) {
											return '<a href="'
											+ Exxo.UI.urls["exttractors-edit-url"]
											+ row[4] + '">' + row[0] + ' / ' + row[1]
											+ '</a>';
										}
									},
									{
										"targets" : [ 2 ],
										"render" : function(data, type, row) {
											return row[2];
										}
									},
									{
										"targets" : [ 0 ],
										"render" : function(data, type, row) {
											if (row[3]) {
												return '<i class="state stateConnected" title="' + row[5] + '"></i>';
											} else {
												return '<i class="state stateDisconnected" title="' + row[5] + '"></i>';
											}
										}
									} ]
						});
	}
};