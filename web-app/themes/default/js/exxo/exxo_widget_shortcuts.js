/***
 @file      exxo_widget_shortcuts.js
 @brief     Fuctions for the Shortcuts Widget
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.WidgetsShortcuts = {
	initShortcutsWidget: function(){
		$( "#shortcuts-widget" ).exxoWidget({
			helpMsg:	Exxo.UI.translates['widget-shortcuts-info-msg'],
			showDashboardTitleRef:	false,
			enableConfigWindow:		true
		});
		
		$('#reg-exx-window').dialog({
	          height: 375,
	          width: 600,
	          modal: true,
	          autoOpen: false,
	          buttons: [ 
					{
						text: Exxo.UI.translates['registerButton'],
						"class": 'mainModalWindowBtn',
						click: function() {
							$("#reg-exx-form").submit();
						}
					},
	                { text: Exxo.UI.translates['cancelButton'],
	                  click: function() {
	                	  $('#reg-exx-window').dialog("close");
	                  }
	                }	                
	          ]
	    });
		
		$('#shortcuts-widget .widget-config-window').dialog({
	          height: 300,
	          width: 400,
	          modal: true,
	          autoOpen: false,
	          buttons: [
					{
						text: Exxo.UI.translates['saveButton'],
						"class": 'mainModalWindowBtn',
						click: function() {
							Exxo.UserDashboard.saveWidgetConfig();
						}
					},
	                { text: Exxo.UI.translates['closeButton'],
	                  click: function() {
	                	  $('.widget-config-window').dialog("close");
	                  }
	                }
	          ]
	    });
		
		Exxo.WidgetsShortcuts.loadUserShortcuts();
	},
	loadUserShortcuts: function(){
		$("#main-content").hide();
		$("#loading-div").show();
        $.ajax({type:'POST',
              data:'object=shortcuts&companyId=' + Exxo.UI.vars["company-id"] + '&locationId=' + Exxo.UI.vars["location-id"],                                  
              url: Exxo.UI.urls["widget-get-detailed-data-url"],
              success:function(data) {
            	  if(data.error === false){
            		  $("#main-content").html(data.links);
            	  } else {
            		  $("#main-content").html("");
            	  }
            	  $("#loading-div").hide();
        		  $("#main-content").show();
        		  Exxo.WidgetsShortcuts.initSpecialShortcuts();
              },
              error:function(XMLHttpRequest,textStatus,errorThrown){
                   console.debug("Unexpected error getting the user shortcuts: " + errorThrown);
                   $("#main-content").html("");
                   $("#main-content").show();
                   $("#loading-div").hide();
              }
         });
	}, // End of loadUserShortcuts
	loadAllListShortcuts: function(){
		$("#shortcuts-list").hide();
		$("#shortcuts-list-container").html("");
		$("#loading-shortcuts-list-div").show();
        $.ajax({type:'POST',
              data:'object=shortcutsall&companyId=' + Exxo.UI.vars["company-id"] + '&locationId=' + Exxo.UI.vars["location-id"],                                  
              url: Exxo.UI.urls["widget-get-detailed-data-url"],
              success:function(data) {
            	  if(data.error === false){
            		  $("#shortcuts-list-container").html(data.links);
            	  } else {
            		  $("#shortcuts-list-container").html("");
            	  }
            	  $("#loading-shortcuts-list-div").hide();
            	  $("#shortcuts-list").show();
        		  Exxo.WidgetsShortcuts.initShortcutsCBEvent();
              },
              error:function(XMLHttpRequest,textStatus,errorThrown){
                   console.debug("Unexpected error getting the user shortcuts: " + errorThrown);
                   $("#shortcuts-list").show();
                   $("#loading-shortcuts-list-div").hide();
              }
         });
	}, // End of loadAllListShortcuts
	openConfigWindow: function(widget){
		var widgetId = widget.attr("id");
		Exxo.WidgetsShortcuts.loadAllListShortcuts();
		$('#' + widgetId + '-conf').dialog('open');
	}, // End of openConfigWindow
	initSpecialShortcuts: function(){
		$("#reg-exx-btn").unbind().click(function(e){
			e.preventDefault();
			Exxo.WidgetsShortcuts.cleanRegisterExXtractorForm();
			$('#reg-exx-window').dialog("open");
			return false;
		});
		
		$("#reg-exx-form").unbind().submit(function(e){
			e.preventDefault();
			Exxo.WidgetsShortcuts.registerExXtractor(this);
		});
	},
	cleanRegisterExXtractorForm: function(){
		$("#reg-exx-form").find("#serial").val("");
		$("#reg-exx-form").find("#name").val("");
		$("#reg-exx-error").html("").hide();
	},
	registerExXtractor: function($this){
		Exxo.showSpinner();
		$.ajax({
			url: Exxo.UI.urls["register-exx"], // Url to which the request is send
			type: "POST",             // Type of request to be send, called as method
			data: new FormData($this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,        // To send DOMDocument or non processed data file it is set to false
			success: function(data)   // A function to be called if request succeeds
			{
				Exxo.hideSpinner();
				if(data.error){ //There was an error
					//alert(data.msg);
					$("#reg-exx-error").html(data.msg).show();
				} else {//Upload success
					$('#reg-exx-window').dialog("close");
					Exxo.UserDashboard.refreshWidget("exxtractors-widget");
					Exxo.showInfoMessage(data.msg, true, '', false);
				}
			},
			error: function()
			{
				Exxo.hideSpinner();
				//Exxo.WidgetsLocation.closeUploadLogoWindow(false, "");
			}
		});
	},
	initShortcutsCBEvent: function(){
		$(".shortcut-cb").unbind();
		
		$(".shortcut-cb").on('click',function(e){
			var selectedCounter = 0;
			$('input:checkbox.shortcut-cb').each(function () {
				selectedCounter += this.checked? 1:0;
			});
			
			if($(this).is(':checked')){				
				if(selectedCounter > (Exxo.UI.vars["max-shortcuts"])){
					Exxo.showInfoMessage(Exxo.UI.translates['max-shortcuts-error'], true, '', false);
					$(this).attr("checked",false);
					$(this).removeAttr('checked');
					e.preventDefault();
				    e.stopPropagation();
					return false;
				}
			} else {
				if(selectedCounter === 0){
					Exxo.showInfoMessage(Exxo.UI.translates['min-shortcuts-error'], true, '', false);
					$(this).attr("checked",true);
					e.preventDefault();
				    e.stopPropagation();
					return false;
				}
			}
		});
	},
	saveWidgetConfig: function(widgetId){
		//Must get the checked shortcuts from the List
		var selectedShortcuts = "";
		$('input:checkbox.shortcut-cb').each(function () {
			if(this.checked){
				selectedShortcuts += (selectedShortcuts === "")? $(this).val():','+$(this).val();
			}
		});
		//alert(selectedShortcuts);
		Exxo.showSpinner();
		$.ajax({type:'POST',
            data:'object=saveShortcuts&companyId=' + Exxo.UI.vars["company-id"] + '&locationId=' + Exxo.UI.vars["location-id"]
					+ '&shortcuts=' + selectedShortcuts,
            url: Exxo.UI.urls["widget-get-detailed-data-url"],
            success:function(data) {
            	Exxo.hideSpinner();
            	if(data.error === false){
            		$('#' + widgetId + '-conf').dialog('close');
            		Exxo.UserDashboard.refreshWidget("shortcuts-widget");
          	  	} else {
          		  Exxo.showErrorMessage("Unexpected error saving the information", true, '', false);
          	  	}
            },
            error:function(XMLHttpRequest,textStatus,errorThrown){
                 console.debug("Unexpected error saving the user shortcuts: " + errorThrown);
                 Exxo.hideSpinner();
                 Exxo.showErrorMessage("Unexpected error saving the information", true, '', false);
            }
       });
	}
};