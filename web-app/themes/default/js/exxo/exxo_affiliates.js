/***
 @file      exxo_affiliates.js
 @brief     Custom functions and events for the Affiliates pages
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Affiliates = {
   Establishments : {
       init : function() {         
         Exxo.UI.vars['selectedLocations'] = "";
         
         $("#affiliates").select2();
         $("#affiliates").change();
         
         
         $("#resetButton").click(function(e){
            e.preventDefault();
            //workingWithLocationCB = true;            
            var selectedLocationsIds = Exxo.UI.vars['selectedLocations'].split(",");
            $(".cbType").prop('checked', false);
            $(".cbSubtype").prop('checked', false);
            for(i=0; i < selectedLocationsIds.length; i++) {
               $("#type_" + selectedLocationsIds[i]).prop('checked', true);
            }
            //workingWithLocationCB = false;
        });
         
         $("#saveForm").submit(function(e){
            e.preventDefault();
            var selectedOptions = "";
            $(".cbType").each(function( index, elem ) {
               if($(elem).attr("hasChilds") !== "1" && $(elem).is(':checked')){
                  //console.debug("type->" + $(elem).attr("typeId") + ": " + $(elem).attr("text") );
                  selectedOptions += (selectedOptions === "")? $(elem).attr("typeId"):","+$(elem).attr("typeId");
               }
            });
            $(".cbSubtype").each(function( index, elem ) {
               if($(elem).is(':checked')){
                  //console.debug("subtype->" + $(elem).attr("subTypeId") + ": " + $(elem).attr("text") );
                  selectedOptions += (selectedOptions === "")? $(elem).attr("subTypeId"):","+$(elem).attr("subTypeId");
               }
            });
            
            //Send request to save the selected options
            Exxo.Affiliates.Establishments.saveAssociations(selectedOptions);
            
            return false;//Prevent all events including submit
         });
       },
       saveAssociations: function(selected){
          Exxo.showSpinner();
          Exxo.Affiliates.hideMessages();
          $.ajax({type:'POST',
                data:'a=' + $("#affiliates").val() + '&s='+selected,                                  
                url: $("#saveForm").attr('action'), 
                success:function(data) {
                   Exxo.hideSpinner();
                   if(data.error === false){
                      Exxo.Affiliates.showSuccessMessage(data.msg);
                   } else {
                      //There was an error
                      console.debug("msg: " + data.msg);
                      Exxo.Affiliates.showError(data.msg);                      
                   }
                  },
                error:function(XMLHttpRequest,textStatus,errorThrown){                              
                     //alert(errorThrown);
                      console.debug("error inesperado");
                     Exxo.hideSpinner();
              }
           });
       },
       initAssociationTableEvents: function(){
          
          var selectedLocationsIds = Exxo.UI.vars['selectedLocations'].split(",");
          
          $.propHooks.checked = {
             set: function(elem, value, name) {
               var ret = (elem[ name ] = value);
               $(elem).trigger("change");
               return ret;
             }
           };
        
           var workingWithCompanyCB = false;
           var workingWithLocationCB = false;
           
           $(".cbType").change(function(e){
               if(!workingWithLocationCB){
                   workingWithCompanyCB = true;
                   e.preventDefault();
                   if ($(this).is(':checked')) {
                       $(".cb_" + $(this).attr('typeId')).prop('checked', true);
                   } else {
                       $(".cb_" + $(this).attr('typeId')).prop('checked', false);
                   } 
                   workingWithCompanyCB = false;
               }
           });
           
           $(".cbSubtype").change(function(e){
               if(!workingWithCompanyCB){
                   workingWithLocationCB = true;
                   e.preventDefault();
                   if ($(this).is(':checked')) {
                       var len = $(".cb_" + $(this).attr('typeId')).length;
                       var chk = $('.cb_' + $(this).attr('typeId') + ':checked').length;
                       if (len == chk){//All of them are checked                       
                           $("#allType_" + $(this).attr('typeId')).prop('checked', true);
                       }
                   } else {                    
                       $("#allType_" + $(this).attr('typeId')).prop('checked', false);
                   }
                   
                   workingWithLocationCB = false;
               }
           });
                      
           for(i=0; i < selectedLocationsIds.length; i++){
               $("#type_" + selectedLocationsIds[i]).prop('checked', true);
           }
           
           $(".minus").click(function(e){
               e.preventDefault();
               $("#subtypesContainer_" + $(this).attr('typeId')).slideUp();
               $(this).hide();
               $("#plus_"+ $(this).attr('typeId')).show();
           });
           
           $(".plus").click(function(e){
               e.preventDefault();
               $("#subtypesContainer_" + $(this).attr('typeId')).slideDown();
               $(this).hide();
               $("#minus_"+ $(this).attr('typeId')).show();
           });
       }
    },
    hideMessages: function(){
       $("#successMsgContainer").hide();
       $("#errorMsgContainer").hide();
    },
    showError: function(msg){
       $("#errorMsg").html(msg);
       $("#errorMsgContainer").show();
    },
    showSuccessMessage: function(msg){
       $("#successMsg").html(msg);
       $("#successMsgContainer").show();
    }
};