/***
 @file      exxo_box_software_update.js
 @brief     Custom functions and events for the Box Software Update Page.
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Box = {
    Software: {
       Update : {
          init : function() {
             $("#save").click(function (e) {
                       $("#save").css({visibility: "hidden"});
                       $("#divImageUpdateProgress").css({visibility: "visible", display:"block"});
                       e.preventDefault();
                   }
               );
             (function($){
               $(document).ready(function() {             	    
             	var ajax_call = function() {
             	   if(!pageUpdated) {
             			var serial=$("input[name='serialNumber']").val();
             		  //your jQuery ajax code
             			$.ajax({type:'POST',
             	            data: "" ,
             	            url: baseURL + '/boxSoftware/asReconnected?serialNumber='+serial, 
             	            async: false,
             	            cache: false,
             	            contentType: false,
             	            processData: false,
             	            success: function(data) {
             	            	switch (data.status) {
	             	            	case 1: // In progress do nothing and keep waiting
	             	            		break;
	             	            	case 2: // Done with success
	             	            	case 3: // Failed
	             	            		pageUpdated = true;
	             	            		var statusTD = $("#status_"+serial);
	            		            	statusTD.html(Exxo.UI.translates["status.msg." + data.status]);
	            		            	$(".hideOnProgress").show();
	            		            	$("#finishedAt").html(data.finishedAtAsString);
	            		            	$("#description").html(data.msg);
	            		            	$("#messageUpdate").hide();
	             	            		break;
	             	            	case 0: // The reference was not found
	             	            	default:
	             	            		break;
             	            	}
             	            }
             			 });
             	   }
             	};
             	var interval = 1000 * 5; // every 30 seconds
             	setInterval(ajax_call, interval);
               }); // $(document).ready  
             })(jQuery);
          }
       }
    }
};