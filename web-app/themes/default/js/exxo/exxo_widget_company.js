/***
 @file      exxo_widget_company.js
 @brief     Fuctions for the Company Widget
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.WidgetsCompany = {
	initCompaniesListWidget: function(){
		
		$( "#companies-widget" ).exxoWidget({
			helpMsg:	Exxo.UI.translates['widget-companies-list-info-msg'],
			referenceIcon: 	'list',
			referenceUrl: 	Exxo.UI.urls['companies-list']});
		
		Exxo.UI.vars["widgetCompaniesTable"] = $('#widgetCompaniesTable').dataTable( {
			"pagingType": "full", // More info at: https://datatables.net/examples/basic_init/alt_pagination.html
			"lengthMenu": [[5], [5]], // More info at: https://datatables.net/examples/advanced_init/length_menu.html
       	 	"processing": true,
            "serverSide": true,
            "ajax": {
            	"url": Exxo.UI.urls["widget-get-data-url"],
    			"data": function ( d ) {
    		        d.object = 'company';
    		    }
            },
            "columnDefs": [
                  {
                	  "targets": [ 0 ],
		              "render": function ( data, type, row ) {
		            	  return '<a href="' + Exxo.UI.urls["company-dashboard-url"] + row[0] + '">' + row[1] + '</a>';
		              }
                  },
		          {
		        	  "targets": [ 1 ],
		              "render": function ( data, type, row ) {
		            	  return '<div class="state ' + 
		            	  		(row[2]? 'stateGreen':'stateGray') + '"></div>';
		              }
		          }
      		 ]
        } );
	},	
	initCompanyWidget: function(){
		Exxo.UI.vars["isIntegrator"] = false;
		Exxo.UI.vars["changing-logo"] = false;
		$( "#company-widget" ).exxoWidget({
			helpMsg:	Exxo.UI.translates['widget-company-info-msg'],
			showDashboardTitleRef:	false,
			allowRemove:			false
		});
		$("#company-edit-btn").click(function(e){
			e.preventDefault();
			if(Exxo.UI.vars["isIntegrator"]){
				window.location = Exxo.UI.urls["edit-integrator-url"] + Exxo.UI.vars["companyId"];
			} else {
				window.location = Exxo.UI.urls["edit-company-url"] + Exxo.UI.vars["companyId"];
			}
			return false;
		});
		// Event when click the change logo link
		$("#change-logo-btn").click(function(e){
			e.preventDefault();
			Exxo.WidgetsCompany.openChangeLogoWindow();
			return false;
		});
		$("#upload-logo-window" ).dialog({
	          height: 500,
	          width: 700,
	          modal: true,
	          autoOpen: false,
	          buttons: [ 
	                { text: Exxo.UI.translates['cancelButton'],
	                  click: function() {
	                	  Exxo.WidgetsCompany.closeUploadLogoWindow(false, "");
	                  }
	                },
	                {
	                	text: Exxo.UI.translates['uploadButton'],
	                	click: function() {
	                		$("#uploadLogo").submit();
	                	}
	                }
	          ]
	    });
		$("#uploadLogo").submit(function(e){
			e.preventDefault();
			Exxo.WidgetsCompany.uploadLogo(this);
		});
		
		Exxo.WidgetsCompany.loadCompanyInfo();
	},
	uploadLogo: function($this) {
		var file = $("#bgLogoUrl");
		if($("#"+file.attr('id') + "-updated").val() == 0){
			var errorDiv = document.getElementById('errorDiv');
            errorDiv.innerHTML = '<ul><li>' + Exxo.UI.translates['select-image-before-save'] + '</li></ul>';
            errorDiv.style.display = 'block';
		} else {
			Exxo.showSpinner();
			$.ajax({
				url: Exxo.UI.urls["upload-file"], // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method
				data: new FormData($this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				contentType: false,       // The content type used when sending data to the server.
				cache: false,             // To unable request pages to be cached
				processData:false,        // To send DOMDocument or non processed data file it is set to false
				success: function(data)   // A function to be called if request succeeds
				{
					if(data.error){ //There was an error
						alert(data.msg);
					} else {//Upload success
						Exxo.WidgetsCompany.closeUploadLogoWindow(true, data.url);
						Exxo.hideSpinner();
					}
				},
				error: function()
				{
					Exxo.hideSpinner();
					Exxo.WidgetsCompany.closeUploadLogoWindow(false, "");
				}
			});
		}
	},
	closeUploadLogoWindow: function(mustUpdateImage, imgSrc){
		if(mustUpdateImage){
			$("#company-logo").attr("src", imgSrc);
		}
		Exxo.UI.vars["changing-logo"] = false;
		$( "#upload-logo-window" ).dialog("close");
	},
	loadCompanyInfo: function(){
		if(Exxo.UI.vars["company-id"] == 0) return;
		$("#company-info").hide();
		$("#company-loading").show();
        $.ajax({type:'POST',
              data:'object=company&companyId=' + Exxo.UI.vars["company-id"],                                  
              url: Exxo.UI.urls["widget-get-detailed-data-url"],
              success:function(data) {
            	  console.debug("*** Company Info: " + data);
            	  //console.debug(data);
            	  Exxo.UI.vars["companyId"] = data.companyId;
            	  if(data.companyId != 0) { //Company was found
            		  var fields = new Array("name", "webSite", "phoneNumber", "companyWideIntegrator", 
            				  "typeOfCompany", "typeOfEstablishment", "timezone", "zipCode", "address", 
            				  "enabled", "poc");
            		  var index;
            		  for (index = 0; index < fields.length; index++) {
            			    $("#" + fields[index] +"Div").find(".field-value").html(data[fields[index]]);
            		  }
            		  Exxo.UI.vars["isIntegrator"] = data["isIntegrator"]; //Save the type for the Edit link
            		  //Logo
            		  if(data.hasLogo){
            			  $("#company-logo").attr("src", data["logo"]);  
            		  } else {
            			  $("#company-logo").attr("src", "");
            		  }
            		  
            		  $("#company-loading").hide();
            		  $("#company-info").show();
            	  } else {
            		  $("#company-loading").hide();
            		  console.debug("Should hide the Widget");
            	  }
                 //Exxo.hideSpinner();
                 if(data.error == false) {//There was an error
                	 $("#company-loading").hide();
                 }	
              },
              error:function(XMLHttpRequest,textStatus,errorThrown){
                   console.debug("Unexpected error getting company info: " + errorThrown);
                   $("#company-loading").hide();
              }
         });
	}, 
	openChangeLogoWindow: function() {
		if(!Exxo.UI.vars["changing-logo"]){ //Only do something when the window is not already changing the logo
			Exxo.UI.vars["changing-logo"] = true;
			Exxo.Images.resetComponent($("#bgLogoUrl"));
			$("#upload-logo-window").dialog("open");
		}
	}
};