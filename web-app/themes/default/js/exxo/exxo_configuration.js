/***
 @file      exxo_configuration.js
 @brief     Custom functions and events for the Configuration pages
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Configurations = {
   Edit : {
	   rowsIndex: 0,
       init : function() {
    	   Exxo.Configurations.Edit.assignDeleteEventToRows();
    	   Exxo.Configurations.Edit.rowsIndex = $('#values-list tr').length;
    	   
    	   $("#add-item-btn").click(function(e){
    		   var newIndex = Exxo.Configurations.Edit.rowsIndex + 1;
    		   $('#values-list tr:last').after('<tr id="item-'+newIndex+'" class="odd"><td><input type="text" class="item" value=""></td><td><a href="#" rel="'+newIndex+'" class="action-icons delete" title="Delete"></a></td></tr>');
    		   Exxo.Configurations.Edit.assignDeleteEventToRows();
    		   Exxo.Configurations.Edit.rowsIndex = newIndex;
    		   e.preventDefault();
    		   e.stopPropagation();
    		   return false;
    	   });
    	   
    	   $("#updateForm").submit(function(e) {
    		   if(Exxo.UI.vars["configuration-type"] === Exxo.UI.vars["configuration-list-type"]) {
    			   //Only validates if the configuration is a list
    			   var strValue = "";
        		   var itemValue = "";
        		   var emptyItems = false;
        		   $('.item').each(function(i, obj) {
        			   itemValue = $(obj).val().trim();
        			   if(itemValue == ""){
        				   emptyItems = true;
        			   }
        			   strValue += (strValue == "")? itemValue:","+itemValue;
        			});
        		   if(emptyItems){
        			   Exxo.showErrorMessage("Can't leave empty items, remove the item if you don't need it.", true, '', false);
        			   e.preventDefault();
        			   e.stopPropagation();
        			   return false;
        		   } else {
        			   $("#value").val(strValue);
        		   }
    		   }
    	   });
       },
       assignDeleteEventToRows: function() {
    	   $(".delete").unbind();
    	   $(".delete").click(function(e){
    		   e.preventDefault();
    		   e.stopPropagation();
    		   var index = $(this).attr("rel");
    		   $("item-" + index).remove();
    		   $('table#values-list tr#item-'+ index).remove();
    		   return false;
    	   });
       }
    }
};