
function showSpinner(){
   $('#ajaxFlowWaitImg').show(40);
   Exxo.showSpinner();
}
function hideSpinner(){
   $('#ajaxFlowWaitImg').hide();
   Exxo.hideSpinner();
}

(function($){
	  $(document).ready(function()  {
	     Exxo.Images.init();
	     
	    $('#myUpload').submit(function(e){	    	
			e.preventDefault();
			showSpinner();
			if($("#file-updated").val() == 0){
				//There was an error
                var errorDiv = document.getElementById('errorDiv');
                hideSpinner();
                errorDiv.innerHTML = '<ul><li>' + Exxo.UI.translates['select-image-before-save'] + '</li></ul>';
                errorDiv.style.display = 'block';
			} else {
				var formData = new FormData($(this)[0]);
				
				 $.ajax({type:'POST',
		            data: formData ,
		            url: Exxo.UI.urls['baseURL'] + '/box/uploadFile?type=small', 
		            async: true,
		            cache: false,
		            contentType: false,
		            processData: false,
		            success: function(data) {
		               hideSpinner();
	                   if(data.error){
	                      //There was an error
	                      var errorDiv = document.getElementById('errorDiv');
	                      hideSpinner();
	                      errorDiv.innerHTML = '<ul><li>' + data.textStatus + '</li></ul>';
	                      errorDiv.style.display = 'block';
	                   } else {//There's no error
	                      var channelNumber = data.channelNumber;                 
	                      $('#demo_' + channelNumber).show(); 
	                      
	                      $("tr#"+channelNumber+ " input[name='imageURL']").val(data.imageURL);
	                      
	                      var oImage = document.getElementById('target_'+channelNumber);
	                      oImage.src = data.imageURL;
	                      
	                      $('#target_'+channelNumber).show();
	                      
	                      $("#dialogPlaceholder").dialog("close");
	                   }
		            	
		            },
		            error: function(xhr, textStatus, error){
		               //whatever you need to do here - output errors etc.
		            	var errorMessage = $.parseJSON(xhr.responseText);
		              var errorDiv = document.getElementById('errorDiv');
		              hideSpinner();
		              errorDiv.innerHTML = '<ul><li>' + errorMessage.textStatus + '</li></ul>';
		          	  errorDiv.style.display = 'block';
		                           
		            }
				 });
			}
	    });
	    	
	  });
})(jQuery);