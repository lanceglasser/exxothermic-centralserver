/***
 @file      exxo_employee_list.js
 @brief     General functions for the employees list pages
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.EmployeeForm = {
	init : function() {
		Exxo.UI.vars['stopEvent'] = true;
	   	Exxo.UI.vars['target'] = true;
	   		   	
	   	$("#userRoles").select2();
	
	}
};