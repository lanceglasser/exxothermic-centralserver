/*******************************************************************************
 * @file exxo_exxtractors_table.js
 * @brief Functions for the Exxtractors List Table
 * @author Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Exxtractors = {
	Table: {
		initTable: function(){
			Exxo.UI.vars["widgetExxtractorsTable"] = $('#widgetExxtractorsTable')
			.dataTable(
					{
					"pagingType" : "full", // More info at:
											// https://datatables.net/examples/basic_init/alt_pagination.html
					"lengthMenu" : [ [ 5 ], [ 5 ] ], // More info at:
														// https://datatables.net/examples/advanced_init/length_menu.html
					"processing" : true,
					"serverSide" : true,
					"ajax" : {
						"url" : Exxo.UI.urls["exxtractors-get-list-url"],
						"data" : function(d) {
							d.object = 'exxtractor';
							d.companyId = Exxo.UI.vars["company-id"];
							d.locationId = Exxo.UI.vars["location-id"];
						}
					},
					"columnDefs" : [     
							{
								"targets" : [ 1 ],
								"render" : function(data, type, row) {
									return '<a href="'
									+ Exxo.UI.urls["exttractors-edit-url"]
									+ row[4] + '">' + row[0] + ' / ' + row[1]
									+ '</a>';
								}
							},
							{
								"targets" : [ 2 ],
								"render" : function(data, type, row) {
									return row[2];
								}
							},
							{
								"targets" : [ 0 ],
								"render" : function(data, type, row) {											
									return '<div class="state '
											+ (row[3] ? 'stateGreen'
													: 'stateGray')
											+ '"></div>';
								}
							} ]
				});
		}
	}
};