/***
 @file      exxo_constants.js
 @brief     General constants for the Javasript functions
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Constants = {
    VIDEO_MAX_SIZE_KEY: 'VIDEO_MAX_SIZE',
    VIDEO_MAX_SIZE_DEFAULT: 20 * 1024 * 1024,
    DOCUMENT_MAX_SIZE_KEY: 'DOCUMENT_MAX_SIZE',
    DOCUMENT_MAX_SIZE_DEFAULT: 5 * 1024 * 1024,
    IMAGE_MAX_SIZE_KEY: 'IMAGE_MAX_SIZE',
    IMAGE_MAX_SIZE_DEFAULT: 20 * 1024 * 1024,
    ERROR_FILE_INVALID: 'ERROR_FILE_INVALID',
    ERROR_FILE_SIZE: 'ERROR_FILE_SIZE',
    getValue: function(key, dfl){
       return (Exxo.UI.vars[key] != undefined )? 
             Exxo.UI.vars[key] : dfl;
    }
    
};