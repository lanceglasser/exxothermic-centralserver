/***
 @file      exxo_skin_create.js
 @brief     Custom functions and events for the creation and edit of the App Skins
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Skin = {
   Create : {
	   firstSelection: true,
	   	clearScrean: function(){
	   		if(!Exxo.Skin.Create.firstSelection){
	   			$(".alert").hide();
	   			$("#syncResultContainer").hide();
	   		}
	   		Exxo.Skin.Create.firstSelection = false;
	   		Exxo.showSpinner();
	   	},
      jcrop_api_1: null,
      jcrop_api_2: null,
      initCreate: function(){
    	Exxo.Skin.Create.init();
    	//Render Initial App Preview
  		Exxo.AppDemo.renderInitialAppPreview();
        
        Exxo.AppDemo.init();
        
        $("#primaryColor").change(function(){
        	Exxo.AppDemo.setMainColorOfAppPreview($(this).val());
        });
        
        $("#secondaryColor").change(function(){
        	Exxo.AppDemo.setSecondColorOfAppPreview($(this).val());
        });
        
        $("#title").keyup(function(){
       	 //Exxo.Skin.Create.setSecondColorOfAppPreview($(this).val());
       	 $(".phone-app #app-title").html($(this).val());
        });
        
        $("#title").blur(function(){
          	 //Exxo.Skin.Create.setSecondColorOfAppPreview($(this).val());
          	 $(".phone-app #app-title").html($(this).val());
           });
      },
      init : function() {
         //Init Images Functions
         Exxo.Images.init();
         //Exxo.Tables.init();
         
         //Show the div according to the initial Welcome Type
         $("#" + Exxo.UI.vars["welcomeType"] + "-SELECT-DIV").removeClass("hide");
         
         $("#welcomeType").select2();
         
         $("#welcomeType").change(
               function(e){
                  //Hide/Show Container according if value is Image or Video
                  $(".welcome-file").addClass('hide'); //Hide all the divs
                  $("#" + $(this).val() + "-SELECT-DIV").removeClass("hide"); //Show the one according to the value
               }
         );
         
         $("#secondaryColor, #mainColor").change(function() {
            var textColor = this.value;
            var myhex = textColor.replace(/[^0-9a-fA-F]/g, '');
            this.value = "#" + myhex;
         });
         
         $("#createForm").submit(function(e) {
            Exxo.showSpinner();
            var ids = "";
            $(".cbLocation").each(function(){
               if ($(this).is(':checked')) {
                  ids += (ids == "")? $(this).attr("locationId"):","+$(this).attr("locationId"); 
               }
            });
            $("#locationsToSave").val(ids);
         });
         
         $("#syncronizeForm").submit(function(e) {
            Exxo.showSpinner();
         });
         
         /*$("#bgImageUrl").change(function(){
            $("#adsBackgroundImageUpdated").val(1);
            fileSelectHandler($(this), Exxo.Skin.Create.jcrop_api_1);
         });*/
         
         $(".imgUpload").change(function(e){
            var width = $(this).attr('maxWidth');
            var height = $(this).attr('maxHeight');
            var maxSize = $(this).attr('maxSize');
            Exxo.Utils.Files.validateImageFileExactSize($(this), width, height, maxSize,
                  Exxo.Skin.Create.successImageSelection, Exxo.Skin.Create.errorImageSelection)
         });
         
         $("#welcomeVideoUrl").change(function(e){
            //var result = Exxo.Utils.Files.validateVideoFile($(this));
            if(Exxo.Utils.Files.validateVideoFile($(this))){//valid
               $("#wVideoUpd").val(1);
               $('#fakeupload' + $(this).attr('ref')).val($(this).val().split(/(\\|\/)/g).pop());
            } else {
               e.preventDefault();
            }
         });
                  
         $("#view-video").click(function(e){
            Exxo.Skin.Create.openVideoPlayer();
            return false;
         });
         
         $("#video-container").html($("#video").html());
         $("#video").html("");
         
         $("#activeSkin").select2();
         $("#activeSkin").trigger('change');
         
         /* Enable/Disable Autogeneration of Feature Image for Tablets */
         $("#genFeaturedImgForTablets").change(function(e){
         	if(!$(this).is(":checked")) {
         		//If uncheck must give the option to upload a specific image
         		$("#tabletsFeatureImageContainer").show();
         		$("#featureGenExampleDiv").hide();
 	        } else {
 	        	//If check, hide the upload component and shows help message
 	        	$("#tabletsFeatureImageContainer").hide();
 	        	$("#featureGenExampleDiv").show();
 	        }
          });
         
         $("#genFeaturedImgForTablets").trigger('change');
         
         /* Enable/Disable Autogeneration of Dialog Image for Tablets */
         $("#genDialogImgForTablets").change(function(e){
         	if(!$(this).is(":checked")) {
         		//If uncheck must give the option to upload a specific image
         		$("#dialogImageTabletObject").show();
         		$("#dialogGenExampleDiv").hide();
 	        } else {
 	        	//If check, hide the upload component and shows help message
 	        	$("#dialogImageTabletObject").hide();
 	        	$("#dialogGenExampleDiv").show();
 	        }
          });
         
         $("#genDialogImgForTablets").trigger('change');
         
         $(".view-imageHelp").click(function(e){
  			e.preventDefault();
  			 			
  			/*$( "#imageHelp-viewer" ).dialog("open");
  			 			
  			// adds to the url tab1 to set tab1 as selected
  			var url=String(window.location); 			 			
  			if(url.search("#tab")==-1)
  			{	
  					window.location.href = url + "#tab1"
  			}*/
  			
  			Exxo.openHelpWindow($(this).attr("cat"), $(this).attr("template"));
  			
  		});
  		
  		$("#imageHelp-viewer" ).dialog({
  	          height: 800,
  	          width: 890,
  	          modal: true,
  	          autoOpen: false,
  	          buttons: [ 
  	                { text: Exxo.UI.translates['closeButton'],  
  	                  class:  "dialog-close",
  	                  click: function() {
  	                      //$( this ).dialog( "close" );                   
  	                  }
  	              }
  	          ]
  	        });
  		var buttons = $('.ui-dialog-buttonset').children('button');
  		// remove default color class that overwrite custom css
  		buttons.removeClass('ui-state-default'); 		
  		$( ".dialog-close" ).off(); // remove event handler that adds an hover class // tried to remove only the event, but didn't work
  		$( ".dialog-close" ).click(
  				function() {
  					$("#imageHelp-viewer" ).dialog( "close" );
  				}	
  		);
      },
      successImageSelection: function(file){
         $("#wImgUpd" + file.attr('ref')).val(1);
         $('#fakeupload' + file.attr('ref')).val(file.val().split(/(\\|\/)/g).pop());
      }, 
      errorImageSelection: function(file){
         file.val("");
         $('#fakeupload' + file.attr('ref')).val("");
         $("#wImgUpd" + file.attr('ref')).val(0);
      },
      showSpinner : function() {
         $('div#ajaxFlowWait').show(40);
         //document.getElementById('errorDiv').style.display = 'none';
      },
      hideSpinner : function() {
         $('div#ajaxFlowWait').hide();
         document.getElementById('errorDiv').style.display = 'none';
      },
      openVideoPlayer: function(){
         $("body").click(function(e){
            Exxo.Skin.Create.closeVideoPlayer();
         });
         $("#video-player").show(40);
         $(".video-js")[0].player.play();
      },
      closeVideoPlayer: function() {
         $(".video-js")[0].player.pause();
         $("#video-player").hide();
         $("body").unbind( "click" );
      }
   }
};