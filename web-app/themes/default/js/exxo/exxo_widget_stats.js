/***
 @file      exxo_widget_stats.js
 @brief     Functions for the Stats Widget
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.WidgetsStats = {
	generateExxtractorsInfoChart: function(){
		$("#exxtractors-info-loading").show();
		$("#exxtractors-info-container").hide();
		$.ajax({type:'POST',        	        //
			url: Exxo.UI.urls["widget-get-detailed-data-url"],		
		    data: {object:'exxtractor', companyId:Exxo.UI.vars["company-id"], locationId:Exxo.UI.vars["location-id"]},	    
	        success: function(data) {
	        	//Exxo.hideSpinner();
	           if(data) {
	        	   /*var data = google.visualization.arrayToDataTable([
	                 ['Venues', 'Status of Venues'],
	                 ['Offline & needs upgrade',     data.offline_needs_upgrade],
	                 ['Offline & Upgraded',      data.offline_upgraded],
	                 ['Online & Upgraded',  data.online_upgraded],
	                 ['Online & needs upgrade', data.online_needs_upgrade],
	                 ['Outdated',    data.outdated],
	                 ['Unversioned',    data.unversioned]	                 
	               ]);
	
	               var options = {
	                 'width':360,
	                 'height':300,
	                 'top':0,
	                 'pieSliceText':'value',
	                 'tooltip':{
	               	  'text':'both'
	                 },
	                 'chartArea':{
	               	  left:20,
	               	  top:20,
	               	  width:'100%',
	               	  height:'100%'}
	               };
	               
	               var chart = new google.visualization.PieChart(document.getElementById('widgetExxtractorsInfo'));
	               */
	               
	               var chartData = google.visualization.arrayToDataTable([
                    ['Status', 'Online', 'Offline', { role: 'annotation' } ],
                    ['', (data.online_upgraded + data.online_needs_upgrade), (data.offline_needs_upgrade + data.offline_upgraded), '']
                  ]);

                  var options = {
                    width: 200,
                    height: 150,
                    //legend: {  position: 'top', alignment: 'center'},
                    bar: { groupWidth: '75%' }, 
                    chartArea: {  left:"13%" ,width: "50%", height: "70%" },
                    isStacked: true
                  };
	
	               var chart = new google.visualization.ColumnChart(document.getElementById('connectivityStatusContainer'));
	
	               chart.draw(chartData, options);
	               
	               /**/
	               var chartData2 = google.visualization.arrayToDataTable([
                      ['Status', 'Update', 'Outdate', { role: 'annotation' } ],
                      ['', (data.offline_upgraded + data.online_upgraded), (data.offline_needs_upgrade + data.online_needs_upgrade), '']
                    ]);

                    var options2 = {
                      width: 200,
                      height: 150,
                      //legend: { position: 'top', maxLines: 5 },
                      bar: { groupWidth: '75%' },
                      chartArea: {  left:"13%" ,width: "50%", height: "70%" },
                      isStacked: true
                    };
  	
  	               var chart2 = new google.visualization.ColumnChart(document.getElementById('softwareVersionStatusContainer'));
  	
  	               chart2.draw(chartData2, options2);
  	               
  	               $("#unsupported-div").html(""+data.outdated);
  	               $("#unversioned-div").html(""+data.unversioned);
  	               
  	             $("#exxtractors-info-loading").hide();
  	             $("#exxtractors-info-container").show();
	           }
	        },
	        error: function(xhr, textStatus, error){
		       	alert("error: " + textStatus);
				$("#exxtractors-info-container").hide();
	        }
		 });
	},
	initExxtractorsInfoWidget: function(){
		$( "#exxtractors-info-widget" ).exxoWidget(
				{
					helpMsg:			Exxo.UI.translates['widget-stats-info-msg'],
					enableAutoRefresh:	true,
					enableConfigWindow:	true
				}
		);
		Exxo.WidgetsStats.generateExxtractorsInfoChart();
		
		Exxo.WidgetsStats.startAutoRefreshIfMust();
		
		$('#exxtractors-info-widget .widget-config-window').dialog({
	          height: 300,
	          width: 400,
	          modal: true,
	          autoOpen: false,
	          buttons: [ 
					{
						text: Exxo.UI.translates['saveButton'],
						"class": 'mainModalWindowBtn',
						click: function() {
							Exxo.UserDashboard.saveWidgetConfig();
						}
					},
	                { text: Exxo.UI.translates['closeButton'],
	                  click: function() {
	                	  $('.widget-config-window').dialog("close");
	                  }
	                }
	                
	          ]
	    });
	},
	openConfigWindow: function(widget){
		var widgetId = widget.attr("id");
		var autoRefreshStatus = widget.attr("refreshStatus");
		var autoRefreshTime = widget.attr("refreshTime");
		autoRefreshTime = (autoRefreshTime < 3)? 3:autoRefreshTime;
		$("#" + widgetId + "-refreshStatus").prop('checked', (autoRefreshStatus==1));
		$("#" + widgetId + "-refreshTime").val(autoRefreshTime);		
		$('#' + widgetId + '-conf').dialog('open');
	}, // End of openConfigWindow
	saveWidgetConfig: function(widgetId){
		var widget = $("#"+widgetId);
		clearInterval(Exxo.UI.vars["exxsStatusInterval"]);
		widget.attr("refreshTime", $("#" + widgetId + "-refreshTime").val());
		widget.attr("refreshStatus", ($("#" + widgetId + "-refreshStatus").is(':checked')? 1:0));
		Exxo.WidgetsStats.startAutoRefreshIfMust();
	},
	startAutoRefreshIfMust: function(){
		var autoRefreshStatus = $( "#exxtractors-info-widget" ).attr("refreshStatus");
		var autoRefreshTime = $( "#exxtractors-info-widget" ).attr("refreshTime");
		if(autoRefreshStatus == 1){
			var exxsStatusInterval = setInterval(function(){ Exxo.UserDashboard.refreshWidget("exxtractors-info-widget"); }, autoRefreshTime * 60 * 1000);
			Exxo.UI.vars["exxsStatusInterval"] = exxsStatusInterval;
		}
	}
};