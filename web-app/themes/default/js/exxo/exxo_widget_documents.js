/***
 @file      exxo_widget_documents.js
 @brief     Functions for the Documents List Widget
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.WidgetsDocuments = {
	initDocumentsListWidget: function(){
		
		$( "#documents-widget" ).exxoWidget({
			helpMsg:	Exxo.UI.translates['widget-documents-list-info-msg'],
			referenceIcon: 	'list',
			referenceUrl: 	Exxo.UI.urls['documents-list']});
		
		Exxo.UI.vars["widgetDocumentsTable"] = $('#widgetDocumentsTable').dataTable( {

			"pagingType": "full", // More info at: https://datatables.net/examples/basic_init/alt_pagination.html
			"lengthMenu": [[5], [5]], // More info at: https://datatables.net/examples/advanced_init/length_menu.html
       	 	"processing": true,
            "serverSide": true,
            "ajax": {
            	"url": Exxo.UI.urls["widget-get-data-url"],
    			"data": function ( d ) {
    				   d.object = 'document';
    				   d.companyId = Exxo.UI.vars["company-id"];
       		           d.locationId = Exxo.UI.vars["location-id"];
    		    }
            },
            "columnDefs": [
					{
						"targets" : [ 0 ],
						"render" : function(data, type, row) {
							return '<a href="'
								+ Exxo.UI.urls["documents-edit-url"]
								+ row[2] + '">' + row[0]
								+ '</a>';
						}
					},
 		          {
 		              "targets": [ 1 ],
 		              "render": function ( data, type, row ) {
 		            	  return row[1];
 		              }
 		          },
					{
						"targets" : [ 2 ],
						"render" : function(data, type, row) {
							return row[3];
						}
					}]
		});
	}
};