/***
 @file      exxo_document.js
 @brief     Custom functions and events for the Company Create Page.
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Document = {
    Create : {
       init : function() {
    	   
    	   $(".view-imageHelp").click(function(e){
    			e.preventDefault();
    			 			
    			$( "#imageHelp-viewer" ).dialog("open");
    			 			
    			// adds to the url tab1 to set tab1 as selected
    			var url=String(window.location); 			 			
    			if(url.search("#tab")==-1)
    			{	
    					window.location.href = url + "#tab1"
    			}
    			
    		});
    		
    		$("#imageHelp-viewer" ).dialog({
    	          height: 800,
    	          width: 890,
    	          modal: true,
    	          autoOpen: false,
    	          buttons: [ 
    	                { text: Exxo.UI.translates['closeButton'],  
    	                  class:  "dialog-close",
    	                  click: function() {
    	                      //$( this ).dialog( "close" );                   
    	                  }
    	              }
    	          ]
    	        });
    		var buttons = $('.ui-dialog-buttonset').children('button');
    		// remove default color class that overwrite custom css
    		buttons.removeClass('ui-state-default'); 		
    		$( ".dialog-close" ).off(); // remove event handler that adds an hover class // tried to remove only the event, but didn't work
    		$( ".dialog-close" ).click(
    				function() {
    					$("#imageHelp-viewer" ).dialog( "close" );
    				}	
    		); 
    	   
    	   
    	   
    	  var inputFile = $('#file');
    	  
          //Init  Functions
          $("#type").select2();
          
          $("#createForm").submit(function(e){
        	  Exxo.showSpinner();
          });
          
          $("#file").change(function(e){
        	  var file = $(this);
        	  if(file[0].files[0] == null) {//Press cancel button
        		  $("#file-updated").val(0);
        		  var ref = ($(this).attr('ref') != null && $(this).attr('ref') != "undefined")? $(this).attr('ref'):"";
                  $('#fakeupload' + ref).val("");
                  e.preventDefault();
                  e.stopPropagation();
                  return false;
        	  } else {
        		  if(Exxo.Utils.Files.validateDocumentFile($(this))){//valid
                      //$('#fakeupload').val($(this).val().replace('C:\\fakepath\\', ''));
                 	  $("#file-updated").val(1);
                 	  var ref = ($(this).attr('ref') != null && $(this).attr('ref') != "undefined")? $(this).attr('ref'):"";
                      $('#fakeupload' + ref).val($(this).val().split(/(\\|\/)/g).pop());
                   } else {
                 	 $("#file-updated").val(0);
                      e.preventDefault();
                      return false;
                   }        		  
        	  }
           });
          
          $('#fakeupload').val(Exxo.UI.vars['filename']);
          
          /*** Start of events for Documents List ***/
	  		//Init PD Viewers, only for the Document List Page	  		
	  		
	  		$(".view-pdf").click(function(e){
	  			e.preventDefault();	  			
	  			/* For embed object, required browser plugin */
	  			//$("#pdf-object").attr("data", $(this).attr("href"));
	  			
	  			/* For jQuery Media Plugin 		  
	  			$("#pdf-viewer").html("");
	  			$('<a>',{
	  				id: 'pdf-viewer-link',
	  			    class: '',
	  			    title: '',
	  			    href: $(this).attr("href")
	  			}).appendTo('#pdf-viewer');
	  			
	  			$("#pdf-viewer-link").attr('href', $(this).attr("href"));
	  			$("#pdf-viewer-link").media({ width: 800, height: 480 }); */	
	  			
	  			
	  			/* For Google Docs Viewer */
	  			$("#pdf-viewer").html("");
	  			$('<a>',{
	  				id: 'pdf-viewer-link',
	  			    text: '',
	  			    title: '',
	  			    href: $(this).attr("href")
	  			}).appendTo('#pdf-viewer');
	  			
	  			$("#pdf-viewer-link").attr('href', $(this).attr("href"));
	  			$("#pdf-viewer-link").gdocsViewer({ width: 800, height: 480 });
	  			
	  			/* With Google Docs Viewer iFrame 
	  			$("#pdf-iframe").attr('src', 'https://docs.google.com/viewer?embedded=true&url='+$(this).attr("href")); */
	  			
	  			$( "#pdf-viewer" ).dialog("open");
	  		});
	  		
	  		$("#pdf-viewer" ).dialog({
	  	          height: 600,
	  	          width: 830,
	  	          modal: true,
	  	          autoOpen: false,
	  	          buttons: [ 
	  	                { text: Exxo.UI.translates['closeButton'],   
	  	                  click: function() {
	  	                      $( this ).dialog( "close" );                   
	  	                  }
	  	              }
	  	          ]
	  	        });
	  		
	  		/*** Ends of events for Documents List ***/
       }
    }
};