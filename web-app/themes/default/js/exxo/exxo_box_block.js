/*******************************************************************************
 * @file exxo_box_block.js
 * @brief Custom functions and events for the Box Block Page.
 * @author Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Box = {
	Block : {
		init : function() {
			$("#box").select2();
			
			$(".close").click(function(e){
				e.preventDefault();
				e.stopPropagation();
				$("#divError").hide();
			});
			$("#blockForm").submit(function(e){
				e.preventDefault();
				console.debug("Block server with id: " + $("#box").val());
				Exxo.showSpinner();
				$.ajax({
					url: $(this).attr("action"), 	// Url to which the request is send
					type: "POST",             		// Type of request to be send, called as method
					data: {							// Data sent to server, a set of key/value pairs (i.e. form fields and values)
						boxId: 	$("#box").val(),
						reason:	$("#reason").val()
					},
					cache: false,             		// To unable request pages to be cached
					success: function(data)   		// A function to be called if request succeeds
					{
						Exxo.hideSpinner();
						if(data.error){ //There was an error
							$("#errorMsg").html(data.msg);
							$("#divError").show();
						} else {//Upload success
							Exxo.showInfoMessage(data.msg, true, '', false);
							window.location.href = Exxo.UI.urls["black-list-page"];
						}
					},
					error: function()
					{
						Exxo.hideSpinner();
					}
				});
			});
		}
	}
};