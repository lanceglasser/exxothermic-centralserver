/***
 @file      exxo_location_show.js
 @brief     Custom functions and events for the Company Location Show Page.
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Location = {
    Show : {
       init : function() {
          //Init Images Functions
          Exxo.Tables.init();
          
          /* Move to the desire tab*/
          var tab = Exxo.Utils.Urls.getUrlParam(window.location.href.toString(), 'tab');
          if(tab != null){
             if(tab === "exx-users"){
            	 $("#boxUsersLink").trigger('click');
             } else {
            	 if(tab === "exx-content"){
                	 $("#boxContentLink").trigger('click');
                 } else {
                	 if(tab === "exx-document"){
                    	 $("#boxDocumentsLink").trigger('click');
                     } else {
                     }
                 }
             }
          }
          
          $("#view-video").click(function(e){
             Exxo.Location.Show.openVideoPlayer();
             return false;
          });          
                   
          $("#video-container").html($("#video").html());
          $("#video").html("");
          
          $("#contentOrderedList tr").hover(function() {
  		      $(this.cells[0]).find('div').removeClass('hide');
	  		}, function() {
	  		      $(this.cells[0]).find('div').addClass('hide');
	  		});
          //$(this).find('div').toggleClass('hide');
          $(".up,.down").click(function(){
              var row = $(this).parents("tr:first");
              if ($(this).is(".up")) {
            	  row.toggleClass("movedRow");            	  
                  row.insertBefore(row.prev());
                  setTimeout(function(){                	  
                	  row.toggleClass("movedRow");
                   },1000);
                  
              } else if ($(this).is(".down")) {
            	  row.toggleClass("movedRow");
                  row.insertAfter(row.next());
                  setTimeout(function(){                	  
                	  row.toggleClass("movedRow");
                   },1000);
              }
              Exxo.Location.Show.reDrawRows();
          });
            $("#contentOrderedList").mouseup(function(){
            	setTimeout(function(){                	  
            		Exxo.Location.Show.reDrawRows();
                 },500);
            	
            });
          
          /***********save contents order************/
        
          
          $('#contentsOrder').click(function(e){	
        	  var IDs = new Object();
        	  var count = 0;
        	  $('#contentOrderedList > tbody  > tr').each(function(i, v) 
              		{
              		  IDs[i] = $(this).attr('contentId');
              		  count++;      		  
              		});
        	  
              var ContentsIDs = JSON.stringify(IDs);
              
        		Exxo.showSpinner();
        		 $.ajax({type:'POST',        	        //
        	        url: Exxo.UI.urls['baseURL'] + '/location/SaveContentsOrder',        	        
        	        data: {contents:ContentsIDs, count:count, locationId:Exxo.locationID},
        	        success: function(data) {
        	        	Exxo.hideSpinner();
        	           if(data.error){
        	              //There was an error
        	        	   	var messageFail="";
    	        	   		messageFail += "<div id='containerFailMessage'>";
		       	        	messageFail += "<div id='ready' class='alert alert-error'>";
		       	        	messageFail += "<a class='close' data-dismiss='alert'>×</a>";
		       	        	messageFail += "<div id='alert-errorMessage'>";
		       	        	messageFail += data.textStatus;
		       	        	messageFail += "</div>";
		       	        	messageFail += "</div>";
		       	        	messageFail += "</div>";
		       	        	$("#containerFailMessage").remove();
		       	        	$("#containerSuccessMessage").remove();
		       	        	$("#locationTitle" ).after( messageFail );
	       	        	   	
       	        	   $( "#locationTitle" ).after( messageSuccess );
        	           } else {//There's no error
        	        	   	var messageSuccess="";
        	        	   	messageSuccess += "<div id='containerSuccessMessage'>";
        	        	   	messageSuccess += "<div id='ready' class='alert alert-success'>";
        	        	   	messageSuccess += "<a class='close' data-dismiss='alert'>×</a>";
        	        	   	messageSuccess += "<div id='alert-sucessMessage'>";
        	        	   	messageSuccess += data.textStatus;
        	        	   	messageSuccess += "</div>";
        	        	   	messageSuccess += "</div>";
        	        	   	messageSuccess += "</div>";
        	        	   	$("#containerSuccessMessage").remove();
        	        	   	$("#containerFailMessage").remove();
        	        	   $( "#locationTitle" ).after( messageSuccess );
        	        	   
        	           }
        	        	
        	        },
        	        error: function(xhr, textStatus, error){
	        	       	Exxo.hideSpinner();
	        	       	$("#containerFailMessage").remove();
	        	       	$("#containerSuccessMessage").remove();
		        	   	var messageFail="";
	        	   		messageFail += "<div id='containerFailMessage'>";
	       	        	messageFail += "<div id='ready' class='alert alert-error'>";
	       	        	messageFail += "<a class='close' data-dismiss='alert'>×</a>";
	       	        	messageFail += "<div id='alert-errorMessage'>";
	       	        	messageFail += textStatus;
	       	        	messageFail += "</div>";
	       	        	messageFail += "</div>";
	       	        	messageFail += "</div>";
	       	        	$("#locationTitle" ).after( messageFail );
        	        }
        		 });
        	});// end click event
          /***********end save contents order*********/
          
          /* */
          Exxo.UI.vars['stopEvent'] = true;
   	   		Exxo.UI.vars['target'] = true;
          $(".delete").click(function(e){
 			 if (Exxo.UI.isSafari()) {
 				 Exxo.UI.vars['target'] = $(this);
 	       	 } else {
 	       		 Exxo.UI.vars['target'] = e.target;
 	       	 }
            
            if (Exxo.UI.vars['stopEvent']){
                e.preventDefault();             
                $( "#confirm-delete-dialog" ).dialog("open");            
            } else {
  				Exxo.showSpinner();
  				location.href = $(this).attr("href");
  			}
        });
 		
 		$("#confirm-delete-dialog" ).dialog({
 	          height: 180,
 	          width: 360,
 	          modal: true,
 	          autoOpen: false,
 	          buttons: [ {
 	                    text: Exxo.UI.translates['okButton'], 
 	                    click: function() {
 	                        $( this ).dialog( "close" );    
 	                        Exxo.UI.vars['stopEvent'] = false;                      
 	                        Exxo.UI.vars['target'].click();
 	                    }
 	                   },
 	                { text: Exxo.UI.translates['cancelButton'],   
 	                  click: function() {
 	                      $( this ).dialog( "close" );                   
 	                  }
 	              }
 	          ]
 	        });
 		
          /* Delete Exxtractor User*/
          	Exxo.UI.vars['stopEventDeleteExxUser'] = true;
   			Exxo.UI.vars['targetDeleteExxUser'] = true;
   	   	   
   			$(".deleteExxtractorUser").click(function(e){
   				if (Exxo.UI.isSafari()) {
   					Exxo.UI.vars['targetDeleteExxUser'] = $(this);
   	       	 	} else {
   	       	 		Exxo.UI.vars['targetDeleteExxUser'] = e.target;
   	       	 	}
   				
   				if (Exxo.UI.vars['stopEventDeleteExxUser']){
                  e.preventDefault();
                  $( "#confirm-delete-exxtractor-user-dialog" ).dialog("open");
   				} else {
   					Exxo.showSpinner();
   					location.href = $(this).attr("href");
   				}
   			});
   		
   			$("#confirm-delete-exxtractor-user-dialog" ).dialog({
   	          height: 180,
   	          width: 360,
   	          modal: true,
   	          autoOpen: false,
   	          buttons: [ {
   	                    text: Exxo.UI.translates['okButton'], 
   	                    click: function() {
   	                        $( this ).dialog( "close" );    
   	                        Exxo.UI.vars['stopEventDeleteExxUser'] = false;                      
   	                        Exxo.UI.vars['targetDeleteExxUser'].click();
   	                    }
   	                   },
   	                { text: Exxo.UI.translates['cancelButton'],   
   	                  click: function() {
   	                      $( this ).dialog( "close" );                   
   	                  }
   	              }
   	          ]
   	        });
          /// End of Delete ExXtractor User ///
   			
   			/* Unassign Content of Location */
          	Exxo.UI.vars['stopEventUnassignContent'] = true;
   			Exxo.UI.vars['targetUnassignContent'] = true;
   	   	   
   			$(".unassign").click(function(e){
   				if (Exxo.UI.isSafari()) {
   					Exxo.UI.vars['targetUnassignContent'] = $(this);
   	       	 	} else {
   	       	 		Exxo.UI.vars['targetUnassignContent'] = e.target;
   	       	 	}
   				
   				if (Exxo.UI.vars['stopEventUnassignContent']){
                  e.preventDefault();
                  $( "#confirm-unassign-dialog" ).dialog("open");
   				}  else {
   					Exxo.showSpinner();
   					location.href = $(this).attr("href");
   				}
   			});
   		
   			$("#confirm-unassign-dialog" ).dialog({
   	          height: 180,
   	          width: 360,
   	          modal: true,
   	          autoOpen: false,
   	          buttons: [ {
   	                    text: Exxo.UI.translates['okButton'], 
   	                    click: function() {
   	                        $( this ).dialog( "close" );    
   	                        Exxo.UI.vars['stopEventUnassignContent'] = false;                      
   	                        Exxo.UI.vars['targetUnassignContent'].click();
   	                    }
   	                   },
   	                { text: Exxo.UI.translates['cancelButton'],   
   	                  click: function() {
   	                      $( this ).dialog( "close" );                   
   	                  }
   	              }
   	          ]
   	        });
          /// End of Unassign dialog///
          
       },
       openVideoPlayer: function(){
           $("body").click(function(e){
              Exxo.Location.Show.closeVideoPlayer();
           });
           $("#video-player").show(40);
           $(".video-js")[0].player.play();
        },
        reDrawRows: function(){
        	$('#contentOrderedList > tbody  > tr').each(function(i, v) 
             {              		  
        		//${(i % 2) == 0 ? 'even' : 'odd'
        		$(this).removeClass('even');
        		$(this).removeClass('odd');
        		if(i % 2 == 0)
        		{
        			$(this).addClass('odd');		
        		}else{
        			$(this).addClass('even');
        		}              		         		  
             });// end of for each
         },
       showSpinner: function() {
    	   $('#ajaxFlowWaitImg').show(40);
    	   Exxo.showSpinner();
       },
       hideSpinner: function() {
    	   $('#ajaxFlowWaitImg').hide();
    	   Exxo.hideSpinner();
        }
    }
};