/***
 @file      exxo_app_skin_list.js
 @brief     General functions for the App Skin list pages
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.ExxtractorsUsers = {
	createInit: function(){
		Exxo.ExxtractorsUsers.generalInit();
		Exxo.ExxtractorsUsers.init();
	},
	editInit: function(){
		Exxo.ExxtractorsUsers.generalInit();
		$("#username").attr("readOnly", true);
		$("#confirmUserPassword").removeAttr('required');
		$("#userPassword").removeAttr('required');
		$("#location").select2("readonly", true);
		$("#edit-password-note").show();
	},
	generalInit: function(){
		$("#userType").select2();
		$("#location").select2();
		$("#formCreate").submit(function(e){
			console.debug("*** Form Submitted ***");
			e.preventDefault();
			console.debug("Will try to create the user");
		    Exxo.ExxtractorsUsers.createUser();
		});
	},
	init : function() {
		
	   //Init form dialog panel
	   $("#user-form" ).dialog({
          height: 330,
          width: 600,
          modal: true,
          autoOpen: false,
          buttons: [ {
                    text: Exxo.UI.translates['saveButton'], 
                    click: function() {
                       $("#formCreate").submit();
                    }
                   },
                { text: Exxo.UI.translates['cancelButton'],   
                  click: function() {
                      $( this ).dialog( "close" );                   
                  }
              }
          ]
        });
	   
	   
	   
	   $("#createBtn").click(function(e){
	      e.preventDefault();
	      Exxo.ExxtractorsUsers.cleanUserForm();
	      $( "#user-form" ).dialog("open");
	   });
	   
	   $("#syncBtn").click(function(e){
	      e.preventDefault();
	      Exxo.showSpinner();
	      location.href = Exxo.UI.urls["sync"];
	   });
	   
	   $(".edit").click(function(e){
	      e.preventDefault();
	      Exxo.ExxtractorsUsers.loadUserForm($(this));
	      $( "#user-form" ).dialog("open");
	   });
	   //----------------------
	   
	},
	loadUserForm: function(el){
	   //Load Username and Status to the Form
	   var id = el.attr("ref");
	   $("#edit-password-note").show();
	   $("#username").val($("#username-"+id).html());
	   $("#username").attr("readOnly", true);
	   //$("#enable").prop('checked', ($("#status-"+id).val() == true));
	   $("#userType").val( $("#type-"+id).val() );
	   $("#userType").change();
	   $("#userId").val(id);
       $("#userPassword").val("");
       $("#confirmUserPassword").val("");
	},
	cleanUserForm: function(){
	   Exxo.Validation.hideError();
	   $("#edit-password-note").hide();
	   $("#username").val("");
	   $("#username").attr("readOnly", false);
	   $("#userPassword").val("");
	   $("#confirmUserPassword").val("");
	   $("#userType").val( 1 );
	   $("#userType").change();
	   //$("#enable").prop('checked', true);
	   $("#userId").val(0);
	},
	createUser: function(){
	   console.debug("*** Create User ***");
	   if(Exxo.ExxtractorsUsers.validateUserFields()){//Validates the fields values
	      //If the values are correct; request for the creation
	      Exxo.showSpinner();
          Exxo.UI.vars['locationId'] = $("#location").val();
          $.ajax({type:'POST',
                data:'u=' + $("#username").val().toLowerCase() + '&p='+$("#userPassword").val() + "&locationId=" + Exxo.UI.vars['locationId']
                   + "&ena=true&uId=" + $("#userId").val() + "&t=" + $("#userType").val(),                                  
                url: $("#formCreate").attr('action'), 
                success:function(data) {
                   if(data.error === false){
                	   var syncUrl = Exxo.UI.urls["exxtractor-users-with-sync"];
                	   if(syncUrl.indexOf("replaceLocationId") > -1){
                		   syncUrl = syncUrl.replace("replaceLocationId", Exxo.UI.vars['locationId']);
                	   }                		   
                      location.href = syncUrl;
                   } else {
                      //There was an error
                	   console.debug("Data: " + data + ", error: " + data.error +  ", msg: " + data.msg + ", o: " + data['msg']);
                      Exxo.Validation.showError(data.msg);
                      Exxo.hideSpinner();
                   }
                  },
                error:function(XMLHttpRequest,textStatus,errorThrown){                              
                     Exxo.hideSpinner();
                     Exxo.showMessageError(errorThrown, true);
                }
           });
	   }
    },
    validateUserFields: function() {
       Exxo.Validation.hideError();
       if($("#userId").val() == 0){
          //It's creating a new user
          if(Exxo.Validation.NO_ERROR === Exxo.Validation.validateUsername('username')
             && Exxo.Validation.NO_ERROR === Exxo.Validation.validatePasswordWithConfirmation(
                   'userPassword','confirmUserPassword',Exxo.Validation.EXXTRACTORS_USER_PASSWORD)){
             return true;
          } else {
             //There was an error with the validation
             return false;
          }
       } else {
          //It's editing a user
          if($("#userPassword").val() != "" || $("#confirmUserPassword").val() != ""){
             //It's changing the password, must validate
             if(Exxo.Validation.NO_ERROR === Exxo.Validation.validateUsername('username')
                && Exxo.Validation.NO_ERROR === Exxo.Validation.validatePasswordWithConfirmation(
                      'userPassword','confirmUserPassword',Exxo.Validation.EXXTRACTORS_USER_PASSWORD)){
                return true;
             } else {
                //There was an error with the validation
                return false;
             }
          } else {
             //It's not changing the password, nothing to validate
             return true;
          }
       }       
    }
};