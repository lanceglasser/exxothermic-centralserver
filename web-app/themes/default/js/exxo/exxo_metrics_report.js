/***
 @file      exxo_metrics_report.js
 @brief     Custom functions and events of the page for the generation of ExXtractors Usage Reports
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Metrics = {
   Report : {
       init : function() {
    	   
    	// Simulates PHP's date function
    	 Date.prototype.format=function(e){var t="";var n=Date.replaceChars;for(var r=0;r<e.length;r++){var i=e.charAt(r);if(r-1>=0&&e.charAt(r-1)=="\\"){t+=i}else if(n[i]){t+=n[i].call(this)}else if(i!="\\"){t+=i}}return t};Date.replaceChars={shortMonths:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],longMonths:["January","February","March","April","May","June","July","August","September","October","November","December"],shortDays:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],longDays:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],d:function(){return(this.getDate()<10?"0":"")+this.getDate()},D:function(){return Date.replaceChars.shortDays[this.getDay()]},j:function(){return this.getDate()},l:function(){return Date.replaceChars.longDays[this.getDay()]},N:function(){return this.getDay()+1},S:function(){return this.getDate()%10==1&&this.getDate()!=11?"st":this.getDate()%10==2&&this.getDate()!=12?"nd":this.getDate()%10==3&&this.getDate()!=13?"rd":"th"},w:function(){return this.getDay()},z:function(){var e=new Date(this.getFullYear(),0,1);return Math.ceil((this-e)/864e5)},W:function(){var e=new Date(this.getFullYear(),0,1);return Math.ceil(((this-e)/864e5+e.getDay()+1)/7)},F:function(){return Date.replaceChars.longMonths[this.getMonth()]},m:function(){return(this.getMonth()<9?"0":"")+(this.getMonth()+1)},M:function(){return Date.replaceChars.shortMonths[this.getMonth()]},n:function(){return this.getMonth()+1},t:function(){var e=new Date;return(new Date(e.getFullYear(),e.getMonth(),0)).getDate()},L:function(){var e=this.getFullYear();return e%400==0||e%100!=0&&e%4==0},o:function(){var e=new Date(this.valueOf());e.setDate(e.getDate()-(this.getDay()+6)%7+3);return e.getFullYear()},Y:function(){return this.getFullYear()},y:function(){return(""+this.getFullYear()).substr(2)},a:function(){return this.getHours()<12?"am":"pm"},A:function(){return this.getHours()<12?"AM":"PM"},B:function(){return Math.floor(((this.getUTCHours()+1)%24+this.getUTCMinutes()/60+this.getUTCSeconds()/3600)*1e3/24)},g:function(){return this.getHours()%12||12},G:function(){return this.getHours()},h:function(){return((this.getHours()%12||12)<10?"0":"")+(this.getHours()%12||12)},H:function(){return(this.getHours()<10?"0":"")+this.getHours()},i:function(){return(this.getMinutes()<10?"0":"")+this.getMinutes()},s:function(){return(this.getSeconds()<10?"0":"")+this.getSeconds()},u:function(){var e=this.getMilliseconds();return(e<10?"00":e<100?"0":"")+e},e:function(){return"Not Yet Supported"},I:function(){var e=null;for(var t=0;t<12;++t){var n=new Date(this.getFullYear(),t,1);var r=n.getTimezoneOffset();if(e===null)e=r;else if(r<e){e=r;break}else if(r>e)break}return this.getTimezoneOffset()==e|0},O:function(){return(-this.getTimezoneOffset()<0?"-":"+")+(Math.abs(this.getTimezoneOffset()/60)<10?"0":"")+Math.abs(this.getTimezoneOffset()/60)+"00"},P:function(){return(-this.getTimezoneOffset()<0?"-":"+")+(Math.abs(this.getTimezoneOffset()/60)<10?"0":"")+Math.abs(this.getTimezoneOffset()/60)+":00"},T:function(){var e=this.getMonth();this.setMonth(0);var t=this.toTimeString().replace(/^.+ \(?([^\)]+)\)?$/,"$1");this.setMonth(e);return t},Z:function(){return-this.getTimezoneOffset()*60},c:function(){return this.format("Y-m-d\\TH:i:sP")},r:function(){return this.toString()},U:function(){return this.getTime()/1e3}}
    	 
         $("#location").select2();         
         $("#location").change();
         
         $("#getDataForm").submit(function(e){
            e.preventDefault();
            Exxo.showSpinner();
            
            $("#result-data-container").show();
            
            if(Exxo.UI.vars["table-result"] == undefined){
            	Exxo.UI.vars["table-result"] = $('#table-result').dataTable( {
               	 	"processing": true,
                    "serverSide": true,
                    "ajax": {
                    	"url": Exxo.UI.urls["get-data-url"],
            			"data": function ( d ) {
            		        d.locationId = $("#location").val();
            				d.startDate = $("#startDate").val();
            				d.endDate = $("#endDate").val();
            		    },
            		    "error": function () {
            		    	location.reload(); 
            		    }
                    }
                } );
            	
            	Exxo.UI.vars["table-result-hour"] = $('#table-result-hour').dataTable( {
               	 	"processing": true,
                    "serverSide": true,
                    "ajax": {
                    	"url": Exxo.UI.urls["get-hour-clients-data-url"],
            			"data": function ( d ) {
            		        d.locationId = $("#location").val();
            				d.startDate = $("#startDate").val();
            				d.endDate = $("#endDate").val();
            		    },
            		    "error": function () {
            		    	location.reload(); 
            		    }
                    }
                } );
            	
            	Exxo.UI.vars["table-result-daily"] = $('#table-result-daily').dataTable( {
               	 	"processing": true,
                    "serverSide": true,
                    "ajax": {
                    	"url": Exxo.UI.urls["get-daily-clients-data-url"],
            			"data": function ( d ) {
            		        d.locationId = $("#location").val();
            				d.startDate = $("#startDate").val();
            				d.endDate = $("#endDate").val();
            		    },
            		    "error": function () {
            		    	location.reload(); 
            		    }
                    }
                } );
            	
            }
            
            Exxo.UI.vars["table-result"].api().ajax.reload(function(json){Exxo.Metrics.Report.refreshCharts()});
            Exxo.UI.vars["table-result-hour"].api().ajax.reload(function(json){Exxo.Metrics.Report.refreshCharts2()});
            Exxo.UI.vars["table-result-daily"].api().ajax.reload();
            
            return false;//Prevent all events including submit
         });
                  
         //$(".usageReportTabs").hide();
         
         $("#genFileButton").click(function(e){
        	 $(this).attr("href", 
        			 Exxo.UI.urls["gen-file-url"] + "?locationId=" + $("#location").val() +
        			 "&startDate=" + $("#startDate").val() + "&endDate=" + $("#endDate").val());
         });
         
         $('#chartsDays').change(function(e) {
        	 
        	var url = Exxo.UI.urls["get-chart-data-url"]
				+ "?locationId=" + $("#location").val()
				+ "&startDate=" + $(this).val() + "&endDate=" + $(this).val();

			$.getJSON(url, function(data){
				   try {
					   if(data.locationsCount > 0){
						   //If there is at least 1 Location must show the charts
						   var options = {
			     			    title: Exxo.UI.translates["chart-time-title"]+"\n"+Exxo.UI.translates["total-day"]+data.TotalTime,			     			    
			     			    height: 200,
			     			    width: 500,
			     			    chartArea: {  left:40,width: "70%", height: "70%" },			     			    
			     			    hAxis: { 
			   		              title: Exxo.UI.translates["hours"], 
			   		              viewWindowMode:'explicit'
			   		            },
			       			    vAxis: { 
			   		              title: "", 
			   		              viewWindowMode:'explicit',
			   		              textStyle: {color: 'black'},
			   		              viewWindow:{
			   		                max:data.maximumTimeNumber,
			   		                min:0
			   		              }
			   		            }
			    		   };
			     	   
			    		   var data1 = google.visualization.arrayToDataTable(data.chartData);
			    		   var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
			    		   chart.draw(data1, options);
			    		   
			    		   //Draw Sessions Total Chart.
			    		   var options2 = {
			     			    title: Exxo.UI.translates["chart-total-title"],			     			    
			     			    height: 200,
			     			    width: 500,
			     		        chartArea: {  left:35,width: "70%", height: "70%" },
			     			    hAxis: { 
			 		              title: Exxo.UI.translates["hours"], 
			 		              viewWindowMode:'explicit'
			 		            },
			     			    vAxis: { 
			 		              title: "", 
			 		              viewWindowMode:'explicit',
			 		              viewWindow:{
			 		                max:data.maximumTotalNumber,
			 		                min:0
			 		              }
			 		            }
			    		   };
			    		   
			    		   var data2 = google.visualization.arrayToDataTable(data.sessionsTotalData);
				 		   var chart2 = new google.visualization.LineChart(document.getElementById('chart_sessions_total_div'));
				 		   chart2.draw(data2, options2);
				 		   
				 		   //Draw Sessions Max Chart
				 		   var options3 = {
				     			    title: Exxo.UI.translates["chart-max-title"],				     			    
				     			    height: 200,
				     			    width: 500,
				     			   chartArea: {  left:35,width: "70%", height: "70%" },
				     			    hAxis: { 
				 		              title: Exxo.UI.translates["hours"], 
				 		              viewWindowMode:'explicit'
				 		            },
				     			    vAxis: { 
				 		              title: "", 
				 		              viewWindowMode:'explicit',
				 		              viewWindow:{
				 		                max:data.maximumMaxNumber,
				 		                min:0
				 		              }
				 		            }
				    		   };
				 		   var data3 = google.visualization.arrayToDataTable(data.sessionsMaxData);
				 		   var chart3 = new google.visualization.LineChart(document.getElementById('chart_sessions_max_div'));
				 		   chart3.draw(data3, options3);
			    		   
			    		   $("#chartsNoData").hide();
			    		   $("#chartsContainer").show();
			    		   $("#chartsContainerCombo1").show();
					   } else {
						   $("#chartsContainerCombo1").show();
						   //If there is not data must hide the visual components
						   $("#chartsContainer").hide();						   
						   $("#chartsNoData").show();
					   }
					   
			  }catch(Exception){                       
			  }  
			  Exxo.hideSpinner();
			});
         });
         //alert(Exxo.UI.translates["chart-hourly-title"].replace("{0}",$("#endDate").val()));
         $('#chartsDays2').change(function(e){
        	 console.debug("Charts Days select change to value: " + $(this).val());
        	 
        	 var url = Exxo.UI.urls["get-hour-clients-chart-data-url"]				
				+ "?locationId=" + $("#location").val()
				+ "&startDate=" + $("#startDate").val() + "&endDate=" + $("#endDate").val()+ "&selectedDate=" + $(this).val();
        	 

			$.getJSON(url, function(data){
				   try {
					   $("#chartsContainer2").show();
					   if(data.locationsCount > 0){
						   //If there is at least 1 Location must show the charts
						   var options = {
			     			    title: Exxo.UI.translates["chart-hourly-title"].replace("{0}",$("#chartsDays2").val()),			     			    
			     		        legend: 'true',
			     		        width: 650,
			     		        height: 400,
			     		        chartArea: {  left:35,width: "70%", height: "70%" },
			     			    hAxis: { 
			   		              title: Exxo.UI.translates["hours"], 
			   		              viewWindowMode:'explicit'			   		              
			   		            },
			       			    vAxis: { 
			   		              title: "", 
			   		              viewWindowMode:'explicit',			   		              
			   		              viewWindow:{
			   		                max:data.maximumTimeNumber,
			   		                min:0
			   		              }
			   		            }
			    		   };					
			     	   
			    		   var data1 = google.visualization.arrayToDataTable(data.data);
			    		   var chart = new google.visualization.LineChart(document.getElementById('chart_hour_clients'));
			    		   chart.draw(data1, options);
			    		   
			    		   $("#chartsNoDataHour").hide();
			    		   $("#chart_hour_clients").show();
					   } else {
						   //If there is not data must hide the visual components
						   $("#chartsNoDataHour").show();
			    		   $("#chart_hour_clients").hide();
					   }
					   
					   if(data.locationsCountDaily > 0){
						   //If there is at least 1 Location must show the charts
			    		   //Draw Sessions Total Chart.
			    		   var options2 = {
			     			    title: Exxo.UI.translates["chart-daily-title"].replace("{0}",$("#startDate").val()).replace("{1}",$("#endDate").val()),
			     			    legend: 'true',
			     		        width: 650,
			     		        height: 400,
			     		        chartArea: {  left:35,width: "70%", height: "70%" },
			     			    hAxis: { 
			 		              title: ''/*Exxo.UI.translates["days"]*/, 
			 		              viewWindowMode:'explicit', 
			 		              /*direction:-1, */
			 		              textPosition: 'out',
			 		              slantedText:true, 
			 		              slantedTextAngle:90
			 		            },
			     			    vAxis: { 
			 		              title: "", 
			 		              viewWindowMode:'explicit',
			 		              viewWindow:{
			 		                max:data.maximumDailyClientsNumber,
			 		                min:0
			 		              }
			 		            }
			    		   };
			    		   
			    		   var data2 = google.visualization.arrayToDataTable(data.dataDaily);
				 		   var chart2 = new google.visualization.LineChart(document.getElementById('chart_daily_clients'));
				 		   chart2.draw(data2, options2);
				 		   
			    		   $("#chartsNoDataDaily").hide();
			    		   $("#chart_daily_clients").show();
					   } else {
						   //If there is not data must hide the visual components
						   $("#chartsNoDataDaily").show();
			    		   $("#chart_daily_clients").hide();
					   }
					   
					   
					   
			  }catch(Exception){                       
			  }  
			  Exxo.hideSpinner();
			});
         });
         
         $("#tabs").find("li").click(function(e){
        	 //alert("Click on: " + $(this).find("a").attr("href"));
         });
       },
       StringFormat : function() {
    	    // The string containing the format items (e.g. "{0}")
    	    // will and always has to be the first argument.
    	    var theString = arguments[0];
    	    
    	    // start with the second argument (i = 1)
    	    for (var i = 1; i < arguments.length; i++) {
    	        // "gm" = RegEx options for Global search (more than one instance)
    	        // and for Multiline search
    	        var regEx = new RegExp("\\{" + (i - 1) + "\\}", "gm");
    	        theString = theString.replace(regEx, arguments[i]);
    	    }
    	    
    	    return theString;
    	},
       refreshCharts: function(){
    	   $("#tableContainer").show();
    	   //$("#chartsContainerCombo1").show();
    	   //$("#tableContainer2").show();
    	   
    	   	var startDate = new Date( $("#startDate").val() ),
    	    endDate = new Date( $("#endDate").val() ),
    	    currentDate = startDate,
    	    between = [];
    	   	
    	   	$('#chartsDays')[0].options.length = 0;
    	   	$('#chartsDays2')[0].options.length = 0;
    	   	
	    	while (currentDate <= endDate) {
	    	    //between.push(new Date(currentDate).format('m/d/Y'));
	    	    var value = new Date(currentDate).format('m/d/Y');
	    	    $('#chartsDays').append($('<option>', { value : value }).text(value));
	    	    $('#chartsDays2').append($('<option>', { value : value }).text(value));
	    	    currentDate.setDate(currentDate.getDate() + 1);
	    	}
	    	
	    	$('#chartsDays').select2();
	    	$('#chartsDays').change();
	    	//$('#chartsDays2').select2();
	    	//$('#chartsDays2').change();
	    	
	    	$("#tabs").show();				
	    	//$(".usageReportTabs").show();
	    	$("#chartsContainer").show();
	    	//$("#mainTab").click();
	    	Exxo.Metrics.Report.refreshGeneralCharts();
	    	
       }, //End of refreshCharts
       refreshGeneralCharts: function() {
    	   	/*		
    	   var url = Exxo.UI.urls["get-range-chart-data-url"]
			+ "?locationId=" + $("#location").val()
			+ "&startDate=" + $("#startDate").val() + "&endDate=" + $("#endDate").val()

			$.getJSON(url, function(data){
				   try {
					   if(data.locationsCount > 0) {
						   console.debug("There are locations!!!");
						   //If there is at least 1 Location must show the charts
						   var options = {
			     			    title: Exxo.UI.translates["chart-time-title"],
			     			    legend: { position: 'bottom' },
			     			    height: 200,
			     			    hAxis: { 
			   		              title: Exxo.UI.translates["hours"], 
			   		              viewWindowMode:'explicit',
				 		          format:'0'
			   		            },
			       			    vAxis: { 
			   		              title: "", 
			   		              viewWindowMode:'explicit',
				 		          format:'0',
			   		              viewWindow:{
			   		                max:data.maximumTimeNumber,
			   		                min:0
			   		              }
			   		            }
			    		   };
			     	   
			    		   var data1 = google.visualization.arrayToDataTable(data.chartData);
			    		   var chart = new google.visualization.LineChart(document.getElementById('main_chart_div'));
			    		   chart.draw(data1, options);
			    		   
			    		   //$("#chartsNoData").hide();
			    		   //$("#chartsContainer2").show();
					   } else {
						   //If there is not data must hide the visual components
						   //$("#chartsContainer2").hide();
						   //$("#chartsNoData").show();
						   console.debug("No locations with data");
					   }
			  }catch(Exception){                       
			  }  
			  Exxo.hideSpinner();
			});*/
       }, // End of refreshGeneralCharts
       refreshCharts2: function(){
    	    $("#tableContainer2").show();
    	   
    	   	var startDate = new Date( $("#startDate").val() ),
    	    endDate = new Date( $("#endDate").val() ),
    	    currentDate = startDate,
    	    between = [];
    	   	
    	   	$('#chartsDays2')[0].options.length = 0;
    	   	
	    	while (currentDate <= endDate) {
	    	    //between.push(new Date(currentDate).format('m/d/Y'));
	    	    var value = new Date(currentDate).format('m/d/Y');
	    	    $('#chartsDays2').append($('<option>', { value : value }).text(value));
	    	    currentDate.setDate(currentDate.getDate() + 1);
	    	}
	    	
	    	$('#chartsDays2').select2();
	    	$('#chartsDays2').change();
	    	
	    	//$("#tabs").show();				
	    	//$(".usageReportTabs").show();    
	    	$("#chartsContainer2").show(); 	
	    	
       },
   }
};