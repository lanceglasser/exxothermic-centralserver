/***
 @file      exxo_metrics_report.js
 @brief     Custom functions and events of the page for the generation of ExXtractors Usage Reports
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Software = {
   Job : {
	   show: function() {
		   $('#table-list').dataTable();
		   
		   $("#steps-details").dialog({
	 			height : 400,
	 			width : 600,
	 			modal : true,
	 			autoOpen : false,
	 			buttons : [ {
	 				text : Exxo.UI.translates['okButton'],
	 				click : function() {
	 					$(this).dialog("close");
	 				}
	 			}]
	 		});
		 
		   $(".view-details").click(function(e){
			   e.preventDefault();
			   Exxo.Software.Job.getStepsDetail($(this).attr('href'));
			   return false;
		   });
	   },
	   getStepsDetail: function(url){
		   $('#steps-tbody-list').html("");
		   
		   Exxo.showSpinner();
		   
		   $.getJSON(url, function(data){
				try {
					Exxo.hideSpinner();
					if(data.error == false){
						for(var i=0; i < data.steps.length; i++) {
							$('#steps-tbody-list').append('<tr role="row" class="' + (i%2==0? 'odd':'even') + '"><td style="text-align: center;">' 
									+ data.steps[i][0] + 
				    				   '</td><td>' +
				    				   data.steps[i][1] + '</td><td>' + data.steps[i][2] + '</td></tr>');
						}
						$("#serial-container").html(data.serial);
						$("#steps-details").dialog("open");
						
					} else {
						Exxo.showErrorMessage("Upps, something goes wrong; please try again", true, '',false);
					}
				}catch(Exception){                       
				}
			});
	   },
	   list: function() {
		   $('#table-list').dataTable();
	   },
       init : function() {
    	   
         $("#location").select2();         
         $("#location").change();
         
         $("#getExXtractorsButton").click(function(e){
        	 e.preventDefault();
             //Exxo.showSpinner();
             if(Exxo.UI.vars["table-result"] == undefined){
             	Exxo.UI.vars["table-result"] = $('#table-result').dataTable( {
                	 //"processing": true,
                     //"serverSide": true,
             		 //scrollY: 300,
             		"columnDefs": [
     		              {
     		                  "targets": [ 0 ],
     		                  "visible": false,
     		                  "searchable": false
     		              },
     		              {
     		                  "targets": [ 3 ],
     		                  "render": function ( data, type, row ) {
     		                	  return '<span '
     		                	  	+ ' id="serial_' + row[0] + '"' + '>' + data + '</span>';
     		                    return data;
     		                  }
     		              },
     		              {
     		                  "targets": [ 5 ],
     		                  "render": function ( data, type, row ) {
     		                	  return '<span '
     		                	  	+ ' id="tsv_' + row[0] + '"' + ' tsvId="' + row[7] + '"' + '>' + data + '</span>';
     		                    return data;
     		                  }
     		              },
     		              {
     		                  "targets": [ 6 ],
     		                  "render": function ( data, type, row ) {
     		                	  return '<input type="checkbox" class="boxUpdateCB"'
     		                	  	+ ' boxId="' + row[0] + '"' + ' value="'+ data + '">';
     		                    return data;
     		                  },
     		                  "orderable": false
     		              },
     		              {
     		                  "targets": [ 7 ],
     		                  "visible": false,
     		                  "searchable": false
     		              }
     		         ],
     		         "order": [[ 3, "asc" ]],
             	     paging: false,
             	     searching: false,
                     "ajax": {
                     	"url": Exxo.UI.urls["get-exxtractors-url"],
             			"data": function ( d ) {
             		        d.locationId = $("#location").val();
             		    }
                     }
                 } );
             } else {
            	 Exxo.UI.vars["table-result"].api().ajax.reload(); 
             }
             $("#tableContainer").show();
             
             //function(json){Exxo.Metrics.Report.refreshCharts()}
             return false;//Prevent all events including submit
         });
         
         $("#createJobButton").click(function(e){
        	 e.preventDefault();
        	 var selectedExx = new Array();
        	 var selectedExxSerials = new Array();
        	 var selectedExxTargetLabel = new Array();
        	 //var selectedExxDetails = new Array();
        	 $('.boxUpdateCB').each(function(index, obj) {
        		 if($(obj).is(':checked')){
        			 var boxId = $(this).attr("boxId");
        			 selectedExx.push(boxId + "-" + $("#tsv_" + boxId).attr("tsvId") );
        			 selectedExxSerials.push($("#serial_" + boxId).html());
        			 selectedExxTargetLabel.push($("#tsv_"+boxId).html());
        			 //selectedExxDetails.push('<tr><td>' + $("#serial_" + boxId).html() + '</td><td>' + $("#tsv_" + boxId).attr("tsvId") + '</td></tr>' );
        		 }
        	 });
        	 if(selectedExx.length == 0) {
        		 //Shows error since needs at least one exxtractor to upgrade
        		 Exxo.showErrorMessage(Exxo.UI.translates['at-least-one-error'], true, '', false);
        	 } else {
        		 Exxo.UI.vars['selected-exxs'] = selectedExx;
        		 Exxo.UI.vars['selected-exxs-serials'] = selectedExxSerials;
        		 Exxo.UI.vars['selected-exxs-targets'] = selectedExxTargetLabel;
        		 Exxo.Software.Job.confirmJob();
        	 }
        	 //alert("Selected: \n<table>" + selectedExxDetails + '</table>');
        	 return false;
         });
         
         $("#confirm-job").dialog({
 			height : 400,
 			width : 450,
 			modal : true,
 			autoOpen : false,
 			buttons : [ {
 				text : Exxo.UI.translates['okButton'],
 				click : function() {
 					$(this).dialog("close");
 					Exxo.Software.Job.createJob();
 				}
 			}, {
 				text : Exxo.UI.translates['cancelButton'],
 				click : function() {
 					$(this).dialog("close");
 				}
 			} ]
 		});
       },
       confirmJob: function(selectedExx){
    	   $('#tbody-list').html("");
    	   for(var i=0; i < Exxo.UI.vars['selected-exxs'].length; i++) {
    		   $('#tbody-list').append('<tr role="row" class="' + (i%2==0? 'odd':'even') + '"><td>' + Exxo.UI.vars['selected-exxs-serials'][i] + 
    				   '</td><td style="text-align: center;">' +
    				   Exxo.UI.vars['selected-exxs-targets'][i] + '</td></tr>');
    	   }
    	   
    	   $("#confirm-job").dialog("open");
       },
       createJob: function() {
    	   
    	   Exxo.showSpinner();
    	   
    	   var url = Exxo.UI.urls["create-job-url"];

			$.getJSON(url, {boxes: JSON.stringify(Exxo.UI.vars['selected-exxs']), desc: 'myDesc'}, function(data){
				try {
					Exxo.hideSpinner();
					if(data.error == false){
						//Exxo.showInfoMessage("The Job was created; you can review the status of the Job at the details page", true, '', false);
						location.href = Exxo.UI.urls["job-show-url"] + "/"+ data.jobId;
					} else {
						Exxo.showErrorMessage("Upps, something goes wrong; please try again", true, '',false);
					}  
				}catch(Exception){                       
				}
			});
       }
       
       
   }
};