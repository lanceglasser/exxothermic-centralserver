/***
 @file      exxo_custom_button_assign.js
 @brief     Custom functions and events for the Custom Button Assign Page.
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Button = {
    Assign : {
    	firstSelection: true,
    	clearScrean: function(){
    		if(!Exxo.Button.Assign.firstSelection){
    			$(".alert").hide();
    			$("#syncResultContainer").hide();
    		}
    		Exxo.Button.Assign.firstSelection = false;
    		Exxo.showSpinner();
    	},
       init : function() {
    	  console.debug("-> Exxo.Button.Assign.init <-");
    	  Exxo.hideSpinner();
    	  
          Exxo.Tables.init();
         
          $("#buttons").change(function(e){
             var id = $("#buttons").find(":selected").val();
             $("#customButtonId").val(id);
         });
                  
         if(Exxo.UI.vars['defaultButtonId'] != 0 ){
             $('#buttons').val(Exxo.UI.vars['defaultButtonId']);
         } else {
        	 $("#buttons option")
        	    .removeAttr('selected')
        	    .find(':first')     //you can also use .find('[value=MyVal]')
        	        .attr('selected','selected');
         }
         
         $("#buttons").select2();  
         //$("#buttons").val(Exxo.UI.vars['defaultButtonId']);
         $("#buttons").trigger('change');
                  
         $("#createForm").submit(function(e){
        	 Exxo.showSpinner();
             var locationsIds = "";
             $('.cbLocation:checked').each(function () {
                 locationsIds += (locationsIds === "")? $(this).attr("locationId"):"," + $(this).attr("locationId");
               });
             $("#locationsToSave").val(locationsIds);
         });
         
         $("#syncForm").submit(function(e){
        	 console.debug("syncForm submitted");
        	 Exxo.showSpinner();
         });
         
         
        /* $("#btnSyncronize").click(function(e){
             var id = $("#buttons").find(":selected").val();
             var url   =  "syncronizeCustomButtons" + "?id=" + id;  //$("a[name='urlLinkToSyncronize']").attr("href")+ "?id=" + id; 
             var i = 0;
             var error = new Array();
             var ready = new Array();
             
             $.getJSON(url, function(data)
             {   
                 if(data.successful)
                 {
                     ready.push(id)
                     //$("tr#"+id+ " input[name='oldlabel']").val(name);
                     //$("tr#"+id+ " input[name='oldpa']").prop('checked', pa);
                 }
                 else
                 {
                     error.push(id)
                 }                     
             });
         });*/
         
         /*function getCustomButtonId() {
             var id = $("#buttons").find(":selected").val();
             return id;
         }*/
       }
    }
};