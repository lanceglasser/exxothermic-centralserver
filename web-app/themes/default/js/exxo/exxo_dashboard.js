/***
 @file      exxo_dashboard.js
 @brief     General functions for the dashboard layout as the logout and menu functions.
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Dashboard = {
	init : function() {
		Exxo.Dashboard.initButtons();
		
		/*$('#slides').superslides({
		      animation: 'fade',
		      play: false
		    });*/
		
		$('.skin-square input').iCheck({
	        checkboxClass: 'icheckbox_square-orange',
	        increaseArea: '20%'
	      });
		$("#e1").select2();
	      $("#e2").select2(); 
	      $("#e3").select2(); 
	},
	initButtons: function(){

       // on click logout button: redirect to logout url
       $("#btn-logout").click(function() {
          window.location.href = Exxo.UI.urls['logout'];
       });
       
       $("#btn-homepage").click(function() {
           window.location.href = Exxo.UI.urls['baseURL'];
        });
       
       $("#btn-my-account").click(function(){
          window.location.href = Exxo.UI.urls["my_account_url"];
       });
       
	}
};