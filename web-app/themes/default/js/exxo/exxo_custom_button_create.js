
/***
 @file      exxo_custom_button_create.js
 @brief     Custom functions and events for the Custom Button Creation Page
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Button = {
   Create : {
	   initForEdit: function(){
		   Exxo.Button.Create.init();
		   $("#type").select2("readonly", true);
	   },
	   init : function() {
		   
         Exxo.Images.init();
         
         if ( !Exxo.UI.vars['is-offer'] ) {
        	 Exxo.Button.Create.showDefaultOptions();
        	 /*$("#affiliate").val(3);
        	 $("#affiliate").change();*/
         }
         var inputFile = $('#file');
         
         $("#file").change(function(e){

             if(Exxo.Utils.Files.validateDocumentFile($(this))){//valid
           	  $("#file-updated").val(1);
           	  var ref = ($(this).attr('ref') != null && $(this).attr('ref') != "undefined")? $(this).attr('ref'):"";
                $('#fakeupload' + ref).val($(this).val().split(/(\\|\/)/g).pop());
             } else {
           	 $("#file-updated").val(0);
                e.preventDefault();
                return false;
             }
          });
         
         $('#fakeupload').val(Exxo.UI.vars['filename']);
         
         $('#scheduler-start-time').ptTimeSelect({
        	 containerClass: "timeCntr",
        	 containerWidth: "350px",
        	 setButtonLabel: Exxo.UI.translates['select'],
        	 minutesLabel: "Min",
        	 hoursLabel: "Hrs",
        	 onClose: function(i) {
        			console.debug('onClose(event)Time selected:' + $(i).val() + "");
        			Exxo.Button.Create.displayScheduleInfo($(i).val(), $("#hoursToDisplay").val() );
        	 }
         });
         $("#hoursToDisplay").change(function(e){
        	 Exxo.Button.Create.displayScheduleInfo($('#scheduler-start-time').val(), $(this).val() );
         });
         Exxo.Button.Create.displayScheduleInfo($('#scheduler-start-time').val(), $("#hoursToDisplay").val() );
        $(".view-imageHelp").click(function(e){
   			e.preventDefault();   			
   			Exxo.openHelpWindow($(this).attr("cat"), $(this).attr("template"));
   		});
 		
 		$("#imageHelp-viewer" ).dialog({
 	          height: 800,
 	          width: 890,
 	          modal: true,
 	          autoOpen: false,
 	          buttons: [ 
 	                { text: Exxo.UI.translates['closeButton'],  
 	                  class:  "dialog-close",
 	                  click: function() {
 	                      //$( this ).dialog( "close" );                   
 	                  }
 	              }
 	          ]
 	        });
 		var buttons = $('.ui-dialog-buttonset').children('button');
 		// remove default color class that overwrite custom css
 		buttons.removeClass('ui-state-default'); 		
 		$( ".dialog-close" ).off(); // remove event handler that adds an hover class // tried to remove only the event, but didn't work
 		$( ".dialog-close" ).click(
 				function() {
 					$("#imageHelp-viewer" ).dialog( "close" );
 				}	
 		);
 		
 		function manageRadio(e)
 		{
 			if (e.val()=="URL" && e.is(":checked")) { 	           
 	        	$("#fileDiv").hide();
 				$("#filePreview").hide();
 				//if(e.val() != ""){
 					//$("#urlReference").val("");
 				//}
 				$("#file").prop('required', false);
 	        	$("#urlReferenceDiv").show();
 	        }else { 	        	
 	        	$("#urlReferenceDiv").hide();
 	        	//$("#urlReference").val($("#urlSave").val()); 	        	
 	        	$("#fileDiv").show(); 
 	        	$("#file").prop('required', true);
 	        	$("#filePreview").show(); 
 	        }
 			
 			if($('#fakeupload').val().trim()=='' && e.val()=="PDF"){
 	    		 $("#file").prop('required', true);
 	    	
 	    	 }else{        		 
 	    		 $("#file").prop('required', false);
 	    	
 	    	 } 
 		}
 		
 		
 		function manageIsFile(e)
 		{
 			if(e.is(":checked")) {
	        	$("#radioURL").hide();
	        	$("#radioLabelUrl").hide();	        	
	        	$("#radioPDF").prop('checked', true);	
	        	$("#file").prop('required', true);
	        	$("#urlReferenceDiv").hide();
 	        	$("#fileDiv").show(); 
	        }else{
	        	$("#radioURL").show();
	        	//$("#file").prop('required',false);
	        	$("#radioLabelUrl").show();
	        }
 			if($("#fakeupload").length > 0)
 	 		{ 		
 	 		if($('#fakeupload').val().trim()==''){
 	   		 $("#file").prop('required', true);
 	   	
 		   	 }else{        		 
 		   		 $("#file").prop('required', false);
 		   	 } 
 	 		}
 		}
 		
 		
 		//$("#urlDiv").hide(); 		
 		//manageRadio($("input[name=referenceType]:radio"));
 		$("input[name=referenceType]:radio").change(function () {
 	        	manageRadio($(this));
 	    });
 		
 		manageIsFile($("#isFile"));
 		
 		$("#isFile").change(function() {
	        
	        manageIsFile($(this));
	                
	    });
 		
 		if ($("#radioURL").is(":checked")) { 	           
 				$("#fileDiv").hide();
 				$("#filePreview").hide();
	        	$("#urlReferenceDiv").show();
	        	$("#file").prop('required',false);
	        }
	        else { 	        	
	        	$("#urlReferenceDiv").hide();
	        	$("#fileDiv").show();  
	        	$("#filePreview").show(); 
	        	$("#file").prop('required',true);
	        	 
	        }
 		if($("#fakeupload").length > 0 && !$("#radioURL").is(":checked"))
 		{ 		
 		if($('#fakeupload').val().trim()==''){
   		 $("#file").prop('required', true);
   	
	   	 }else{        		 
	   		 $("#file").prop('required', false);
	   	 } 
 		}
 		//$("input[name=referenceType]:radio").trigger('change');
 		
 		
         /****/
         $("#urlReference").change(
               function() {
                  if (this.value.substr(0, 7) != 'http://'
                        && this.value.substr(0, 8) != 'https://') {
                	  if(this.value.trim() == ""){
                		  this.value = "";
                	  } else {
                		  this.value = 'http://' + this.value;
                		  //this.value = encodeURI('http://' + this.value);
                	  }
                  } else {
                	  // this.value = encodeURI(this.value);
                  }
               });

         $("#color").change(function() {
            var textColor = this.value;
            var myhex = textColor.replace(/[^0-9a-fA-F]/g, '');
            this.value = "#" + myhex;

         });

         $("#createForm,#updateForm").submit(function(e) {
        	 
        	 var formData = new FormData($(this)[0]);
        	 var url = $(this).attr("action");
        	 Exxo.showSpinner();
    	    $.ajax({
    	        url: url,
    	        type: 'POST',
    	        data: formData,
    	        async: true,
    	        success: function (data) {
    	            //alert(data);
    	            if( data.error ){
    	            	$("#errorMessageContainer").html(data.msg);
    	            	$("#divError").show();
    	            	Exxo.hideSpinner();
    	            } else {
    	            	$("#errorMessageContainer").html("");
    	            	$("#divError").hide();
    	            	if(data.jsonRedirect){
    	            		window.location.replace(data.jsonRedirectUrl);
    	            	} else {
    	            		Exxo.hideSpinner();
    	            	}
    	            }
    	        },
    	        cache: false,
    	        contentType: false,
    	        processData: false
    	    });

        	 //Exxo.showSpinner();
	       	 e.preventDefault();
	       	 e.stopPropagation();
         });

         $("#type").select2({        
              allowClear: true
          });
         
         $("#affiliate").select2({        
             allowClear: false
         });
        
         $("#isDefaultOfLocation").change(function(e){
        	if(!$(this).is(":checked")) {
        		Exxo.UI.vars['clientForDefault'] = $("#affiliate").val();
        		$("#affiliate").val("null");
        		$("#affiliate").change();
        		$("#affiliate").select2("readonly", true);
        		$("#select-default-client").hide();
	        } else {
	        	$("#affiliate").val(Exxo.UI.vars['clientForDefault']);
	        	$("#affiliate").change();
	        	$("#affiliate").select2("readonly", false);
	        	$("#select-default-client").show();
	        }
         });
                  
         if(!$("#isDefaultOfLocation").is(":checked")) {
        	 $("#select-default-client").hide();
        	 $("#affiliate").select2("readonly", true);
        	 $("#affiliate").change();
         }
         
         $("#featured").change(function() {
		        if($(this).is(":checked")) {
		        	//$("#featuredImageObject").show("slow");
		        	//$("#file").prop('required',true);
		        	$("#urlReferenceDiv").show();
		        	Exxo.Button.Create.showDefaultOptions();
		        }else{
		        	 //$("#featuredImageObject").hide("slow");
		        	 //$("#file").prop('required',false);
		        	 $("#urlReferenceDiv").hide();
		        	 Exxo.Button.Create.hideDefaultOptions();
		        }
		        
		        if ($("#radioURL").is(":checked")) { 	           
	 				$("#fileDiv").hide();
	 				$("#filePreview").hide();
		        	$("#urlReferenceDiv").show();
		        }
		        else { 	        	
		        	$("#urlReferenceDiv").hide();
		        	$("#fileDiv").show();  
		        	$("#filePreview").show();  
		        }
		        
		        Exxo.AppDemo.setOfferAsBannerIfApplies();
		    });
         
         if(TYPE_SELECTED=="") {
        	 Exxo.Button.Create.typeImage();
    	 } else{
    		 Exxo.Button.Create.typeSelection(TYPE_SELECTED);
    	 }
                  
         $("#type").change(function() {
        	 Exxo.Button.Create.typeSelection($("#type").val());
         });
         
         /* Enable/Disable Autogeneration of Dialog Images */
         $("#genDialogImages").change(function(e){
        	 if($(this).is(":checked")) {
        		// If checked, hide the upload components and shows help message
   	        	$("#dialogImgsGenExample").show();
   	        	$("#dialogImagesContainer").hide();
  	        } else {
  	        	//	If unchecked must give the option to upload the specific images
          		$("#dialogImagesContainer").show();
          		$("#dialogImgsGenExample").hide();
  	        }
        	 Exxo.AppDemo.updateGenerationOptions();
         });
         
         /* Enable/Disable Autogeneration of Feature Image for Tablets */
         $("#genFeaturedImgForTablets").change(function(e){
         	if(!$(this).is(":checked")) {
         		//If uncheck must give the option to upload a specific image
         		$("#tabletsFeatureImageContainer").show();
         		$("#featureGenExampleDiv").hide();
 	        } else {
 	        	//If check, hide the upload component and shows help message
 	        	$("#tabletsFeatureImageContainer").hide();
 	        	$("#featureGenExampleDiv").show();
 	        }
          });
         
         $("#genFeaturedImgForTablets").trigger('change');
         
         /* Enable/Disable Autogeneration of Dialog Image for Tablets */
         $("#genDialogImgForTablets").change(function(e){
         	if(!$(this).is(":checked")) {
         		//If uncheck must give the option to upload a specific image
         		$("#dialogImageTabletObject").show();
         		$("#dialogGenExampleDiv").hide();
 	        } else {
 	        	//If check, hide the upload component and shows help message
 	        	$("#dialogImageTabletObject").hide();
 	        	$("#dialogGenExampleDiv").show();
 	        }
          });
         
         $("#genDialogImgForTablets").trigger('change');
         
        //Render Initial App Preview
         Exxo.AppDemo.initOfferOrBannerPreview();
         
         $("#color").change(function(){
         	Exxo.AppDemo.setBannerOrOfferColor();
         });
         
         $("#title").keyup(function(){
        	 Exxo.AppDemo.setOfferDemoTitle($(this).val());
            });
            
         $("#title").blur(function(){
        	Exxo.AppDemo.setOfferDemoTitle($(this).val());
           });
         
         $("#description").keyup(function(){
        	 Exxo.AppDemo.setOfferDemoText($(this).val());
            });
            
         $("#description").blur(function(){
        	Exxo.AppDemo.setOfferDemoText($(this).val());
           });
                     
         $("#end_date").blur(function(){
        	Exxo.AppDemo.setBannerOrOfferExpiration($(this).val());
         });
         
         $("#urlReference").keyup(function(e){
        	 Exxo.AppDemo.setOfferOrBannerMoreInfo();
         }); 
         
         $("#urlReference").blur(function(e){
        	 Exxo.AppDemo.setOfferOrBannerMoreInfo();
         });
         
         $("#radioURL, #radioPDF").click(function(e){
        	 Exxo.AppDemo.setOfferOrBannerMoreInfo();
         });
         
         $("#isFile").change(function(e){
        	 Exxo.AppDemo.setOfferOrBannerMoreInfo();
        	 Exxo.AppDemo.setOfferAsDocumentIfApplies();
         });
                  
         $(".scheduler .days li").click(function(e){
        	 if(Exxo.UI.vars['allowScheduleEdition']){
				 Exxo.AppDemo.setAvailableOptions();
        	 } else {
        		 //console.debug("Edition is not allowed");
        	 }
         });
         
         $(".hours_range .start, .hours_range .end").change(function(){
        	 Exxo.AppDemo.setAvailableOptions();
         });
         
         $("#type").change(function(e){
        	 Exxo.AppDemo.setBannerType($("#title").val());
         });
      },
      typeSelection: function(typeValue){
     	 if(typeValue==TYPE_IMAGE) {
     		 Exxo.Button.Create.typeImage();
          } else if(typeValue==TYPE_OFFER) {
         	 Exxo.Button.Create.typeOffer();
          } else if(typeValue==TYPE_MESSAGE) {
         	 Exxo.Button.Create.typeMessage();
  		 }
      },
	  typeImage: function(){
		 Exxo.Button.Create.hideDescriptionRequiredInfo();
		  
	  	 $("#featured, #file, #dialogFile").prop('required',false);
 	 
     	 $("#descriptionDiv, #titleDiv, #colorDiv, #urlReferenceDiv, #fileDiv, #featuredImageObject, #dialogImageObject, #helpDiv").show();
     	 $("#featuredDiv").hide();
     	 
     	 $("#title, #color, #dialogFile").prop('required',true);
     	 $("#featured").attr("checked", false);
	  },
	  typeOffer: function(){
		//$("#featured").attr("checked", false);
      	 
     	 $("#titleDiv, #descriptionDiv, #dialogImageObject, #featuredDiv, #colorDiv, #helpDiv").show();
     	 //$("#featuredImageObject").hide();
     	 
     	 $("#urlReference, #featured, #dialogFile").prop('required',false);        	         	 
     	 $("#title, #color").prop('required',true);
     	 
     	Exxo.Button.Create.hideDescriptionRequiredInfo();
     	
     	 if($("#featured").is(":checked")) {
 			//$("#featuredImageObject").show();		        	
	        //$("#file").prop('required',true);
	        Exxo.Button.Create.showDefaultOptions();
         } else {
	        //$("#featuredImageObject").hide();
	        //$("#file").prop('required',false);
	        Exxo.Button.Create.hideDefaultOptions();
 		 }
	  },
	  typeMessage: function(){
		 $("#titleDiv, #descriptionDiv, #urlReferenceDiv").show();
     	 $("#featuredImageObject, #dialogImageObject, #featuredDiv, #colorDiv, #helpDiv").hide();
   	 
     	 $("#file, #dialogFile, #featured, #urlReference, #color").prop('required',false);        	 
     	 $("#title").prop('required',true);
     	 Exxo.Button.Create.showDescriptionRequiredInfo();
     	 $("#featured").attr("checked", false);
	  },
	  showDescriptionRequiredInfo: function(){
		  $("#descRequired").show();
		  $("#description").prop('required',true);
	  },
	  hideDescriptionRequiredInfo: function(){
		  $("#descRequired").hide();
		  $("#description").prop('required',false);
	  },
	  showDefaultOptions: function(){
		  $("#affiliate").val(Exxo.UI.vars['clientForDefault']);
		  $('#isDefaultOfLocation').prop('checked', Exxo.UI.vars['isDefault']);
		  $("#default-options-container").show();
	  },
	  hideDefaultOptions: function(){
		  Exxo.UI.vars['clientForDefault'] = $("#affiliate").val();
		  Exxo.UI.vars['isDefault'] = $("#isDefaultOfLocation").is(":checked");
		  $('#isDefaultOfLocation').prop('checked', false);
		  $("#affiliate").val('null');
		  $("#default-options-container").hide();
	  },
	  displayScheduleInfoFromShow: function() {
		  var startTimeParts = Exxo.UI.vars["timeConfiguration"].split("-");
		  Exxo.Button.Create.displayScheduleInfo(startTimeParts[0], startTimeParts[1]);
	  },
	  displayScheduleInfo: function(startTime, duration) {
		  duration = duration*1;
		  startTime = startTime.trim();
		  var finalDBValue = startTime + "-" + duration;
		  var startTimeParts = startTime.split(" ");
		  var startTimeHourParts = startTimeParts[0].split(":");
		  var finalStr = "";
		  var initialHour = startTimeHourParts[0]*1;
		  if(startTimeParts[1] === "AM"){
			  initialHour = (initialHour == 12)? 0:initialHour;
		  } else {
			  initialHour += (initialHour == 12)? 0:12;
		  }
		  var finalHour = initialHour+duration;
		  var finalAMPM = "";
		  if(finalHour >= 24) {
			  var hoursOnOtherDay = finalHour%24;
			  finalAMPM = (hoursOnOtherDay < 12)? "AM":"PM";
			  finalHour = (hoursOnOtherDay == 0 || hoursOnOtherDay == 12)? 12:finalHour%12;
			  finalStr = Exxo.UI.translates["of next day"];
		  } else {
			  var hoursOnOtherDay = finalHour%12;			  
			  finalAMPM = (finalHour < 12)? "AM":"PM";
			  finalHour = (hoursOnOtherDay == 0)? 12:hoursOnOtherDay;
			  finalStr = ""
		  }
		  var finalMsg = Exxo.UI.translates["schedule.time.final"];
		  finalMsg = finalMsg.replace("{start}", startTime);
		  finalMsg = finalMsg.replace("{end}", finalHour + ":" + startTimeHourParts[1] + " " + finalAMPM);
		  finalMsg = finalMsg.replace("{endMsg}", finalStr);
		  $("#final-scheduler-time").html("* " + finalMsg);
		  $("#_content_hours").val(finalDBValue);
	  }
   }
};