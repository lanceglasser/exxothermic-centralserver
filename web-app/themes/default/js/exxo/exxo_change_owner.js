/***
 @file      exxo_change_owner.js
 @brief     Custom functions and events for the Change Owner Page
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Company = {
	Owners : {
		init : function() {			
			$("#owners").select2();

			$("#change-owner-dialog").dialog({
				height : 215,
				width : 400,
				modal : true,
				autoOpen : false,
				buttons : [ {
					text : Exxo.UI.translates['changeButton'],
					click : function() {
						Exxo.Company.Owners.changeCompanyOwner();
					}
				}, {
					text : Exxo.UI.translates['cancelButton'],
					click : function() {
						$(".select2-drop").hide();
						$(this).dialog("close");
					}
				} ]
			});
			
			$(".ui-dialog").click(function(e){
				$(".select2-drop").hide();
			});
			
			$(".dataTable thead th").click(function(e){			
				if(!$(this).hasClass("sorting_disabled")){
					Exxo.Company.Owners.setPaginationButtonsEvent();
					Exxo.Company.Owners.initOwnerIconsEvent();
				}
			});
			
			$("#table-search-input").keyup(function(e){
				Exxo.Company.Owners.setPaginationButtonsEvent();
				Exxo.Company.Owners.initOwnerIconsEvent();
			});
			
			$("#table-list_length").change(function(e){
				Exxo.Company.Owners.setPaginationButtonsEvent();
				Exxo.Company.Owners.initOwnerIconsEvent();
			});
			
			Exxo.Company.Owners.setPaginationButtonsEvent();
			Exxo.Company.Owners.initOwnerIconsEvent();
		},
		setPaginationButtonsEvent: function(){
			$(".paginate_button").click(function(e){
				Exxo.Company.Owners.initOwnerIconsEvent();
				Exxo.Company.Owners.setPaginationButtonsEvent();
			});
		},
		initOwnerIconsEvent: function() {
			$(".owner").unbind();
			$(".owner").click(function(e) {
				e.preventDefault();
				Exxo.UI.vars["company-id"] = $(this).attr("rel");
				Exxo.UI.vars["owner-id"] = $(this).attr("owner");
				$("#company").html($("#company-" + Exxo.UI.vars["company-id"]).html());
				$("#owners").val(Exxo.UI.vars["owner-id"]);
				$("#owners").change();
				$("#change-owner-dialog").dialog({
					height : 215
				}).dialog("open");
			});
		},
		changeCompanyOwner : function(selected) {
			Exxo.showSpinner();
			$.ajax({
				type : 'GET',
				data : 'ownerId=' + $("#owners").val() + '&companyId=' + Exxo.UI.vars["company-id"],
				url : Exxo.UI.urls["change-company-url"],
				success : function(data) {
					Exxo.hideSpinner();
					if (data.error === false) {
						$("#change-owner-dialog").dialog("close");
						Exxo.showInfoMessage(data.msg, true, '', false);
						$("#owner-" + Exxo.UI.vars["company-id"]).html(data.owner);
						$("#btn-owner-" + Exxo.UI.vars["company-id"]).attr("owner", $("#owners").val());
					} else {
						//There was an error
						Exxo.showErrorMessage(data.msg, true, '', false);
						console.debug("msg: " + data.msg);
					}
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					console.debug("Unexpected error: " + textStatus);
					Exxo.hideSpinner();
				}
			});
		}
	}
};