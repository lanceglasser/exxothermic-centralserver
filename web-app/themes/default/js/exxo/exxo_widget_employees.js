/***
 @file      exxo_widget_employees.js
 @brief     Functions for the Employees List Widget
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.WidgetsEmployees = {
	initEmployeesListWidget: function(){
		
		$( "#employees-widget" ).exxoWidget({
			helpMsg:	Exxo.UI.translates['widget-employees-list-info-msg'],
			referenceIcon: 	'list',
			referenceUrl: 	Exxo.UI.urls['employees-list']});
		
		Exxo.UI.vars["widgetEmployeesTable"] = $('#widgetEmployeesTable').dataTable( {
			"pagingType": "full", // More info at: https://datatables.net/examples/basic_init/alt_pagination.html
			"lengthMenu": [[5], [5]], // More info at: https://datatables.net/examples/advanced_init/length_menu.html
       	 	"processing": true,
            "serverSide": true,
            "ajax": {
            	"url": Exxo.UI.urls["widget-get-data-url"],
    			"data": function ( d ) {
    		        d.object = 'employee';
    		        d.companyId = Exxo.UI.vars["company-id"];
    		        d.locationId = Exxo.UI.vars["location-id"];
    		    }
            },
            "columnDefs": [
 		        {
 		            "targets": [ 0 ],
 		            "render": function ( data, type, row ) {
 		            	return '<div class="state ' + 
 		             		(row[3]? 'stateGreen':'stateGray') + '"></div>';
 		            }
 		        },
 		        {
					"targets": [ 1 ],
					"render": function ( data, type, row ) {
						return '<a href="' + Exxo.UI.urls["employee-show-url"] + row[0] + '">' + row[2] + ', ' + row[1] + '</a>';
					}
				},
				{
					"targets": [ 2 ],
					"render": function ( data, type, row ) {
						return row[3];
					}
				}
       		 ]
        } );
	}
};