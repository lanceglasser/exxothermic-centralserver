/***
 @file      exxo_app_skin_list.js
 @brief     General functions for the App Skin list pages
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.AppSkin = {
	initDocumentListEvents: function(){
		/*** Start of events for Documents List ***/
		//Init PD Viewers, only for the Document List Page		
		$(".view-pdf").click(function(e){
			e.preventDefault();
			
			/* For embed object, required browser plugin */
			//$("#pdf-object").attr("data", $(this).attr("href"));
			
			/* For jQuery Media Plugin 		  
			$("#pdf-viewer").html("");
			$('<a>',{
				id: 'pdf-viewer-link',
			    class: '',
			    title: '',
			    href: $(this).attr("href")
			}).appendTo('#pdf-viewer');
			
			$("#pdf-viewer-link").attr('href', $(this).attr("href"));
			$("#pdf-viewer-link").media({ width: 800, height: 480 }); */	
			
			
			/* For Google Docs Viewer */
			$("#pdf-viewer").html("");
			$('<a>',{
				id: 'pdf-viewer-link',
			    text: '',
			    title: '',
			    href: $(this).attr("href")
			}).appendTo('#pdf-viewer');
			
			$("#pdf-viewer-link").attr('href', $(this).attr("href"));
			$("#pdf-viewer-link").gdocsViewer({ width: 800, height: 480 });
			
			/* With Google Docs Viewer iFrame 
			$("#pdf-iframe").attr('src', 'https://docs.google.com/viewer?embedded=true&url='+$(this).attr("href")); */
			
			$( "#pdf-viewer" ).dialog("open");
		});
		
		$("#pdf-viewer" ).dialog({
	          height: 600,
	          width: 830,
	          modal: true,
	          autoOpen: false,
	          buttons: [ 
	                { text: Exxo.UI.translates['closeButton'],   
	                  click: function() {
	                      $( this ).dialog( "close" );                   
	                  }
	              }
	          ]
	        });
		
		/*** Ends of events for Documents List ***/
	},
	init : function() {
		
		Exxo.AppSkin.initDocumentListEvents();
		
	   Exxo.UI.vars['stopEvent'] = true;
	   Exxo.UI.vars['target'] = true;
	   	      
		$('#table-list').dataTable();
		
		$(".delete").click(function(e){
			 if (Exxo.UI.isSafari()) {
				 Exxo.UI.vars['target'] = $(this);
	       	 } else {
	       		 Exxo.UI.vars['target'] = e.target;
	       	 }
           
           if (Exxo.UI.vars['stopEvent']){
               e.preventDefault();             
               $( "#confirm-delete-dialog" ).dialog("open");            
           } else {
 				Exxo.showSpinner();
 				if (Exxo.UI.isSafari()) {
 					location.href = $(this).attr("href");
 				}
 			}
       });
		
		$("#confirm-delete-dialog" ).dialog({
	          height: 180,
	          width: 360,
	          modal: true,
	          autoOpen: false,
	          buttons: [ {
	                    text: Exxo.UI.translates['okButton'], 
	                    click: function() {
	                        $( this ).dialog( "close" );    
	                        Exxo.UI.vars['stopEvent'] = false;                      
	                        Exxo.UI.vars['target'].click();
	                    }
	                   },
	                { text: Exxo.UI.translates['cancelButton'],   
	                  click: function() {
	                      $( this ).dialog( "close" );                   
	                  }
	              }
	          ]
	        });
	}
};