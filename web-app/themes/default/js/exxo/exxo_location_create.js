/***
 @file      exxo_location_create.js
 @brief     Custom functions and events for the Company Location Creation Page.
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Location = {
    Create : {
       code: null,
       isEventGeneratedFromCheckboxUseCompanyInfo: false,
       defaultCountryId: null,
       defaultTypeEstablishmentId: null,
       defaultStateId: null,
       defaultTimezone: "UTC",
       updateGoogleMapPosition: function(ref){
			if (!Exxo.UI.vars["isLoadingCompanyInfo"]) {
				var address = ($("#country").val() != "") ? "" + $("#country").select2('data').text : "";
				if ($("#stateNameDiv").is(":visible")) {// Use the input box
					address += ($("#stateName").val() != "") ? "," + $("#stateName").val(): "";
				} else {// Use the select
					address += ($("#state").val() != "") ? "," + $("#state").select2('data').text + " State" : "";
				}
				address += ($("#city").val() != "") ? "," + $("#city").val() : "";
				address += ($("#address").val() != "") ? "," + $("#address").val() : "";
				showAddress(address);
			}
       },
       init : function() {
    	  Exxo.UI.vars["blockGoogleMapUpdate"] = false;
    	  Exxo.UI.vars["isUsingCompanyInfo"] = false;
    	  Exxo.UI.vars["isLoadingCompanyInfo"] = false;
    	  
        //Init Images Functions
          Exxo.Images.init();
          
          google.maps.event.addDomListener(window, 'load', initialize(Exxo.UI.vars['lat'], Exxo.UI.vars['lng']));
          
          Exxo.Location.Create.defaultStateId  = $("#state").val();
          Exxo.Location.Create.defaultTypeEstablishmentId = $("#type").val();
          Exxo.Location.Create.defaultCountryId = $("#country").val();
          Exxo.Location.Create.code = $( "#type").find(":selected").attr("id");    
          $('#timezone').val(Exxo.Location.Create.defaultTimezone);
          $("#type,#country,#state,#company,#timezone").select2();  
              
          $("#commercialIntegrator").select2({        
              allowClear: true
          });
          
          $("#updateForm").submit(function(e){
             Exxo.showSpinner();
          });
          
          if( Exxo.Location.Create.code =='other' || Exxo.Location.Create.code == 'waitingRoomSpecify')
          {
              $("#controlCompanyOtherType").show();
              $("#otherType").attr("required","required");
              
          }
          else
          {
              $("#controlCompanyOtherType").hide();
              $("#otherType").removeAttr("required");
          }
          
          $("#type").change(function(){
              
             Exxo.Location.Create.code = $( "#type").find(":selected").attr("id");
              
              if(Exxo.Location.Create.code=='other' || Exxo.Location.Create.code=='waitingRoomSpecify')
              {
                  $("#controlCompanyOtherType").show();
                  $("#otherType").attr("required","required");
                  
              }
              else
             {
                  $("#controlCompanyOtherType").hide();
                  $("#otherType").removeAttr("required");
             }
          });
          
          $("#country").change(function(){
              if(!Exxo.Location.Create.isEventGeneratedFromCheckboxUseCompanyInfo) {
                 var url = Exxo.UI.urls["loadStatesByCountryUrl"];
                 if(Exxo.Utils.Urls.getUrlParam(url, 'execution') == null){
                    url += "?id="+$(this).val();
                 } else {
                    url += "&id="+$(this).val();
                 }
                  $.getJSON(url,function(data){                 
                      try
                      {
                          if(data.states.length > 0 ){
                              $("#stateNameDiv").hide();
                              $("#stateName").removeAttr("required");
                              $("#stateDiv").show();
                              //remove the plugin select2
                              $("#state").select2("destroy");
                              //add the data
                              $("#state").setSelectOptionsWithCode(data.states,"id","name", "code");
                              
                              if(Exxo.UI.vars["stateFromGoogle"] != ""){
                            	  $("#state option[code='" + Exxo.UI.vars["stateFromGoogle"] + "']").attr("selected","selected");
                              }
                              
                              //restart the pluggin select2
                              $("#state").select2();
                              //Trigger to change the City select box
                              $("#state").change();
                          }else{
                        	  if(Exxo.UI.vars["blockGoogleMapUpdate"] == false){
                            	  Exxo.Location.Create.updateGoogleMapPosition(1);
                              }
                        	  Exxo.UI.vars["blockGoogleMapUpdate"] = false;
                              $("#stateNameDiv").show();
                              if(Exxo.UI.vars["stateFromGoogle"] != ""){
                            	  $("#stateName").val(Exxo.UI.vars["stateFromGoogle"]);
                              }
                              $("#stateName").attr("required","required");
                              $("#stateDiv").hide();
                          }
                          
                          Exxo.UI.vars["stateFromGoogle"] = "";
                      }
                      catch(Exception){}                 
                  });
              }
          });
          
          $("#state").change(function () {
        	  console.debug("blockGoogleMapUpdate on A: " + Exxo.UI.vars["blockGoogleMapUpdate"]);
        	  if(Exxo.UI.vars["blockGoogleMapUpdate"] == false){
        		  Exxo.Location.Create.updateGoogleMapPosition(2);
        	  } else {
        		  Exxo.UI.vars["blockGoogleMapUpdate"] = false;
        	  }
          });
          
          $("#address, #city, #stateName").change(function () {
        	  console.debug("Address, City or StateName changed!!!");
        	  console.debug("blockGoogleMapUpdate on B: " + Exxo.UI.vars["blockGoogleMapUpdate"]);
        	  if(Exxo.UI.vars["blockGoogleMapUpdate"] == false){
        		  console.debug("Flag 1");
        		  Exxo.Location.Create.updateGoogleMapPosition(3);
        	  } else {
        		  console.debug("Flag 2");
        		  Exxo.UI.vars["blockGoogleMapUpdate"] = false;
        	  }
          });
                            
          /*
          //Functions to change the pin of an specific location on the map
          $("#latitude").change(function () {
             
              if(flaglng == true){
                  
                  if($("#latitude").val() != ""){
                      flaglat = true;
                      var latlng = latitude.value +"," + longitude.value;
                      showLatLng(latlng);
                  }           
              }
              else{
                  flaglat = true;
              }
          });
          
          $("#longitude").change(function () {
                 
              if(flaglat == true){
                  
                  if($("#longitude").val() != ""){
                      flaglng = true;
                      var latlng = latitude.value +"," + longitude.value;
                      showLatLng(latlng);
                  }           
              }
              else{
                  flaglng = true;
              }
          });               
          */
          
           $("#maxOccupancy, #numberOfTv, #numberOfTvWithExxothermicDevice").change(function () { 
                this.value = this.value.replace(/[^\d]/g,'');
            }); 
          
          $("#company").change(function(){    
              $("#useCompanyinformationCheckbox").change();
              Exxo.Location.Create.loadSelectedCompanyLogo();
          });
          
          // Ajax to use company information
          $("#useCompanyinformationCheckbox").change(function(){
              Exxo.Location.Create.isEventGeneratedFromCheckboxUseCompanyInfo = true;
              var isChecked = $("#useCompanyinformationCheckbox").prop("checked");
              Exxo.UI.vars["isLoadingCompanyInfo"] = true;
               if(isChecked == "checked" || isChecked == true) {
            	   Exxo.UI.vars["isUsingCompanyInfo"] = true;
                   //DO NOT REMOVE, need this for IE
                   $.ajaxSetup({ cache: false });
                   var url = Exxo.UI.urls['getCompanyInfoURL'];
                   if(Exxo.Utils.Urls.getUrlParam(url, 'execution') == null){
                      url += "?companyId="+$("#company").val();
                   } else {
                      url += "&companyId="+$("#company").val();
                   }
                   $.getJSON( url , function(data){                      
                       try                
                       {               
                          Exxo.UI.vars["lastCompanyLogoLoaded"] = $("#company").val();
                          Exxo.UI.vars["companyLogo"] = data.logoUrl;
                          
                          //Address
                          $("#address").val(data.address); 
                          $('#address').attr("readonly", true);
                          
                          //country
                          $("#country").val(data.countryId).trigger("change");
                          $("#country").select2("readonly", true);
                          
                          //city
                          $("#city").val(data.city);
                          $('#city').attr("readonly", true);
                          
                          //Timezone
                          $("#timezone").val(data.timezone).trigger("change");
                          $("#timezone").select2("readonly", true);
                          
                          //type
                          if(data.typeId != null) {
                              $("#type").val(data.typeId).trigger("change");//change the value and update the display
                              $("#type").select2("readonly", true);  
                              $("#otherType").val(data.other);
                              $('#otherType').attr("readonly", true);
                          }
                          
                          //state
                          if(data.states.length > 0 ){
                              $("#stateNameDiv").hide();
                              $("#stateName").removeAttr("required");
                              $("#stateDiv").show();
                              //remove the plugin select2
                              $("#state").select2("destroy");
                              //add the data
                              $("#state").setSelectOptions(data.states,"id","name");
                              //select the option                     
                              $("#state").val(data.stateId);
                              //restart the pluggin select2
                              $("#state").select2();
                              $("#state").select2("readonly", true);
                              //Trigger to change the City select box
                              $("#state").change();     
                          }else{
                              $("#stateNameDiv").show();
                              $("#stateName").attr("required","required");
                              $("#stateDiv").hide();
                              $("#stateName").val(data.stateName);
                              $('#stateName').attr("readonly", true);                                                 
                          }  
                          if(Exxo.UI.vars["blockGoogleMapUpdate"] == false){
                    		  Exxo.Location.Create.updateGoogleMapPosition(4);
                    	  } else {
                    		  Exxo.UI.vars["blockGoogleMapUpdate"] = false;
                    	  }
                          Exxo.UI.vars["isLoadingCompanyInfo"] = false;
                          Exxo.Location.Create.updateGoogleMapPosition("After Load Company Info");
                      } catch(Exception){                       
                      }          
                   });
               } else {
            	   Exxo.UI.vars["isUsingCompanyInfo"] = false;
                   Exxo.Location.Create.isEventGeneratedFromCheckboxUseCompanyInfo = false;                   
                   $("#stateDiv").show();
                   $("#stateNameDiv").hide();
                   $("#stateName").removeAttr("required");
                   $('#address').attr("readonly", false);
                   $("#country").select2("readonly", false);
                   $('#city').attr("readonly", false);
                   $('#stateName').attr("readonly", false);
                   $("#type").select2("readonly", false);
                   $('#otherType').attr("readonly", false);
                   $("#state").select2();
                   $("#state").select2("readonly", false);
                   $("#timezone").select2("readonly", false);
                   Exxo.UI.vars["isLoadingCompanyInfo"] = false;
                   Exxo.Location.Create.updateGoogleMapPosition("After set default fields");
               }
          });
          
          
          if(Exxo.UI.vars['lat'] === -1 ){
             Exxo.Location.Create.updateGoogleMapPosition(5);
          }
          
          //Shows div according if must use or not the company logo
          if(Exxo.UI.vars['useCompanyLogo']){
             Exxo.Location.Create.loadSelectedCompanyLogo();
          } else {
             $("#companyLogo").hide();
             $("#uploadOwnLogo").show();
          }
          $("#useCompanyLogo").change(function(e){
             Exxo.Location.Create.changeUseCompanyLogoOption($(this))
          });
       },
       changeUseCompanyLogoOption: function(elem){
          if (elem.is(':checked')){
             Exxo.Location.Create.loadSelectedCompanyLogo();             
          } else {
             $("#companyLogo").hide();
             $("#uploadOwnLogo").show();
          }
       },
       showCompanyLogo: function(){
    	  $("#uploadOwnLogo").hide();
          $("#companyLogo").show();
          
    	  if(Exxo.UI.vars["companyLogo"] != undefined && Exxo.UI.vars["companyLogo"] != ""){
    		  //The Company has a Logo; show the image and hide the message
    		  $("#notCompanyLogoMessage").hide();
    		  $("#companyLogoImg").attr("src", Exxo.UI.vars["companyLogo"]);
    		  $("#companyLogoImg").show();
    	  } else {
    		  //The Company do not has a Logo; show the message and hide the image
    		  $("#companyLogoImg").hide();
    		  $("#notCompanyLogoMessage").show();
    	  }
       },
       loadSelectedCompanyLogo: function(){
          if ($("#useCompanyLogo").is(':checked')){
             if(Exxo.UI.vars["lastCompanyLogoLoaded"] !== $("#company").val()){
                Exxo.showSpinner();
                var url = Exxo.UI.urls['getCompanyInfoURL'];
                if(Exxo.Utils.Urls.getUrlParam(url, 'execution') == null){
                   url += "?companyId="+$("#company").val();
                } else {
                   url += "&companyId="+$("#company").val();
                }
                $.getJSON(url, function(data){
                  try {
                     Exxo.UI.vars["lastCompanyLogoLoaded"] = $("#company").val();
                     Exxo.UI.vars["companyLogo"] = data.logoUrl;
                     Exxo.Location.Create.showCompanyLogo();
                  }
                  catch(Exception){                       
                  }  
                  Exxo.hideSpinner();
               });
             } else {
                Exxo.Location.Create.showCompanyLogo();
             }
          }
          
          
          
         /* $.getJSON(Exxo.UI.urls['getCompanyInfoURL'] + "?companyId="+$("#company").val(), function(data){             
             try                
            {                   
                //Address
                $("#address").val(data.address); 
                $('#address').attr("readonly", true);               
            }
            catch(Exception){                       
            }          
         });*/
       }
    }
};