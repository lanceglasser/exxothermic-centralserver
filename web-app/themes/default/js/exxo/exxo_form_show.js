(function( $ ) {
 
    $.fn.createShowForm = function(options) {
 
          var settings = $.extend({
             // These are the defaults.
             title: "Default Title",
             columns: [],
             buttons: [],
             texts: {
                yes:    'Yes',
                no:     'No',
                empty:  '---'
            }
         }, options );
          
          var columnsHtml = [];
          
          $.each(settings.columns, function( index, rows ) {
             var columnHtml = "";
             $.each(rows, function( index, row ) {
               console.debug(row + "--> " + row.label + " :: " + row['label'] + ": " + row['value'] + ", " + row.isEmpty);
               
               if(row.show == null || row.show === "true"){
                  columnHtml += '<div class="col-xs-5 text-to-right">';
                  columnHtml += '  <span class="field-label">' + row.label + '</span>';
                  columnHtml += '</div>';
                  columnHtml += '<div class="col-xs-7 text-to-left">';
                  
                  if(row.useMainText != null && row.useMainText === "false"){
                     if(row.value2 == null){
                        columnHtml += '  <span class="field-value">' + settings.texts.empty + '</span>';
                     } else {
                        columnHtml += '  <span class="field-value">' + row.value2 + '</span>';
                     }
                  } else {
                     if(row.type && row.type==="bool"){
                        if(row.value === "true"){
                           columnHtml += '  <span class="field-value">' + settings.texts.yes + '</span>';
                        } else {
                           columnHtml += '  <span class="field-value">' + settings.texts.no + '</span>';
                        }
                     } else {
                        columnHtml += '  <span class="field-value">' + row.value + '</span>';
                     }
                  }
                  
                  columnHtml += '  </div>';
                  columnHtml += '  <span class="clearfix"></span>';
               }
               
               
               
               /*columnHtml += '  <g:if test="${company.webSite}">';
               columnHtml += '      <span class="field-value">${company.webSite}></span>';
               columnHtml += '  </g:if>';
               columnHtml += '  <g:else>';
               columnHtml += '      <span class="field-value"><g:message code="default.field.empty" /></span>';
               columnHtml += '  </g:else>';*/
               
             });
             
             columnsHtml[index] = columnHtml;
          });
          
          var buttonsHtml = "";
          $.each(settings.buttons, function( index, value ) {
             buttonsHtml += "&nbsp;" + value;
          });
          
          var html = "<h1>" + settings.title + "</h1>";
          
          html += '<div class="content form-info">';
          html += '   <!-- Left Column -->';
          html += '   <div class="col-xs-6 resize-formleft">';
          html +=   columnsHtml[0];                            
          html += '   </div>';
          html += '   <!-- Right Column -->';
          html += '   <div class="col-xs-6 resize-formright">';
          html +=   columnsHtml[1];
          html += '   </div>';
          html += '   <span class="clearfix"></span>';
 
          html += '   <!-- Buttons -->';
          html += buttonsHtml;
          html += '</div>';
          
          this.html(html);
 
        return this;
 
    };
 
}( jQuery ));