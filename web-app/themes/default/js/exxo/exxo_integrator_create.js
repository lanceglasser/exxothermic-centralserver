/***
 @file      exxo_integrator_create.js
 @brief     Custom functions and events for the Company Integrator Create Page.
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Integrator = {
    Create : {
       init : function() {
    	  Exxo.Images.init();
    	  
          $("#country,#state,#typeOfCompany,#owner,#numberOfEmployees,#timezone,#levelOfService").select2();    
          
          $("#updateForm, #createForm").submit(function(e){
        	  Exxo.showSpinner();
          });
          $("#webSite").change(function(){
              if(this.value.substr(0,7) != 'http://'){
                  this.value = 'http://' + this.value;
                }
                if(this.value.substr(this.value.length-1, 1) != '/'){
                    this.value = this.value + '/';
                }
          });
            
          $("#country").change(function(){        
             Exxo.UI.setInputsEventsByCountry($(this).val());
              $.getJSON(Exxo.UI.urls["loadStatesByCountryUrl"] + "?id="+$(this).val(),function(data){
                  try
                  {
                      if(data.states.length > 0 ){
                          $("#stateNameDiv").hide();
                          $("#stateDiv").show();
                          //remove the plugin select2
                          $("#state").select2("destroy");
                          //add the data
                          $("#state").setSelectOptions(data.states,"id","name");
                          //restart the pluggin select2
                          $("#state").select2();
                          //Trigger to change the City select box
                          $("#state").change();
                      }else{
                          $("#stateNameDiv").show();
                          $("#stateDiv").hide();
                      }
                  }
                  catch(Exception){}             
              });
          });
       }
    }
};