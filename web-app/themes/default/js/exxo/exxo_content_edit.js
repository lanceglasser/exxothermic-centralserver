/***
 @file      exxo_custom_button_create.js
 @brief     Custom functions and events for the Custom Button Creation Page
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Button = {
   Create : {
      init : function() {
         Exxo.Images.init();
         
         
         /****/
         $("#url").change(
               function() {

                  if (this.value.substr(0, 7) != 'http://'
                        && this.value.substr(0, 8) != 'https://') {
                     this.value = 'http://' + this.value;
                  }
               });

         $("#color").change(function() {
            var textColor = this.value;
            var myhex = textColor.replace(/[^0-9a-fA-F]/g, '');
            this.value = "#" + myhex;

         });

         $("#createForm").submit(function(e) {
            Exxo.Button.Create.showSpinner();
         });

         $("#type").select2({        
              allowClear: true
          });
         // shows and hide needed fields
         function typeImage()
         {
        	 
        	 $("#description, #featured, #file, #dialogFile").prop('required',false);
        	 
        	 $("#titleDiv, #colorDiv, #urlDiv, #fileDiv, #featuredImageObject, #dialogImageObject").show();
        	 $("#descriptionDiv, #featuredDiv").hide();
        	 
        	 $("#title, #color, #file, #dialogFile").prop('required',true);
        	 $("#featured").attr("checked", false);
         }
         

         function typeOffer()
         {
        	 
        	 //$("#featured").attr("checked", false);
        	         	 
        	 $("#titleDiv, #descriptionDiv, #dialogImageObject, #featuredDiv, #colorDiv").show();
        	 $("#urlDiv, #featuredImageObject").hide();
        	 
        	 $("#file, #url, #featured, #dialogFile").prop('required',false);        	         	 
        	 $("#title, #description, #color").prop('required',true);
        	 
        	 if($("#featured").is(":checked"))
        		 {
        			$("#featuredImageObject").show();		        	
		        	$("#file").prop('required',true);
		        }else{
		        	 $("#featuredImageObject").hide();
		        	 $("#file").prop('required',false); 
        		 }
         }
         
         function typeMessage()
         {
        	 $("#titleDiv, #descriptionDiv").show();
        	 $("#featuredImageObject, #dialogImageObject, #featuredDiv, #urlDiv, #colorDiv").hide();
        	 
        	 $("#file, #dialogFile, #featured, #url, #color").prop('required',false);        	 
        	 $("#title, #description").prop('required',true);
        	 $("#featured").attr("checked", false);
         }
                  
         
        $("#featured").change(function() {
		        if($(this).is(":checked")) {
		        	$("#featuredImageObject").show("slow");		        	
		        	//$("#file").prop('required',true);
		        }else{
		        	 $("#featuredImageObject").hide("slow");
		        	// $("#file").prop('required',false);
		        }
		                
		    });
         
         function typeSelection(typeValue)
         {
        	if(typeValue==TYPE_IMAGE)
            {
             	typeImage();
            }else if(typeValue==TYPE_OFFER)
             	{
             		typeOffer();
             	}else if(typeValue==TYPE_MESSAGE)
             		{
             			typeMessage();
             		}
            $("#file").prop('required',false);
         }
         
        if(TYPE_SELECTED=="")
    	{
    	   typeImage();
    	}else{
    	   typeSelection(TYPE_SELECTED);
    	}

        $("#file").prop('required',false);
         
                  
        $("#type").change(
            function() {
                typeSelection($("#type").val());
            });
       

      }
   }
};

