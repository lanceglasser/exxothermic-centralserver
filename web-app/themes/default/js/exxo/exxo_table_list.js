/***
 @file      exxo_table_list.js
 @brief     General functions for handler of the DataTables
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Tables = {
	init : function() {		
		//alert(Exxo.module);		
	    if("location-show"==Exxo.module)
	    {
	    	$("#contentOrderedList").dataTable(
	    			{
	    				"bSort": false ,
	    				"bPaginate": false,
	    			    "bLengthChange": false,
	    			    "bFilter": false,
	    			    "bInfo": false,
	    			    "bAutoWidth": false,
	    			    stateSave: true,
	    			    "language": {
				            "lengthMenu"	: Exxo.UI.translates.lengthMenu,
				            "zeroRecords"	: Exxo.UI.translates.zeroRecords,
				            "info"			: Exxo.UI.translates.info,
				            "infoEmpty"		: Exxo.UI.translates.infoEmpty,
				            "infoFiltered"	: Exxo.UI.translates.infoFiltered,
				            "sSearch"		: Exxo.UI.translates.sSearch
				        }
				    }		
	    	).rowReordering();		
	    }
	    
	    var boxListTable = $('#box-list').dataTable({
	    	"dom": '<"registerBox">flrtip',
	    	"aoColumns":  [ 
	                       null,// 0- serial
	                       null,// 1- name
	                       null,// 2- PA
	                       null,// 3- version	                       
	                       { "iDataSort": 5 },//Use column 6 to perform sorting	                       
	                       null,
	                       null
	                   ],
	        "pageLength":  		(Exxo.UI.vars["rowsPerPage"] != undefined)? Exxo.UI.vars["rowsPerPage"]:Exxo.UI.vars["defaultTableRowsPerPage"],
	        "displayStart":		(Exxo.UI.vars["firstRecord"] != undefined)? Exxo.UI.vars["firstRecord"]:0,
    		"search": {
    			"search": 		(Exxo.UI.vars["tableFilter"] != undefined)? Exxo.UI.vars["tableFilter"]:''
    		},
    		"order": 			[ Exxo.UI.vars["columnOrder"], Exxo.UI.vars["orderDir"] ],
	        "language": {
	            "lengthMenu"	: Exxo.UI.translates.lengthMenu,
	            "zeroRecords"	: Exxo.UI.translates.zeroRecords,
	            "info"			: Exxo.UI.translates.info,
	            "infoEmpty"		: Exxo.UI.translates.infoEmpty,
	            "infoFiltered"	: Exxo.UI.translates.infoFiltered,
	            "sSearch"		: Exxo.UI.translates.sSearch
	        }
	    });
	    
	    if("box-list"==Exxo.module)
	    {
		    $("div.registerBox").html(' <a id="registerLink" href="'+Exxo.UI.urls['registerBoxURL']+'">'+Exxo.UI.translates['registerBox']+'</a> ');
		    
		    $('#table-search-input').on( 'keyup', function () {
		        console.log( $("#table-search-input").val() + ' length: '+ $(".dataTables_empty").length);
		    	if($(".dataTables_empty").length > 0){
		    		$("#registerLink").show();
		    		$("#registerLink").attr("href", Exxo.UI.urls['registerBoxURL']+'?serial='+$("#table-search-input").val());
		    	}else{
		    		$("#registerLink").hide();
		    	}
		    } );
	    }	    
		
		$('#table-list, #table-list-two, .table-list').each(function(){
		    var bFilter = true;
		    if($(this).hasClass('nofilter')){
		        bFilter = false;
		    }
		    
		    var columnSort = new Array; 
		   $(this).find('thead tr th').each(function(){
		       if($(this).attr('data-bSortable') == 'false' || $(this).html().trim() === "Actions"){
		          columnSort.push({ "bSortable": false });
		       } else {
                  columnSort.push({ "bSortable": true });
               }
		    });
		   
		    	$(this).dataTable({
			        "aoColumns": columnSort,
			        "pageLength":  		(Exxo.UI.vars["rowsPerPage"] != undefined)? Exxo.UI.vars["rowsPerPage"]:Exxo.UI.vars["defaultTableRowsPerPage"],
			        "displayStart":		(Exxo.UI.vars["firstRecord"] != undefined)? Exxo.UI.vars["firstRecord"]:0,
	        		"search": {
		    			"search": 		(Exxo.UI.vars["tableFilter"] != undefined)? Exxo.UI.vars["tableFilter"]:''
	        		},
	        		"order": 			[ Exxo.UI.vars["columnOrder"], Exxo.UI.vars["orderDir"] ],
			        "language": {
			            "lengthMenu"	: Exxo.UI.translates.lengthMenu,
			            "zeroRecords"	: Exxo.UI.translates.zeroRecords,
			            "info"			: Exxo.UI.translates.info,
			            "infoEmpty"		: Exxo.UI.translates.infoEmpty,
			            "infoFiltered"	: Exxo.UI.translates.infoFiltered,
			            "sSearch"		: Exxo.UI.translates.sSearch
			        }
			    });
		    	
		    	//var table = $('#data_table').dataTable();
		    	//console.debug("-> Change table to page 2");
		    	//$(this).fnPageChange(2,true);
		    	
		    	
		});
		
		
		Exxo.Tables.initPaginationEvents();
		
		$('div.dataTables_filter input').focus();
	},
	loadPaginationInitValuesFromString: function(sessionPaginationInfo){
		var obj = jQuery.parseJSON( sessionPaginationInfo );
		Exxo.UI.vars["rowsPerPage"] = obj.rows;
		Exxo.UI.vars["firstRecord"] = obj.page * obj.rows;
		Exxo.UI.vars["tableFilter"] = obj.text;
		Exxo.UI.vars["columnOrder"] = obj.columnOrder;
		Exxo.UI.vars["orderDir"] = obj.orderDir;
	},
	initPaginationEvents: function(){
		$("select[name=box-list_length]").change(function(e){
			Exxo.Tables.saveTableInfo();
			Exxo.Tables.setPaginationButtonsEvent();
		});
		$("select[name=table-list_length]").change(function(e){
			Exxo.Tables.saveTableInfo();
			Exxo.Tables.setPaginationButtonsEvent();
		});
		$("#table-search-input").blur(function(e){
			Exxo.Tables.saveTableInfo();
			Exxo.Tables.setPaginationButtonsEvent();
		});
		$(".dataTable thead th").click(function(e){			
			if(!$(this).hasClass("sorting_disabled")){
				Exxo.UI.vars["columnOrder"] = $(".dataTable thead th").index($(this));
				Exxo.UI.vars["orderDir"] = ($(this).hasClass("sorting_asc"))? "asc":"desc";
				Exxo.Tables.saveTableInfo();
				Exxo.Tables.setPaginationButtonsEvent();
			}
		});
		Exxo.Tables.setPaginationButtonsEvent();
	},
	setPaginationButtonsEvent: function(){
		$(".paginate_button").click(function(e){
			Exxo.Tables.saveTableInfo();
			Exxo.Tables.setPaginationButtonsEvent();
		});
	},
	saveTableInfo: function(){
		if(Exxo.UI.vars["page-code"] !== undefined && Exxo.UI.vars["page-code"] !== ""){
			var rowsPerPage = $("select[name=table-list_length]").val();
			if(Exxo.UI.vars["page-code"] === "exxtractors-list" ){
				rowsPerPage = $("select[name=exxtractorsTable_length]").val();
			}
			var page = $(".paginate_button.current").attr("data-dt-idx");
			var searchText = $("#table-search-input").val();
	        $.ajax({type:'POST',
	              data:'rows=' + rowsPerPage + '&search=' + searchText +"&page=" + page + "&code=" + Exxo.UI.vars["page-code"]
	        			+ "&columnOrder=" + Exxo.UI.vars["columnOrder"] + "&orderDir=" + Exxo.UI.vars["orderDir"],                                  
	              url: Exxo.UI.urls['savePaginationInfo'], 
	              success:function(data) {
	                 if(data.error !== false) { // There was an error
	                    console.debug("msg: " + data.msg);
	                 }
	                },
	              error:function(XMLHttpRequest,textStatus,errorThrown){   
	                   console.debug("error inesperado");
	              }
	         });
		}
	}
};