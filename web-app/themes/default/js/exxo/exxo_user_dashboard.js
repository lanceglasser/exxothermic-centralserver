/***
 @file      exxo_user_dashboard.js
 @brief     Initiliazation functions for the User Dashboard
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.UserDashboard = {
	openAddWidgetWindow: function(){		
		Exxo.showSpinner();
        $.ajax({type:'POST',
              data:'dash=' + Exxo.UI.vars["dashboard-name"] + '&object=getAvailableWidgetsToAdd',                                  
              url: Exxo.UI.urls["widget-get-detailed-data-url"],
              success:function(data) {
                 Exxo.hideSpinner();
                 if(data.error !== false){
                    //There was an error
                	console.debug("msg: " + data.msg);
                 } else {
                	 if(data.widgets.length == 0){
                		 Exxo.showInfoMessage(Exxo.UI.translates["no-more-widgets-to-add"], true, '', false);
                	 } else {
                		 $("#addWidgetDialogPlaceHolder").html("");
                    	 for(var i=0; i < data.widgets.length; i++){
                    		 var html = '<div class="widget-row">'
                    				+ '<div class="texts">'
                    				+ '<div class="title">' + data.widgets[i].name + '</div>'
                    				+ '<div class="description">' + data.widgets[i].desc + '</div>'
                    				+ '</div>'
                    		
                    				+ '<div class="btn-div">' 
                    				+ '<a href="#" class="widget-add-btn" rel="' + data.widgets[i].id + '">'
                    				+ '<span class="form-btn btn-primary" style="width: 150px;">'
                    				+ Exxo.UI.translates['addButton']
                    				+ '</span>'
                    				+ '</a>'
                    				+ '</div>'
                    				+ '<span class="clearfix"></span>'
                    				+ '</div>';
                    		 
                    		 $("#addWidgetDialogPlaceHolder").append(html);
                    	 }
                    	 
                    	 $(".widget-add-btn").unbind().click(function(e){
                    		 e.preventDefault();
                    		 var rel = $(this).attr("rel");
                    		 //alert("Add widget: " + rel);
                    		 Exxo.UserDashboard.addWidget(rel);
                    	 });
                    	 
                    	 $("#addWidgetDialogPlaceHolder").dialog('open');
                	 }
                 }
                },
              error:function(XMLHttpRequest,textStatus,errorThrown){                              
                   //alert(errorThrown);
                   console.debug("error inesperado");
                   Exxo.hideSpinner();
            }
         });
	},
	addWidget: function(widgetId){
		Exxo.showSpinner();
		
		jQuery.ajax(
		{
		type:'POST',
		data:{'dash': Exxo.UI.vars["dashboard-name"], 'widgetId': widgetId}, 
		url:Exxo.UI.urls["get-widget-html"],
		success:function(data,textStatus){
			Exxo.hideSpinner();
			
			//welcomeList-widget
			$("#temp-add-widget").html("");
			$("#temp-add-widget").html(data);
						
			switch(widgetId){
			case "companies-widget":
				Exxo.WidgetsCompany.initCompaniesListWidget();
				break;
			case "locations-widget":
				Exxo.WidgetsLocations.initLocationsListWidget();
				break;
			case "banners-widget":
				Exxo.WidgetsBanners.initBannersListWidget();
				break;
			case "appSkin-widget":
				Exxo.WidgetsAppSkins.initAppSkinsListWidget();
				break;
			case "welcomeList-widget":
				Exxo.WidgetsWelcomeAd.initWelcomeAdsListWidget();
				break;
			case "offers-widget":
				Exxo.WidgetsOffers.initOffersListWidget();
				break;
			case "documents-widget":
				Exxo.WidgetsDocuments.initDocumentsListWidget();
				break;
			case "exxtractors-widget":
				Exxo.WidgetsExxtractors.initExxtractorsListWidget();
				break;
			case "exxtractors-info-widget":
				Exxo.WidgetsStats.initExxtractorsInfoWidget();
				break;
			case "employees-widget":
				Exxo.WidgetsEmployees.initEmployeesListWidget();
				break;
			case "company-widget":
				Exxo.WidgetsCompany.initCompanyWidget();
				break;
			case "location-widget":
				Exxo.WidgetsLocation.initLocationWidget();
				break;
			case "waAppTheme-widget":
				Exxo.WidgetsWaAppTheme.initWaAppThemeWidget();
				break;
			case "shortcuts-widget":
				Exxo.WidgetsShortcuts.initShortcutsWidget();
				break;
			default:
				console.debug("Trying to refresh widget: " + widgetId);
				break;
			}
			
						
			Exxo.UI.vars["gridster"].add_widget($("#" + widgetId), 
					$("#" + widgetId).attr("data-sizex"), $("#" + widgetId).attr("data-sizey"));
			
			$("#temp-add-widget").html("");
			
			Exxo.UserDashboard.startWidgetOptionsEvents();
			
			Exxo.UserDashboard.saveWidgetsConfig(Exxo.UI.vars["gridster"].serializeForExxo());
			
			$("#addWidgetDialogPlaceHolder").dialog('close');
		},
		error:function(XMLHttpRequest,textStatus,errorThrown){
			Exxo.hideSpinner();
		}
		});
	},
	minWidget: function(o){
		var widget = o.parent().parent().parent().parent();
		var currentDimensions = Exxo.UI.vars["gridster"].serialize(widget)[0];
		
		widget.attr("maximizeHeight", o.parent().parent().parent().parent().css("height"));
		widget.attr("maximizeX", currentDimensions.size_x);
		widget.attr("maximizeY", currentDimensions.size_y);
		
		widget.css("height", "27px");
		widget.attr('status', 'min');
		
		o.hide();
		
		widget.find(".widgetMax").show();
		widget.find(".widget-internal-content").hide();
		Exxo.UI.vars["gridster"].resize_widget( widget, currentDimensions.size_x, 1);
		Exxo.UI.vars["gridster"].disable_resize( widget );
		Exxo.UI.vars["gridster"].recalculate_faux_grid();
		Exxo.UserDashboard.saveWidgetsConfig(Exxo.UI.vars["gridster"].serializeForExxo());
	},
	maxWidget: function(o){
		var widget = o.parent().parent().parent().parent();
		widget.css("height", widget.attr("maximizeHeight"));
		widget.find(".widget-internal-content").show();
		widget.attr('status', 'max')
		o.hide();
		o.parent().parent().parent().parent().find(".widgetMin").show();
		Exxo.UI.vars["gridster"].enable_resize( widget );
		Exxo.UI.vars["gridster"].resize_widget( widget, widget.attr("maximizeX")*1, widget.attr("maximizeY")*1);
		Exxo.UserDashboard.saveWidgetsConfig(Exxo.UI.vars["gridster"].serializeForExxo());
	},
	saveWidgetConfig:function(){
		var widgetId = Exxo.UI.vars["currentWidgetId"];
		switch(widgetId){
		case "exxtractors-info-widget":
			var time = $("#exxtractors-info-widget-refreshTime").val();
			if(time < 3 || time > 8){
				Exxo.showInfoMessage(Exxo.UI.translates['widget-refresh-min-max-error'], true, '', false);
			} else {
				Exxo.WidgetsStats.saveWidgetConfig(widgetId);
				Exxo.UserDashboard.saveWidgetsConfig(Exxo.UI.vars["gridster"].serializeForExxo());
				$('.widget-config-window').dialog("close");
			}
			break;
		case "shortcuts-widget":
			Exxo.WidgetsShortcuts.saveWidgetConfig(widgetId);
			break;
		default:
			console.debug("Trying to open the config of unknow widget: " + widgetId);
			break;
		}
	},
	loadConfigWindowWidget: function(widget){
		var widgetId = widget.attr("id");
		Exxo.UI.vars["currentWidgetId"] = widgetId;
		switch(widgetId){
		case "exxtractors-info-widget":
			Exxo.WidgetsStats.openConfigWindow(widget);
			break;
		case "shortcuts-widget":
			Exxo.WidgetsShortcuts.openConfigWindow(widget);
			break;
		default:
			console.debug("Trying to open the config of unknow widget: " + widgetId);
			break;
		}
		Exxo.UI.setOnlyNumbersEvent();
	},
	refreshWidget: function(widgetId){
		switch(widgetId){
		case "companies-widget":
			Exxo.UI.vars["widgetCompaniesTable"].api().ajax.reload(function(json){});
			break;
		case "locations-widget":
			Exxo.UI.vars["widgetLocationsTable"].api().ajax.reload(function(json){});
			break;
		case "banners-widget":
			Exxo.UI.vars["widgetBannersTable"].api().ajax.reload(function(json){});
			break;
		case "appSkin-widget":
			Exxo.UI.vars["widgetAppSkinsTable"].api().ajax.reload(function(json){});
			break;
		case "welcomeList-widget":
			Exxo.UI.vars["widgetWelcomeAdsTable"].api().ajax.reload(function(json){});
			break;
		case "offers-widget":
			Exxo.UI.vars["widgetOffersTable"].api().ajax.reload(function(json){});
			break;
		case "documents-widget":
			Exxo.UI.vars["widgetDocumentsTable"].api().ajax.reload(function(json){});
			break;
		case "exxtractors-widget":
			Exxo.UI.vars["widgetExxtractorsTable"].api().ajax.reload(function(json){});
			break;
		case "exxtractors-info-widget":
			Exxo.WidgetsStats.generateExxtractorsInfoChart();
			break;
		case "employees-widget":
			Exxo.UI.vars["widgetEmployeesTable"].api().ajax.reload(function(json){});
			break;
		case "company-widget":
			Exxo.WidgetsCompany.loadCompanyInfo();
			break;
		case "location-widget":
			Exxo.WidgetsLocation.loadLocationInfo();
			break;
		case "waAppTheme-widget":
			Exxo.WidgetsWaAppTheme.loadWaAppThemeInfo();
			break;
		case "shortcuts-widget":
			Exxo.WidgetsShortcuts.loadUserShortcuts();
			break;
		default:
			console.debug("Trying to refresh widget: " + widgetId);
			break;
		}
	},
	saveWidgetsConfig: function(jsonConfig){
		Exxo.showSpinner();
        $.ajax({type:'POST',
              data:'dash=' + Exxo.UI.vars["dashboard-name"] + '&jsonConfig='+jsonConfig,                                  
              url: Exxo.UI.urls["widget-save-config-url"],
              success:function(data) {
                 Exxo.hideSpinner();
                 if(data.error !== false){
                    //There was an error
                	console.debug("msg: " + data.msg);
                 }
                },
              error:function(XMLHttpRequest,textStatus,errorThrown){                              
                   //alert(errorThrown);
                   console.debug("error inesperado");
                   Exxo.hideSpinner();
            }
         });
	},
	init : function() {         
	   $(document).ready(function() {
			
			Exxo.WidgetsCompany.initCompaniesListWidget();
			
			Exxo.WidgetsLocations.initLocationsListWidget();
			
			Exxo.WidgetsBanners.initBannersListWidget();
			
			Exxo.WidgetsAppSkins.initAppSkinsListWidget();
			
			Exxo.WidgetsWelcomeAd.initWelcomeAdsListWidget();
			
			Exxo.WidgetsOffers.initOffersListWidget();
			
			Exxo.WidgetsDocuments.initDocumentsListWidget();
						
			Exxo.WidgetsExxtractors.initExxtractorsListWidget();
			
			Exxo.WidgetsCompany.initCompanyWidget();
			
			Exxo.WidgetsEmployees.initEmployeesListWidget();
			
			Exxo.WidgetsLocation.initLocationWidget();
			
			Exxo.WidgetsStats.initExxtractorsInfoWidget();	
			
			Exxo.WidgetsShortcuts.initShortcutsWidget();
			
			Exxo.WidgetsWaAppTheme.initWaAppThemeWidget();
			
			Exxo.Images.init();
			
			Exxo.UserDashboard.startWidgetOptionsEvents();
			
			$("#how-use-widgets-btn").click(function(e){
				e.preventDefault();
				//Exxo.openHelpWindow('widgets','widget-how-to-use');
				Exxo.openHelpWindow('widgets','widgets');
				return false;
			});
					   		   
		   $('a.widgetMenuIcon').click(function(){
			   alert("Open Widget Menu");
		   });
		   
		   $(".add-widget").click(function(){
			   Exxo.UserDashboard.openAddWidgetWindow();
		   });
		   
			Exxo.UI.vars["gridster"] = $(".gridster ul").gridster({
			          widget_base_dimensions: [450, 30],
					  //widget_base_dimensions: [85, 1],
			          widget_margins: [10, 10],
			          max_size_x:		3,
			          //autogrow_cols: true,
			          resize: {
			            enabled: false,
			            stop: function(event, ui, $widget){
			            	Exxo.UserDashboard.saveWidgetsConfig(Exxo.UI.vars["gridster"].serializeForExxo());
			            }
			          },
			          draggable: {
			            handle: 'header',
			            ignore_dragging: function (event) {
			                return true;
			            },
			            stop: function(event, ui) {
			            	Exxo.UserDashboard.saveWidgetsConfig(Exxo.UI.vars["gridster"].serializeForExxo());
			            }
			          }
			}).data('gridster');
		});
     },
     startWidgetOptionsEvents: function() {
    	// hide the "Show 5 Entries" in datatables
		$(".dataTables_length").hide();
			
    	 $('.widgetInfo').unbind().click(function(e){
				var widget = $(this).parent().parent();
				//Exxo.openHelpWindow('widgets',widget.attr("id"));
				Exxo.openHelpWindow('widgets','widgets');
			});			
			
			$('a.widgetInfo').unbind().click(
				function(e){
					e.preventDefault();
					return false;
				});
		
		   $('a.widgetMin').unbind().click(
			function(e){
				e.preventDefault();				
				Exxo.UserDashboard.minWidget($(this));
				return false;
			});

		   $('a.widgetMax').unbind().click(
			function(e){
				e.preventDefault();
				Exxo.UserDashboard.maxWidget($(this));
				return false;
			});
		   
		   $('a.widgetDelete').unbind().click(
			 function(e){
				e.preventDefault();
				var widget = $(this).parent().parent().parent().parent();
				Exxo.UI.vars["gridster"].remove_widget( widget );
				Exxo.UserDashboard.saveWidgetsConfig(Exxo.UI.vars["gridster"].serializeForExxo());
				return false;
		   });
		   
		   $('a.widgetRefresh').unbind().click(
			function(e){
				e.preventDefault();
				var widget = $(this).parent().parent().parent().parent();
				Exxo.UserDashboard.refreshWidget(widget.attr("id"));
				return false;
		   });
		   
		   $('a.widgetConfig').unbind().click(
			function(e){
				e.preventDefault();
				var widget = $(this).parent().parent().parent().parent();
				Exxo.UserDashboard.loadConfigWindowWidget(widget);
				return false;
		   });
     }
};