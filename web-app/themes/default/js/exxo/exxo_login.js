/***
 @file      exxo_login.js
 @brief     Specific functions for the Login page including the sign up and reset password calls.
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Login = {
	init : function() {
		"use strict";
        $('#Default').perfectScrollbar();
        $('#SuppressScrollX').perfectScrollbar({suppressScrollX: true});
        $('#SuppressScrollY').perfectScrollbar({suppressScrollY: false});
/*
		$('#slides').superslides({
			animation : 'fade',
		      play: false
		});*/

		$('.skin-square input').iCheck({
			checkboxClass : 'icheckbox_square-orange',
			increaseArea : '20%'
		});
		
		Exxo.Login.initButtons();
	},
	initButtons: function(){

       // on click signup It Hide Login Form and Display Registration Form
       $("#backtologin").click(function() {
    	   $(".alert").remove();
          $("#third").slideUp("slow", function() {
             $("#first").slideDown("slow");
          });
       });

       // on click signin It Hide Registration Form and Display Login Form
       $("#signin").click(function() {
          $("#second").slideUp("slow", function() {
             $("#first").slideDown("slow");
          });
       });

       // on click Forgot it goes to the third div
       $("#forgot").click(function() {
          $("#first").slideUp("slow", function() {
             $("#third").slideDown("slow");
          });
       });
       
       $("#forgot-form").submit(function(e){
          e.preventDefault();
          Exxo.Login.resetPassword($(this));
       });
	},
	resetPassword: function($form){
	   Exxo.showSpinner();
	   Exxo.post($form.attr("action"), {username: $("#forgot-name").val()}, 
	         function(data){
	            Exxo.hideSpinner();
	            if(data.error){
	               Exxo.showErrorMessage(data.msg, true, '', false);
	            } else {
	               Exxo.showInfoMessage(data.msg, true, '', false);
	               $("#forgot-name").val("");
	            }
	         }, function(){
	            Exxo.hideSpinner();
	            Exxo.showInfoMessage("There was an error calling the reset function, please try again.", true, '', false);
	         }, true);
	}
};