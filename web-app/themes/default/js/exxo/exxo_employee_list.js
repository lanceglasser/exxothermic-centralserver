/***
 @file      exxo_employee_list.js
 @brief     General functions for the employees list pages
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Employee = {
	init : function() {
	   Exxo.UI.vars['stopEvent'] = true;
	   Exxo.UI.vars['target'] = true;
	   	      
		$('#table-list').dataTable();
		
		$(".changePassword").click(function(e){
			if (Exxo.UI.isSafari()) {
				 Exxo.UI.vars['target'] = $(this);
	       	 } else {
	       		 Exxo.UI.vars['target'] = e.target;
	       	 }
           
           if (Exxo.UI.vars['stopEvent']){
               e.preventDefault();             
               $( "#confirm-reset-dialog" ).dialog("open");            
           } else {
 				Exxo.showSpinner();
 				if (Exxo.UI.isSafari()) {
 					location.href = $(this).attr("href");
 				}
 			}
       });
		
		$(".change-role").click(function(e){
			if (Exxo.UI.isSafari()) {
				 Exxo.UI.vars['target'] = $(this);
	       	 } else {
	       		 Exxo.UI.vars['target'] = e.target;
	       	 }
           
           if (Exxo.UI.vars['stopEvent']){
               e.preventDefault();             
               $( "#confirm-role-change-dialog" ).dialog("open");            
           } else {
 				Exxo.showSpinner();
 				if (Exxo.UI.isSafari()) {
 					location.href = $(this).attr("href");
 				}
 			}
       });
		
		$("#confirm-reset-dialog" ).dialog({
	          height: 250,
	          modal: true,
	          autoOpen: false,
	          buttons: [ {
	                    text: Exxo.UI.translates['okButton'], 
	                    click: function() {
	                        $( this ).dialog( "close" );    
	                        Exxo.UI.vars['stopEvent'] = false;                      
	                        Exxo.UI.vars['target'].click();
	                    }
	                   },
	                { text: Exxo.UI.translates['cancelButton'],   
	                  click: function() {
	                      $( this ).dialog( "close" );                   
	                  }
	              }
	          ]
	        });
		
		$("#confirm-role-change-dialog" ).dialog({
	          height: 200,
	          minWidth: 400,
	          width: 400,
	          modal: true,
	          autoOpen: false,
	          buttons: [ {
	                    text: Exxo.UI.translates['okButton'], 
	                    click: function() {
	                        $( this ).dialog( "close" );    
	                        Exxo.UI.vars['stopEvent'] = false;                      
	                        Exxo.UI.vars['target'].click();
	                    }
	                   },
	                { text: Exxo.UI.translates['cancelButton'],   
	                  click: function() {
	                      $( this ).dialog( "close" );                   
	                  }
	              }
	          ]
	        });
	}
};