/*******************************************************************************
 * @file exxo_metrics_report.js
 * @brief Custom functions and events of the page for the generation of
 *        ExXtractors Usage Reports
 * @author Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Metrics = {
	Charts : {
		extraInit: function() {
			$("#location").select2();
			$("#location").change();
		} ,
		init : function() {

			// Simulates PHP's date function
			Date.prototype.format = function(e) {
				var t = "";
				var n = Date.replaceChars;
				for (var r = 0; r < e.length; r++) {
					var i = e.charAt(r);
					if (r - 1 >= 0 && e.charAt(r - 1) == "\\") {
						t += i
					} else if (n[i]) {
						t += n[i].call(this)
					} else if (i != "\\") {
						t += i
					}
				}
				return t
			};
			
			Date.replaceChars = {
				shortMonths : [ "Jan", "Feb", "Mar", "Apr", "May", "Jun",
						"Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ],
				longMonths : [ "January", "February", "March", "April", "May",
						"June", "July", "August", "September", "October",
						"November", "December" ],
				shortDays : [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ],
				longDays : [ "Sunday", "Monday", "Tuesday", "Wednesday",
						"Thursday", "Friday", "Saturday" ],
				d : function() {
					return (this.getDate() < 10 ? "0" : "") + this.getDate()
				},
				D : function() {
					return Date.replaceChars.shortDays[this.getDay()]
				},
				j : function() {
					return this.getDate()
				},
				l : function() {
					return Date.replaceChars.longDays[this.getDay()]
				},
				N : function() {
					return this.getDay() + 1
				},
				S : function() {
					return this.getDate() % 10 == 1 && this.getDate() != 11 ? "st"
							: this.getDate() % 10 == 2 && this.getDate() != 12 ? "nd"
									: this.getDate() % 10 == 3
											&& this.getDate() != 13 ? "rd"
											: "th"
				},
				w : function() {
					return this.getDay()
				},
				z : function() {
					var e = new Date(this.getFullYear(), 0, 1);
					return Math.ceil((this - e) / 864e5)
				},
				W : function() {
					var e = new Date(this.getFullYear(), 0, 1);
					return Math.ceil(((this - e) / 864e5 + e.getDay() + 1) / 7)
				},
				F : function() {
					return Date.replaceChars.longMonths[this.getMonth()]
				},
				m : function() {
					return (this.getMonth() < 9 ? "0" : "")
							+ (this.getMonth() + 1)
				},
				M : function() {
					return Date.replaceChars.shortMonths[this.getMonth()]
				},
				n : function() {
					return this.getMonth() + 1
				},
				t : function() {
					var e = new Date;
					return (new Date(e.getFullYear(), e.getMonth(), 0))
							.getDate()
				},
				L : function() {
					var e = this.getFullYear();
					return e % 400 == 0 || e % 100 != 0 && e % 4 == 0
				},
				o : function() {
					var e = new Date(this.valueOf());
					e.setDate(e.getDate() - (this.getDay() + 6) % 7 + 3);
					return e.getFullYear()
				},
				Y : function() {
					return this.getFullYear()
				},
				y : function() {
					return ("" + this.getFullYear()).substr(2)
				},
				a : function() {
					return this.getHours() < 12 ? "am" : "pm"
				},
				A : function() {
					return this.getHours() < 12 ? "AM" : "PM"
				},
				B : function() {
					return Math
							.floor(((this.getUTCHours() + 1) % 24
									+ this.getUTCMinutes() / 60 + this
									.getUTCSeconds() / 3600) * 1e3 / 24)
				},
				g : function() {
					return this.getHours() % 12 || 12
				},
				G : function() {
					return this.getHours()
				},
				h : function() {
					return ((this.getHours() % 12 || 12) < 10 ? "0" : "")
							+ (this.getHours() % 12 || 12)
				},
				H : function() {
					return (this.getHours() < 10 ? "0" : "") + this.getHours()
				},
				i : function() {
					return (this.getMinutes() < 10 ? "0" : "")
							+ this.getMinutes()
				},
				s : function() {
					return (this.getSeconds() < 10 ? "0" : "")
							+ this.getSeconds()
				},
				u : function() {
					var e = this.getMilliseconds();
					return (e < 10 ? "00" : e < 100 ? "0" : "") + e
				},
				e : function() {
					return "Not Yet Supported"
				},
				I : function() {
					var e = null;
					for (var t = 0; t < 12; ++t) {
						var n = new Date(this.getFullYear(), t, 1);
						var r = n.getTimezoneOffset();
						if (e === null)
							e = r;
						else if (r < e) {
							e = r;
							break
						} else if (r > e)
							break
					}
					return this.getTimezoneOffset() == e | 0
				},
				O : function() {
					return (-this.getTimezoneOffset() < 0 ? "-" : "+")
							+ (Math.abs(this.getTimezoneOffset() / 60) < 10 ? "0"
									: "")
							+ Math.abs(this.getTimezoneOffset() / 60) + "00"
				},
				P : function() {
					return (-this.getTimezoneOffset() < 0 ? "-" : "+")
							+ (Math.abs(this.getTimezoneOffset() / 60) < 10 ? "0"
									: "")
							+ Math.abs(this.getTimezoneOffset() / 60) + ":00"
				},
				T : function() {
					var e = this.getMonth();
					this.setMonth(0);
					var t = this.toTimeString().replace(/^.+ \(?([^\)]+)\)?$/,
							"$1");
					this.setMonth(e);
					return t
				},
				Z : function() {
					return -this.getTimezoneOffset() * 60
				},
				c : function() {
					return this.format("Y-m-d\\TH:i:sP")
				},
				r : function() {
					return this.toString()
				},
				U : function() {
					return this.getTime() / 1e3
				}
			}

			$("#getDataForm").submit(function(e) {
				e.preventDefault();
				Exxo.showSpinner();
				Exxo.Metrics.Charts.refreshMainCharts();
				e.stopPropagation();
				return false; // Prevent all events including submit
			});

			$("#genFileButton").click(
					function(e) {
						Exxo.showInfoMessage(Exxo.UI.translates["file-generation-notification"], true, '', false);
						$(this).attr(
								"href",
								Exxo.UI.urls["gen-file-url"] + "?locationId="
									+ $("#location").val() + "&startDate="
									+ $("#startDate").val() + "&endDate="
									+ $("#endDate").val());
			});
			
			$( "#startDate" ).datepicker({
		        defaultDate: "-1w",
		        changeMonth: true,
		        numberOfMonths: 1,
		        minDate: "-1Y",
		        onClose: function( selectedDate ) {
		          $( "#endDate" ).datepicker( "option", "minDate", selectedDate );
		        }
		      });
		      $( "#endDate" ).datepicker({
		        defaultDate: "-1D",
		        changeMonth: true,
		        numberOfMonths: 1, 
		        maxDate: "-1D",
		        onClose: function( selectedDate ) {
		          $( "#startDate" ).datepicker( "option", "maxDate", selectedDate );
		        }
		      });
		},
		StringFormat : function() {
			// The string containing the format items (e.g. "{0}")
			// will and always has to be the first argument.
			var theString = arguments[0];

			// start with the second argument (i = 1)
			for (var i = 1; i < arguments.length; i++) {
				// "gm" = RegEx options for Global search (more than one
				// instance)
				// and for Multiline search
				var regEx = new RegExp("\\{" + (i - 1) + "\\}", "gm");
				theString = theString.replace(regEx, arguments[i]);
			}

			return theString;
		},
		refreshMainCharts : function() {
			$("#divError").hide();
			var url = Exxo.UI.urls["get-main-charts-data-url"]
					+ "?locationId=" + $("#location").val() + "&startDate="
					+ $("#startDate").val() + "&endDate=" + $("#endDate").val()
					+ "&selectedDate=" + $("#startDate").val();
			$.getJSON(url, function(data) {
				$("#main-charts-container").hide();
				try {
					if (data.error) {
						$("#time-per-day-charts").hide();
						$("#time-per-day-charts-no-data").show();
						
						$("#max-concurrent-per-day-charts").hide();
						$("#max-concurrent-per-day-charts-no-data").show();
						
						// If there is not data must hide the visual components
						$("#clients-per-day-charts-no-data").show();
						$("#clients-per-day-charts").hide();
						
						$("#errorMessageContainer").html(data.msg);
						$("#divError").show();
					} else {
						if (data.locationsCount > 0) {
							// If there is at least 1 Location must show the charts
							var options = {
								title : "Total: " + data.totalTime,
								width : 650,
								height : 400,
								chartArea : {
									left : 80,
									width : "65%",
									height : "70%"
								},
								hAxis : {
									title : ''/* Exxo.UI.translates["days"] */,
									viewWindowMode : 'explicit',
									textPosition : 'out',
									slantedText : true,
									slantedTextAngle : 90
								},
								vAxis : {
									title : "",
									viewWindowMode : 'explicit',
									textStyle : {
										color : 'black'
									},
									viewWindow : {
										max : data.maximumTimeNumber,
										min : 0
									}
								}
							};

							var data1 = google.visualization
									.arrayToDataTable(data.chartData);
							var chart = new google.visualization.LineChart(
									document.getElementById('time-per-day-charts'));
							chart.draw(data1, options);
							
							var options2 = {
								title : "",
								width : 650,
								height : 400,
								chartArea : {
									left : 80,
									width : "65%",
									height : "70%"
								},
								hAxis : {
									title : ''/* Exxo.UI.translates["days"] */,
									viewWindowMode : 'explicit',
									textPosition : 'out',
									slantedText : true,
									slantedTextAngle : 90
								},
								vAxis : {
									title : "",
									viewWindowMode : 'explicit',
									textStyle : {
										color : 'black'
									}
								}
							};

							var data2 = google.visualization
									.arrayToDataTable(data.sessionsMaxData);
							var chart2 = new google.visualization.LineChart(
									document.getElementById('max-concurrent-per-day-charts'));
							chart2.draw(data2, options2);
							
							$("#max-concurrent-per-day-charts-no-data").hide();
							$("#max-concurrent-per-day-charts").show();
							
							$("#time-per-day-charts-no-data").hide();
							$("#time-per-day-charts").show();
							
							var options3 = {
								legend : 'true',
								width : 650,
								height : 400,
								chartArea : {
									left : 80,
									width : "65%",
									height : "70%"
								},
								hAxis : {
									title : ''/* Exxo.UI.translates["days"] */,
									viewWindowMode : 'explicit',
									/* direction:-1, */
									textPosition : 'out',
									slantedText : true,
									slantedTextAngle : 90
								},
								vAxis : {
									title : "",
									viewWindowMode : 'explicit',
									viewWindow : {
										max : data.maximumDailyClientsNumber,
										min : 0
									}
								}
							};

							var data3 = google.visualization
									.arrayToDataTable(data.dataDaily);
							var chart3 = new google.visualization.LineChart(
									document.getElementById('clients-per-day-charts'));
							chart3.draw(data3, options3);

							$("#clients-per-day-charts-no-data").hide();
							$("#clients-per-day-charts").show();
						} else {
							$("#time-per-day-charts").hide();
							$("#time-per-day-charts-no-data").show();
							
							$("#max-concurrent-per-day-charts").hide();
							$("#max-concurrent-per-day-charts-no-data").show();
							
							// If there is not data must hide the visual components
							$("#clients-per-day-charts-no-data").show();
							$("#clients-per-day-charts").hide();
						}
					}
				} catch (Exception) {
				}
				$("#main-charts-container").show();
				Exxo.hideSpinner();
			});
		}
	}
};