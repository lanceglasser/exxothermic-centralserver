/***
 @file      exxo_user_create.js
 @brief     Custom functions and events for the Company Create Page.
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.User = {
	    Create : {
	       init : function() {
	    	   $("#role").select2();
	       }
	    }
}