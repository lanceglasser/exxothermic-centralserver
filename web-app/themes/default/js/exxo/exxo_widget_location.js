/***
 @file      exxo_widget_location.js
 @brief     Fuctions for the Location Widget
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.WidgetsLocation = {
	initLocationWidget: function(){
		if(Exxo.UI.vars["location-id"] == 0) return;
		
		Exxo.UI.vars["changing-logo"] = false;
		$( "#location-widget" ).exxoWidget({
			helpMsg:	Exxo.UI.translates['widget-location-info-msg'],
			showDashboardTitleRef: 	false,
			allowRemove:			false
		});
		$("#location-extra-btn").click(function(e){
			e.preventDefault();
			$( "#extra-location-info-window" ).dialog("open");
			return false;
		});
		$("#location-edit-btn").click(function(e){
			e.preventDefault();
			window.location = Exxo.UI.urls["edit-location-url"] + Exxo.UI.vars["locationId"];
			return false;
		});
		$("#company-link").click(function(e){
			e.preventDefault();
			window.location = Exxo.UI.urls["company-dashboard-url"] + Exxo.UI.vars["companyId"];
			return false;
		});
		
		// Event when click the change logo link
		$("#change-logo-btn").click(function(e){
			e.preventDefault();
			Exxo.WidgetsLocation.openChangeLogoWindow();
			return false;
		});
		$("#upload-logo-window" ).dialog({
	          height: 500,
	          width: 700,
	          modal: true,
	          autoOpen: false,
	          buttons: [ 
	                { text: Exxo.UI.translates['cancelButton'],
	                  click: function() {
	                	  Exxo.WidgetsLocation.closeUploadLogoWindow(false, "");
	                  }
	                },
	                {
	                	text: Exxo.UI.translates['uploadButton'],
	                	click: function() {
	                		$("#uploadLocationLogo").submit();
	                	}
	                }
	          ]
	    });
		$("#uploadLocationLogo").submit(function(e){
			e.preventDefault();
			Exxo.WidgetsLocation.uploadLogo(this);
		});
		$("#extra-location-info-window" ).dialog({
	          height: 500,
	          width: 800,
	          modal: true,
	          autoOpen: false,
	          buttons: [ 
	                { text: Exxo.UI.translates['closeButton'],
	                  click: function() {
	                	  $( "#extra-location-info-window" ).dialog("close");
	                  }
	                }
	          ]
	    });
		$("#app-preview-window" ).dialog({
	          height: 600,
	          width: 800,
	          modal: true,
	          autoOpen: false,
	          buttons: [ 
	                { text: Exxo.UI.translates['closeButton'],
	                  click: function() {
	                	  $( "#app-preview-window" ).dialog("close");
	                  }
	                }
	          ]
	    });
		$("#app-preview-btn").click(function(e){
			e.preventDefault();
			$( "#app-preview-window" ).dialog("open");
		});
		
		Exxo.WidgetsLocation.loadLocationInfo();
		Exxo.AppDemo.initFullPreview();
	},
	uploadLogo: function($this) {
		var file = $("#bgLogoUrl");
		if($("#"+file.attr('id') + "-updated").val() == 0){
			var errorDiv = document.getElementById('errorDiv');
            errorDiv.innerHTML = '<ul><li>' + Exxo.UI.translates['select-image-before-save'] + '</li></ul>';
            errorDiv.style.display = 'block';
		} else {
			Exxo.showSpinner();
			$.ajax({
				url: Exxo.UI.urls["upload-file"], // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method
				data: new FormData($this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				contentType: false,       // The content type used when sending data to the server.
				cache: false,             // To unable request pages to be cached
				processData:false,        // To send DOMDocument or non processed data file it is set to false
				success: function(data)   // A function to be called if request succeeds
				{
					if(data.error){ //There was an error
						alert(data.msg);
					} else {//Upload success
						Exxo.WidgetsLocation.closeUploadLogoWindow(true, data.url);
						Exxo.hideSpinner();
					}
				},
				error: function()
				{
					Exxo.hideSpinner();
					Exxo.WidgetsLocation.closeUploadLogoWindow(false, "");
				}
			});
		}
	},
	closeUploadLogoWindow: function(mustUpdateImage, imgSrc){
		if(mustUpdateImage){
			$("#location-logo").attr("src", imgSrc);
		}
		Exxo.UI.vars["changing-logo"] = false;
		$( "#upload-logo-window" ).dialog("close");
	},
	loadLocationInfo: function(){
		if(Exxo.UI.vars["location-id"] == 0) return;
		$("#location-info").hide();
		$("#location-loading").show();
        $.ajax({type:'POST',
              data:'object=location&locationId=' + Exxo.UI.vars["location-id"],                                  
              url: Exxo.UI.urls["widget-get-detailed-data-url"],
              success:function(data) {
            	  //console.debug("*** Location Info: " + data);
            	  //console.debug(data);
            	  Exxo.UI.vars["locationId"] = data.locationId;
            	  Exxo.UI.vars["companyId"] = data.companyId;
            	  if(data.locationId !== 0) { //Company was found
            		  var fields = new Array("name", "company", "type", "commercialIntegrator", 
            				  "useCompanyLogo", "contentsLimit", "timezone", "address", "enabled", "createdBy",
            				  "name2", "maxOccupancy", "numberOfTv", "numberOfTvWithExxothermicDevice", "gpsLocation",
            				  "poc");
            		  var index;
            		  for (index = 0; index < fields.length; index++) {
            			    $("#" + fields[index] +"Div").find(".field-value").html(data[fields[index]]);
            		  }
            		  //Logo
            		  if(data.hasLogo){
            			  $("#location-logo").attr("src", data["logo"]);
            		  } else {
            			  $("#location-logo").attr("src", "");
            		  }
            		  if(data.mustUseCompanyLogo){
            			  $("#change-logo-btn").hide();
            			  $("#notLogoChangeMsg").show();
            		  } else {
            			  $("#notLogoChangeMsg").hide();
            			  $("#change-logo-btn").show();
            		  }
            		  
            		  google.maps.event.addDomListener(window, 'load', initialize(data['latitude'], data['longitude'], true));
            		  
            		  $("#location-loading").hide();
            		  $("#location-info").show();
            	  } else {
            		  $("#location-loading").hide();
            		  console.debug("Should hide the Widget");
            	  }
                 //Exxo.hideSpinner();
                 if(data.error == false) {//There was an error
                	 $("#location-loading").hide();
                 }	
              },
              error:function(XMLHttpRequest,textStatus,errorThrown){
                   console.debug("Unexpected error getting location info: " + errorThrown);
                   $("#location-loading").hide();
              }
         });
	}, 
	openChangeLogoWindow: function() {
		if(!Exxo.UI.vars["changing-logo"]){ //Only do something when the window is not already changing the logo
			Exxo.UI.vars["changing-logo"] = true;
			Exxo.Images.resetComponent($("#bgLogoUrl"));
			$("#upload-logo-window").dialog("open");
		}
	}
};