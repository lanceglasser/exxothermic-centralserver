/***
 @file      exxo_box_register.js
 @brief     Custom functions and events for the Box Registration page.
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Box = {
    Register : {
       init : function() {
    	  Exxo.UI.vars["ctrlDown"] = false;
    	  Exxo.UI.vars["ctrlKeyCode"] = 17;
          $("#location").select2();
          
          $("#wizardForm").submit(function(e){
        	  Exxo.showSpinner();
          });
          $('#serial').bind('input propertychange', function() {
        	  $('#serial').val($(this).val().replace(/\s/g, "").toUpperCase()); 
          });
          
          $('#serial').keydown(function (e) {
        	 if (e.keyCode === Exxo.UI.vars["ctrlKeyCode"]) {
        		 Exxo.UI.vars["ctrlDown"] = true;
        	 }
             var allowEvent = SerialUpperCase(e.keyCode, e.ctrlKey, e.shiftKey);
             if(!allowEvent) {
            	 e.preventDefault();
            	 e.stopPropagation();
            	 return false;
             }
          });
          
          $('#serial').keyup(function (e) {
        	  var result = Exxo.Box.Register.removeSpaces(e.keyCode, e.ctrlKey, e.shiftKey);
        	  if (e.keyCode === Exxo.UI.vars["ctrlKeyCode"]) {
        		  Exxo.UI.vars["ctrlDown"] = false;
        	  }
        	  if(!result) {
        		  e.preventDefault();
        		  return false;
        	  }
           });
          
          $('#serial').blur(function () {
             $('#serial').val($('#serial').val().replace(/\s/g, "").toUpperCase()); 
          });
       },
       removeSpaces: function(keyCode, ctrlKey, shiftKey) {
    	   if( (Exxo.UI.vars["ctrlDown"] && keyCode == 17) ) { // Allow: Ctrl+V
    		   var fixedValue = $('#serial').val().replace(/\s/g, "").toUpperCase();
    		   $('#serial').val(fixedValue);
    		   return false;
		   }
    	   return true;
       }
    }
};

function SerialUpperCase(keyCode, ctrlKey, shiftKey){
	// Block keys: space[32]
	if ($.inArray(keyCode, [32]) !== -1) {
		return false;
	}
    if (
   		// Allow: backspace:8, delete:46, escape:27, enter:13, shift:16, ctrl:17
    		$.inArray(keyCode, [8, 46, 27, 13, 16, 17, 110]) !== -1 ||
    	// Allow: Ctrl+V
            //(keyCode == 17 || ctrlKey === true) || 
        // Allow: Ctrl+A
            (keyCode == 16 || shiftKey === true) ||
        // Allow: home, end, left, right
            (keyCode >= 35 && keyCode <= 39)) {
             // let it happen, don't do anything
             return true;
             //shiftKey === true ||
    }
    $('#serial').val($('#serial').val().toUpperCase());
    return true;
}

