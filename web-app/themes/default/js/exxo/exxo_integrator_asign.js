/***
 @file      exxo_integrator_asign.js
 @brief     Custom functions and events for the Company - Asign Integrator page
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Company = {
    AsignIntegrator : {
       init : function() {
    	   
          $("#integrator").select2();
          console.debug("Trigger change of combo of integrators");
          $("#integrator").change();
          
          $("#createForm").submit(function(e) {
              Exxo.showSpinner();
           });
       }
    }
};