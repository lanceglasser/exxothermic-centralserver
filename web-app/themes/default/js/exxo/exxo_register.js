/***
 @file      exxo_register.js
 @brief     Specific functions for the Account Registration.
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Register = {
	init : function() {
	    //Hack for the registration form
	    $("#main").css("width","600px");
	    $("#first").css("width","600px");
	    $("#SuppressScrollX").css("width", "500px");
	    $(".contentHolder .content").css("width", "500px");
	    $(".contentHolder .content form").css("width", "500px");
	    //End of Hacks
	    
		"use strict";
        $('#Default').perfectScrollbar();
        $('#SuppressScrollX').perfectScrollbar({suppressScrollX: true});
        $('#SuppressScrollY').perfectScrollbar({suppressScrollY: false});
        
        $('#slides').superslides({
           animation : 'fade',
           play: false
        });

        $('.skin-square input').iCheck({
           checkboxClass : 'icheckbox_square-orange',
           increaseArea : '20%'
        });
       
        Exxo.UI.vars['code'] = "";
        
        Exxo.UI.vars['code'] = $( "#type").find(":selected").attr("id");
        
        $("#type,#country,#state,#typeOfCompany,#numberOfEmployees").select2();
                
        
        if( Exxo.UI.vars['code'] ==='other' || Exxo.UI.vars['code'] ==='waitingRoomSpecify')
        {
            $("#controlCompanyOtherType").show();
            $("#otherType").attr("required","required");
            
        }
        else
        {
            $("#controlCompanyOtherType").hide();
            $("#otherType").removeAttr("required");
        }
       
        
        $("#type").change(function(){
           console.debug("On Change of Type!!!");
           Exxo.UI.vars['code'] = $( "#type").find(":selected").attr("id");
            
            if(Exxo.UI.vars['code'] === 'other' || Exxo.UI.vars['code'] === 'waitingRoomSpecify')
            {
                $("#controlCompanyOtherType").show();
                $("#otherType").attr("required","required");
                
            }
            else
           {
                $("#controlCompanyOtherType").hide();
                $("#otherType").removeAttr("required");
           }
        });
        
        
        $("#webSite").change(function(){
            if(this.value.substr(0,7) != 'http://'){
                this.value = 'http://' + this.value;
              }
              if(this.value.substr(this.value.length-1, 1) != '/'){
                  this.value = this.value + '/';
              }
        });
        
        $("#typeOfCompany").change(function(){
            console.debug("On Change of TypeOfCompany: " + $("#typeOfCompany option:selected").text());
            if("Integrator"==$("#typeOfCompany option:selected").text())
            {
                $(".form-extrainformation").slideDown(500);           
                $("#type").removeAttr("required");
                $("#type").attr("disabled",true);
                $("#otherType").removeAttr("required");
                $("#otherType").attr("disabled",true);
                $("#controlCompanyOtherType").hide();
                $("#controlTypeOfCompany").hide();
                
            }
            else
            {
                $(".form-extrainformation").slideUp(500); 
                $("#type").attr("required","required");
                $("#type").removeAttr("disabled");
                $("#otherType").removeAttr("disabled");
                $('#otherType').attr("readonly", false);
                $("#controlTypeOfCompany").show();
                $("#type").trigger('change');         
            }    
        });

        
        $("#country").change(function(){
           Exxo.UI.setInputsEventsByCountry($(this).val());               
            $.getJSON(Exxo.UI.urls['getStatesByCountry'] + "?id="+$(this).val(),function(data){
                try
                {
                    if(data.states.length > 0 ){
                        $("#stateNameDiv").hide();
                        $("#stateDiv").show();
                        //remove the plugin select2
                        $("#state").select2("destroy");
                        //add the data
                        $("#state").setSelectOptions(data.states,"id","name");
                        //restart the pluggin select2
                        $("#state").select2();
                        //Trigger to change the City select box
                        $("#state").change();
                    }else{
                        $("#stateNameDiv").show();
                        $("#stateDiv").hide();
                    }
                }
                catch(Exception){}
               
            });
        });
        
        $("#typeOfCompany").trigger('change');
	}
};