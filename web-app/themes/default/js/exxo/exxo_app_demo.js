/***
 @file      exxo_app_demo.js
 @brief     Functions for the App Demo View
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.AppDemo = {
	init: function(){
		$(".app-tabs .tab").click(function(e){
			console.debug();
			$(".app-tabs .tab").removeClass("current");
			$(this).addClass("current");
			$(".phone-app .app-tabs .current").css('border-bottom-color', Exxo.UI.vars["demo-second-color"]);
			$(".phone-app .tab-container").hide();
			$(".phone-app .tab-container-part").hide();
			
			$("#APP-DEMO-DIV-" + $(this).find('span').html()).show();
			$(".APP-DEMO-PART-" + $(this).find('span').html()).show();
		});
	},
    renderInitialAppPreview: function(){
  	  Exxo.AppDemo.setMainColorOfAppPreview(Exxo.UI.vars["main-color"]);
  	  Exxo.AppDemo.setSecondColorOfAppPreview(Exxo.UI.vars["second-color"]);
  	  
	  	if(Exxo.UI.vars['back-img'] != ""){
	  		$("#app-skin-background-img").attr("src", Exxo.UI.vars['back-img']);
	  		$("#app-skin-background-img").show();
	    }
	  	
	  	if(Exxo.UI.vars['dialog-img'] != ""){
	  		$("#dialog-img").attr("src", Exxo.UI.vars['dialog-img']);
	  		$("#dialog-img").show();
	    }
	  	
	  	Exxo.AppDemo.updateGenerationOptions();
    },
    initOfferOrBannerPreview: function(){
    	Exxo.AppDemo.renderInitialAppPreview();
        Exxo.AppDemo.setBannerOrOfferColor();
        Exxo.AppDemo.setOfferDemoTitle($('#title').val());
        Exxo.AppDemo.setOfferDemoText($('#description').val());
        Exxo.AppDemo.setBannerOrOfferExpiration($('#end_date').val());
        Exxo.AppDemo.setOfferOrBannerMoreInfo();
        Exxo.AppDemo.setAvailableOptions();
        Exxo.AppDemo.setBannerType($('#title').val());
        if(Exxo.UI.vars['is-offer']){
        	Exxo.AppDemo.setOfferAsBannerIfApplies();
            Exxo.AppDemo.setOfferAsDocumentIfApplies();
            $("#dialog-view-available").show();
        } else {
        	$("#dialog-view-available").hide();
        	Exxo.AppDemo.setBannerImg();
        }
        
        if(Exxo.UI.vars['dialog-img'] != ""){
        	if(Exxo.UI.vars['is-offer']){
        		$("#offer-demo-thumbnail-img").attr("src", Exxo.UI.vars['dialog-img']);
            	$("#offer-demo-thumbnail-img").show();
            	$("#dialog-img").attr("src", Exxo.UI.vars['dialog-img']);
            	$("#dialog-img").show();
        	} else { //It's a banner
            	$("#dialog-img").attr("src", Exxo.UI.vars['dialog-img']);
            	$("#dialog-img").show();
        	}
        } else {
        	$("#offer-demo-thumbnail-img").hide();
        }
        
        if(Exxo.UI.vars['banner-img'] != ""){
        	if(Exxo.UI.vars['is-offer']){
        		$("#gen-dialog-img").attr("src", Exxo.UI.vars['banner-img']);
            	$("#offer-gen-thumbnail-img").attr("src", Exxo.UI.vars['banner-img']);
        	} else { //It's a banner
            	$("#gen-dialog-img").attr("src", Exxo.UI.vars['banner-img']);
        	}
        }
        
        if($("#radioPDF").is(':checked')){
        	$("#dialog-view-more-btn").show();
        }
        
        $("#dialog-view-more-btn").click(function(e){
  	  		e.preventDefault();
  	  		if(Exxo.UI.vars['is-offer']){
	  			Exxo.showInfoMessage('This will open the pdf file', true, '', false);
	  		} else {
	  			var url = $(this).attr("url");
	  	  		if(url != undefined && url != ""){
	  	  			Exxo.showInfoMessage('Will open the file; if you want to see it click <a target="_blank" href="' + 
	  	  					url + '">Here</a>', true, '', false);
	  	  		}
	  		}
  	  	});
        
        Exxo.AppDemo.init();
    },
    initFullPreview: function(){
    	Exxo.AppDemo.setMainColorOfAppPreview(Exxo.UI.vars["main-color"]);
    	Exxo.AppDemo.setSecondColorOfAppPreview(Exxo.UI.vars["second-color"]);
    	  
  	  	if(Exxo.UI.vars['back-img'] != ""){
  	  		$("#app-skin-background-img").attr("src", Exxo.UI.vars['back-img']);
  	  		$("#app-skin-background-img").show();
  	    }
  	  	
  	  	if(Exxo.UI.vars['dialog-img'] != ""){
  	  		$("#dialog-img").attr("src", Exxo.UI.vars['dialog-img']);
  	  		$("#dialog-img").show();
  	    }
  	  	
  	  	$(".docs-container .doc").click(function(e){
  	  		e.preventDefault();
  	  		Exxo.showInfoMessage('Will download the file, if you want download it click <a href="' 
  	  				+ $(this).attr('ref') + '" target="_blank">Here</a>', true, '', false);
  	  	});
  	  	
  	  $(".offers-container .offer").click(function(e){
	  		e.preventDefault();
	  		$("#app-main-view").hide();
	  		
			var app = $(".phone-app");
			var $this = $(this);
			app.find(".dialog-app-header .title").html($this.attr('title'));
			app.find("#dialog-description").html($this.attr('desc'));
			$("#dialog-view-expiration").html($this.find(".expiration").html());
			
			var hours = $this.attr('hours');			
			var days = $this.attr('days');
			
			var daysLabels = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
	       	var availableText = "";
	       	var selectedDays = days.split(",");
	       	for(var i=0; i < selectedDays.length - 1; i++){
	       		availableText += (availableText === "")? daysLabels[selectedDays[i*1]]:', '+daysLabels[selectedDays[i*1]];
	       	}
	       	if(selectedDays.length > 1){
	       		availableText += " and " + daysLabels[selectedDays[selectedDays.length - 1]];
	       	}
	       	availableText = "Available " + availableText + "<br>between " + hours;
	       	
	       	$("#dialog-view-available").html(availableText);
	       	
	       	var url = $this.attr('url').trim();
	       	$("#dialog-view-more-btn").attr('url',url);
	       	if(url === ""){
	       		$("#dialog-view-more-btn").hide();
	       	} else {
	       		$("#dialog-view-more-btn").show();
	       	}
			
	       	//Change Dialog Image
	       	$("#dialog-img").attr('src', $this.attr('dialog'));
	  		$("#app-detail-view").show();
	  	});
  	  
  	  	$(".phone-app .dialog-app-header").click(function(e){
  	  		e.preventDefault();
  	  		$("#app-detail-view").hide();
  	  		$("#app-main-view").show();
  	  	});
  	  	
  	  	$("#dialog-view-more-btn").click(function(e){
  	  		e.preventDefault();
  	  		if(Exxo.UI.vars['is-offer']){
  	  			Exxo.showInfoMessage('Will open the file; if you want to see it click <a target="_blank" href="' + 
	  					url + '">Here</a>', true, '', false);
  	  		} else {
	  	  		var url = $(this).attr("url");
	  	  		if(url != undefined && url != ""){
	  	  			Exxo.showInfoMessage('Will open the file; if you want to see it click <a target="_blank" href="' + 
	  	  					url + '">Here</a>', true, '', false);
	  	  		}
  	  		}
  	  	});
  	  	
  	  	// Init slidding banners
  	  	var optionsSlider = {

              $AutoPlay: true,                //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
              $DragOrientation: 0,            //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)
              $AutoPlayInterval: 5000,        //[Optional] Interval to play next slide since the previous stopped if a slideshow is auto playing, default value is 3000
              $PauseOnHover: 3
          };

        var jssor_slider1 = new $JssorSlider$("slider_banners_container", optionsSlider);

        //When click a banner
        $(".general-banner-container").click(function(e){
        	var $this = $(this);
        	var app = $(".phone-app");
        	var type = $this.attr("type");
        	
        	if(type == "text"){
        		app.find(".dialog-app-header .title").html($("#app-title").html() + " MESSAGE");
        		$("#dialog-img").attr('src', '');
        		$("#dialog-view-expiration").hide();
        		$("#dialog-view-available").hide();
        	} else if(type == "image") {
        		app.find(".dialog-app-header .title").html($this.attr('title'));
        		$("#dialog-img").attr('src', $this.attr('dialog'));
        		$("#dialog-view-expiration").hide();
        		$("#dialog-view-available").hide();
        	} else { //It's an Offer
        		app.find(".dialog-app-header .title").html($this.attr('title'));
        		$("#dialog-img").attr('src', $this.attr('dialog'));
        		$("#dialog-view-expiration").show();
        		$("#dialog-view-available").show();
        		
        		$("#dialog-view-expiration").html($this.find(".expiration").html());
    			
    			var hours = $this.attr('hours');			
    			var days = $this.attr('days');
    			
    			var daysLabels = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    	       	var availableText = "";
    	       	var selectedDays = days.split(",");
    	       	for(var i=0; i < selectedDays.length - 1; i++){
    	       		availableText += (availableText === "")? daysLabels[selectedDays[i*1]]:', '+daysLabels[selectedDays[i*1]];
    	       	}
    	       	if(selectedDays.length > 1){
    	       		availableText += " and " + daysLabels[selectedDays[selectedDays.length - 1]];
    	       	}
    	       	availableText = "Available " + availableText + "<br>between " + hours;
    	       	
    	       	$("#dialog-view-available").html(availableText);
    	       	$("#dialog-view-expiration").html("Expires: " + $this.attr('expiration'));
        	}
        	
        	app.find("#dialog-description").html($this.attr('desc'));
        	
        	var url = $this.attr('url').trim();
	       	$("#dialog-view-more-btn").attr('url',url);
	       	if(url === ""){
	       		$("#dialog-view-more-btn").hide();
	       	} else {
	       		$("#dialog-view-more-btn").show();
	       	}
	       	
        	$("#app-main-view").hide();
	  		$("#app-detail-view").show();
        });
  	  	
  	  	Exxo.AppDemo.updateGenerationOptions();
    	Exxo.AppDemo.init();
    },
    updateGenerationOptions: function(){
    	if($("#genDialogImages").is(':checked') && $("#type").val() !== "text"){
    		$(".phone-app .dialog-img-div").hide();
        	$(".phone-app .gen-dialog-img-div").show();
        	
        	$(".phone-app .offer-thumbnail-img-div").hide();
        	$(".phone-app .gen-offer-thumbnail-img-div").show();
    	} else {
    		$(".phone-app .gen-dialog-img-div").hide();
    		$(".phone-app .dialog-img-div").show();
    		
    		$(".phone-app .gen-offer-thumbnail-img-div").hide();
    		$(".phone-app .offer-thumbnail-img-div").show();
    	}
    },
    setBannerBackgroundColor: function(color){
    	if(color === undefined){
    		color = "#989696";    	  
        }
    	Exxo.UI.vars['banner-background-color'] = color;
    	$("#app-skin-background").css('background-color', Exxo.UI.vars['banner-background-color']);
    },
    setMainColorOfAppPreview: function(mainColor){
      if(mainColor === undefined){
    	  mainColor = "#989696";    	  
      }
      Exxo.UI.vars["main-color"] = mainColor;
  	  $(".phone-app .main-color").css('background-color', mainColor);
  	  $(".phone-app .main-color").css('border-left-color', mainColor);
  	  $(".main-color-part").css('background-color', mainColor);
    },
    setSecondColorOfAppPreview: function(secondColor){
    	if(secondColor === undefined){
    		secondColor = "#989696";
        }
    	Exxo.UI.vars["demo-second-color"] = secondColor;
  	  	$(".phone-app .app-tabs .current").css('border-bottom-color', secondColor);
  	  	$(".phone-app .docs-container .header").css('background-color', secondColor);
  	  	$(".second-color-part").css('background-color', secondColor);
    },
    setBannerType: function(text){
    	if(Exxo.UI.vars['is-offer']){
    		$("#offer-demo").find('.title').html(text);
    		$(".phone-app .dialog-app-header .title").html(text);
    		$(".phone-app .text-banner-container").hide();
    		$("#app-skin-background-img").show();
    		$("#app-skin-background").css('background-color', $("#color").val());
    	} else {
    		if($("#type").val() == 'text'){
    			$(".phone-app .dialog-app-header .title").html("APP THEME TITLE MESSAGE");
    			var text = $("#description").val();
    			if(text.length > 125){
    	    		text = text.substring(0, 128) + " ...";
    	    	}
    			$(".phone-app .text-banner-msg-box").html(text);
    			$(".phone-app .text-banner-container").show();
    			$("#app-skin-background-img").hide();
    			$("#app-skin-background").css('background-color', 'rgb(152, 150, 150)');
    		} else {
    			$(".phone-app .dialog-app-header .title").html(text);
    			$(".phone-app .text-banner-container").hide();
    			$("#app-skin-background-img").show();
    			$("#app-skin-background").css('background-color', $("#color").val());
    		}
    	}
    	Exxo.AppDemo.updateGenerationOptions();
    },
    setOfferDemoTitle: function(text){
    	if(Exxo.UI.vars['is-offer']){
    		$(".docs-container .demo .doc-title").html(text);
    		$("#offer-demo").find('.title').html(text);
    		$(".phone-app .dialog-app-header .title").html(text);
    	} else {
    		if($("#type").val() == 'text'){
    			$(".phone-app .dialog-app-header .title").html("APP THEME TITLE MESSAGE");
    		} else {
    			$(".phone-app .dialog-app-header .title").html(text);
    		}
    	}
    },
    setOfferDemoText: function(text){
    	$(".phone-app #dialog-description").html(text);
    	if(text.length > 125){
    		text = text.substring(0, 128) + " ...";
    	}
    	$(".phone-app .text-banner-msg-box").html(text);
    },
    setBannerOrOfferColor: function(color){
    	if(color === undefined){
    		color = "#989696";
        }
    	var color = $("#color").val();
    	Exxo.UI.vars['banner-offer-color'] = color;
    	if(Exxo.UI.vars['is-offer']){
    		if($('#featured').is(':checked')){
    			$("#app-skin-background").css('background-color', Exxo.UI.vars['banner-offer-color']);
    		}
    		$("#offer-demo .offer-thumbnail").css('background-color', Exxo.UI.vars['banner-offer-color']);
    		//$("#app-skin-background").css('background-color', Exxo.UI.vars['banner-background-color']);
    	} else {
    		if($("#type").val() == 'text'){
    			$("#app-skin-background").css('background-color', 'rgb(152, 150, 150)');
    		} else {
    			$("#app-skin-background").css('background-color', Exxo.UI.vars['banner-offer-color']);
    		}
    	}
    	$(".phone-app .gen-dialog-img-div").css('background-color', Exxo.UI.vars['banner-offer-color']);
    	
    },
    setBannerImg: function(){
    	var ref = $("#featuredImageFile").attr("ref");
		var img = $("#target"+ref);
		if(img != undefined && img.attr("src") != undefined){
			var imgSrc = img.attr("src").trim();
    		if(imgSrc === ""){
    			//$("#app-skin-background-img").hide();
    			//$("#app-skin-background-img").attr("src", imgSrc);
    		} else if(imgSrc.indexOf("http") == 0){
    			//$("#app-skin-background-img").show();
    			//$("#app-skin-background-img").attr("src", imgSrc);
    		}
    		$("#app-skin-background-img").show();
    		$("#app-skin-background-img").attr("src", imgSrc);
		} else {
			$("#app-skin-background-img").attr("src", "");
		}
		if($("#type").val() == 'text'){
			$("#app-skin-background").css('background-color', 'rgb(152, 150, 150)');
		} else {
			$("#app-skin-background").css('background-color', $("#color").val());
		}
    },
    setOfferAsBannerIfApplies: function(){
    	if($('#featured').is(':checked')){
    		$("#featuredImageFile").attr("update1", 1);
    		Exxo.AppDemo.setBannerImg();
    	} else {
    		$("#featuredImageFile").attr("update1", 0);
    		$("#app-skin-background").css('background-color', "#000");
    		$("#app-skin-background-img").attr("src","").hide();
    	}
    },
    setOfferAsDocumentIfApplies: function(){
    	if($('#isFile').is(':checked')){
    		//$("#APP-DEMO-DIV-INFORMATION .example").hide();
    		$("#APP-DEMO-DIV-INFORMATION .demo .doc-title").html($("#title").val().trim());
    		$("#APP-DEMO-DIV-INFORMATION .demo").show();
    	} else {
    		//$("#APP-DEMO-DIV-INFORMATION .example").show();
    		$("#APP-DEMO-DIV-INFORMATION .demo").hide();
    	}
    },
    setOfferOrBannerMoreInfo: function(){
    	if(Exxo.UI.vars['is-offer']){
    		//if($('#isFile').is(':checked')){
    		if($('#radioPDF').is(':checked')){//If it's a PDF must show the button
    			$("#dialog-view-more-btn").show();
    		} else {
    			if($('#radioURL').is(':checked')){
    				var urlReference = $("#urlReference").val().trim();
    				if(urlReference == "" || urlReference == "http://"){ // There is not a URl, must hide the button
    					$("#dialog-view-more-btn").hide();
    				} else { // There is a URL, show the button    					
    					$("#dialog-view-more-btn").attr("url", encodeURI(urlReference));
    					$("#dialog-view-more-btn").show();
    				}
        		} else {
        			//console.debug("URL is not Checked");
        		}
    		}
    		$("#offer-demo .offer-thumbnail").css('background-color', Exxo.UI.vars['banner-offer-color']);
    		//$("#app-skin-background").css('background-color', Exxo.UI.vars['banner-background-color']);
    	} else {
    		var urlReference = $("#urlReference").val().trim();
			if(urlReference == "" || urlReference == "http://"){ // There is not a URl, must hide the button
				$("#dialog-view-more-btn").hide();
			} else { // There is a URL, show the button
				$("#dialog-view-more-btn").attr("url", encodeURI(urlReference));
				$("#dialog-view-more-btn").show();
			}
    	}
    },
    setBannerOrOfferExpiration: function(text){
    	text = text.trim();
    	text = (text.trim() === "")? "Never":text;
    	if(Exxo.UI.vars['is-offer']){
    		$("#offer-demo .expiration").html("Expires: " + text);
    		$("#dialog-view-expiration").html("Expires: " + text);
    	} else {
    		$("#dialog-view-expiration").html("");
    	}
    },
    setAvailableOptions: function(){
    	if(Exxo.UI.vars['is-offer']){
    		var item = selectedDays = [];
	   		 $(".scheduler .days").find("li").each(
	   				 function(index) { 
	   					 if($(this).attr("class") == "selected"){selectedDays = selectedDays.concat(index)} });		 
	       	var daysLabels = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
	       	var availableText = "";
	       	for(var i=0; i < selectedDays.length; i++){
	       		availableText += (availableText === "")? daysLabels[selectedDays[i]]:', '+daysLabels[selectedDays[i]];
	       	}
	       	availableText += " between ";
	       	var start = $(".hours_range .start").val() * 1;
	       	var end = $(".hours_range .end").val() * 1;
	       	if(start == 0){
	       		availableText += "12:00 AM and ";
	       	} else if(start < 12){
	       		availableText += start + ":00 AM and ";
	       	} else if(start == 12){
	       		availableText += "12:00 PM and ";
	       	} else {
	       		availableText += (start-12) + ":00 PM and ";
	       	}
	       	if(end == 0){
	       		availableText += "11:59 PM";
	       	} else if(end < 12){
	       		availableText += end + ":00 AM";
	       	} else if(end == 12){
	       		availableText += "12:00 PM";
	       	} else {
	       		availableText += (end-12) + ":00 PM";
	       	}
	       	
	       	$("#dialog-view-available").html("Available " + availableText);
    	} else { //It's a Banner; not show this option
    		$("#dialog-view-available").html("");
    	}
    }
};