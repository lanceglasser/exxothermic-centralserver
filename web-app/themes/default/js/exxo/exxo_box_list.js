/*******************************************************************************
 * @file exxo_box_list.js
 * @brief Custom functions and events for the Box List Page.
 * @author Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Box = {
	List : {
		init : function() {

			Exxo.UI.vars['stopEvent'] = true;
			Exxo.UI.vars['target'] = null;

			$("#confirm-reset-dialog").dialog({
				height : 140,
				width : 400,
				modal : true,
				autoOpen : false,
				buttons : [ {
					text : Exxo.UI.translates['okButton'],
					click : function() {
						$(this).dialog("close");
						Exxo.UI.vars['stopEvent'] = false;
						$(Exxo.UI.vars['target']).trigger("click");
					}
				}, {
					text : Exxo.UI.translates['cancelButton'],
					click : function() {
						$(this).dialog("close");
					}
				} ]
			});

			$("#confirm-restore-dialog").dialog({
				height : 140,
				width : 400,
				modal : true,
				autoOpen : false,
				buttons : [ {
					text : Exxo.UI.translates['okButton'],
					click : function() {
						$(this).dialog("close");
						Exxo.UI.vars['stopEvent'] = false;
						$(Exxo.UI.vars['target']).trigger("click");
					}
				}, {
					text : Exxo.UI.translates['cancelButton'],
					click : function() {
						$(this).dialog("close");
					}
				} ]
			});

			$("#confirm-retire-dialog").dialog({
				height : 140,
				width : 400,
				modal : true,
				autoOpen : false,
				buttons : [ {
					text : Exxo.UI.translates['okButton'],
					click : function() {
						$(this).dialog("close");
						Exxo.UI.vars['stopEvent'] = false;
						$(Exxo.UI.vars['target']).trigger("click");
					}
				}, {
					text : Exxo.UI.translates['cancelButton'],
					click : function() {
						$(this).dialog("close");
					}
				} ]
			});

			$("#confirm-factory-reset-dialog").dialog({
				height : 140,
				width : 400,
				modal : true,
				autoOpen : false,
				buttons : [ {
					text : Exxo.UI.translates['okButton'],
					click : function() {
						$(this).dialog("close");
						Exxo.UI.vars['stopEvent'] = false;
						$(Exxo.UI.vars['target']).trigger("click");
					}
				}, {
					text : Exxo.UI.translates['cancelButton'],
					click : function() {
						$(this).dialog("close");
					}
				} ]
			});
			
			var columnSort = new Array; 
			$('#exxtractorsTable').find('thead tr th').each(function(){
		       if($(this).attr('data-bSortable') == 'false' || $(this).html().trim() === "Actions"){
		          columnSort.push({ "bSortable": false });
		       } else {
                  columnSort.push({ "bSortable": true });
               }
		    });
			   
			Exxo.UI.vars["widgetExxtractorsTable"] = $('#exxtractorsTable')
					.dataTable({
						"dom": '<"registerBox">flrtip',
						"aoColumns": columnSort,
				        "pageLength":  		(Exxo.UI.vars["rowsPerPage"] != undefined)? Exxo.UI.vars["rowsPerPage"]:Exxo.UI.vars["defaultTableRowsPerPage"],
				        "displayStart":		(Exxo.UI.vars["firstRecord"] != undefined)? Exxo.UI.vars["firstRecord"]:0,
		        		"search": {
			    			"search": 		(Exxo.UI.vars["tableFilter"] != undefined)? Exxo.UI.vars["tableFilter"]:''
		        		},
		        		"order": 			[ Exxo.UI.vars["columnOrder"], Exxo.UI.vars["orderDir"] ],
						"processing" : true,
						"serverSide" : true,
						"ajax" : {
							"url" : Exxo.UI.urls["widget-get-data-url"],
							"data" : function(d) {
								d.object = 'exxtractor-full';
								d.companyId = 0;
								d.locationId = 0;
							}
						},
						"fnDrawCallback": function( ) {
							Exxo.Tables.saveTableInfo();
							Exxo.Box.List.initializeEvents();
							$("div.registerBox").html(' <a id="registerLink" href="'+Exxo.UI.urls['registerBoxURL']+'">'+Exxo.UI.translates['registerBox']+'</a> ');
					    	if($(".dataTables_empty").length > 0){
					    		$("#registerLink").show();
					    		$("#registerLink").attr("href", Exxo.UI.urls['registerBoxURL']+'?serial='+$("#table-search-input").val());
					    	} else {
					    		$("#registerLink").hide();
					    	}
					    },
						"columnDefs" : [
								{
									"targets" : [ 0 ],
									"render" : function(data, type, row) {
										if (Exxo.UI.vars["is-admin"]) {
											return row[0];
										} else {
											return '<a href="' + Exxo.UI.urls["exxtractors-edit-url"]
												+ row[6] + '">' + row[0] + '</a>';
										}
										
									}
								},
						        {
									"targets" : [ 1 ],
									"render" : function(data, type, row) {
										return row[1] == ""? Exxo.UI.translates['emptyText']:row[1];
									}
								},
								{
									"targets" : [ 2 ],
									"render" : function(data, type, row) {
										return row[2];
									}
								},
								{
									"targets" : [ 3 ],
									"render" : function(data, type, row) {
										return row[3];
									}
								},
								{//Last Seen
									"targets" : [ 4 ],
									"render" : function(data, type, row) {
										if (row[5]) {
											return '<i class="state stateConnected" title="' + row[4] + '"></i>'
											+ '<span class="greenText">'
											+ row[4] + '</span>';
										} else {
											return '<i class="state stateDisconnected" title="' + row[4] + '"></i>'
											+ ' <span class="redText">'
											+ row[4] + '</span>';
										}
									}
								},
								{
									"targets" : [ 5 ],
									"render" : function(data, type, row) {
										// Show icon for everybody
										/*var html = '<a href="' + Exxo.UI.urls["exxtractors-edit-url"]  + row[6] + 
											'" class="action-icons view" title="View"></a>';*/
										var html = Exxo.UI.vars["action-show-link"];
										if (!Exxo.UI.vars["is-admin"]) { // Edit icon only for no Admin
											html += Exxo.UI.vars["action-edit-link"];
										}
										if (row[5]) { // If it's connected
											if (Exxo.UI.vars["is-admin-or-helpdesk"]) { //It's admin or help desk
												// Box List Log Files
												html += Exxo.UI.vars["action-logs-link"];
											}
											// BoxSoftware Update Version
											html += Exxo.UI.vars["action-software-link"];
											// Box Remote Reset
											html += Exxo.UI.vars["action-reset-link"]
										}
										if (Exxo.UI.vars["is-admin-or-helpdesk"]) { //It's admin or help desk
											// Retire Box
											html += Exxo.UI.vars["action-retire-link"];
										}
										html = html.replaceAll("BOX_ID", row[6]);
										return html;
									}
								},
					            {
					                "targets": [ 6 ],
					                "visible": false
					            }]
					});
			$(".dataTable thead th").click(function(e){
				if(!$(this).hasClass("sorting_disabled")){
					Exxo.UI.vars["columnOrder"] = $(".dataTable thead th").index($(this));
					Exxo.UI.vars["orderDir"] = ($(this).hasClass("sorting_asc"))? "desc":"asc";
				}
			});
			setInterval(function(){ Exxo.UI.vars["widgetExxtractorsTable"].api().ajax.reload(function(json){}); }, 60*1000); // Every 1 minute
		},
		initializeEvents : function() {
			$(".delete").click(function(e) {

				Exxo.UI.vars['target'] = e.target || e.srcElement;

				if (Exxo.UI.vars['stopEvent']) {
					e.preventDefault();
					$("#confirm-retire-dialog").dialog({
						height : 175
					}).dialog("open");
				} else {
					Exxo.showSpinner();
					Exxo.UI.vars['stopEvent'] = true;
					window.location.href = Exxo.UI.vars['target'];
				}
			});

			$(".reset").click(function(e) {
				Exxo.UI.vars['target'] = e.target || e.srcElement;

				if (Exxo.UI.vars['stopEvent']) {
					e.preventDefault();
					$("#confirm-reset-dialog").dialog({
						height : 175
					}).dialog("open");
				} else {
					Exxo.showSpinner();
					Exxo.UI.vars['stopEvent'] = true;
					window.location.href = Exxo.UI.vars['target'];
				}
			});

			$(".restore").click(function(e) {
				Exxo.UI.vars['target'] = e.target || e.srcElement;
				if (Exxo.UI.vars['stopEvent']) {
					e.preventDefault();
					$("#confirm-restore-dialog").dialog({
						height : 175
					}).dialog("open");
				} else {
					Exxo.showSpinner();
					Exxo.UI.vars['stopEvent'] = true;
					window.location.href = Exxo.UI.vars['target'];
				}
			});

			$(".resetToFactory").click(function(e) {

				Exxo.UI.vars['target'] = e.target || e.srcElement;

				if (Exxo.UI.vars['stopEvent']) {
					e.preventDefault();
					$("#confirm-factory-reset-dialog").dialog({
						height : 175
					}).dialog("open");
				} else {
					Exxo.showSpinner();
					Exxo.UI.vars['stopEvent'] = true;
					window.location.href = Exxo.UI.vars['target'];
				}
			});
		}
	}
};