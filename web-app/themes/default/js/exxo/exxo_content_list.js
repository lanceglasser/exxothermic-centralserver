/***
 @file      exxo_content_list.js
 @brief     General functions for the Content list pages
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.Content = {
	initConfirmDeleteOptions: function() {
		Exxo.UI.vars['stopEvent'] = true;
		Exxo.UI.vars['target'] = true;
		$(".delete").click(function(e) {
			if (Exxo.UI.isSafari()) {
				 Exxo.UI.vars['target'] = $(this);
	       	 } else {
	       		 Exxo.UI.vars['target'] = e.target;
	       	 }
			if (Exxo.UI.vars['stopEvent']) {
				e.preventDefault();
				$("#confirm-delete-dialog").dialog("open");
			} else {
				Exxo.showSpinner();
				if (Exxo.UI.isSafari()) {
 					location.href = $(this).attr("href");
 				}
			}
		});

		$("#confirm-delete-dialog").dialog({
			height : 180,
			width : 360,
			modal : true,
			autoOpen : false,
			buttons : [ {
				text : Exxo.UI.translates['okButton'],
				click : function() {
					$(this).dialog("close");
					Exxo.UI.vars['stopEvent'] = false;
					Exxo.UI.vars['target'].click();
				}
			}, {
				text : Exxo.UI.translates['cancelButton'],
				click : function() {
					$(this).dialog("close");
				}
			} ]
		});
	},
	init : function() {
		
		$("#content-disable-dialog").dialog({
			height : 140,
			modal : true,
			autoOpen : false
		});

		$(".viewLocation-disable").click(function(e) {
			e.preventDefault();
			//alert(disableMessage);
			$("#content-disable-dialog").dialog("open");
		});
		
		Exxo.Content.initConfirmDeleteOptions();
	}
};