/***
 @file      exxo_box_manufacturer_edit.js
 @brief     Custom functions and events for the Box Manufacturer Edition Page.
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.BoxManufacturer = {
    Edit : {
       init : function() {
          $("#memoryType,#memory,#processorBrand,#machineBrand").select2();
       }
    }
};