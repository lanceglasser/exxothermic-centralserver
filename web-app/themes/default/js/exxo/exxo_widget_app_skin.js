/***
 @file      exxo_widget_app_skin.js
 @brief     Functions for the AppSkin List Widget
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.WidgetsAppSkins = {
	initAppSkinsListWidget : function() {

		$("#appSkin-widget").exxoWidget({
			helpMsg : Exxo.UI.translates['widget-app-theme-list-info-msg'],
			referenceIcon : 'list',
			referenceUrl : Exxo.UI.urls['app-theme-list']
		});

		Exxo.UI.vars["widgetAppSkinsTable"] = $('#widgetAppSkinsTable')
				.dataTable(
						{

							"pagingType" : "full", // More info at: https://datatables.net/examples/basic_init/alt_pagination.html
							"lengthMenu" : [ [ 5 ], [ 5 ] ], // More info at: https://datatables.net/examples/advanced_init/length_menu.html
							"processing" : true,
							"serverSide" : true,
							"ajax" : {
								"url" : Exxo.UI.urls["widget-get-data-url"],
								"data" : function(d) {
									d.object = 'appSkin';
									d.companyId = Exxo.UI.vars["company-id"];
									d.locationId = Exxo.UI.vars["location-id"];
								}
							},
							"columnDefs" : [ 
								{
									"targets" : [ 0 ],
									"render" : function(data, type, row) {
										return '<a href="'
											+ Exxo.UI.urls["settings-edit-url"]
											+ row[3] + '">' + row[0]
											+ '</a>';
									}
								},
				                {
								"targets" : [ 2 ],
								"render" : function(data, type, row) {
									if (row[2] == "" || 
										row[2] == "null" ||
										row[2] == undefined) {
										return "-";
									} else {
										return '<img src="' + row[2]
										+ '" width="64px" height="28px"/>';
									}
								}
							} ]
						});
	}
};