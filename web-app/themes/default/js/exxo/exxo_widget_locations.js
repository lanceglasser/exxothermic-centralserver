/***
 @file      exxo_widget_locations.js
 @brief     Functions for the Locations List Widget
 @author    Cecropia Solutions <dev@cecropiasolutions.com>
 */

var Exxo = Exxo || {};

Exxo.WidgetsLocations = {
	initLocationsListWidget: function(){
		
		$( "#locations-widget" ).exxoWidget({
			helpMsg:	Exxo.UI.translates['widget-locations-list-info-msg'],
			referenceIcon: 	'list',
			referenceUrl: 	Exxo.UI.urls['locations-list']});
		
		Exxo.UI.vars["widgetLocationsTable"] = $('#widgetLocationsTable').dataTable( {
			"pagingType": "full", // More info at: https://datatables.net/examples/basic_init/alt_pagination.html
			"lengthMenu": [[5], [5]], // More info at: https://datatables.net/examples/advanced_init/length_menu.html
       	 	"processing": true,
            "serverSide": true,
            "ajax": {
            	"url": Exxo.UI.urls["widget-get-data-url"],
    			"data": function ( d ) {
    		        d.object = 'location';
    		        d.companyId = Exxo.UI.vars["company-id"];
    		    }
            },
            "columnDefs": [
				{
						"targets": [ 0 ],
					    "render": function ( data, type, row ) {
					  	  return '<a href="' + Exxo.UI.urls["location-dashboard-url"] + row[0] + '">' + row[1] + '</a>';
					    }
				},
 		          {
 		              "targets": [ 1 ],
 		              "render": function ( data, type, row ) {
 		            	  return '<div class="state ' + 
 		            	  		(row[2]? 'stateGreen':'stateGray') + '"></div>';
 		              }
 		          }
       		 ]
        } );
	}
};