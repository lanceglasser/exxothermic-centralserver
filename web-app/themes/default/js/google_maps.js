
var map = null;
var geocoder = null;
//var infowindow = new google.maps.InfoWindow();
var marker;
var flaglat = false;
var flaglng = false;
var isUpdatingAddress = false;

function initialize(lat, lng) {
	initialize(lat, lng, false);
}

function initialize(lat, lng, lock) {
	Exxo.UI.vars["initializing"] = true;
    if (GBrowserIsCompatible()) {
      map = new GMap2(document.getElementById("map-canvas"));
      map.setCenter(new GLatLng(37.4419, -122.1419), 1);
      map.setUIToDefault();
      geocoder = new GClientGeocoder();
      if(lat !== -1){
         setPointByCoordinates(lat, lng, lock);
      } else {
    	  Exxo.UI.vars["initializing"] = false;
      }
    }
  }

function setPointByCoordinates(lat, lng){
	setPointByCoordinates(lat, lng, false)
}
function setPointByCoordinates(lat, lng, lock){
   if (geocoder && map) {
      var point = new GLatLng(lat, lng);
      map.setCenter(point, 10);
      
      if(lock){
    	  marker = new GMarker(point, {draggable: false});
      } else {
    	  marker = new GMarker(point, {draggable: true});
      }
      
      getPlaceWithLatLng(point);
      
      //setCoordinates(marker.getLatLng().lat(), marker.getLatLng().lng());
      GEvent.addListener(marker, 'dragstart', function() {
    	  Exxo.UI.vars["blockGoogleMapUpdate"] = true;
    	  console.debug("Put X to true");
          map.closeInfoWindow();
      });
      
      GEvent.addListener(marker, "dragend", function() {
    	  getPlaceInfoAfterDrag(marker);
    	  Exxo.UI.vars["blockGoogleMapUpdate"] = false;
    	  console.debug("Put X to false");
      });
      
      map.addOverlay(marker);
   }
}

function showAddress(address) {
	console.debug("showAddress: " + address);
   if (geocoder && map && isUpdatingAddress == false) {
	  isUpdatingAddress = true;
      if(marker){
         marker.closeInfoWindow();
      }
      map.clearOverlays();
      console.debug("getDefaultUI A");
      map.getDefaultUI();
      
      if(address !== null && address !== ""){         
         geocoder.getLocations(
           address,
           showAddressInMap
         );
  	  }
   }
}

function getPlaceInfoAfterDrag(marker){
	Exxo.UI.vars["blockGoogleMapUpdate"] = true;
	getPlaceWithLatLng(marker.getLatLng());
}

function getPlaceWithLatLng(latLng){
	if(!Exxo.UI.vars["initializing"]){
		Exxo.UI.vars["blockGoogleMapUpdate"] = true;
	}
	geocoder.getLocations(
			latLng,
	           placeInfoCallBack
	         );
}

function placeInfoCallBack(response){
	if(!Exxo.UI.vars["initializing"]){
		if (!response || response.Status.code != 200) {
			Exxo.showErrorMessage("It's not a valid direction");
		} else {
			
			place = response.Placemark[0];
			setPlaceValues(place);
		}
	}
	Exxo.UI.vars["initializing"] = false;
}

function showAddressInMap(response){
	if (!response || response.Status.code != 200) {
        Exxo.showErrorMessage("It's not a valid direction");
    } else {
        place = response.Placemark[0];
        point = new GLatLng(place.Point.coordinates[1],
                            place.Point.coordinates[0]);
                
        map.setCenter(point, 10);
        marker = new GMarker(point, {draggable: true});
        setCoordinates(marker.getLatLng().lat(), marker.getLatLng().lng());
        GEvent.addListener(marker, 'dragstart', function() {
        	Exxo.UI.vars["blockGoogleMapUpdate"] = true;
      	  	console.debug("Put Y to true");
            map.closeInfoWindow();
        });
        GEvent.addListener(marker, "dragend", function(point) {
            getPlaceInfoAfterDrag(marker);
        });        
        map.addOverlay(marker);
    }
	isUpdatingAddress = false;
}

function setPlaceValues(place){
    setCoordinates(place.Point.coordinates[1], place.Point.coordinates[0]);
    //Only change when is not using the Company Info
	if(Exxo.UI.vars["isUsingCompanyInfo"] == undefined || Exxo.UI.vars["isUsingCompanyInfo"] == false){
		console.debug("Actualizar valores con: " + place);
		console.debug(place);
		Exxo.UI.vars["stateFromGoogle"] = place.AddressDetails.Country.AdministrativeArea.AdministrativeAreaName;
		console.debug(Exxo.UI.vars["stateFromGoogle"]);
		//console.debug("Ciudad: " + place.AddressDetails.Country.AdministrativeArea.Locality.LocalityName);
		console.debug("Address: " + place.address);
		var address_array = place.address.split(',');
		if(Exxo.UI.vars["selectedCountryCode"] == place.AddressDetails.Country.CountryNameCode){
			console.debug("A [" + Exxo.UI.vars["selectedCountryCode"] + " : " + place.AddressDetails.Country.CountryNameCode + "]");
			$('#state option:selected').removeAttr('selected');
			$("#state").change();
			$("#state option[code='" + Exxo.UI.vars["stateFromGoogle"] + "']").attr("selected","selected");
			$("#state").change();
		} else {
			Exxo.UI.vars["selectedCountryCode"] = place.AddressDetails.Country.CountryNameCode;
			console.debug("B [" + Exxo.UI.vars["selectedCountryCode"] + "]");
			//$('#state').find('option:selected').removeAttr('selected');
			$("#country option[code='" + place.AddressDetails.Country.CountryNameCode + "']").attr("selected","selected");
			$("#country").change();
		}
		if(address_array.length > 1) $("#city").val(address_array[1].trim());
		if(address_array.length > 0) $("#address").val(address_array[0].trim());
	}
}

function setCoordinates(lat, lng){
   $("#latitude").val(lat);
   $("#longitude").val(lng);
}