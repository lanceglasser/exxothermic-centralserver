/**
 * RetrieveUsageDataJob.groovy
 */
package mycentralserver


/**
 * Job class to retrieve the usage data from all connected ExXtractors,
 * this job must be executed ones per day at some moment during night.
 * 
 * @author Cecropia Solutions
 *
 */
class RetrieveUsageDataJob {
	
	/* Injection of required services */
	def retrieveUsageDataService;
	
	//Trigger this job at 7:00 A.M. UTC Time
    static triggers = {
		cron name: 'usageDataRetrieverTrigger', cronExpression: "0 0 7 * * ?"
    }
	
	def group = "ExxoGroup";
	
	def description = "Retrieve the usage data file from all connected ExXtractors";

    def execute() {
        // execute job
		log.info( "Starting Job to retrieve ExXtractors usage data file");
		try {
			retrieveUsageDataService.retrieveExXtractorsData();
		} catch(Exception e) {
			log.error("Error executing the job to retrieve the usage data files.", e);
		}
    }
}
