/**
 * CheckBlackListJob.groovy
 */
package mycentralserver;

/**
 * Job class to check the stop working date of the venue servers at the black list,
 * the connected servers that reached the stop working date will be reset to force
 * the disconnection of the system, this job must be executed ones per day at some moment during night.
 *
 * @author Cecropia Solutions
 *
 */
class CheckBlackListJob {
    /* Injection of required services */
	def boxBlackListService;
	
	//Trigger this job at 6:00 A.M. UTC Time
    static triggers = {
		cron name: 'blackListCheckTrigger', cronExpression: "0 0 6 * * ?"
    }
	
	def group = "ExxoGroup";
	
	def description = "Check the stop working date of all connected Venue servers";

    def execute() {
        // execute job
		println "*** Starting Job to check the stop working date of all connected Venue servers ***"
		log.info( "Starting Job to check the stop working date of all connected Venue servers");
		try {
			boxBlackListService.checkStopWorkingDate();
		} catch(Exception e) {
			log.error("Error executing the job that check the stop working date of all connected Venue servers.", e);
		}
    }
}
