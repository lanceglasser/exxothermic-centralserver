package mycentralserver.utils;

import java.util.HashMap;

import org.apache.commons.lang.StringUtils;

/**
 * This class will be use to store and handle the common response information of the Ajax requests and how to render the
 * response
 * 
 * @author sdiaz
 * @since 11/06/2015
 */
public class AjaxResponse {

    private static final String MESSAGE = "msg";
    private static final String ERROR = "error";
    private static final String JSON_REDIRECT_URL = "jsonRedirectUrl";
    private static final String MUST_REDIRECT = "jsonRedirect";

    private String msg = "";
    private String redirectUrl = "";
    private boolean error = true; // The default on the response is false, if you need true then use the constructor
    private boolean mustRedirect = false;

    /*
     * Default constructor
     */
    public AjaxResponse() {
    }

    /**
     * Secondary constructor that allows to define the initial value of the error property
     * 
     * @param error
     *            Initial value of the error property
     */
    public AjaxResponse(final boolean error) {
        this.error = error;
    }

    /**
     * Sets only the error and message fields on the response, if you retrieve full response later will use the defaults
     * for the other fields
     * 
     * @param error
     *            The request found an error, yes or not
     * @param msg
     *            Descriptive message of the result of the operation, can include an error or success message
     */
    public void setBasicInformation(final boolean error, final String msg) {
        this.error = error;
        this.msg = msg;
    }

    /**
     * Sets all the fields on the response
     * 
     * @param error
     *            The request found an error, yes or not
     * @param msg
     *            Descriptive message of the result of the operation, can include an error or success message
     * @param mustRedirect
     *            True or False depending if the client must execute a redirect after receive a success response
     * @param redirectUrl
     *            If the client must do a redirect, must do it to this url
     */
    public void setFullInformation(final boolean error, final String msg, final boolean mustRedirect,
            final String redirectUrl) {
        this.error = error;
        this.msg = msg;
        this.mustRedirect = mustRedirect;
        this.redirectUrl = redirectUrl;
    }

    /**
     * Returns the hash map only with the error and message values
     * 
     * @return HashMap<String, Object> with 2 references: error and msg
     */
    public HashMap<String, Object> getBasicResponseMap() {
        HashMap<String, Object> jsonMap = new HashMap<String, Object>();
        jsonMap.put(AjaxResponse.MESSAGE, msg);
        jsonMap.put(AjaxResponse.ERROR, error);
        return jsonMap;
    }

    /**
     * Returns the hash map with all the information
     * 
     * @return HashMap<String, Object> with 4 references: error, msg, jsonRedirect and jsonRedirectUrl
     */
    public HashMap<String, Object> getFullResponseMap() {
        HashMap<String, Object> jsonMap = new HashMap<String, Object>();
        jsonMap.put(AjaxResponse.MESSAGE, msg);
        jsonMap.put(AjaxResponse.ERROR, error);
        jsonMap.put(AjaxResponse.JSON_REDIRECT_URL, redirectUrl);
        jsonMap.put(AjaxResponse.MUST_REDIRECT, mustRedirect);
        return jsonMap;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * This method will set the message property only if it's empty
     * 
     * @param msg
     *            New message to set if the current value is empty
     */
    public void setMsgOnlyIfEmpty(String msg) {
        this.msg = StringUtils.isBlank(this.msg) ? msg : this.msg;
    }

    /**
     * Append extra content to the msg property
     *  
     * @param msg
     *          Extra content to append
     */
    public void addToMsg(final String msg) {
        this.msg += msg;
    }

    /**
     * Returns True or false depending if the current msg is null or empty
     * 
     * @return True or false depending if the current msg is null or empty
     */
    public boolean hasEmptyMsg() {
        return StringUtils.isBlank(this.msg);
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public boolean isMustRedirect() {
        return mustRedirect;
    }

    public void setMustRedirect(boolean mustRedirect) {
        this.mustRedirect = mustRedirect;
    }
}