/**
 * ShortcutsEnum.groovy
 */
package mycentralserver.utils;

/*
 * Enum with the list of Shortcuts for the Dashboards
 */
enum ShortcutsEnum {

	CREATE_OFFER("CRT_OFFER"),
	CREATE_DOCUMENT("CRT_DOC"),
	CREATE_BANNER("CRT_BAN"),
	CREATE_LOCATION("CRT_LOC"),
	CREATE_EMPLOYEE("CRT_EMP"),
	CREATE_COMPANY("CRT_COM"),
	CREATE_SOFT_VER("CRT_SOF"),
	CREATE_DOC_CATEGORY("CRT_DOC_CAT"),
	CREATE_APP_THEME("CRT_APP_THM"),
	CREATE_WELCOME_AD("CRT_WAD"),
	REGISTER_EXXTRACTOR("REG_EXX"),
	UNREGISTER_EXXTRACTOR("UNR_EXX"),
	USAGE_REPORT("USG_RPT")	
			
	final String value;
    
	ShortcutsEnum (String value) {
        this.value = value
    }

	String toString() {value }
	 
    String value() { return value }
    String getKey() { name() }
}
