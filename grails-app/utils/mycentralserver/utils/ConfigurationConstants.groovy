/**
 * ConfigurationConstants.groovy
 */
package mycentralserver.utils;

/**
 * Constants related with the configurations store at DB
 * 
 * @author Cecropia Solutions
 */
class ConfigurationConstants {
	
	/* Configuration Codes */
	public static final String DASHBOARD_MAIN_DEFAULT = "DASH_MAIN";
	public static final String DASHBOARD_COMPANY_DEFAULT = "DASH_COM";
	public static final String DASHBOARD_LOCATION_DEFAULT = "DASH_LOC";
	
} // End of Class
