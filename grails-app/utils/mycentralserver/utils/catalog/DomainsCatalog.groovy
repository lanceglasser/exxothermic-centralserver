package mycentralserver.utils.catalog;

/**
 * Helper class with the definition of some of the properties for the Domains to be use as reference for Json
 * constructions between other functionalities
 * 
 * @author sdiaz
 * @since 11/08/2015
 */
public class DomainsCatalog {

    /**
     * CommonProperties is for properties that are common in the Domains
     */
    public static class CommonProperties {
        public static enum SimpleFields {
            ID("id"), CREATION_DATE("creationDate"), LAST_MODIFICATION_DATE("lastModificationDate"), ENABLED("enabled"),
            AM("AM"), SERIAL_NUMBER("serialNumber"), CONTENTS("contents");

            private String fieldName;

            private SimpleFields(String fieldName) {
                this.fieldName = fieldName;
            }

            public String fieldName() {
                return fieldName;
            }
        }
    }

    /**
     * Properties for Content domain
     */
    public static class ContentProperties {
        public static enum CommonFields {
            TYPE_IMAGE("image"), TYPE_TEXT("text"), TYPE_OFFER("offer");

            private String fieldName;

            private CommonFields(String fieldName) {
                this.fieldName = fieldName;
            }

            public String fieldName() {
                return fieldName;
            }
        }
        public static enum JsonFields {
            DESCRIPTION("description"), DIALOG_IMAGE("dialogImage"), TABLET_DIALOG_IMAGE("tabletDialogImage"), 
            START_DATE("startDate"), END_DATE("endDate"), SCHEDULE("schedule"), CHANNEL_LABEL("channelLabel"),
            CHANNEL_CONTENT("channelContent"), CHANNEL_COLOR("channelColor"), TYPE("type"), IMAGE_URL("imageURL"),
            TABLET_FEATURED_IMAGE("tabletFeaturedImage"), FEATURED("featured"), TITLE("title"), URL("url"),
            BACKGROUND_COLOR("backgroundColor");

            private String fieldName;

            private JsonFields(String fieldName) {
                this.fieldName = fieldName;
            }

            public String fieldName() {
                return fieldName;
            }
        }
    }

    /**
     * Properties for Scheduler domain
     */
    public static class SchedulerProperties {
        public static enum CommonFields {
            DEFAULT_START_HOUR("12:00 AM"), DEFAULT_DURATION("24"), DEFAULT_TIME_ZONE("UTC"), 
            UTC_FORMAT("yyyy-MM-dd HH:mm:ss");

            private String fieldName;

            private CommonFields(String fieldName) {
                this.fieldName = fieldName;
            }

            public String fieldName() {
                return fieldName;
            }
        }
        public static enum JsonFields {
            HOURS("hours"), DAYS("days"), EXPIRATION_DATE("expirationDate"), TIMEZONE("timezone");

            private String fieldName;

            private JsonFields(String fieldName) {
                this.fieldName = fieldName;
            }

            public String fieldName() {
                return fieldName;
            }
        }
    }
}