package mycentralserver.utils

import java.util.Random;

import mycentralserver.app.Affiliate;
import mycentralserver.utils.enumeration.Architecture;

/**
 * Helper class with methods related to the generation and validation of the serial numbers for the Venue Servers
 * 
 * @author Cecropia Solution
 * @since 01/01/2014
 */
class SerialNumberGeneratorHelper {

    private static final String ALPHA_CAPS = "ABCDEFGHIJKMNPQRSTUVWXYZ"
    private static final String ALPHA = "abcdefghijkmnpqrstuvwxyz"
    private static final String NUM = "23456789"
    private static final int SERIALS_NEW_FORMAT_LENGTH = 19;

    /**
     * Generates a random serial number of 18 characters
     * 
     * @return Generated serial number
     */
    public static String generateSerial() {
        final String serial =  UUID.randomUUID().toString().toUpperCase();
        return serial.substring(0, 18);
    }

    /**
     * This method returns the partner code from the venue server's serial
     * 
     * @param serial
     *             Serial number of the venue server
     * @return Partner code, default when not found.
     */
    public static String getPartnerCodeFromSerial(String serial){
        String code = serial.toString().substring(0, 3);
        final Affiliate affiliate = Affiliate.findByCode(code.toUpperCase());
        if (affiliate == null) { // If the affiliate do not exists, returns default
            code = PartnerEnum.getDefaultPartnerCode();
        }
        return code.toUpperCase();
    }

    /**
     * This method returns the affiliate associated to the serial number of the venue server
     *
     * @param serial
     *             Serial number of the venue server
     * @return Affiliate associated to the serial, null when not found.
     */
    public static Affiliate getPartnerFromSerial(String serial){
        final String code = serial.toString().substring(0, 3);
        return Affiliate.findByCode(code.toUpperCase());
    }

    /**
     * Using the received code will return the associated architecture value; if the code is not found then the default
     * value will be returned
     * 
     * @param architecture
     *              Code of the architecture to find
     * @return Associated architecture value for the code or default value when is not founded
     */
    public static int getArchitectureReferenceFromCode(String architecture) {
        switch(architecture){
            case Architecture.LEGACY32.toString():
                return Architecture.LEGACY32.getValue();
            case Architecture.X64.toString():
                return Architecture.X64.getValue();
            case Architecture.ODROID.toString():
                return Architecture.ODROID.getValue();
            case Architecture.LW300X.toString():
                Architecture.LW300X.getValue();
            case Architecture.LW200P.toString():
                Architecture.LW200P.getValue();
            default: //Default is X32
                return Architecture.X32.getValue();
        }
    }
	
	public static generateSerial(String partner, int architecture) {
		String serial =  UUID.randomUUID().toString().toUpperCase();
		serial = partner.substring(0, 3) + architecture + "-" + serial.toString().substring(4, 18);
		return serial;
	}

	public static generateSerial2() {
		int minLen = 20
		int maxLen = 20
		int noOfCAPSAlpha = 5
		int noOfDigits = 10
		int noOfSplChars = 0
		return generatePssw(minLen, maxLen, noOfCAPSAlpha, noOfDigits).toString()
	}

  public static generateManagementPasscode() {
    int minLen = 4
    int maxLen = 4
    int numberOfCapitalizedAlpha = 1
    int numberOfDigits = 1
    return generatePssw(minLen, maxLen, numberOfCapitalizedAlpha, numberOfDigits).toString()
  }

	public static char[] generatePssw(int minLen, int maxLen, int noOfCAPSAlpha, int noOfDigits) {

		if(minLen > maxLen)
			throw new IllegalArgumentException("Min. Length > Max. Length!");
		if( (noOfCAPSAlpha + noOfDigits) > minLen )
			throw new IllegalArgumentException
			("Min. Length should be atleast sum of (CAPS, DIGITS, SPL CHARS) Length!");
		Random rnd = new Random();
		int len = rnd.nextInt(maxLen - minLen + 1) + minLen;
		char[] pswd = new char[len];
		int index = 0;
		for (int i = 0; i < noOfCAPSAlpha; i++) {
			index = getNextIndex(rnd, len, pswd);
			pswd[index] = ALPHA_CAPS.charAt(rnd.nextInt(ALPHA_CAPS.length()));
		}
		for (int i = 0; i < noOfDigits; i++) {
			index = getNextIndex(rnd, len, pswd);
			pswd[index] = NUM.charAt(rnd.nextInt(NUM.length()));
		}
		for(int i = 0; i < len; i++) {
			if(pswd[i] == 0) {
				pswd[i] = ALPHA.charAt(rnd.nextInt(ALPHA.length()));
			}
		}
		return pswd;
	}

	private static int getNextIndex(Random rnd, int len, char[] pswd) {
		int index = rnd.nextInt(len);
		while(pswd[index = rnd.nextInt(len)] != 0);
			return index;
	}
	
    /**
     * Gets the associated Architecture of a serial number; the architecture value is in general part of the serial but
     * for really old servers it's not; for those cases the architecture will be LEGACY32
     * 
     * @param serial
     *              Serial number to evaluate
     * @return Architecture related to the serial number
     */
    public static Architecture getArchitectureFromSerial(final String serial) {
        try {
            // If the size of the serial corresponds to the new format
            if ( serial.length() == SERIALS_NEW_FORMAT_LENGTH ) {
                final String architectureValueOnSerial = serial.substring(3, 4);
                return Architecture.valueOf(Integer.parseInt(architectureValueOnSerial));
            } else { // If the serial don't has the new format it's a legacy serial
                return Architecture.LEGACY32;
            }
        } catch(Exception e) {
            // If fails must assume that the architecture is legacy
            return Architecture.LEGACY32;
        }
    }
}
