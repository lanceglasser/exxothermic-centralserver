package mycentralserver.utils

import java.security.MessageDigest
import java.util.zip.GZIPInputStream;

class FileHelper {

	static private JPG= "jpg";
	static private PNG= "png";
	static private TAR_GZ = ".tar.gz";
	static private TAR_GZ_SIZE = 7;
	
	static def getFileExtension(String fileName) {
		
		if(fileName == null) {
			return "";
		}
		
		String[] array = fileName.split("\\.")
		def sizeArray = array.size()
		
		if(sizeArray <= 1) {
			return ""
		}
		
		def ext
		if(FileHelper.isFileTypeTarGz(fileName)) {
			ext = (array[sizeArray-2] + "." +array[sizeArray-1])
		} else {
			ext = array[sizeArray-1]
		}
		return ext;
	} // END getFileExtention
	
	static  def getFileNameWithoutExtension(fileName) {
		
		if(fileName == null) {
			return null;
		}
		
		String[] array = fileName.split("\\.")
		def sizeArray = array.size()
		def fileNameNew = ""
		
		if(sizeArray <= 1) {
			return fileName
		}
		
		def sizeArrayWithOutExt
		
		if(FileHelper.isFileTypeTarGz(fileName)) {
			sizeArrayWithOutExt = ( sizeArray - 2 )
		} else {
			sizeArrayWithOutExt = ( sizeArray - 1 )
		}
		
		for (int i = 0; i < sizeArrayWithOutExt ; i++) {
			fileNameNew = ( fileNameNew + array[i] + ".");
		}
		
		def lengthWithoutPoint = (fileNameNew.length()-2);
		fileNameNew = fileNameNew[0..lengthWithoutPoint]
		
		return fileNameNew;
	} // END getFileNameWithoutExtension
	
	static private def isFileTypeTarGz(String fileName) {
		
		if(fileName.size() < TAR_GZ_SIZE) {
			return false;
		} 
		
		def fileNameSize = fileName.size();
		def extention = fileName[(fileNameSize-TAR_GZ_SIZE)..(fileNameSize-1)]
		
		if(extention == TAR_GZ) {
			return true
		}
		
		return false
	}
	
	static private def isValidImage(String fileName) {
		
		def extention = getFileExtension(fileName)
		
		if(extention == PNG || extention == JPG) {
			return true
		}else{		
			return false
		}
	}
	
	/**
	 * This method decompressed a tar gz file to a text file
	 * 
	 * @param gzipFile	Path of the tar.gz file
	 * @param newFile	Path where to save the text file with the content
	 */
	static private void decompressGzipFile(String gzipFile, String newFile) {
		try {
			FileInputStream fis = new FileInputStream(gzipFile);
			GZIPInputStream gis = new GZIPInputStream(fis);
			FileOutputStream fos = new FileOutputStream(newFile);
			byte[] buffer = new byte[1024];
			int len;
			while((len = gis.read(buffer)) != -1){
				fos.write(buffer, 0, len);
			}
			//close resources
			fos.close();
			gis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}