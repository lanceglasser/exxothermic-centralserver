package mycentralserver.utils

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.security.AlgorithmParameters;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.bouncycastle.util.encoders.Base64;

class EncrypterHelper {
	/**
	 *
	 * @param simmetricalKey it is a String with length is a multiple of 8
		in this case if long 32 allows AES-256 (32 * 8)
	 * @param message
	 * @return
	 */
	static def  encryptWithAes(String secretValue,String ivValue,String message) {
		  
		  try {		  
			  byte[] secret = hexStringToByteArray(secretValue);
			  byte[] iv = hexStringToByteArray(ivValue);
			  SecretKeySpec key = new SecretKeySpec(secret,  Constants.ENCRYPTION_ALGORITHM_AES);
			  Cipher cipher;
			  cipher = Cipher.getInstance(Constants.ENCRYPTION_ALGORITHM_AES_CBC);
  
			 //Comienzo a encriptar
			 cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv));
			 byte[] cipherData = cipher.doFinal(message.getBytes());
			 String dataEncrypt = cipherData.encodeBase64(); // cipherData.toString()
			
			 return dataEncrypt;
		  } catch (Exception e) {
			   e.printStackTrace();			   
			   return null;
		  }

	} // END encryptWithAes
	
	
	static def  decryptWithAes(String secretValue,String ivValue,String message) {
		  try {
			  byte[] secret = hexStringToByteArray(secretValue);
			  byte[] iv = hexStringToByteArray(ivValue);
			 
			  SecretKeySpec key = new SecretKeySpec(secret, Constants.ENCRYPTION_ALGORITHM_AES);
			  Cipher cipher;
			  cipher = Cipher.getInstance( Constants.ENCRYPTION_ALGORITHM_AES_CBC);
			  
			 //Comienzo a encripta
			 cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
			 byte[] messageWithout64 = message.decodeBase64() //Base64.decode(message);
			 byte[] cipherData = cipher.doFinal(messageWithout64);
			String word = new String(cipherData);
			return word;
		  } catch (Exception e) {
			   e.printStackTrace();
			   return null;
		  }

	} // END encryptWithAes
	
	
	static def hexStringToByteArray(String s) {
		int len = s.length();
		int halfLen = (int)(len / 2)
		byte[] data = new byte[halfLen];
		for (int i = 0; i < len; i += 2 ) {
			int halfI = (int)(i / 2)
			data[halfI] = (byte) ((Character.digit(s.charAt(i), 16) << 4)  + Character.digit(s.charAt(i+1), 16));			
		}
		return data;
		//return DatatypeConverter.parseHexBinary(s);
	}
	
	/**
	 *
	 * @param simmetricalKey it is a String with length is a multiple of 8
		in this case if long 32 allows AES-256 (32 * 8)
	 * @param message
	 * @return
	 */
	static def encryptWithAesAndReturnBytesArray(String secretValue,String ivValue,String message) {
		  
		  try {
			  byte[] secret = hexStringToByteArray(secretValue);
			  byte[] iv = hexStringToByteArray(ivValue);
			  SecretKeySpec key = new SecretKeySpec(secret, "AES");
			  Cipher cipher;
			  cipher = Cipher.getInstance( "AES/CBC/PKCS5PADDING");  
			  cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv));
			  byte[] cipherData = cipher.doFinal(message.getBytes());			
			  return cipherData;
		  } catch (Exception e) {
			   e.printStackTrace();			   
			   return null;
		  }
	} // END encryptWithAes
	
	public void main (String [] args)
	{
		String s = "23E54ACAB9031BEB1321FFD3125FBA1AB0000000000000000000000000000000";
		for (int i = 0;  i < 100 ; i++){
			println hexStringToByteArray(s);
		}
	}
	
	
	
	/************************************/ 
	/*** Functions for RSA Encryption ***/
	/************************************/
		
	/**
	 * Encrypt the plain text using public key.
	 *
	 * @param text
	 *            : original plain text
	 * @return Encrypted text
	 * @throws java.lang.Exception
	 */
	public byte[] encryptRSA(String text) {
		byte[] cipherText = null;
		try {
			// get an RSA cipher object and print the provider
			final Cipher cipher = Cipher.getInstance(Constants.ALGORITHM);
			// Encrypt the plain text using the public key
			PublicKey key = getPublicKey();
			cipher.init(Cipher.ENCRYPT_MODE, key);
			cipherText = cipher.doFinal(text.getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cipherText;
	}

	/**
	 * Decrypt text using private key.
	 *
	 * @param text
	 *            :encrypted text
	 * @param key
	 *            :The private key
	 * @return plain text
	 * @throws java.lang.Exception
	 */
	public String decryptRSA(byte[] text) {
		byte[] dectyptedText = null;
		try {
			
			// Get an RSA cipher object and print the provider
			final Cipher cipher = Cipher.getInstance(Constants.ALGORITHM);
			
			// Decrypt the text using the private key
			PrivateKey key = getPrivateKey();
			cipher.init(Cipher.DECRYPT_MODE, key);
			dectyptedText = cipher.doFinal(text);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return new String(dectyptedText);
	}

	/**
	 * This method will convert to a Hex String a bytes array
	 * 
	 * @param bytes	Array to convert
	 * @return		The bytes array as a Hex string
	 */
	public String bytesToHex(byte[] bytes) {
		return DatatypeConverter.printHexBinary(bytes);
	}

	/**
	 * This method will return the Public key to be use for
	 * the encryption process; is loading this key from a
	 * .der file.
	 * 
	 * @return				Public Key
	 * @throws Exception	When any error
	 */
	public PublicKey getPublicKey() throws Exception {		
		def config = org.codehaus.groovy.grails.commons.ConfigurationHolder.config;
		File f = new File(config.basePathKeys + Constants.PUBLIC_KEY_FILE_NAME);
		FileInputStream fis = new FileInputStream(f);
		DataInputStream dis = new DataInputStream(fis);
		byte[] keyBytes = new byte[(int) f.length()];
		dis.readFully(keyBytes);
		dis.close();

		X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		return kf.generatePublic(spec);
	}

	/**
	 * This method will return the Private key to be use for
	 * the decryption process; is loading this key from a
	 * .der file.
	 * @return				Private Key
	 * @throws Exception	When any error
	 */
	public PrivateKey getPrivateKey() throws Exception {

		def config = org.codehaus.groovy.grails.commons.ConfigurationHolder.config;		
		File f = new File(config.basePathKeys + Constants.PRIVATE_KEY_FILE_NAME);
		FileInputStream fis = new FileInputStream(f);
		DataInputStream dis = new DataInputStream(fis);
		byte[] keyBytes = new byte[(int) f.length()];
		dis.readFully(keyBytes);
		dis.close();

		PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		return kf.generatePrivate(spec);
	}
	
	/**
	 * This method will encrypt a plain text using
	 * RSA public key encryption
	 * 
	 * @param decrypted		Text to be crypted
	 * @return				Crypted text as Base64
	 */
	public String encryptText2Base64WithRSA(String decrypted){
		final byte[] cipherText = encryptRSA(decrypted);
		String crypted = new String(Base64.encode(cipherText));
		System.out.println("Decrypted: " + decrypted + ", Encrypted Base64: " + crypted);
		return crypted;
	}
	
	/**
	 * This method will decrypt a Base64 text using
	 * RSA private key decryption
	 *
	 * @param decrypted		Base64 text to be decrypted
	 * @return				Plain text
	 */
	public String decryptBase642TextWithRSA(String crypted){
		final String decrypted = decryptRSA(Base64.decode(crypted));
		System.out.println("Encrypted Base64: " + crypted + ", Decrypted: " + decrypted);
		return decrypted;
	}
	
}
