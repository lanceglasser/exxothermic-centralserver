package mycentralserver.utils;

/**
 * Enumeration with the list of Partners for different validations
 * 
 * @author sdiaz
 * @since 06/24/2016
 */
public enum PartnerEnum {

    EXX("EXX"), MYE("MYE"), DXX("DXX");

    private final String code;

    PartnerEnum(final String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public String toString() {
        return code;
    }

    /**
     * Returns the default partner from the list
     * 
     * @return Default PartnerEnum object
     */
    public static PartnerEnum getDefault() {
        return PartnerEnum.EXX;
    }

    /**
     * Returns the default partner code from the list
     * 
     * @return 3 characters code of the default partner
     */
    public static String getDefaultPartnerCode() {
        return PartnerEnum.EXX.getCode();
    }

    /**
     * Returns the list of codes as a String
     * 
     * @return List of codes with the format [COD1, COD2, COD3]
     */
    public static String getCodesListAsString() {
        StringBuilder strBuilder = new StringBuilder(50);
        strBuilder.append("[");
        boolean isAfterFirst = true;
        for (PartnerEnum partner : PartnerEnum.values()) {
            if (isAfterFirst) {
                strBuilder.append(",");
                isAfterFirst = true;
            }
            strBuilder.append(partner.getCode());
        }
        strBuilder.append("]");
        return strBuilder.toString();
    }
}