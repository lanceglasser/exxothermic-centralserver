package mycentralserver.utils;

/**
 * Interface to implement the list of actions on a controller
 * 
 * @author sdiaz
 * @since 02/25/2016
 */
public interface ActionNames {

    public String actionName();
    
    public String name();
}