package mycentralserver.utils;

import java.util.Locale;


public class MessageUtils {
	public static String determineLangVal (Locale locale ) {
		String langVal;

		if (locale != null) {
			if (locale.getLanguage() == "en") {
				langVal = "";
			} else if (locale.getLanguage() == "pt") {
				langVal = "_pt_BR";
			} else if (locale.getLanguage() == "zh") {
				langVal = "_zh_CN";
			} else if (locale.getLanguage() == "") {
				langVal = "";
			} else {
				langVal = "_"+locale.getLanguage();
			} 
		} else {
			langVal = "";
		}

		return langVal;
	}
}
