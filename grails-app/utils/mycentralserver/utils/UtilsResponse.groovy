package mycentralserver.utils

import java.util.Map;

/**
 * Object to store the response of an Utils method with relevant information of the execution.
 * 
 * @author sdiaz
 * @since 10/27/2015
 */
class UtilsResponse {

    private boolean result;
    private String msgCodeToTranslate;
    private Map<String, Object> objects;

    public UtilsResponse(){
        this.result = false;
        this.msgCodeToTranslate = "";
    }

    public UtilsResponse(final boolean result, final boolean msgCodeToTranslate){
        this.result = result;
        this.msgCodeToTranslate = msgCodeToTranslate;
    }

    public boolean getResult() {
        return result;
    }

    public UtilsResponse setResult(final boolean result) {
        this.result = result;
        return this;
    }

    public String getMsgCodeToTranslate() {
        return msgCodeToTranslate;
    }

    public UtilsResponse setMsgCodeToTranslate(final String msgCodeToTranslate) {
        this.msgCodeToTranslate = msgCodeToTranslate;
        return this;
    }

    /**
     * Will set the result as false with the code of the error desire error message
     * 
     * @param msgCodeToTranslate
     *          Key of the error message
     */
    public void setFailResult(final String msgCodeToTranslate){
        if(this.msgCodeToTranslate.equals("")) {
            this.msgCodeToTranslate = msgCodeToTranslate;
        }
        this.result = false;
    }

    /**
     * This method adds a new object to list of objects of the response
     * 
     * @param key
     *      Key to be use on the Map for the object
     * @param obj
     *      Object to be use as the value on the Map for the received key
     */
    public void addObjectToResponse(final String key, final Object obj) {
        if(objects == null) {
            objects = new HashMap<String, Object>();
        }
        objects.put(key, obj);
    }

    /**
     * Returns an object from the Map using the received key, if the map or the object don't exists will return null
     * 
     * @param key
     *      Key of the object to find on the Map
     * @return Object from the Map using the received key
     */
    public Object getObject(final String key) {
        return (objects == null)? null:objects.get(key);
    }
}