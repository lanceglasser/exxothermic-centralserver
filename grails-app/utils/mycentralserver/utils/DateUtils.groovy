package mycentralserver.utils

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Date;

/**
 * Utility class with helper methods for the use of Dates on the system
 * 
 * @author sdiaz
 * @since 11/12/2015
 */
class DateUtils {

    public static final String SCHEDULER = "scheduler";
    public static final String DATE_FORMAT = "MM/dd/yyyy";
    public static final String FILE_DATE_FORMAT = "MM-dd-yyyy";
    public static final String MYSQL_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final DateFormat MYSQL_DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final DateFormat CHARTS_DATE_FORMATTER = new SimpleDateFormat("MM/dd");

    /**
     * Will return the current date using the system format
     *
     * @return Current Date
     */
    public static Date getCurrentDate() {
        DateFormat utcFormat = new SimpleDateFormat(DATE_FORMAT + " HH:mm:ss");
        DateFormat formatTimeZone = new SimpleDateFormat(DATE_FORMAT + " HH:mm:ss");
        Date currentDate = (Date) utcFormat.parse(formatTimeZone.format(new Date()));
        return currentDate;
    }

    /**
     * Will return the current date using the system format as a String
     * 
     * @return Current Date
     */
    public static String getSimpleCurrentDateFormat() {
        DateFormat utcFormat = new SimpleDateFormat(FILE_DATE_FORMAT);
        return utcFormat.format(new Date());
    }

    /**
     * Returns the converted string to a Date object
     * 
     * @param maxDateAllowed
     *      String with the value for the maximum allowed date
     * @return Date object of the maximum date including the time
     */
    public static Date getMaxAllowedDate(final String maxDateAllowed) {
        DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT + " HH:mm:ss");
        return dateFormat.parse(maxDateAllowed + " 23:59:59");
    }

    /**
     * This method receives a String with a date and validates that the date is valid and returns the Date object
     *
     * @param possibleDate
     *                      Date as a String
     * @param timeStr
     *                      Time of the day for the validation, example 23:59:59
     * @return Date from the String or Null when invalid
     */
    public static Date isValidDate(final String possibleDate, final String timeStr) {
        try {
            if (possibleDate.length() == DATE_FORMAT.length()) { // Must have the same number of characters
                DateFormat sdf = new SimpleDateFormat(DATE_FORMAT + " HH:mm:ss");
                sdf.setLenient(false);
                return sdf.parse(possibleDate + " " + timeStr);
            } else {
                return null;
            }
        } catch(java.text.ParseException e) {
            return null;
        }
    }

    /**
     * This method receives a String with a date and validates that the date is valid and returns the Date object for the
     * end of the day, 23:59:59
     *
     * @param maybeDate
     *      Date as a String
     * @return Date from the String or Null when invalid
     */
    public static Date isValidDateAtEndOfDay(String maybeDate) {
        return isValidDate(maybeDate, "23:59:59");
    }

    /**
     * This method receives a String with a date and validates that the date is valid and returns the Date object for the
     * start of the day, 00:00:00
     *
     * @param maybeDate
     *      Date as a String
     * @return Date from the String or Null when invalid
     */
    public static Date isValidDateAtStartOfDay(String maybeDate) {
        return isValidDate(maybeDate, "00:00:00");
    }

    /**
     * Return a received date as a String using the required MySQL format
     * 
     * @param date
     *          Date to format
     * @return String with the date on the required format
     */
    public static String getMySqlFormattedDate(Date date) {
        MYSQL_DATE_FORMATTER.format(date)
    }

    /**
     * Return a received date as a String using the required charts format
     *
     * @param date
     *          Date to format
     * @return String with the date on the required format
     */
    public static String getChartsFormattedDate(Date date) {
        CHARTS_DATE_FORMATTER.format(date)
    }

    /**
     * Format the received date when is not null using the format MM/dd/yyyy
     * 
     * @param date
     * 
     * @return Date as a String with format MM/dd/YYYY
     */
    public static String formatDate(Date date) {
        final DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT)
        return date? dateFormat.format(date):'-';
    }
}