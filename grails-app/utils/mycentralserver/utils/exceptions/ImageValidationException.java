package mycentralserver.utils.exceptions;

public class ImageValidationException extends Exception {

	private static final long serialVersionUID = 1997753363232807009L;

	public ImageValidationException() {
	}

	public ImageValidationException(String message) {
		super(message);
	}

	public ImageValidationException(Throwable cause) {
		super(cause);
	}

	public ImageValidationException(String message, Throwable cause) {
		super(message, cause);
	}

	public ImageValidationException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}