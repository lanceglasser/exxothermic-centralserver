package mycentralserver.utils

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.UrlValidator;

/**
 * Helper for some of the validations to be use on services and controllers
 * 
 * @author sdiaz
 * @since 11/08/2015
 *
 */
class ValidationHelper {

    private static final String HEX_PATTERN = '^#([A-Fa-f0-9]{6})$';

    /**
     * Validates if a string is a valid URL
     * 
     * @param url
     *      URL to review
     * @return True if the string is a valid URL
     */
    static boolean isValidUrl(final String url) {
        UrlValidator urlValidator = new UrlValidator();
        return (StringUtils.isNotBlank(url) && urlValidator.isValid(url));
    }

    /**
     * Validates if a string is not a valid URL
     * 
     * @param url
     *      URL to review
     * @return True if the string is not a valid URL
     */
    static boolean isInvalidUrl(final String url) {
        return !isValidUrl(url);
    }

    /**
     * Validates if a string is a valid color with a hexadecimal value, a valid color is on the form #2a5f3d
     *
     * @param color
     *      Color string to review
     * @return True if the string is a valid hexadecimal color
     */
    static boolean isValidColor(final String color) {
        final Pattern pattern = Pattern.compile(HEX_PATTERN);
        final Matcher matcher = pattern.matcher(color);
        return (StringUtils.isNotBlank(color) && matcher.matches());
    }

    /**
     * Validates if a string is not a valid Color, a valid color is on the form #2a5f3d
     *
     * @param color
     *      Color to review
     * @return True if the string is not a valid Color
     */
    static boolean isInvalidColor(final String color) {
        return !isValidColor(color);
    }

    static boolean isValidInteger(final String value) {
        try {
            final int number = Integer.parseInt(value);
            return true;
        } catch(NumberFormatException e) {
            return false;
        }
    }
}