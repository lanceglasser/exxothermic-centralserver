package mycentralserver.utils

import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

import mycentralserver.generaldomains.Scheduler;

/**
 * Util class with helper methods for the use of the Scheduler component
 * 
 * @author sdiaz
 * @since 10/26/2015
 */
class SchedulerUtils {

    public static final String SCHEDULER = "scheduler";

    /**
     * This method validates the scheduler definition and return a result including the scheduler object
     * 
     * @param params
     *      Request parameters
     * @param maxDateAllowed
     *      Value as a String of the maximum allowed date from the configuration
     * @return Object with the result of the validation
     */
    public static UtilsResponse validateSchedulerInformation(params, final String maxDateAllowed, Scheduler schedule) {
        final String startDateStr = params.start_date? params.start_date.trim():"";
        final String endDateStr = params.end_date? params.end_date.trim():"";
        final String daysStr = params._content_days? params._content_days.trim():"";
        final String hoursStr = params._content_hours? params._content_hours.trim():"";
        Scheduler mySchedule = schedule? schedule:new Scheduler();
        mySchedule.days = daysStr;
        mySchedule.hours = hoursStr;
        UtilsResponse utilsResponse = new UtilsResponse();
        Date currentDate = DateUtils.getCurrentDate();
        Date maxAllowedDate = DateUtils.getMaxAllowedDate(maxDateAllowed);
        Date startDate = DateUtils.isValidDateAtStartOfDay(startDateStr);
        String errorMessageCode = "";
        if (startDate == null || startDate > maxAllowedDate) {
            utilsResponse.setFailResult("schedule.error.invalid.start.date");
        }
        Date endDate = null;
        if (!endDateStr.equals("")) {
            endDate = DateUtils.isValidDateAtEndOfDay(endDateStr);
            if(endDate == null || endDate < startDate || endDate < currentDate || endDate > maxAllowedDate) {
                utilsResponse.setFailResult("schedule.error.invalid.end.date");
            }
        }
        mySchedule.startDate = startDate;
        mySchedule.endDate = endDate;
        if (daysStr.equals("")) {
            utilsResponse.setFailResult("schedule.error.emptyDays");
        }
        // No error, set the result to true since the default is false
        if (utilsResponse.getMsgCodeToTranslate().equals("")) {
            utilsResponse.setResult(true);
        }
        // Attach the scheduler object to the response
        utilsResponse.addObjectToResponse(SCHEDULER, mySchedule);
        return utilsResponse;
    }

    /**
     * Returns the epoch long for a received date
     *
     * @param dt
     *          Date to retrieved the epoch time
     * @param timezone
     *          Time zone to use for the date
     * @return Long with the epoch value for the received date on the time zone
     */
    public static long strDateToUnixTimestamp(Date dt, String timezone) {
        if (dt == null) { return 0;}
        DateFormat timeZoneID = new SimpleDateFormat("Z");
        timeZoneID.setTimeZone(TimeZone.getTimeZone(timezone));
        final Date utcDate = new SimpleDateFormat(DateUtils.MYSQL_DATE_FORMAT + " Z")
                .parse(DateUtils.MYSQL_DATE_FORMATTER.format(dt) +" "+  timeZoneID.format(dt));
        return (utcDate.getTime() / 1000L);
    }
}