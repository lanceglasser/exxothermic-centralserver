/**
 *  Origin: http://theopentutorials.com/tutorials/java/util/generating-a-random-password-with-restriction-in-java/
 * */
package mycentralserver.utils

import java.security.SecureRandom;

import org.apache.commons.lang.RandomStringUtils;

class PasswordGeneratorHelper {
	
	private static def ALPHA_CAPS  = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	private static def ALPHA   = "abcdefghijklmnopqrstuvwxyz"
	private static def NUM     = "0123456789"
	private static def SPL_CHARS   = "!@#?_-%\$"
 	
	private static getSecureRandom() {
		return  new SecureRandom();
	}
	
	public static generatePassword2() {
		//def random = getSecureRandom()		
		//return new BigInteger(130, random).toString(32)
		return RandomStringUtils.random(12)
	}
	
	public static generatePassword() { 
		int minLen = 9
		int maxLen = 15
		int noOfCAPSAlpha = 3  
		int noOfDigits = 3
		int noOfSplChars = 3
		return generatePssw(minLen, maxLen, noOfCAPSAlpha, noOfDigits, noOfSplChars).toString()
	}
	
	public static generateSerial() {
		int minLen = 20
		int maxLen = 20
		int noOfCAPSAlpha = 10
		int noOfDigits = 10
		int noOfSplChars = 0
		return generatePssw(minLen, maxLen, noOfCAPSAlpha, noOfDigits, noOfSplChars).toString()
	}
	
	public static generateTokenForPasswordReset() {
		int minLen = 12
		int maxLen = 15
		int noOfCAPSAlpha = 3
		int noOfDigits = 3
		int noOfSplChars = 0
		return generatePssw(minLen, maxLen, noOfCAPSAlpha, noOfDigits, noOfSplChars).toString()
	}
	
	public static char[] generatePssw(int minLen, int maxLen, int noOfCAPSAlpha, int noOfDigits, int noOfSplChars) {
	
		if(minLen > maxLen)
			throw new IllegalArgumentException("Min. Length > Max. Length!");
		if( (noOfCAPSAlpha + noOfDigits + noOfSplChars) > minLen )
			throw new IllegalArgumentException
			("Min. Length should be atleast sum of (CAPS, DIGITS, SPL CHARS) Length!");
		Random rnd = new Random();
		int len = rnd.nextInt(maxLen - minLen + 1) + minLen;
		char[] pswd = new char[len];
		int index = 0;
		for (int i = 0; i < noOfCAPSAlpha; i++) {
			index = getNextIndex(rnd, len, pswd);
			pswd[index] = ALPHA_CAPS.charAt(rnd.nextInt(ALPHA_CAPS.length()));
		}
		for (int i = 0; i < noOfDigits; i++) {
			index = getNextIndex(rnd, len, pswd);
			pswd[index] = NUM.charAt(rnd.nextInt(NUM.length()));
		}
		for (int i = 0; i < noOfSplChars; i++) {
			index = getNextIndex(rnd, len, pswd);
			pswd[index] = SPL_CHARS.charAt(rnd.nextInt(SPL_CHARS.length()));
		}
		for(int i = 0; i < len; i++) {
			if(pswd[i] == 0) {
				pswd[i] = ALPHA.charAt(rnd.nextInt(ALPHA.length()));
			}
		}
		return pswd;
	}
 
	private static int getNextIndex(Random rnd, int len, char[] pswd) {
		int index = rnd.nextInt(len);
		while(pswd[index = rnd.nextInt(len)] != 0);
		return index;
	}
}
