package mycentralserver.utils

import java.security.MessageDigest

class DigestHelper {

	static def generateMD5Checksum(byte[] data) {
		return generateChecksum(data, 'MD5')
	}
	
	static def generateChecksum(byte[] data, String digestAlgorithm) {
		
		if(digestAlgorithm ==  null) {
			digestAlgorithm = 'MD5'
		}
		
		MessageDigest md = MessageDigest.getInstance(digestAlgorithm)
		md.update(data)
		def dataHashed = md.digest().encodeHex();
		return dataHashed//md.digest().encodeHex()
	}
}