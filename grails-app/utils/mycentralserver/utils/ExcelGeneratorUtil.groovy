/**
 * ExcelGeneratorUtil.groovy
 */
package mycentralserver.utils

import jxl.Workbook;
import jxl.write.*;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import org.apache.commons.lang.math.NumberUtils;

/**
 * Util class use for the generation of Excel files in a 
 * easy way; this util works with the jxl library.
 * 
 * @author Cecropia Solutions
 *
 */
class ExcelGeneratorUtil {

	public final static int STRING_TYPE = 0;
	public final static int INTEGER_TYPE = 1;
	public final static int DOUBLE_TYPE = 2;
	public final static int DATE_TYPE = 3;
	
	/**
	 * Creates a Sheet into a workbook for a table with columns headers
	 * 
	 * @param workbook		Workbook to work
	 * @param name			Name of the new sheet
	 * @param headers		Column Headers
	 * @param headerRow		Row where to put the header row
	 * @return				Created Sheet 
	 */
	static WritableSheet createTableSheet(WritableWorkbook workbook, String name, def headers, int headerRow) {
		
		WritableSheet sheet = workbook.createSheet(name, 0);
		
		WritableFont headersFont = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);
				 
		WritableCellFormat headersFormat = new WritableCellFormat (headersFont);
		headersFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		headersFormat.setAlignment(Alignment.CENTRE);
		
		for(int i=0; i < headers.size(); i++){
			sheet.addCell(new Label(i+1, headerRow, " " + headers[i] + " ", headersFormat));
		}
		
		return sheet;
	} // End of createTableSheet
	
	/**
	 * Sets the autosize option to a range of columns into a sheet
	 * 
	 * @param sheet					Sheet where to work
	 * @param initialColumnIndex	Initial column number; first column is 0 (A)
	 * @param finalColumnIndex		Final column number to apply the option
	 */
	public static void setColumnsAutosize(WritableSheet sheet, int initialColumnIndex, int finalColumnIndex){
		
		def cellView = null;
		
		for(int i=initialColumnIndex; i <= finalColumnIndex; i++){
			cellView = sheet.getColumnView(i);
			cellView.setAutosize(true);
			sheet.setColumnView(i, cellView);
		}		
	} //End of setColumnsAutosize
	
	/**
	 * Writes an array of information to a row in a sheet
	 * 
	 * @param sheet			Sheet where to put the row
	 * @param rowIndex		Row Index
	 * @param dataArray		Array with the information
	 * @param format		Format of the Cell
	 */
	public static void addTableRow(WritableSheet sheet, int rowIndex, def dataArray, WritableCellFormat format) {
		
			for(int i = 0; i < dataArray.size(); i++){
				if(NumberUtils.isNumber(""+dataArray[i])){					
					sheet.addCell(new Number(i+1, rowIndex, Double.parseDouble(""+dataArray[i])));
				}else{
					sheet.addCell(new Label(i+1, rowIndex, ""+dataArray[i]));
				}
				
			}
		
	} //End of addTableRow
	
}