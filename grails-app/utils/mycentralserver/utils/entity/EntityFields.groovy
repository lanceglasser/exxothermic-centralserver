package mycentralserver.utils.entity

/**
 * Interface that define the required methods for the EntityFields enumerations
 * 
 * @author sdiaz
 * @since 03/02/2015
 */
public interface EntityFields {

    /**
     * Retrieves the name of field at the domain 
     * 
     * @return Field name
     */
    public String fieldName();

    /**
     * Enumeration method that returns the name of the enumeration object
     * 
     * @return String name of the enumeration object
     */
    public String name();

    /**
     * Returns the default value if exists for the field
     * 
     * @return Default value of the field
     */
    public Object getDefault();
}