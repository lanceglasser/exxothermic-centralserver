package mycentralserver.utils.entity

import java.util.Date;

import mycentralserver.app.AppSkin;
import mycentralserver.app.WelcomeAd;
import mycentralserver.company.Company;
import mycentralserver.company.CompanyCatalogSubType;
import mycentralserver.company.CompanyIntegrator;
import mycentralserver.generaldomains.Country;
import mycentralserver.generaldomains.State;
import mycentralserver.user.User;

/**
 * Class with the definition of the domains fields
 * 
 * @author sdiaz
 * @since 03/02/2016
 */
public class EntitiesCatalog {

    /**
     * CommonProperties is for properties that are common in the DBEntities
     */
    public static class CommonProperties {
        public static enum SimpleFields implements EntityFields {
            ID("id", 0),
            CREATION_DATE("creationDate", null),
            LAST_MODIFICATION_DATE("lastModificationDate", null);

            private String fieldName;
            private Object defaultValue;

            private SimpleFields(String fieldName, Object defaultValue) {
                this.fieldName = fieldName;
                this.defaultValue = defaultValue;
            }

            public String fieldName() {
                return fieldName;
            }

            public Object getDefault() {
                return defaultValue;
            }
        }
    }

    public static class VenueProperties {
        public static enum SimpleFields implements EntityFields {
            NAME("name", null),
            ADDRESS("address", null),
            CITY("city", null),
            MAX_OCCUPANCY("maxOccupancy", new Integer(0)),
            NUMBER_OF_TV("numberOfTv", new Integer(0)),
            NUMBER_OF_TV_WITH_DEVICES("numberOfTvWithExxothermicDevice", new Integer(0)),
            CONTENTS_LIMIT("contentsLimit", new Integer(6)),
            OTHER_TYPE("otherType", null),
            LATITUDE("latitude", null),
            LONGITUDE("longitude", null),
            TIMEZONE("timezone", null),
            LOGO_URL("logoUrl", ""),
            POC_EMAIL("pocEmail", ""),
            POC_PHONE("pocPhone", ""),
            POC_FIRST_NAME("pocFirstName", ""),
            POC_LAST_NAME("pocLastName", ""),
            POC_ACCOUNT_CREATED("pocAccountCreated", new Boolean(false)),
            ENABLE("enable", new Boolean(true)),
            USE_COMPANY_LOGO("useCompanyLogo", new Boolean(true)),
            COMPANY("company", null),
            COUNTRY("country", null),
            STATE_NAME("stateName", null),
            STATE("state", null),
            TYPE("type", null),
            ADMINISTRATOR("administrator", null),
            COMMERCIAL_INTEGRATOR("commercialIntegrator", null),
            ACTIVE_SKIN("activeSkin", null),
            WELCOME_AD("welcomeAd", null),
            CONTENTS_ORDER("contentsOrder", null),
            TOTAL_CONTENTS("totalContents", new Integer(0)),
            LAST_BOX_USRS_UPDATE("lastBoxUsersUpdate", null);

            private String fieldName;
            private Object defaultValue;

            private SimpleFields(String fieldName, Object defaultValue) {
                this.fieldName = fieldName;
                this.defaultValue = defaultValue;
            }

            public String fieldName() {
                return fieldName;
            }

            public Object getDefault() {
                return defaultValue;
            }
        }
    }
}