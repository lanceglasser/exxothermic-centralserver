package mycentralserver.utils

class Constants {
    // validate the field not conatins < , >, not allow XSS
    public static def REGULAR_EXPRESSION_FOR_PASSWORD_FIELD = '((?=.*\\d)(?=.*[a-zA-Z])(?=.*[%&@#-_$]).{6,})'
    public static def REGULAR_EXPRESSION_FOR_STRING_FIELD = /^[\p{L}\d\ \%\&\@\#\-\_\.\:\/\']+$/
    public static def REGULAR_EXPRESSION_FOR_CHANNELS_STRING 	=  '([a-zA-Z0-9\\s]+)'
    public static def REGULAR_EXPRESSION_FOR_ADDRESS_FIELD 		=  /([\d\s\w\S\n]*)/
    public static def REGULAR_EXPRESSION_FOR_PHONE_NUMBER = /^(\+)?(([0-9]+)(([\.\-\(\)\/\ ])+([0-9]+))?)+$/
    public static def REGULAR_EXPRESSION_FOR_SOFTWARE_VERSION_LABEL = /^\d+(\.\d+(\.\d+)?)?$/

	public static def  PARAM_OFFSET = "offset"

	//Catalog Constants If Id is different change the constants
	public static final String BRANDS="BRANDS"
	public static final String MEMORIES="MEMORY"
	public static final String MEMORIES_TYPE="MEMORY_TYPES"
	//End Catalog Constants


	public static final String ACTION_GENERATE_CERTIFICATE = "generateCertificate"
	public static final String ADMIN_USER_EMAIL = "admin@exxothermic.com"

	public static final String ACTION_VALIDATE_CERTIFICATE = "validateCertificate"

	public static def OK = 200
	public static def CREATED = 201
	public static def NO_CONTENT = 204
	public static def BAD_REQUEST = 400
	public static def NOT_FOUND = 401
	public static def CONFLICT = 409
	public static def INTERNAL_ERROR = 500
	
	public final static int RESULT_ERROR = 500;
	public final static int RESULT_OK = 200;
	public final static int RESULT_NOT_FOUND = 404;
	public final static int RESULT_NOT_ALLOWED = 403; 

	public static final String CONTENT_TYPE_IMAGE = "image"
	public static final String CONTENT_TYPE_TEXT = "text"
	public static final String CONTENT_TYPE_OFFER = "offer"
	
	public static final Float MIN_TEXT_BANNER_SUPPORTED = 2.10; // min version that support text banners
	
	public static final String OFFER_REFERENCE_URL = "URL"
	public static final String OFFER_REFERENCE_PDF = "PDF"
	public static final String DEFAULT_OFFERS_CATEGORY = "OFFERS"

	public static final String  ENCRYPTION_ALGORITHM_AES_CBC = "AES/CBC/PKCS5PADDING"
	public static final String  ENCRYPTION_ALGORITHM_AES = "AES"
	
	public static final int MAXIMUM_SHORTCUTS = 6;
	//RSA Encription Constants
	/**
	 * String to hold name of the encryption algorithm.
	 */
	// public static final String ALGORITHM = "RSA";
	public static final String ALGORITHM = "RSA/ECB/PKCS1Padding";

	/**
	 * String to hold the name of the private key file.
	 */
	public static final String PRIVATE_KEY_FILE_NAME = "private.der";

	/**
	 * String to hold name of the public key file.
	 */
	public static final String PUBLIC_KEY_FILE_NAME = "public.der";

	public static final String MAX_BOX_USERS_FOR_LOCATION = 5;
    public static final String MAX_DISTINCT_ALLOWED_IP_CONNECTIONS=4
	public static final String PARAM_MAX = "max"
	public static final String PARAM_COMPANIES="companies"
	public static final String PARAM_EMPLOYEERS="employeers"
	public static final String PARAM_BOXES="boxes"
	public static final String PARAM_LOCATIONS="locations"
	public static final String PARAM_LOCATION="location"
	public static final String PARAM_SKIN="skin"
	public static final String PARAM_WELCOME_AD="welcomeAd";
	public static final String PARAM_HEADER_IMG='headerImg'
	public static final String PARAM_SPLASH_IMG='splashImage'
	public static final String PARAM_HEADER_TEXT_COLOR="headerTextColor"
	public static final String PARAM_HEADER_TEXT="headerText"
	public static final String PARAM_BACKGROUND_COLOR="backgroundColor"
	public static final String PARAM_MENU_FONT="menuFont"
	public static final String PARAM_MENU_TEXT_COLOR="menuTextColor"
	public static final String PARAM_ID="id"
	public static final String PARAM_TITLE="title";
	public static final String PARAM_MAIN_COLOR="mainColor";
	public static final String PARAM_ADS_BACKGROUND_IMG="bgImageUrl"
	public static final String PARAM_WELCOME_FILE_IMG="welcomeImgUrl"
	public static final String PARAM_WELCOME_FILE_VIDEO="welcomeVideoUrl"
	public static final String JOBS = "jobs"
	public static final String INVALID_EXTENSION = "invalidExtension"
	public static final String PARAM_UPLOAD_RESULT = "uploadResult";
	public static final String PARAM_SERIAL_NUMBER = "serialNumber";
	public static final String PARAM_USERS = "users";
	public static final String PARAM_DOCUMENTS = "documents";
	public static final String PARAM_CUSTOM_BUTTONS = "customButtons";
	public static final String PARAM_OFFERS = "offers";
	public static final String PARAM_TECH_USER_NAME = "techUserName";
	public static final String PARAM_PARTNER_ID = "partnerId";
	public static final String PARAM_IOS_DOWNLOAD_URL = "iosDownloadUrl";
	public static final String PARAM_ANDROID_DOWNLOAD_URL = "androidDownloadUrl";
	public static final String PARAM_USERNAME = "username";
	public static final String PARAM_PASSWORD = "password";
	public static final String PARAM_LEVEL = "level";
	public static final String PARAM_ACTION = "action";
	public static final String PARAM_LAST_UPDATE = "lastUpdate";
	public static final String PARAM_VERSION = "version";
	public static final String PARAM_CHECKSUM = "checksum";
	public static final String PARAM_PACKAGE_URI = "packageUri";
	public static final String PARAM_CODE = "code";
	public static final String PARAM_VALUE = "value";
	public static final String PARAM_DEVICES = "devices";
	public static final String PARAM_FILE = "file";
	public static final String PARAM_FILE_UPDATED = "file-updated";
	public static final String PARAM_MASTER = "master";
	public static final String PARAM_RELAY = "relay";
	public static final String PARAM_OFF = "off";	
	public static final String PARAM_UPDATED_VALUE = "1";
	public static final String PARAM_COMPANY_ID = "companyId";
	public static final String PARAM_OWNER_ID = "ownerId";
	public static final String PARAM_BG_LOGOUT_URL = "bgLogoUrl";
    public static final String PARAM_AUXILIAR_PA = "_pa";
    public static final String PARAM_HMAC = "hmac";
    public static final String PARAM_ARCHITECTURE = "architecture";
    public static final String PARAM_PARTNER = "partner";
    public static final String PARAM_AUDIO_CARD_GENERATION = "audio card generation";
    public static final String PARAM_JUMPER_CONFIGURATION = "jumper configuration";
    public static final String PARAM_AUDIO_INPUT_MODE = "audio input mode";
    public static final String PARAM_SMALL_IMAGE_URL_FILE = "smallImageUrlFile";
    public static final String PARAM_WELCOME_VIDEO_URL_UPDATED = "welcomeVideoUrl-updated";
    public static final String PARAM_FAKEUPLOAD = "fakeupload";
    public static final String PARAM_VIDEO = "VIDEO";
    public static final String PARAM_NOTIFICATIONS = "notifications";
    public static final String PARAM_NOTIFICATIONS_INFO = "notificationInfo";
    public static final String PARAM_TIME = "time";
    public static final String PARAM_CHANNEL = "channel";
    public static final String PARAM_USER = "user";
    public static final String PARAM_PORT = "port";
    public static final String PARAM_LABEL = "label";
    public static final String PARAM_IS_PA = "isPa";
    public static final String PARAM_DESCRIPTION = "description";
    public static final String PARAM_IMAGE_URL = "imageUrl";
    public static final String PARAM_LARGE_IMAGE_URL = "largeImageUrl";
    public static final String PARAM_NUMBER_OF_CHANNELS = "numberOfChannels";
    public static final String PARAM_PA_MODE = "PAMode";
    public static final String PARAM_CHANNELS = "channels";

	public static final String VIEW_CREATE = "create"
	public static final String VIEW_LIST_ALL= "listall"
	//noticed that one is UPPER CASE
	public static final String VIEW_LISTALL = "listAll"
	public static final String VIEW_INDEX = "index"
	public static final String VIEW_SHOW = "show"
	public static final String VIEW_EDIT = "edit"
	public static final String VIEW_NOT_COMPANY ="notcompany"
	public static final String VIEW_ASSIGN_INTEGRATOR="assignIntegrator"
	public static final String VIEW_ASSIGN_LOCATION="assignLocation"
	public static final String VIEW_NOT_INTEGRATOR ="notintegrator"
	public static final String VIEW_NOT_LOCATIONS ="notlocations"
	public static final String VIEW_NO_SKINS ="noSkins"
	public static final String VIEW_NOT_USER ="notuser"
	public static final String VIEW_NOT_EMPLOYEES= "notEmployees"
	public static final String VIEW_NOT_EMPLOYEES_TO_ASSIGN="notEmployeesToAssign"
	public static final String VIEW_NOT_MANUFACTURER= "notManufacturer"
	public static final String VIEW_MYACCOUNT="myaccount"
	public static final String VIEW_CHANGE_USER_PASSWORD="changeUserPassword"
	public static final String VIEW_FIRST_PAGE= "_first_page"
	public static final String VIEW_CREATE_COMPANY="_create_company"
	public static final String VIEW_CREATE_LOCATION="_create_location"
	public static final String VIEW_EXIT_FLOW="_exit_flow"
	public static final String VIEW_FINAL_PAGE="_final_page"
	public static final String VIEW_REGISTER_BOX = "_register_box"
	public static final String VIEW_ASSIGN_COMPANY="assignCompany"
	public static final String VIEW_LIST_NON_ASSIGN="listAllNonAssign"
	public static final String VIEW_ASSIGN_SKIN ="assignSkin"
	public static final String VIEW_EDIT_RESULTS="editResults"

	public static final String ASSIGN_SKIN_TEMPLATE="assignSkinTemplate"
	public static final String SORT_ASC="asc"
    public static final String SORT_DESC="desc";
	public static final String NAME="name"
	public static final String EMAIL="email"
	public static final String FIRST_NAME="firstName"
	public static final String COMPANY_NAME= "company.name"
	public static final String CREATE_COMPANY= "createCompany"
	public static final String CREATE_LOCATION="createLocation"
	public static final String WELCOME_PAGE= "welcomePage"
	public static final String REGISTER_BOX= "registerBox"
	public static final String FINAL_PAGE= "finalPage"
	public static final String EXIT_FLOW= "exitFlow"
	public static final String SAVE_BOX = "saveBox"
	public static final String ERROR= "error"
	public static final String SUCCESS = "success"
	public static final String CONTINUE= "continue"
	public static final String CANCEL = "cancel"
	public static final String NEXT = "next"
	public static final String PREVIOUS = "previous"
	public static final String SAVE_LOCATION = "saveLocation"
	public static final String SAVE_COMPANY ="saveCompany"
	public static final String PASSWORD= "password"
	public static final String DEFAULT_THEME = "default"; 
	public static final String HEADER_HOST = "Host";
    public static final String VALID_CHANNEL_CONTENT = "USB";

	public static final String CONTROLLER_HOME= "home"
	public static final String CONTROLLER_COMPANY= "company"
	public static final String CONTROLLER_LOCATION="location"
	public static final String CONTROLLER_USER="user"
	public static final String CONTROLLER_LOGIN="login"
	public static final String CONTROLLER_EMPLOYEE="employee"
	public static final String CONTROLLER_BOX_MANUFACTURER="boxManufacturer"

	public static final String ESTABLISHMENT_TYPE_OTHER= "other"
	public static final String ESTABLISHMENT_TYPE_WAITING_ROOM= "waitingRoomSpecify"

	// The provider configures jclouds To use the Rackspace Cloud (US)
	// To use the Rackspace Cloud (UK) set the system property or default value to "rackspace-cloudfiles-uk"
	public static final String PROVIDER = System.getProperty("provider.cf", "rackspace-cloudfiles-us");
	public static final String REGION = System.getProperty("region", "DFW");

	public static final String CONTAINER_PUBLISH = "ImageContainer";
	public static final String CONTAINER_DOC_PUBLISH = "DocumentContainer";
	public static final String SUFFIX = "jpg";
	public static final String PNG_SUFFIX = "png";
	public static final int CUSTOM_BUTTON_MAX_SIZE = 5 * 1024 * 1024;//300Kbs
	public static final String[] CUSTOM_BUTTON_ALLOWED_EXTENSIONS = ["jpg", "png"];

	public static final int WELCOME_VIDEO_FILE_MAX_SIZE = 20 * 1024 * 1024; //3MBs
	public static final String[] WELCOME_VIDEO_FILE_ALLOWED_EXTENSIONS = ["avi", "mp4"];

	public static final String DEFAULT_SKIN_TITLE = "Default";

	public static final String DEFAULT_BOX_USERNAME = "tech_exxo";
	public static final String DEFAULT_PARTNER_ID = "1";
	public static final String DEFAULT_IOS_DOWNLOAD_URL = "itms-apps://itunes.apple.com/app/id646388002";
	public static final String DEFAULT_ANDROID_DOWNLOAD_URL = "market://details?id=org.exxothermic.streamCatcher";

	/* Images Values */
	public static final int IMAGE_TO_CROP_MAX_WIDTH = 2456;
	public static final int IMAGE_TO_CROP_MAX_HEIGHT = 1840;
	public static final int IMAGE_TO_CROP_MAX_SIZE = 5 * 1024 * 1024; //5MB
	
	public static final int COMPANY_LOGO_WIDTH = 150;
	public static final int COMPANY_LOGO_HEIGHT = 150;
	
	public static final int CHANNEL_IMAGE_LARGE_WIDTH = 600;
	public static final int CHANNEL_IMAGE_LARGE_HEIGHT = 200;
	public static final int CHANNEL_IMAGE_SMALL_WIDTH = 200;
	public static final int CHANNEL_IMAGE_SMALL_HEIGHT = 200;
	
	public static final int FEATURED_CONTENT_IMAGE_WIDTH = 640;
	public static final int FEATURED_CONTENT_IMAGE_HEIGHT = 280;
	public static final int DIALOG_CONTENT_IMAGE_WIDTH = 640;
	public static final int DIALOG_CONTENT_IMAGE_HEIGHT = 500;
	public static final int THUMBNAIL_CONTENT_IMAGE_WIDTH = 288;
	public static final int THUMBNAIL_CONTENT_IMAGE_HEIGHT = 384;
	
	public static final int FEATURED_CONTENT_IMAGE_TABLET_WIDTH = 640;
	public static final int FEATURED_CONTENT_IMAGE_TABLET_HEIGHT = 500;
	public static final int DIALOG_CONTENT_IMAGE_TABLET_WIDTH = 924;
	public static final int DIALOG_CONTENT_IMAGE_TABLET_HEIGHT = 462;
	public static final int THUMBNAIL_CONTENT_IMAGE_TABLET_WIDTH = 900;
	public static final int THUMBNAIL_CONTENT_IMAGE_TABLET_HEIGHT = 720;
	
	public static final int FEATURED_CONTENT_CROP_WIDTH = 640;
	public static final int FEATURED_CONTENT_CROP_HEIGHT = 280;
	public static final int DIALOG_CONTENT_CROP_WIDTH = 640;
	public static final int DIALOG_CONTENT_CROP_HEIGHT = 500;
	public static final int THUMBNAIL_CONTENT_CROP_WIDTH = 288;
	public static final int THUMBNAIL_CONTENT_CROP_HEIGHT = 384;
	public static final int TEXT_CONTENT_BACKGROUND_WIDTH = 640;
	public static final int TEXT_CONTENT_BACKGROUND_HEIGHT = 500;
	
	public static final int FEATURED_CONTENT_CROP_TABLET_WIDTH = 640;
	public static final int FEATURED_CONTENT_CROP_TABLET_HEIGHT = 500;
	public static final int DIALOG_CONTENT_CROP_TABLET_WIDTH = 924;
	public static final int DIALOG_CONTENT_CROP_TABLET_HEIGHT = 462;
	public static final int THUMBNAIL_CONTENT_CROP_TABLET_WIDTH = 288;
	public static final int THUMBNAIL_CONTENT_CROP_TABLET_HEIGHT = 384;
	public static final int TEXT_CONTENT_BACKGROUND_TABLET_WIDTH = 924;
	public static final int TEXT_CONTENT_BACKGROUND_TABLET_HEIGHT = 462;
	
	public static final int CONTENT_IMAGE_WIDTH = 900;
	public static final int CONTENT_IMAGE_HEIGHT = 720;


	//Images Sizes for different devices for Welcome Ad and App Settings (Skin)
	public static final int IMAGE_PHONE_3_5_WIDTH = 640;
	public static final int IMAGE_PHONE_3_5_HEIGHT = 960;
	public static final int IMAGE_PHONE_3_5_SIZE = 300*1024;//300KB
	public static final int IMAGE_PHONE_4_WIDTH = 640;
	public static final int IMAGE_PHONE_4_HEIGHT = 1136;
	public static final int IMAGE_PHONE_4_SIZE = 2*1024*1024;//2MB
	public static final int IMAGE_TABLET_WIDTH = 2048;
	public static final int IMAGE_TABLET_HEIGHT = 1536;
	public static final int IMAGE_TABLET_SIZE = 3*1024*1024;//3MB

	//public static final String DEFAULT_COMPANY_LOGO_URL = "http://b3156eed02529384f460-241f618633f5fb5fa55cd8e92e0f5f20.r84.cf1.rackcdn.com/AudioEverywhere-Logo.png";
	public static final String DEFAULT_COMPANY_LOGO_URL = "";

	public static final int BOX_USER_TYPE_TECH = 1;
	public static final int BOX_USER_TYPE_ADMIN = 2;
	public static final int BOX_USER_TYPE_USER = 3;
	
	public static final int BOX_SOFTWARE_UPGRADE_STATUS_CREATED = 10;
	public static final int BOX_SOFTWARE_UPGRADE_STATUS_STARTED = 20;
	public static final int BOX_SOFTWARE_UPGRADE_STATUS_COMPLETED = 30;
	
	/* wa & App theme Location Widget */
	public static final String WNAME = "wName";
	public static final String SKIP_ENABLE = "skipEnable";
	public static final String SKIP_TIME = "skipTime";
	public static final String WELCOME_TYPE = "welcomeType";
	public static final String WELCOME_TYPE_CODE = "welcomeTypeCode";
	public static final String SMALL_IMAGE_URL = "smallImageUrl";
	public static final String VIDEO_URL = "videoUrl";
	
	public static final String ANAME = "aName";
	public static final String PRIMARY_COLOR = "primaryColor";
	public static final String SECONDARY_COLOR = "secondaryColor";
	public static final String TITLE = "title";
	public static final String BACKGROUND_IMAGE_URL = "backgroundImageUrl";
	
	
	/* DataTable.Net */
	public static final String LOCATION_ID = "locationId";
	public static final String COMPANY_ID = "companyId";
	public static final String DATA_TABLES_START = "start";
	public static final String DATA_TABLES_LENGTH = "length";
	public static final String DATA_TABLES_ORDER_DIR = "orderDir";
	public static final String DATA_TABLES_ORDER_DIR_PARAM = "order[0][dir]";
	public static final String DATA_TABLES_ORDER_COLUMN = "order[0][column]";
	public static final String DATA_TABLES_SEARCH_PARAM = "search[value]";
	public static final String DATA_TABLES_DRAW = "draw";
	public static final String DATA_TABLES_RECORDS_TOTAL = "recordsTotal";
	public static final String DATA_TABLES_RECORDS_FILTERED = "recordsFiltered";
	public static final String DATA_TABLES_TABLE_DATA = "tableData";
	public static final String DATA_TABLES_DATA = "data";
	public static final String DATA_TABLES_ERROR = "error";
	public static final String DATA_TABLES_SEARCH = "searchText";
	public static final String DATA_TABLES_COLUMN_ORDER_NAME = "columnOrderName";
	
	/* Box Info widget */
	public static final String OFFLINE_NEEDS_UPGRADE = "offline_needs_upgrade";
	public static final String OFFLINE_UPGRADED		 = "offline_upgraded";
	public static final String ONLINE_UPGRADED 		 = "online_upgraded";
	public static final String ONLINE_NEEDS_UPGRADE  = "online_needs_upgrade";
	public static final String OUTDATED				 = "outdated";
	public static final String UNVERSIONED = "unversioned";
	public static final String MIN_SUPPORTED_VERSION 	 = "2.00";
	
	/* Tables fields */
	public static final String WEB_SITE = "webSite";
	public static final String PHONE_NUMBER = "phoneNumber";
	public static final String COMPANY_WIDE_INTEGRATOR = "companyWideIntegrator";
	public static final String TYPE_OF_COMPANY = "typeOfCompany";
	public static final String TYPE_OF_ESTABLISHMENT = "typeOfEstablishment";
	public static final String TIMEZONE = "timezone";
	public static final String ZIP_CODE = "zipCode";
	public static final String ADDRESS = "address";
	public static final String ENABLED = "enabled";
	public static final String IS_INTEGRATOR = "isIntegrator";
	public static final String LOGO = "logo";
	public static final String HAS_LOGO = "hasLogo";
	public static final String COMPANY = "company";
	public static final String COMMERCIAL_INTEGRATOR = "commercialIntegrator";
	public static final String TYPE = "type";
	public static final String USE_COMPANY_LOGO = "useCompanyLogo";
	public static final String CONTENTS_LIMIT = "contentsLimit";
	public static final String MUST_USE_COMPANY_LOGO = "mustUseCompanyLogo";
	public static final String MAX_OCCUPANCY = "maxOccupancy";
	public static final String NUMBER_OF_TV = "numberOfTv"
	public static final String NUMBER_OF_TV_WITH_EXXOTHERMIC_DEVICE = "numberOfTvWithExxothermicDevice";
	public static final String CREATED_BY = "createdBy";
	public static final String GPS_LOCATION = "gpsLocation";
	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";
	public static final String SHORTCUTS = "shortcuts";
	public static final String POC = "poc";
    public static final String FILE_NAME = "fileName";
    public static final String ALL = "All";
    public static final String DATE = "date";
	
	/* Companies Level of Service */
	public static final int COMPANY_LEVEL_OF_SERVICE_BASIC = 1;
	public static final int COMPANY_LEVEL_OF_SERVICE_PREMIUM = 2;
	
	/* Input Types */
	public static final int INPUT_TYPE_LABEL = 0;
	public static final int INPUT_TYPE_SELECT = 1;
	public static final int INPUT_TYPE_TEXT = 2;
	public static final int INPUT_TYPE_NUMBER = 3;
	
	/* Box Configurations Codes */
	public static final String BOX_CONF_CODE_DEBUG_LEVEL = "DEBUG_LEVEL";
	public static final String BOX_CONF_CODE_TOS_FIELD = "TOS_FIELD";
	public static final String BOX_CONF_CODE_VLAN_PRIO = "VLAN_PRIO";
	public static final String BOX_CONF_CODE_PKG_SIZE = "PKG_SIZE";
	public static final String BOX_CONF_CODE_MAINTENANCE_MSG = "MAINTENANCE_MSG";
	
	/* General Configuration Types */
	public static final int CONFIGURATION_TYPE_TEXT = 0;
	public static final int CONFIGURATION_TYPE_LIST = 1;
	
	/* System Configurations Codes */
	public static final String SMALL_PKGS_DEVICES = "SMALL_PKG_DEVICES";
    
    /* Usage Report */
    public static final String USAGE_REPORT_DEFAULT_DEVICE_ID = "OLDAPP";
    public static final String USAGE_REPORT_FIELD_START_EVENT = "start event";
    public static final String USAGE_REPORT_FIELD_STOP_EVENT = "stop event";
    public static final String USAGE_REPORT_FIELD_DURATION = "duration";
    public static final String USAGE_REPORT_FIELD_STREAMINGS = "current streamings";
	
} // END Constants
