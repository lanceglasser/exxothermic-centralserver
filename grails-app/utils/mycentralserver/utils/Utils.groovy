package mycentralserver.utils

import java.nio.charset.Charset;
import static java.nio.charset.StandardCharsets.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Utils {

	static def paginateList( list, params) {
		
		return list
		
		//Keep Commented until works on pagination with the new tables.
		/*if(list.size() > 0){
			def offset
			if (!params.offset){
				 offset = 0
			}else{
				 offset = params.int("offset")
			}
			if (params.max) {
				def off = offset + params.int("max")
				def max = Math.min(off , list.size())
				return list.subList(offset, max)
			}else{
				return list.subList(offset, list.size())
			}
		}else{
			return list
		}*/
	 }
	
	static def sortByName( list) {
		return (list != null)? list.sort{a, b -> a.getName().compareToIgnoreCase(b.getName())}:list;
	}
	
	static def sortByTitle( list) {
		return (list != null)? list.sort{a, b -> a.getTitle().compareToIgnoreCase(b.getTitle())}:list;
	}
	
	static def sortUserByName( list) {
		return list.sort{a, b -> a.getFirstNameAndLastName().compareToIgnoreCase(b.getFirstNameAndLastName())}
	}
	
	static def sortEmployeeByEmail( list) {
		return list.sort{a, b -> a.user.email.compareToIgnoreCase(b.user.email)}
	}
	
	static def sortUserByEmail( list) {
		return list.sort{a, b -> a.email.compareToIgnoreCase(b.email)}
	}
	
	static def sortBoxBySerial( list) {		
		return list.sort{a, b -> a.location.name <=> b.location.name ?: a.name <=> b.name ?: a.serial <=> b.serial}
	}
	
	static def sortListBySerialOnly( list ){
		return (list != null)? list.sort{a, b -> a.getSerial().compareToIgnoreCase(b.getSerial())}:list;
	}
	
	static def sortIntegratorByName( list) {
		return list.sort{a, b -> a.company.name.compareToIgnoreCase(b.company.name)}
	}
	
	static def sortunregisteredBoxBySerial( list) {
		return list.sort{a, b ->  a.serial <=> b.serial}
	}
		
	
	static def parseIntTheValueDoubleAsString(value){
		Double.parseDouble(value) as Integer
	}
	
	static def calculatePositonHeight(height,posy,heightOriginal){
		
	   if(isHeightOutOfRaster(height,posy,heightOriginal)){
		   return heightOriginal - height
	   }
	   return posy
	}
	
	static def calculatePositonWidth(width,posx,widthOriginal){
	   if(isWidthOutOfRaster(width,posx,widthOriginal)){
		   return widthOriginal - width
	   }
	   return posx
	}
		
	static def isHeightOutOfRaster(height,posy,heightOriginal){
	   height + posy > heightOriginal
	}
	
	static def isWidthOutOfRaster(width,posx,widthOriginal){
	   width + posx > widthOriginal
	}
	
	static def String getPathFromFile(String storagePath, String fileNameOrigin ) {
		def extention = FileHelper.getFileExtension(fileNameOrigin)
		def fileName = String.valueOf(System.currentTimeMillis()) + "." + extention
		def path = storagePath + "/" + fileName
		return path
	}
	
	static def String getFileHumanSize(double fileSize) {
		String str = "";
		String unit = "Bytes";
		if(fileSize>=1024){
			unit = "KB";
			fileSize /= 1024;
			if(fileSize>=1024){
				unit = "MB";
				fileSize /= 1024;
				if(fileSize>=1024){
					unit = "GB";
					fileSize /= 1024;
				}
			}
		}
		return String.format( "%.2f", fileSize) + " " + unit;
	}
	
	static def String getStringValueWithDefault(String value, String defaultVal){
		//return (value == null)? stringToUnicode(defaultVal) : value.encodeAsHTML();
		return (value == null)? stringToUnicode(defaultVal) : stringToUnicode(value);
	}
	
	static def String getStringValue(String val){
		return Utils.getStringValueWithDefault(val, "");
	}
	
	static String stringToUnicode(String original) {
		//return new String(original.getBytes(UTF_8), UTF_8);
		
		StringBuffer ostr = new StringBuffer();
		for(int i=0; i<original.length(); i++)
		{
			char ch = original.charAt(i);
			if ((ch >= 0x0020) && (ch <= 0x007e))	// Does the char need to be converted to unicode?
			{
				ostr.append(ch);					// No.
			} else 									// Yes.
			{
				if(ch instanceof java.lang.Character){
					ostr.append(unicodeEscaped2(ch));
				} else {
					ostr.append(unicodeEscaped(ch));
				}
				
			}
		}
		return (new String(ostr));
	}
	
	/**
	 * Converts the string to the unicode format '\u0020'.
	 *
	 * This format is the Java source code format.
	 *
	 * <pre>
	 *   CharUtils.unicodeEscaped(' ') = "\u0020"
	 *   CharUtils.unicodeEscaped('A') = "\u0041"
	 * </pre>
	 *
	 * @param ch  the character to convert
	 * @return the escaped unicode string
	 */
	 static String unicodeEscaped(char ch) {
		 int code = (int) ch;
		 //println "Get unicode value of: " + ch + " -> Code: " + code + " -> " + "\\u" + Integer.toHexString(code | 0x10000).substring(1).toUpperCase();
		 return "\\u" + Integer.toHexString(code | 0x10000).substring(1).toUpperCase();
	}
	 
	 /**
	  * Converts the string to the unicode format '\u0020'.
	  *
	  * This format is the Java source code format.
	  *
	  * If <code>null</code> is passed in, <code>null</code> will be returned.
	  *
	  * <pre>
	  *   CharUtils.unicodeEscaped(null) = null
	  *   CharUtils.unicodeEscaped(' ')  = "\u0020"
	  *   CharUtils.unicodeEscaped('A')  = "\u0041"
	  * </pre>
	  *
	  * @param ch  the character to convert, may be null
	  * @return the escaped unicode string, null if null input
	  */
	 public static String unicodeEscaped2(Character ch) {
		 if (ch == null) {
			 return null;
		 }
		 return unicodeEscaped(ch);
	 }
	
	/**
	 * This method receive a String with a date and validates
	 * that the date is valid and returns the Date object
	 * 
	 * @param maybeDate
	 * @param format
	 * @param lenient
	 * @return				Date or null when invalid
	 */
	static Date parseDate(String maybeDate, String format, boolean lenient) {
		Date date = null;
	
		// test date string matches format structure using regex
		// - weed out illegal characters and enforce 4-digit year
		// - create the regex based on the local format string
		String reFormat = Pattern.compile("d+|M+").matcher(Matcher.quoteReplacement(format)).replaceAll("\\\\d{1,2}");
		reFormat = Pattern.compile("y+").matcher(reFormat).replaceAll("\\\\d{4}");
		if ( Pattern.compile(reFormat).matcher(maybeDate).matches() ) {
	
		  // date string matches format structure,
		  // - now test it can be converted to a valid date
		  SimpleDateFormat sdf = (SimpleDateFormat)DateFormat.getDateInstance();
		  sdf.applyPattern(format);
		  sdf.setLenient(lenient);
		  try { date = sdf.parse(maybeDate); } catch (ParseException e) { }
		}
		return date;
	  }
	
	/**
	 * This method receive a String with a date and validates
	 * that the date is valid and returns the Date object
	 * 
	 * @param maybeDate
	 * @param format
	 * @param lenient
	 * @return
	 */
	static Date isLegalDate(String maybeDate, String format, boolean lenient) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		sdf.setLenient(lenient);
		return sdf.parse(maybeDate, new ParsePosition(0));
	}
	
	/**
	 * This method will check if a string is empty
	 * 
	 * @param str	String to be checked
	 * @return		True when empty or null; False when not
	 */
	static isEmptyString(String str){
		return (str == null || str.trim() == "");
	}
    
    /**
     * This method will check if a string is not empty
     *
     * @param str   String to be checked
     * @return      True when not empty or null; False when it is
     */
    static isNotEmptyString(String str){
        return (str != null && str.trim().length() > 0);
    }
}
