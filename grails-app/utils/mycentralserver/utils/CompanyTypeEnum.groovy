package mycentralserver.utils;

enum CompanyTypeEnum {
	
	OWNER("Owner"), 
	INTEGRATOR ("Integrator")
	 
	final String value;
    
	CompanyTypeEnum(String value) {
        this.value = value
    }

	String toString() {value }
	 
    String value() { return value }
    String getKey() { name() }
}
