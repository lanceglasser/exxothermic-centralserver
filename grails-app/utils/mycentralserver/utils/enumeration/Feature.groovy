package mycentralserver.utils.enumeration;

/**
 * Enum with the list of Features associated with the Feature domain to be use for different validations
 * 
 * @author sdiaz
 * @since 09/24/2015
 */
enum Feature {

    /*
     * Second implementation of the Usage Report Data for the Venue Servers
     */
    SECOND_USAGE_DATE("SECOND_USAGE_DATA"),
    /**
     * Can the box be the PA server
     */
    BOX_PA("BOX_PA"),
    /**
     * Can the box handles a delay on every channel
     */
    CHANNELS_DELAY("CHANNELS_DELAY");

    final String code;

    Feature(final String code) {
        this.code = code;
    }

    public String toString() {
        return code;
    }

    
    /**
     * Will return the Enumeration related to the received code
     *
     * @param code
     *            Code to found on the enumeration list
     * @return Related Enumeration or null if not founded
     */
    public static Feature codeOf(final String code) {
        for (Feature answer : Feature.values()) {
            if (code.equals(answer.toString())) {
                return answer;
            }
        }
        // The code was not found, will return null
        return null;
    }
}