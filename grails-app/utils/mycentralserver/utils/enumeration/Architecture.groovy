package mycentralserver.utils.enumeration;

/**
 * Enum with the list of allowed Architectures of the ExXtractors
 * 
 * @author ExXothermic Development Team - Cecropia Solutions
 * @since 2018-05-23
 */
enum Architecture {

    LEGACY32("LEG32", 1),
    X32("X32", 2),           // Shuttle based x86 ExXothermic units
    X64("X64", 3),           // Shuttle based x86_64 ExXothermic units
    ODROID("ODROID", 4),     // Odroid C1+ based ExXothermic units
    LW300X("LW300X", 5),     // ASUS based Listen Tech WiFi units
    LW200P("LW200P", 6);     // Odroid XU4 based Listen Tech WiFi units

    final String code;
    final int value;

    Architecture(final String code, final int value) {
        this.value = value;
        this.code = code;
    }

    public int getValue() {
        return value;
    }

    public String toString() {
        return code;
    }

    /**
     * Returns the default architecture of the System that it's been use when the architecture is not
     * received.
     * 
     * @return Default architecture of the System
     */
    public static Architecture getDefault() {
        return X32;
    }
    
    /**
     * Will return the Enumeration related to the received code
     *
     * @param code
     *            Code to found on the enumeration list
     * @return Related Enumeration or null if not founded
     */
    public static Architecture codeOf(String code) {
        if (code) {
            code = code.trim().toUpperCase();
            for (Architecture answer : Architecture.values()) {
                if (code.equals(answer.getCode().toUpperCase())) {
                    return answer;
                }
            }
        }
        // The code was not found, will return null
        return null;
    }

    /**
     * Will return the Enumeration related to the received value
     *
     * @param value
     *            Value to find on the enumeration list
     * @return Related Enumeration or null if not exists
     */
    public static Architecture valueOf(final int value) {
        for (Architecture answer : Architecture.values()) {
            if (value == answer.getValue()) {
                return answer;
            }
        }
        // The value was not found, will return null
        return null;
    }

    /**
     * Returns the list of the names on the enumeration as a String separated by ','
     *
     * @return List of Names as String
     */
    public static String printNamesList() {
        StringBuilder strBuilder = new StringBuilder(256);
        for (Architecture answer : Architecture.values()) {
            strBuilder.append(answer.toString() + ", ");
        }
        strBuilder.toString().substring(0, strBuilder.toString().size()-2);
    }
}
