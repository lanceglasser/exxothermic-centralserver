package mycentralserver.utils.enumeration;

/**
 * Enumeration with the list of configuration codes of the system from configuration database table; this is required 
 * for some validations during the executing of specific process
 * 
 * @author sdiaz
 * @since 02/04/2016
 */
enum SystemConfiguration {

    REGISTRATION_MAIL("REGISTRATION_MAIL");

    final String code;

    SystemConfiguration (final String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public String toString() {
        return code;
    }

    /**
     * Will return the Enumeration related to the received code
     *
     * @param code
     *            Code to find on the enumeration list
     * @return Related Enumeration or null if not exists
     */
    public static SystemConfiguration findByCode(String code) {
        if (code) {
            code = code.trim().toUpperCase();
            for (SystemConfiguration answer : SystemConfiguration.values()) {
                if (code.equals(answer.getCode().toUpperCase())) {
                    return answer;
                }
            }
        }
        // The code was null or not found, will return null
        return null;
    }
}