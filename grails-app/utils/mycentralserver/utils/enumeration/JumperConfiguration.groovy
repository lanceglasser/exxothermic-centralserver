package mycentralserver.utils.enumeration;

/**
 * Enumeration with the list of allowed Jumper Configurations of the ExXtractors with the new audio cards
 * 
 * @author sdiaz
 * @since 12/29/2015
 */
enum JumperConfiguration {

    NONE("None", new Integer(0)), // Will be use for all the audio cards of first generation
    SUM_TO_MONO("Sum2Mono", new Integer(1)),
    DIFF_MONO("DiffMono", new Integer(2)),
    MONO("Mono", new Integer(3));

    final String name;
    final Integer value;

    JumperConfiguration(final String name, final Integer value) {
        this.value = value;
        this.name = name;
    }

    public int getValue() {
        return value.value;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return name;
    }

    /**
     * Returns the default audio card generation of the systems that it's been use when the value is not received.
     * 
     * @return Default Audio Card of the Systems
     */
    public static JumperConfiguration getDefault() {
        return SUM_TO_MONO;
    }

    /**
     * Will return the Enumeration related to the received value
     *
     * @param value
     *            Value to find on the enumeration list
     * @return Related Enumeration or null if not exists
     */
    public static JumperConfiguration findByValue(final Integer value) {
        if (value != null) {
            for (JumperConfiguration answer : JumperConfiguration.values()) {
                if (value.equals(answer.getValue())) {
                    return answer;
                }
            }
        }
        // The value was null or not found, will return null
        return null;
    }

    /**
     * Will return the Enumeration related to the received name
     *
     * @param name
     *            Name to find on the enumeration list
     * @return Related Enumeration or null if not exists
     */
    public static JumperConfiguration findByName(String name) {
        if (name) {
            name = name.trim().toUpperCase();
            for (JumperConfiguration answer : JumperConfiguration.values()) {
                if (name.equals(answer.getName().toUpperCase())) {
                    return answer;
                }
            }
        }
        // The name was null or not found, will return null
        return null;
    }

    /**
     * Returns the default JumperConfiguration depending of the AudioCardGeneration
     * 
     * @param audioCardGeneration
     *              Audio Card Generation to retrieve the default jumper configuration
     * @return Default Jumper Configuration or null if the audio card generation is null
     */
    public static JumperConfiguration getDefaultByAudioGeneration(AudioCardGeneration audioCardGeneration) {
        if (audioCardGeneration == null) {
            return null;
        }
        // The default change depending of the audio card generation type; for any other than Second returns NONE
        return audioCardGeneration.equals(AudioCardGeneration.SECOND)?
                JumperConfiguration.getDefault():JumperConfiguration.NONE;
    }

    /**
     * Returns the list of the names on the enumeration as a String separated by ','
     *
     * @return List of values as String
     */
    public static String printNamesList() {
        StringBuilder strBuilder = new StringBuilder(256);
        for (JumperConfiguration answer : JumperConfiguration.values()) {
            strBuilder.append(answer.getName() + ", ");
        }
        strBuilder.toString().substring(0, strBuilder.toString().size()-2);
    }
}