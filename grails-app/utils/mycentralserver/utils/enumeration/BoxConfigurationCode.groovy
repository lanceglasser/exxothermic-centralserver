package mycentralserver.utils.enumeration;

/**
 * Enumeration with the list of box configurations codes from the box_config database table; this is required for some
 * validations during the executing of specific process like the change of the audio input mode
 * 
 * @author sdiaz
 * @since 12/31/2015
 */
enum BoxConfigurationCode {

    DEBUG_LEVEL("DEBUG_LEVEL"), AUDIO_INPUT_MODE("AUDIO_INPUT_MODE");

    final String code;

    BoxConfigurationCode(final String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public String toString() {
        return code;
    }

    /**
     * Will return the Enumeration related to the received code
     *
     * @param code
     *            Code to find on the enumeration list
     * @return Related Enumeration or null if not exists
     */
    public static BoxConfigurationCode findByCode(String code) {
        if (code) {
            code = code.trim().toUpperCase();
            for (BoxConfigurationCode answer : BoxConfigurationCode.values()) {
                if (code.equals(answer.getCode().toUpperCase())) {
                    return answer;
                }
            }
        }
        // The code was null or not found, will return null
        return null;
    }
}