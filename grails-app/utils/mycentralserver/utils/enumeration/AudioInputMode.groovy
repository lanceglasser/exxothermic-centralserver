package mycentralserver.utils.enumeration;

/**
 * Enum with the list of allowed AudioInputModes of the ExXtractors; must map with the list of options for the
 * AUDIO_INPUT_MODE_CONFIG
 * 
 * @author sdiaz
 * @since 01/27/2016
 */
enum AudioInputMode {

    STEREO("Stereo"),
    MONAURAL("Monaural");

    final String code;

    AudioInputMode(final String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public String toString() {
        return code;
    }

    /**
     * Will return the Enumeration related to the received code
     *
     * @param code
     *            Code to found on the enumeration list
     * @return Related Enumeration or null if not founded
     */
    public static AudioInputMode codeOf(String code) {
        if (code) {
            code = code.trim().toUpperCase();
            for (AudioInputMode answer : AudioInputMode.values()) {
                if (code.equals(answer.getCode().toUpperCase())) {
                    return answer;
                }
            }
        }
        // The code was not found, will return null
        return null;
    }

    /**
     * Returns the list of the code on the enumeration as a String separated by ','
     *
     * @return List of Codes as String
     */
    public static String printNamesList() {
        StringBuilder strBuilder = new StringBuilder(256);
        for (AudioInputMode answer : AudioInputMode.values()) {
            strBuilder.append(answer.toString() + ", ");
        }
        strBuilder.toString().substring(0, strBuilder.toString().size()-2);
    }

    /**
     * Returns the default Audio Input Mode depending of the received architecture
     * 
     * @param arch
     *          Architecture to review
     * @return The default input mode of the architecture
     */
    public static AudioInputMode getDefaultByArchitecture(Architecture architecture) {
        if (architecture == null) {
            return null;
        }
        if (architecture.equals(Architecture.ODROID) ) {
            // For Odroid must be Monaural
            return AudioInputMode.MONAURAL;
        } else {
            // For others must be Stereo
            return AudioInputMode.STEREO;
        }
    }
}