package mycentralserver.utils.enumeration;

/**
 * Enumeration with the list of Actions for the requests to the ExXtractors
 * 
 * @author sdiaz
 * @since 10/06/2015
 */
public enum BoxRequestActionsEnum {

    UNREGISTER("unregister");

    final String action;

    BoxRequestActionsEnum(final String action) {
        this.action = action;
    }

    public String getAction() {
        return action;
    }

    public String toString() {
        return action;
    }
}