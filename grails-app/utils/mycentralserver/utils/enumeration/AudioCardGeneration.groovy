package mycentralserver.utils.enumeration;

/**
 * Enumeration with the list of allowed Audio Card Generations of the ExXtractors
 * 
 * @author sdiaz
 * @since 12/29/2015
 */
enum AudioCardGeneration {

    FIRST("First", new Integer(1)),
    SECOND("Second", new Integer(2));

    final String name;
    final Integer value;

    AudioCardGeneration(final String name, final Integer value) {
        this.value = value;
        this.name = name;
    }

    public int getValue() {
        return value.value;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return name;
    }

    /**
     * Returns the default audio card generation of the systems that it's been use when the value is not received.
     * 
     * @return Default Audio Card of the Systems
     */
    public static AudioCardGeneration getDefault() {
        return SECOND;
    }

    /**
     * Will return the Enumeration related to the received value
     *
     * @param value
     *            Value to find on the enumeration list
     * @return Related Enumeration or null if not exists
     */
    public static AudioCardGeneration findByValue(final Integer value) {
        if (value) {
            for (AudioCardGeneration answer : AudioCardGeneration.values()) {
                if (value.equals(answer.getValue())) {
                    return answer;
                }
            }
        }
        // The value was null or not found, will return null
        return null;
    }
    
    /**
     * Will return the Enumeration related to the received value as a String
     *
     * @param value
     *            Value as String to find on the enumeration list
     * @return Related Enumeration or null if not exists
     */
    public static AudioCardGeneration findByStringValue(final String value) {
        if (value) {
            try {
                final Integer valueAsInteger = new Integer(Integer.parseInt(value));
                for (AudioCardGeneration answer : AudioCardGeneration.values()) {
                    if (valueAsInteger.equals(answer.getValue())) {
                        return answer;
                    }
                }
            } catch ( NumberFormatException e) { // If the input is invalid then will return null
            }
        }
        // The value was null or not found, will return null
        return null;
    }

    /**
     * Returns the list of the values on the enumeration as a String separated by ','
     * 
     * @return List of values as String
     */
    public static String printValuesList() {
        StringBuilder strBuilder = new StringBuilder(256);
        for (AudioCardGeneration answer : AudioCardGeneration.values()) {
            strBuilder.append(answer.getValue() + ", ");
        }
        strBuilder.toString().substring(0, strBuilder.toString().size()-2);
    }
}