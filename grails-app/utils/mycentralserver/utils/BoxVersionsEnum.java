package mycentralserver.utils;

/**
 * Enumeration with the list of some versions of the box code for validations of functionalities
 * 
 * @author sdiaz
 * @since 10/05/2015
 */
public enum BoxVersionsEnum {

    V2_38(40, "2.38");

    final double version;
    final String label;

    BoxVersionsEnum(final double version, final String label) {
        this.version = version;
        this.label = label;
    }

    public double getVersion() {
        return version;
    }

    public String getLabel() {
        return label;
    }

    public String toString() {
        return label;
    }
}