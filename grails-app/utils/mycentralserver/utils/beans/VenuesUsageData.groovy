package mycentralserver.utils.beans

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import mycentralserver.utils.DateUtils;

/**
 * Class to store and handle the usage information for multiple Venue Servers over multiple days with the use of a Map
 * 
 * @author sdiaz
 * @since 11/22/2015
 */
class VenuesUsageData {

    /**
     * Map with the Usage information of multiple Venues using a <LocationId, VenueData> structure
     */
    private Map<Integer, VenueData> venuesData;

    /**
     * Default constructor
     */
    public VenuesUsageData() {
        venuesData = new HashMap<Integer, VenueData>();
    }

    /**
     * Save the value of the streaming minutes over a day, if the value already exists will be overwrite
     *
     * @param venueId
     *          Id of the Venue to store the information
     * @param venueName
     *          Name of the Venue
     * @param day
     *          Day key to set the value
     * @param uniqueCounter
     *          Value of the number of unique clients for the venue on the day
     * @param streamingMinutes
     *          Number of minutes of streaming for the Venue over the day
     * @param simultaneousStreamingPeak
     *          Maximum number of concurrent streaming
     */
    public void setVenueDayData(final int venueId, final String venueName, final String day, final int uniqueCounter, 
            final double streamingMinutes, final int simultaneousStreamingPeak) {
        VenueData venueData = getDataForVenue(venueId, venueName);
        venueData.setDayValues(day, uniqueCounter, streamingMinutes, simultaneousStreamingPeak);
    }

    /**
     * Sets the streaming time and peak number for a venue on a day using an ID of venue as Long
     *
     * @param venueId
     *          Long value of the Id of the Venue to store the information
     * @param venueName
     *          Name of the Venue
     * @param day
     *          Day key to set the value
     * @param uniqueCounter
     *          Total number of unique clients on the venue for the day
     * @param mustOverwrite
     *          Tells if the data should be handle as overwrite or not
     */
    public void setVenueUniqueClientsData(final long venueId, final String venueName, final String day,
        final long uniqueCounter, final boolean mustOverwrite) {
        setVenueUniqueClientsData(venueId.intValue(), venueName, day, uniqueCounter.intValue(), mustOverwrite);
    }

    /**
     * Sets the streaming time and peak number for a venue on a day using an ID of venue as Integer
     *
     * @param venueId
     *          Integer value of the Id of the Venue to store the information
     * @param venueName
     *          Name of the Venue
     * @param day
     *          Day key to set the value
     * @param uniqueCounter
     *          Total number of unique clients on the venue for the day
     * @param mustOverwrite
     *          Tells if the data should be handle as overwrite or not
     */
    public void setVenueUniqueClientsData(final int venueId, final String venueName, final String day, 
            final int uniqueCounter, final boolean mustOverwrite) {
        VenueData venueData = getDataForVenue(venueId, venueName);
        venueData.setDayUniqueCounter(day, uniqueCounter, mustOverwrite);
    }

    /**
     * Sets the streaming time and peak number for a venue on a day using an ID of venue as Long
     * 
     * @param venueId
     *          Long value of the Id of the Venue to store the information
     * @param venueName
     *          Name of the Venue
     * @param day
     *          Day key to set the value
     * @param streamingTimeInSeconds
     *          Total time in seconds of streaming
     * @param simultaneousStreamingPeak
     *          Maximum number of simultaneous streaming on a day
     * @param mustOverwrite
     *          Tells if the data should be handle as overwrite or not
     */
    public void setVenueStreamingAndPeakData(final long venueId, final String venueName, final String day,
        final double streamingTimeInSeconds, final int simultaneousStreamingPeak, final boolean mustOverwrite) {
    }

    /**
     * Sets the streaming time and peak number for a venue on a day using an ID of venue as Integer
     *
     * @param venueId
     *          Integer value of the Id of the Venue to store the information
     * @param venueName
     *          Name of the Venue
     * @param day
     *          Day key to set the value
     * @param streamingTimeInSeconds
     *          Total time in seconds of streaming
     * @param simultaneousStreamingPeak
     *          Maximum number of simultaneous streaming on a day
     * @param mustOverwrite
     *          Tells if the data should be handle as overwrite or not
     */
    public void setVenueStreamingAndPeakData(final int venueId, final String venueName, final String day,
            final double streamingTimeInSeconds, final int simultaneousStreamingPeak, final boolean mustOverwrite) {
        VenueData venueData = getDataForVenue(venueId, venueName);
        venueData.setDayStreamingTimeInSeconds(day, streamingTimeInSeconds, mustOverwrite);
        venueData.setDaySimultaneousStreamingPeak(day, simultaneousStreamingPeak, mustOverwrite);
    }

    /**
     * Returns all the usage information stored on the object
     * 
     * @param startDate
     *              Start date object of the filtered data
     * @param endDate
     *              End date object of the filtered data
     * @return Json format of all usage data required for the charts
     */
    public String getCompleteJsonDataForCharts(final Date startDate, final Date endDate) {
        final StringBuilder allDataBuilder = new StringBuilder(1024);
        if (venuesData.size() > 0) {
            // Initialize the string builders for all the charts data objects
            final StringBuilder venueHeadersBuiler = new StringBuilder(1024);
            final StringBuilder streamingTimeBuiler = new StringBuilder(1024);
            final StringBuilder uniquesCounterBuiler = new StringBuilder(1024);
            final StringBuilder peaksValueBuiler = new StringBuilder(1024);
            venueHeadersBuiler.append('[""'); //This is only required for the chart data format
            boolean headersAreInitialized = false;
            int totalStreamingTimeInSeconds = 0;
            final DecimalFormat decimalFormatter = new DecimalFormat("######");
            // Calculate the number of days between the start and end dates
            final long diffDays = (endDate.getTime() - startDate.getTime()) / (24 * 60 * 60 * 1000);
            Calendar c = Calendar.getInstance();
            String currentSeparator = "";
            for (int i=0; i <= diffDays; i++) { // For each day between start and end date will put the data
                c.setTime(startDate); // Move from start date
                c.add(Calendar.DATE, i); // Adding i days
                final String day = DateUtils.getChartsFormattedDate(c.getTime()); // Get format MM/dd
                // Put the Day Header on all the data, required for different charts
                uniquesCounterBuiler.append(currentSeparator + '["' + day + '"');
                streamingTimeBuiler.append(currentSeparator + '["' + day + '"');
                peaksValueBuiler.append(currentSeparator + '["' + day + '"');

                // Try to find the day data for each Venue in the list
                for (Map.Entry<Integer, VenueData> entry : venuesData.entrySet()) {
                    final VenueData venueData = entry.getValue();
                    if(!headersAreInitialized) { // The first time must save the names of the Venues
                        venueHeadersBuiler.append(',"' + venueData.getVenueName() + '"');
                    }
                    // Find the day data for the venue
                    final VenueDailyData venueDailyData = venueData.getVenueData().get(day);
                    if (venueDailyData) { // If there is data will put the values for the day
                        totalStreamingTimeInSeconds += venueDailyData.getStreamingTimeInSeconds();
                        uniquesCounterBuiler.append(',' + venueDailyData.getUniqueCounter());
                        final int timeInMinutes = Integer.parseInt(""+
                            decimalFormatter.format(venueDailyData.getStreamingTimeInSeconds()/60));
                        streamingTimeBuiler.append(',' + timeInMinutes);
                        peaksValueBuiler.append(',' + venueDailyData.getSimultaneousStreamingPeak());
                    } else { // If there is not data for the day will put 0 for all the values
                        uniquesCounterBuiler.append(',0');
                        streamingTimeBuiler.append(',0');
                        peaksValueBuiler.append(',0');
                    }
                }
                currentSeparator = ","; // After the first day must put , 
                headersAreInitialized = true;
                // Close the data for the day
                uniquesCounterBuiler.append("]");
                streamingTimeBuiler.append("]");
                peaksValueBuiler.append("]");
            }
            venueHeadersBuiler.append(']');
            // Convert the streaming time in minutes to the format HH:MM:SS
            final String hms = String.format("%02d:%02d:%02d",
                TimeUnit.SECONDS.toHours(totalStreamingTimeInSeconds),
                TimeUnit.SECONDS.toMinutes(totalStreamingTimeInSeconds) -
                TimeUnit.HOURS.toMinutes(TimeUnit.SECONDS.toHours(totalStreamingTimeInSeconds)),
                totalStreamingTimeInSeconds -
                TimeUnit.MINUTES.toSeconds(TimeUnit.SECONDS.toMinutes(totalStreamingTimeInSeconds)));

            allDataBuilder.append('{"error":false');
            allDataBuilder.append(',"locationsCount":' + venuesData.size());
            allDataBuilder.append(',"totalTime":"' + hms + ' HH:MM:SS"');
            allDataBuilder.append(',"chartData":[' + venueHeadersBuiler.toString() + ',' + streamingTimeBuiler.toString() + ']');
            allDataBuilder.append(',"dataDaily":[' + venueHeadersBuiler.toString() + ',' + uniquesCounterBuiler.toString() + ']');
            allDataBuilder.append(',"sessionsMaxData":[' + venueHeadersBuiler.toString() + ',' + peaksValueBuiler.toString() + ']');
            allDataBuilder.append('}');
        } else { // If there are not venues data for the range of dates required
            allDataBuilder.append('{"error":false,"locationsCount":0}');
        }
        return allDataBuilder.toString();
    }

    /**
     * Returns the VenueData object for the location id received; if the object don't exists will create a new one and
     * put it to the map
     *
     * @param venueId
     *          Key of the Venue to retrieve the data object
     * @param venueName
     *          Name of the Venue, if the object already exists the value will be overwrited
     * @return Related daily data object for the received day key
     */
    private VenueData getDataForVenue(final int venueId, final String venueName) {
        VenueData venueData = venuesData.get(venueId);
        if (venueData == null) {
            venueData =  new VenueData();
            venuesData.put(venueId, venueData);
        }
        venueData.setVenueName(venueName);
        return venueData;
    }
}