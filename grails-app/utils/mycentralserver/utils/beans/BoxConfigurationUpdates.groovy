package mycentralserver.utils.beans

import mycentralserver.utils.enumeration.BoxConfigurationCode;

/**
 * Object uses ton control the process that saves the configurations of a Box, will include a method to tell if at least
 * one of the configurations changed
 * 
 * @author sdiaz
 * @since 12/30/2015
 */
class BoxConfigurationUpdates {

    /**
     * Contains the list of the updated configurations to be use for validations
     */
    private Map<String, String> updatedConfigurations = new HashMap<String, String>();

    /**
     * Tells if at least one of the configurations changed
     * 
     * @return True if the list contains at least 1 element
     */
    public boolean hasChanged() {
        return updatedConfigurations.size() > 0;
    }

    /**
     * Checks if a configuration has changed by using the code
     *
     * @param configurationCode
     *                  BoxConfigurationCode from the Enumeration of the configuration that want to check
     * @return True if the code is on the list of updated configurations
     */
    public boolean configurationHasChanged(final BoxConfigurationCode configurationCode) {
        return configurationHasChanged(configurationCode.getCode());
    }

    /**
     * Checks if a configuration has changed by using the code
     * 
     * @param configurationCode
     *                  Code of the configuration that want to check
     * @return True if the code is on the list of updated configurations
     */
    public boolean configurationHasChanged(final String configurationCode) {
        return updatedConfigurations.containsKey(configurationCode);
    }

    /**
     * Add the configuration code to the list of updated configurations
     * 
     * @param updatedConfigurationCode
     *              Code of the updated configuration
     */
    public void addUpdatedConfiguration(final String updatedConfigurationCode, final String value) {
        updatedConfigurations.put(updatedConfigurationCode, value);
    }

    /**
     * Returns the list of updated configurations
     * 
     * @return Updated configurations list
     */
    public Map<String, String> getUpdatedConfigurations() {
        return updatedConfigurations;
    }

    /**
     * Returns the updated value of the configuration by the code
     * 
     * @param code
     *          Configuration code to get the new value
     * @return Updated value of the configuration or null if the configuration was not updated
     */
    public String getUpdatedConfigurationValue(final String code) {
        return updatedConfigurations.get(code);
    }

    @Override
    public String toString() {
        return '{"hasChanged": ' + hasChanged() + ', "updatedConfigurations": ' + updatedConfigurations + "}";
    }
}