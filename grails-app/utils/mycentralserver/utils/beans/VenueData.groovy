package mycentralserver.utils.beans

/**
 * Class to store and handle the usage information for a Venue over multiple days with the use of a Map
 * 
 * @author sdiaz
 * @since 11/22/2015
 */
class VenueData {

    /**
     * Map with the Usage information using a <DayString, VenueDailyData> structure; the day key should be on the format
     * MM/dd only without the year to be use on the charts 
     */
    private Map<String, VenueDailyData> dataPerDays;

    private String venueName;

    public VenueData(final String venueName) {
        dataPerDays = new HashMap<String, VenueDailyData>();
        this.venueName = venueName; 
    }

    public void setVenueName(String venueName) {
        this.venueName = venueName;
    }

    public String getVenueName() {
        return venueName;
    }

    /**
     * Returns the Map with the Venue Information
     * 
     * @return Venue Information using a Map<DayString, VenueDailyData> structure
     */
    public Map<String, VenueDailyData> getVenueData() {
        return dataPerDays;
    }

    /**
     * Save the value of the streaming minutes over a day, if the value already exists will be overwrite
     *
     * @param day
     *          Day key to set the value
     * @param uniqueCounter
     *          Value of the number of unique clients for the venue on the day
     * @param streamingMinutes
     *          Number of minutes of streaming for the Venue over the day
     * @param simultaneousStreamingPeak
     *          Maximum number of concurrent streaming
     */
    public void setDayValues(final String day, final int uniqueCounter, final double streamingMinutes, 
        final int simultaneousStreamingPeak) {
        VenueDailyData venueDailyData = getDailyDataForDay(day);
        venueDailyData.setUniqueCounter(uniqueCounter);
        venueDailyData.setStreamingTimeInSeconds(streamingMinutes);
        venueDailyData.setSimultaneousStreamingPeak(simultaneousStreamingPeak);
    }
    
    /**
     * Save the value of the streaming minutes over a day, if the value already exists will be overwrite
     *
     * @param day
     *          Day key to set the value
     * @param streamingMinutes
     *          Number of minutes of streaming for a Venue
     */
    public void setDayStreamingMinutes(final String day, final double streamingMinutes) {
        setDayStreamingTimeInSeconds(day, streamingMinutes, true)
    }

    /**
     * Save the value of the streaming minutes over a day, the value will be overwrite or added depending of the
     * parameter 
     *
     * @param day
     *          Day key to set the value
     * @param streamingTimeInSeconds
     *          Number of seconds of streaming for a Venue
     * @param mustOverwrite
     *          Sets if the value must be overwrite or added
     */
    public void setDayStreamingTimeInSeconds(final String day, final double streamingTimeInSeconds,
            final boolean mustOverwrite) {
        VenueDailyData venueDailyData = getDailyDataForDay(day);
        venueDailyData.setStreamingTimeInSeconds(
            mustOverwrite? streamingTimeInSeconds:venueDailyData.getStreamingTimeInSeconds()+streamingTimeInSeconds);
    }

    /**
     * Save the value of the maximum number of concurrent streaming over a day, if the value already exists will be
     * overwrite
     *
     * @param day
     *          Day key to set the value
     * @param simultaneousStreamingPeak
     *          Maximum number of concurrent streaming
     */
    public void setDaySimultaneousStreamingPeak(final String day, final int simultaneousStreamingPeak) {
        setDaySimultaneousStreamingPeak(day, simultaneousStreamingPeak, true);
    }

    /**
     * Save the value of the maximum number of concurrent streaming over a day, the value will be overwrite or added 
     * depending of the parameter 
     *
     * @param day
     *          Day key to set the value
     * @param simultaneousStreamingPeak
     *          Maximum number of concurrent streaming
     * @param mustOverwrite
     *          Sets if the value must be overwrite or added
     *          
     */
    public void setDaySimultaneousStreamingPeak(final String day, final int simultaneousStreamingPeak, 
            final boolean mustOverwrite) {
        VenueDailyData venueDailyData = getDailyDataForDay(day);
        venueDailyData.setSimultaneousStreamingPeak(
            mustOverwrite? simultaneousStreamingPeak:venueDailyData.getSimultaneousStreamingPeak()+simultaneousStreamingPeak
            );
    }

    /**
     * Save the value of the counter of unique clients over a day, the value will be overwrite
     * 
     * @param day
     *          Day key to set the value
     * @param uniqueCounter
     *          Value of the number of unique clients for the venue on the day
     */
    public void setDayUniqueCounter(final String day, final int uniqueCounter) {
        setDayUniqueCounter(day, uniqueCounter, true);
    }

    /**
     * Save the value of the counter of unique clients over a day, the value will be overwrite or added depending of the
     * parameter
     *
     * @param day
     *          Day key to set the value
     * @param uniqueCounter
     *          Value of the number of unique clients for the venue on the day
     * @param mustOverwrite
     *          Sets if the value must be overwrite or added
     */
    public void setDayUniqueCounter(final String day, final int uniqueCounter, final boolean mustOverwrite) {
        VenueDailyData venueDailyData = getDailyDataForDay(day);
        venueDailyData.setUniqueCounter(mustOverwrite? uniqueCounter:venueDailyData.getUniqueCounter()+uniqueCounter);
    }

    /**
     * Returns the VenueDailyData object for the day key received; if the object don't exists will create a new one and
     * put it to the map
     * 
     * @param day
     *          Key of the day to retrieve the daily data object
     * @return Related daily data object for the received day key
     */
    private VenueDailyData getDailyDataForDay(final String day) {
        VenueDailyData venueDailyData = dataPerDays.get(day);
        if (venueDailyData == null) {
            venueDailyData =  new VenueDailyData();
            dataPerDays.put(day, venueDailyData);
        }
        return venueDailyData;
    }

    @Override
    public String toString() {
        StringBuilder strBuilder = new StringBuilder(1024);
        for (Map.Entry<String, VenueDailyData> entry : dataPerDays.entrySet()) {
            strBuilder.append("   Day: " + entry.getKey() + "/" + entry.getValue() + "/n");
        }
        return strBuilder.toString();
    }
}