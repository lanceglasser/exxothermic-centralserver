package mycentralserver.utils.beans

/**
 * Object that maps the daily usage information of a Venue
 * 
 * @author sdiaz
 * @since 11/23/2015
 */
class VenueDailyData {

    /**
     * Number of Unique Clients on a day for the Venue
     */
    private int uniqueCounter = 0;

    /**
     * Number of minutes of streaming on a day for the Venue 
     */
    private double streamingTimeInSeconds = 0;

    /**
     * Maximum number of simultaneous streaming on the Venue
     */
    private int simultaneousStreamingPeak = 0;

    /**
     * Constructor with default values
     */
    public VenueDailyData() {
    }

    /**
     * Constructor with all the parameters 
     * 
     * @param uniqueCounter
     *              Number of unique clients connected to the Venue on a Day
     * @param streamingTimeInSeconds
     *              Number of minutes of streaming for the Venue
     * @param simultaneousStreamingPeak
     *              Maximum number of simultaneous streaming on the Venue
     */
    public VenueDailyData(final int uniqueCounter, final double streamingTimeInSeconds, final int simultaneousStreamingPeak) {
        this.uniqueCounter = uniqueCounter;
        this.streamingTimeInSeconds = streamingTimeInSeconds;
        this.simultaneousStreamingPeak = simultaneousStreamingPeak;
    }

    /**
     * Constructor without the peak number of simultaneous streamings
     *
     * @param uniqueCounter
     *              Number of unique clients connected to the Venue on a Day
     * @param streamingMinutes
     *              Number of minutes of streaming for the Venue
     */
    public VenueDailyData(final int uniqueCounter, final double streamingMinutes) {
        this.uniqueCounter = uniqueCounter;
        this.streamingTimeInSeconds = streamingMinutes;
    }

    public VenueDailyData setUniqueCounter(int uniqueCounter) {
        this.uniqueCounter = uniqueCounter;
        return this;
    }

    public int getUniqueCounter() {
        return uniqueCounter;
    }

    public VenueDailyData setStreamingTimeInSeconds(double streamingTimeInSeconds) {
        this.streamingTimeInSeconds = streamingTimeInSeconds;
        return this;
    }

    public double getStreamingTimeInSeconds() {
        return streamingTimeInSeconds;
    }

    public VenueDailyData setSimultaneousStreamingPeak(int simultaneousStreamingPeak) {
        this.simultaneousStreamingPeak = simultaneousStreamingPeak;
        return this;
    }

    public int getSimultaneousStreamingPeak() {
        return simultaneousStreamingPeak;
    }

    @Override
    public String toString() {
        return '{"uniqueCounter": ' + uniqueCounter + ', "streamingTimeInSeconds": ' + streamingTimeInSeconds + 
            ', "simultaneousStreamingPeak": ' + simultaneousStreamingPeak + "}";
    }
}