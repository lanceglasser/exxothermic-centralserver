package mycentralserver.utils;

enum BoxStatusEnum {
	
	CONNECTED("Connected"),
	REGISTERED("Registered"),
	ERROR("Error"),	
	DISCONNECTED ("Disconnected"),
	UPDATING("Updating")
	 
	final String value;
    
	BoxStatusEnum(String value) {
        this.value = value
    }

	String toString() {value }
	 
    String value() { return value }
    String getKey() { name() }
}
