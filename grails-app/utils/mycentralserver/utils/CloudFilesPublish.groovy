package mycentralserver.utils


import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import org.jclouds.ContextBuilder;
import org.jclouds.io.Payload;
import org.jclouds.io.Payloads;
import org.jclouds.openstack.swift.v1.domain.ObjectList
import org.jclouds.openstack.swift.v1.domain.SwiftObject
import org.jclouds.openstack.swift.v1.features.ObjectApi;
import org.jclouds.openstack.swift.v1.options.CreateContainerOptions;
import org.jclouds.rackspace.cloudfiles.v1.CloudFilesApi;
import org.jclouds.rackspace.cloudfiles.v1.features.CDNApi;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.ByteSource;
import com.google.common.io.Closeables;
import com.google.common.io.Files;
import org.apache.commons.io.FileUtils
import org.jclouds.openstack.swift.v1.options.ListContainerOptions;
import org.apache.commons.io.FileUtils;


class CloudFilesPublish  implements Closeable {
	
//https://github.com/jclouds/jclouds-examples/blob/master/rackspace/src/main/java/org/jclouds/examples/rackspace/cloudfiles/CloudFilesPublish.java
	private final CloudFilesApi cloudFiles;
	private String fileFullPath;
	private String folderContainer;


	public CloudFilesPublish(String fileFullPath,  String username, String apiKey) {
		cloudFiles = ContextBuilder.newBuilder(Constants.PROVIDER)
			  .credentials(username, apiKey)
			  .buildApi(CloudFilesApi.class)
		this.fileFullPath = fileFullPath;
		def config = org.codehaus.groovy.grails.commons.ConfigurationHolder.config
		this.folderContainer = config.rackspaceImageContainer;
	 }


  public CloudFilesPublish(String fileFullPath,  String username, String apiKey, String folder) {
    cloudFiles = ContextBuilder.newBuilder(Constants.PROVIDER)
        .credentials(username, apiKey)
        .buildApi(CloudFilesApi.class)
    this.fileFullPath = fileFullPath;
    this.folderContainer = folder;
   }

	public CloudFilesPublish(String username, String apiKey){
		cloudFiles = ContextBuilder.newBuilder(Constants.PROVIDER).credentials(username, apiKey).buildApi(CloudFilesApi.class);
		def config = org.codehaus.groovy.grails.commons.ConfigurationHolder.config
		this.folderContainer = config.rackspaceImageContainer;
		this.fileFullPath = "";
	}

   def  String uploadFileToRackspace() {

	  String fileURL = null
      try {
	    createContainer()
		createObjectFromFile()
		fileURL = enableCdnContainer()
		
		//here, we have already upload the file. We can delete it, as the reference now is the returned URL
		FileUtils.deleteQuietly(new File(this.fileFullPath))

        /*if(fileURL != null){
          File tempFile = new File(this.fileFullPath)
          FileUtils.deleteQuietly(tempFile);
        }*/
      }
      catch (e) {
         e.printStackTrace()
		 throw e
      }
      finally {
         close()
      }

	  return fileURL
   }

   def  void deleteFileToRackspace(String previousFile) {
	  try {
			createContainer()
			deleteFile(previousFile)
	  }
	  catch (e) {
		 e.printStackTrace()
		 throw e
	  }
	  finally {
		 close()
	  }
   }


   /**
	* This method will create a container in Cloud Files where you can store and
	* retrieve any kind of digital asset.
	*/
   private def createContainer() {
     // System.out.format("Create Container%n");
      cloudFiles.containerApiInRegion(Constants.REGION).createIfAbsent(this.folderContainer, CreateContainerOptions.NONE);
     // System.out.format(" %s%n", Constants.CONTAINER_PUBLISH);
   }

   /**
	* This method will put a plain text object into the container.
	*/
   private def createObjectFromFile() throws IOException {
      //System.out.format("Create Object From File%n");

      File tempFile = new File (fileFullPath);
      tempFile.deleteOnExit();

      ObjectApi objectApi = cloudFiles.objectApiInRegionForContainer(Constants.REGION, this.folderContainer);

      ByteSource byteSource = Files.asByteSource(tempFile);
      Payload payload = Payloads.newByteSourcePayload(byteSource);

      objectApi.replace(tempFile.getName(), payload, ImmutableMap.<String, String>of());

      //println("File created");
   }

   /**
	* This method will put a plain text object into the container.
	* Base on: https://github.com/jclouds/jclouds-examples/blob/master/rackspace/src/main/java/org/jclouds/examples/rackspace/cloudfiles/DeleteObjectsAndContainer.java
	*/
   private def deleteFile(String fileName) throws IOException {
	  if(fileName != null){
		  ObjectApi objectApi = cloudFiles.objectApiInRegionForContainer(Constants.REGION, this.folderContainer);
		  // ObjectList objects = objectApi.list(ListContainerOptions.NONE);
		  // objectApi.ge
		   File tempFile = new File (fileName);
		  // println("Delete File:" + tempFile.getName());
		   SwiftObject object =  objectApi.get(tempFile.getName(), null)
	 
		   if(object != null){
			   objectApi.delete(object.getName());
		   }
	  }
   }

   /**
	* This method will put your container on a Content Distribution Network and
	* make it 100% publicly accessible over the Internet.
	*/
   private def String  enableCdnContainer() {
     // System.out.format("Enable CDN Container%n");

      CDNApi cdnApi = cloudFiles.cdnApiInRegion(Constants.REGION);
      URI cdnURI = cdnApi.enable(this.folderContainer);

	  File tempFile = new File (fileFullPath);
	  String fileURL = String.format("%s/%s", cdnURI, tempFile.getName());
     // System.out.format(" Go to %s/%s%n", cdnURI, tempFile.getName());

	  return fileURL;
   }

   /**
* Always close your service when you're done with it.
*/
   public void close() throws IOException {
      Closeables.close(cloudFiles, true);
   }
}