package mycentralserver.utils;

enum RoleEnum {

	OWNER("ROLE_OWNER"), 
	INTEGRATOR ("ROLE_INTEGRATOR"),
	ADMIN ("ROLE_ADMIN"),
	HELP_DESK ("ROLE_HELP_DESK"),
	OWNER_STAFF ("ROLE_OWNER_STAFF"),
	INTEGRATOR_STAFF ("ROLE_INTEGRATOR_STAFF")
		
	final String value;
    
	RoleEnum(String value) {
        this.value = value
    }

	String toString() {value }
	 
    String value() { return value }
    String getKey() { name() }
}
