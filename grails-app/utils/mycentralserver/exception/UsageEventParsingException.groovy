package mycentralserver.exception;

import groovy.transform.InheritConstructors;

/**
 * Custom extension class to be use for the parsing process of the usage report files; only extends from Exception
 * since we not require extra functionality for now
 * 
 * @author sdiaz
 * @since 12/01/2015
 */
@InheritConstructors
class UsageEventParsingException extends Exception {

}