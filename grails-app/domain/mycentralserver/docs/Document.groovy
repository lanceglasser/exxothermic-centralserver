package mycentralserver.docs

import java.io.Serializable;
import java.util.Date;
import org.hibernate.criterion.CriteriaSpecification;
import mycentralserver.user.User;
import mycentralserver.docs.DocCategory;
import mycentralserver.company.Company
import mycentralserver.company.CompanyLocation

class Document implements Serializable {
	String name
	String url
	String filename
	DocCategory type
	Date expirationDate
	User createdBy
	Date dateCreated
	Date lastUpdated

	static hasMany= [locations:CompanyLocation]

	static constraints = {
		name 			blank:false,minSize:1, maxSize:255
		url 			blank:false,minSize:1, maxSize:255
		filename 		blank:false,minSize:1, maxSize:255
		type 			nullable:false
		expirationDate  nullable:true
		createdBy		nullable:true
	}

	static mapping = { autoTimestamp true }
	
	static namedQueries = {
		getByCompanies { companies , user ->
			createAlias("locations", "l", CriteriaSpecification.LEFT_JOIN)
							
			if(companies.size() > 0){
				or {
					and{
						'in'("l.company.id", companies)
					}
					eq("createdBy", user)
				}
			} else {
				eq("createdBy", user)
			}		   
			
			order("name", "asc")
			
		}
		listAllDocumentsForUITable { start, length, columnName, orderDir, searchText, onlyEnable, user, 
			isHelpDesk, isAdmin, affiliate, companies, locationId, getCount ->
			
			projections {
				if(getCount){	// get the count of registers when the dev set this value to true
					countDistinct("id")
				}else{
					distinct(['id','name','type','expirationDate'])
				}
			}
			
			
			createAlias("locations", "l", CriteriaSpecification.LEFT_JOIN)
			
			// When is using the filter
			if( searchText != ""){
				or {
					like("name", "%" + searchText + "%")
				}
			}
			
			if(locationId != 0){
				eq("l.id", locationId)
			}
			
			if( isAdmin || isHelpDesk){
				
			}else{ // user
				or {
					and{
						'in'("l.company.id", companies)
					}
					eq("createdBy", user)
				}
				order("name", "asc")
			}
			
			firstResult(start)
			
			if(length != 0){
				maxResults(length)
			}
			
			order (columnName, orderDir)
			} // End of listData query
	}
}