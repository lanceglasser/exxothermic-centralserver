package mycentralserver.docs

import java.util.Date;

import mycentralserver.user.User;

class DocCategory {
	
	String name = ""
	
    Date dateCreated
	Date lastUpdated
	User createdBy
	User lastUpdatedBy
	
	static hasMany= [documents:Document]
	
    static constraints = {
		name nullable:false, unique: true, blank:false, maxSize:50
    }
	
	//Mapping with database table
	static mapping= {
		table "docs_categories"
		autoTimestamp true
	}
	
	static namedQueries = {
				 
	}
		
	String toString()
	{
		name;
	}
}
