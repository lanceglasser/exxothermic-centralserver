/*
 * Company.groovy
 */
package mycentralserver.company;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.CriteriaSpecification;



import mycentralserver.user.User
import mycentralserver.generaldomains.State
import mycentralserver.generaldomains.Country
import mycentralserver.custombuttons.CustomButton
import mycentralserver.content.Content
import mycentralserver.company.CompanyIntegrator;
import mycentralserver.generaldomains.CompanyType

import org.hibernate.criterion.CriteriaSpecification
import org.hibernate.Criteria

import mycentralserver.user.Employee
import mycentralserver.utils.Constants;

/**
 * Domain class with the definition of the Company object
 * 
 * @author Cecropia Solutions
 */
class Company  implements Serializable {
	
	//Properties
	String name
	String webSite
	String phoneNumber
	String address
	String zipCode
	String city
	String taxId
	String otherType
	String timezone
	String logoUrl = ""
	String pocEmail = ""
	String pocPhone = ""
	String pocFirstName = ""
	String pocLastName = ""

	boolean pocAccountCreated = false
	/*
	 * For now define if the company is a basic or premium account
	 */
	int levelOfService = Constants.COMPANY_LEVEL_OF_SERVICE_BASIC;
	
	boolean enable=true	
	//Record information
	User owner
	User createdBy
	Date dateCreated
	Date lastUpdated
	State state
	Country country
	String stateName
	CompanyCatalogSubType type
    CompanyType typeOfCompany
	CompanyIntegrator companyWideIntegrator
	//Relationship
	static belongsTo = [User]	
	static hasMany= [locations:CompanyLocation, employees:Employee]
	
	static mapping = {		
		sort "name"
		autoTimestamp true
        locations sort: 'name', order: 'asc'
	}	
	
    static constraints = {
		name 		blank:false,maxSize:50,  matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
		webSite 	url:true,  maxSize:100
		phoneNumber nullable: true, maxSize:20, matches: Constants.REGULAR_EXPRESSION_FOR_PHONE_NUMBER
		type 		nullable:true
		otherType 	nullable:true,blank:true,maxSize:20,   matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
		address 	blank:false,minSize:7, maxSize:100,  matches: Constants.REGULAR_EXPRESSION_FOR_ADDRESS_FIELD
		city  		blank:false,maxSize:100,  matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
		zipCode 	blank:false, minSize:4, maxSize:9
		taxId 		nullable:true,blank:true
		companyWideIntegrator nullable:true
		owner 		nullable:false		
		state		nullable:true
		stateName 	nullable:true,blank:true		
		timezone	nullable:true
		logoUrl		nullable:true
		pocEmail	nullable:true, blank:true, maxSize:255, email:true
		pocPhone	nullable:true, blank:true, maxSize:50, matches: Constants.REGULAR_EXPRESSION_FOR_PHONE_NUMBER
		pocFirstName	nullable:true, blank:true, maxSize:150
		pocLastName 	nullable:true, blank:true, maxSize:150
    }
	
	static namedQueries = {
		// Query that filter the data
		listAllCompaniesForUITable { start, length, columnName, orderDir, searchText, onlyEnable, user, getCount ->
			createAlias("employees", "e", CriteriaSpecification.LEFT_JOIN)
			
			projections {
				if(getCount){	// get the count of registers when the dev set this value to true
					countDistinct("id")
				}else{
					distinct(['id', 'name', 'enable'])
				}
			}
			
			// When is using the filter
			if( searchText != ""){
				or {
					like("name", "%" + searchText + "%")
				}
			}
			
			// If must find only the enable companies
			if(onlyEnable){
				eq("enable", true)
			}
			
			// If must find only the associated to a User
			if(user){
				or {
					eq("owner", user)
					and {
						eq("e.user", user)
						eq("e.enabled", true)
					}
				}
			} // End of if user
			
			firstResult(start)
			
			if(length != 0){
				maxResults(length)
			}
			
			order (columnName, orderDir)
		} // End of listData query
		
		getEnabledCompanies { user ->
			createAlias("employees", "e", CriteriaSpecification.LEFT_JOIN)
			
			  and{
				  eq("enable", true)				  			
				  or {
				   eq("owner", user)
				   and{
					   eq("e.user", user)
					   eq("e.enabled", true)
				   }
				  }  
			  }
			  resultTransformer Criteria.DISTINCT_ROOT_ENTITY
			  order("name", "asc")
		}
		
		getEnabledAndPremiumCompanies { user ->
			createAlias("employees", "e", CriteriaSpecification.LEFT_JOIN)
			
			  and{
				  eq("enable", true)
				  eq("levelOfService", Constants.COMPANY_LEVEL_OF_SERVICE_PREMIUM)
				  or {
				   eq("owner", user)
				   and{
					   eq("e.user", user)
					   eq("e.enabled", true)
				   }
				  }
			  }
			  resultTransformer Criteria.DISTINCT_ROOT_ENTITY
			  order("name", "asc")
		}
		
		getAllCompanies { user -> 
			createAlias("employees", "e", CriteriaSpecification.LEFT_JOIN)	
			  	  		
			  or {
			   eq("owner", user)
			   and{
					   eq("e.user", user)
					   eq("e.enabled", true)
				   }
			  }			
			  
			  order("name", "asc")	
			 
		}	
		
		
		getAllCompaniesByType { user , type->
			createAlias("employees", "e", CriteriaSpecification.LEFT_JOIN)
			and{
				eq("typeOfCompany", type)
			    or {
				   eq("owner", user)				  
				   and{
					   eq("e.user", user)
					   eq("e.enabled", true)
				   }
				}
			}
		}	
		getEnabledCompaniesByType { user , type->
			createAlias("employees", "e", CriteriaSpecification.LEFT_JOIN)
			  and{
				  eq("enable", true)
				  eq("typeOfCompany", type)
				  or {
				   eq("owner", user)
				   and{
					   eq("e.user", user)
					   eq("e.enabled", true)
				   }
				  }
			  }
		}
		
		typeIntegrator{  			
			eq("typeOfCompany", CompanyType.findByName("Integrator"))			
		}
		
		byType{ type->
			eq("typeOfCompany",  type)
		}
		
		getAllOwnedCompanies {
			eq("typeOfCompany", CompanyType.findByName("Owner"))
		}
				
	 
	}
	
	def beforeValidate() {
		name = name?.trim()
		phoneNumber = phoneNumber?.trim()
		city = city?.trim()
		address = address?.trim()		
	}
	
	
	List colaborators(){	 
		return employees.collect{it.user}		
	}
		
	
	String toString()
	{
		name;
	}
	
	def getRelatedButtons(){
		def relatedButtons = []
		for(location in locations){
			if(location.enable){
				relatedButtons += location.contents
			}
		}
		return relatedButtons.unique()	
	}
}
