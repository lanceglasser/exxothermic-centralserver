package mycentralserver.company

import java.io.Serializable;

import mycentralserver.company.Company
import mycentralserver.utils.Constants;

/**
 * Domain class to map the Company Integrator table
 * 
 * @author Cecropia Solutions
 * @since 01/01/2014
 */
class CompanyIntegrator implements Serializable {

    String numberOfEmployees
    String cediaNumber=""
    String contractorLicense=""
    String reseller=""
    String taxId
    boolean approved
    Company company

    static hasMany= [assignedCompanies:Company]
    static mappedBy =   [assignedCompanies: "companyWideIntegrator"]
    static transients = ['companyName']

    static constraints = {
        numberOfEmployees maxSize:100,  matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
        cediaNumber maxSize:40,  matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
        contractorLicense maxSize:40,  matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
        reseller maxSize:40,  matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
        taxId   nullable: true, blank:true, maxSize:10,matches:/^([07][1-7]|1[0-6]|2[0-7]|[35][0-9]|[468][0-8]|9[0-589])-?\d{7}$/
        approved defaultValue: false
        company nullable: false
    }
	
    static mapping= {
        sort "company.name"
        table "company_integrator"
    }

    static namedQueries = {
        getEnabledAndApprovedCompanies {
            createAlias("company", "c")
            and {
                eq("approved", true)
                eq("c.enable", true)
            }
        }

        getByUserByTypeAndApproved { user , type ->
            createAlias("company", "c" )
            and {
                eq("approved", true)
                eq("c.typeOfCompany", type)
                eq("c.owner", user)
            }
            order("c.dateCreated", "asc")
        }
    }

    String getCompanyName(){
        company.name
    }
}