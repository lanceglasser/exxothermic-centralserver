package mycentralserver.company

import java.io.Serializable;
import java.util.Date;

class CompanyCatalogSubType implements Serializable{
	
	String name
	String description
	String code
		
	//Relationship
	static belongsTo= [type:CompanyCatalogType]
	
	//Record Information
	boolean enable=true;
	
    static constraints = {
		name blank:false, maxLength:100
		description maxLength:100
		code null:false
    }
	
	static mapping= {
		table "catalog_sub_type"

	}
	@Override
	public String toString() {
		return this.name;
	}
}
