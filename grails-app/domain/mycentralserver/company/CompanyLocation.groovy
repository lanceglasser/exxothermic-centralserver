package mycentralserver.company

import mycentralserver.app.AppSkin
import mycentralserver.app.WelcomeAd
import mycentralserver.box.Box
import mycentralserver.box.BoxUsers
import mycentralserver.content.Content
import org.hibernate.criterion.CriteriaSpecification;
import mycentralserver.docs.Document;
import mycentralserver.generaldomains.Country
import mycentralserver.generaldomains.State
import mycentralserver.user.User
import mycentralserver.utils.Constants
import mycentralserver.utils.entity.EntitiesCatalog.VenueProperties;

import org.hibernate.criterion.CriteriaSpecification

class CompanyLocation implements Serializable{
	String name
	String address
	String city
	int maxOccupancy
	int numberOfTv
	int numberOfTvWithExxothermicDevice
	int contentsLimit
	String otherType
	String latitude
	String longitude
	String timezone
	String logoUrl = VenueProperties.SimpleFields.LOGO_URL.getDefault();
	String pocEmail = VenueProperties.SimpleFields.POC_EMAIL.getDefault();
	String pocPhone = VenueProperties.SimpleFields.POC_PHONE.getDefault();
	String pocFirstName = VenueProperties.SimpleFields.POC_FIRST_NAME.getDefault();
	String pocLastName = VenueProperties.SimpleFields.POC_LAST_NAME.getDefault();

	//Record information
	boolean pocAccountCreated = false
	boolean enable=true
	boolean useCompanyLogo=true
	User createdBy
	Company company
	Country country
	String stateName
	State state
	CompanyCatalogSubType type;
	User administrator
	Date dateCreated
	Date lastUpdated
	CompanyIntegrator commercialIntegrator
	AppSkin activeSkin
	WelcomeAd welcomeAd
	String contentsOrder // json string to keep the contents order
	int totalContents = VenueProperties.SimpleFields.TOTAL_CONTENTS.getDefault(); // total of contents 

	Date lastBoxUsersUpdate = new Date();
	
	// Social fields
	String facebookId
	
	//Relationship
	static belongsTo=[Content, Document]

	static hasMany=[boxes:Box, contents:Content, boxUsers:BoxUsers, documents:Document]

	static fetchMode = [ boxes:"lazy", contents:"lazy", documents:"lazy", welcomeAd:"lazy", activeSkin:"lazy" ]

  static constraints = {
		city 			blank:false , maxSize:100
		name 			blank:false , maxSize:50, matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
		address 		blank:false, minSize:7, maxSize:100, matches: Constants.REGULAR_EXPRESSION_FOR_ADDRESS_FIELD
		maxOccupancy 	min:0 ,defaultValue: 0
		numberOfTv 		min:0 , defaultValue: 0
		numberOfTvWithExxothermicDevice min:0 , defaultValue: 0
		contentsLimit 	min:1, max:6, defaultValue: 1
		type 			nullable:false
		administrator 	nullable:false
		company 		nullable:false
		otherType 		nullable:true,blank:true,maxSize:20
		createdBy 		nullable:false
		commercialIntegrator nullable:true
		activeSkin  	nullable:true
		latitude		nullable:true
		contentsOrder  	nullable:true
		longitude		nullable:true
		state			nullable:true
		stateName 		nullable:true,blank:true
		timezone		nullable:true
		welcomeAd		nullable:true
		logoUrl			nullable:true
		pocEmail		nullable:true, blank:true, maxSize:150, email:true
		pocPhone		nullable:true, blank:true, maxSize:35, matches: Constants.REGULAR_EXPRESSION_FOR_PHONE_NUMBER
		pocFirstName	nullable:true, blank:true, maxSize:50, matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
		pocLastName 	nullable:true, blank:true, maxSize:50, matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
		boxes			lazy: true
		contents		lazy: true
		company			lazy: true
		documents		lazy: true
		facebookId		nullable:true
    }

	static mapping= {
		table "company_location"
		autoTimestamp true
	}


	static namedQueries = {

		// Query that filter the data
		listAllLocationsForUITable { start, length, columnName, orderDir, searchText, 
			onlyEnable, user, companyId, getCount ->
			
			createAlias("company", "c")
			createAlias("c.employees", "e", CriteriaSpecification.LEFT_JOIN)

			createAlias("c.companyWideIntegrator", "cwi" , CriteriaSpecification.LEFT_JOIN)
			createAlias("cwi.company", "c2" , CriteriaSpecification.LEFT_JOIN)
			createAlias("c2.employees", "e2", CriteriaSpecification.LEFT_JOIN)

			createAlias("commercialIntegrator", "ci", CriteriaSpecification.LEFT_JOIN)
			createAlias("ci.company", "c3", CriteriaSpecification.LEFT_JOIN)
			createAlias("c3.employees", "e3", CriteriaSpecification.LEFT_JOIN)
			
			projections {
				if(getCount){	// get the count of registers when the dev set this value to true
					countDistinct("id")
				}else{
					distinct(['id', 'name', 'enable'])
				}
			}
			
			// When is using the filter
			if( searchText != ""){
				or {
					like("name", "%" + searchText + "%")
				}
			}
			
			// If must find only the enable companies
			if(onlyEnable){
				eq("enable", true)
			}
			
			// If must filter by company
			if(companyId != 0){
				eq("c.id", companyId)
			}
			
			// If must find only the associated to a User
			if(user){
				or {
					eq("c.owner", user)
					eq("createdBy", user)
					
					and {
						eq("e.user", user)
						eq("e.enabled", true)
					}
	
				    and {
					   isNull("commercialIntegrator")
					   isNotNull("c.companyWideIntegrator")
	
					   or{
						   eq("c2.owner", user)
						   and {
							   eq("e2.user", user)
							   eq("e2.enabled", true)
						   }
					   	}
				    }
	
					and{
						isNotNull("commercialIntegrator")
						or{
							eq("c3.owner", user)
							and {
								eq("e3.user", user)
								eq("e3.enabled", true)
							}
						}
					}
				} // End of or
			} // End of if user
			
			firstResult(start)
			
			if(length != 0){
				maxResults(length)
			}
			
			order (columnName, orderDir)
		} // End of listData query
		
		getAllLocations { user , company ->

			createAlias("company", "c")
			createAlias("c.employees", "e", CriteriaSpecification.LEFT_JOIN)

			createAlias("c.companyWideIntegrator", "cwi" , CriteriaSpecification.LEFT_JOIN)
			createAlias("cwi.company", "c2" , CriteriaSpecification.LEFT_JOIN)
			createAlias("c2.employees", "e2", CriteriaSpecification.LEFT_JOIN)


			createAlias("commercialIntegrator", "ci", CriteriaSpecification.LEFT_JOIN)
			createAlias("ci.company", "c3", CriteriaSpecification.LEFT_JOIN)
			createAlias("c3.employees", "e3", CriteriaSpecification.LEFT_JOIN)

			if(company){
				eq("company", company)
			}
			or {
				eq("c.owner", user)
				eq("createdBy", user)
				and{
					eq("e.user", user)
					eq("e.enabled", true)
				}

			    and{
				   isNull("commercialIntegrator")
				   isNotNull("c.companyWideIntegrator")

				   or{
					   eq("c2.owner", user)
					   and {
						   eq("e2.user", user)
						   eq("e2.enabled", true)
					   }
				   	}
			     }

				and{
					isNotNull("commercialIntegrator")
					or{
						eq("c3.owner", user)
						and {
							eq("e3.user", user)
							eq("e3.enabled", true)
						}
					}
				}
			}

		order ("name", "asc")
		} //END getAllLocations
	}

	@Override
	public String toString() {
		return this.name
	}


	public boolean isInteger(a) {
		String.isNumber()
		return (!(Integer.parseInt(a).toString() == 'NaN'))
		  }

}
