package mycentralserver.device

class Device {

    String name = ""
    String manufacturer = ""
    String buildFingerprint = ""

    /**
     * When true must be include in the list of devices that should handle smaller packages for audio
     */
    boolean useSmallerPackages = false

    static constraints = {
        name 				nullable:false, unique: true, blank:false, minSize:1, maxSize:250
        buildFingerprint 	nullable:false, unique: true, blank:false, minSize:1, maxSize:250
        manufacturer        maxSize:250
    }

    //Mapping with database table
    static mapping= { table "devices" }

    static namedQueries = {
    }

    String toString() {
        name;
    }
}