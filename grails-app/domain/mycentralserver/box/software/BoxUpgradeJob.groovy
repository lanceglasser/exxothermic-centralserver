/**
 * BoxUpgradeJob.groovy
 */
package mycentralserver.box.software

import java.util.Date;

import org.hibernate.criterion.CriteriaSpecification;

import mycentralserver.box.Box;
import mycentralserver.utils.Constants;

/**
 * Class domain for the Upgrade Job of a
 * single ExXtractor as part of a Job of multiple ExXtractors
 * 
 * @author Cecropia Solutions
 *
 */
class BoxUpgradeJob {

	/**
	 * Current Status of the upgrade process of a single ExXtractor
	 */
	int status = Constants.BOX_SOFTWARE_UPGRADE_STATUS_CREATED;
	
	/**
	 * Total number of steps of this Job
	 */
	int totalSteps = 0;
	
	/**
	 * Counter with completed steps of this Job
	 */
	int completedSteps = 0;
	
	/**
	 * ExXtractor related to this single upgrade process
	 */
	Box box;
	
	/**
	 * Final Version to Upgrade the Single ExXtractor
	 */
	BoxSoftware boxSoftware;
	
	/**
	 * Job entity related to this upgrade 
	 */
	BoxSoftwareUpgradeJob boxSoftwareUpgradeJob;
	
	Date dateCreated;
	Date lastUpdated;
	
	static belongsTo = [BoxSoftwareUpgradeJob, Box]
	
	static hasMany= [boxUpgradeSteps: BoxUpgradeStep]
	
    static constraints = {
		
    } // End of constraints
	
	static mapping = {
		table "box_upgrade_job"
		autoTimestamp true
	} // End of mapping
	
	static namedQueries = {
		
		// Get the list of Box Jobs that are not completed
		getIncompleteBoxUpgradeJob { box ->

			createAlias("box", "b")
			createAlias("boxSoftware", "boxSoftware")
			eq("b.id", box.id)
			ne("status", Constants.BOX_SOFTWARE_UPGRADE_STATUS_COMPLETED)
			order ('boxSoftware.versionSoftware', 'asc')
			
		} // End of getIncompleteBoxUpgradeJob query
	} // End of namedQueries
	
	/**
	 * When a step is completed should  increase this counter
	 */
	public void increaseCompletedSteps() {
		this.completedSteps = (this.completedSteps < this.totalSteps)? this.completedSteps + 1:this.totalSteps;
		if(this.completedSteps == this.totalSteps){
			this.status = Constants.BOX_SOFTWARE_UPGRADE_STATUS_COMPLETED;
			this.boxSoftwareUpgradeJob.increaseCompletedBoxes();
		}
		this.lastUpdated = new Date();
		this.save(flush:true);
	} // End of increaseCompletedSteps method
} // End of class
