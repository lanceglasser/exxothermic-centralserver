/**
 * BoxUpgradeStep.groovy
 */
package mycentralserver.box.software

import java.util.Date;

import mycentralserver.utils.Constants;

/**
 * Class domain of a step as part of the upgrade job
 * of a single ExXtractors; one step refers to the upgrade
 * from one version to the next one.
 * 
 * @author Cecropia Solutions
 *
 */
class BoxUpgradeStep {

	/**
	 * Current status of this step
	 */
	int status = Constants.BOX_SOFTWARE_UPGRADE_STATUS_CREATED;
	
	/**
	 * This step will upgrade to this version
	 */
	BoxSoftware		boxSoftware;
	
	/**
	 * This step belongs to this single box upgrade job
	 */
	BoxUpgradeJob 	boxUpgradeJob;
	
	Date 			dateCreated;
	Date 			lastUpdated;
	
	static belongsTo = [BoxUpgradeJob]
	
    static constraints = {
    }
	
	static mapping = {
		table "box_upgrade_step"
		autoTimestamp true
	} // END mapping
	
	static namedQueries = {
		
		// Get the list of Steps that are not completed
		getIncompleteBoxUpgradeJobSteps { box ->
			createAlias("boxUpgradeJob", "job")
			createAlias("job.box", "b")
			createAlias("boxSoftware", "boxSoftware")
			eq("b.id", box.id)
			ne("status", Constants.BOX_SOFTWARE_UPGRADE_STATUS_COMPLETED)
			order ('boxSoftware.versionSoftware', 'asc')
			
		} // End of getIncompleteBoxUpgradeJobSteps query
	} // End of namedQueries
} // End of class
