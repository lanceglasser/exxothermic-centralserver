package mycentralserver.box.software

import mycentralserver.app.Affiliate;

/**
 * This domain map the list of features and the availability definition depending of the architecture, affiliate and
 * others
 * 
 * @author sdiaz
 * @since 11/16/2015
 */
class FeatureDetail {

    String code;
    String description;
    String architectures;
    int minimumVersion;
    Affiliate affiliate;

    static constraints = {
    }
}