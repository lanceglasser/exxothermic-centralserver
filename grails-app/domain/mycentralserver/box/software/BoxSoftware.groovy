package mycentralserver.box.software

import java.util.Date;

import mycentralserver.app.Affiliate;
import mycentralserver.user.User;
import mycentralserver.utils.Constants;
import mycentralserver.utils.enumeration.Architecture;

class BoxSoftware implements Serializable{

	String 	name;
	int 	versionSoftware = 1;
	String 	versionSoftwareLabel;
	String 	description;
	String 	fileName;
	String 	fileNameOrigin;
	String 	pathLocation;
	String 	checksum;
	boolean enable = true
	boolean isSupported = true
	User 	createdBy
	User 	lastUpdatedBy
	Date 	dateCreated
	Date 	lastUpdated
	
	/**
	 * Type of the Image [Legacy32=1; X32=2; X64=3]: Obsolete; using associatedArchitectures now.
	 */
	int 	imageType = 0
	
    String associatedArchitectures = ""; 
    
	Affiliate affiliate // null value means its avaiable for all clients
	
    static constraints = {
		name 			blank:false,unique:true,maxSize:50,  matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
		versionSoftware	unique:true
		versionSoftwareLabel blank:false, maxSize:50, matches: Constants.REGULAR_EXPRESSION_FOR_SOFTWARE_VERSION_LABEL
		description 	blank:false,maxSize:255, matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
		fileName 		blank:false, unique:true,  maxSize:100,  matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
		fileNameOrigin 	blank:false,maxSize:100,  matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
		pathLocation 	blank:false,maxSize:300
		checksum 		blank:true, maxSize:300;
		affiliate		nullable:true
        associatedArchitectures nullable:false, maxSize: 50
    } // END constraints
	
	static mapping = { 
		table "box_software"
		autoTimestamp true
	} // END mapping
	
		   
	static namedQueries = {
		getLastVersionSoftwareUpdate { boxSoftwareId ->
				eq("enable", true)	
				gt("id", boxSoftwareId)
		}
		
		getAllByVersionSoftwareGreaterThanEnableAndSupported { versionId, imageType, affiliate ->
			eq("enable", true)
			eq("isSupported", true)
			or {
                ilike("associatedArchitectures", "%[" + imageType + "]%")
			}
			or {
				eq("affiliate", affiliate)
				isNull("affiliate")
			}
			gt("versionSoftware", versionId)
		}
		
		getLastSoftwareVersionEnabled  {
			and {
				eq("enable", true)
				order("id", "desc")
			}
		}
	} // END namedQueries

    /**
     * Checks if the received architecture number is on the list of architectures associated to this software version
     * @param arch
     *      Related number of the architecture to check
     * @return True or False depending if the architecture is associated or not
     */
    boolean isArchAssociated(int arch) {
        return (associatedArchitectures && associatedArchitectures.indexOf("[" + arch + "]") > -1);
    }

    /**
     * Returns the list of the associated architectures to this software version as a String
     * @return List of associated architectures separated by ','; if All of them are associated will return "All"
     */
    String getArchitecturesList() {
        String architectures = "";
        int architecturesCounter = 0;
        final Architecture[] architecturesEnums = Architecture.values();
        for(Architecture currentArchitecture : Architecture.values()) {
            if(isArchAssociated(currentArchitecture.getValue())) {
                architectures += (architectures == "")? currentArchitecture: "," + currentArchitecture;
                architecturesCounter++;
            }
        }
        return (architecturesCounter == architecturesEnums.length)? Constants.ALL:architectures;
    }
    
    public static enum SimpleFields {
        ID("id"),
        VERSION_SOFTWARE("versionSoftware");

        private String fieldName;

        private SimpleFields(String fieldName) {
            this.fieldName = fieldName;
        }

        public String fieldName() {
            return fieldName;
        }
    }
} // END BoxVersion
