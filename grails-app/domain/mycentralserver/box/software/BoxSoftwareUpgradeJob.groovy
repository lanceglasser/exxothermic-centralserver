/**
 * BoxSoftwareUpgradeJob.groovy
 */
package mycentralserver.box.software

import java.util.Date;

import mycentralserver.box.Box;
import mycentralserver.user.User;
import mycentralserver.utils.Constants;

/**
 * Class domain of a Job for the software upgrade of multiple
 * ExXtractors 
 * 
 * @author Cecropia Solutions
 *
 */
class BoxSoftwareUpgradeJob {

	String description;
	
	int status = Constants.BOX_SOFTWARE_UPGRADE_STATUS_STARTED;
	
	/**
	 * Total number of ExXtractors to Upgrade with this Job
	 */
	int totalBoxes;
	
	/**
	 * Counter with the number of ExXtractor of this Job that
	 * already complete their Upgrade to the Final Version
	 */
	int completedBoxes;
	
	User 	createdBy;
	User 	lastUpdatedBy;
	Date 	dateCreated;
	Date 	lastUpdated;
	
	static hasMany= [boxUpgradeJobs: BoxUpgradeJob]
	
    static constraints = {
		description 			blank:false,unique:true,minSize:1,maxSize:255
    } // End of constraints
	
	static mapping = {
		table "box_software_upgrade_job"
		autoTimestamp true
	} // End of mapping
	
	static namedQueries = {
		
	} // End of namedQueries
	
	/**
	 * When an ExXtractor completed their Upgrade should  increase this counter
	 */
	public void increaseCompletedBoxes() {
		this.completedBoxes = (this.completedBoxes < this.totalBoxes)? this.completedBoxes + 1:this.totalBoxes;
		if(this.completedBoxes == this.totalBoxes) {
			this.status = Constants.BOX_SOFTWARE_UPGRADE_STATUS_COMPLETED;
		}
		this.lastUpdated = new Date();
		this.save(flush:true);
	} // End of increaseCompletedBoxes method
} // End of class
