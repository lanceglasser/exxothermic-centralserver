package mycentralserver.box

import java.util.Date;
import java.util.List;

import mycentralserver.company.CompanyLocation
import mycentralserver.user.User
import mycentralserver.utils.Constants;
import mycentralserver.utils.EncrypterHelper;

import org.hibernate.criterion.CriteriaSpecification
 
import org.codehaus.groovy.grails.orm.hibernate.cfg.IdentityEnumType

class BoxUsers {
		
	/**
	 * Username of the User
	 */
	String username = "";
	
	/**
	 * Encrypted Password
	 */
	String encryptedPassword = "";
	
	/**
	 * Defines if the user is enabled	
	 */
	boolean enable = true;
	
	/**
	 * Defines the type of the User: Tech=1 or Admin=2
	 */
	int type  = 1;
	
	CompanyLocation location
				
	Date dateCreated
	Date lastUpdated
	User createdBy
	User lastUpdatedBy
	
	static belongsTo = [location:CompanyLocation]
	
    static constraints = {
		username 			nullable:false
		encryptedPassword	nullable:true
    }
	
	//Mapping with database table
	static mapping= {
		table "box_users"
		encryptedPassword	type: 'text'
		autoTimestamp true
	}
		
	static namedQueries = {
		
	}
	 
	String toString()
	{
		return "-> Exxtractor User: " + username;
	}
	
	/**
	 * This method receives the plain pass and returns the 
	 * encrypted value
	 * @param pass	Password to encrypt
	 * @return		Encrypted Password
	 */
	public static String encryptPassword(String pass){
		EncrypterHelper encrypterHelper = new EncrypterHelper();
		return encrypterHelper.encryptText2Base64WithRSA(pass);
	}
	
	/**
	 * This method decrypts a password
	 * @param encryptedPass	Password to decrypt
	 * @return				Decrypted Password
	 */
	public static String decryptPassword(String encryptedPass){
		EncrypterHelper encrypterHelper = new EncrypterHelper();
		return encrypterHelper.decryptBase642TextWithRSA(encryptedPass);
	}
	
	public static int getTypeFromLevel(String value){
		int typeValue = 0;
		switch(value){
			case "tech":
				typeValue = Constants.BOX_USER_TYPE_TECH;
				break;
			case "admin":
				typeValue = Constants.BOX_USER_TYPE_ADMIN;
				break;
			case "user":
				typeValue = Constants.BOX_USER_TYPE_USER;
				break;
			default:
				typeValue = 0;
				break;
		}
		return typeValue;
	}
	
	public String getLevelDescription(){
		switch(this.type){
			case Constants.BOX_USER_TYPE_TECH:
				return "tech";
			case Constants.BOX_USER_TYPE_ADMIN:
				return "admin";
			case Constants.BOX_USER_TYPE_USER:
				return "user";
			default :
				return "none";
		}
	}
	
}
