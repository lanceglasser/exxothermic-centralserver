/**
 * BoxData.groovy
 */
package mycentralserver.box

import java.util.Date;

import org.hibernate.criterion.CriteriaSpecification;

import mycentralserver.company.CompanyLocation

/**
 * Domain class that store the usage report aggregated per day
 * per Venue information of a ExXtractor
 * after 2 weeks and for 1 year
 *  
 * @author Cecropia Solutions
 */
class BoxAggregatedData {
	
	int 	totalSessions = 0;
	int 	maxConcurrentSessions = 0;
	int		uniqueClients = 0;
	double	time = 0;
	
	Date 	date;
		
	static belongsTo= [location:CompanyLocation]
	
	
    static constraints = {		
    }
	
	static mapping = {
	}
	
	static namedQueries = {
	} // End of namedQueries
	
} // End of Class
