package mycentralserver.box

import java.util.Date;
import java.util.List;

import mycentralserver.company.CompanyLocation
import mycentralserver.user.User
import mycentralserver.utils.Constants;
import mycentralserver.utils.EncrypterHelper;

import org.hibernate.criterion.CriteriaSpecification

import org.codehaus.groovy.grails.orm.hibernate.cfg.IdentityEnumType

class BoxConfig {

    /**
     * Unique code of the configuration
     */
    String code = "";

    /**
     * Label use for the message send it to the ExXtractors
     */
    String jsonLabel = "";

    /**
     * Label Code for translation
     */
    String labelCode = "";

    /**
     * Default value of the config
     */
    String defaultValue = "";

    /**
     * Translation code for the help message, empty or null for no message
     */
    String helpMsgLabelCode = null;

    /**
     * Box version label since the configuration is valid
     */
    String validBoxVersion = null;

    /**
     * Extra configurations required by the config; for example for number type can have min, max and step
     */
    String configOptions = null;

    /**
     * Sets if the configuration can be configured from the UI
     */
    boolean enableForUI = false;

    /**
     * Define the type of input element for this configuration
     * 1: Select
     * 2: Text Input
     * 3: Number Input
     */
    int inputType = Constants.INPUT_TYPE_TEXT;

    static hasMany= [options:BoxConfigOption]

    static constraints = {
        code                nullable:false
        labelCode           nullable:false
        jsonLabel           nullable:false
        defaultValue        nullable:true
        helpMsgLabelCode    nullable:true
        validBoxVersion     nullable:true
        configOptions       nullable:true
        version: false
    }

    //Mapping with database table
    static mapping= { table "box_configs" }

    static namedQueries = {
    }

    String toString(){
        return this.code;
    }
}
