package mycentralserver.box

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import mycentralserver.user.User;
import mycentralserver.utils.Constants;
import mycentralserver.utils.enumeration.AudioCardGeneration;
import mycentralserver.utils.enumeration.JumperConfiguration;

/**
 * Domain class with the definition of the box_manufacturer table
 *
 * @author sdiaz
 * @since 01/01/2015
 */
class BoxManufacturer implements Serializable {
    String serialNumber
    String legacySerialNumber
    String certificateLocation
    String certificateFileName
    String physicalSerial
    int architecture = 0;
    int audioCardGeneration = AudioCardGeneration.SECOND.getValue();
    int jumperConfiguration = JumperConfiguration.SUM_TO_MONO.getValue();
    String audioInputMode
    String managementPasscode
    Date dateCreated
    Date lastUpdated
    //Relationship
    static belongsTo= [deviceType:BoxDeviceType, createdBy:User]

    static constraints = {
        serialNumber            blank:false,unique:true,maxLength:40,  matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
        legacySerialNumber      nullable:true,blank:false,unique:true,maxLength:40,  matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
        certificateLocation     blank:false,unique:true,maxLength:100,  matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
        certificateFileName     blank:false,unique:true,maxLength:40,  matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
        physicalSerial          nullable:true,blank:false,unique:true,maxLength:40,  matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
        deviceType              nullable:true
        managementPasscode      nullable:true
    }

    static mapping = { autoTimestamp true }

    String getMacAddress() {
        final BoxIp boxIp = BoxIp.findBySerial(serialNumber);
        return boxIp? boxIp.macAddress:StringUtils.EMPTY;
    }
}
