package mycentralserver.box

import java.util.Date;

import org.hibernate.criterion.CriteriaSpecification;

import mycentralserver.company.Company;
import mycentralserver.company.CompanyLocation
import mycentralserver.utils.Constants;
import mycentralserver.box.software.BoxSoftware
import mycentralserver.box.BoxChannel
import mycentralserver.utils.BoxStatusEnum

class Box implements Serializable{

	String name
	//Box information
	/**
	 * This information will be send by the box
	 * */
	String versionSO;
	String frameworkVersion;
	String model;
	String serial;
	String bios;
	boolean pa=false;
	Date dateCreated;
	Date lastUpdated;
	boolean hasCertificate = false;
	int imageType;
    int audioCardGeneration;
    int jumperConfiguration;
	
	//Relationship
	static belongsTo= [location:CompanyLocation,status:BoxStatus, softwareVersion:BoxSoftware, certificate:BoxCertificate] 	
	
	static hasMany=[channels:BoxChannel]
	
	//
	static optionals =["frameworkVersion","versionSO","model", "softwareVersion"]
	
	static mapping = {		
		/*autoTimestamp true*/
		channels cascade: "all-delete-orphan"
	}
	
    static constraints = {
		serial 				blank:false,unique:true,maxLength:40,  matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
		name 				blank:true,maxSize:50
		versionSO 			nullable:true
		bios 				nullable:true
		frameworkVersion 	nullable:true
		model 			 	nullable:true
		
		location 			nullable:false
		status 				nullable:false		
		softwareVersion  	nullable:true		
		certificate			nullable:true			
    }
		
	static namedQueries = {
		
		// Query that filter the data
		listAllExxtractorsForUITable { start, length, columnName, orderDir, searchText, user,
			companyId, locationId, getCount ->
									
			createAlias("location", "l")
			createAlias("status", "s")
            createAlias("softwareVersion", "sv", CriteriaSpecification.LEFT_JOIN)
			createAlias("l.company", "c")
			createAlias("c.employees", "e", CriteriaSpecification.LEFT_JOIN)

			createAlias("c.companyWideIntegrator", "cwi" , CriteriaSpecification.LEFT_JOIN)
			createAlias("cwi.company", "c2" , CriteriaSpecification.LEFT_JOIN)
			createAlias("c2.employees", "e2", CriteriaSpecification.LEFT_JOIN)

			createAlias("l.commercialIntegrator", "ci", CriteriaSpecification.LEFT_JOIN)
			createAlias("ci.company", "c3", CriteriaSpecification.LEFT_JOIN)
			createAlias("c3.employees", "e3", CriteriaSpecification.LEFT_JOIN)

			projections {
				if(getCount){	// get the count of registers when the dev set this value to true
					countDistinct("id")
				}else{
					distinct(['id', 'serial', 'name', 'l.name', 's.description', 'lastUpdated', 'pa', 'versionSO', 
                        'sv.versionSoftwareLabel', 'sv.versionSoftware', 'model'])
				}
			}
			
			if(companyId != 0) {
				eq("c.id", companyId)
			}
			
			if(locationId != 0) {
				eq("l.id", locationId)
			}
			
			// When is using the filter
			if( searchText != ""){
				or {
					like("name", "%" + searchText + "%")
					like("serial", "%" + searchText + "%")
					like("l.name", "%" + searchText + "%")
                    like("sv.versionSoftwareLabel", "%" + searchText + "%")
                    like("model", "%" + searchText + "%")
				}
			}
						
			// If must find only the associated to a User
			if(user){
				or {
					eq("c.owner", user)
					eq("l.createdBy", user)
					
					and {
						eq("e.user", user)
						eq("e.enabled", true)
					}
	
					and {
					   isNull("l.commercialIntegrator")
					   isNotNull("c.companyWideIntegrator")
	
					   or{
						   eq("c2.owner", user)
						   and {
							   eq("e2.user", user)
							   eq("e2.enabled", true)
						   }
						   }
					}
	
					and{
						isNotNull("l.commercialIntegrator")
						or{
							eq("c3.owner", user)
							and {
								eq("e3.user", user)
								eq("e3.enabled", true)
							}
						}
					}
				} // End of or
			} // End of if user
			
			firstResult(start)
			
			if(length != 0){
				maxResults(length)
			}
			
			order (columnName, orderDir)
		} // End of listAllExxtractorsForUITable query
		
		boxStatusData { unsupportedVersion, user , companyId, locationId, status_filter,
						lastVersionOS ->
						
			createAlias("status", "st", CriteriaSpecification.LEFT_JOIN)
			createAlias("softwareVersion", "sv", CriteriaSpecification.LEFT_JOIN)
			createAlias("location", "l")
			createAlias("l.company", "c")
			createAlias("c.employees", "e", CriteriaSpecification.LEFT_JOIN)
			createAlias("c.companyWideIntegrator", "cwi" , CriteriaSpecification.LEFT_JOIN)
			createAlias("cwi.company", "c2" , CriteriaSpecification.LEFT_JOIN)
			createAlias("c2.employees", "e2", CriteriaSpecification.LEFT_JOIN)
			createAlias("l.commercialIntegrator", "ci", CriteriaSpecification.LEFT_JOIN)
			createAlias("ci.company", "c3", CriteriaSpecification.LEFT_JOIN)
			createAlias("c3.employees", "e3", CriteriaSpecification.LEFT_JOIN)
			
			projections {
				countDistinct("id")
			}
			
			if(companyId != 0) {
				eq("c.id", companyId)
			}
			
			if(locationId != 0) {
				eq("l.id", locationId)
			}
			
			if(status_filter == "offline_needs_upgrade") {
				Date nowDate = new Date();
				GregorianCalendar gc = new GregorianCalendar();
				gc.setTime(nowDate);
				gc.add(Calendar.MINUTE, -7);
				Date lastConnectedTime = gc.getTime();
				
				or {
					lt("lastUpdated", lastConnectedTime)// offline boxes
					ne("st.description", BoxStatusEnum.CONNECTED.value)
				}
				
				//lt("softwareVersion",lastVersionOS) // needs upgrade
				lt("sv.versionSoftware",lastVersionOS.versionSoftware)
				eq("sv.isSupported", true)
			}else if(status_filter == "offline_upgraded"){
			
				Date nowDate = new Date();
				GregorianCalendar gc = new GregorianCalendar();
				gc.setTime(nowDate);
				gc.add(Calendar.MINUTE, -7);
				Date lastConnectedTime = gc.getTime();
				
				or {
					lt("lastUpdated", lastConnectedTime)// offline boxes
					ne("st.description", BoxStatusEnum.CONNECTED.value)
				}
				eq("sv.versionSoftware",lastVersionOS.versionSoftware)
				eq("sv.isSupported", true)
			}else if(status_filter == "online_upgraded"){
				Date nowDate = new Date();
				GregorianCalendar gc = new GregorianCalendar();
				gc.setTime(nowDate);
				gc.add(Calendar.MINUTE, -7);
				Date lastConnectedTime = gc.getTime();
							
				ge("lastUpdated", lastConnectedTime)// online boxes
				eq("st.description", BoxStatusEnum.CONNECTED.value)
				
				eq("softwareVersion",lastVersionOS) //up to date boxes
				eq("sv.isSupported", true)
			}else if(status_filter == "online_needs_upgrade"){
				Date nowDate = new Date();
				GregorianCalendar gc = new GregorianCalendar();
				gc.setTime(nowDate);
				gc.add(Calendar.MINUTE, -7);
				Date lastConnectedTime = gc.getTime();
							
				ge("lastUpdated", lastConnectedTime)// online boxes
				eq("st.description", BoxStatusEnum.CONNECTED.value)
				
				lt("softwareVersion",lastVersionOS) // needs upgrade
				eq("sv.isSupported", true)
			}else if(status_filter == "outdated"){				
				eq("sv.isSupported",false)
			}else if(status_filter == "unversioned"){				
				isNull("softwareVersion")
			}
									
			// If must find only the associated to a User
			if(user){
				or {
					eq("c.owner", user)
					eq("l.createdBy", user)
					
					and {
						eq("e.user", user)
						eq("e.enabled", true)
					}
	
					and {
					   isNull("l.commercialIntegrator")
					   isNotNull("c.companyWideIntegrator")
	
					   or{
						   eq("c2.owner", user)
						   and {
							   eq("e2.user", user)
							   eq("e2.enabled", true)
						   }
						   }
					}
	
					and{
						isNotNull("l.commercialIntegrator")
						or{
							eq("c3.owner", user)
							and {
								eq("e3.user", user)
								eq("e3.enabled", true)
							}
						}
					}
				} // End of or
			} // End of if user
			
			
		} // End of boxStatusData query
		
		// Get the list of "connected" ExXtractors using the status domain
		connectedByStatus { locationId, locations ->
			createAlias("location", "l", CriteriaSpecification.LEFT_JOIN)
			createAlias("status", "st", CriteriaSpecification.LEFT_JOIN)

			eq("st.description", BoxStatusEnum.CONNECTED.value)
			
			Date nowDate = new Date();
			GregorianCalendar gc = new GregorianCalendar();
			gc.setTime(nowDate);
			gc.add(Calendar.MINUTE, -7);
			Date lastConnectedTime = gc.getTime();
						
			ge("lastUpdated", lastConnectedTime);
			
			// When is filtered by Location
			if(locationId != 0){
				eq("l.id", locationId)
			} else {
				if(locations){
					//Only applies when there is a restringed list of locations
					'in'(
						'l.id', locations.collect {loc ->
						  return loc.id;
						}
					)
				}
			}
			
		} // End of connectedByStatus query
	}

	
	String toString()
	{
		name;
	}
	
	String lastConnected(String messageFormat) {
		
		Date start = lastUpdated 
		Date end = new Date()
	
	
		long diffInSeconds = (end.getTime() - start.getTime()) / 1000
		long [] diff  =   [0,0,0,0]
		
		/* sec */
		diff[3] =  (diffInSeconds >= 60 ? diffInSeconds % 60 : diffInSeconds)
		/* min */
		diff[2] =  (diffInSeconds = (diffInSeconds / 60)) >= 60 ? diffInSeconds % 60 : diffInSeconds
		/* hours */
		diff[1] =  (diffInSeconds = (diffInSeconds / 60)) >= 24 ? diffInSeconds % 24 : diffInSeconds
		/* days */
		diff[0] =  (diffInSeconds = (diffInSeconds / 24))
		
				
		return String.format(messageFormat, diff[0], diff[1], diff[2], diff[3]);
	
	}
	
    public static checkConectivity(String statusDescription, def lastUpdated, String messageFormat){
        final Date start = lastUpdated
        final Date end = new Date();
        long [] diff = [0, 0, 0, 0];
        long diffInSeconds = (end.getTime() - start.getTime()) / 1000;
        /* sec */
        diff[3] =  (diffInSeconds >= 60 ? diffInSeconds % 60 : diffInSeconds);
        /* min */
        diff[2] =  (diffInSeconds = (diffInSeconds / 60)) >= 60 ? diffInSeconds % 60 : diffInSeconds;
        /* hours */
        diff[1] =  (diffInSeconds = (diffInSeconds / 60)) >= 24 ? diffInSeconds % 24 : diffInSeconds;
        /* days */
        diff[0] =  (diffInSeconds = (diffInSeconds / 24));
        if ( diff[0] == 0 && diff[1] == 0 && diff[2] < 7  && (statusDescription.equals(BoxStatusEnum.CONNECTED.value))) {
            return [true, String.format(messageFormat, diff[0], diff[1], diff[2], diff[3])]
        } else {
            return [false, String.format(messageFormat, diff[0], diff[1], diff[2], diff[3])]
        }
    }
	
	boolean connected(){
		Date start = lastUpdated
		Date end = new Date()
	
		long diffInSeconds = (end.getTime() - start.getTime()) / 1000
		/* sec */
		def sec =  (diffInSeconds >= 60 ? diffInSeconds % 60 : diffInSeconds)
		/* min */
		def min =  (diffInSeconds = (diffInSeconds / 60)) >= 60 ? diffInSeconds % 60 : diffInSeconds
		/* hours */
		def hours =  (diffInSeconds = (diffInSeconds / 60)) >= 24 ? diffInSeconds % 24 : diffInSeconds
		/* days */
		def days =  (diffInSeconds = (diffInSeconds / 24))
		
		
		
		if ( days == 0 && hours == 0 && min < 7  && (status != null && status.description.equals(BoxStatusEnum.CONNECTED.value)))
		{
			return true
		}else{
			return false
		}		
	}
	
	public String getBoxVersion(String emptyValue){
		
		String ver="";
		if(this.softwareVersion != null){
			ver = softwareVersion.versionSoftwareLabel.encodeAsHTML() + " v" + softwareVersion.versionSoftware.encodeAsHTML();
		} else {
			if(this.versionSO != null && versionSO.trim() != ""){
				ver = "v" + versionSO;
			} else {
				ver = emptyValue;
			}
		}
		return ver;
	}		
	
}
