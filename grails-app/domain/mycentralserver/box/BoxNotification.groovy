package mycentralserver.box

import java.util.Date;

import mycentralserver.utils.Constants;

class BoxNotification {
	String action
	//We use an String because we have to store the IP from the users connected
	String user
	//the time is in seconds
	int time
	
	BoxChannel boxChannel	
	Date dateCreated
	Box box

    static constraints = {
		action 	blank:false,maxLength:40,  matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
		user 	blank:false,maxLength:40, matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
		box nullable:false
		boxChannel nullable:false
    }
	
	static mapping = {
		version false
		autoTimestamp true
	}
}
