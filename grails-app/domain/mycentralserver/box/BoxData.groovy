/**
 * BoxData.groovy
 */
package mycentralserver.box

import java.util.Date;

import org.hibernate.criterion.CriteriaSpecification;

import mycentralserver.company.CompanyLocation

/**
 * Domain class that store the usage report information of a ExXtractor
 * 
 * @author Cecropia Solutions
 *
 */
class BoxData {

	//DAY_OF_YEAR/DATE/PORT/HOUR/NAME/TRCK_TIME/TOTAL_SESSIONS/MAX_SESSIONS
	
	String 	port;
	String 	channelName;
	int 	hour;
	int 	totalSessions = 0;
	int 	maxSessions = 0;
	double	time = 0;
	Date 	date;
	String serial;
	String boxName;
	
	Date 	dateCreated;
	Date 	lastUpdated;
	
	static belongsTo= [location:CompanyLocation]
	
	
    static constraints = {
		hour(unique: ['location','serial', 'port', 'date']) //Unique key
    }
	
	static mapping = {
		autoTimestamp true
	}
	
	static namedQueries = {
		
		// Query that filter the data
		listData { start, length, columnName, orderDir, searchText, locationId, startDate, endDate, locations ->

			//createAlias("box", "b")
			createAlias("location", "l")
			//createAlias("l.boxes", "b", CriteriaSpecification.LEFT_JOIN)
			
			ge("date", startDate)
			le("date", endDate)
			
			// When is filtered by Location
			if(locationId != 0){
				eq("l.id", locationId)
			} else {
				if(locations){
					//Only applies when there is a restringed list of locations
					'in'(
						'l.id', locations.collect {loc ->
						  return loc.id;
						}
					)
				}
			}
			
			// When is using the filter
			if( searchText != ""){
				or {
					like("l.name", "%" + searchText + "%")
					like("boxName", "%" + searchText + "%")
					like("serial", "%" + searchText + "%")
					like("port", "%" + searchText + "%")
					like("channelName", "%" + searchText + "%")
				}
			}
			
			firstResult(start)
			
			if(length != 0){
				maxResults(length)
			}
			
			if(columnName == "hour"){
				order ("date", orderDir)
				order ("hour", orderDir)
			} else {
				order (columnName, orderDir)
			}
			
			
		} // End of listData query
	}
}
