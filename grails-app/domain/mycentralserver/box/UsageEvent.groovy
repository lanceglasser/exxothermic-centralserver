package mycentralserver.box

import java.util.Date;

import org.apache.commons.lang.StringUtils;

import mycentralserver.company.CompanyLocation;
import mycentralserver.exception.UsageEventParsingException;
import mycentralserver.utils.Constants;
import mycentralserver.utils.enumeration.VenueServerEvent;

/**
 * This domain map the information of the usage events retrieved from the venue servers
 * 
 * @author sdiaz
 * @since 11/12/2015
 */
class UsageEvent {

    String serialNumber;
    String deviceId;
    int eventType;
    int currentStreamings = 0;
    long eventStart = 0;
    long eventStop = 0;
    long eventDuration = 0;
    String channelPort = "";
    String channelLabel = "";

    Date dateCreated;
    Date lastUpdated;

    static belongsTo= [location:CompanyLocation]

    static constraints = {
    }

    static mapping = { 
        version false
        autoTimestamp true
    }

    static namedQueries = {
    }

    /**
     * Tries to read the information of a UsageEvent and return the object with the information
     * 
     * @param data
     *          Array of strings from 1 of the lines on the files
     * @param box
     *          VenueServer that is processing the file
     * @return UsageEvent object with all the information received and validated from the array
     * @throws UsageEventParsingException When any of the data validations failed with the message of the error
     */
    public static UsageEvent getEvent(String[] data, Box box) throws UsageEventParsingException {
        try {
            VenueServerEvent lineEvent = VenueServerEvent.nameOf(data[0]);
            if(lineEvent) {
                if(lineEvent.getNumberOfFields() == data.length) {
                    // Validate common info
                    // Validates that the serial number on the record is the same of the venue server
                    if(!box.serial.equals(data[1])) {
                        throw new Exception("Wrong serial number");
                    }
                    // Validates the device id
                    if(StringUtils.isBlank(data[2])) {
                        // If the device id is empty we will handle the record as the same device for all of the kind
                        data[2] = Constants.USAGE_REPORT_DEFAULT_DEVICE_ID;
                    }
                    final long eventStart = validateLongValue(Constants.USAGE_REPORT_FIELD_START_EVENT, data[3]);
                    switch(lineEvent) {
                        case VenueServerEvent.CONNECTION:
                            UsageEvent event = getConnectionEvent(box, data[2], eventStart);
                            break;
                        case VenueServerEvent.STREAMING:
                            final long eventStop = validateLongValue(Constants.USAGE_REPORT_FIELD_STOP_EVENT, data[4]);
                            final long eventDuration = validateLongValue(Constants.USAGE_REPORT_FIELD_DURATION, data[5]);
                            final int currentStreamings = validateIntValue(Constants.USAGE_REPORT_FIELD_STREAMINGS, data[8]);
                            if(StringUtils.isBlank(data[6])) {
                                throw new UsageEventParsingException("The port is required");
                            }
                            UsageEvent event = getStreamingEvent(box, data[2], eventStart, eventStop,
                                eventDuration, data[6], data[7], currentStreamings);
                            break;
                        default:
                            throw new UsageEventParsingException("The parse of the event is not implemented");
                    }
                } else {
                    throw new UsageEventParsingException("The number of fields for this event is incorrect");
                }
            } else {
                throw new UsageEventParsingException("Unknown event");
            }
        } catch (UsageEventParsingException e) {
            throw e;
        } catch(Exception e) {
            // We catch the generic exception for unexpected error from grails runtime processes
            throw new UsageEventParsingException("Unexpected error: " + e.getMessage());
        }
    }

    /**
     * Gets an UsageEvent object with the information of the Connection event only 
     * 
     * @param box
     *          VenueServer that is processing the file
     * @param deviceId
     *          Id of the device that generates the event
     * @param eventStart
     *          Start time value of the event
     * @return UsageEvent object with the information of the Connection event only
     */
    private static UsageEvent getConnectionEvent(final Box box, final String deviceId, final long eventStart) {
        UsageEvent event = getCommonEvent(box, deviceId);
        event.eventType = VenueServerEvent.CONNECTION.getValue();
        event.eventStart = eventStart;
        return event;
    }

    /**
     * Gets an UsageEvent object with the information of the Streaming event only
     *
     * @param box
     *          VenueServer that is processing the file
     * @param deviceId
     *          Id of the device that generates the event
     * @param eventStart
     *          Start time value of the streaming
     * @param eventStop
     *          Stop time value of the streaming
     * @param eventDuration
     *          Duration in seconds of the streaming
     * @param channelPort
     *          String with the port of the channel that is streaming
     * @param label
     *          Current label of the channel that is streaming
     * @param streamings
     *          Current number of streaming that the box is doing when the event starts
     * @return UsageEvent object with the information of the Streaming event
     */
    private static UsageEvent getStreamingEvent(final Box box, final String deviceId, final long eventStart,
        final long eventStop, final long eventDuration, final String port, final String label, final int streamings) {
        UsageEvent event = getCommonEvent(box, deviceId);
        event.eventType = VenueServerEvent.STREAMING.getValue();
        event.eventStart = eventStart;
        event.eventStop = eventStop;
        event.eventDuration = eventDuration;
        event.channelPort = port;
        event.channelLabel = label;
        event.currentStreamings = streamings;
        return event;
    }

    /**
     * Gets an UsageEvent object with the common information between all the events
     * 
     * @param box
     *          VenueServer that is processing the file
     * @param deviceId
     *          Id of the device that generates the event
     * @return UsageEvent object with common information
     */
    private static UsageEvent getCommonEvent(final Box box, final String deviceId) {
        UsageEvent event = new UsageEvent();
        event.serialNumber = box.serial;
        event.deviceId = deviceId;
        event.dateCreated = new Date();
        event.location = box.location;
        return event;
    }

    /**
     * Validates that a String is a valid long and returns the long object
     * 
     * @param fieldName
     *          Name of the field for the error message
     * @param value
     *          Value to validate
     * @return Converted Long from the String when is valid
     */
    private static long validateLongValue(final String fieldName, final String value) {
        try {
            final long longValue = Long.parseLong(value.trim());
            if (longValue > 0) {
                return longValue;
            }
            throw new UsageEventParsingException("The " + fieldName + " value must be higher than 0");
        } catch(NumberFormatException e) {
            throw new UsageEventParsingException("The " + fieldName + " value is invalid");
        }
    }
    
    /**
     * Validates that a String is a valid Integer and returns the int object
     *
     * @param fieldName
     *          Name of the field for the error message
     * @param value
     *          Value to validate
     * @return Converted Integer from the String when is valid
     */
    private static long validateIntValue(final String fieldName, final String value) {
        try {
            final int intValue = Integer.parseInt(value.trim());
            if (intValue > 0) {
                return intValue;
            }
            throw new UsageEventParsingException("The " + fieldName + " value must be higher than 0");
        } catch(NumberFormatException e) {
            throw new UsageEventParsingException("The " + fieldName + " value is invalid");
        }
    }
}