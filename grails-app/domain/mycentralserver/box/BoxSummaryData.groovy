/**
 * BoxData.groovy
 */
package mycentralserver.box

import java.util.Date;

import mycentralserver.company.CompanyLocation

import org.hibernate.criterion.CriteriaSpecification;

/**
 * Domain class that store the usage report information of a ExXtractor
 * 
 * @author Cecropia Solutions
 *
 */
class BoxSummaryData {

	//DAY_OF_YEAR/DATE/PORT/HOUR/NAME/TRCK_TIME/TOTAL_SESSIONS/MAX_SESSIONS
	
	String 	day; // day of the year 1-366
	String 	summaryType; // HourlyClients, DailyClients
	int 	hour = 0; // hour of the day,
	int 	reportedSessions = 0;	
	double	time = 0;
	Date 	date;
	
	Date 	dateCreated;
	Date 	lastUpdated;
	
	String serial;
	String boxName;
	
	
	static belongsTo= [location:CompanyLocation]
	
    static constraints = {
		hour(unique: ['location','serial', 'hour', 'date', 'summaryType']) //Unique key
		day 		nullable:true		
    }
	
	static mapping = {
		autoTimestamp true
	}
	
	static namedQueries = {
		
		// Query that filter the data
		listHourClientsData { start, length, columnName, orderDir, searchText, locationId, startDate, endDate, locations ->

			//createAlias("box", "b")
			createAlias("location", "l")
			//createAlias("l.boxes", "b", CriteriaSpecification.LEFT_JOIN)

			eq("summaryType","HourlyClients")
			ge("date", startDate)
			le("date", endDate)
			
			// When is filtered by Location
			if(locationId != 0){
				eq("l.id", locationId)
			} else {
				if(locations){
					//Only applies when there is a restringed list of locations
					'in'(
						'l.id', locations.collect {loc ->
						  return loc.id;
						}
					)
				}
			}
			
			// When is using the filter
			if( searchText != ""){
				or {
					like("l.name", "%" + searchText + "%")
					like("boxName", "%" + searchText + "%")
					like("serial", "%" + searchText + "%")
				}
			}
			
			firstResult(start)
			
			if(length != 0){
				maxResults(length)
			}
			
			if(columnName == "hour"){
				order ("date", orderDir)
				order ("hour", orderDir)
			} else {
				order (columnName, orderDir)
			}
			
			
		} // End of listData query
		listDailyClientsData { start, length, columnName, orderDir, searchText, locationId, startDate, endDate, locations ->
			
						//createAlias("box", "b")
						createAlias("location", "l")
						//createAlias("l.boxes", "b", CriteriaSpecification.LEFT_JOIN)
			
						eq("summaryType","DailyClients")
						ge("date", startDate)
						le("date", endDate)
						
						// When is filtered by Location
						if(locationId != 0){
							eq("l.id", locationId)
						} else {
							if(locations){
								//Only applies when there is a restringed list of locations
								'in'(
									'l.id', locations.collect {loc ->
									  return loc.id;
									}
								)
							}
						}
						
						// When is using the filter
						if( searchText != ""){
							or {
								like("l.name", "%" + searchText + "%")
								like("boxName", "%" + searchText + "%")
								like("serial", "%" + searchText + "%")
							}
						}
						
						firstResult(start)
						
						if(length != 0){
							maxResults(length)
						}
						
						order (columnName, orderDir)
						
						
						
					} // End of listData query
		
		listHourClientsChartData { start, length, columnName, orderDir, searchText, locationId, startDate, endDate, locations ->
			
						//createAlias("box", "b")
						createAlias("location", "l")
						//createAlias("l.boxes", "b", CriteriaSpecification.LEFT_JOIN)
						
						projections {
							groupProperty 'l.id'
							groupProperty 'date'
							groupProperty 'hour'
							property("l.name")
							sum('reportedSessions')
						}
						
						eq("summaryType","HourlyClients")
						ge("date", startDate)
						le("date", endDate)
						
						// When is filtered by Location
						if(locationId != 0){
							eq("l.id", locationId)
						} else {
							if(locations){
								//Only applies when there is a restringed list of locations
								'in'(
									'l.id', locations.collect {loc ->
									  return loc.id;
									}
								)
							}
						}
						
						// When is using the filter
						if( searchText != ""){
							or {
								like("l.name", "%" + searchText + "%")
								like("boxName", "%" + searchText + "%")
								like("serial", "%" + searchText + "%")
							}
						}
						
						firstResult(start)
						
						if(length != 0){
							maxResults(length)
						}
						
						if(columnName == "hour"){
							order ("date", orderDir)
							order ("hour", orderDir)
						} else {
							order (columnName, orderDir)
						}
						
						
					} // End of listData query
		
			listDailyClientsChartData { start, length, columnName, orderDir, searchText, locationId, startDate, endDate, locations ->
			
						//createAlias("box", "b")
						createAlias("location", "l")
						//createAlias("l.boxes", "b", CriteriaSpecification.LEFT_JOIN)
			
						projections {
							groupProperty 'l.id'
							groupProperty 'date'
							property("l.name")
							sum('reportedSessions')
						}
						
						eq("summaryType","DailyClients")
						ge("date", startDate)
						le("date", endDate)
						
						// When is filtered by Location
						if(locationId != 0){
							eq("l.id", locationId)
						} else {
							if(locations){
								//Only applies when there is a restringed list of locations
								'in'(
									'l.id', locations.collect {loc ->
									  return loc.id;
									}
								)
							}
						}
						
						// When is using the filter
						if( searchText != ""){
							or {
								like("l.name", "%" + searchText + "%")
								like("boxName", "%" + searchText + "%")
								like("serial", "%" + searchText + "%")
							}
						}
						
						firstResult(start)
						
						if(length != 0){
							maxResults(length)
						}
						
						order (columnName, orderDir)
						
						
						
					} // End of listData query
	}
}
