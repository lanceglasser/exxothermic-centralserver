package mycentralserver.box

import java.io.Serializable;

import mycentralserver.utils.Constants;

class BoxIp implements Serializable{
	String serial
	String ipAddress
	String macAddress
	Date lastUpdated
	Date dateCreated

	static constraints = {
		serial 	blank:false,maxLength:40,  matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
		ipAddress 	blank:false,maxLength:40, matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
		macAddress 	blank:false,maxLength:40, matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
	}

	static mapping = {
		version false
		autoTimestamp true
	}
	
	
	static namedQueries = {
		getBySerialDistinctMacAddress { serial ->
			and{
				eq("serial", serial)														
			}			
			projections {
				distinct("macAddress")
			}
		}
		
		getBoxLastIpConnected { serial ->
			and{
				eq("serial", serial)
			}
			maxResults(1)
			order("dateCreated", "desc")
		}
	} 	
		
}
