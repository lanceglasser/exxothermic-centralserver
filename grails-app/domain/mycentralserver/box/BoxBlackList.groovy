package mycentralserver.box

import java.util.Date;

import mycentralserver.utils.Constants;

class BoxBlackList {
	String serial
	String reason = ""
	Date dateCreated
	Date dateStopWorking
	Date lastUpdated

	static constraints = {
		serial 	unique:true,blank:false,maxLength:40,  matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
		reason	nullable:true
	}

	static mapping = {
		version false
		autoTimestamp true
	}
}
