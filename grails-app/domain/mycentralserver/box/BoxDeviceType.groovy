package mycentralserver.box

import java.io.Serializable;
import java.util.Date;

import mycentralserver.generaldomains.Catalog;
import mycentralserver.user.User;
import mycentralserver.utils.Constants;

class BoxDeviceType implements Serializable {
	String name
	String machineModel
	Date dateCreated
	Catalog machineBrand
	Catalog processorBrand
	Catalog memory
	Catalog memoryType
	User createdBy
	
    static constraints = {
		name blank:false,maxLength:40,  matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
		machineModel blank:false,maxLength:40,  matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
		machineBrand nullable:true
		processorBrand nullable:true
		memory nullable:true
		memoryType nullable:true
    }
	
	static mapping = {
		autoTimestamp true
	}

}
