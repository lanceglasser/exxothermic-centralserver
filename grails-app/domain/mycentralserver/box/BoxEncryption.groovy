package mycentralserver.box

import java.io.Serializable;
import java.util.Date;

import mycentralserver.utils.Constants;

class BoxEncryption implements Serializable{
	String serial;
	String publicKeyFile;
	boolean enable = true
	Date dateCreated
	Date lastUpdated
	
	static belongsTo= []
	static hasMany=[]
	//
	static optionals =[]
	
	static mapping = {		
		autoTimestamp true
	}
	
    static constraints = {
		serial 			blank:false, unique:true, nullable:false, maxLength:40,  matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
		publicKeyFile   blank:false, nullable:false, maxLength:150
    }	
}
