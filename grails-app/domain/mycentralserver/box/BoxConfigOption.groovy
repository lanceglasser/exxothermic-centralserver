package mycentralserver.box

import java.util.Date;
import java.util.List;

import mycentralserver.company.CompanyLocation
import mycentralserver.user.User
import mycentralserver.utils.Constants;
import mycentralserver.utils.EncrypterHelper;

import org.hibernate.criterion.CriteriaSpecification
 
import org.codehaus.groovy.grails.orm.hibernate.cfg.IdentityEnumType

class BoxConfigOption {
		
	/**
	 * Value of the Option
	 */
	String value = "";
	
	/**
	 * Label code for translation
	 */
	String labelCode = "";
		
	BoxConfig boxConfig;
	
	static belongsTo = [boxConfig:BoxConfig]
	
    static constraints = {
		value 			nullable:false
		labelCode		nullable:false
		version: false
    }
	
	//Mapping with database table
	static mapping= {
		table "box_config_options"
	}
		
	static namedQueries = {		
	}
	 
	String toString(){
		return this.labelEn;
	}
}
