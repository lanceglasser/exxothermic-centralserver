package mycentralserver.box

import java.util.Date;

import org.apache.commons.lang.StringEscapeUtils;

import mycentralserver.utils.Constants;

/**
 * Domain definition of the Box Channel Table
 * 
 * @author Cecropia Solutions
 * @since 07/08/2013
 */
class BoxChannel implements Serializable{

    int channelNumber
    String channelLabel //title
    String description
    String subTitle
    String name
    String codec
    String channelPort
    String imageURL
    String largeImageURL

    boolean isPa;
    boolean enabled;
    boolean enabledForApp = true; // Specified if the Channel is shown or not in the App
    int rmsVoltage;
    int currentSessions;
    int gain = 0; // Set the Buster Level of the Channel used by the Box
    int delay = 0; // Configured delay in milliseconds of the channel
    Date dateCreated;
    Date lastUpdated;

    static belongsTo= [box:Box]
    static hasMany=[] //boxes:Box, customButtons:CustomButton
    static optionals =[]

    static mapping = {		 autoTimestamp true		 }

    static constraints = {
        channelNumber   min:0
        channelLabel    blank:false, nullable:false, maxLength:14
        codec           blank:false, nullable:false, maxLength:50
        rmsVoltage      min:0
        currentSessions min:0
        gain            min:0
        delay           min:0, max: 3000
        channelPort     nullable:true
        imageURL        nullable:true
        description     nullable:true, maxLength:100
        name            nullable:true, maxLength:10
        subTitle        nullable:true, maxLength:20
        largeImageURL   nullable:true
    }

    static namedQueries = {
        getButtonByBox { box1, channel ->
            eq ("box",box1)
            eq ("channelNumber",channel)
            uniqueResult = true

        }
    }

    static boolean unlink(id, box) {
        def e = BoxChannel.get(id)
        if (e) {
            box?.removeFromChannels(e)
            e.delete()
            return true
        }
        return false
    }

    String getChannelLabel() {
        return  StringEscapeUtils.unescapeJava(channelLabel);
    }

    String getChannelDescription() {
        return  StringEscapeUtils.unescapeJava(description);
    }
}