package mycentralserver.box


import java.io.Serializable;
import java.util.Date;

import mycentralserver.utils.Constants;

class BoxUnregistered implements Serializable{
	
	String serial
	String versionSoftwareLabel
	String certificateLabel;
	boolean hasCertificate = false;
	Date date
	Date lastUpdated
	
    static constraints = {
		serial blank:false,unique:true,maxLength:40 ,  matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
		versionSoftwareLabel nullable:true,maxLength:50,  matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
		certificateLabel   nullable:true,maxLength:50
    }
	
	static mapping = {
		version false
		autoTimestamp true
	}
	
	
	boolean connected(){
		Date start = lastUpdated
		Date end = new Date()
	
		long diffInSeconds = (end.getTime() - start.getTime()) / 1000
		/* sec */
		def sec =  (diffInSeconds >= 60 ? diffInSeconds % 60 : diffInSeconds)
		/* min */
		def min =  (diffInSeconds = (diffInSeconds / 60)) >= 60 ? diffInSeconds % 60 : diffInSeconds
		/* hours */
		def hours =  (diffInSeconds = (diffInSeconds / 60)) >= 24 ? diffInSeconds % 24 : diffInSeconds
		/* days */
		def days =  (diffInSeconds = (diffInSeconds / 24))
		
		
		
		if ( days == 0 && hours == 0 && min < 5 ){
			return true
		}else{
			return false
		}
	}
	
	String lastConnected(String messageFormat) {
		
		Date start = lastUpdated
		Date end = new Date()
	
	
		long diffInSeconds = (end.getTime() - start.getTime()) / 1000
		long [] diff  =   [0,0,0,0]
		
		/* sec */
		diff[3] =  (diffInSeconds >= 60 ? diffInSeconds % 60 : diffInSeconds)
		/* min */
		diff[2] =  (diffInSeconds = (diffInSeconds / 60)) >= 60 ? diffInSeconds % 60 : diffInSeconds
		/* hours */
		diff[1] =  (diffInSeconds = (diffInSeconds / 60)) >= 24 ? diffInSeconds % 24 : diffInSeconds
		/* days */
		diff[0] =  (diffInSeconds = (diffInSeconds / 24))
		
				
		return  (String.format(
			messageFormat,
			diff[0],
			diff[1],
			diff[2],
			diff[3]))	
	}
	
}
