package mycentralserver.box

import java.util.Date;
import java.util.List;

import mycentralserver.company.CompanyLocation
import mycentralserver.user.User
import mycentralserver.utils.Constants;
import mycentralserver.utils.EncrypterHelper;

import org.hibernate.criterion.CriteriaSpecification
 
import org.codehaus.groovy.grails.orm.hibernate.cfg.IdentityEnumType

class BoxConfigValue {
		
	/**
	 * Value of the Configuration
	 */
	String value = "";
	
	/**
	 * Related Box
	 */
	Box box;
	
	/**
	 * Related Configuration
	 */
	BoxConfig boxConfig;
			
	static belongsTo = [box:Box, boxConfig: BoxConfig]
	
    static constraints = {
		value	nullable:false
    }
	
	//Mapping with database table
	static mapping= {
		table "box_config_values"
	}
		
	static namedQueries = {		
	}
	 
	String toString(){
		return this.value;
	}
}
