package mycentralserver.box

import java.util.Date;

class BoxCertificate {

	String 	type; 				  //x509
	String  certificateVersion; //v3
	String 	description;		  //certification type x.509 version 3
	String 	encryption; 		  //MD5WithRsa
	String 	label; 				  //x509v3
	Date 	dateCreated
	Date 	lastUpdated
	
	static belongsTo= []
	static hasMany=[]
	//
	static optionals =[]
	
	static mapping = {
		autoTimestamp true
	}
	
    static constraints = {
		type   			blank:false, nullable:false, maxLength:50
		certificateVersion blank:false, nullable:false, maxLength:20
		description   	blank:true, nullable:true, maxLength:150
		encryption   	blank:false, nullable:false, maxLength:50
		label   		blank:false, nullable:false, maxLength:50
    }
}
