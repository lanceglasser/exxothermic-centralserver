package mycentralserver.box

import mycentralserver.utils.Constants;
/*
 * 0-Failed ( Problem in the communication with the device, service not found,host unknown etc)
 * 1-Process
 * 2-Success
 * 3-Error ( Timeout, another error in the backend )
 * */
class BoxStatus implements Serializable{
	String description;
	int id;
    static constraints = {
		description blank:false,   matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
    }

	static mapping={
		table "box_status";
		id description: 'description'
		version false
		id generator: 'assigned'
	}
	
	String toString()
	{
		description;
	}
}



