package mycentralserver.user

import java.io.Serializable;

class Role implements Serializable{

	String authority
	String description 
	
	static mapping = {
		cache false
	}

	static constraints = {
		authority blank: false, unique: true
	}
}
