/**
 * UserDashboard.groovy
 */
package mycentralserver.user;

/**
 * Domain class for the definition of the UserDashboard
 * information; this table contains the configuration of the
 * different system dashboards for a single User
 * 
 * @author Cecropia Solutions
 */
class UserDashboard {

	/**
	 * Configuration of the main dashboard of the system
	 */
	String userDashboard = '{}';
	
	/**
	 * Configuration of the company dashboard view
	 */
	String companyDashboard = '{}';
	
	/**
	 * Configuration of the location dashboard view
	 */
	String locationDashboard = '{}';
	
	/**
	 * List of visible shortcuts for the shortcuts widget at the Main Dashboard
	 */
	String userShortcuts = '';
	
	/**
	 * List of visible shortcuts for the shortcuts widget at the Company Dashboard
	 */
	String companyShortcuts = '';
	
	/**
	 * List of visible shortcuts for the shortcuts widget at the Location Dashboard
	 */
	String locationShortcuts = '';
	
	/**
	 * Related User
	 */
	User user;
	
	static mapping = {
		cache false
		table "user_dashboards"
	}
	
    static constraints = {
		version: false
		userDashboard			size:0..1024
		companyDashboard		size:0..1024
		locationDashboard		size:0..1024
		userShortcuts			size:0..1024
		companyShortcuts		size:0..1024
		locationShortcuts		size:0..1024
    }
}
