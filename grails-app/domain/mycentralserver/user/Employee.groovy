package mycentralserver.user

import java.io.Serializable;
import java.util.Date;
import org.hibernate.criterion.CriteriaSpecification;

import mycentralserver.company.Company;
import mycentralserver.user.User;

class Employee implements Serializable {

	User user
	Company company
	boolean enabled = false
	Date dateCreated
	Date lastUpdated
		
	static belongsTo = [User, Company]
	
	static mapping = {
		cache false			
		autoTimestamp true
		table "user_employeers"
	}
	
	static constraints = {
		 
		user(unique:['company'])
	}

	static namedQueries = {
		// Query that filter the data
		listAllEmployeesForUITable { start, length, columnName, orderDir, searchText, user, companyId ->
			createAlias("company", "c", CriteriaSpecification.LEFT_JOIN)
			createAlias("user", "u", CriteriaSpecification.LEFT_JOIN)
			
			// When is using the filter
			if( searchText != ""){
				or {
					like("u.email", "%" + searchText + "%")
					like("u.firstName", "%" + searchText + "%")
					like("u.lastName", "%" + searchText + "%")
				}
			}
			
			// If must find only the enable companies
			if(companyId != 0){
				eq("c.id", companyId)
			}
						
			firstResult(start)
			
			if(length != 0){
				maxResults(length)
			}
			
			order (columnName, orderDir)
		} // End of listData query
	} // End of namedQueries
	
	static Employee link(user, company) {
		 def e = Employee.findByUserAndCompany(user, company) 
		 if (!e) { 
			 e = new Employee()
			 e.user = user
			 e.company = company
			 e.enabled = true			 
			 if( e.save() ){ 
				 user?.addToJobs(e)
				 company?.addToEmployees(e)
			 }else{
			 	e = null
			 }
	     } 
		 return e
   }
	
	static boolean unlink(user, company) { 
		def e = Employee.findByUserAndCompany(user, company) 
		if (e) {			 
			user?.removeFromJobs(e) 
			company?.removeFromEmployees(e)
			e.delete()
			return true				
		} 
		return false
	}
	
	
	
}
