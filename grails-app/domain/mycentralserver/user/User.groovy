package mycentralserver.user

import groovy.time.TimeCategory;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.hibernate.criterion.CriteriaSpecification;

import mycentralserver.company.Company
import mycentralserver.content.Content
import mycentralserver.docs.Document;
import mycentralserver.utils.Constants;
import mycentralserver.app.AppSkin


class User implements Serializable{
	transient springSecurityService
	private static final Date NULL_DATE = new Date(0)

	String email

	String password
	boolean enabled
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired
	Date    passwordExpirationDate =NULL_DATE
	String firstName
	String lastName
	String phoneNumber

	User lastUpdatedBy
	User createdBy
	Date dateCreated
	Date lastUpdated

	String resetPasswordToken
	Date resetPasswordTime

	static transients = [ 'firstNameAndLastName' ]


	static hasMany= [roles:UserRole, companies:Company, jobs:Employee, created:Company, createdContents:Content, createdAppSkins: AppSkin, createdDocuments: Document]

	// need to disambiguate the multiple hasMany references with the
	// 'mappedBy' property:

	static mappedBy =   [jobs: "user",
						roles: "user",
						companies: "owner",
						created: "createdBy",
						createdCustomButtons: "createdBy",
						createdAppSkins: "createdBy"]

	static constraints = {
		email 				blank: false, maxSize:150, unique: true, email:true
		password 			blank: false, maxSize:100, matches: Constants.REGULAR_EXPRESSION_FOR_PASSWORD_FIELD
		firstName 			blank:false, maxSize:50, matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
		lastName 			blank:false, maxSize:50, matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
		phoneNumber 		nullable: true, maxSize:35 ,matches: Constants.REGULAR_EXPRESSION_FOR_PHONE_NUMBER
		lastUpdatedBy 		nullable: true
		createdBy 			nullable: true
		resetPasswordToken 	nullable: true
		resetPasswordTime 	nullable: true
		passwordExpirationDate defaultValue:new Date()
	}

	static mapping = {
		password column: '`password`'
		companies sort: "name"
		autoTimestamp true
		jobs cascade: "all-delete-orphan"
	}
	
	static namedQueries = {
		
		// Query that filter the data
		listAllEmployeesNotAssign { start, length, columnName, orderDir, searchText, staffRoles, user ->
						
			createAlias("jobs", "j", CriteriaSpecification.LEFT_JOIN)
			
			
			roles {
				'in' ("role", staffRoles)
			}
			
			// When is using the filter
			if( searchText != ""){
				or {
					like("email", "%" + searchText + "%")
					like("firstName", "%" + searchText + "%")
					like("lastName", "%" + searchText + "%")
				}
			}
						
			// If must find only the associated to a User
			if(user){
				eq("createdBy", user)
			} // End of if user
			
			firstResult(start)
			
			if(length != 0){
				maxResults(length)
			}
			
			order (columnName, orderDir)
		} // End of listAllEmployeesNotAssign query	.
		
		listAllOwners { staffRoles ->
			roles {
				'in' ("role", staffRoles)
			}			
			order ('firstName', 'desc')
		}
	}

	Set<Role> getAuthorities() {
		UserRole.findAllByUser(this).collect { it.role } as Set
	}

	def beforeInsert() {
		encodePassword()

		if (passwordExpirationDate == NULL_DATE) {
			use(TimeCategory) {
				passwordExpirationDate = new Date() + 60.days
			}
		 }
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}

	}

	def setNewPassword(String newPassword){
		password = newPassword
		if (newPassword != null && !newPassword.isEmpty()){
			encodePassword()
		}
	}

	protected void encodePassword() {
		password = springSecurityService.encodePassword(password)
	}

	List employeers(){

		return jobs.collect{it.company}

	}

	boolean addToEmployeers(Company company) {
		return Employee.link(this , company)
	}

	boolean removeFromEmployeers(Company company) {
		return Employee.unlink(this, company)
	}

	String toString()
	{
		firstName + "  " + lastName;
	}

	String getFirstNameAndLastName() {
		firstName + "  " + lastName;
	}
}
