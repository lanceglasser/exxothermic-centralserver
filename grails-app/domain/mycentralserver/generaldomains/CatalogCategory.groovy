package mycentralserver.generaldomains

import java.util.Date;

import mycentralserver.user.User;
import mycentralserver.utils.Constants;

class CatalogCategory implements Serializable{

	String name
	Date dateCreated

	//Relationship
	static belongsTo= [createdBy:User]
	static constraints = {
		name blank:false,maxLength:40,  matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
	}

	static mapping = { autoTimestamp true }
	
}
