package mycentralserver.generaldomains

import java.io.Serializable;
import java.util.Date;

class CompanyType implements Serializable{

    
	 String name
	 Date dateCreated
	 Date lastUpdated
	  
	 static mapping = {				
			autoTimestamp true			
	 }
	 
	 static constraints = {				 
		 name blank: false, unique: true
	 }
	 
	 String toString()
	 {
		name;
	 }

}
