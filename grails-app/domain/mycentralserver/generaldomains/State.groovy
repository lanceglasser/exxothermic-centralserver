package mycentralserver.generaldomains

import java.io.Serializable;

class State implements Serializable{
	int id
	String name
		
	//Relationship
	static belongsTo =[country:Country]
    static constraints = {
		name blank:false, unique: true
		id id: 'id'
		version: false
		id generator: 'assigned'		
    }
	
	static mapping = {
		sort "name"
	}
	
	String toString()
	{
		return name;
	}
	
}
