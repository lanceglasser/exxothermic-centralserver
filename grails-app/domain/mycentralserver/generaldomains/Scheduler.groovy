package mycentralserver.generaldomains

import java.io.Serializable;
import java.util.Date;
import org.hibernate.criterion.CriteriaSpecification;
import mycentralserver.user.User;
import mycentralserver.utils.Constants;
import mycentralserver.content.Content;

class Scheduler implements Serializable{
	Date startDate
	Date endDate
	String days = "0,1,2,3,4,5,6"
	String hours
	Date dateCreated
	Date lastUpdated
	
	static belongsTo=[contents:Content]

	static constraints = {
		startDate 	nullable:true
		endDate 	nullable:true
		days 		nullable:true
		hours		nullable:true
	}
	

	static mapping = { autoTimestamp true }
	
}