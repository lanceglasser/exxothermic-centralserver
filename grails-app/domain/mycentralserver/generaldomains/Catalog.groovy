package mycentralserver.generaldomains

import java.io.Serializable;
import java.util.Date;

import org.hibernate.criterion.CriteriaSpecification;

import mycentralserver.user.User;
import mycentralserver.utils.Constants;

class Catalog implements Serializable{
	String value
	Date dateCreated	

	//Relationship
	static belongsTo= [catalogCategory:CatalogCategory,createdBy:User]

	static constraints = {
		value blank:false,maxLength:40,  matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
	}

	static mapping = { autoTimestamp true }
	
}
