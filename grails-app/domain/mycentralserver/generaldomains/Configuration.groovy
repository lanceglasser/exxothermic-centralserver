/**
 * Configuration.groovy
 */
package mycentralserver.generaldomains;

import mycentralserver.utils.Constants;

/**
 * Domain class to store the default configuration values of the Application
 *  
 * @author Cecropia Solutions
 *
 */
class Configuration implements Serializable {
	
	/**
	 * Unique code of the configuration record
	 */
	String code;
	
	/**
	 * Description of the configuration record
	 */
	String description;
	
	/**
	 * Value of the configuration record
	 */
	String value;
	
	/**
	 * When true it means that the configuration is required by the servers,
	 * a must keep it synchronized
	 */
	boolean boxConfig = false;
	
	/**
	 * This is use for the render to know how
	 * to show the configuration form for the edition
	 * 0: TextArea for single test, 1: List 
	 */
	int type = Constants.CONFIGURATION_TYPE_TEXT;
	
	static mapping = {
		cache true
		table "configurations"
	}
	
	static constraints = {
		code(blank:false)
		version: false
		code			size:0..100
		description		size:0..255
		value			size:0..10000
	}
		
	String toString()
	{
		code;
	}
	
} // End of Class