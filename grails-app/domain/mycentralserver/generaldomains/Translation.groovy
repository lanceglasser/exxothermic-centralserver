package mycentralserver.generaldomains

class Translation implements Serializable{
	
	String code;
	String locale;
	String value;
	
	static constraints = {
		code(blank:false)
		version: false
	}
	
	
	String toString(){
		value;
	}
	
}
