package mycentralserver.generaldomains

class Country implements Serializable{
	int id
	String name;
	String shortName;
	String code; //Used by the Google Geolocation Functions
	boolean enabled = true;	
	//Relationship
	static hasMany=[states:State];
	
	
	static constraints = {
		name(blank:false)
		id id: 'id'
		version: false
		id generator: 'assigned'
	}
	
	
	String toString()
	{
		shortName;
	}

	def boolean hasStates(){
		if (states.size() > 0){
			return true
		}else{
			return false
		}
	}
}
