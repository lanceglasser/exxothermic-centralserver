package mycentralserver.event

import java.io.Serializable;
import java.util.Date;

import org.hibernate.criterion.CriteriaSpecification;

import mycentralserver.user.User;
import mycentralserver.utils.Constants;

class Event_Box_Status_Log implements Serializable{
	String status
	Date dateCreated
	int boxDisconnected
	int totalBoxes

	

	static constraints = {
		status blank:false,maxLength:10
	}

	static mapping = { autoTimestamp true }
	
}
