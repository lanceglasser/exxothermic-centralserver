/**
 * Affiliate.groovy
 */
package mycentralserver.app

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import mycentralserver.company.CompanyLocation
import mycentralserver.content.Content;
import mycentralserver.user.User

import org.hibernate.criterion.CriteriaSpecification
 
/**
 * Domain Class of the Affiliates of Partners of the System,
 * this domain includes the email signature, app download urls between
 * others.
 * 
 * @author Cecropia Solutions
 *
 */
class Affiliate implements Serializable{
	
	/**
	 * Name of the Affiliate
	 */
	String name
		
	/**
	 * Domain of the URL to be used in order to recognize the Affiliate 
	 */
	String 	domain
	
	/**
	 * Enabled or disabled status of affiliate
	 */
	boolean enabled = false

	/**
	 * CSS folder name of the associated Theme
	 */
	String 	theme
	
	/**
	 * List of type of estrablishments for this Affiliate
	 */
	String associateEstablishments = "";
	
	/**
	 * Affiliate code; i.e: MYE or EXX
	 */
	String code;
	
	/**
	 * This value is used for the emails as the client signature
	 */
	String signature;
	
	/**
	 * This value is used for the emails as the from email
	 */
	String emailContact;
	
	/**
	 * Default username for the Box use by the manufacturing script
	 */
	String boxUsername = "Tech_User";
	
	/**
	 * Url to download the IOS App for this Affiliate
	 */
	String iosAppUrl = "";
	
	/**
	 * Url to download the Android App for this Affiliate
	 */
	String androidAppUrl = "";
	
	/**
	 * When is True use the ExXothermic Affiliate info for Partner Id
	 * and Apps Urls
	 */
	boolean useDefaultPartnerInfo = false;
	
	Date dateCreated
	Date lastUpdated
	User createdBy
	User lastUpdatedBy
	
	static hasMany= [contents:Content]
		
    static constraints = {
        domain                  nullable:true, maxSize:255
        theme                   nullable:true, maxSize:255
        code                    nullable:true, unique:true, maxSize:255
        boxUsername             nullable:true, maxSize:255
        associateEstablishments nullable:true, maxSize:255
        signature               nullable:true, maxSize:255
        iosAppUrl               nullable:true, maxSize:255
        androidAppUrl           nullable:true, maxSize:255
        name                    unique:true, maxSize:255
        emailContact            nullable:true, maxSize:255
    }
	
	//Mapping with database table
	static mapping= {
		table "app_affiliates"
		autoTimestamp true
		contents cascade: "all-delete-orphan"
	}

	static namedQueries = {
		/**
		 * Must Returns the list of Skins created by the User and
		 * all the Skins assigned to a Location allowed to the User
		 */
		getAffiliateForDomain { domain ->
			ilike("domain", "%" + domain + "%")
		}
	}
		
	String toString()
	{
		name;
	}
}
