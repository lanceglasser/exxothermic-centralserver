package mycentralserver.app

import java.util.Date;
import java.util.List;

import mycentralserver.company.CompanyLocation
import mycentralserver.user.User
import mycentralserver.utils.Constants;

import org.hibernate.criterion.CriteriaSpecification
 
import org.codehaus.groovy.grails.orm.hibernate.cfg.IdentityEnumType

class AppSkin {
		
	/**
	 * Name of the Skin for User reference
	 */
	String name = "";
	
	/**
	 * Hex Main Color
	 */
	String primaryColor = "";
	
	/**
	 * Hex Secondary Color
	 */
	String secondaryColor = "";
	
	//ToDo: ChannelInfoEnabled:bool
	
	/**
	 * Main Title of the App
	 */
	String title = "";
	
	//ToDo: LogoUrl
	
	/**
	 * Background Image for the Ads
	 */
	String backgroundImageUrl = "";
	
	/**
	 * Background Tablet Image for the Ads
	 */
	String backgroundImageUrlTablet = "";
	
	/**
	 * Background for general i.e. Text Banners
	 */
	String dialogImageUrl = "";
	
	/**
	 * Tablets background for general i.e. Text Banners
	 */
	String tabletDialogImageUrl = "";
	
	/**
	 * Sets if the channel must be visible by the App
	 */
	boolean channelInfoEnable = true;
				
	/**
	 * The feature image for tablets is generated from the small image
	 */
	boolean	genFeaturedImgForTablets = false;
	
	/**
	 * The dialog image for tablets is generated from the small image
	 */
	boolean genDialogImgForTablets = false;
	
	Date dateCreated
	Date lastUpdated
	User createdBy
	User lastUpdatedBy
	
	static hasMany=[locations:CompanyLocation]
	
    static constraints = {
		name 						blank:false,minSize:1, maxSize:255
		title 						blank:false,minSize:1, maxSize:20
		primaryColor 				blank:false,minSize:1, maxSize:50
		secondaryColor 				blank:false,minSize:1, maxSize:50
		backgroundImageUrl 			nullable:true
		backgroundImageUrlTablet	nullable:true
		dialogImageUrl				nullable:true
		tabletDialogImageUrl		nullable:true
    }
	
	//Mapping with database table
	static mapping= {
		table "app_skin"
		autoTimestamp true
	}
		
	static namedQueries = {
		/**
		 * Must Returns the list of Skins created by the User and
		 * all the Skins assigned to a Location allowed to the User
		 */
		getSkinsByUser { user, companies ->
			createAlias("locations", "l", CriteriaSpecification.LEFT_JOIN)
			if(companies.size() > 0){
				or {
					and{
						'in'("l.company.id", companies)
					}
					eq("createdBy", user)
				}
			} else {
				eq("createdBy", user)
			}
						
			order("name", "asc")
		}
		
		getDefaultSkin {
			eq("title", Constants.DEFAULT_SKIN_TITLE)
		}
		
		getSkinsByCompanies { companies , user ->
			createAlias("locations", "l", CriteriaSpecification.LEFT_JOIN)
										   
			or {
				and{
					'in'("l.company.id", companies)
					
				}
				eq("createdBy", user)
			}
			order("name", "asc")
		}
		listAllSkinsForUITable { start, length, columnName, orderDir, searchText, user,
			isHelpDesk, isAdmin, companies, locationId, getCount ->
			
			
			projections {
				if(getCount){	// get the count of registers when the dev set this value to true
					countDistinct("id")
				}else{
					distinct(['id','name','title', 'backgroundImageUrl'])
				}
			}
			
			
			createAlias("locations", "l", CriteriaSpecification.LEFT_JOIN)
			
			// When is using the filter
			if( searchText != ""){
				or {
					like("name", "%" + searchText + "%")
					like("title", "%" + searchText + "%")
				}
			}
			
			if(locationId != 0){
				eq("l.id", locationId)				
			}
						
			
			// if not admin or helpdesk, get the data of the current user
			if(!isAdmin && !isHelpDesk){
				or {
					and{
						'in'("l.company.id", companies)						
					}
					eq("createdBy", user)
				}
			}
			
			firstResult(start)
			
			if(length != 0){
				maxResults(length)
			}
			
			order (columnName, orderDir)
		} // End of listData query
	}
 
	public boolean hasLocation (locationInstance){
		if (locations != null){
			return locations.collect().contains(locationInstance)
		}else{
			return false
		}
	} 
	
	
	String toString()
	{
		return "-> AppSkin: " + name;
	}
}
