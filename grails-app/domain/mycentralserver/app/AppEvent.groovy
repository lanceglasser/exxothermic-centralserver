package mycentralserver.app

import java.util.Date;
import java.util.List;

import mycentralserver.company.CompanyLocation
import mycentralserver.user.User
import mycentralserver.app.AppEvent
import mycentralserver.utils.Constants;

import org.hibernate.criterion.CriteriaSpecification
 
import org.codehaus.groovy.grails.orm.hibernate.cfg.IdentityEnumType

class AppEvent {
		
	
	String event = "";	
	CompanyLocation location;
	AppDevice device;
	String details = ""; // maybe this should be a json string with details about each event based on their type			
	Date dateCreated;
	Date lastUpdated;
	
    static constraints = {		
		lastUpdated 			nullable:true
		details 				nullable:true		
		
    }

	
	//Mapping with database table
	static mapping= {
		table "app_event"
		autoTimestamp true
	}

	String toString()
	{
		return "-> AppDevice: " + event;
	}
}
