package mycentralserver.app

import java.util.Date;
import java.util.List;

import mycentralserver.company.CompanyLocation
import mycentralserver.user.User
import mycentralserver.utils.Constants;

import org.hibernate.criterion.CriteriaSpecification
 
import org.codehaus.groovy.grails.orm.hibernate.cfg.IdentityEnumType

class AppDevice {
		
	
	String name = "";
	AppUser appUser;				
	Date dateCreated;
	Date lastUpdated;
	
	static hasMany=[locations:CompanyLocation]
	
    static constraints = {
		name 					nullable:true
		lastUpdated 			nullable:true
		appUser		 			nullable:true
    }

	
	//Mapping with database table
	static mapping= {
		table "app_device"
		autoTimestamp true
	}

	String toString()
	{
		return "-> AppDevice: " + id;
	}
}
