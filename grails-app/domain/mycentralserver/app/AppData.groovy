package mycentralserver.app

import java.util.Date;
import java.util.List;

import mycentralserver.company.CompanyLocation
import mycentralserver.user.User
import mycentralserver.app.AppEvent
import mycentralserver.utils.Constants;

import org.hibernate.criterion.CriteriaSpecification
 
import org.codehaus.groovy.grails.orm.hibernate.cfg.IdentityEnumType

class AppData {
		
	
	String name;	
	String appid;
	String category;	
	Date dateCreated;
	Date lastUpdated;
	
    static constraints = {		
		lastUpdated 			nullable:true
		category 				nullable:true
    }

	
	//Mapping with database table
	static mapping= {
		table "app_data"
		autoTimestamp true
	}

	String toString()
	{
		return "-> AppData: " + name;
	}
}
