package mycentralserver.app

import java.util.Date;

import mycentralserver.company.CompanyLocation;
import mycentralserver.user.User;
import mycentralserver.utils.Constants;

import org.hibernate.criterion.CriteriaSpecification

class WelcomeAd {

	/**
	 * Name of the Welcome just as a Reference.
	 */
	String name = "";
	
	/**
	 * Set the type of welcome message
	 */
	WelcomeType welcomeType = WelcomeType.IMAGE;
	
	/**
	 * Url of the welcome file, could be to an image or to a video; for image must has a resolution of 640x960
	 */
	String smallImageUrl = "";
	
	/**
	 * Url of the second welcome file, only for Image type and must be with a resolution of  640x1136
	 */
	String mediumImageUrl = "";
	
	/**
	 * Url of the third welcome file, only for Image type and must be with a resolution of 1536 � 2048
	 */
	String largeImageUrl = "";
	
	/**
	 * Url of the video welcome file.
	 */
	String videoUrl = "";
	
	/**
	 * Only use when the type is Video; name of the file for UI info only
	 */
	String videoFileName = "";
	
	/**
	 * Sets if the skip option is enabled or not.
	 */
	boolean skipEnable = true;
	
	/**
	 * Minimum time in seconds that the welcome message must be
	 * watched before skip when is enable
	 */
	int skipTime = 10;
	
	/**
	 * The feature image for tablets is generated from the small image
	 */
	boolean	genImgForTablets = true;
	
	Date dateCreated
	Date lastUpdated
	User createdBy
	User lastUpdatedBy
	
    static hasMany=[locations:CompanyLocation]
	
    static constraints = {
		name 					blank:false,minSize:1, maxSize:255		
		smallImageUrl 			nullable:true
		mediumImageUrl 			nullable:true
		largeImageUrl 			nullable:true
		videoUrl 				nullable:true
		videoFileName			nullable:true
        skipTime                min:1, max: 600
    }
	
	//Mapping with database table
	static mapping= {
		table "app_welcome_ads"
		autoTimestamp true
	}
	
	enum WelcomeType {
		IMAGE, VIDEO
	}
	
	static namedQueries = {
		/**
		 * Must Returns the list created by the User and
		 * all the Skins assigned to a Location allowed to the User
		 */
		getFullListByUser { user ->
			eq("createdBy", user)
			order("name", "asc")
		}
		
		getDefaultWelcomeAd {
			eq("name", Constants.DEFAULT_SKIN_TITLE)
		}
		listAllAdsForUITable { start, length, columnName, orderDir, searchText, user,
			isHelpDesk, isAdmin, companies, locationId, getCount ->
			
			
			projections {
				if(getCount){	// get the count of registers when the dev set this value to true
					countDistinct("id")
				}else{
					distinct(['id','name','welcomeType', 'smallImageUrl', 'videoUrl'])
				}
			}
			
			
			createAlias("locations", "l", CriteriaSpecification.LEFT_JOIN)
			
			// When is using the filter
			if( searchText != ""){
				or {
					like("name", "%" + searchText + "%")
					//like("welcomeType.value", "%" + searchText + "%")
				}
			}
			
			if(locationId != 0){
				eq("l.id", locationId)
			}
						
			
			// if not admin or helpdesk, get the data of the current user
			if(!isAdmin && !isHelpDesk){
				or {
					and{
						'in'("l.company.id", companies)
					}
					eq("createdBy", user)
				}
			}
			
			firstResult(start)
			
			if(length != 0){
				maxResults(length)
			}
			
			order (columnName, orderDir)
		} // End of listData query
		
		
	}
 
	public boolean hasLocation (locationInstance){
		if (locations != null){
			return locations.collect().contains(locationInstance)
		}else{
			return false
		}
	}
	
	
	String toString()
	{
		return "-> Welcome Ad: " + name;
	}
}
