package mycentralserver.custombuttons

import java.util.Date;

import org.codehaus.groovy.grails.validation.MinSizeConstraint

import mycentralserver.company.Company
import mycentralserver.company.CompanyLocation
import mycentralserver.user.User
import mycentralserver.utils.Constants;
import mycentralserver.generaldomains.Scheduler
import org.hibernate.criterion.CriteriaSpecification
import mycentralserver.utils.CustomButtonTypeEnum

class CustomButton {

	String 	name = ""
	String 	channelLabel = ""
	String 	channelColor = ""
	String 	channelContent = ""
	String  imageURL = ""
	String 	type  = CustomButtonTypeEnum.LINK.value
	User 	createdBy 
	Date dateCreated
	Date lastUpdated
	boolean enabled
	//Record information
	Scheduler schedule
	
	//static belongsTo = [CompanyLocation]
	//HasMany
	/*static hasMany= [locations:CompanyLocation]*/
	
	
    static constraints = {
		name 			blank:false , maxSize:20, matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
		channelLabel 	blank:false , maxSize:20, matches: Constants.REGULAR_EXPRESSION_FOR_STRING_FIELD
		channelColor 	blank:false , maxSize:10, matches: /^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/
		channelContent 	url:true,blank:false , maxSize:50
		imageURL 		nullable:true 
    }	
	 

	//Mapping with database table
	static mapping= {
		table "custom_buttons"		
		autoTimestamp true
	}
	
	
	static namedQueries = {
		getButtonsByCompanies { companies , user ->
			createAlias("locations", "l", CriteriaSpecification.LEFT_JOIN)
								 		  
			or {
				and{
					'in'("l.company.id", companies)
					
				}				
				eq("createdBy", user)
			}
			order("name", "asc")
			
		}		
		
		getButtonsByLocationAndEnabled { location , enabled ->
			//createAlias("locations", "l", CriteriaSpecification.LEFT_JOIN)
										   
			or {
				and{
					'in'("locations", location)
					
				}
				eq("enabled", enabled)
			}
			order("name", "asc")
			
		}
	}
	
	String toString()
	{
		name;
	}
} // END CustomButtons
