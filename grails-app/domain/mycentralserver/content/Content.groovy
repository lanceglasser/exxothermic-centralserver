/**
 * 
 */
package mycentralserver.content

import java.io.Serializable;
import java.util.Date;

import org.codehaus.groovy.grails.validation.MinSizeConstraint

import mycentralserver.app.Affiliate;
import mycentralserver.company.Company
import mycentralserver.company.CompanyLocation
import mycentralserver.generaldomains.Scheduler;
import mycentralserver.user.User
import mycentralserver.utils.Constants;

import org.hibernate.criterion.CriteriaSpecification

//import mycentralserver.utils.CustomButtonTypeEnum





import mycentralserver.utils.ContentTypeEnum
import mycentralserver.utils.CustomButtonTypeEnum
import mycentralserver.utils.PartnerEnum;

/**
 * @author Cecropia Solutions
 *
 */
class Content implements Serializable {
	String 	title = ""; 				// Channel Label 
	String 	name = ""; 					// Content Name
	String 	description = "";			// Offers and Message Description- not used in contents 
	String 	color = "";					// color
	String 	url = "";					// Content URL
	String 	dialogImage = "";			// Offer detailed view
	String 	featuredImage = "";			// Content Image
	String 	dialogImageTablet = "";		// Offer detailed view for Tablets
	String 	featuredImageTablet = "";	// Content Image for Tablets
	String 	type  = "";					// image, text or offer 
	String 	offerCode  = "";			// Content, Offer or
	boolean featured = false; 			// Flag to know if the offer is a Content too	
	boolean isFile = false;				// Flag to know if the offer is will be shown in information Files section
	String 	filename = "";				// fileName if is PDF
	String 	referenceType = ""; 		// Define if the offer reference is an URL or a PDF file
	User 	createdBy;
	Date 	dateCreated;
	Date 	lastUpdated;
	boolean enabled	;
	
	boolean	genFeaturedImgForTablets = false;
	
	boolean	genDialogImages = false;
	boolean genDialogImgForTablets = false;
	
	//Record information
	Scheduler schedule;
	
	/*
	 * For Default Association of new Locations
	 */
	Affiliate affiliate

	/**
	 * If true; will be associate to new Locations of the related affiliate
	 */
	boolean isDefaultOfLocation = false;
	
	
	//static belongsTo = [CompanyLocation]
	//HasMany
	static hasMany= [locations:CompanyLocation]
	
	
	static constraints = {
		title						blank:false,minSize:1, maxSize:100
		offerCode	 				nullable:true
		dialogImage 				nullable:true
		featuredImage 				nullable:true
		description					nullable:true, maxSize:400
		schedule   					nullable:true
		name   						nullable:true
		affiliate					nullable:true		
		filename					nullable:true
		dialogImageTablet 			nullable:true
		featuredImageTablet 		nullable:true
	}
	
	enum type {
		image, text, offer
	}
	
	enum referenceType {
		PDF, URL
	}
	
	//Mapping with database table
	static mapping = {
		table "contents"
		autoTimestamp true
	}
	
	
	static namedQueries = {
		// Query that filter the data
		listAllBannersForUITable { start, length, columnName, orderDir, searchText, onlyEnable, user, 
			isHelpDesk, isAdmin, affiliate, companies, locationId, getCount ->
			
			
			projections {
				if(getCount){	// get the count of registers when the dev set this value to true
					countDistinct("id")
				}else{
					distinct(['id','title', 'featuredImage','enabled'])
				}
			}
			
			
			createAlias("locations", "l", CriteriaSpecification.LEFT_JOIN)
			
			// When is using the filter
			if( searchText != ""){
				or {
					like("title", "%" + searchText + "%")
				}
			}
			
			if(locationId != 0){
				eq("l.id", locationId)
				eq("enabled", true)
			}
			
			if(isAdmin){
				ne("type", "offer")
			}else if(isHelpDesk){
				if(affiliate.code != PartnerEnum.getDefaultPartnerCode()){
					//Other affiliates can se only their Default Contents					
						ne("type", "offer")
						or{
							isNull("affiliate")
							eq("affiliate", affiliate)
						}					
				}else{
					ne("type", "offer")
				}
			}else{ // user
				ne('type', 'offer')
				eq('isDefaultOfLocation',false)
				or {
					and{
						'in'("l.company.id", companies)
						
					}
					eq("createdBy", user)
				}
				order("name", "asc")
			}
			
			firstResult(start)
			
			if(length != 0){
				maxResults(length)
			}
			
			order (columnName, orderDir)
		} // End of listData query
		
		listAllOffersForUITable { start, length, columnName, orderDir, searchText, onlyEnable, user,
			isHelpDesk, isAdmin, affiliate, companies, locationId, getCount ->
			
			projections {
				if(getCount){	// get the count of registers when the dev set this value to true
					countDistinct("id")
				}else{
					distinct(['id','title', 'dialogImage','enabled'])
				}
			}
			
			createAlias("locations", "l", CriteriaSpecification.LEFT_JOIN)
			
			// When is using the filter
			if( searchText != ""){
				or {
					like("title", "%" + searchText + "%")
				}
			}
			
			if(locationId != 0){
				eq("l.id", locationId)
				eq("enabled", true)
			}
			
			if(isAdmin){
				eq("type", "offer")
			}else if(isHelpDesk){
				if(affiliate.code != PartnerEnum.getDefaultPartnerCode()){
					//Other affiliates can see only their Default Contents
						eq("type", "offer")
						or{
							isNull("affiliate")
							eq("affiliate", affiliate)
						}
				}else{
					eq("type", "offer")
				}
			}else{ // user
				
				eq('type', 'offer')
				eq('isDefaultOfLocation',false)
				or {
					and{
						'in'("l.company.id", companies)
						
					}
					eq("createdBy", user)
				}
				order("name", "asc")
			}
			// If must find only the enable companies
			/*if(onlyEnable){
				eq("enabled", true)
			}*/
			
			firstResult(start)
			
			if(length != 0){
				maxResults(length)
			}
			
			order (columnName, orderDir)
			} // End of listData query
		getOffersContentsByCompanies { companies , user ->
			createAlias("locations", "l", CriteriaSpecification.LEFT_JOIN)
			eq('type', 'offer')
			or {
				and{
					'in'("l.company.id", companies)
					
				}
				eq("createdBy", user)
			}
			order("name", "asc")
			
		}
		getNotOffersContentsByCompanies { companies , user ->
			createAlias("locations", "l", CriteriaSpecification.LEFT_JOIN)
			ne('type', 'offer')
			or {
				and{
					'in'("l.company.id", companies)
					
				}
				eq("createdBy", user)
			}
			order("name", "asc")
			
		}
		
		getContentsByCompanies { companies , user ->
			createAlias("locations", "l", CriteriaSpecification.LEFT_JOIN)
										   
			or {
				and{
					'in'("l.company.id", companies)
					
				}
				eq("createdBy", user)
			}
			order("name", "asc")
			
		}
		
		getContentsByLocationAndEnabled { location , enabled ->
			//createAlias("locations", "l", CriteriaSpecification.LEFT_JOIN)
										   
			or {
				and{
					'in'("locations", location)
					
				}
				eq("enabled", enabled)
			}
			order("name", "asc")
			
		}
	}
	
	String toString()
	{
		title;
	}
	
} // END Contents


