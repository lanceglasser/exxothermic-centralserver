/**
 * RetrieveUsageDataService.groovy
 */
package mycentralserver

import java.util.HashMap.Entry;
import java.util.logging.Logger;

import mycentralserver.box.Box;

/**
 * Service class with the logic to retrieve the usage data file
 * from all the connected ExXtractors; this service handles the way
 * of retrieve the information one at the time.
 * 
 * @author Cecropia Solutions
 *
 */
class RetrieveUsageDataService {
	
	/* Inject required services */
	def boxMetricsService;
	
	public static Queue<Box> boxInQueue = new LinkedList<Box>();
	
	/**
	 * This method will get the list of connected ExXtractors and
	 * start the process that retrieve the data file one at the time
	 */
    public void retrieveExXtractorsData() {
		
		//Get the list of Connected ExXtractors by Status
		def exxtractors = Box.connectedByStatus(0, null).list();
		
		// Go over the list and add the Box if it is not in the list already
		for(exx in exxtractors){			
			if(!RetrieveUsageDataService.boxInQueue.contains(exx.serial)){
				//If it's not in the list
				RetrieveUsageDataService.boxInQueue.add(exx.serial);
			}			
		}
		
		log.info("Retrieve data file from this list: " + RetrieveUsageDataService.boxInQueue);
		
		//Start the process
		runRetrieveProcess();
    }
	
	/**
	 * This method will get the data file from the list of ExXtractors
	 * in the queue using a waiting period in order to execute the upload process
	 * one at the time
	 */
	public void runRetrieveProcess(){
		
		String currentSerial = "";
		int counter = 0;
		
		while(RetrieveUsageDataService.boxInQueue.size() > 0) {
			//There are boxes in the queue
			if(currentSerial == "" || !RetrieveUsageDataService.boxInQueue.contains(currentSerial)){
				//If it's the first box or the box was already remove from the queue go with next
				currentSerial = RetrieveUsageDataService.boxInQueue.getFirst();
				log.info("Request usage data file from: " + currentSerial);
				boxMetricsService.requestMetricsDataFromBox(currentSerial);
				counter = 1;
			} else {
				counter++;
				if(counter < 6 * 1){
					//Only wait for 1 minutes; after this will remove the box from the queue in order
					//to continue with the next one;
					RetrieveUsageDataService.boxInQueue.remove(currentSerial);
					log.info("Box remove from the queue after wait more than the timeout period: " + currentSerial);
				} else {
					log.info("Waiting the upload and process of data file from: " + currentSerial + " with counter: " + counter);
				}
			}
			
			//Sleep for 10 seconds in order to wait for the upload and process
			try{
				Thread.sleep(10000);
			} catch(Exception ex){}
		}
				
	}
	
	/**
	 * This method will remove the serial number of a ExXtractor
	 * from the Queue if it was in there
	 * 
	 * @param serial	Serial to be removed from queue
	 */
	public void removeFromQueue(String serial) {
		if(RetrieveUsageDataService.boxInQueue.contains(serial)){
			RetrieveUsageDataService.boxInQueue.remove(serial);
			log.info("Box removed from the queue: " + serial);
		} else {
			log.info("The Box is not in the queue: " + serial);
		}
	}
	
}
