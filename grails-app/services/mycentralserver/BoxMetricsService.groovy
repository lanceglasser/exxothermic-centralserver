package mycentralserver;

import java.text.SimpleDateFormat;
import java.text.DecimalFormat;
import java.util.Map.Entry;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import java.util.concurrent.TimeUnit;

import javax.swing.text.DateFormatter;

import jxl.Workbook;
import jxl.write.*;
import jxl.format.Alignment;
import mycentralserver.box.Box;
import mycentralserver.box.BoxData;
import mycentralserver.box.BoxSummaryData;
import mycentralserver.company.CompanyLocation;
import mycentralserver.utils.DateUtils;
import mycentralserver.utils.ExcelGeneratorUtil;
import mycentralserver.utils.FileHelper;
import mycentralserver.utils.enumeration.Feature;

/**
 * Service with the logic for the store and generation of the ExXtractors Metrics.
 *
 * @author sdiaz
 * @since 03/27/2015
 */
class BoxMetricsService {

    //Inject services
    def restClientService;
    def messageErrorService;
    def userSessionService;
    def grailsApplication;
    def usageEventService;
    def featureService;

    // Auto inject SessionFactory we can use to get the current Hibernate session.
    def sessionFactory;

    /* Constants */
    public final static String METRICS_DATA_SEPARATOR = "/";
    public final static int METRICS_DATE_POSITION = 1;
    public final static int METRICS_PORT_POSITION = 2;
    public final static int METRICS_HOUR_POSITION = 3;
    public final static int METRICS_CHANNEL_NAME_POSITION = 4;
    public final static int METRICS_TIME_POSITION = 5;
    public final static int METRICS_SESSIONS_TOTAL_POSITION = 6;
    public final static int METRICS_SESSIONS_MAX_POSITION = 7;
    public final static int METRICS_TOTAL_FIELDS = 8;

    public final static int METRICS_HOURLY_CLIENTS_TOTAL_FIELDS = 5;
    public final static int METRICS_DAILY_CLIENTS_TOTAL_FIELDS = 4;

    public final static int METRICS_SUMMARY_TYPE_POSITION = 2;
    public final static int METRICS_HC_REPORTED_SESSIONS_POSITION = 4;
    public final static int METRICS_DC_REPORTED_SESSIONS_POSITION = 3;

    public final static String[] ommitedPorts = ["none", "pci-0000:00:1b.0"];

    /**
     * Send the request to a Venue Server for the upload of the metrics data.
     *
     * @param serial
     *              Serial Number of the ExXtractor
     * @return True when success; False when not
     */
    public boolean requestMetricsDataFromBox(String serial) {
        boolean finalResult = false; //default as false; only when success change to true
        final String fileName = "Metrics_" + serial + "_" + DateUtils.getSimpleCurrentDateFormat() + ".tar.gz";
        final def fullFilePath = grailsApplication.config.basePathMyBoxMetricsFiles + fileName;
        File file = new File(fullFilePath);
        if(file.exists()) {
            file.delete();
        }
        final def url = grailsApplication.config.urlServiceUploadMetricsFile;
        final def jsonResponse = restClientService.getMetricsDataFile(serial, fileName, url);
        if(jsonResponse) {
            if(jsonResponse.successful) {
                finalResult = true;
            } else {
                def messageErrorDescriptionHash  = messageErrorService.getMessageErrorDescription();
                log.warn("The request of the metrics file for serial " + serial + 
                    " failed with error: " + messageErrorDescriptionHash.get(jsonResponse.errorCode));
            }
        } else {
            log.warn("The request of the metrics file failed with unknown error for serial: " + serial);
        }
        return finalResult;
    }

    /**
     * This method saves the received metrics data file to the configured path
     *
     * @param request
     *              Request object containing the File
     * @param response
     *              Response object to set the final status
     */
    public void uploadDataFile(def request, def response) {
        def file = request.getFile('uploadFile');
        String storagePath = grailsApplication.config.basePathMyBoxMetricsFiles;
        log.debug("BoxMetricsService :: uploadDataFile :: save file to: " + storagePath + "/" + 
            file.getOriginalFilename());
        file.transferTo( new File(storagePath + file.getOriginalFilename() ) );
        response.sendError(200, 'Done');
    }

    /**
     * This method will process the upload notification from an ExXtractor
     *
     * @param box
     *         ExXtractor notifying the upload
     * @param uploadResult
     *         Result of the upload process
     */
    public void processUploadNotification(Box box, boolean uploadResult) {
        log.info("ProcessUploadNotification :: " + box.serial + " - " + uploadResult);
        if(uploadResult) {
            // Try to read the file related to the Box
            final String currentDate = DateUtils.getSimpleCurrentDateFormat();
            final String fileName = "Metrics_" + box.serial + "_" + currentDate + ".tar.gz";
            final String txtFileName = "Metrics_" + box.serial + "_" + currentDate + ".txt";
            // Process the file depending of the venue server
            if (featureService.isFeatureAllowedForBox(box, Feature.SECOND_USAGE_DATE)) {
                usageEventService.readAndLoadFile(fileName, txtFileName, box);
            } else {
                processMetricsDataFile(fileName, txtFileName, box);
            }
            // Remove from the process queue if exists
            if(RetrieveUsageDataService.boxInQueue != null &&
                RetrieveUsageDataService.boxInQueue.contains(box.serial)) {
                RetrieveUsageDataService.boxInQueue.remove(box.serial);
                log.info("Box removed from the queue: " + box.serial);
            } else {
                log.info("The Box is not in the queue: " + box.serial);
            }
        }
    }

    /**
     * This method will try to read and process a tar.gz file with the metrics data
     *
     * @param fileName
     *         File name of the tar.gz file to process
     * @param txtFileName
     *         File name where to put the content
     * @param box
     *         Related box of the process
     * @throws Exception 
     */
    public void processMetricsDataFile(String fileName, String txtFileName, Box box) throws Exception {
        
        final String path = grailsApplication.config.basePathMyBoxMetricsFiles;
        final File file = new File(path + fileName);
        if(file.exists()) {
            FileHelper.decompressGzipFile(path + fileName, path + txtFileName);
            File txtFile = new File(path + txtFileName);
            if(txtFile.exists()){
                BufferedReader br = new BufferedReader(new FileReader(path + txtFileName));
                String line = null;
                Date date = null;
                String port = "";
                String name = "";
                String summaryType = "";
                double time = 0;
                int sessionsTotal = 0;
                int sessionsMax = 0;
                int hour = 0;
                SimpleDateFormat dateFormatter = new SimpleDateFormat('MM-dd-yyyy');

                while ((line = br.readLine()) != null) {
                    String[] data = line.split(BoxMetricsService.METRICS_DATA_SEPARATOR);
                    if(data.length == BoxMetricsService.METRICS_TOTAL_FIELDS) {
                        //Looks like a valid line
                        try {
                            ////DAY_OF_YEAR/DATE/PORT/HOUR/NAME/TRCK_TIME/TOTAL_SESSIONS/MAX_SESSIONS
                            port = data[BoxMetricsService.METRICS_PORT_POSITION];
                            if(!BoxMetricsService.ommitedPorts.contains(port)){
                                // Only include registers when the port is not in the ommited list
                                /* check if the incoming data has a valid data( deny future dates )*/
                                Date currentDate = new Date();
                                Date registerDate = dateFormatter.parse(data[BoxMetricsService.METRICS_DATE_POSITION]);

                                Calendar c = Calendar.getInstance();
                                c.setTime(registerDate); // substract 1 day fom the date to get correcto value
                                c.add(Calendar.HOUR_OF_DAY, Integer.parseInt(data[BoxMetricsService.METRICS_HOUR_POSITION]));
                                registerDate = c.getTime();


                                if(currentDate.compareTo(registerDate)>0){
                                    // currentDate is after registerDate
                                    date = dateFormatter.parse(data[BoxMetricsService.METRICS_DATE_POSITION]);
                                    hour = Integer.parseInt(data[BoxMetricsService.METRICS_HOUR_POSITION]);
                                    name = data[BoxMetricsService.METRICS_CHANNEL_NAME_POSITION];
                                    time = Double.parseDouble(data[BoxMetricsService.METRICS_TIME_POSITION]);
                                    sessionsTotal = Integer.parseInt(data[BoxMetricsService.METRICS_SESSIONS_TOTAL_POSITION]);
                                    sessionsMax = Integer.parseInt(data[BoxMetricsService.METRICS_SESSIONS_MAX_POSITION]);

                                    BoxData boxData = BoxData.findOrCreateWhere(location:box.location ,serial: box.serial, port: port, date: date, hour: hour);
                                    boxData.channelName = name;
                                    boxData.boxName = box.name;
                                    boxData.time += time;
                                    boxData.totalSessions = sessionsTotal;
                                    boxData.maxSessions = sessionsMax;

                                    boxData.save();
                                }
                            }
                        } catch(Exception lineEx){
                            log.error("Invalid metrics data line: " + line, lineEx);
                        }
                    } else if(data.length == BoxMetricsService.METRICS_HOURLY_CLIENTS_TOTAL_FIELDS) {
                        //Looks like a valid line
                        try {
                            ////DAY_OF_YEAR/DATE/resumeType/HOUR/TOTAL_SESSIONS
                            date = dateFormatter.parse(data[BoxMetricsService.METRICS_DATE_POSITION]);
                            hour = Integer.parseInt(data[BoxMetricsService.METRICS_HOUR_POSITION]);
                            summaryType = data[BoxMetricsService.METRICS_SUMMARY_TYPE_POSITION];
                            sessionsTotal = Integer.parseInt(data[BoxMetricsService.METRICS_HC_REPORTED_SESSIONS_POSITION  ]);
                            BoxSummaryData boxSummary = BoxSummaryData.findOrCreateWhere(location:box.location,serial: box.serial, date: date, hour: hour, summaryType: summaryType);
                            boxSummary.summaryType = summaryType;
                            boxSummary.boxName = box.name;
                            boxSummary.reportedSessions = sessionsTotal;
                            boxSummary.hour = hour;
                            boxSummary.save(flush: true, failOnError: true);
                        } catch(Exception lineEx){
                            log.error("Invalid metrics data line: " + line, lineEx);
                        }
                    }else if(data.length == BoxMetricsService.METRICS_DAILY_CLIENTS_TOTAL_FIELDS) {
                        //Looks like a valid line
                        try {
                            ////DAY_OF_YEAR/DATE/resumeType/TOTAL_SESSIONS
                            date = dateFormatter.parse(data[BoxMetricsService.METRICS_DATE_POSITION]);
                            summaryType = data[BoxMetricsService.METRICS_SUMMARY_TYPE_POSITION];
                            sessionsTotal = Integer.parseInt(data[BoxMetricsService.METRICS_DC_REPORTED_SESSIONS_POSITION  ]);
                            BoxSummaryData boxSummary = BoxSummaryData
                                .findOrCreateWhere(location:box.location,serial: box.serial, date: date, hour: 0,
                                    summaryType: summaryType);
                            boxSummary.boxName = box.name;
                            boxSummary.summaryType = summaryType;
                            boxSummary.reportedSessions = sessionsTotal;
                            boxSummary.save(flush: true, failOnError: true);
                        } catch(Exception lineEx) {
                            log.error("Invalid metrics data line: " + line, lineEx);
                        }
                    } else {
                        log.debug("The length of the line is wrong: " + data.length + " - " + BoxMetricsService.METRICS_TOTAL_FIELDS + " - " +
                                (data.length == BoxMetricsService.METRICS_TOTAL_FIELDS));
                    }
                }
                txtFile.delete();
                file.delete();
            } else {
                throw new Exception("Decompressed file doesn't exists: " + path + txtFileName);
            }
        } else {
            throw new Exception("The file doesn't exists: " + path + fileName);
        }
    }

	/**
	 * This method will return a Json object with the data information filtered
	 * by the params received; it's use for the generation of the data result table of the
	 * report.
	 *
	 * @param params		Request params
	 * @return				Data structure for the table
	 * @throws Exception	When something goes wrong
	 */
	public HashMap getHourClientsData(params) throws Exception {
	
		HashMap jsonMap = new HashMap();
		def data = [];
		
		// Get params information
		long locationId = params.locationId? params.locationId.toLong():0;
		def locations = null;
		if(locationId == 0 /*&& !userSessionService.isAdminOrHelpDesk()*/){
			//If must get the data from all Locations and it is not Help Desk or Admin must filter the data for associated Locations
			locations = userSessionService.getPermitedLocations();
		}
		
		String startDateStr = params["startDate"];
		String endDateStr = params["endDate"];
		
		int start = params.start? params.start.toInteger():0;
		int length = params.length? params.length.toInteger():10;
		int columnOrder = params["order[0][column]"]? params["order[0][column]"].toInteger():0;
		String orderDir = params["order[0][dir]"]? params["order[0][dir]"]:"asc";
		String searchText = (params["search[value]"])? params["search[value]"]:'';
		String columnOrderName = "";
		switch(columnOrder){
			case 0: columnOrderName = "l.name"; break;
			case 1: columnOrderName = "data.box_name"; break;
			case 2: columnOrderName = "data.serial"; break;
			case 3: columnOrderName = "LocDate"; break;
			case 4: columnOrderName = "LocHour"; break;
			case 5: columnOrderName = "data.reported_sessions"; break;
		}
		
		log.debug("Paginate metrics data using [Start: " + start + ", length: " + length + ", search: " + searchText
			+ ", Location: " + locationId + ", startDate: " + startDateStr + ", endDate: " + endDateStr + "]");
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
		
		Date startDate = dateFormatter.parse(startDateStr + " 00:00:00");
		Date endDate = dateFormatter.parse(endDateStr + " 23:59:59");
		
		//int totalCount = BoxSummaryData.listHourClientsData(0, 0, columnOrderName, orderDir, "", locationId, startDate, endDate, locations).count();
		//int filteredCount = BoxSummaryData.listHourClientsData(0, 0, columnOrderName, orderDir, searchText, locationId, startDate, endDate, locations).count();
		//def dataResult = BoxSummaryData.listHourClientsData(start, length, columnOrderName, orderDir, searchText, locationId, startDate, endDate, locations).list();
		
		int totalCount = getHourlyClientsDataCountForTable("", locationId, startDate, endDate, locations);
		int filteredCount = getHourlyClientsDataCountForTable(searchText, locationId, startDate, endDate, locations);
		def dataResult = getHourlyClientsDataForTable( start, length, columnOrderName, orderDir, searchText, locationId, startDate, endDate, locations);
		BoxSummaryData boxData = null;
		dateFormatter = new SimpleDateFormat("MM/dd/yyyy");
		
		HashMap<String, HashMap<Integer, HashMap<String, Double>>> locationsMap = new HashMap<String, HashMap<Integer, HashMap<String, Double>>>();
		Box box;
		for(row in dataResult){
			data.add([row[0], row[1], row[2],
				dateFormatter.format(row[3]), row[4] + ":00",
				row[5]]);
		}
		
		//Put data to json structure
		jsonMap["draw"] = params["draw"]? params["draw"]:1;
		jsonMap["recordsTotal"] = totalCount;
		jsonMap["recordsFiltered"] = filteredCount;
		jsonMap["error"] =  false;
		jsonMap["data"] = data;
				  
		return jsonMap;
	} // End of getClientsData method
	
	
	/**
	 * This method will return a Json object with the data information filtered
	 * by the params received; it's use for the generation of the data result table of the
	 * report.
	 *
	 * @param params		Request params
	 * @return				Data structure for the table
	 * @throws Exception	When something goes wrong
	 */
	public HashMap getDailyClientsData(params) throws Exception {
	
		HashMap jsonMap = new HashMap();
		def data = [];
		
		// Get params information
		long locationId = params.locationId? params.locationId.toLong():0;
		def locations = null;
		if(locationId == 0 && !userSessionService.isAdminOrHelpDesk()){
			//If must get the data from all Locations and it is not Help Desk or Admin must filter the data for associated Locations
			locations = userSessionService.getPermitedLocations();
		}
		
		String startDateStr = params["startDate"];
		String endDateStr = params["endDate"];
		
		int start = params.start? params.start.toInteger():0;
		int length = params.length? params.length.toInteger():10;
		int columnOrder = params["order[0][column]"]? params["order[0][column]"].toInteger():0;
		String orderDir = params["order[0][dir]"]? params["order[0][dir]"]:"asc";
		String searchText = (params["search[value]"])? params["search[value]"]:'';
		String columnOrderName = "";
		switch(columnOrder){
			case 0: columnOrderName = "l.name"; break;
			case 1: columnOrderName = "boxName"; break;
			case 2: columnOrderName = "serial"; break;
			case 3: columnOrderName = "date"; break;
			case 4: columnOrderName = "reportedSessions"; break;
		}
		
		log.debug("Paginate metrics data using [Start: " + start + ", length: " + length + ", search: " + searchText
			+ ", Location: " + locationId + ", startDate: " + startDateStr + ", endDate: " + endDateStr + "]");
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
		
		Date startDate = dateFormatter.parse(startDateStr + " 00:00:00");
		Date endDate = dateFormatter.parse(endDateStr + " 23:59:59");
		
		int totalCount = BoxSummaryData.listDailyClientsData(0, 0, columnOrderName, orderDir, "", locationId, startDate, endDate, locations).count();
		int filteredCount = BoxSummaryData.listDailyClientsData(0, 0, columnOrderName, orderDir, searchText, locationId, startDate, endDate, locations).count();
		def dataResult = BoxSummaryData.listDailyClientsData(start, length, columnOrderName, orderDir, searchText, locationId, startDate, endDate, locations).list();
		BoxSummaryData boxData = null;
		dateFormatter = new SimpleDateFormat("MM/dd/yyyy");
		
		HashMap<String, HashMap<Integer, HashMap<String, Double>>> locationsMap = new HashMap<String, HashMap<Integer, HashMap<String, Double>>>();
		Box box;
		
		for(row in dataResult){
			boxData = row;
			box = Box.findBySerial(boxData.serial);
			
			data.add([boxData.location.name,boxData.boxName,boxData.serial,
				dateFormatter.format(boxData.date),
				boxData.reportedSessions]);
		}
		
		//Put data to json structure
		jsonMap["draw"] = params["draw"]? params["draw"]:1;
		jsonMap["recordsTotal"] = totalCount;
		jsonMap["recordsFiltered"] = filteredCount;
		jsonMap["error"] =  false;
		jsonMap["data"] = data;
				  
		return jsonMap;
	} // End of getClientsData method
	
	
	/**
	 * This method will return a Json object with the data information filtered
	 * by the params received; it's use for the generation of the data result table of the
	 * report.
	 *
	 * @param params		Request params
	 * @return				Data structure for the table
	 * @throws Exception	When something goes wrong
	 */
	public HashMap getData(params) throws Exception {
	
		HashMap jsonMap = new HashMap();
		def data = [];
		
		// Get params information
		long locationId = params.locationId? params.locationId.toLong():0;
		def locations = null;
		if(locationId == 0 && !userSessionService.isAdminOrHelpDesk()){
			//If must get the data from all Locations and it is not Help Desk or Admin must filter the data for associated Locations
			locations = userSessionService.getPermitedLocations();
		}
		
		String startDateStr = params["startDate"];
		String endDateStr = params["endDate"];
		
		int start = params.start? params.start.toInteger():0;
		int length = params.length? params.length.toInteger():10;
		int columnOrder = params["order[0][column]"]? params["order[0][column]"].toInteger():0;
		String orderDir = params["order[0][dir]"]? params["order[0][dir]"]:"asc";
		String searchText = (params["search[value]"])? params["search[value]"]:'';
		String columnOrderName = "";
		switch(columnOrder){
			case 0: columnOrderName = "l.name"; break;
			case 1: columnOrderName = "bd.box_name"; break;
			case 2: columnOrderName = "bd.serial"; break;
			case 3: columnOrderName = "LocDate"; break;
			case 4: columnOrderName = "bd.port"; break;
			case 5: columnOrderName = "bd.channel_name"; break;
			case 6: columnOrderName = "LocHour"; break;
			case 7: columnOrderName = "bd.time"; break;
			case 8: columnOrderName = "bd.max_sessions"; break;
			case 9: columnOrderName = "bd.total_sessions"; break;
			default: columnOrderName = "l.name"; break;
		}
				
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
		
		Date startDate = dateFormatter.parse(startDateStr + " 00:00:00");
		Date endDate = dateFormatter.parse(endDateStr + " 23:59:59");
		
		//int totalCount = BoxData.listData(0, 0, columnOrderName, orderDir, "", locationId, startDate, endDate, locations).count();
		//int filteredCount = BoxData.listData(0, 0, columnOrderName, orderDir, searchText, locationId, startDate, endDate, locations).count();
		//def dataResult = BoxData.listData(start, length, columnOrderName, orderDir, searchText, locationId, startDate, endDate, locations).list();
		
		int totalCount = getStreamingDataCountForTable("", locationId, startDate, endDate, locations);
		int filteredCount = getStreamingDataCountForTable(searchText, locationId, startDate, endDate, locations);
		def dataResult = getStreamingDataForTable(start, length, columnOrderName, orderDir, searchText, locationId, startDate, endDate, locations);
		BoxData boxData = null;
		dateFormatter = new SimpleDateFormat("MM/dd/yyyy");
		
		HashMap<String, HashMap<Integer, HashMap<String, Double>>> locationsMap = new HashMap<String, HashMap<Integer, HashMap<String, Double>>>();
		Box box;
		for(row in dataResult){
			//boxData = row;
			box = Box.findBySerial(row[0]);
			long time = row[1].toLong();
			String hms = String.format("%02d:%02d:%02d",
				TimeUnit.SECONDS.toHours(time),
				TimeUnit.SECONDS.toMinutes(time) -
				TimeUnit.HOURS.toMinutes(TimeUnit.SECONDS.toHours(time)),
				time -
				TimeUnit.MINUTES.toSeconds(TimeUnit.SECONDS.toMinutes(time)))
			
			
			data.add([row[2], row[3], row[0],
				dateFormatter.format(row[4]), row[5], row[6], row[7] + ":00",
				hms, row[8], row[9]]);
		}
		
		//Put data to json structure
		jsonMap["draw"] = params["draw"]? params["draw"]:1;
		jsonMap["recordsTotal"] = totalCount;
		jsonMap["recordsFiltered"] = filteredCount;
		jsonMap["error"] =  false;
		jsonMap["data"] = data;
				  
		return jsonMap;
	} // End of getData method
	
	/**
	 * This method will return a Json object with the data information filtered
	 * by the parameters received; it's use for the generation of the report charts.
	 *
	 * @param params		Request params
	 * @return				Data structure for the charts
	 * @throws Exception	When something goes wrong
	 */
	public HashMap getChartData(params) throws Exception {
	
		HashMap jsonMap = new HashMap();
		
		// Get params information
		long locationId = params.locationId? params.locationId.toLong():0;
		String startDateStr = params["startDate"];
		String endDateStr = params["endDate"];
				
		HashMap<String, HashMap<Integer, HashMap<String, Double>>> locationsMap =
			getDataRecords(locationId, startDateStr, endDateStr);
		
		//Put data to json structure
		
		// Define Chart Data
		int maximumMaxNumber = 0;
		int maximumTimeNumber = 0;
		int maximumTotalNumber = 0;
		def chartData = [];
		def totalSessionsChartData = [];
		def maxSessionsChartData = [];
		int TotalTime = 0;
		double timeInMinutes = 0;
				
		if(locationsMap.size() > 0){
			def headersData = [];
			headersData.add("X");
			for(Entry<String, Object> entry: locationsMap.entrySet()) {
				headersData.add(entry.getKey());
			}
			chartData.add(headersData);
			totalSessionsChartData.add(headersData);
			maxSessionsChartData.add(headersData);
			
			DecimalFormat decimalFormatter = new DecimalFormat("######");
			
			for(int i=0; i < 24; i++){
				def rowData = [];
				def rowTotalSessionsData = [];
				def rowMaxSessionsData = [];
				
				rowData.add(""+i);
				rowTotalSessionsData.add(""+i);
				rowMaxSessionsData.add(""+i);
				for(Entry<String, Object> entry: locationsMap.entrySet()) {
					HashMap<String, HashMap<Integer, HashMap<String, Double>>> locationData = entry.getValue();
					if(locationData.containsKey(i)){
						HashMap<String, Double> hourData = locationData.get(i);
						//Display the time in minutes
						timeInMinutes = Integer.parseInt(""+decimalFormatter.format(hourData.get("time")/60));
						rowData.add(timeInMinutes);
						rowTotalSessionsData.add(hourData.get("totalSessions"));
						rowMaxSessionsData.add(hourData.get("maxSessions"));
						maximumMaxNumber = (maximumMaxNumber > hourData.get("maxSessions"))? maximumMaxNumber:hourData.get("maxSessions");
						maximumTotalNumber = (maximumTotalNumber > hourData.get("totalSessions"))? maximumTotalNumber:hourData.get("totalSessions");
						maximumTimeNumber = (maximumTimeNumber > timeInMinutes)? maximumTimeNumber:timeInMinutes;
						TotalTime += hourData.get("time");
					} else {
						rowData.add(0);
						rowTotalSessionsData.add(0);
						rowMaxSessionsData.add(0);
					}
				}
				chartData.add(rowData);
				totalSessionsChartData.add(rowTotalSessionsData);
				maxSessionsChartData.add(rowMaxSessionsData);
			}
		}
				
		String hms = String.format("%02d:%02d:%02d",
			TimeUnit.SECONDS.toHours(TotalTime),
			TimeUnit.SECONDS.toMinutes(TotalTime) -
			TimeUnit.HOURS.toMinutes(TimeUnit.SECONDS.toHours(TotalTime)), // The change is in this line
			TotalTime -
			TimeUnit.MINUTES.toSeconds(TimeUnit.SECONDS.toMinutes(TotalTime)));
		
		jsonMap["locationsCount"] = locationsMap.size();
		jsonMap["chartData"] = chartData;
		jsonMap["sessionsTotalData"] = totalSessionsChartData;
		jsonMap["sessionsMaxData"] = maxSessionsChartData;
		jsonMap["TotalTime"] = hms;
		jsonMap["maximumMaxNumber"] = maximumMaxNumber;
		jsonMap["maximumTotalNumber"] = maximumTotalNumber;
		jsonMap["maximumTimeNumber"] = maximumTimeNumber;
		
		return jsonMap;
	} // End of getData method
	
	/**
	 * This method will return a Json object with the data information filtered
	 * by the parameters received; it's use for the generation of the report charts.
	 *
	 * @param params		Request params
	 * @return				Data structure for the charts
	 * @throws Exception	When something goes wrong
	 */
	public HashMap getHourClientsChartData(params) throws Exception {
	
		HashMap jsonMap = new HashMap();
		
		int maximumClientsNumber = 0;
		int maximumDailyClientsNumber = 0;
		
		// Get params information
		long locationId = params.locationId? params.locationId.toLong():0;
		String startDateStr = params["startDate"];
		String endDateStr = params["endDate"];
		String selectedStr = params["selectedDate"];
				
		HashMap<String, HashMap<Integer, HashMap<String, Double>>> locationsMap =
			getHourClientsDataRecords(locationId, selectedStr, selectedStr);
		
		//Put data to json structure
		def chartData = [];
		
		if(locationsMap.size() > 0){
			def headersData = [];
			headersData.add("X");
			for(Entry<String, Object> entry: locationsMap.entrySet()) {
				headersData.add(entry.getKey());
			}
			chartData.add(headersData);
			for(int i=0; i < 24; i++){
				def rowData = [];
				rowData.add(""+i);
				for(Entry<String, Object> entry: locationsMap.entrySet()) {
					HashMap<String, HashMap<Integer, HashMap<String, Double>>> locationData = entry.getValue();
					if(locationData.containsKey(i)){
						HashMap<String, Double> hourData = locationData.get(i);
						rowData.add(hourData.get("reportedSessions"));
						maximumClientsNumber = (maximumClientsNumber > hourData.get("reportedSessions"))? maximumClientsNumber:hourData.get("reportedSessions");
					} else {
						rowData.add(0);
					}
				}
				chartData.add(rowData);
			}
		}
		
		jsonMap["locationsCount"] = locationsMap.size();
		jsonMap["data"] = chartData;
		jsonMap["maximumTimeNumber"] = maximumClientsNumber;
		
		// init daily clients chart data
		//HH converts hour in 24 hours format (0-23), day calculation
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
 
		Date d1 = null;
		Date d2 = null;
 
		d1 = format.parse(startDateStr);
		d2 = format.parse(endDateStr);
 
		//in milliseconds
		long diff = d2.getTime() - d1.getTime();
		long diffDays = diff / (24 * 60 * 60 * 1000);
		
		HashMap<String, HashMap<Integer, HashMap<String, Double>>> locationsMapDaily =
		getDailyClientsDataRecords(locationId, startDateStr, endDateStr);
	
	//Put data to json structure
			
	def chartDataDaily = [];
	
	if(locationsMapDaily.size() > 0){
		def headersData = [];
		headersData.add("X");
		for(Entry<String, Object> entry: locationsMapDaily.entrySet()) {
			headersData.add(entry.getKey());
		}
		chartDataDaily.add(headersData);
		for(int i=0; i <= diffDays; i++){
			
			format = new SimpleDateFormat("MM/dd");
			Calendar c = Calendar.getInstance();
			c.setTime(d1); // Now use today date.
			c.add(Calendar.DATE, i); // Adding i days
			//String output = sdf.format(c.getTime());
			
			
			def rowData = [];
			rowData.add(""+format.format(c.getTime()));
			for(Entry<String, Object> entry: locationsMapDaily.entrySet()) {
				HashMap<String, HashMap<Integer, HashMap<String, Double>>> locationData = entry.getValue();
				if(locationData.containsKey(""+format.format(c.getTime()))){
					HashMap<String, Double> hourData = locationData.get(""+format.format(c.getTime()));
					rowData.add(hourData.get("reportedSessions"));
					maximumDailyClientsNumber = (maximumDailyClientsNumber > hourData.get("reportedSessions"))? maximumDailyClientsNumber:hourData.get("reportedSessions");
				} else {
					rowData.add(0);
				}
			}
			chartDataDaily.add(rowData);
		}
	}
	
	jsonMap["locationsCountDaily"] = locationsMapDaily.size();
	jsonMap["dataDaily"] = chartDataDaily;
	jsonMap["maximumDailyClientsNumber"] = maximumDailyClientsNumber;
		
		return jsonMap;
	} // End of getClientsChartData method
	
	
	
	/**
	 * This method will return a Json object with the data information filtered
	 * by the parameters received; it's use for the generation of the report charts
	 * for all the dates range.
	 *
	 * @param params		Request params
	 * @return				Data structure for the charts
	 * @throws Exception	When something goes wrong
	 */
	public HashMap getRangeChartsData(params) throws Exception {
	
		HashMap jsonMap = new HashMap();
		
		// Get params information
		long locationId = params.locationId? params.locationId.toLong():0;
		String startDateStr = params["startDate"];
		String endDateStr = params["endDate"];
		
		HashMap<String, HashMap<Integer, HashMap<String, Double>>> locationsMap =
			getDataRecords(locationId, startDateStr, endDateStr);
		
		//Put data to json structure
		
		// Define Chart Data
		int maximumTimeNumber = 0;
		def chartData = [];
		
		if(locationsMap.size() > 0){
			def headersData = [];
			headersData.add("X");
			for(Entry<String, Object> entry: locationsMap.entrySet()) {
				headersData.add(entry.getKey());
			}
			chartData.add(headersData);
			
			SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy");
			
			Date startDate = dateFormatter.parse(startDateStr);
			Date endDate = dateFormatter.parse(endDateStr);
			
			while(startDate <= endDate) {
				def rowData = [];
				rowData.add(""+dateFormatter.format(startDate));
				double time = 0;
				for(Entry<String, Object> entry: locationsMap.entrySet()) {
					HashMap<String, HashMap<Integer, HashMap<String, Double>>> locationData = entry.getValue();
					time = 0;
					for(int i=1; i < 25; i++){
						if(locationData.containsKey(i)){
							HashMap<String, Double> hourData = locationData.get(i);
							time += hourData.get("time");
							
						}
					} // End of for of hours per location
					maximumTimeNumber = (maximumTimeNumber > time)? maximumTimeNumber:time;
					rowData.add(time);
				} // End of for per Location
				startDate = new Date(startDate.getTime() + (1000 * 60 * 60 * 24)); //Moving to the next day
				chartData.add(rowData);
			} // End of While of Dates
		} // End of if locations > 0
		
		jsonMap["locationsCount"] = locationsMap.size();
		jsonMap["chartData"] = chartData;
		jsonMap["maximumTimeNumber"] = maximumTimeNumber;
		
		return jsonMap;
	} // End of getRangeChartsData method
	
	/**
	 * Return the records filtered by dates and Location of the BoxData table
	 *
	 * @param locationId	Location Id to filter; 0 when not filter must be applied
	 * @param startDateStr	Start Date with format MM/dd/yyyy
	 * @param endDateStr	End Date with format MM/dd/yyyy
	 * @return				HashMap with information
	 */
	public HashMap<String, HashMap<Integer, HashMap<String, Double>>> getDataRecords(long locationId, String startDateStr, String endDateStr) {
		
		def locations = null;
		if(locationId == 0){
			//If must get the data from all Locations and it is not Help Desk or Admin must filter the data for associated Locations
			locations = userSessionService.getPermitedLocations();
		}
				
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
		
		Date startDate = dateFormatter.parse(startDateStr + " 00:00:00");
		Date endDate = dateFormatter.parse(endDateStr + " 23:59:59");
		
		def dataResult = getStreamingData(locationId, startDate, endDate, locations);
		//BoxData boxData = null;
		
		dateFormatter = new SimpleDateFormat("MM/dd/yyyy");
		
		HashMap<String, HashMap<Integer, HashMap<String, Double>>> locationsMap = new HashMap<String, HashMap<Integer, HashMap<String, Double>>>();
		for(row in dataResult){
			//boxData = row;
			// Save data for charts information
			if(locationsMap.containsKey(row[1])){
				//Location Map already exist
				HashMap<Integer, HashMap<String, Double>> locationData = locationsMap.get(row[1]);
				if(locationData.containsKey(row[2])){
					HashMap<String, Double> hourData = locationData.get(row[2]);
					hourData.put("time", hourData.get("time") + row[3]);
					if(hourData.get("maxSessions") < row[4])
					{
						hourData.put("maxSessions", row[4]);
					}
					hourData.put("totalSessions", hourData.get("totalSessions") + row[5]);
					locationData.put(row[2], hourData);
				} else {
					HashMap<String, Double> hourData = new HashMap<String, Double>();
					hourData.put("time", row[3]);
					hourData.put("maxSessions", row[4]);
					hourData.put("totalSessions", row[5]);
					locationData.put(row[2], hourData);
				}
				locationsMap.put(row[1], locationData);
			} else {
				//Location Map do not exist
				HashMap<Integer, HashMap<String, Double>> locationData = new HashMap<Integer, HashMap<String, Double>>();
				HashMap<String, Double> hourData = new HashMap<String, Double>();
				hourData.put("time", row[3]);
				hourData.put("maxSessions", row[4]);
				hourData.put("totalSessions", row[5]);
				locationData.put(row[2], hourData);
				locationsMap.put(row[1], locationData);
			}
		}
		return locationsMap;
	} // End of getDataRecords
	
	/**
	 * Return the records filtered by dates and Location of the BoxData table
	 *
	 * @param locationId	Location Id to filter; 0 when not filter must be applied
	 * @param startDateStr	Start Date with format MM/dd/yyyy
	 * @param endDateStr	End Date with format MM/dd/yyyy
	 * @return				HashMap with information
	 */
	public HashMap<String, HashMap<Integer, HashMap<String, Double>>> getHourClientsDataRecords(long locationId, String startDateStr, String endDateStr) {
		
		def locations = null;
		if(locationId == 0){
			//If must get the data from all Locations and it is not Help Desk or Admin must filter the data for associated Locations
			locations = userSessionService.getPermitedLocations();
		}
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
		
		Date startDate = dateFormatter.parse(startDateStr + " 00:00:00");
		Date endDate = dateFormatter.parse(endDateStr + " 23:59:59");
		
		dateFormatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
		
		def results = getClientsData(startDate, endDate, locationId, locations);
				
		//def dataResult = BoxSummaryData.listHourClientsChartData(0, 0, "l.name", "asc", "", locationId, startDate, endDate, locations).list();
		
		//BoxSummaryData boxData = null;
		
		//dateFormatter = new SimpleDateFormat("MM/dd/yyyy");
		
		HashMap<String, HashMap<Integer, HashMap<String, Double>>> locationsMap = new HashMap<String, HashMap<Integer, HashMap<String, Double>>>();
		for(row in results){
			//boxData = row;
			// [55, 2015-01-21 00:00:00.0, 17, ExX Location, 4]
			// Save data for charts information
			if(locationsMap.containsKey(row[3])){
				//Location Map already exist
				HashMap<Integer, HashMap<String, Double>> locationData = locationsMap.get(row[3]);
				if(locationData.containsKey(row[2])){
					HashMap<String, Double> hourData = locationData.get(row[2]);
					//hourData.put("time", hourData.get("time") + boxData.time);
					hourData.put("reportedSessions", hourData.get("reportedSessions") + row[4]);
					//hourData.put("totalSessions", hourData.get("totalSessions") + boxData.totalSessions);
					locationData.put(row[2], hourData);
				} else {
					HashMap<String, Double> hourData = new HashMap<String, Double>();
					//hourData.put("time", boxData.time);
					hourData.put("reportedSessions", row[4]);
					//hourData.put("totalSessions", boxData.totalSessions);
					locationData.put(row[2], hourData);
				}
				locationsMap.put(row[3], locationData);
			} else {
				//Location Map do not exist
				HashMap<Integer, HashMap<String, Double>> locationData = new HashMap<Integer, HashMap<String, Double>>();
				HashMap<String, Double> hourData = new HashMap<String, Double>();
				//hourData.put("time", boxData.time);
				hourData.put("reportedSessions", row[4]);
				//hourData.put("totalSessions", boxData.totalSessions);
				locationData.put(row[2], hourData);
				locationsMap.put(row[3], locationData);
			}
		}
		return locationsMap;
	} // End of getDataRecords
	
	/**
	 * Return the records filtered by dates and Location of the BoxData table
	 *
	 * @param locationId	Location Id to filter; 0 when not filter must be applied
	 * @param startDateStr	Start Date with format MM/dd/yyyy
	 * @param endDateStr	End Date with format MM/dd/yyyy
	 * @return				HashMap with information
	 */
	public HashMap<String, HashMap<Integer, HashMap<String, Double>>> getDailyClientsDataRecords(long locationId, String startDateStr, String endDateStr) {
		
		def locations = null;
		if(locationId == 0 && !userSessionService.isAdminOrHelpDesk()){
			//If must get the data from all Locations and it is not Help Desk or Admin must filter the data for associated Locations
			locations = userSessionService.getPermitedLocations();
		}
				
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
		
		Date startDate = dateFormatter.parse(startDateStr + " 00:00:00");
		Date endDate = dateFormatter.parse(endDateStr + " 23:59:59");
		
		def dataResult = BoxSummaryData.listDailyClientsChartData(0, 0, "l.name", "asc", "", locationId, startDate, endDate, locations).list();
		
		BoxSummaryData boxData = null;
		
		dateFormatter = new SimpleDateFormat("MM/dd");
		
		HashMap<String, HashMap<Integer, HashMap<String, Double>>> locationsMap = new HashMap<String, HashMap<Integer, HashMap<String, Double>>>();
		for(row in dataResult){
			//boxData = row;
			//return values format [2, 2015-01-22 00:00:00.0, Curri Lab, 7]
			// Save data for charts information
			if(locationsMap.containsKey(row[2])){
				//Location Map already exist
				HashMap<Integer, HashMap<String, Double>> locationData = locationsMap.get(row[2]);
				if(locationData.containsKey(row[1])){
					HashMap<String, Double> hourData = locationData.get(row[1]);
					//hourData.put("time", hourData.get("time") + boxData.time);
					hourData.put("reportedSessions", hourData.get("reportedSessions") + row[3]);
					//hourData.put("totalSessions", hourData.get("totalSessions") + boxData.totalSessions);
					locationData.put(dateFormatter.format(row[1]), hourData);
				} else {
					HashMap<String, Double> hourData = new HashMap<String, Double>();
					//hourData.put("time", boxData.time);
					hourData.put("reportedSessions", row[3]);
					//hourData.put("totalSessions", boxData.totalSessions);
					locationData.put(dateFormatter.format(row[1]), hourData);
				}
				locationsMap.put(row[2], locationData);
			} else {
				//Location Map do not exist
				HashMap<Integer, HashMap<String, Double>> locationData = new HashMap<Integer, HashMap<String, Double>>();
				HashMap<String, Double> hourData = new HashMap<String, Double>();
				//hourData.put("time", boxData.time);
				hourData.put("reportedSessions", row[3]);
				//hourData.put("totalSessions", boxData.totalSessions);
				locationData.put(dateFormatter.format(row[1]), hourData);
				locationsMap.put(row[2], locationData);
			}
		}
		
		return locationsMap;
	} // End of getDataRecords
		
	/**
	 * Generates the Excel File with the information depending of parameters
	 *
	 * @param response	Request response object
	 * @param params	Request params; must has the locationId, start date and end date
	 */
	public void generateExcelFile(response, params){
		
		long locationId = params.locationId? Long.parseLong(params.locationId):0;
		String startDateStr = params.startDate;
		String endDateStr = params.endDate;
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
		Date startDate = dateFormatter.parse(startDateStr + " 00:00:00");
		Date endDate = dateFormatter.parse(endDateStr + " 23:59:59");
		//Get Location Name
		String locationName = "All";
		def locations = null;
		if(locationId != 0){
			CompanyLocation location = CompanyLocation.read(locationId);
			locationName = location.name;
		} else {
			//If must get the data from all Locations
			if(!userSessionService.isAdminOrHelpDesk()){
				//If not Help Desk or Admin must filter the data for associated Locations
				locations = userSessionService.getPermitedLocations();
			}
		}
		
		// Set our header and content type
		response.setContentType('application/vnd.ms-excel');
		response.setHeader('Content-Disposition', 'Attachment;Filename="UsageReport.xls"');
		
		WritableWorkbook workbook = Workbook.createWorkbook(response.outputStream);
		
		def headers = ["Venue","Server Name","Server Serial","Date",
			"Port","Channel Name","Hour","Time in Seconds","Max # of Sessions","Total # of Sessions"];
		def headersHourly = ["Venue","Server Name","Server Serial","Date",
			"Hour","Total # of Clients"];
		def headersDaily = ["Venue","Server Name","Server Serial","Date", "Total # of Clients"];
		
		//Create the Sheet and include the Column Headers
		WritableSheet sheet = ExcelGeneratorUtil.createTableSheet(workbook, "Streaming", headers, 7);
		WritableSheet sheetHourly = ExcelGeneratorUtil.createTableSheet(workbook, "Hourly Clients", headersHourly, 7);
		WritableSheet sheetDaily = ExcelGeneratorUtil.createTableSheet(workbook, "Daily Clients", headersDaily, 7);
				
		//Add params Information to the file
		
		// Style format of the header report
		WritableFont headerReport = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD, false);
		WritableCellFormat headerReportFormat = new WritableCellFormat (headerReport);
		headerReportFormat.setAlignment(Alignment.CENTRE);
		
		//Style format of the Location and Dates Titles
		WritableFont paramsTitle = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD, false);
		WritableCellFormat paramsTitleFormat = new WritableCellFormat (paramsTitle);
		
		sheet.mergeCells(1, 1, 10, 1);
		sheet.addCell(new Label(1, 1, "Venue Servers Usage Report", headerReportFormat));
		
		sheet.addCell(new Label(1, 3, "Venue", paramsTitleFormat));
		sheet.addCell(new Label(3, 3, locationName));
		
		sheet.addCell(new Label(6, 3, "Generator User", paramsTitleFormat));
		sheet.addCell(new Label(8, 3, userSessionService.getCurrentUserEmail()));
		
		sheet.addCell(new Label(1, 5, "Start Date", paramsTitleFormat));
		sheet.addCell(new Label(3, 5, startDateStr));
		
		sheet.addCell(new Label(6, 5, "End Date", paramsTitleFormat));
		sheet.addCell(new Label(8, 5, endDateStr));
		/**********Hourly Clients*************/
		sheetHourly.mergeCells(1, 1, 10, 1);
		sheetHourly.addCell(new Label(1, 1, "Venue Servers Usage Report", headerReportFormat));
		
		sheetHourly.addCell(new Label(1, 3, "Venue", paramsTitleFormat));
		sheetHourly.addCell(new Label(3, 3, locationName));
		
		sheetHourly.addCell(new Label(6, 3, "Generator User", paramsTitleFormat));
		sheetHourly.addCell(new Label(8, 3, userSessionService.getCurrentUserEmail()));
		
		sheetHourly.addCell(new Label(1, 5, "Start Date", paramsTitleFormat));
		sheetHourly.addCell(new Label(3, 5, startDateStr));
		
		sheetHourly.addCell(new Label(6, 5, "End Date", paramsTitleFormat));
		sheetHourly.addCell(new Label(8, 5, endDateStr));
		/************************/
		/**********Daily Clients*************/
		sheetDaily.mergeCells(1, 1, 10, 1);
		sheetDaily.addCell(new Label(1, 1, "Venue Servers Usage Report", headerReportFormat));
		
		sheetDaily.addCell(new Label(1, 3, "Venue", paramsTitleFormat));
		sheetDaily.addCell(new Label(3, 3, locationName));
		
		sheetDaily.addCell(new Label(6, 3, "Generator User", paramsTitleFormat));
		sheetDaily.addCell(new Label(8, 3, userSessionService.getCurrentUserEmail()));
		
		sheetDaily.addCell(new Label(1, 5, "Start Date", paramsTitleFormat));
		sheetDaily.addCell(new Label(3, 5, startDateStr));
		
		sheetDaily.addCell(new Label(6, 5, "End Date", paramsTitleFormat));
		sheetDaily.addCell(new Label(8, 5, endDateStr));
		/************************/
		dateFormatter = new SimpleDateFormat("MM/dd/yyyy");
		
		//Format style of the data table
		WritableFont dataFont = new WritableFont(WritableFont.TIMES, 8, WritableFont.NO_BOLD, false);
		WritableCellFormat dataFormat = new WritableCellFormat (dataFont);
		dataFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

		int rowIndex = 8;
		
		//def dataResult = BoxData.listData(0, 0, "l.name", "asc", "", locationId, startDate, endDate, locations).list();
		def dataResult = getStreamingDataForTable(0, 0, 'l.name', 'asc', "", locationId, startDate, endDate, locations);
		BoxData boxData = null;
		Box box;
		
		for(row in dataResult){
			/*boxData = row;
			ExcelGeneratorUtil.addTableRow(sheet, rowIndex, [boxData.location.name,boxData.boxName, boxData.serial,
				dateFormatter.format(boxData.date), boxData.port, boxData.channelName, boxData.hour,
				boxData.time, boxData.maxSessions, boxData.totalSessions], dataFormat);*/
			ExcelGeneratorUtil.addTableRow(sheet, rowIndex, [row[2], row[3], row[0],
				dateFormatter.format(row[4]), row[5], row[6], row[7],
				row[1], row[8], row[9]], dataFormat);
			rowIndex++;
		}
		
		/********Hourly Clients Data ***********/
		//def dataResultHourly = BoxSummaryData.listHourClientsData(0, 0, "l.name", "asc", "", locationId, startDate, endDate, locations).list();
		def dataResultHourly = getHourlyClientsDataForTable(0, 0, "l.name", "asc", "", locationId, startDate, endDate, locations);
		//BoxSummaryData hourlyData = null;
		rowIndex = 8;
		
		for(row in dataResultHourly){
			//hourlyData = row;
			/*ExcelGeneratorUtil.addTableRow(sheetHourly, rowIndex, [hourlyData.location.name, hourlyData.boxName, hourlyData.serial,
				dateFormatter.format(hourlyData.date),  hourlyData.hour,
				hourlyData.reportedSessions], dataFormat);*/
			ExcelGeneratorUtil.addTableRow(sheetHourly, rowIndex, [row[0], row[1], row[2],
				dateFormatter.format(row[3]),  row[4],
				row[5]], dataFormat);
			rowIndex++;
		}
		/****************************************/
		/********Hourly Clients Data ***********/
		def dataResultDaily = BoxSummaryData.listDailyClientsData(0, 0, "l.name", "asc", "", locationId, startDate, endDate, locations).list();
		BoxSummaryData dailyData = null;
		rowIndex = 8;
		
		for(row in dataResultDaily){
			dailyData = row;
			ExcelGeneratorUtil.addTableRow(sheetDaily, rowIndex, [dailyData.location.name, dailyData.boxName, dailyData.serial,
				dateFormatter.format(dailyData.date),
				dailyData.reportedSessions], dataFormat);
			
			rowIndex++;
		}
		/****************************************/
		// Set the auto size to data columns, starts with 1 because the Util stars with that column
		ExcelGeneratorUtil.setColumnsAutosize(sheet, 1, 10);
		ExcelGeneratorUtil.setColumnsAutosize(sheetHourly, 1, 10);
		ExcelGeneratorUtil.setColumnsAutosize(sheetDaily, 1, 10);
		
		workbook.write();
		workbook.close();
	}
	
	/**
	 * Generates the Excel File with the information depending of parameters
	 *
	 * @param response	Request response object
	 * @param params	Request params; must has the locationId, start date and end date
	 */
	public void generateMainMetricsExcelFile(response, params){
		
		long locationId = params.locationId? Long.parseLong(params.locationId):0;
		String startDateStr = params.startDate;
		String endDateStr = params.endDate;
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
		Date startDate = dateFormatter.parse(startDateStr + " 00:00:00");
		Date endDate = dateFormatter.parse(endDateStr + " 23:59:59");
		//Get Location Name
		String locationName = "All";
		def locations = null;
		if(locationId != 0){
			CompanyLocation location = CompanyLocation.read(locationId);
			locationName = location.name;
		} else {
			//If must get the data from all Locations
			if(!userSessionService.isAdminOrHelpDesk()){
				//If not Help Desk or Admin must filter the data for associated Locations
				locations = userSessionService.getPermitedLocations();
			}
		}
		
		// Set our header and content type
		response.setContentType('application/vnd.ms-excel');
		response.setHeader('Content-Disposition', 'Attachment;Filename="MainMetricsUsageReport.xls"');
		
		WritableWorkbook workbook = Workbook.createWorkbook(response.outputStream);
		
		//Create the Sheet and include the Column Headers
		def headers = ["Venue","Date","Unique People"];
		def headersTime = ["Venue","Date","Minutes"];
		def headersSimultaneousPeak = ["Venue","Date","Peak Number"];
		
		WritableSheet sheetUniquePeople = ExcelGeneratorUtil.createTableSheet(workbook, "UniquePeople", headers, 7);
		WritableSheet sheetTime = ExcelGeneratorUtil.createTableSheet(workbook, "MinutesUsed", headersTime, 7);
		WritableSheet sheetSimultaneous = ExcelGeneratorUtil.createTableSheet(workbook, "SimultaneousPeak", headersSimultaneousPeak, 7);
				
		//Add params Information to the file
		
		// Style format of the header report
		WritableFont headerReport = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD, false);
		WritableCellFormat headerReportFormat = new WritableCellFormat (headerReport);
		headerReportFormat.setAlignment(Alignment.CENTRE);
		
		//Style format of the Location and Dates Titles
		WritableFont paramsTitle = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD, false);
		WritableCellFormat paramsTitleFormat = new WritableCellFormat (paramsTitle);
		
		sheetUniquePeople.mergeCells(1, 1, 5, 1);
		sheetUniquePeople.addCell(new Label(1, 1, "Number of unique people who sign on per day per location", headerReportFormat));
		
		sheetUniquePeople.addCell(new Label(1, 3, "Venue", paramsTitleFormat));
		sheetUniquePeople.addCell(new Label(2, 3, locationName));
		
		sheetUniquePeople.addCell(new Label(4, 3, "Generator User", paramsTitleFormat));
		sheetUniquePeople.addCell(new Label(5, 3, userSessionService.getCurrentUserEmail()));
		
		sheetUniquePeople.addCell(new Label(1, 5, "Start Date", paramsTitleFormat));
		sheetUniquePeople.addCell(new Label(2, 5, startDateStr));
		
		sheetUniquePeople.addCell(new Label(4, 5, "End Date", paramsTitleFormat));
		sheetUniquePeople.addCell(new Label(5, 5, endDateStr));
		
		/********** Number of Minutes *************/
		sheetTime.mergeCells(1, 1, 10, 1);
		sheetTime.addCell(new Label(1, 1, "Total number of minutes used per day per location", headerReportFormat));
		
		sheetTime.addCell(new Label(1, 3, "Venue", paramsTitleFormat));
		sheetTime.addCell(new Label(2, 3, locationName));
		
		sheetTime.addCell(new Label(4, 3, "Generator User", paramsTitleFormat));
		sheetTime.addCell(new Label(5, 3, userSessionService.getCurrentUserEmail()));
		
		sheetTime.addCell(new Label(1, 5, "Start Date", paramsTitleFormat));
		sheetTime.addCell(new Label(2, 5, startDateStr));
		
		sheetTime.addCell(new Label(4, 5, "End Date", paramsTitleFormat));
		sheetTime.addCell(new Label(5, 5, endDateStr));
		
		/************************/
		/**********Daily Clients*************/
		sheetSimultaneous.mergeCells(1, 1, 10, 1);
		sheetSimultaneous.addCell(new Label(1, 1, "Peak number of simultaneous users per day per location", headerReportFormat));
		
		sheetSimultaneous.addCell(new Label(1, 3, "Venue", paramsTitleFormat));
		sheetSimultaneous.addCell(new Label(2, 3, locationName));
		
		sheetSimultaneous.addCell(new Label(4, 3, "Generator User", paramsTitleFormat));
		sheetSimultaneous.addCell(new Label(5, 3, userSessionService.getCurrentUserEmail()));
		
		sheetSimultaneous.addCell(new Label(1, 5, "Start Date", paramsTitleFormat));
		sheetSimultaneous.addCell(new Label(2, 5, startDateStr));
		
		sheetSimultaneous.addCell(new Label(4, 5, "End Date", paramsTitleFormat));
		sheetSimultaneous.addCell(new Label(5, 5, endDateStr));
		
		dateFormatter = new SimpleDateFormat("MM/dd/yyyy");
		
		//Format style of the data table
		WritableFont dataFont = new WritableFont(WritableFont.TIMES, 8, WritableFont.NO_BOLD, false);
		WritableCellFormat dataFormat = new WritableCellFormat (dataFont);
		dataFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

		/******** Unique People Data ***********/
		int rowIndex = 8;
		
		/* Adding Number of Unique People Data */				
		SimpleDateFormat tempFormatter = new SimpleDateFormat("MM/dd/yyyy");
		def dataResult = BoxSummaryData.listDailyClientsChartData(0, 0, "l.name", "asc", "", locationId, startDate, endDate, locations).list();
		for(row in dataResult) {
			ExcelGeneratorUtil.addTableRow(sheetUniquePeople, rowIndex, [row[2], tempFormatter.format(row[1]), row[3]], dataFormat);			
			rowIndex++;
		}
				
		/******** Simultaneous Peak an Time Data ***********/
		rowIndex = 8;
		
		dataResult = getStreamingDataPerDay(locationId, startDate, endDate, locations);
					
		for(row in dataResult){
			ExcelGeneratorUtil.addTableRow(sheetTime, rowIndex, [row[1], tempFormatter.format(row[5]), row[2]/60], dataFormat);
			ExcelGeneratorUtil.addTableRow(sheetSimultaneous, rowIndex, [row[1], tempFormatter.format(row[5]), row[3]], dataFormat);
			rowIndex++;
		}		
		
		// Set the auto size to data columns, starts with 1 because the Util stars with that column	
		ExcelGeneratorUtil.setColumnsAutosize(sheetUniquePeople, 1, 10);
		ExcelGeneratorUtil.setColumnsAutosize(sheetTime, 1, 10);
		ExcelGeneratorUtil.setColumnsAutosize(sheetSimultaneous, 1, 10);
		
		workbook.write();
		workbook.close();
	}
	
	public def getClientsData(Date startDate, Date endDate, long locationId, def locations){
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		
		// Get the current Hiberante session.
		final session = sessionFactory.currentSession;
		
		String query = "select loc.id as 'LocId', ";
		
		query += " DATE_ADD(DATE_ADD(data.date, INTERVAL data.hour HOUR) , ";
		query += "    INTERVAL TIMESTAMPDIFF(HOUR, UTC_TIMESTAMP(), CONVERT_TZ(UTC_TIMESTAMP(), 'Etc/UTC', loc.timezone)) HOUR) as LocDate,";
		
		query += " HOUR(";
		query += " DATE_ADD(DATE_ADD(data.date, INTERVAL data.hour HOUR) , ";
		query += "    INTERVAL TIMESTAMPDIFF(HOUR, UTC_TIMESTAMP(), CONVERT_TZ(UTC_TIMESTAMP(), 'Etc/UTC', loc.timezone)) HOUR)";
		query += " ) as 'LocHour', ";
		query += "loc.name as 'LocName',";
		query += " sum(data.reported_sessions) as y4_";
		query += " from box_summary_data data ";
		query += " inner join company_location loc on data.location_id=loc.id";
		query += " where data.summary_type = :summaryType";
		if(locationId != 0){
			query += " and loc.id=" + locationId;
		} else {
			String locationsStr = "";
			for(loc in locations){
				locationsStr += (locationsStr == "")? loc.id: "," + loc.id;
			}
			query += " and loc.id in (" + locationsStr + ")"
		}
		
		query += " group by loc.id, data.date, data.hour ";
		query += " having LocDate >= :startDate and LocDate <= :endDate";
		query += " order by loc.name asc;";
		// Create native SQL query.
		final sqlQuery = session.createSQLQuery(query)
		// Use Groovy with() method to invoke multiple methods on the sqlQuery object.
		final results = sqlQuery.with {
			setString('summaryType', 'HourlyClients')
			setString('startDate', dateFormatter.format(startDate))
			setString('endDate', dateFormatter.format(endDate))
			list()
		}
		
		return results;
	}
	
	def getStreamingData(long locationId, Date startDate, Date endDate, def locations){
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		
		// Get the current Hiberante session.
		final session = sessionFactory.currentSession;
		
		String query = "SELECT l.id, l.name, ";
		query += " HOUR(";
		query += " DATE_ADD(DATE_ADD(bd.date, INTERVAL bd.hour HOUR) , ";
		query += "    INTERVAL TIMESTAMPDIFF(HOUR, UTC_TIMESTAMP(), CONVERT_TZ(UTC_TIMESTAMP(), 'Etc/UTC', l.timezone)) HOUR)";
		query += " ) as 'LocHour', ";
		query += " SUM(bd.time), MAX(bd.max_sessions), SUM(bd.total_sessions), ";
		
		query += " DATE_ADD(DATE_ADD(bd.date, INTERVAL bd.hour HOUR) , ";
		query += "    INTERVAL TIMESTAMPDIFF(HOUR, UTC_TIMESTAMP(), CONVERT_TZ(UTC_TIMESTAMP(), 'Etc/UTC', l.timezone)) HOUR) as LocDate";
				
		query += " from box_data bd";
		query += " inner join company_location l on bd.location_id=l.id ";
		if(locationId != 0){
			query += " where l.id = " + locationId;
		} else {
			String locationsStr = "";
			for(loc in locations){
				locationsStr += (locationsStr == "")? loc.id: "," + loc.id;
			}
			query += " where l.id in (" + locationsStr + ")"
		}
		query += " group by l.id, bd.date, bd.hour";
		query += " having LocDate >= :startDate and LocDate <= :endDate";
		query += " order by l.name asc";
		
		// Create native SQL query.
		final sqlQuery = session.createSQLQuery(query)
		// Use Groovy with() method to invoke multiple methods on the sqlQuery object.
		final results = sqlQuery.with {
			setString('startDate', dateFormatter.format(startDate))
			setString('endDate', dateFormatter.format(endDate))
			list()
		}
		
		return results;
	}
	
	def getStreamingDataForTable(int start, int length, String columnOrderName, String orderDir,
		String searchText, long locationId, Date startDate, Date endDate, def locations){
				
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		
		// Get the current Hiberante session.
		final session = sessionFactory.currentSession;
		
		String query = "SELECT bd.serial, bd.time, l.name, bd.box_name,";
		query += " DATE_ADD(DATE_ADD(bd.date, INTERVAL bd.hour HOUR) , ";
		query += "    INTERVAL TIMESTAMPDIFF(HOUR, UTC_TIMESTAMP(), CONVERT_TZ(UTC_TIMESTAMP(), 'Etc/UTC', l.timezone)) HOUR) as LocDate, ";
		query += " bd.port, bd.channel_name, ";
		query += " HOUR(";
		query += " DATE_ADD(DATE_ADD(bd.date, INTERVAL bd.hour HOUR) , ";
		query += "    INTERVAL TIMESTAMPDIFF(HOUR, UTC_TIMESTAMP(), CONVERT_TZ(UTC_TIMESTAMP(), 'Etc/UTC', l.timezone)) HOUR)";
		query += " ) as 'LocHour', ";
		query += " bd.max_sessions, bd.total_sessions";
		query += " from box_data bd";
		query += " inner join company_location l on bd.location_id=l.id ";
		if(locationId != 0){
			query += " WHERE l.id = " + locationId;
		} else {
			if( locations ){
				String locationsStr = "";
				for(loc in locations){
					locationsStr += (locationsStr == "")? loc.id: "," + loc.id;
				}
				query += " WHERE l.id in (" + locationsStr + ")"
			}
		}
		if( searchText != ""){
			query += ((locationId !=0 || locations)? " AND ":" WHERE ") + "(l.name like :searchText or bd.box_name like :searchText or bd.serial like :searchText or bd.port like :searchText or bd.channel_name like :searchText)";
		}
		//query += " group by l.id, bd.date, bd.hour, bd.serial, bd.port";
		query += " HAVING LocDate >= :startDate and LocDate <= :endDate";
		
		if(columnOrderName == "hour"){
			query += " ORDER BY date, hour " + orderDir;
		} else {
			query += " ORDER BY " + columnOrderName + " " + orderDir;
		}
		
		if(length != 0){
			query += " LIMIT " + start + ", " + length;
		}
		
		// Create native SQL query.
		final sqlQuery = session.createSQLQuery(query);
		final results = sqlQuery.with {
			setString('startDate', dateFormatter.format(startDate))
			setString('endDate', dateFormatter.format(endDate))
			if( searchText != ""){
				setString('searchText', "%" + searchText + "%")
			}
			list()
		}
		
		return results;
	}
	
	public int getStreamingDataCountForTable(String searchText, long locationId, Date startDate, Date endDate, def locations){
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		
		// Get the current Hiberante session.
		final session = sessionFactory.currentSession;
		
		String query = "SELECT count(*)";
		query += " from box_data bd";
		query += " inner join company_location l on bd.location_id=l.id";
		if(locationId != 0){
			query += " WHERE l.id = " + locationId;
		} else {
			if(locations){
				String locationsStr = "";
				for(loc in locations){
					locationsStr += (locationsStr == "")? loc.id: "," + loc.id;
				}
				query += " WHERE l.id in (" + locationsStr + ")"
			}
		}
		query += (locationId != 0 || locations)? " AND ":" WHERE ";
		query += " DATE_ADD(DATE_ADD(bd.date, INTERVAL bd.hour HOUR), INTERVAL TIMESTAMPDIFF(HOUR, UTC_TIMESTAMP(),";
		query += " CONVERT_TZ(UTC_TIMESTAMP(), 'Etc/UTC', l.timezone)) HOUR) between :startDate AND :endDate";
		
		if( searchText != ""){
			query += " and (l.name like :searchText or bd.box_name like :searchText or bd.serial like :searchText or bd.port like :searchText or bd.channel_name like :searchText)";
		}
		
		// Create native SQL query.
		final sqlQuery = session.createSQLQuery(query);
		final results = sqlQuery.with {
			setString('startDate', dateFormatter.format(startDate))
			setString('endDate', dateFormatter.format(endDate))
			if( searchText != ""){
				setString('searchText', "%" + searchText + "%")
			}			
			list()
		}
		
		int counter = results[0];
		return counter;
	}
	
	public def getHourlyClientsDataForTable(int start, int length, String columnOrderName, String orderDir,
		String searchText, long locationId, Date startDate, Date endDate, def locations){
				
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		
		// Get the current Hiberante session.
		final session = sessionFactory.currentSession;
		
		String query = "SELECT l.name, data.box_name, data.serial,";
		query += " DATE_ADD(DATE_ADD(data.date, INTERVAL data.hour HOUR) , ";
		query += "    INTERVAL TIMESTAMPDIFF(HOUR, UTC_TIMESTAMP(), CONVERT_TZ(UTC_TIMESTAMP(), 'Etc/UTC', l.timezone)) HOUR) as LocDate, ";
		query += " HOUR(";
		query += " DATE_ADD(DATE_ADD(data.date, INTERVAL data.hour HOUR) , ";
		query += "    INTERVAL TIMESTAMPDIFF(HOUR, UTC_TIMESTAMP(), CONVERT_TZ(UTC_TIMESTAMP(), 'Etc/UTC', l.timezone)) HOUR)";
		query += " ) as 'LocHour', ";
		query += " data.reported_sessions";
		
		query += " FROM box_summary_data data ";
		query += " INNER JOIN company_location l ON data.location_id=l.id";
		query += " WHERE data.summary_type = :summaryType";
		if(locationId != 0){
			query += " AND l.id = " + locationId;
		} else {
			if( locations ){
				String locationsStr = "";
				for(loc in locations){
					locationsStr += (locationsStr == "")? loc.id: "," + loc.id;
				}
				query += " AND l.id in (" + locationsStr + ")"
			}
		}
				
		if( searchText != ""){
			query += " AND (l.name like :searchText or data.box_name like :searchText or data.serial like :searchText)";
		}
		query += " group by l.id, data.date, data.hour, data.serial";
		query += " having LocDate >= :startDate and LocDate <= :endDate";
		
		if(columnOrderName == "hour"){
			query += " ORDER BY data.date, data.hour " + orderDir;
		} else {
			query += " ORDER BY " + columnOrderName + " " + orderDir;
		}
		
		if(length != 0){
			query += " LIMIT " + start + ", " + length;
		}
		
		// Create native SQL query.
		final sqlQuery = session.createSQLQuery(query);
		final results = sqlQuery.with {
			setString('startDate', dateFormatter.format(startDate))
			setString('endDate', dateFormatter.format(endDate))
			setString('summaryType', 'HourlyClients')
			if( searchText != ""){
				setString('searchText', "%" + searchText + "%")
			}
			list()
		}
		
		return results;
	}
	
	public int getHourlyClientsDataCountForTable(String searchText, long locationId, Date startDate, Date endDate, def locations){
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		
		// Get the current Hiberante session.
		final session = sessionFactory.currentSession;
		
		String query = "SELECT count(*)";
		query += " FROM box_summary_data data ";
		query += " INNER JOIN company_location l ON data.location_id=l.id";
		query += " WHERE data.summary_type = :summaryType";
		if(locationId != 0){
			query += " AND l.id=" + locationId;
		} else {
			String locationsStr = "";
			for(loc in locations){
				locationsStr += (locationsStr == "")? loc.id: "," + loc.id;
			}
			query += " AND l.id in (" + locationsStr + ")"
		}
		query += " AND DATE_ADD(DATE_ADD(data.date, INTERVAL data.hour HOUR), INTERVAL TIMESTAMPDIFF(HOUR, UTC_TIMESTAMP(),";
		query += " CONVERT_TZ(UTC_TIMESTAMP(), 'Etc/UTC', l.timezone)) HOUR) between :startDate AND :endDate";
		
		if( searchText != ""){
			query += " AND (l.name like :searchText or data.box_name like :searchText or data.serial like :searchText)";
		}
		
		// Create native SQL query.
		final sqlQuery = session.createSQLQuery(query)
		final results = sqlQuery.with {
			setString('summaryType', 'HourlyClients')
			setString('startDate', dateFormatter.format(startDate))
			setString('endDate', dateFormatter.format(endDate))
			if( searchText != ""){
				setString('searchText', "%" + searchText + "%")
			}
			list()
		}
		
		int counter = results[0];
		return counter;
	}
	
	/**
	 * This method will return a Json object with the data information filtered
	 * by the parameters received; it's use for the generation of the report charts.
	 *
	 * @param params		Request params
	 * @return				Data structure for the charts
	 * @throws Exception	When something goes wrong
	 */
	public HashMap getMainChartsData(params) throws Exception {
	
		HashMap jsonMap = new HashMap();
		
		int maximumClientsNumber = 0;
		int maximumDailyClientsNumber = 0;
		
		// Get params information
		long locationId = params.locationId? params.locationId.toLong():0;
		String startDateStr = params["startDate"];
		String endDateStr = params["endDate"];
		String selectedStr = params["selectedDate"];
		
		// init daily clients chart data
		//HH converts hour in 24 hours format (0-23), day calculation
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
 
		Date d1 = null;
		Date d2 = null;
 
		d1 = format.parse(startDateStr);
		d2 = format.parse(endDateStr);
 
		//in milliseconds
		long diff = d2.getTime() - d1.getTime();
		long diffDays = diff / (24 * 60 * 60 * 1000);
		
		HashMap<String, HashMap<Integer, HashMap<String, Double>>> locationsMapDaily =
			getDailyClientsDataRecords(locationId, startDateStr, endDateStr);
	
		//Put data to json structure
				
		def chartDataDaily = [];
		
		if(locationsMapDaily.size() > 0){
			def headersData = [];
			headersData.add("X");
			for(Entry<String, Object> entry: locationsMapDaily.entrySet()) {
				headersData.add(entry.getKey());
			}
			chartDataDaily.add(headersData);
			for(int i=0; i <= diffDays; i++) {
				
				format = new SimpleDateFormat("MM/dd");
				Calendar c = Calendar.getInstance();
				c.setTime(d1); // Now use today date.
				c.add(Calendar.DATE, i); // Adding i days
				//String output = sdf.format(c.getTime());
				
				def rowData = [];
				rowData.add(""+format.format(c.getTime()));
				for(Entry<String, Object> entry: locationsMapDaily.entrySet()) {
					HashMap<String, HashMap<Integer, HashMap<String, Double>>> locationData = entry.getValue();
					if(locationData.containsKey(""+format.format(c.getTime()))){
						HashMap<String, Double> hourData = locationData.get(""+format.format(c.getTime()));
						rowData.add(hourData.get("reportedSessions"));
						maximumDailyClientsNumber = (maximumDailyClientsNumber > hourData.get("reportedSessions"))? maximumDailyClientsNumber:hourData.get("reportedSessions");
					} else {
						rowData.add(0);
					}
				}
				chartDataDaily.add(rowData);
			}
		}
		
		jsonMap["locationsCountDaily"] = locationsMapDaily.size();
		jsonMap["dataDaily"] = chartDataDaily;
		jsonMap["maximumDailyClientsNumber"] = maximumDailyClientsNumber;
		
		
		/**/
		HashMap<String, HashMap<Integer, HashMap<String, Double>>> locationsMap =
			getStreamingDataPerDayRecords(locationId, startDateStr, endDateStr);
	
		//Put data to json structure
		// Define Chart Data
		int maximumMaxNumber = 0;
		int maximumTimeNumber = 0;
		int maximumTotalNumber = 0;
		def chartData = [];
		def totalSessionsChartData = [];
		def maxSessionsChartData = [];
		int TotalTime = 0;
		double timeInMinutes = 0;
				
		if(locationsMap.size() > 0){
			def headersData = [];
			headersData.add("X");
			for(Entry<String, Object> entry: locationsMap.entrySet()) {
				headersData.add(entry.getKey());
			}
			chartData.add(headersData);
			totalSessionsChartData.add(headersData);
			maxSessionsChartData.add(headersData);
			
			DecimalFormat decimalFormatter = new DecimalFormat("######");
			
			SimpleDateFormat fullDateFormat = new SimpleDateFormat("MM/dd/yyyy");
			format = new SimpleDateFormat("MM/dd");
			
			for(int i=0; i <= diffDays; i++){
				
				Calendar c = Calendar.getInstance();
				c.setTime(d1); // Now use today date.
				c.add(Calendar.DATE, i); // Adding i days
				
				def rowData = [];
				def rowTotalSessionsData = [];
				def rowMaxSessionsData = [];
				
				String shortDate = format.format(c.getTime()) 
				String checkingDate = fullDateFormat.format(c.getTime());
				rowData.add(shortDate);
				rowTotalSessionsData.add(shortDate);
				rowMaxSessionsData.add(shortDate);
				
				for(Entry<String, HashMap<String, Double>> entry: locationsMap.entrySet()){
					HashMap<String, HashMap<String, Double>> locationData = entry.getValue();
					if(locationData.containsKey(checkingDate)){
						HashMap<String, Double> hourData = locationData.get(checkingDate);
						//Display the time in minutes
						timeInMinutes = Integer.parseInt(""+decimalFormatter.format(hourData.get("time")/60));
						rowData.add(timeInMinutes);
						rowTotalSessionsData.add(hourData.get("totalSessions"));
						rowMaxSessionsData.add(hourData.get("maxSessions"));
						TotalTime += hourData.get("time");
					} else {
						rowData.add(0);
						rowTotalSessionsData.add(0);
						rowMaxSessionsData.add(0);
					}
				}
				
				chartData.add(rowData);
				totalSessionsChartData.add(rowTotalSessionsData);
				maxSessionsChartData.add(rowMaxSessionsData);
			}
		}
				
		String hms = String.format("%02d:%02d:%02d",
			TimeUnit.SECONDS.toHours(TotalTime),
			TimeUnit.SECONDS.toMinutes(TotalTime) -
			TimeUnit.HOURS.toMinutes(TimeUnit.SECONDS.toHours(TotalTime)), // The change is in this line
			TotalTime -
			TimeUnit.MINUTES.toSeconds(TimeUnit.SECONDS.toMinutes(TotalTime)));
		
		jsonMap["locationsCount"] = locationsMap.size();
		jsonMap["chartData"] = chartData;
		jsonMap["sessionsMaxData"] = maxSessionsChartData;
		jsonMap["TotalTime"] = hms;
		
		return jsonMap;
	} // End of getClientsChartData method
	
	/**
	 * Return the records filtered by dates and Location of the BoxData table
	 *
	 * @param locationId	Location Id to filter; 0 when not filter must be applied
	 * @param startDateStr	Start Date with format MM/dd/yyyy
	 * @param endDateStr	End Date with format MM/dd/yyyy
	 * @return				HashMap with information
	 */
	public HashMap<String, HashMap<Integer, HashMap<String, Double>>> 
		getStreamingDataPerDayRecords(long locationId, String startDateStr, String endDateStr) {
		
		def locations = null;
		if(locationId == 0 && !userSessionService.isAdminOrHelpDesk()){
			//If must get the data from all Locations and it is not Help Desk or Admin must filter the data for associated Locations
			locations = userSessionService.getPermitedLocations();
		}
				
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
		
		Date startDate = dateFormatter.parse(startDateStr + " 00:00:00");
		Date endDate = dateFormatter.parse(endDateStr + " 23:59:59");
		
		def dataResult = getStreamingDataPerDay(locationId, startDate, endDate, locations);
		//BoxData boxData = null;
		
		dateFormatter = new SimpleDateFormat("MM/dd/yyyy");
		
		HashMap<String, HashMap<Integer, HashMap<String, Double>>> locationsMap = 
			new HashMap<String, HashMap<Integer, HashMap<String, Double>>>();
			
		for(row in dataResult){
			//boxData = row;
			String recordDate = dateFormatter.format(row[5]);
			
			// Save data for charts information
			if(locationsMap.containsKey(row[1])){
				//Location Map already exist
				HashMap<String, HashMap<String, Double>> locationData = locationsMap.get(row[1]);
				HashMap<String, Double> locationDate = null;
				if(locationData.containsKey(recordDate)){
					locationDate = locationData.get(recordDate);
					locationDate.put("time", locationDate.get("time") + row[2]);
					if(locationDate.get("maxSessions") < row[3])
					{
						locationDate.put("maxSessions", row[3]);
					}
					locationDate.put("totalSessions", locationDate.get("totalSessions") + row[4]);
				} else {
					locationDate = new HashMap<String, Double>();
					locationDate.put("time", row[2]);
					locationDate.put("maxSessions", row[3]);
					locationDate.put("totalSessions", row[4]);
				}
				locationData.put(recordDate, locationDate);
				locationsMap.put(row[1], locationData);
			} else {
				//Location Map do not exist
				HashMap<String, Double> locationDate = new HashMap<String, Double>();
				locationDate.put("time", row[2]);
				locationDate.put("maxSessions", row[3]);
				locationDate.put("totalSessions", row[4]);
				
				HashMap<String, HashMap<String, Double>> locationMap = new HashMap<String, HashMap<String, Double>>();
				locationMap.put(recordDate, locationDate);
				locationsMap.put(row[1], locationMap);
			}
		}
		return locationsMap;
	} // End of getDataRecords
		
	def getStreamingDataPerDay(long locationId, Date startDate, Date endDate, def locations){
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		
		// Get the current Hiberante session.
		final session = sessionFactory.currentSession;
		
		String query = "SELECT l.id, l.name, ";
		query += " SUM(bd.time), MAX(bd.max_sessions), SUM(bd.total_sessions), ";
		
		query += " DATE_ADD(DATE_ADD(bd.date, INTERVAL bd.hour HOUR) , ";
		query += "    INTERVAL TIMESTAMPDIFF(HOUR, UTC_TIMESTAMP(), CONVERT_TZ(UTC_TIMESTAMP(), 'Etc/UTC', l.timezone)) HOUR) as LocDate";
				
		query += " from box_data bd";
		query += " inner join company_location l on bd.location_id=l.id ";
		if(locationId != 0){
			query += " where l.id = " + locationId;
		} else {
			if(locations){
				String locationsStr = "";
				for(loc in locations){
					locationsStr += (locationsStr == "")? loc.id: "," + loc.id;
				}
				query += " where l.id in (" + locationsStr + ")"
			}
		}
		query += " group by l.id, Date(LocDate)";
		query += " having LocDate >= :startDate and LocDate <= :endDate";
		query += " order by l.name asc";
		
		// Create native SQL query.
		final sqlQuery = session.createSQLQuery(query)
		// Use Groovy with() method to invoke multiple methods on the sqlQuery object.
		final results = sqlQuery.with {
			setString('startDate', dateFormatter.format(startDate))
			setString('endDate', dateFormatter.format(endDate))
			list()
		}
		
		return results;
	}
}
