package mycentralserver

import java.text.SimpleDateFormat;

import mycentralserver.box.Box;
import mycentralserver.box.BoxBlackList;
import mycentralserver.utils.BoxStatusEnum;
import mycentralserver.utils.Constants;

class BoxBlackListService {
	
	// Inject require services
	def boxService;
	def messageSource;
	def dataSource;

	/**
	 * Take the legacySerial for update the blackList when the box is upgrading the serial and certificate
	 * @param serial New serial number
	 * @param legacySerial Old serial number
	 * @return
	 */
	def updateBoxBlackListSerial (String serial, String legacySerial){
		try{
			log.info("updateBoxBlackListSerial serial["+serial+"]" +" legacy serial["+legacySerial+"]")
			BoxBlackList boxBlackList= BoxBlackList.findBySerial(legacySerial)
			if (boxBlackList != null) {
				boxBlackList.serial=serial
				boxBlackList.save(flush: true, failOnError: true)
			}
		}catch(e){
			e.printStackTrace()
			throw e
		}
	}

	def isBlackListed(String serial){
		BoxBlackList boxBlackListed=BoxBlackList.findBySerial(serial)
		if (boxBlackListed == null || boxBlackListed.dateStopWorking.compareTo(new Date()) > 0) {
			return false
		}
		return true
	}

	/**
	 * Try to add a box to the black list using one Id, will check if
	 * the box exists, add to black list and try to restart the box
	 * 
	 * @param id	Id of the box that wants to move to the black list
	 * @return		Empty string when no error or error when something goes wrong
	 */
	public String addToBlackListById(long id, String reason, Date stopWorking){		
		Box box = Box.read(id);
		String message = "";
		// Check that the box exists
		if( box ){
			// Check if the box is already black listed
			BoxBlackList boxBlackListed = BoxBlackList.findBySerial(box.serial);
			if(boxBlackListed) {
				message = messageSource.getMessage('box.is.black.listed', null, null);
			} else {
				message = addToBlackList(box.serial, reason, stopWorking);
			}
		} else {
			message = messageSource.getMessage('default.not.found.message', null, null);
		}
		return message;
	}
	
	/**
	 * This method will add a register of the box to the black list table and will try to restart
	 * the server to avoid connected boxes moved to the black list
	 * 
	 * @param serialNumber	Serial number of the box to black list
	 * @param reason		Why is been move to the black list
	 * @param stopWorking	When the box should not been allow to connect
	 * @return
	 */
	public String addToBlackList(String serialNumber, String reason, Date stopWorking) {
		
		BoxBlackList boxBlackList= new BoxBlackList();
		boxBlackList.serial = serialNumber;
		boxBlackList.reason = reason;
		boxBlackList.dateCreated = new Date();
		boxBlackList.dateStopWorking = stopWorking;
		
		if(boxBlackList.save()) {
			try {
				boxService.sendRemoteResetRequest(serialNumber);
			} catch(Exception e){
				log.warn("There was an error sending the reset request to the server: " + e.getMessage());
			}
			return "";
		} else {
			println boxBlackList.errors.allErrors.join(' \n')
			return messageSource.getMessage('box.block.error', null, null);
		}
	}
	
	/**
	 * This method will check the connected venue servers that are in the black
	 * list and will send a remote reset for the servers that reached the stop working date
	 */
	public void checkStopWorkingDate() {
		def boxesSerials = this.getConnectedBlackListedServerSerials();
		for(rowResult in boxesSerials){
			try {
				boxService.sendRemoteResetRequest(rowResult.get('serial'));
			} catch(Exception e){
				log.warn("There was an error sending the reset request to the server: " + e.getMessage());
			}
		}
	}
	
	/**
	 * Returns the list of serials of the connected venue servers on the
	 * black list that reached the stop working date
	 *  
	 * @return
	 */
	public List getConnectedBlackListedServerSerials() {
		
		Date nowDate = new Date();
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(nowDate);
		gc.add(Calendar.MINUTE, -7);
		Date lastConnectedTime = gc.getTime();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
		
		String query = "SELECT b.serial FROM box b INNER JOIN box_status s ON s.id = b.status_id";
		query += " JOIN box_black_list bl ON bl.serial = b.serial ";
		query += " WHERE s.description = '" + BoxStatusEnum.CONNECTED.value + "'";
		query += " AND b.last_updated >= '" + formatter.format(lastConnectedTime) + "'";
		query += " AND bl.date_stop_working < now()";
		
		groovy.sql.Sql sql = new groovy.sql.Sql(dataSource)
		log.info(sql)
		log.info("datasource " + dataSource)
		def t = sql.rows(query);
		sql.close();
		return t;
	}
}