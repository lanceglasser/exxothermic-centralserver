package mycentralserver

/**
 * This service will only handle the list of errors from the communication component in order to be use by the system
 * on different processes
 * 
 * @author Cecropia Solutions
 * @since 02/09/2014
 */
class MessageErrorService {

    public final static String ERROR_CODE_NO_ERROR = "0";
    public final static String ERROR_CODE_TIME_OUT = "1";
    public final static String ERROR_CODE_GENERAL_EXCEPTION = "2";
    public final static String ERROR_CODE_NO_CHANNELS = "3";
    public final static String ERROR_CODE_DEVICE_NOT_FOUND = "4";
    public final static String ERROR_CODE_UPDATE_FAIL = "5";
    public final static String ERROR_CODE_NO_CONNECTION = "6";

    /**
     * Default Constructor for service injection.
     */
    def MessageErrorService(){}

    def getMessageErrorDescription() {
        HashMap<String, String> errorsMessage = new HashMap<String, String>()
        errorsMessage.put(ERROR_CODE_NO_ERROR, "centralServerWebSocket.error.errorCodeNoError")
        errorsMessage.put(ERROR_CODE_TIME_OUT, "centralServerWebSocket.error.errorCodeTimeOut")
        errorsMessage.put(ERROR_CODE_GENERAL_EXCEPTION, "centralServerWebSocket.error.errorCodeGeneralException")
        errorsMessage.put(ERROR_CODE_NO_CHANNELS, "centralServerWebSocket.error.errorCodeNoChannels")
        errorsMessage.put(ERROR_CODE_DEVICE_NOT_FOUND, "centralServerWebSocket.error.errorCodeDeviceNotFound")
        errorsMessage.put(ERROR_CODE_UPDATE_FAIL, "centralServerWebSocket.error.errorCodeUpdateFail")
        errorsMessage.put(ERROR_CODE_NO_CONNECTION, "centralServerWebSocket.error.errorCodeNoConnection")
        return errorsMessage;
    }

    def getMessageErrorDescriptionForGetLog() {
        HashMap<String, String> errorsMessage = getMessageErrorDescription()
        errorsMessage.put(ERROR_CODE_NO_ERROR, "centralServerWebSocket.error.errorCodeNoError");
        errorsMessage.put(ERROR_CODE_TIME_OUT, "centralServerWebSocket.error.errorCodeFileCouldNotBeRetrieved"); // timeOut
        errorsMessage.put(ERROR_CODE_GENERAL_EXCEPTION, "centralServerWebSocket.error.errorCodeFileCouldNotBeRetrieved"); //GeneralException
        errorsMessage.put(ERROR_CODE_NO_CHANNELS, "centralServerWebSocket.error.errorCodeNoChannels");
        errorsMessage.put(ERROR_CODE_DEVICE_NOT_FOUND, "centralServerWebSocket.error.errorCodeDeviceNotFound");
        errorsMessage.put(ERROR_CODE_UPDATE_FAIL, "centralServerWebSocket.error.errorCodeUpdateFail");
        return errorsMessage;
    }
}