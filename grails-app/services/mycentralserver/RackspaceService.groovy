package mycentralserver

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils
import org.apache.commons.lang.ArrayUtils;

import mycentralserver.beans.ImageUploadResult;
import mycentralserver.custombuttons.CustomButton;
import mycentralserver.utils.CloudFilesPublish;
import mycentralserver.utils.Constants;
import mycentralserver.utils.FileHelper;
import mycentralserver.utils.Utils;

class RackspaceService {

	def grailsApplication
	
	/**
	 * This method receive an image file from a Form,
	 * validates the image and upload the file to the rackspace environment.
	 *
	 * @param tempPicture	Image file from the Form
	 * @param x				X position to crop
	 * @param y				Y position to crop
	 * @param w				Width to crop
	 * @param h				Height to crop
	 *
	 * @return				Rackspace Url of the file
	 * @throws Exception	When there is an error with the image file or the upload process
	 */
	public String uploadFile(file) throws Exception {
		
		try{
			def fileNameOrigin = file.getOriginalFilename();
			
			if (!file.isEmpty() && !fileNameOrigin.isEmpty()){
				
				//save file on server
				String fileFullPath = this.saveVideoFile( file );
				
				//upload file to rackspace
				CloudFilesPublish cloudService = new CloudFilesPublish(fileFullPath, grailsApplication.config.rackspaceUser, grailsApplication.config.apiKey);
				String fileUrl = cloudService.uploadFileToRackspace();
				
				//delete temp file
				File tempFile = new File(fileFullPath);
				FileUtils.deleteQuietly(tempFile);
				
				return fileUrl;
			} else {
				throw new Exception("upload.video.invalid.file");
			}
		}catch(e){
			e.printStackTrace();
			throw e;
		}
	}
	
	/**
	 * This method removes a file
	 * 
	 * @param fullPath	Full path of the file to delete
	 */
	public void deleteFileUsingPath(fullPath){
		File tempFile = new File(fullPath)
		FileUtils.deleteQuietly(tempFile);
	}
	
	/**
	 * This method receive an image file from a Form,
	 * validates the image and upload the file to the rackspace environment.
	 * 
	 * @param tempPicture	Image file from the Form
	 * @param x				X position to crop
	 * @param y				Y position to crop
	 * @param w				Width to crop
	 * @param h				Height to crop
	 * 
	 * @return				Rackspace Url of the file
	 * @throws Exception	When there is an error with the image file or the upload process
	 */
	public ImageUploadResult cropAndSaveImage(tempPicture , String x, String y, String w, String h) throws Exception {
		
		return this.cropAndSaveImage(tempPicture, x, y, w, h, Constants.CUSTOM_BUTTON_MAX_SIZE, 0, 0);
		
	}
	
	/**
	 * This method receive an image file from a Form,
	 * validates the image and upload the file to the rackspace environment.
	 *
	 * @param tempPicture	Image file from the Form
	 * @param x				X position to crop
	 * @param y				Y position to crop
	 * @param w				Width to crop
	 * @param h				Height to crop
	 *
	 * @return				Rackspace Url of the file
	 * @throws Exception	When there is an error with the image file or the upload process
	 */
	public ImageUploadResult cropAndSaveImage(tempPicture , String x, String y, String w, String h, 
		int requiredWidth, requiredHeight) throws Exception {
		
		return this.cropAndSaveImage(tempPicture, x, y, w, h, Constants.CUSTOM_BUTTON_MAX_SIZE, requiredWidth, requiredHeight);
		
	}
	
	public ImageUploadResult cropAndSaveImage(tempPicture , String x, String y, String w, String h, 
		double maxSizeAllowed, int requiredWidth, int requiredHeight) 
					throws Exception {
			try{
				def fileNameOrigin = tempPicture.getOriginalFilename();
				
				if (!tempPicture.isEmpty() && !fileNameOrigin.isEmpty()){
					
					//save file on server
					ImageUploadResult result = this.saveImageFile(tempPicture, x, y ,w, h, maxSizeAllowed, requiredWidth, requiredHeight );
					
					//upload file to rackspace
					CloudFilesPublish cloudService = new CloudFilesPublish(result.getPath(), grailsApplication.config.rackspaceUser, grailsApplication.config.apiKey);
					String fileUrl = cloudService.uploadFileToRackspace();
					
					result.setImageUrl(fileUrl);
					
					return result;
				} else {
					throw new Exception("upload.image.invalid.file");
				}
			}catch(e){
				e.printStackTrace();
				throw e;
			}
	}
			
	/**
	 * This method crop the image and save the final to a local direction
	 * 
	 * @param tempPicture	Picture to be cropped and saved
	 * @param x				X position to Crop
	 * @param y				Y position to Crop
	 * @param w				Width to Crop
	 * @param h				Height to Crop
	 * @return				Final full path of the image
	 * @throws Exception	When there is a error with the image or the store process
	 */
	private ImageUploadResult saveImageFile(tempPicture, String x, String y, String w, String h, 
		double maxSizeAllowed, int requiredWidth, int requiredHeight  ) throws Exception {
		
		ImageUploadResult result = null;
		
		try {	
			def storagePath = grailsApplication.config.basePathContentTempFiles;
			def fileNameOrigin = tempPicture.getOriginalFilename();
			def fullPath = Utils.getPathFromFile(storagePath, fileNameOrigin);
			
			//verify that directory exists
			File directory = new File(storagePath);
			if (!directory.exists()){
				directory.mkdirs();
			}
			
			//create temp file
			tempPicture.transferTo( new File(fullPath) );
			File tempFile = new File(fullPath);
			
			BufferedImage image = ImageIO.read(tempFile);
			BufferedImage croppedImage;
			//Make Javascript validations here -- Safari
			String ext = FileHelper.getFileExtension(fileNameOrigin);
			if(tempPicture.getSize() > maxSizeAllowed){
				throw new Exception ("The file size is not allowed [MaxSize: " + 
					mycentralserver.utils.Utils.getFileHumanSize(maxSizeAllowed) +
					", FileSize: " + mycentralserver.utils.Utils.getFileHumanSize(tempPicture.getSize()) + "]");
			}else if(!ArrayUtils.contains( Constants.CUSTOM_BUTTON_ALLOWED_EXTENSIONS, ext.toLowerCase() )){
				throw new Exception ("The file extension is not allowed: " + ext.toLowerCase());
			}
			 
			if(x != null && y != null && w != null && h != null &&
					x != "" && y != "" && w != "" && h != "") {
				
				def posx = Utils.parseIntTheValueDoubleAsString(x);
				def posy = Utils.parseIntTheValueDoubleAsString(y);
				def width = Utils.parseIntTheValueDoubleAsString(w);
				def height = Utils.parseIntTheValueDoubleAsString(h);
				def position_y = Utils.calculatePositonHeight(height,posy,image.height);
				def position_x = Utils.calculatePositonWidth(width,posx,image.width);
					 
				//crop the image
				croppedImage = image.getSubimage(position_x, position_y , width , height );
				
			}else{
				croppedImage = image;
			}
			
			storagePath = grailsApplication.config.basePathContentImagesFiles;
			
			//verify final destination directory exists
			directory = new File(storagePath);
			if (!directory.exists()){
				directory.mkdirs();
			}
			
			//save cropped image
			fullPath = Utils.getPathFromFile(storagePath, fileNameOrigin );
			File croppedPicture = new File(fullPath);			 
			ImageIO.write(croppedImage, ext.toLowerCase(), croppedPicture);
						
			if(requiredWidth != 0 && !validateImageDimensions(croppedPicture, requiredWidth, requiredHeight)){
				FileUtils.deleteQuietly(tempFile);
				FileUtils.deleteQuietly(croppedPicture);
				throw new Exception ("Image Dimensions are wrong: " + requiredWidth + ", " + requiredHeight);
			}
			
			result = new ImageUploadResult(fileNameOrigin, "", fullPath, croppedImage);
			
			//generateImage(croppedImage, fileNameOrigin, ext, requiredWidth, requiredHeight, 2048, 1536);
			
			//delete temp file
			FileUtils.deleteQuietly(tempFile);
		 
			return result;
		} catch(e)	{
			log.error("Error saving a image file", e);
			throw e;
		}
	} // END saveImageFile
		
	public String generateAndUploadImage(BufferedImage originalImage, String fileName,
			int originalWidth, int originalHeight, int w, int h, String backgroundColor){
			
		//println "Generate from [w: " + originalWidth + ", h: " + originalHeight + "] to [w: " + w + ", h: " + h + "]";
		
		final String ext = FileHelper.getFileExtension(fileName); 
		final int generatedWidth = w;
		final int generatedHeight = h; 
		String storagePath = grailsApplication.config.basePathContentImagesFiles;
		String path = Utils.getPathFromFile(storagePath, "gen-"+fileName );
		
		//println "Save generated image to: " + path;
		
		File generatedPicture = new File(path);		
		
		BufferedImage img = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		File f = null;
				
		int initialW = 0;
		
		if(generatedWidth >= originalWidth){
			initialW = (generatedWidth - originalWidth) / 2;
		}
		
		int initialH = 0;
	
		if(generatedHeight >= originalHeight){
			initialH = (generatedHeight - originalHeight) / 2;
		}
		
		int finalW = initialW + originalWidth;
		
		if(originalWidth >= generatedWidth){
			finalW = generatedWidth;
		}
		
		int finalH = initialH + originalHeight;
	
		if(originalHeight >= generatedHeight){
			finalH = generatedHeight;
		}
		
		//println "Put image at -> initial [w: " + initialW + ", h: " + initialH + "], final [w: " + finalW + ", h: "+ finalH + "]";
		
		Graphics2D g = img.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
			RenderingHints.VALUE_INTERPOLATION_BICUBIC);
		g.setBackground(Color.decode(backgroundColor));
				
		g.clearRect(0, 0, generatedWidth, generatedHeight);
				
		g.drawImage(originalImage, 
			initialW, initialH, finalW, finalH,
			0, 0, originalWidth, originalHeight, null);
				
		g.dispose();
				
		ImageIO.write(img, ext.toLowerCase(), generatedPicture);
		
		CloudFilesPublish cloudService = new CloudFilesPublish(path, grailsApplication.config.rackspaceUser, grailsApplication.config.apiKey);
		String fileUrl = cloudService.uploadFileToRackspace();
		
		deleteFileUsingPath(path);
		
		return fileUrl;
	}
	
	/**
	 * This method will check the width and height of 
	 * a Image file
	 * 
	 * @param file	File to be checked
	 * @param w		Width to check
	 * @param h		height to check
	 * @return		True or false depending of if the width and height are the required 
	 */
	public boolean validateImageDimensions(file, int w, int h){
		BufferedImage bimg = ImageIO.read(file);
		int width          = bimg.getWidth();
		int height         = bimg.getHeight();
		//println "RealSize [w:" + width + ",h:" + height + "], RequiredSize [w:" + w + ",h:" + h + "]";
		return (width == w && height == h);
	}
		
	/**
	 * This method upload a Video file after validation
	 *
	 * @param file			Video File to upload
	 * @return				Final full path of the video
	 * @throws Exception	When there is a error with the video or the store process
	 */
	private String saveVideoFile(file) throws Exception {
				
		try {
			def storagePath = grailsApplication.config.basePathContentTempFiles;
			def fileNameOrigin = file.getOriginalFilename();
			def fullPath = Utils.getPathFromFile(storagePath, fileNameOrigin);
			
			//Make Javascript validations here -- Safari
			String ext = FileHelper.getFileExtension(fileNameOrigin);
			if(file.getSize() > Constants.WELCOME_VIDEO_FILE_MAX_SIZE){
				throw new Exception ("The file size is not allowed [MaxSize: " + Constants.WELCOME_VIDEO_FILE_MAX_SIZE +
					", FileSize: " + file.getSize() + "]");
			}else if(!ArrayUtils.contains( Constants.WELCOME_VIDEO_FILE_ALLOWED_EXTENSIONS, ext.toLowerCase() )){
				throw new Exception ("The file extension is not allowed: " + ext.toLowerCase());
			}
			
			//verify that directory exists
			File directory = new File(storagePath);
			if (!directory.exists()){
				directory.mkdirs();
			}
			
			//create temp file
			file.transferTo( new File(fullPath) );
			File tempFile = new File(fullPath);
			
			return fullPath
		} catch(e)	{
			e.printStackTrace();
			throw e;
		}
	} // END saveVideoFile
	
	public void deleteMultipleFiles(List<String> files){
		String[] linesArr = files.toArray(new String[files.size()]);
		deleteMultipleFiles(linesArr);
	}
	
	/**
	 * Try to delete from the Rackspace servers multiple files using their urls
	 * 
	 * @param filesUrls		List of urls to be deleted
	 */
	public void deleteMultipleFiles(String[] filesUrls){
		try {
			if(filesUrls.length > 0){
				
				//Create new service instance
				CloudFilesPublish cloudService = new CloudFilesPublish(grailsApplication.config.rackspaceUser, grailsApplication.config.apiKey);
			
				//Try to delete all the files	
				for(int i = 0; i < filesUrls.length; i++){
					if(filesUrls[i] != null && !filesUrls[i].trim().isEmpty()){
						try {
							cloudService.deleteFileToRackspace(filesUrls[i]);
							log.debug("Deleted file from rackspace: " + filesUrls[i]);
						} catch(e){ //ignore if we could not delete the file
							e.printStackTrace();
						}
					}
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Delete a single file from the Rackspace environment using an Url
	 * 
	 * @param fileUrl	Url of the file to be deleted
	 */
	public void deleteFile(String fileUrl){
		if(fileUrl != null && !fileUrl.isEmpty()){
			try{
				CloudFilesPublish cloudService = new CloudFilesPublish(grailsApplication.config.rackspaceUser, grailsApplication.config.apiKey);
				cloudService.deleteFileToRackspace(fileUrl);
				log.debug("Deleted file from rackspace: " + fileUrl);
			} catch(e){ //ignore if we could not delete the file
				e.printStackTrace();
			}
		}
	}
	
}
