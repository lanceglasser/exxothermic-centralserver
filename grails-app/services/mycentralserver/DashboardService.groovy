/**
 * DashboardService.groovy
 */
package mycentralserver;

import mycentralserver.generaldomains.Configuration;
import mycentralserver.utils.ConfigurationConstants;

/**
 * Service class with some logic related to the dashboards
 * of the Application
 * 
 * @author Cecropia Solutions
 */
class DashboardService {

	public static final int MAIN_DASHBOARD = 1;
	public static final int COMPANY_DASHBOARD = 2;
	public static final int LOCATION_DASHBOARD = 3;
	
	/**
	 * This method will return the default configuration of a application dashboard,
	 * depending of the dashboard and the user role
	 * @param dashboardCode		Dashboard code
	 * @return					Json default configuration 
	 */
    public String getDefaultDashboardConfig(int dashboardCode) {
		String jsonStr = "";
		switch(dashboardCode){
			case DashboardService.MAIN_DASHBOARD:
				Configuration config = Configuration.findByCode(ConfigurationConstants.DASHBOARD_MAIN_DEFAULT);
				jsonStr = (config && config.value && config.value != "")? config.value:
					"{exxtractors-info-widget:{v:true,r:1,c:1,x:1,y:7},shortcuts-widget:{v:true,r:1,c:2,x:1,y:7}}";
				break;
			case DashboardService.COMPANY_DASHBOARD:
				Configuration config = Configuration.findByCode(ConfigurationConstants.DASHBOARD_COMPANY_DEFAULT);
				jsonStr = (config && config.value && config.value != "")? config.value:
					"{company-widget:{v:true,r:1,c:1,x:2,y:7},shortcuts-widget:{v:true,r:1,c:3,x:1,y:7},exxtractors-info-widget:{v:true,r:8,c:1,x:1,y:9},locations-widget:{v:true,r:8,c:1,x:1,y:9},exxtractors-widget:{v:true,r:8,c:1,x:1,y:9}}";
				break;
			case DashboardService.LOCATION_DASHBOARD:
				Configuration config = Configuration.findByCode(ConfigurationConstants.DASHBOARD_LOCATION_DEFAULT);
				jsonStr = (config && config.value && config.value != "")? config.value:
					"{exxtractors-info-widget:{v:true,s:max,r:8,c:1,x:1,y:7,maxH:0,maxX:0,maxY:0,refresh:0,refreshTime:0},shortcuts-widget:{v:true,s:max,r:1,c:3,x:1,y:7,maxH:0,maxX:0,maxY:0,refresh:0,refreshTime:0},location-widget:{v:true,s:max,r:1,c:1,x:2,y:7,maxH:0,maxX:0,maxY:0,refresh:0,refreshTime:0},exxtractors-widget:{v:true,s:max,r:15,c:1,x:1,y:9,maxH:0,maxX:0,maxY:0,refresh:0,refreshTime:0},banners-widget:{v:true,s:max,r:15,c:2,x:1,y:9,maxH:0,maxX:0,maxY:0,refresh:0,refreshTime:0},offers-widget:{v:true,s:max,r:15,c:3,x:1,y:9,maxH:0,maxX:0,maxY:0,refresh:0,refreshTime:0},documents-widget:{v:true,s:max,r:8,c:3,x:1,y:7,maxH:0,maxX:0,maxY:0,refresh:0,refreshTime:0},waAppTheme-widget:{v:true,s:max,r:8,c:2,x:1,y:7,maxH:0,maxX:0,maxY:0,refresh:0,refreshTime:0}}";
				break;
			default:
				jsonStr = "{}";
				break;
		}
		return jsonStr;
    } // End of getDefaultDashboardConfig method
	
} // End of Class
