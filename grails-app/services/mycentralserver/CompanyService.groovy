/**
 * CompanyService.groovy
 */
package mycentralserver;

import mycentralserver.company.Company;
import mycentralserver.company.CompanyIntegrator;
import mycentralserver.company.CompanyLocation;
import mycentralserver.user.User;
import mycentralserver.utils.Constants;
import mycentralserver.utils.RoleEnum;

/**
 * Service class with most of the logic related with
 * the companies
 * 
 * @author Cecropia Solutions
 *
 */
class CompanyService {

	/* Inject required services */
	def userSessionService;
	def grailsApplication;
	def messageSource;
	def imageUploadService;
	def rackspaceService;
	
	/**
	 * Check if a Company exists by Id and if the user has access
	 * 
	 * @param id	Id of the Company to review
	 * @return		
	 */
	private Company checkCompanyAccess (long id) {
		
		Company company = Company.get(id);
		
		if(company) {
			
			def (companies,companiesAssigned)  = getPermitedCompanies();
			
			if( companies.any { it == company } || companiesAssigned.any { it == company } ){
				return company;
			}
			
		} // End of exists Company
		
		return null;
		
	} // End of checkCompanyAccess method
	
	/**
	 * Use from Controller to check the access to the Company Dashboard
	 * 
	 * @param id			Id of the Company to render
	 * @param flash			Flash object to set the error message
	 * @return				Return Company object; null when error after set error to flash
	 * @throws Exception	When something crash
	 */
	public Company renderDashboardPage(long id, flash) throws Exception {
		
		Company company = checkCompanyAccess(id);
		
		if(company){
			return company;
		} else {
			flash.error = messageSource.getMessage('general.object.not.found', null, null);
			return null;
		}
		
	} // End of renderDashboardPage method
	
	/**
	 * Return the list of companies for the current
	 * user depending of role 
	 * 
	 * @return	[companies, companiesAssigned, totalCount, filteredCount]; null when error
	 */
	public def getCompaniesOfUser(HashMap<String, Object> queryParams) {
		
		try {
			def companies = new ArrayList();
			def companiesAssigned = new ArrayList();
			int totalCount = 0;
			int filteredCount = 0;
			
			if(userSessionService.isAdminOrHelpDesk()) {				
				totalCount = Company.listAllCompaniesForUITable(0, 0, queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME), 
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), "", false, null, true).list()[0];
				filteredCount = Company.listAllCompaniesForUITable(0, 0, queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME), 
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH), false, null, true).list()[0];
				companies = Company.listAllCompaniesForUITable(
					queryParams.get(Constants.DATA_TABLES_START), queryParams.get(Constants.DATA_TABLES_LENGTH) , 
					queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME) , 
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH), false, null, false).list();
			} else {
				def temp = null;
				User user = userSessionService.getCurrentUser();
				boolean onlyEnable = (userSessionService.userHasRole(RoleEnum.OWNER_STAFF.value) 
					 || userSessionService.userHasRole(RoleEnum.INTEGRATOR_STAFF.value));
				
				totalCount = Company.listAllCompaniesForUITable(0, 0, queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), "", onlyEnable, user, true).list()[0];
				filteredCount = Company.listAllCompaniesForUITable(0, 0, queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH), onlyEnable, user, true).list()[0];
				companies = Company.listAllCompaniesForUITable(
					queryParams.get(Constants.DATA_TABLES_START), queryParams.get(Constants.DATA_TABLES_LENGTH), 
					queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME), queryParams.get(Constants.DATA_TABLES_ORDER_DIR), 
					queryParams.get(Constants.DATA_TABLES_SEARCH), onlyEnable, user, false).list();
			}
			
			return [companies , companiesAssigned.sort{it.name}, totalCount, filteredCount]
		} catch(Exception e) {
			log.error("Error getting the list of companies of the user", e);
			return null;
		} // End of catch		
	} // End of getCompaniesOfUser
	
	/**
	 * Get the list of Companies where the User has access
	 * 
	 * @return	[companies, companiesAssigned]
	 */
	def getPermitedCompanies(){
		
		def companies = new ArrayList()
		def companiesAssigned = new ArrayList()
		 
		try {
			
			if (userSessionService.isAdminOrHelpDesk()){
				companies = Company.findAll();
			} else {			
				User user = userSessionService.getCurrentUser();
				def temp = null;
				if ( userSessionService.userHasRole(RoleEnum.OWNER_STAFF.value)
					|| userSessionService.userHasRole(RoleEnum.INTEGRATOR_STAFF.value) ) {
					temp = Company.getEnabledCompanies(user);
					companies = temp.listDistinct();
				} else {
					temp = Company.getAllCompanies(user);
					companies = temp.listDistinct();
				}
																					  
				def integrators = temp.typeIntegrator.listDistinct();
				
				for(i in integrators){
						CompanyIntegrator integrator = CompanyIntegrator.findByCompany(i);
						
						def assigned = integrator.assignedCompanies.toList();
						for(a in assigned){
							a.locations = CompanyLocation.getAllLocations(user, a).listDistinct();
						}
						companiesAssigned.addAll(assigned);
				}
				 
				//remove from assigned those where user is the owner
				companiesAssigned = companiesAssigned.findAll { it.owner != user }.collect();
			}
		} catch(Exception e) {
			log.error(e.getMessage());
		}
		
		return [companies , companiesAssigned.sort{it.name}];
		
	} // End of getPermitedCompanies method
	
	/**
	 * Return a jsonMap with the information of a Company if exists
	 * @param parameters	Parameters must containts the Company id to find information
	 * @return				HashMap with the Company information when exists
	 */
	public HashMap<String, Object> getCompanyInformation(HashMap<String, Object> parameters){
		HashMap<String, Object> jsonMap = new HashMap<String, Object>();
		Company company = Company.read(parameters.get(Constants.COMPANY_ID));
		if( company ) { // The Company exists
			String defaultFieldEmpty = messageSource.getMessage("default.field.empty", null, null);
			jsonMap[Constants.COMPANY_ID] = company.id;
			jsonMap[Constants.NAME] = company.name;
			jsonMap[Constants.WEB_SITE] = company.webSite? company.webSite : defaultFieldEmpty;
			jsonMap[Constants.PHONE_NUMBER] = company.phoneNumber? company.phoneNumber : defaultFieldEmpty;
			jsonMap[Constants.COMPANY_WIDE_INTEGRATOR] = company.companyWideIntegrator? company.companyWideIntegrator?.companyName:defaultFieldEmpty;
			jsonMap[Constants.TYPE_OF_COMPANY] = company.typeOfCompany.name? company.typeOfCompany.name : defaultFieldEmpty;
			jsonMap[Constants.TYPE_OF_ESTABLISHMENT] = company.type?.name? 
				messageSource.getMessage("typeOfEstablishment." + company.type.code, null, null) : defaultFieldEmpty;
			if(company.otherType){
				jsonMap[Constants.TYPE_OF_ESTABLISHMENT] = 
					messageSource.getMessage("typeOfEstablishment." + company.type.code, null, null) + ": " + company.otherType;
			}
			jsonMap[Constants.TIMEZONE] = company.timezone? company.timezone : defaultFieldEmpty;
			jsonMap[Constants.ZIP_CODE] = company.zipCode? company.zipCode : defaultFieldEmpty;			
			jsonMap[Constants.ENABLED] = company.enable == true? messageSource.getMessage("default.field.yes", null, null):
											messageSource.getMessage("default.field.no", null, null);
			
			String fullAddress = "";
			// Country
			fullAddress += (company.state? messageSource.getMessage("country.code." + company.state.country.shortName, null, null) : 
				messageSource.getMessage("country.code." + company.country.shortName, null, null)) + ", ";
			// State
			fullAddress += company.state? messageSource.getMessage("state.code." + company.state.name, null, null) :
				( company.stateName? company.stateName : defaultFieldEmpty);
			// City
			fullAddress += ", " + (company.city? company.city : defaultFieldEmpty);
			// Address
			fullAddress += ", " + (company.address? company.address : defaultFieldEmpty);
			
			jsonMap[Constants.ADDRESS] = fullAddress;
			
			jsonMap[Constants.IS_INTEGRATOR] = (company.typeOfCompany.name == 'Integrator');
		
			if(company.logoUrl && company.logoUrl.trim() != ""){
				jsonMap[Constants.HAS_LOGO] = true;
				jsonMap[Constants.LOGO] = company.logoUrl;
			} else {
				jsonMap[Constants.HAS_LOGO] = false;
			}
			String pocInfo = (company.pocFirstName? company.pocFirstName:"") + " ";
			pocInfo += (company.pocLastName? company.pocLastName + ", ":"");
			pocInfo += (company.pocEmail? company.pocEmail + ", ":"");
			pocInfo += (company.pocPhone? company.pocPhone:"");
			jsonMap[Constants.POC] = pocInfo;
		} else { // The Company do not exists
			jsonMap[Constants.COMPANY_ID] = 0;
		}
		return jsonMap;
	} // End of getCompanyInformation method
	
	/**
	 * Upload the Company Logo from the Widget
	 * 
	 * @param companyId		Company to be updated
	 * @param params		Request parameters
	 * @param request		Request object
	 * @param flash			Flash object
	 * @return				Url of the uploaded image
	 * @throws Exception	When something is wrong with the image or something crash
	 */
	public String uploadLogo(companyId, params, request, flash) throws Exception { 
		
		Company company = Company.get(companyId);
		
		if(company){
			// Check and upload the logo Image
			def imagesUrlsToDelete = [];
			def deleteWhenFails = [];
			String fileId = "bgLogoUrl";
			String url = imageUploadService.uploadImageFile(messageSource.getMessage("company.logo", null, null), fileId,
				Constants.COMPANY_LOGO_WIDTH, Constants.COMPANY_LOGO_HEIGHT, params, request, flash);
			if(url != null){
				imagesUrlsToDelete.add(company.logoUrl);
				company.logoUrl = url;
				deleteWhenFails.add(url);
			}
			company.save();
			rackspaceService.deleteMultipleFiles(imagesUrlsToDelete);
			return url;
		} else {
			throw new Exception("Company do not exists.");
		}
	} // End of uploadLogo method
	
} // End of Class
