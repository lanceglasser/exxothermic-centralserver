/**
 * ShortcutsService.groovy
 */
package mycentralserver;

import java.util.HashMap;

import org.codehaus.groovy.grails.web.mapping.LinkGenerator;

import mycentralserver.utils.Constants;
import mycentralserver.utils.RoleEnum;
import mycentralserver.utils.ShortcutsEnum;
import mycentralserver.user.UserDashboard;
import mycentralserver.user.User;

/**
 * Service with the logic related to the Dashboards Shortcuts
 *
 * @author Cecropia Solutions
 *
 */
class ShortcutsService {

	/* Inject required services */
	def userSessionService;
	def messageSource;
	def grailsApplication;
	
	// Inject link generator
	LinkGenerator grailsLinkGenerator;
	
	/**
	 * Will return a hashmap including the html code with the list of all available
	 * shortcuts depending of the user/role and the dashboard
	 * @param parameters	Should contain the CompanyId and LocationId values
	 * @return				Hashmap with the result
	 */
	public HashMap<String, Object> saveUserShortcuts(HashMap<String, Object> parameters){
		HashMap<String, Object> jsonMap = new HashMap<String, Object>();
		long companyId = parameters.get(Constants.COMPANY_ID);
		long locationId = parameters.get(Constants.LOCATION_ID);
		User user = userSessionService.getCurrentUser();
		UserDashboard dashboardsInfo = UserDashboard.findByUser(user);
		def shortcuts = null;
		
		if(!dashboardsInfo) {// If not exists the dashboards configurations
			dashboardsInfo = new UserDashboard();
			dashboardsInfo.user = user;
		}
				
		if(companyId != 0){ // Company Dashboard Shortcuts
			dashboardsInfo.companyShortcuts = parameters.get(Constants.SHORTCUTS);
		} else if(locationId != 0){ // Location Dashboard Shortcuts
			dashboardsInfo.locationShortcuts = parameters.get(Constants.SHORTCUTS);
		} else { // Main Dashboard Shortcuts
			dashboardsInfo.userShortcuts = parameters.get(Constants.SHORTCUTS);
		}
		if(dashboardsInfo.save()){
			jsonMap["error"] = false;
		} else {
			jsonMap["error"] = true;
			jsonMap["msg"] = "Error saving to DB";
		}
		
		return jsonMap;
	} // End of saveUserShortcuts
	
	/**
	 * Will return a hashmap including the html code with the list of all available 
	 * shortcuts depending of the user/role and the dashboard
	 * @param parameters	Should contain the CompanyId and LocationId values
	 * @return				Hashmap with the result
	 */
	public HashMap<String, Object> getAllUserShortcuts(HashMap<String, Object> parameters){
		HashMap<String, Object> jsonMap = new HashMap<String, Object>();
		String htmlLinks = '';
		htmlLinks += getCurrentUserAvailableShortcuts(parameters.get(Constants.COMPANY_ID), parameters.get(Constants.LOCATION_ID));
		jsonMap["links"] = htmlLinks;
		jsonMap["error"] = false;
		return jsonMap;
	} // End of getAllUserShortcuts
	
	/**
	 * Will return a hashmap including the html code with the list of shortcuts depending of the
	 * user/role and the dashboard
	 * @param parameters	Should contain the CompanyId and LocationId values
	 * @return				Hashmap with the result
	 */
	public HashMap<String, Object> getUserShortcuts(HashMap<String, Object> parameters){
		HashMap<String, Object> jsonMap = new HashMap<String, Object>();
		String htmlLinks = '<ul class="shortcuts-list">';
		htmlLinks += getDashboardShortcuts(parameters.get(Constants.COMPANY_ID), parameters.get(Constants.LOCATION_ID));
		htmlLinks += '</ul>';
		jsonMap["links"] = htmlLinks;
		jsonMap["error"] = false;
		return jsonMap;
    } // End of getUserShortcuts
	
	/**
	 * This method will check for a db saved configuration, if not will return the default
	 * shortcuts as a list of codes
	 * @param companyId		If not equals to 0 then is the Company Dashboard
	 * @param locationId	If not equals to 0 then is the Location Dashboard
	 * @return				HTML with the list of default shortcuts
	 */
	public def getCurrentDashboardShortcutsOfUser(long companyId, long locationId){
		User user = userSessionService.getCurrentUser();
		UserDashboard dashboardsInfo = UserDashboard.findByUser(user);
		def shortcuts = null;
		if(dashboardsInfo) {//If the dashboards configurations exists
			if(companyId != 0){ // Company Dashboard Shortcuts
				if(dashboardsInfo.companyShortcuts && dashboardsInfo.companyShortcuts.trim() != ""){
					shortcuts = dashboardsInfo.companyShortcuts.split(',');
				} else { // If not exists the company dashboard shortcuts config or is empty
					shortcuts = getCurrentUserDefaultShortcutsAsList(companyId, locationId, true);
				}
			} else if(locationId != 0){ // Location Dashboard Shortcuts
				if(dashboardsInfo.locationShortcuts && dashboardsInfo.locationShortcuts.trim() != ""){
					shortcuts = dashboardsInfo.locationShortcuts.split(',');
				} else { // If not exists the company dashboard shortcuts config or is empty
					shortcuts = getCurrentUserDefaultShortcutsAsList(companyId, locationId, true);
				}
			} else { // Main Dashboard Shortcuts
				if(dashboardsInfo.userShortcuts && dashboardsInfo.userShortcuts.trim() != ""){
					shortcuts = dashboardsInfo.userShortcuts.split(',');
				} else { // If not exists the company dashboard shortcuts config or is empty
					shortcuts = getCurrentUserDefaultShortcutsAsList(companyId, locationId, true);
				}
			}
		} else { //If not exists the dashboards configurations
			shortcuts = getCurrentUserDefaultShortcutsAsList(companyId, locationId, true);
		}
		return shortcuts;
	} // End of getDashboardShortcuts method
	
	/**
	 * This method will check for a db saved configuration, if not will return the default
	 * shortcuts
	 * @param companyId		If not equals to 0 then is the Company Dashboard
	 * @param locationId	If not equals to 0 then is the Location Dashboard
	 * @return				HTML with the list of default shortcuts
	 */
	public String getDashboardShortcuts(long companyId, long locationId){
		String htmlLinks = "";
		User user = userSessionService.getCurrentUser();
		UserDashboard dashboardsInfo = UserDashboard.findByUser(user);
		if(dashboardsInfo) {//If the dashboards configurations exists
			if(companyId != 0){ // Company Dashboard Shortcuts
				if(dashboardsInfo.companyShortcuts && dashboardsInfo.companyShortcuts.trim() != ""){
					def shortcuts = dashboardsInfo.companyShortcuts.split(',');
					for(shortcut in shortcuts){
						htmlLinks += '<li>' + getShortcutLinkByCode(shortcut, companyId, locationId) + '</li>';
					}
				} else { // If not exists the company dashboard shortcuts config or is empty
					htmlLinks = getCurrentUserDefaultShortcuts(companyId, locationId);
				}
			} else if(locationId != 0){ // Location Dashboard Shortcuts
				if(dashboardsInfo.locationShortcuts && dashboardsInfo.locationShortcuts.trim() != ""){
					def shortcuts = dashboardsInfo.locationShortcuts.split(',');
					for(shortcut in shortcuts){
						htmlLinks += '<li>' + getShortcutLinkByCode(shortcut, companyId, locationId) + '</li>';
					}
				} else { // If not exists the company dashboard shortcuts config or is empty
					htmlLinks = getCurrentUserDefaultShortcuts(companyId, locationId);
				}
			} else { // Main Dashboard Shortcuts
				if(dashboardsInfo.userShortcuts && dashboardsInfo.userShortcuts.trim() != ""){
					def shortcuts = dashboardsInfo.userShortcuts.split(',');
					for(shortcut in shortcuts){
						htmlLinks += '<li>' + getShortcutLinkByCode(shortcut, companyId, locationId) + '</li>';
					}
				} else { // If not exists the company dashboard shortcuts config or is empty
					htmlLinks = getCurrentUserDefaultShortcuts(companyId, locationId);
				}
			}
		} else { //If not exists the dashboards configurations
			htmlLinks = getCurrentUserDefaultShortcuts(companyId, locationId);
		}
		return htmlLinks;
	} // End of getDashboardShortcuts method
	
	/**
	 * Return the list of default shortcuts codes depending of the
	 * dashboard and the User Role
	 * @param companyId		If not equals to 0 then is the Company Dashboard
	 * @param locationId	If not equals to 0 then is the Location Dashboard
	 * @return				List of default shortcuts codes
	 */
	public def getCurrentUserDefaultShortcutsAsList(long companyId, long locationId, boolean onlyCodes){
		def shortcuts = [];
		if(userSessionService.isAdmin()){
			shortcuts = [ShortcutsEnum.CREATE_SOFT_VER, ShortcutsEnum.CREATE_DOC_CATEGORY, ShortcutsEnum.CREATE_APP_THEME,
				ShortcutsEnum.CREATE_WELCOME_AD]
		} else if(userSessionService.isHelpDesk()){
			shortcuts = [ShortcutsEnum.CREATE_OFFER, ShortcutsEnum.CREATE_DOCUMENT, ShortcutsEnum.CREATE_BANNER,
				ShortcutsEnum.REGISTER_EXXTRACTOR, ShortcutsEnum.UNREGISTER_EXXTRACTOR, ShortcutsEnum.USAGE_REPORT]
		} else if(userSessionService.userHasRole(RoleEnum.INTEGRATOR.value)){
			if(companyId != 0){//It's Company Dashboard
				shortcuts = [ShortcutsEnum.CREATE_OFFER, ShortcutsEnum.CREATE_DOCUMENT, ShortcutsEnum.CREATE_BANNER,
					ShortcutsEnum.REGISTER_EXXTRACTOR, ShortcutsEnum.UNREGISTER_EXXTRACTOR, ShortcutsEnum.USAGE_REPORT]
			} else if(locationId != 0){ //It's Location Dashboard
				shortcuts = [ShortcutsEnum.CREATE_OFFER, ShortcutsEnum.CREATE_DOCUMENT, ShortcutsEnum.CREATE_BANNER,
					ShortcutsEnum.REGISTER_EXXTRACTOR, ShortcutsEnum.UNREGISTER_EXXTRACTOR, ShortcutsEnum.USAGE_REPORT]
			} else { //It's Main Dashboard
				shortcuts = [ShortcutsEnum.CREATE_OFFER, ShortcutsEnum.CREATE_DOCUMENT, ShortcutsEnum.CREATE_BANNER,
					ShortcutsEnum.REGISTER_EXXTRACTOR, ShortcutsEnum.UNREGISTER_EXXTRACTOR, ShortcutsEnum.USAGE_REPORT]
			}
		} else if(userSessionService.userHasRole(RoleEnum.OWNER.value)){
			shortcuts = [ShortcutsEnum.CREATE_EMPLOYEE, ShortcutsEnum.CREATE_OFFER, ShortcutsEnum.CREATE_DOCUMENT, ShortcutsEnum.CREATE_BANNER]
		} else if(userSessionService.userHasRole(RoleEnum.INTEGRATOR_STAFF.value)){
			shortcuts = [ShortcutsEnum.CREATE_OFFER, ShortcutsEnum.CREATE_DOCUMENT, ShortcutsEnum.CREATE_BANNER,
				ShortcutsEnum.REGISTER_EXXTRACTOR, ShortcutsEnum.UNREGISTER_EXXTRACTOR, ShortcutsEnum.USAGE_REPORT]
		} else if(userSessionService.userHasRole(RoleEnum.OWNER_STAFF.value)){
			shortcuts = [ShortcutsEnum.CREATE_OFFER, ShortcutsEnum.CREATE_DOCUMENT, ShortcutsEnum.CREATE_BANNER]
		}
		if(onlyCodes) { // Will return an array only with the codes
			def shortcutsCode = [];
			for(shortcut in shortcuts){
				shortcutsCode.add(shortcut.value);
			}
			return shortcutsCode;
		} else { // Will return the list as shortcuts enum objects
			return shortcuts;
		}
	} // End of getCurrentUserDefaultShortcutsAsList method
	
	/**
	 * Return the html code with the list of default shortcuts depending of the
	 * dashboard and the User Role
	 * @param companyId		If not equals to 0 then is the Company Dashboard
	 * @param locationId	If not equals to 0 then is the Location Dashboard
	 * @return				HTML with the list of default shortcuts
	 */
	public String getCurrentUserDefaultShortcuts(long companyId, long locationId){
		String htmlLinks = "";
		def shortcuts = getCurrentUserDefaultShortcutsAsList(companyId, locationId, false);		
		for(shortcut in shortcuts){
			htmlLinks += '<li>' + getShortcutLinkByCode(shortcut.value, companyId, locationId) + '</li>';
		}	
		return htmlLinks;
	} // End of getCurrentUserDefaultShortcuts
	
	/**
	 * This method will return the html link depending of the code,
	 * the method can change the link depending of Company or Location Id and User Role
	 * @param companyId		If not equals to 0 then is the Company Dashboard
	 * @param locationId	If not equals to 0 then is the Location Dashboard
	 * @return				HTML with the link of the shortcut
	 */
	public String getShortcutLinkByCode(String code, long companyId, long locationId) {
		String link = '';
		switch(code){
			//
			case ShortcutsEnum.CREATE_OFFER.value:
				java.lang.Object[] args = new java.lang.Object[1];
				args[0] = messageSource.getMessage('content.message.type.offer', null, null);
				link += '<a href="' + grailsLinkGenerator.link(controller: 'offer', action: 'create', absolute: true) + '"><span class="icon create-icon"></span>' + messageSource.getMessage('default.create.label', args, null) + '</a>';
				break;
			case ShortcutsEnum.CREATE_DOCUMENT.value:
				java.lang.Object[] args = new java.lang.Object[1];
				args[0] = messageSource.getMessage('document', null, null);
				link += '<a href="' + grailsLinkGenerator.link(controller: 'document', action: 'create', absolute: true) + '"><span class="icon create-icon"></span>' + messageSource.getMessage('default.create.label', args, null) + '</a>';
				break;
			case ShortcutsEnum.CREATE_BANNER.value:
				java.lang.Object[] args = new java.lang.Object[1];
				args[0] = messageSource.getMessage('default.title.Content', null, null);
				link += '<a href="' + grailsLinkGenerator.link(controller: 'content', action: 'create', absolute: true) + '"><span class="icon create-icon"></span>' + messageSource.getMessage('default.create.label', args, null) + '</a>';
				break;
			case ShortcutsEnum.CREATE_LOCATION.value:
				java.lang.Object[] args = new java.lang.Object[1];
				args[0] = messageSource.getMessage('default.field.location', null, null);
				link += '<a href="' + grailsLinkGenerator.link(controller: 'location', action: 'create', absolute: true) + '"><span class="icon create-icon"></span>' + messageSource.getMessage('default.create.label', args, null) + '</a>';
				break;
			case ShortcutsEnum.CREATE_EMPLOYEE.value:
				java.lang.Object[] args = new java.lang.Object[1];
				args[0] = messageSource.getMessage('employee', null, null);
				link += '<a href="' + grailsLinkGenerator.link(controller: 'employee', action: 'create', absolute: true) + '"><span class="icon create-icon"></span>' + messageSource.getMessage('default.create.label', args, null) + '</a>';
				break;
			case ShortcutsEnum.CREATE_COMPANY.value:
				java.lang.Object[] args = new java.lang.Object[1];
				args[0] = messageSource.getMessage('default.title.Company', null, null);
				link += '<a href="' + grailsLinkGenerator.link(controller: 'company', action: 'create', absolute: true) + '"><span class="icon create-icon"></span>' + messageSource.getMessage('default.create.label', args, null) + '</a>';
				break;
			case ShortcutsEnum.CREATE_SOFT_VER.value:
				java.lang.Object[] args = new java.lang.Object[1];
				args[0] = messageSource.getMessage('software.version', null, null);
				link += '<a href="' + grailsLinkGenerator.link(controller: 'boxSoftware', action: 'create', absolute: true) + '"><span class="icon create-icon"></span>' + messageSource.getMessage('default.create.label', args, null) + '</a>';
				break;
			case ShortcutsEnum.CREATE_DOC_CATEGORY.value:
				java.lang.Object[] args = new java.lang.Object[1];
				args[0] = messageSource.getMessage('doc.category', null, null);
				link += '<a href="' + grailsLinkGenerator.link(controller: 'docCategory', action: 'create', absolute: true) + '"><span class="icon create-icon"></span>' + messageSource.getMessage('default.create.label', args, null) + '</a>';
				break;
			case ShortcutsEnum.CREATE_APP_THEME.value:
				java.lang.Object[] args = new java.lang.Object[1];
				args[0] = messageSource.getMessage('default.home.skinApp', null, null);
				link += '<a href="' + grailsLinkGenerator.link(controller: 'appSkin', action: 'create', absolute: true) + '"><span class="icon create-icon"></span>' + messageSource.getMessage('default.create.label', args, null) + '</a>';
				break;
			case ShortcutsEnum.CREATE_WELCOME_AD.value:
				java.lang.Object[] args = new java.lang.Object[1];
				args[0] = messageSource.getMessage('welcome.ad', null, null);
				link += '<a href="' + grailsLinkGenerator.link(controller: 'welcomeAd', action: 'create', absolute: true) + '"><span class="icon create-icon"></span>' + messageSource.getMessage('default.create.label', args, null) + '</a>';
				break;
			case ShortcutsEnum.REGISTER_EXXTRACTOR.value:
				if(locationId != 0) { // It's at Location dashboard, this link will open a modal window for ExXtractor registration
					link += '<a href="#" id="reg-exx-btn"><span class="icon exxtractor-icon"></span>' + messageSource.getMessage('register.to.this.location', null, null) + '</a>';
				} else {
					link += '<a href="' + grailsLinkGenerator.link(controller: 'box', action: 'register', absolute: true) + '"><span class="icon exxtractor-icon"></span>' + messageSource.getMessage('wizard.label.register', null, null) + '</a>';
				}
				break;
			case ShortcutsEnum.UNREGISTER_EXXTRACTOR.value:
				link += '<a href="' + grailsLinkGenerator.link(controller: 'box', action: 'unregister', absolute: true) + '"><span class="icon exxtractor-icon"></span>' + messageSource.getMessage('default.box.unregister', null, null) + '</a>';
				break;
			case ShortcutsEnum.USAGE_REPORT.value:
				link += '<a href="' + grailsLinkGenerator.link(controller: 'boxMetrics', action: 'report', absolute: true) + '"><span class="icon report-icon"></span>' + messageSource.getMessage('usage.report.title', null, null) + '</a>';
				break;
			default:
				break;
		}
		return link;
	} // End of getShortcutLinkByCode
	
	/**
	 * Return the html code with the list of all available shortcuts depending of the
	 * dashboard and the User Role
	 * @param companyId		If not equals to 0 then is the Company Dashboard
	 * @param locationId	If not equals to 0 then is the Location Dashboard
	 * @return				HTML with the list of available shortcuts
	 */
	public String getCurrentUserAvailableShortcuts(long companyId, long locationId){
		String htmlLinks = "";
		def shortcuts = [];
		if(userSessionService.isAdmin()){
			shortcuts = [ShortcutsEnum.CREATE_SOFT_VER, ShortcutsEnum.CREATE_DOC_CATEGORY, ShortcutsEnum.CREATE_APP_THEME,
				ShortcutsEnum.CREATE_WELCOME_AD];
		} else if(userSessionService.isHelpDesk()){
			if(companyId != 0){ // It's the Company Dashboard
				shortcuts = [ShortcutsEnum.CREATE_LOCATION, 
					ShortcutsEnum.CREATE_OFFER, ShortcutsEnum.CREATE_DOCUMENT, ShortcutsEnum.CREATE_BANNER,
					ShortcutsEnum.REGISTER_EXXTRACTOR, ShortcutsEnum.UNREGISTER_EXXTRACTOR, ShortcutsEnum.USAGE_REPORT];
			} else if(locationId != 0){ // It's the Locatin Dashboard
				shortcuts = [ShortcutsEnum.CREATE_OFFER, ShortcutsEnum.CREATE_DOCUMENT, ShortcutsEnum.CREATE_BANNER,
					ShortcutsEnum.REGISTER_EXXTRACTOR, ShortcutsEnum.UNREGISTER_EXXTRACTOR, ShortcutsEnum.USAGE_REPORT];
			} else { // It's the Main Dashboard
				shortcuts = [ShortcutsEnum.CREATE_OFFER, ShortcutsEnum.CREATE_DOCUMENT, ShortcutsEnum.CREATE_BANNER,
					ShortcutsEnum.REGISTER_EXXTRACTOR, ShortcutsEnum.UNREGISTER_EXXTRACTOR, ShortcutsEnum.USAGE_REPORT,
					ShortcutsEnum.CREATE_COMPANY, ShortcutsEnum.CREATE_LOCATION, ShortcutsEnum.CREATE_DOC_CATEGORY,
					ShortcutsEnum.CREATE_APP_THEME, ShortcutsEnum.CREATE_WELCOME_AD];
			}
		} else if(userSessionService.userHasRole(RoleEnum.INTEGRATOR.value)){
			if(companyId != 0){//It's Company Dashboard
				shortcuts = [ShortcutsEnum.CREATE_OFFER, ShortcutsEnum.CREATE_DOCUMENT, ShortcutsEnum.CREATE_BANNER,
					ShortcutsEnum.REGISTER_EXXTRACTOR, ShortcutsEnum.UNREGISTER_EXXTRACTOR, ShortcutsEnum.USAGE_REPORT,
					ShortcutsEnum.CREATE_EMPLOYEE, ShortcutsEnum.CREATE_LOCATION];
			} else if(locationId != 0){ //It's Location Dashboard
				shortcuts = [ShortcutsEnum.CREATE_OFFER, ShortcutsEnum.CREATE_DOCUMENT, ShortcutsEnum.CREATE_BANNER,
					ShortcutsEnum.REGISTER_EXXTRACTOR, ShortcutsEnum.UNREGISTER_EXXTRACTOR, ShortcutsEnum.USAGE_REPORT];
			} else { //It's Main Dashboard
				shortcuts = [ShortcutsEnum.CREATE_OFFER, ShortcutsEnum.CREATE_DOCUMENT, ShortcutsEnum.CREATE_BANNER,
					ShortcutsEnum.REGISTER_EXXTRACTOR, ShortcutsEnum.UNREGISTER_EXXTRACTOR, ShortcutsEnum.USAGE_REPORT,
					ShortcutsEnum.CREATE_EMPLOYEE, ShortcutsEnum.CREATE_COMPANY, ShortcutsEnum.CREATE_LOCATION, 
					ShortcutsEnum.CREATE_DOC_CATEGORY, ShortcutsEnum.CREATE_APP_THEME, ShortcutsEnum.CREATE_WELCOME_AD];
			}
		} else if(userSessionService.userHasRole(RoleEnum.OWNER.value)){
			if(companyId != 0){ // It's the Company Dashboard
				shortcuts = [ShortcutsEnum.CREATE_OFFER, ShortcutsEnum.CREATE_DOCUMENT, 
					ShortcutsEnum.CREATE_BANNER];
			} else if(locationId != 0){ // It's the Locatin Dashboard
				shortcuts = [ShortcutsEnum.CREATE_EMPLOYEE, ShortcutsEnum.CREATE_OFFER, ShortcutsEnum.CREATE_DOCUMENT, 
					ShortcutsEnum.CREATE_BANNER];
			} else { // It's the Main Dashboard
				shortcuts = [ShortcutsEnum.CREATE_EMPLOYEE, ShortcutsEnum.CREATE_OFFER, ShortcutsEnum.CREATE_DOCUMENT, 
					ShortcutsEnum.CREATE_BANNER, ShortcutsEnum.CREATE_APP_THEME, ShortcutsEnum.CREATE_WELCOME_AD];
			}
		} else if(userSessionService.userHasRole(RoleEnum.INTEGRATOR_STAFF.value)){
			if(companyId != 0){ // It's the Company Dashboard
				shortcuts = [ShortcutsEnum.CREATE_OFFER, ShortcutsEnum.CREATE_DOCUMENT, ShortcutsEnum.CREATE_BANNER,
					ShortcutsEnum.REGISTER_EXXTRACTOR, ShortcutsEnum.UNREGISTER_EXXTRACTOR, ShortcutsEnum.USAGE_REPORT, 
					ShortcutsEnum.CREATE_APP_THEME, ShortcutsEnum.CREATE_WELCOME_AD];
			} else if(locationId != 0){ // It's the Locatin Dashboard
				shortcuts = [ShortcutsEnum.CREATE_OFFER, ShortcutsEnum.CREATE_DOCUMENT, ShortcutsEnum.CREATE_BANNER,
					ShortcutsEnum.REGISTER_EXXTRACTOR, ShortcutsEnum.UNREGISTER_EXXTRACTOR, ShortcutsEnum.USAGE_REPORT];
			} else { // It's the Main Dashboard
				shortcuts = [ShortcutsEnum.CREATE_OFFER, ShortcutsEnum.CREATE_DOCUMENT, ShortcutsEnum.CREATE_BANNER,
					ShortcutsEnum.REGISTER_EXXTRACTOR, ShortcutsEnum.UNREGISTER_EXXTRACTOR, ShortcutsEnum.USAGE_REPORT];
			}
		} else if(userSessionService.userHasRole(RoleEnum.OWNER_STAFF.value)){
			if(companyId != 0){ // It's the Company Dashboard
				shortcuts = [ShortcutsEnum.CREATE_OFFER, ShortcutsEnum.CREATE_DOCUMENT, ShortcutsEnum.CREATE_BANNER];
			} else if(locationId != 0){ // It's the Locatin Dashboard
				shortcuts = [ShortcutsEnum.CREATE_OFFER, ShortcutsEnum.CREATE_DOCUMENT, ShortcutsEnum.CREATE_BANNER];
			} else { // It's the Main Dashboard
				shortcuts = [ShortcutsEnum.CREATE_OFFER, ShortcutsEnum.CREATE_DOCUMENT, ShortcutsEnum.CREATE_BANNER, 
					ShortcutsEnum.CREATE_APP_THEME, ShortcutsEnum.CREATE_WELCOME_AD];
			}
		}
		
		def selectedList = getCurrentDashboardShortcutsOfUser(companyId, locationId);
		for(shortcut in shortcuts){
			htmlLinks += getShortcutCheckBoxByCode(shortcut.value, locationId, selectedList.contains(shortcut.value));
		}
		return htmlLinks;
	} // End of getCurrentUserAvailableShortcuts
	
	/**
	 * This method will return the html checkbox depending of the code,
	 * the method can change the link depending of Company or Location Id and User Role
	 * @param companyId		If not equals to 0 then is the Company Dashboard
	 * @param locationId	If not equals to 0 then is the Location Dashboard
	 * @return				HTML with the checkbox of the shortcut
	 */
	public String getShortcutCheckBoxByCode(String code, double locationId, boolean isChecked) {
		String link = '';
		String label = '';
		switch(code){
			case ShortcutsEnum.CREATE_OFFER.value:
				java.lang.Object[] args = new java.lang.Object[1];
				args[0] = messageSource.getMessage('content.message.type.offer', null, null);
				label =  messageSource.getMessage('default.create.label', args, null);
				break;
			case ShortcutsEnum.CREATE_DOCUMENT.value:
				java.lang.Object[] args = new java.lang.Object[1];
				args[0] = messageSource.getMessage('document', null, null);
				label =  messageSource.getMessage('default.create.label', args, null);
				break;
			case ShortcutsEnum.CREATE_BANNER.value:
				java.lang.Object[] args = new java.lang.Object[1];
				args[0] = messageSource.getMessage('default.title.Content', null, null);
				label =  messageSource.getMessage('default.create.label', args, null);
				break;
			case ShortcutsEnum.CREATE_LOCATION.value:
				java.lang.Object[] args = new java.lang.Object[1];
				args[0] = messageSource.getMessage('default.field.location', null, null);
				label =  messageSource.getMessage('default.create.label', args, null);
				break;
			case ShortcutsEnum.CREATE_EMPLOYEE.value:
				java.lang.Object[] args = new java.lang.Object[1];
				args[0] = messageSource.getMessage('employee', null, null);
				label =  messageSource.getMessage('default.create.label', args, null);
				break;
			case ShortcutsEnum.CREATE_COMPANY.value:
				java.lang.Object[] args = new java.lang.Object[1];
				args[0] = messageSource.getMessage('default.title.Company', null, null);
				label =  messageSource.getMessage('default.create.label', args, null);
				break;
			case ShortcutsEnum.CREATE_SOFT_VER.value:
				java.lang.Object[] args = new java.lang.Object[1];
				args[0] = messageSource.getMessage('software.version', null, null);
				label =  messageSource.getMessage('default.create.label', args, null);
				break;
			case ShortcutsEnum.CREATE_DOC_CATEGORY.value:
				java.lang.Object[] args = new java.lang.Object[1];
				args[0] = messageSource.getMessage('doc.category', null, null);
				label =  messageSource.getMessage('default.create.label', args, null);
				break;
			case ShortcutsEnum.CREATE_APP_THEME.value:
				java.lang.Object[] args = new java.lang.Object[1];
				args[0] = messageSource.getMessage('default.home.skinApp', null, null);
				label =  messageSource.getMessage('default.create.label', args, null);
				break;
			case ShortcutsEnum.CREATE_WELCOME_AD.value:
				java.lang.Object[] args = new java.lang.Object[1];
				args[0] = messageSource.getMessage('welcome.ad', null, null);
				label =  messageSource.getMessage('default.create.label', args, null);
				break;
			case ShortcutsEnum.REGISTER_EXXTRACTOR.value:
				if(locationId != 0) { // It's at Location dashboard, this link will open a modal window for ExXtractor registration
					label = messageSource.getMessage('register.to.this.location', null, null);
				} else {
					label = messageSource.getMessage('wizard.label.register', null, null);
				}
				break;
			case ShortcutsEnum.UNREGISTER_EXXTRACTOR.value:
				label = messageSource.getMessage('default.box.unregister', null, null);
				break;
			case ShortcutsEnum.USAGE_REPORT.value:
				label = messageSource.getMessage('usage.report.title', null, null);
				break;
			default:
				break;
		}
		link += '<span class="shortcut-cb-div">';
		link += '	<div class="col-xs-8 text-to-left">';
		link += '	  <label>' + label + '</label>';
		link += '	</div>';
		link += '	<div class="col-xs-4 text-to-left">';
		link += '		<input type="checkbox" value="' + code + '" class="input-checkbox shortcut-cb"';
		link += (isChecked? ' checked="checked"':'') + '>';
		link += '	</div>';
		link += '</span>';
		link += '<span class="clearfix"></span>';
		return link;
	} // End of getShortcutCheckBoxByCode
	
} // End of Class