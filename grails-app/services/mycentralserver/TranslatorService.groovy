/**
 * TranslatorService.groovy
 */
package mycentralserver

import mycentralserver.generaldomains.Translation;

/**
 * Service that handle most of the logic related to the translation stores
 * at data base; original created for the box configurations
 * 
 * @author Cecropia Solutions
 *
 */
class TranslatorService {
	
	def translate(String code) {
		return translate(code, null, getDefaultLocale());
	}
	
	def translate(String code, String locale) {
		return translate(code, null, locale);
	}
	
    def translate(String code, args, String locale) {
		Translation translation = Translation.findByLocaleAndCode(locale, code);
		if(!translation){
			translation = Translation.findByLocaleAndCode(getDefaultLocale(), code);
		}
		
		return (translation)? translation.value:code;
    }
	
	protected getDefaultLocale(){
		return "en";
	}
}
