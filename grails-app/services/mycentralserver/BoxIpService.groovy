package mycentralserver

import mycentralserver.box.BoxBlackList;
import mycentralserver.box.BoxIp;
import mycentralserver.utils.Constants;

/**
 * Service class with some logic to handle the Box Ip information
 * 
 * @author Cecropia Solutions
 * @since 01/01/2014
 */
class BoxIpService {

    /**
     * Check if the current serial haven't been registered with another macAdrress. Furthermore save the IP addresses
     * 
     * @param serial 
     *                  MyBox serial number
     * @param ipAddress 
     *                  MyBox current IpAddress 
     * @param macAddress 
     *                  MyBox current macAddress
     * @return true if the serial has been registered with more than 1 macAddress
     */
    def checkMacAddressAndSerialConnections(String serial, String ipAddress, String macAddress) {
        //final BoxIp boxIp = BoxIp.findBySerialAndMacAddressAndIpAddress(serial,macAddress, ipAddress);
        final BoxIp boxIp = BoxIp.getBoxLastIpConnected(serial).get();
        // check ipAddress and macAddress do not be null
        log.info("CheckMacAddressAndSerialConnections - Serial: "  + serial + " Ip Address " + ipAddress 
            + " MacAddress " + macAddress);
        if (ipAddress && macAddress) {
            BoxIp.withTransaction {status ->
                try{
                    if (boxIp && boxIp.ipAddress == ipAddress) {
                        log.info("BoxIp it's the same than last connection");
                        boxIp.lastUpdated = new Date();
                        boxIp.save(flush: true, failOnError: true);
                    } else {
                        log.info("BoxIp is different than last connection, create a new record");
                        boxIp = new BoxIp(serial: serial, ipAddress: ipAddress,macAddress:macAddress, lastUpdated : new Date());
                        boxIp.save(flush: true, failOnError: true);
                    }
                    def boxIpList = BoxIp.getBySerialDistinctMacAddress(serial).list();
                    if (boxIpList.size() > 1) {
                        log.info("CheckMacAddressAndSerialConnections - checking Black List-> Serial: " + serial + 
                            " Ip Address " + ipAddress);
                        BoxBlackList registered = BoxBlackList.findBySerial(serial);
                        if (!registered) {
                            BoxBlackList boxToBlackList = new BoxBlackList();
                            boxToBlackList.serial = serial;
                            Calendar stopworking = Calendar.getInstance();
                            stopworking.add(Calendar.DATE, 33);
                            boxToBlackList.dateStopWorking = stopworking.getTime();
                            boxToBlackList.save(flush: true, failOnError: true);
                            log.info("CheckMacAddressAndSerialConnections - registering in Black List-> Serial: " + 
                                serial + " Ip Address " + ipAddress);
                        }
                    }
                } catch(Exception e) {
                    log.error(e);
                    status.setRollbackOnly();
                    throw e;
                }
            }
        } else {
            log.info("BoxIpService - checkMacAddressAndSerialConnections -No Ip Address or macaddress-> Serial: "  + 
                serial);
        }
    }

	/**
	 * Take the legacySerial for update IpAddress when the box is upgrading the serial and certificate
	 * @param serial New serial number
	 * @param legacySerial Old serial number
	 * @return
	 */
	def updateBoxIPSerials (String serial, String legacySerial){
		try{		
			log.info("updateBoxIPSerials serial["+serial+"]" +" legacy serial["+legacySerial+"]")
			def boxIps= BoxIp.findAllBySerial(legacySerial)
			for(BoxIp ip:boxIps){
				log.info("updateBoxIPSerials ip: ["+ ip.serial +"]" )
				ip.serial=serial
				ip.save(flush: true, failOnError: true);
				log.info("updateBoxIPSerials ip after save: ["+ ip.serial +"]" )
			}
		}catch(e){
			e.printStackTrace()
			throw e
		}
		
	}
}
