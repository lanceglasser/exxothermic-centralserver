/**
 * SoftwareUpgradeService.groovy
 */
package mycentralserver;

import java.text.SimpleDateFormat;

import mycentralserver.app.Affiliate;
import mycentralserver.box.Box;
import mycentralserver.box.BoxStatus;
import mycentralserver.box.software.BoxSoftware;
import mycentralserver.box.software.BoxSoftwareUpgradeJob;
import mycentralserver.box.software.BoxUpgradeJob;
import mycentralserver.box.software.BoxUpgradeStep;
import mycentralserver.utils.BoxStatusEnum;
import mycentralserver.utils.Constants;
import mycentralserver.utils.SerialNumberGeneratorHelper;
import grails.converters.JSON;

/**
 * Service with the logic for the software upgrade process
 * of the ExXtractors.
 *
 * @author Cecropia Solutions
 *
 */
class SoftwareUpgradeService {

	/* Inject required services */
	def userSessionService;
	def messageSource;
	def grailsApplication;
	def restClientService;
	
	/**
	 * Return the list of Jobs depending of the current user;
	 * for HelpDesk return all the list; for others returns only 
	 * the one the user create
	 * 
	 * @return
	 */
	public def getJobList(){
		
		if( userSessionService.isHelpDesk() ){
			return BoxSoftwareUpgradeJob.findAll();
		} else {
			return BoxSoftwareUpgradeJob.findAllByCreatedBy(userSessionService.getCurrentUser());
		}
		
	} // End of getJobList method
	
	/**
	 * Get and id and validate the access to a Software Upgrade Job,
	 * will return an array with the result and the object if everything is ok
	 * 
	 * @param id		Id of the Job to get
	 * @param flash		Flash object to set the error message when there is one
	 * @return			[result int, job object]
	 */
	public def showJobDetails( long id, flash ) {
		try {
			BoxSoftwareUpgradeJob job = existsAndAllowed(id);
			if(job){
				return [Constants.RESULT_OK, job];
			} else {
				// Not found or not allowed to see it
				flash.error = messageSource.getMessage('general.object.not.found', null, null);
				return [Constants.RESULT_NOT_FOUND, null];
			}
		}catch(Exception e){
			log.error("Error showing a Software Upgrade Job: " + id, e);
			flash.error = messageSource.getMessage('default.error.problem', null, null);
			return [Constants.RESULT_ERROR, null];
		}
	} // End of showJobDetails method
	
	/**
	 * This method will find all the ExXtractors that required a Software
	 * Upgrade depending of the received Location parameter.
	 * 
	 * @param params		Request parameters
	 * @return				HashMap with required data for datatables.net
	 * @throws Exception	When something fails
	 */
    public HashMap<String, Object> getExXtractorsToUpgrade(params) throws Exception {
		
		HashMap jsonMap = new HashMap();
		def data = [];
		
		long locationId = params.locationId? params.locationId.toLong():0;
		def locations = null;
		if(locationId == 0 && !userSessionService.isAdminOrHelpDesk()){
			//If must get the data from all Locations and it is not Help Desk or Admin must filter the data for associated Locations
			locations = userSessionService.getPermitedLocations();
		}
				
		//Get the list of Connected ExXtractors by Status
		def exxtractors = Box.connectedByStatus(locationId, locations).list();
		
		// Go over the list and add the Box if it is not in the list already
		boolean mustUpgradeBox = false;
		BoxSoftware updateToVersion = null;
		
		for(box in exxtractors){
			(mustUpgradeBox, updateToVersion) = mustUpgradeExXtractor(box);
			if(mustUpgradeBox){
				data.add([box.id, box.location.name, box.name, box.serial, 
					box.softwareVersion.versionSoftwareLabel, updateToVersion.versionSoftwareLabel, false, updateToVersion.id]);
			}
		}
		
		
		//Put data to json structure
		jsonMap["draw"] = params["draw"]? params["draw"]:1;
		jsonMap["recordsTotal"] = data.size();
		jsonMap["recordsFiltered"] = data.size();
		jsonMap["error"] =  false;
		jsonMap["data"] = data;
				  
		return jsonMap;
	} // End of getExXtractorsToUpgrade method
		
    /**
     * This method receives the list of ExXtractors to Upgrade and to which version; will create the Job and all the 
     * steps for the Upgrade for each ExXtractor
     *
     * @param params
     *      Request parameters
     * @returnHashMap response with result
     */
    public HashMap<String, Object> createUpgradeJob(params) {
        HashMap jsonMap = new HashMap();
        BoxSoftwareUpgradeJob.withTransaction { status ->
            try {
                BoxSoftwareUpgradeJob job = new BoxSoftwareUpgradeJob();
                job.status = Constants.BOX_SOFTWARE_UPGRADE_STATUS_STARTED;
                job.description = "Job " + (new Date()).getTime();
                job.createdBy = userSessionService.getCurrentUser();
                job.lastUpdatedBy = userSessionService.getCurrentUser();
                final List<String> boxesList = JSON.parse(params.boxes);
                job.totalBoxes = boxesList.size();
                if(job.save()) {
                    for(int i=0; i < boxesList.size(); i++) {
                        def values = boxesList.get(i).split("-");
                        Box box = Box.read(Long.parseLong(values[0]));
                        BoxSoftware softwareVersion = BoxSoftware.read(Long.parseLong(values[1]));
                        if(box && softwareVersion) {
                            BoxUpgradeJob boxJob = new BoxUpgradeJob();
                            boxJob.status = Constants.BOX_SOFTWARE_UPGRADE_STATUS_CREATED;
                            boxJob.box = box;
                            boxJob.boxSoftware = softwareVersion;
                            boxJob.boxSoftwareUpgradeJob = job;
                            boxJob.totalSteps = 1;
                            if(boxJob.save()){
                                createBoxJobSteps(boxJob, softwareVersion);
                            } else {
                                throw new Exception("Error creating box job: " + values);
                            }
                        } else {
                            throw new Exception("Invalid box or software version: " + values);
                        }
                    }
                    jsonMap["error"] =  false;
                    jsonMap["jobId"] = job.id;
                } else { //Error when save job
                    jsonMap["error"] =  true;
                    jsonMap["msg"] = messageSource.getMessage('default.error.problem', null, null);
                }
            } catch(Exception dbEx)	{
                log.error("Error creating a software upgrade job", dbEx);
                jsonMap["error"] =  true;
                jsonMap["msg"] = messageSource.getMessage('default.error.problem', null, null);
                status.setRollbackOnly();
            }
        } // End of Transaction
        return jsonMap;
    }
	
	/**
	 * This method will check for incomplete software upgrade steps and will update
	 * the status depending of their status and current software version of the ExXtractor;
	 * if there is some pending steps will send the request to the ExXtractor to the upgrade
	 * 
	 * @param box
	 */
	public void checkSoftwareUpgradeStatus(Box box) {
		
		def incompleteBoxJobSteps = BoxUpgradeStep.getIncompleteBoxUpgradeJobSteps(box).list();
		
		if(incompleteBoxJobSteps != null && incompleteBoxJobSteps.size() > 0) {
			// There are incomplete jobs for this Box
			BoxUpgradeStep jobStep = null;
			for(int i=0; i<incompleteBoxJobSteps.size(); i++) {
				jobStep = incompleteBoxJobSteps.get(i);
				if(box.softwareVersion.versionSoftware >= jobStep.boxSoftware.versionSoftware){
					log.info("SoftwareUpgrade :: Box(" + box.serial + "-" + box.softwareVersion.versionSoftware 
						+ ") :: Set as complete the step to upgrade to version " + jobStep.boxSoftware.versionSoftware);
					//If Box version is greater or equal than step software version then this step is complete
					jobStep.status = Constants.BOX_SOFTWARE_UPGRADE_STATUS_COMPLETED;
					jobStep.lastUpdated = new Date();
					if(!jobStep.save(flush:true)){
						jobStep.errors.each {
							println it
						}
					}
					
					jobStep.boxUpgradeJob.increaseCompletedSteps();
					
				} else {
					log.info("SoftwareUpgrade :: Box(" + box.serial + "-" + box.softwareVersion.versionSoftware 
						+ ") :: Sent the upgrade to version " + jobStep.boxSoftware.versionSoftware);
					// This step is not completed and must trigger the upgrade to this version
					if(sendSoftwareVersionUpgrade(box, jobStep.boxSoftware) == Constants.RESULT_OK ) {
						jobStep.status = Constants.BOX_SOFTWARE_UPGRADE_STATUS_STARTED;
						jobStep.lastUpdated = new Date();
						jobStep.save(flush:true);
					} else {
						// Wait to next try
						log.info("status was not ok");
					}
					break; // Get out of the bucle
				}
			} // End of for
		} // End of incompleteBoxJobSteps.size() > 0
		
	} // End of checkSoftwareUpgradeStatus method
	
	/**
	 * This method will try to send the request of a software upgrade to
	 * a ExXtractor
	 * 
	 * @param box			ExXtractor to upgrade
	 * @param boxSoftware	Software Version to Upgrade
	 * @return				Status of the request; 0=OK; 6=Something wrong
	 */
	public int sendSoftwareVersionUpgrade(Box box, BoxSoftware boxSoftware ) {
		
		int errorCode = Constants.RESULT_OK;
		def json = "";
		try {
			List<HashMap> listRequest = createRequestUpdate(boxSoftware, box)
			json = restClientService.updateSoftwareVersion(listRequest)
			
			if (json == null){ 
				// Service is down or something went wrong
				errorCode = Constants.RESULT_ERROR;
			} else {
			 	// When the message was sent must change the status of the ExXtractor
				BoxStatus statusUpdatingBox = BoxStatus.findByDescription(BoxStatusEnum.UPDATING.value);
				box.status = statusUpdatingBox;
				box.save();
			}
		} catch(Exception e) {
			log.error("There was an error sending a software upgrade for Box: " + box.serial + " to Version: " + boxSoftware.versionSoftware, e);
			errorCode = Constants.RESULT_ERROR;
		}
		
		return errorCode;
	} // End of sendSoftwareVersionUpgrade method
	
	/**
	 * This method will create the required HashMap list to trigger the
	 * software upgrade request to the ExXtractor
	 * 
	 * @param boxSoft	Upgrade to this Software Version
	 * @param box		ExXtractor to upgrade
	 * @return			HashMap list with required information
	 */
	public List<HashMap> createRequestUpdate(BoxSoftware boxSoft, Box box) {
		
		List<HashMap> listRequest = new ArrayList<HashMap>();
		HashMap hasRequest = new HashMap();
		def pathBaseUrl = grailsApplication.config.baseUrlMyBoxSoftwareUpdates;
		hasRequest.put(Constants.PARAM_PACKAGE_URI, pathBaseUrl + boxSoft.getId());
		hasRequest.put(Constants.PARAM_CHECKSUM, boxSoft.getChecksum());
		hasRequest.put(Constants.PARAM_VERSION, boxSoft.getVersionSoftwareLabel());
		hasRequest.put(Constants.PARAM_SERIAL_NUMBER, box.getSerial());
		listRequest.add(hasRequest);
		return listRequest;
		
	} // End of createRequestUpdate method
	
	/**
	 * This method will return a Json array with the details of the steps
	 * of a Software Job
	 * 
	 * @param id	Id of the Software Job
	 * @return		HashMap with the information of all the steps of the Job
	 */
	public HashMap<String, Object> getJobStepsDetail(long id) {
		
		HashMap<String, Object> mapResponse = new HashMap<String, Object>();
		BoxUpgradeJob job = BoxUpgradeJob.read(id);
		if(job){
			mapResponse.put("error", false);
			SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
			def stepsDetail = [];
			for(upgradeStep in job.boxUpgradeSteps){				
				stepsDetail.add(
					[	upgradeStep.boxSoftware.versionSoftware, 
						messageSource.getMessage('box.job.status.' + upgradeStep.status, null, null), 
						dateFormatter.format(upgradeStep.lastUpdated)
					]);
			}
			mapResponse.put("serial", job.box.serial);
			mapResponse.put("steps", stepsDetail);
		} else {
			mapResponse.put("error", true);
			mapResponse.put("msg", "Not found");
		}
		return mapResponse;
		
	} // End of getJobStepsDetail method
	
	/**
	 * This method will validate that the object exists and that the current
	 * user is allowed to see it
	 *
	 * @param id			Id of the object to validate
	 * @return				The object when ok; null when not found or not allowed
	 * @throws Exception	When something crash
	 */
	protected BoxSoftwareUpgradeJob existsAndAllowed( long id) throws Exception {
		BoxSoftwareUpgradeJob job = BoxSoftwareUpgradeJob.read(id);
		if(job) {
			if(userSessionService.isHelpDesk()){
				return job;
			} else {
				return (job.createdBy.id == userSessionService.getCurrentUser().id)? job:null;
			}
		} else {
			return null;
		}
	} // End of existsAndAllowed method
	
    /**
     * This method will find if there is a new Software Version for an ExXtractor and returns the final Version to 
     * Upgrade; return false when there is a job incomplete for the ExXtractor
     * 
     * @param box
     *      ExXtractor to check for a new Software Version
     * @return Array with [mustUpgradeOrNot, finalVersionToUpgradeIfExists]
     * @throws Exception Unexpected grails exception
     */
    protected def mustUpgradeExXtractor( final Box box ) throws Exception {
        def incompleteBoxJobs = BoxUpgradeJob.getIncompleteBoxUpgradeJob(box).list();
        if(incompleteBoxJobs.size() == 0) { //There are not incomplete jobs
            final BoxSoftware versionSoftware = (box.softwareVersion)? box.softwareVersion : null;
            final int versionId = (versionSoftware)? versionSoftware.versionSoftware : 0;
            final Affiliate affiliate = SerialNumberGeneratorHelper.getPartnerFromSerial(box.serial);
            def listSoftwareVersion = BoxSoftware.getAllByVersionSoftwareGreaterThanEnableAndSupported(versionId,
                box.imageType, affiliate).list([max: 1, sort: BoxSoftware.SimpleFields.VERSION_SOFTWARE.fieldName(),
                    order: Constants.SORT_DESC]);
            return (listSoftwareVersion.size() > 0)?
                        [(listSoftwareVersion.get(0).id > box.softwareVersion?.id), listSoftwareVersion.get(0)]:[false, null];
        } else {
            // There are incomplete jobs for this Box; cannot start a new one
            return [false, null];
        }
    }
	
    /**
     * This method will get the next software version of a particular Box from
     * a version number
     * 
     * @param box				Box to check
     * @param versionNumber		Version number from where to get the next version
     * @return					BoxSoftware object or null when not found
     */
    protected BoxSoftware getNextSoftwareVersion(Box box, int versionNumber) {
        Affiliate affiliate = SerialNumberGeneratorHelper.getPartnerFromSerial(box.serial);
        def listSoftwareVersion =
                BoxSoftware.getAllByVersionSoftwareGreaterThanEnableAndSupported(versionNumber, box.imageType, affiliate).
                list([max: 1, sort: BoxSoftware.SimpleFields.VERSION_SOFTWARE.fieldName(), order: Constants.SORT_ASC]);
        BoxSoftware boxSoftware = null;
        boolean isUpdateAvailable = false;
        if(listSoftwareVersion.size() > 0) {
            boxSoftware = listSoftwareVersion.get(0);
            if(boxSoftware.id > box.softwareVersion?.id) {
                isUpdateAvailable = true;
            }
        }
        return (isUpdateAvailable)? boxSoftware:null;
    }
	
    /**
     * Creates the step that upgrade the box to the latest software version
     * 
     * @param boxJob
     *         BoxJob object with the header of the process for the server
     * @param softwareVersion
     *         Latest available software version for the server
     * @throws Exception If the box upgrade step can't be created
     */
    protected void createBoxJobSteps(BoxUpgradeJob boxJob, BoxSoftware finalSoftwareVersion) throws Exception {
        Box box = boxJob.box;
        BoxUpgradeStep upgradeStep = new BoxUpgradeStep();
        upgradeStep.boxUpgradeJob = boxJob;
        upgradeStep.boxSoftware = finalSoftwareVersion;
        if(!upgradeStep.save()){
            throw new Exception("Failed creating a step");
        }
    }
	
} // End of Class
