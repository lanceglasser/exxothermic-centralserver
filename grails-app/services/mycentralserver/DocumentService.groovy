package mycentralserver

import java.util.HashMap
import java.util.List

import mycentralserver.company.CompanyLocation
import mycentralserver.docs.Document
import mycentralserver.content.Content

import org.apache.commons.io.FileUtils
import org.apache.commons.lang.ArrayUtils;
import org.junit.After;

import mycentralserver.utils.CloudFilesPublish
import mycentralserver.utils.FileHelper
import mycentralserver.utils.SchedulerUtils;
import mycentralserver.utils.Utils
import  mycentralserver.utils.Constants
import grails.validation.ValidationException
import mycentralserver.user.User;

import java.util.Date
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.text.ParseException
import java.util.*


class DocumentService {
	RestClientService restClientService
	def userSessionService
	def grailsApplication

	/**
	 * Return the list of documents for the current
	 * user depending of role and the parameters
	 *
	 * @return	[documents, totalCount, filteredCount]; null when error
	 */
	public def getDocumentsOfUser(HashMap<String, Object> queryParams) {
			
		try {
			def documents = new ArrayList();
			int totalCount = 0;
			int filteredCount = 0;
			
			if(userSessionService.isHelpDesk() || userSessionService.isAdmin()) {
				totalCount = Document.listAllDocumentsForUITable(0, 0, queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), "", false, null,true,false,userSessionService.getCurrentSessionAffiliate(), null, queryParams.get(Constants.LOCATION_ID),true).list()[0];
				filteredCount = Document.listAllDocumentsForUITable(0, 0, queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH), false, null,true,false,userSessionService.getCurrentSessionAffiliate(), null, queryParams.get(Constants.LOCATION_ID),true).list()[0];
				documents = Document.listAllDocumentsForUITable(
					queryParams.get(Constants.DATA_TABLES_START), queryParams.get(Constants.DATA_TABLES_LENGTH) ,
					queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME) ,
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH), false, null,true,false, userSessionService.getCurrentSessionAffiliate(), null, queryParams.get(Constants.LOCATION_ID),false).list();
			}else {
				User user = userSessionService.getCurrentUser();
				def listCompanies = userSessionService.getCompaniesForUser();
				
				totalCount = Document.listAllDocumentsForUITable(0, 0, queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), "", false, user,false,false,null,listCompanies.collect(){it.id}, queryParams.get(Constants.LOCATION_ID),true).list()[0];
				filteredCount = Document.listAllDocumentsForUITable(0, 0, queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH), false, user,false,false, null, listCompanies.collect(){it.id}, queryParams.get(Constants.LOCATION_ID),true).list()[0];
				documents = Document.listAllDocumentsForUITable(
					queryParams.get(Constants.DATA_TABLES_START), queryParams.get(Constants.DATA_TABLES_LENGTH) , queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME) ,
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH), false, user,false,false, null, listCompanies.collect(){it.id}, queryParams.get(Constants.LOCATION_ID),false).list();
			}
			
			return [documents, totalCount, filteredCount]
		} catch(Exception e) {
			log.error("Error getting the list of locations of the user", e);
			return null;
		} // End of catch
	} // End of getDocumentsOfUser
	
	
	
	def readDocumentById(Long id) {
		return Document.read(id)
	}

	private def findDocumentEnables(CompanyLocation location) {
		String timezone = (location.timezone != null) ? location.timezone : "UTC";

	    DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    DateFormat formatTimeZone = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    formatTimeZone.setTimeZone(TimeZone.getTimeZone(timezone));
	
	    Date currentDate = (Date) utcFormat.parse(formatTimeZone.format(new Date()));
		def documents = Document.withCriteria {
			locations {
				  'in'('id', location.collect {loc ->
					return loc.id;
				})
			}
		  }
		// gets the offer with Type PDF
		def offers = Content.withCriteria {
				eq('enabled', true)
				eq('type', 'offer')
				eq('isFile', true)
				eq('referenceType', Constants.OFFER_REFERENCE_PDF)				
				locations {
					  'in'('id', location.collect {loc ->
						return loc.id;
					})
				}
			  }
		// end of getting the PDF offers
				
	 	def docsEnabled = documents.findAll{it.expirationDate == null || it.expirationDate >= currentDate}
		List<Object> validDocs = new ArrayList<Object>();

		for(Document doc: docsEnabled){
			validDocs.add(convertDocumentToJson(doc, timezone));
		}
		
		
		
		def offersEnabled = offers.findAll{it.schedule?.endDate  == null || it..schedule.endDate >= currentDate}
				
		for(Content offer: offersEnabled){
			validDocs.add(convertOfferToDocumentJson(offer, timezone));
		}
		
		return validDocs;
	}

	public convertDocumentToJson(Document doc, timezone){
		return [
			id:		doc.id,
			name: 	Utils.getStringValue(doc.name),
			url: 	Utils.getStringValue(doc.url),
			type: 	Utils.getStringValue(doc.type.name),
			expirationDate: SchedulerUtils.strDateToUnixTimestamp(doc.expirationDate, timezone)
		 ];
	}
	
	private convertOfferToDocumentJson(Content offer, timezone){
		return [
			id:		offer.id,
			name: 	Utils.getStringValue(offer.title),
			url: 	Utils.getStringValue(offer.url),
			type: 	Utils.getStringValue(Constants.DEFAULT_OFFERS_CATEGORY),
			expirationDate: SchedulerUtils.strDateToUnixTimestamp(offer.schedule?.endDate , timezone)
		 ];
	}

	public String saveDocument(Document doc, tempfile){
		def fileFullPath;

		try {
			if(doc.validate()) {
				def fileNameOrigin = tempfile.getOriginalFilename()
				doc.filename = fileNameOrigin;

				if (!tempfile.isEmpty() && !fileNameOrigin.isEmpty()){
					//save file on server
					try{
						fileFullPath = saveFile(tempfile);
						CloudFilesPublish cloudService = new CloudFilesPublish(fileFullPath, grailsApplication.config.rackspaceUser, grailsApplication.config.apiKey, grailsApplication.config.rackspaceDocumentContainer);
						doc.url = cloudService.uploadFileToRackspace();
					} catch(e) { return 'document.error.saving'; }
				}
				 if (doc.save(flush: true, failOnError: true)) { return null; } else { return 'document.error.saving';}
			}else{
				return 'default.error.problem';
			}
		}catch(e){
			throw e
		}
	}

	public String deleteDocument(Document doc){
		try{
			CloudFilesPublish cloudService = new CloudFilesPublish(doc.filename, grailsApplication.config.rackspaceUser, grailsApplication.config.apiKey, grailsApplication.config.rackspaceDocumentContainer);
			def locations = doc.locations;
			String file = doc.url;
			int id = doc.id;

			doc.delete();

			if (Document.read(id) == null){
				try{
						cloudService.deleteFileToRackspace(file);

						def jsonTransactionStatus = null;
						def affectedLocations = new ArrayList<Document>();

						if(locations != null && !locations.isEmpty()) {
							jsonTransactionStatus = updateBoxesOfLocations(locations);
							//contentsInfo = getDocumentNames(affectedLocations);
						}

						if(jsonTransactionStatus == null && (locations != null && !locations.isEmpty())) {
							return 'boxes.syncErrorMessage';
						} else { return null; }
					} catch(e) { e.printStackTrace(); }
			} else {
				return 'document.error.deleting';
			}
		} catch(e) { return 'document.error.deleting'; }
	}

	public String updateDocument(Document doc, tempfile){
		def fileFullPath
		String previousFile
		try{
			if(doc.validate()) {
				def fileNameOrigin = (tempfile !=null)? tempfile.getOriginalFilename():'';

				if (tempfile != null && !tempfile.isEmpty() && !fileNameOrigin.isEmpty()){
					previousFile = doc.url;

					//save file on server
					try{
						fileFullPath = saveFile(tempfile);
						CloudFilesPublish cloudService = new CloudFilesPublish(fileFullPath, grailsApplication.config.rackspaceUser, grailsApplication.config.apiKey, grailsApplication.config.rackspaceDocumentContainer);
						doc.url = cloudService.uploadFileToRackspace();
						doc.filename = fileNameOrigin;

						//now delete previous file
						if(previousFile != null && !previousFile.isEmpty()){
							try{ cloudService.deleteFileToRackspace(previousFile); } catch(e) { e.printStackTrace(); }
						}
					} catch(e) { return 'document.error.saving'; }
				}

				//see if we need to update a box
				if(doc.save(flush: true, failOnError: true)){
					HashMap<String, String> contentsInfo = new HashMap<String, String>()
					def jsonTransactionStatus = null
					def affectedLocations = new ArrayList<Document>();
					def locations = doc.locations;

					if(locations != null && !locations.isEmpty()) {
						jsonTransactionStatus = updateBoxesOfLocations(locations);
						contentsInfo = getDocumentNames(affectedLocations);
					}

					if(jsonTransactionStatus == null && (locations != null && !locations.isEmpty())) {
						return 'mycentralserver.custombuttons.syncErrorMessage';
						
					} else { return null; }
				}else{
					return 'document.error.saving';
				}
			}else{
				return 'default.error.problem';
			}
		}catch(e){
			e.printStackTrace()
			throw e
		}
	}

	public String saveFile(docFile) {
		def response = null
		try {
			def storagePath = grailsApplication.config.basePathContentTempFiles;
				def fileNameOrigin = docFile.getOriginalFilename();
				def fullPath = Utils.getPathFromFile(storagePath, fileNameOrigin);

				//verify that directory exists
				File directory = new File(storagePath);
				if (!directory.exists()){
					directory.mkdirs();
				}

				//save file in local storage
				docFile.transferTo( new File(fullPath) )
				File tempFile = new File(fullPath)

				String ext = FileHelper.getFileExtension(fileNameOrigin)

				return fullPath;
			} catch(e)	{
				e.printStackTrace()
				throw e
			}
		return response
	} // END saveFile


	def getDocumentNames(affectedLocations){
		HashMap<String, String> docsInfo = new HashMap<String, String>();

		for(location in affectedLocations){
			for(document in location.documents) {
				docsInfo.put(document.id.toString(), document.name);
			}
			
			def offers = Content.withCriteria {
				eq('enabled', true)
				eq('type', 'offer')
				eq('isFile', true)
				eq('referenceType', Constants.OFFER_REFERENCE_PDF)				
				locations {
					  'in'('id', location.collect {loc ->
						return loc.id;
					})
				}
			  }
			// end of getting the PDF offers
			
			String timezone = (location.timezone != null) ? location.timezone : "UTC";
			DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			DateFormat formatTimeZone = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			formatTimeZone.setTimeZone(TimeZone.getTimeZone(timezone));				
			Date currentDate = (Date) utcFormat.parse(formatTimeZone.format(new Date()));
					
			def offersEnabled = offers.findAll{it.schedule?.endDate  == null || it.schedule.endDate  >= currentDate}
								
			for(Content offer: offersEnabled){
				docsInfo.put("O"+offer.id.toString(), offer.title);
			}
		}

		return  docsInfo;
	}

	def updateBoxesOfLocations(affectedLocations){
		HashMap<String, ArrayList<Document>> hashData = new HashMap<String, ArrayList<Document>>()

		for(location in affectedLocations){
			for(box in location.boxes){
				def listDocument = new ArrayList<Document>()
				def docEnables = findDocumentEnables(location);
				listDocument.addAll(docEnables)
				hashData.put(box.serial, listDocument)
			}
		}

		List<HashMap> listDocumentToRequest = convertHashToHasJsonToConvertJsonUtil(hashData);
				
		return restClientService.setDocuments(listDocumentToRequest);
	} // END updateBoxesOfLocations


	public convertHashToHasJsonToConvertJsonUtil(HashMap<String, List<Object>> hash ) {
		List<Object> listDocuments;

		HashMap<HashMap> has = new HashMap<HashMap>()
		HashMap<String, Object> hasRequest = new HashMap<String, Object>()
		List<HashMap> listRequestSetDocs = new ArrayList<HashMap>();

		for (String serialNumber : hash.keySet()) {
			hasRequest = new HashMap();
			listDocuments = hash.get(serialNumber);
			hasRequest.put("serialNumber", serialNumber);
			hasRequest.put("documents", listDocuments);

			listRequestSetDocs.add(hasRequest);
		}

		return listRequestSetDocs;
	} // END convertHashToHasJsonToConvertJsonUtil

	private def findContentEnableAndConvertInHash(CompanyLocation location) {
		
		def listDocument = new ArrayList<Document>();
		def docEnables = findDocumentEnables(location);
		listDocument.addAll(docEnables)
		
		return listDocument;
	}

} // END DocumentService
