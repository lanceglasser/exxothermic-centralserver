package mycentralserver

import java.util.HashMap;
import java.util.List;

import mycentralserver.box.Box;
import mycentralserver.custombuttons.CustomButton;
import mycentralserver.device.Device;

/**
 * Service in charge of much of the logic related with the
 * Devices; for now it's only been use to send the list of
 * devices that must use smaller packages for audio streaming.
 * 
 * @author sdiaz
 */
class DeviceService {

	/* Inject required services */
	def restClientService;
	
	/**
	 * Will send the list of serials to update and the devices to send to
	 * each one of the boxes.
	 * 
	 * @return   
	 * 		HashMap<String, Object> with the serials and devices
	 */
    def sendListOfDevicesWithSmallerPackages() {
		HashMap<String, Object> requestMap = new HashMap<String, Object>();
		requestMap.put("serials", getConnectedServersSerialsList());
		requestMap.put("devices", getDevicesWithSmallerPackagesHashMap());
		return  restClientService.sendListOfDevicesWithSmallerPackages(requestMap);
	}
	
	/**
	 * Return the list of serial numbers of the connected
	 * boxes
	 * 
	 * @return 
	 * 		List<String> with the list of serials of 
	 * 		the connected boxes
	 */
	private getConnectedServersSerialsList() {
		//Get the list of Connected ExXtractors by Status
		def exxtractors = Box.connectedByStatus(0, null).list();
		List<String> hashMapList = new ArrayList<String>();
		// Go over the list and add the Box if it is not in the list already
		for(exx in exxtractors){
			hashMapList.add(exx.serial);
		}
		return hashMapList;
	}
	
	/**
	 * Get the list of devices that must use smaller packages 
	 * for audio streaming
	 * 
	 * @return
	 * 		List<HashMap> like {[manufacturer: "", buildFingerprint: ""]}
	 */
	private List<HashMap> getDevicesWithSmallerPackagesHashMap() {
		def devicesList = Device.findAllByUseSmallerPackages(true);
		List<HashMap> hashMapList = new ArrayList<HashMap>();
		HashMap hasRequest;
		for (Device device : devicesList) {
			hasRequest = new HashMap();
			hasRequest.put("manufacturer", device.manufacturer);
			hasRequest.put("buildFingerprint", device.buildFingerprint);
			hashMapList.add(hasRequest);
		}
		return hashMapList;
	}
}
