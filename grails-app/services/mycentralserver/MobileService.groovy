package mycentralserver

import java.net.URLDecoder;
import java.util.Date;


import grails.plugins.rest.client.ErrorResponse;
import grails.plugins.rest.client.RestBuilder
import grails.plugins.rest.client.RestResponse
import groovy.json.JsonSlurper
import groovy.json.StreamingJsonBuilder
import mycentralserver.app.AppUser
import mycentralserver.app.AppDevice
import mycentralserver.app.AppEvent
import mycentralserver.app.AppData
import mycentralserver.company.CompanyLocation
import mycentralserver.content.Content;
import mycentralserver.utils.Constants;
import mycentralserver.utils.SchedulerUtils;
import mycentralserver.utils.Utils

import java.util.Date
import java.text.DateFormat
import java.text.SimpleDateFormat

import org.codehaus.groovy.grails.web.json.*;
import org.codehaus.groovy.grails.web.mapping.ResponseCodeMappingData;
import org.springframework.http.ResponseEntity

import grails.converters.JSON;


class MobileService {
	
	def grailsApplication	
	def contentService


 public registerAppEvent(String deviceId, String locationId, String event, String details) {
	 try{
					 
		 AppUser appUser;
		 AppDevice appDevice;
		 AppEvent appEvent;
		 CompanyLocation location;
		 if(event != null && event.trim() != "" )
		 {
			 appDevice = AppDevice.read(deviceId);
			 if(appDevice) // if device exist we need to check if it already have the location registered on it location list
			 {
				location = CompanyLocation.read(locationId);
				if(!location)// invalid locationId
				{
					return 2;
				}
				def locations = appDevice.locations.findAll{it.id == locationId};
				if(locations.size() == 0) // if the device visited the location before just register the event, if not the location is added to device's location list
				{
					appDevice.addToLocations(location);
				}
				appEvent = new AppEvent();
				appEvent.device = appDevice;
				appEvent.event = event;
				appEvent.details = details;
				appEvent.location = location;
				appEvent.save(flush: true, failOnError: true);
				
				 
			 }else{// if not exist the device returns a code of invalid deviceId
				 return 1;
			 }
		 }else{
			 return 3;
		}
		 
		 return 0;
	 }
	 catch(Exception ex) {
		 log.error(" Error creating new appEvent " +  ex)
		 return 500
	 }
 } // END registerAppEvent
 
 def facebookMe(String appToken) {
	 
	 def baseUrl = grailsApplication.config.URLfacebookAuthMe
	 
	 def rest = new RestBuilder()
 
	 try {
		 log.info(baseUrl);
		 def resp = rest.get( baseUrl + appToken );
		 resp as JSON;
		 //log.info("setChannelStatus url: " + baseUrl + "  JSON Response: " + resp.json.toString());
		 if(resp.json.error!=null)
		 {
			 return [true,resp.json.error.message.toString(), resp.json.error.code];
		 }else{
			 return [false ,"success", resp.json.id];
		 }
		 
		 
	 }
	 catch(Exception ex) {
		 log.error(" facebookMe from Central Server " +  ex)
		 return false
	 }
} // END facebookMe

def facebookApp(String appToken) {
 
 def baseUrl = grailsApplication.config.URLfacebookAuthApp
 
 def rest = new RestBuilder()

 try {
	 log.info(baseUrl);
	 def resp = rest.get( baseUrl + appToken );
	 resp as JSON;
	 //log.info("setChannelStatus url: " + baseUrl + "  JSON Response: " + resp.json.toString());
	 if(resp.json.error!=null)
	 {
		 return [true,resp.json.error.message.toString(), resp.json.error.code];
	 }else{
		 AppData appData = AppData.findByName(grailsApplication.config.appName);
		 def appId;
		 if(appData)
		 {
			appId =  appData.appid;
		 }else{
			 appId =  grailsApplication.config.facebookAppId;
		 }
	 
		if(resp.json.id.toString() == appId)
		{
			return [false ,"success", resp.json.id];
		}else{
			return [false ,"invalid app access", resp.json.id];
		}
		 
	 }
 }
 catch(Exception ex) {
	 log.error(" facebookMe from Central Server " +  ex)
	 return false
 }
} // END facebookApp


public registerAppUser(String deviceId, String socialId, String socialToken, String socialKey, String socialType) {
	 try{
					 
		 AppUser appUser;
		 AppDevice appDevice;
		 boolean deviceCreated =false;
		 
		 if(deviceId == "") // if the device is not registered
		 {
			 appDevice = new AppDevice();
			 appDevice.dateCreated = new Date();
			 deviceCreated =true;
			 
		 }else{
			 appDevice = AppDevice.read(deviceId);
			 if(!appDevice) // check if the send deviceId exist
			 {
				 appDevice = new AppDevice();
				 appDevice.dateCreated = new Date();
				 deviceCreated = true;
			 }
		 }
		 if(!appDevice.save(flush: true, failOnError: true))
		 {
			  return [true ,"Cannot subscribe the device", null];
		 }
		 def jsonMe=false;
		 def jsonApp=false;
		 def message=false;
		 def code=false;
		 if(socialId != "" && socialId != null && socialToken != "" && socialToken != null) // if the user have signed in with social Network
		 {
			 
			 (jsonMe,message,code) = facebookMe(socialToken);
			 
			 if(!jsonMe) // if error is false, so continue with next validation
			 {
				 (jsonApp,message,code) = facebookApp(socialToken);
				 
				 if(!jsonApp)
				 {
					 if(socialType != "" && socialType != null )
					 {
						 appUser = AppUser.withCriteria(uniqueResult: true) {
							eq('socialId', socialId)
							eq('socialType', socialType.toString())
						  }
						 if(appUser)// if the user already exist do not create new user, just update the socialToken, socialKey and returns the existing socialId
						 {
							 appUser.socialId = socialId;
							// appUser.socialToken = socialToken;
							// appUser.socialKey = socialKey;
							 appUser.socialType = socialType;
							 appUser.lastUpdated = new Date();
											 
						 }else{ // creates new appUser with social data
							 appUser = new AppUser();
							 appUser.socialId 	= socialId;
							 //appUser.socialToken = socialToken;
							 //appUser.socialKey = socialKey;
							 appUser.socialType = socialType;
							 appUser.dateCreated = new Date();
						 }
						 //appUser.addToDevices(appDevice);
						 
						 appUser.save(flush: true, failOnError: true);
						 appDevice.appUser = appUser;
						 appDevice.save(flush: true, failOnError: true);
						 
					 }else{
						 if(deviceCreated)
						 {
							 appDevice.delete();
						 }
						 return [true , "invalid socialType: " + socialType, null];
					 }
				 }else{
					 if(deviceCreated)
					 {
						 appDevice.delete();
					 }
					 return [true , message, code];
				 }
			 }else{
				 if(deviceCreated)
				 {
					 appDevice.delete();
				 }
				 return [true , message, code];
			 }
			 
		 }
		 return [false, "success",appDevice.id];
	 }
	 catch(Exception ex) {
		 log.error(" Error creating new appUser " +  ex)
		 return [true ,"Error creating new appUser", null];
	 }
 } // END registerAppUser

public def findOffers(offerList, String timezone, device) {
	
	//String timezone = "UTC"//(location.timezone != null) ? location.timezone : "UTC";
	
	DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	DateFormat formatTimeZone = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd");
	formatTimeZone.setTimeZone(TimeZone.getTimeZone(timezone));
	Date currentDate = (Date) utcFormat.parse(formatTimeZone.format(new Date())); // Get the current date by timeZone || default UTC.

	DateFormat timeZoneID = new SimpleDateFormat("z");
	timeZoneID.setTimeZone(TimeZone.getTimeZone(timezone));
	
	def contentEnabled = offerList.findAll{
		it.enabled == true && it.schedule != null && (it.schedule.endDate == null || it.schedule.endDate >= currentDate)
	}
	
	List<Object> validContent = new ArrayList<Object>();

	List<String> dayOfWeek = Arrays.asList("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
	int today = dayOfWeek.indexOf(new SimpleDateFormat("EEEE").format(currentDate));

	for(Content cb: contentEnabled){

	  if (cb.schedule != null) { // Do I have a schedule?
		String[] days = (cb.schedule.days != null && cb.schedule.days != '') ? cb.schedule.days.split(",") : [today.toString()];
		String[] hours = (cb.schedule.hours != null && cb.schedule.hours != '') ? cb.schedule.hours.split("-") : ["00","00"];

			if (hours[1] == "00") {
				hours[1] = "23:59:59";
			} else {
				hours[1] = (hours[0] == hours[1]) ? hours[1]+":59:59" : hours[1]+":00:00";
			}

		  hours[0] +=":00:00";
		if (Arrays.asList(days).contains(today.toString())){ //Should I display the AD today?
				  Date startDate = (Date) utcFormat.parse(simpleFormat.format(currentDate) + " " + hours[0]);
				  Date endDate = (Date) utcFormat.parse(simpleFormat.format(currentDate) + " " + hours[1]);
		  if (endDate.before(startDate)){ // Solve wrong date ranges
			endDate.setTime(endDate.getTime() + 1*24*60*60*1000);
		  }
		  if (startDate.before(currentDate) && endDate.after(currentDate)){ //It's show time
			  def jsonContent = convertOfferToJson(cb, startDate, endDate, timezone, currentDate, hours, days, device);
			  validContent.add(jsonContent);
		  }
		}
	  } else {
		  def jsonContent = convertOfferToJson(cb,
			  (Date) utcFormat.parse(simpleFormat.format(currentDate) + " 00:00:00"),
			  (Date) utcFormat.parse(simpleFormat.format(currentDate) + " 23:59:59"),
			  timezone, currentDate, ["00:00:00", "23:59:59"], [0,1,2,3,4,5,6] , device);
		  validContent.add(jsonContent);

	  }
	}
	
	return validContent;

}

public getDeviceOffers(AppDevice appDevice){
	// gets all the offers
	
	def locationsList = appDevice.locations;
	
	def offerList = new ArrayList<Content>();
	for(location in locationsList){
		offerList.addAll(location.contents.findAll{it.type="offer"});
	}
	offerList = offerList.unique(); // remove duplicates
	// the offers list is ready
	return offerList;
}

public getUserOffers(AppUser appUser){
	// gets all the offers
	def devices = AppDevice.withCriteria {
		eq('appUser', appUser)
	  }
	def locationsList = new ArrayList<CompanyLocation>();
	for(device in devices){
		locationsList.addAll(device.locations);
	}
	locationsList = locationsList.unique();
	
	def offerList = new ArrayList<Content>();
	for(location in locationsList){
		offerList.addAll(location.contents.findAll{it.type="offer"});
	}
	offerList = offerList.unique(); // remove duplicates
	// the offers list is ready
	return offerList;
}

	 
private convertOfferToJson(Content cb, startDate, endDate, timezone, currentDate, hours, days, device){
	DateFormat timeZoneID = new SimpleDateFormat("z");
	timeZoneID.setTimeZone(TimeZone.getTimeZone(timezone));
   def thumbnail, dialog;
   thumbnail = Utils.getStringValue(cb.dialogImage);
	if(device == "2") // phone
	{
		dialog = Utils.getStringValue(cb.dialogImage);
	}else{ // 3 if tablet
		dialog = Utils.getStringValue(cb.dialogImageTablet);
	}
			 
	return [
		id:					cb.id,
		title: 				Utils.getStringValue(cb.title),
		moreInfoUrl: 			Utils.getStringValue(cb.url),
		backgroundColor: 		Utils.getStringValue(cb.color).replace("#", ""),
		description: 			Utils.getStringValue(cb.description),
		thumbnailImageUrl:		thumbnail,
		dialogImageUrl:		dialog,
		schedule:		[
			expirationDate: SchedulerUtils.strDateToUnixTimestamp(cb.schedule.endDate, timezone),
			timezone: timeZoneID.format(currentDate)
			]
	   ];
}
 
	

} // END class
