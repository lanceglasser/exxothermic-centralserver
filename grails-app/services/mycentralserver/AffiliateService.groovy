/**
 * AffiliateService.groovy
 */
package mycentralserver;

import mycentralserver.app.Affiliate;
import mycentralserver.utils.Constants;
import mycentralserver.utils.PartnerEnum;

/**
 * Service class with most of the Logic related with the Affiliates
 * 
 * @author Cecropia Solutions
 */
class AffiliateService {

	/* Inject required services */
	def grailsApplication;
	
	/**
	 * Returns the contact email of the current Affiliate if exists on session
	 * or the default Affiliate
	 * 
	 * @return	Contact Email 
	 */
    public String getCurrentAffiliateConcatEmail(Affiliate affiliate) {
		if(!affiliate){
			affiliate = Affiliate.findByCode(PartnerEnum.getDefaultPartnerCode());
		}			
		return (affiliate && affiliate.emailContact && affiliate.emailContact.trim() != "")? 
			affiliate.emailContact:grailsApplication.config.grails.mail.default.from;		 
    } // End of getCurrentAffiliateConcatEmail method
	
} // End of Class
