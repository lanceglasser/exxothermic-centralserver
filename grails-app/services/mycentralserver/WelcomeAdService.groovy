package mycentralserver

import java.util.HashMap;
import java.util.List;

import mycentralserver.app.AppSkin;
import mycentralserver.app.WelcomeAd
import mycentralserver.app.WelcomeAd.WelcomeType;
import mycentralserver.beans.BoxesSyncResult;
import mycentralserver.box.Box;
import mycentralserver.company.CompanyLocation;
import mycentralserver.utils.Constants
import mycentralserver.utils.PartnerEnum;
import mycentralserver.utils.SerialNumberGeneratorHelper;
import mycentralserver.utils.Utils;
import mycentralserver.user.User;

class WelcomeAdService {

    RestClientService restClientService
	def grailsApplication
	def userSessionService
		
	private static final String TYPE = "type";
	private static final String SMALL_IMAGE_URL = "smallImageUrl";
	private static final String MEDIUM_IMAGE_URL = "mediumImageUrl";
	private static final String LARGE_IMAGE_URL = "largeImageUrl";
	private static final String VIDEO_URL = "videoUrl";
	private static final String SKIP_ENABLE = "skipEnable";
	private static final String SKIP_TIME = "skipTime";
	
	
	/**
	 * Return the list of ads for the current
	 * user depending of role and the parameters
	 *
	 * @return	[welcomeList, totalCount, filteredCount]; null when error
	 */
	public def getWelcomeAdsOfUser(HashMap<String, Object> queryParams) {
			
		try {
			def ads = new ArrayList();
			int totalCount = 0;
			int filteredCount = 0;
			
			if(userSessionService.isHelpDesk() || userSessionService.isAdmin()) {
				totalCount = WelcomeAd.listAllAdsForUITable(0, 0, queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), "", null,true,false,null, queryParams.get(Constants.LOCATION_ID),true).list()[0];
				filteredCount = WelcomeAd.listAllAdsForUITable(0, 0, queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH), 
					null,true,false,null, queryParams.get(Constants.LOCATION_ID), true).list()[0];
				ads = WelcomeAd.listAllAdsForUITable(
					queryParams.get(Constants.DATA_TABLES_START), queryParams.get(Constants.DATA_TABLES_LENGTH) ,
					queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME) ,
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH), null,true,false, null, queryParams.get(Constants.LOCATION_ID),false).list();
			}else {
				User user = userSessionService.getCurrentUser();
				def listCompanies = userSessionService.getCompaniesForUser();
				
				totalCount = WelcomeAd.listAllAdsForUITable(0, 0, queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), "", user,
					false, false, listCompanies.collect(){it.id}, queryParams.get(Constants.LOCATION_ID),true).list()[0];
				filteredCount = WelcomeAd.listAllAdsForUITable(0, 0, queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH), 
					user,false,false, listCompanies.collect(){it.id}, queryParams.get(Constants.LOCATION_ID),true).list()[0];
				ads = WelcomeAd.listAllAdsForUITable(
					queryParams.get(Constants.DATA_TABLES_START), queryParams.get(Constants.DATA_TABLES_LENGTH) , queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME) ,
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH), user,
						false,false, listCompanies.collect(){it.id}, queryParams.get(Constants.LOCATION_ID),false).list();
			}
			
			return [ads, totalCount, filteredCount]
		} catch(Exception e) {
			log.error("Error getting the list of locations of the user", e);
			return null;
		} // End of catch
	} // End of getWelcomeAdsOfUser
	
	
	
	
	/**
	 * Updates the welcome ad on boxes that have this object as active
	 * @param affectedLocations
	 * @param welcomeAd
	 * @return
	 */
	public BoxesSyncResult updateBoxesOfLocations(affectedLocations, WelcomeAd welcomeAd){
		
		log.debug("--> Update Welcome Ads of Boxes of Locations <--");
		HashMap welcomeAdHash;
		HashMap<String, Object> hashData = new HashMap<String, AppSkin>();
		HashMap<String, Integer> boxesLocations = new HashMap<String, Integer>();
		
		List<String> serials = new ArrayList();
		List<String> serialsToDefault = new ArrayList();
				
		//Get hash for skin
		welcomeAdHash = getWelcomeAdHashMap(welcomeAd);
		for(location in affectedLocations){			
			if(location.welcomeAd == null){//Must set to default
				for(box in location.boxes){
					serialsToDefault.add(box.serial);
				}
			} else { //New Association
				for(box in location.boxes){
					serials.add(box.serial);
				}
			}
		}
		
		BoxesSyncResult boxesSyncResult = new BoxesSyncResult("Welcome Ads");
		boolean syncFail = false;
		//Update the new associated boxes
		if(!serials.isEmpty()){
			hashData.put("serials", serials);
			hashData.put(Constants.PARAM_WELCOME_AD, welcomeAdHash);
			def result = restClientService.sendUpdateAction("updateWelcomeAd", hashData);
			if(result != null){
				boxesSyncResult.addBoxResult(result);
			} else {
				syncFail = true;
			}
		}
		if(!syncFail){
			syncFail = !this.sendDefaultToBoxes( serialsToDefault, boxesSyncResult );
		}
		
		return (syncFail)? null : boxesSyncResult;
	} // END updateBoxesOfLocations
	
	/**
	 * Updates the welcome Ad on boxes to the default
	 * @param affectedLocations		Locations to be update
	 * @return						Result of the update to the boxes
	 */
	public BoxesSyncResult updateBoxesToDefaultOfLocations(affectedLocations){
		
		log.debug("--> Welcome Ad Update Boxes to default of Locations <--");
				
		List<String> serialsToDefault = new ArrayList();
				
		for(location in affectedLocations){
			for(box in location.boxes){
				serialsToDefault.add(box.serial);
			}
		}
				
		BoxesSyncResult boxesSyncResult = new BoxesSyncResult("Welcome Ads");
		boolean syncFail = !this.sendDefaultToBoxes( serialsToDefault, boxesSyncResult ); 
		
		return (syncFail)? null : boxesSyncResult;
	} // END updateBoxesOfLocations
	
	
	/**
	 * This method sends the default Welcome Ad to a list of boxes by serials
	 */
	private boolean sendDefaultToBoxes(List<String> serialsToDefault, BoxesSyncResult boxesSyncResult) {
		
		if(!serialsToDefault.isEmpty()){
			def result = null;
			String partner = "";
			HashMap<String, Object> defaultWelcomeAds = new HashMap<String, HashMap>();
			//AppSkin defaultSkin = null;
			List<String> serialsInWelcomeAds = new ArrayList();
			for(serial in serialsToDefault){
				partner = SerialNumberGeneratorHelper.getPartnerCodeFromSerial(serial);
				HashMap<String, Object> item = defaultWelcomeAds.get(partner);
				if(item == null){
					//If not exists, retrieve the skin and put to the list
					serialsInWelcomeAds = new ArrayList();
					serialsInWelcomeAds.add(serial);
					HashMap<String, Object> data = new HashMap<String, HashMap>();
					data.put("welcomeAdHash", getWelcomeAdHashMap(WelcomeAd.findByName('default_' + partner)));
					data.put("serials",serialsInWelcomeAds);
					defaultWelcomeAds.put(partner, data);
				} else {
					serialsInWelcomeAds = item.get('serials');
					serialsInWelcomeAds.add(serial);
					item.put("serials",serialsInWelcomeAds);
					defaultWelcomeAds.put(partner, item);
				}
			}
			
			HashMap<String, Object> hashData = new HashMap<String, WelcomeAd>();
			for(ds in defaultWelcomeAds){
				hashData = new HashMap<String, Object>();
				hashData.put("serials", ds.value.get('serials'));
				hashData.put(Constants.PARAM_WELCOME_AD, ds.value.get('welcomeAdHash'));
				
				result = restClientService.sendUpdateAction("updateWelcomeAd", hashData);
				if(result == null){
					return false;
				} else {
					boxesSyncResult.addBoxResult(result);
				}
			}
		}
		return true;
	}
		
	/**
	 * Creates the hash with the information of a Welcome Ad
	 * 
	 * @param appSkin	Welcome Ad with the information
	 * @return			Hash created
	 */
	private HashMap getWelcomeAdHashMap(WelcomeAd welcomeAd){
		HashMap welcomeAdHash = new HashMap();
		if(welcomeAd != null){
			welcomeAdHash.put(WelcomeAdService.TYPE, 
				(welcomeAd.welcomeType == WelcomeType.IMAGE)? 'image':'video');
			welcomeAdHash.put(WelcomeAdService.SMALL_IMAGE_URL, Utils.getStringValue(welcomeAd.smallImageUrl));
			welcomeAdHash.put(WelcomeAdService.MEDIUM_IMAGE_URL, Utils.getStringValue(welcomeAd.mediumImageUrl));
			welcomeAdHash.put(WelcomeAdService.LARGE_IMAGE_URL, Utils.getStringValue(welcomeAd.largeImageUrl));
			welcomeAdHash.put(WelcomeAdService.VIDEO_URL, Utils.getStringValue(welcomeAd.videoUrl));
			welcomeAdHash.put(WelcomeAdService.SKIP_ENABLE, welcomeAd.skipEnable);
			welcomeAdHash.put(WelcomeAdService.SKIP_TIME, welcomeAd.skipTime);
		} else {
			welcomeAdHash.put(WelcomeAdService.TYPE, "image");
			welcomeAdHash.put(WelcomeAdService.SMALL_IMAGE_URL, "");
			welcomeAdHash.put(WelcomeAdService.MEDIUM_IMAGE_URL, "");
			welcomeAdHash.put(WelcomeAdService.LARGE_IMAGE_URL, "");
			welcomeAdHash.put(WelcomeAdService.VIDEO_URL, "");
			welcomeAdHash.put(WelcomeAdService.SKIP_ENABLE, false);
			welcomeAdHash.put(WelcomeAdService.SKIP_TIME, 0);
		}
		
		return welcomeAdHash;
	}
	
	/**
	 * This method receives a serial number of a Box and returns the hash map
	 * with the welcome ad associated
	 *
	 * @param location		Location associated with the Box
	 * @param serial		Serial Number of the Box
	 * @return				HashMap with the Welcome Ad Information
	 */
	public HashMap getBoxWelcomeAdHashMap(CompanyLocation location, String serial){
		
		return getWelcomeAdHashMap(serial, location.welcomeAd);
		
	} // End of getBoxWelcomeAdHashMap method
	
	public HashMap getBoxWelcomeAdHashMapBySerial( String serial ) {
		
		Box box = Box.findBySerial(serial);
		
		if( box && box.location ) {
			return getWelcomeAdHashMap(serial, box.location.welcomeAd);
		} else {
			return getWelcomeAdHashMap(serial, null);
		}
		
	} // End of getBoxWelcomeAdHashMapBySerial method
	
	/**
	 * Return the HashMap with the WelcomeAd information
	 * 
	 * @param serial		Serial number of ExXtractor getting the information
	 * @param welcomeAd		WelcomeAd to map; could be null when unregister or unrelated
	 * @return				HashMap with Welcome Ad information
	 */
	private HashMap getWelcomeAdHashMap(String serial, WelcomeAd welcomeAd) {
		
		HashMap hashMap;
		if(welcomeAd == null){
			//Must find the default of the Box
			String partner = SerialNumberGeneratorHelper.getPartnerCodeFromSerial(serial);
			welcomeAd = WelcomeAd.findByName('default_' + partner);
			if(welcomeAd == null){
				//Must find the Exxo Default
				welcomeAd = WelcomeAd.findByName('default_' + PartnerEnum.getDefaultPartnerCode());
				if(welcomeAd == null){
					//There're not a default skin, will send harcoded information
					log.info("There is not a welcome ad associated with the box: " + serial + ", not even a default.");
				}
			}
		}
		hashMap = getWelcomeAdHashMap(welcomeAd);
		
		return hashMap;
	} // End of getWelcomeAdHashMap method
	
} // End of Class
