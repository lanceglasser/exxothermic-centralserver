package mycentralserver;

import java.util.HashMap.Entry;

import mycentralserver.box.Box;
import mycentralserver.box.BoxConfig;
import mycentralserver.box.BoxConfigValue;
import mycentralserver.box.software.BoxSoftware;
import mycentralserver.utils.beans.BoxConfigurationUpdates;

/**
 * Service with the related methods of the configuration options of the ExXtractors to be use from other services or
 * from controllers
 * 
 * @author sdiaz
 * @since 01/01/2015
 */
class BoxConfigService {

	/**
	 * Will return the assigned value of the configuration for the
	 * box; if the value is not assigned will return the default value.
	 * 
	 * @param box		 Box to review
	 * @param configCode Code of the configuration to get
	 * @return			 Value of the configuration for the Box or default
	 */
	public String getConfigValueOfBox(Box box, String configCode){
		BoxConfig boxConfig = BoxConfig.findByCode(configCode);
		if ( boxConfig ) {
			BoxConfigValue boxConfigValue = BoxConfigValue.findByBoxAndBoxConfig(box, boxConfig);
			if(boxConfigValue) {
				return boxConfigValue.value;
			} else {
				return boxConfig.defaultValue;
			}
		} else {
			return null;
		}	
	}
	
	/**
	 * Returns a Json with all the Box configs and their values for a Box
	 *
	 * @param box
	 *             Box to get configuration values
	 * @param boxConfigurationUpdates
	 *             Object with the required changes on the configurations
	 * @return String with json including all the configurations 
	 */
    public String getBoxJsonConfigValues(Box box, BoxConfigurationUpdates boxConfigurationUpdates) {
        def boxConfigs = BoxConfig.findAll();
        def boxConfigValues = BoxConfigValue.findAllByBox(box);
        String json = "";
        for (BoxConfig boxConfig: boxConfigs) {
            if (configAllowedForBox(boxConfig, box) && boxConfig.enableForUI) {
                String value = null;
                //If the config is on the updated configurations use that value
                if (boxConfigurationUpdates.configurationHasChanged(boxConfig.code)) {
                    value = boxConfigurationUpdates.getUpdatedConfigurationValue(boxConfig.code);
                } else { // Try to find the configuration on database
                    for (BoxConfigValue boxConfigValue: boxConfigValues) {
                        if (boxConfigValue.boxConfig.id == boxConfig.id) {
                            value = boxConfigValue.value;
                            break;
                        }
                    }
                }
                // If no saved or new value exists, we use the default of the configuration
                value = (value != null)? value:boxConfig.defaultValue;
                json += (json != "")? ",":"";
                json += boxConfig.jsonLabel + ':' + value;
            }
        }
        return json;
    }
	
	/**
	 * Returns a HashMap with all the Box configs and their values 
	 * for a Box
	 * 
	 * @param box	Box to get configuration values
	 * @return		HashMap with configs
	 */
    def getBoxConfigValues(Box box) {
		def boxConfigs = BoxConfig.findAll();
		def boxConfigValues = BoxConfigValue.findAllByBox(box);
		HashMap<Long, String> configsMap = new HashMap<Long, String>();
		boolean found = false;
		String value = "";
		for(BoxConfig boxConfig: boxConfigs){
			found = false;
			for(BoxConfigValue boxConfigValue: boxConfigValues){
				if(boxConfigValue.boxConfig.id == boxConfig.id){
					found = true;
					value = boxConfigValue.value;
					break;
				}
			}
			if(found){
				configsMap.put(boxConfig.id, value);
			} else {
				configsMap.put(boxConfig.id, boxConfig.defaultValue);
			}
		} // End of for boxConfigs
		
		return configsMap;
    } // getBoxConfigValues

    /**
     * Checks all the inputs from UI and verifies if the values for the allowed box configurations changes and update
     * the value on DataBase
     * 
     * @param box
     *          Box that is updating
     * @param params
     *          Request parameters from the UI
     * @return SaveBoxConfigurationsResponse object with the information of the result of the save process
     */
    public BoxConfigurationUpdates reviewBoxConfigurationChanges(Box box, params) {
        BoxConfigurationUpdates saveConfigurationsResponse = new BoxConfigurationUpdates();
        //Get all the configurations
        final def boxConfigs = BoxConfig.findAll();
        //Store the current configuration values to a Map
        final HashMap<Long, String> currentValuesMap = getBoxConfigValues(box);
        for (BoxConfig boxConfig: boxConfigs) {
            if (configAllowedForBox(boxConfig, box) && boxConfig.enableForUI) {
                //Only check for the configurations that are enable for the UI
                final String value = params['CFG-' + boxConfig.code];
                if (value != currentValuesMap.get(boxConfig.id)) {
                    saveConfigurationsResponse.addUpdatedConfiguration(boxConfig.code, value);
                }
            }
        }
        return saveConfigurationsResponse;
    }

    /**
     * Save to database all the updated configurations of a box
     * 
     * @param box
     *              Box to update the configurations
     * @param saveBoxConfigurationsResponse
     *              Object that contains all the list of updated configurations
     */
    public void saveUpdatedConfigurations(Box box, BoxConfigurationUpdates saveBoxConfigurationsResponse) {
        if (saveBoxConfigurationsResponse == null) { // If the object is null there is nothing to save
            return;
        }
        final Map<String, String> configurationsToUpdate = saveBoxConfigurationsResponse.getUpdatedConfigurations();
        println "saveUpdatedConfigurations: " + configurationsToUpdate;
        for (java.util.Map.Entry<String, String> entry : configurationsToUpdate) {
            final BoxConfig boxConfig = BoxConfig.findByCode(entry.getKey());
            final BoxConfigValue boxConfigValue = BoxConfigValue.findOrCreateWhere(box:box, boxConfig: boxConfig);
            boxConfigValue.value = entry.getValue();
            boxConfigValue.save();
        }
    }

    /**
     * Save the configuration received by code with the new value for a Box only when is different from the current one
     * @param box
     *          Box to save the configuration
     * @param configCode
     *          Code of the configuration to save
     * @param value
     *          Value for the configuration
     */
    public void saveBoxConfigurations(Box box, String configCode, String value) {
        //Get all the configurations
        final BoxConfig boxConfig = BoxConfig.findByCode(configCode);
        if(boxConfig){
            final BoxConfigValue boxConfigValue = BoxConfigValue.findOrCreateWhere(box:box, boxConfig: boxConfig);
            if(boxConfigValue.value == "" && boxConfig.defaultValue != value) {
                //Not exists and is different than the default
                boxConfigValue.value = value;
                boxConfigValue.save();
            } else {
                if(boxConfigValue.value != value) {
                    boxConfigValue.value = value;
                    boxConfigValue.save();
                }
            }
        }
    }

    /**
     * Verifies if the configuration is allowed for the box reviewing the minimum required version for the configuration
     * and the version of the box.
     * 
     * @param config
     *          Configuration to check
     * @param box
     *          Box to check
     * @return  True or False depending if the configuration is allowed for the Box or not
     */
    public boolean configAllowedForBox(BoxConfig config, Box box) {
        if(config.validBoxVersion) { // The configuration must has a valid box version
            BoxSoftware minimumAllowedVersion = BoxSoftware.findByVersionSoftwareLabel(config.validBoxVersion);
            if( minimumAllowedVersion && box.softwareVersion &&
                    minimumAllowedVersion.versionSoftware <= box.softwareVersion.versionSoftware ) {
                return true;
            }
        }
        return false;
    }
} // End of Class
