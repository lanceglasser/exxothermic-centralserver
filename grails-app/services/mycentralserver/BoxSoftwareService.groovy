package mycentralserver;

import org.apache.commons.lang.StringUtils;

import mycentralserver.box.software.BoxSoftware;
import mycentralserver.utils.SerialNumberGeneratorHelper;
import mycentralserver.utils.enumeration.Architecture;

/**
 * Service with functional methods for the Box Software domain
 * 
 * @author sdiaz
 * @since 01/18/2016
 */
class BoxSoftwareService {

    /**
     * Find the associated software version for a box using the serial number and the label of the version; from the
     * serial the method gets the architecture that will be use to find the software version
     * 
     * @param serial
     *              Serial number of the box
     * @param label
     *              Version label to find
     * @return Related BoxSoftware domain or null when the label is null or empty or it's not found
     */
    public BoxSoftware findByLabelAndArchitecture(final String serial, final String label) {
        if ( StringUtils.isBlank(label) ) {
            return null;
        }
        final Architecture architecture = SerialNumberGeneratorHelper.getArchitectureFromSerial(serial);
        def results = BoxSoftware.createCriteria().list {
            eq ('versionSoftwareLabel', label)
            like ('associatedArchitectures', "%[" + architecture.getValue() + "]%")
        }
        return (results && results.size() > 0)? results.get(0):null;
    }

    /**
     * Validates that the version label to save is valid for the architectures; this label must be unique per 
     * architecture, if already exists another one then is invalid
     * 
     * @param boxSoftware
     *                  If is updating, this is the old value
     * @param newLabel
     *                  New label to save
     * @param architectures
     *                  List of associated architectures to save
     * @return True when the label value is valid to save or False when it's already defined
     */
    public boolean isValidVersionLabelToSave(final BoxSoftware boxSoftware, final String newLabel, final String architectures) {
        if (boxSoftware && StringUtils.isNotBlank(boxSoftware.versionSoftwareLabel) 
            && boxSoftware.versionSoftwareLabel.equals(newLabel) 
            && boxSoftware.associatedArchitectures.equals(architectures) ) { 
            // Updating with same version and architectures
            return true;
        } else {
            final String[] architectureIndex = architectures.split(",");
            // For each of the architectures try to find a version with the same label
            for (index in architectureIndex) {
                def boxSoftList = BoxSoftware.createCriteria().list {
                    eq ('versionSoftwareLabel', newLabel)
                    like ('associatedArchitectures', "%" + index + "%")
                    if ( boxSoftware != null ) { // If it's updating not consider same record
                        ne ('id', boxSoftware.id)
                    }
                }
                if (boxSoftList.size() > 0) { // Exists another version with the same label for the architecture
                    return false;
                }
            }
            return true;
        }
    }
}