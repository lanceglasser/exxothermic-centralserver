package mycentralserver

import groovy.time.TimeCategory;

class UserServicesService {
	
	def grailsApplication

	/** For new accounts the password expiration date 
	 *  is the same day.this is to change the first login.
	 * @return
	 */
    def getPassswordExpirationDateForNewAccount() {
		return new Date();
    }
	
	/** this method return the password expiration date for 
	 *  account which is changing the password.
	 * @return
	 */
	def getPasswordExpirationDateForExistAccount() {
		def  days = grailsApplication.config.passwordValidDays
		def  passwordExpirationDate = null
		
		use(TimeCategory) {
			passwordExpirationDate = new Date() + days.days
		}
		
		return passwordExpirationDate
	}
}
