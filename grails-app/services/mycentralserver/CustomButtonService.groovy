package mycentralserver

import java.util.HashMap
import java.util.List

import mycentralserver.box.Box
import mycentralserver.company.CompanyLocation
import mycentralserver.custombuttons.CustomButton

import java.awt.image.BufferedImage

import org.apache.commons.io.FileUtils
import org.apache.commons.lang.ArrayUtils
import org.junit.After

import javax.imageio.ImageIO

import mycentralserver.utils.CloudFilesPublish
import mycentralserver.utils.FileHelper
import mycentralserver.utils.SchedulerUtils;
import mycentralserver.utils.Utils
import  mycentralserver.utils.Constants
import grails.validation.ValidationException
import mycentralserver.generaldomains.Scheduler

import java.util.Date
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.text.ParseException
import java.util.*

class CustomButtonService {

	RestClientService restClientService
	def grailsApplication

	def readCustomButtonById(Long id) {
		return CustomButton.read(id)
	}

	private def findCustomButtonEnables(customButtons) {
		return customButtons.findAll{it.enabled == true};
	}

	private def findCustomButtonEnables(CompanyLocation location) {
	   String timezone = (location.timezone != null) ? location.timezone : "UTC";
        
       DateFormat UtcFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
       DateFormat formatTimeZone = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	   SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd");
       formatTimeZone.setTimeZone(TimeZone.getTimeZone(timezone));
       Date currentDate = (Date) UtcFormat.parse(formatTimeZone.format(new Date())); // Get the current date by timeZone || default UTC.
        
       def customButtonEnabled = location.customButtons.findAll{it.enabled == true && it.schedule != null && (it.schedule.endDate == null || it.schedule.endDate >= currentDate)}
       ArrayList<CustomButton> validContent = new ArrayList<CustomButton>();
	   
	   List<String> dayOfWeek = Arrays.asList("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
       int today = dayOfWeek.indexOf(new SimpleDateFormat("EEEE").format(currentDate));

       for(CustomButton cb: customButtonEnabled){
          if (cb.schedule != null) { // Do I have a schedule?
			  
            String[] days = (cb.schedule.days != null && cb.schedule.days != '') ? cb.schedule.days.split(",") : [today.toString()];
            String[] hours = (cb.schedule.hours != null && cb.schedule.hours != '') ? cb.schedule.hours.split("-") : ["00","00"];
			
			if (hours[1] == "00") {
				hours[1] = "23:59:59";
			} else {
				hours[1] = (hours[0] == hours[1]) ? hours[1]+":59:59" : hours[1]+":00:00";
			}
			
            if (Arrays.asList(days).contains(today.toString())){ //Should I display the AD today?
				Date startDate = (Date) UtcFormat.parse(simpleFormat.format(currentDate) + " " + hours[0] + ":00:00");
				Date endDate = (Date) UtcFormat.parse(simpleFormat.format(currentDate) + " " + hours[1]);
            
                if (endDate.before(startDate)){ // Solve wrong date ranges
                    endDate.setTime(endDate.getTime() + 1*24*60*60*1000);
                }
        
                if (startDate.before(currentDate) && endDate.after(currentDate)){ //It's show time 
				 cb.schedule.startDate =  startDate;
				 cb.schedule.endDate   =  endDate;
				 validContent.add(cb);
				}
            }
          } else {
		   cb.schedule.startDate =  (Date) UtcFormat.parse(simpleFormat.format(currentDate) + " 00:00:00");
		   cb.schedule.endDate   = 	(Date) UtcFormat.parse(simpleFormat.format(currentDate) + " 23:59:59");
           validContent.add(cb);
          }
       }
	   
	   return validContent;
	 }
	
	
	private def findCustomButtonEnableAndConvertInHash(customButtons) {
		return findCustomButtonEnables(customButtons).collect {customButton ->
			return [ id: customButton.id,
				name: customButton.name,
				channelLabel: customButton.channelLabel,
				channelColor:(customButton.channelColor.replace("#", "")),
				channelContent:customButton.channelContent,
				imageURL:customButton.imageURL?customButton.imageURL: "",
				type:customButton.type
			]
		}
	}
	
	private def findCustomButtonEnableAndConvertInHash(CompanyLocation location) {     
       return findCustomButtonEnables(location).collect {customButton ->
            return [ id: customButton.id,
                name: customButton.name,
                channelLabel: customButton.channelLabel,
                channelColor:(customButton.channelColor.replace("#", "")),
                channelContent:customButton.channelContent,
                imageURL:customButton.imageURL?customButton.imageURL: "",
                type:customButton.type,
				starDate: SchedulerUtils.strDateToUnixTimestamp(Content.schedule.startDate),
              	endDate: SchedulerUtils.strDateToUnixTimestamp(Content.schedule.endDate)
            ]
        }   
    }

	public updateBoxCustomButtoms(Box box, CompanyLocation location){
		HashMap<String, ArrayList<CustomButton>> hashData = new HashMap<String, ArrayList<CustomButton>>()
		
		def listCustomButton = new ArrayList<CustomButton>()
		def customButtonEnables = findCustomButtonEnables(location);
		listCustomButton.addAll(customButtonEnables)
		hashData.put(box.serial, listCustomButton)

		List<HashMap> listCustomButtonToRequest = convertHashToHasJsonToConvertJsonUtil(hashData);
		return  restClientService.setCustomButtons(listCustomButtonToRequest)
	} // END updateBoxCustomButtoms

	/** the the hash with the format [serial:List<CustomButtons>]
	 * to List<Hash>where every hash has the format [ id:number, channelColor:"String", channelLabel:"String", channelContext:"String"]
	 *
	 * @param hash
	 * @return
	 */
	public convertHashToHasJsonToConvertJsonUtil(HashMap<String, List<CustomButton>> hash ) {
		List<CustomButton> listCustomButton;

		HashMap<HashMap> has = new HashMap<HashMap>()
		HashMap<String, Object> hasRequest = new HashMap<String, Object>()
		List<HashMap> listRequestSetCustomButtons = new ArrayList<HashMap>();

		for (String serialNumber : hash.keySet()) {

			hasRequest = new HashMap();
			listCustomButton = hash.get(serialNumber);
			def hashCustomButton = convertListCustomButtonToListWithHashCustomButtonFields( listCustomButton)
			hasRequest.put("serialNumber", serialNumber);
			hasRequest.put("customButtons", hashCustomButton);
			listRequestSetCustomButtons.add(hasRequest);
		}

		return listRequestSetCustomButtons;
	} // END convertHashToHasJsonToConvertJsonUtil

	private convertListCustomButtonToListWithHashCustomButtonFields(List<CustomButton> listCustomButton) {
		List<HashMap> listCustomButtons = new ArrayList<HashMap>()
		HashMap hasRequest

		for (CustomButton customButton : listCustomButton) {
			hasRequest = new HashMap();

			hasRequest.put("id", customButton.id);
			hasRequest.put("channelLabel", customButton.channelLabel)
			hasRequest.put("channelContent", customButton.channelContent)
			hasRequest.put("channelColor", customButton.channelColor.replace("#",""))
			hasRequest.put("type", customButton.type)

			if(customButton.imageURL != null){
				hasRequest.put("imageURL", customButton.imageURL)
			}else{
				hasRequest.put("imageURL", "")
			}

			listCustomButtons.add(hasRequest)
		}
		return listCustomButtons;
	}

	public String saveCustomButton(CustomButton customButton , tempPicture , String x, String y, String w, String h  ){
		def fileFullPath

		try{
			if(customButton.validate()) {
				def fileNameOrigin = tempPicture.getOriginalFilename()
				if (!tempPicture.isEmpty() && !fileNameOrigin.isEmpty()){
					//save file on server
					try{
						fileFullPath = saveImageFile(tempPicture, x, y ,w, h )
						//upload file to rackspace
						CloudFilesPublish cloudService = new CloudFilesPublish(fileFullPath, grailsApplication.config.rackspaceUser, grailsApplication.config.apiKey);
						String name = cloudService.uploadFileToRackspace()
						customButton.imageURL = name;
					}catch(e){
						String ext = FileHelper.getFileExtension(fileNameOrigin)
						if( !(ext.equals(Constants.PNG_SUFFIX)) & !(ext.equals(Constants.SUFFIX))){
							return 'customButtonImage.incorrect.image.format'
						}else{
							return 'customButtonImage.incorrect.error'
						}
					}
				}
				if(customButton.save()){
					return null
				}else{
					return 'default.error.problem'
				}
			}else{
				return 'default.error.problem'
			}
		}catch(e){
			e.printStackTrace()
			throw e
		}
	}

	public String updateCustomButton(CustomButton customButton , tempPicture , String x, String y, String w, String h  ){
		def fileFullPath
		String previousFile
		try{
			if(customButton.validate()) {
				def fileNameOrigin = tempPicture.getOriginalFilename()
				if (!tempPicture.isEmpty() && !fileNameOrigin.isEmpty()){
					// get previous file
					previousFile = customButton.imageURL

					//save file on server
					try{
						fileFullPath = saveImageFile(tempPicture, x, y ,w, h )
						//upload file to rackspace
						CloudFilesPublish cloudService = new CloudFilesPublish(fileFullPath, grailsApplication.config.rackspaceUser, grailsApplication.config.apiKey);
						String name = cloudService.uploadFileToRackspace()
						customButton.imageURL = name;

						//now delete previous file
						if(previousFile != null && !previousFile.isEmpty()){
							try{
								cloudService = new CloudFilesPublish(fileFullPath, grailsApplication.config.rackspaceUser, grailsApplication.config.apiKey);
								cloudService.deleteFileToRackspace(previousFile)
							} catch(e){ //ignore if we could not delete the file
								e.printStackTrace()
							}
						}
					}catch(e){
						String ext = FileHelper.getFileExtension(fileNameOrigin)
						if( !(ext.equals(Constants.PNG_SUFFIX)) & !(ext.equals(Constants.SUFFIX))){
							return 'customButtonImage.incorrect.image.format'
						}else{
							return 'customButtonImage.incorrect.error'
						}
					}
				}

				//see if we need to update a box
				if(customButton.save()){
			
					HashMap<String, String> customButtonsInfo = new HashMap<String, String>()
					def jsonTransactionStatus = null
					def affectedLocations = new ArrayList<CustomButton>()
					def locations = customButton.locations;
					
					if(customButton.schedule != null && customButton.schedule.id != null) {
						def mySchedule = Scheduler.read(customButton.schedule.id);
						mySchedule.save();
					}

					if(locations != null && !locations.isEmpty()) {
						jsonTransactionStatus = updateBoxesOfLocations(customButton.locations)
						customButtonsInfo = getCustomButtonNames(affectedLocations)
					}
					if(jsonTransactionStatus == null && (locations != null && !locations.isEmpty())) {
						return 'mycentralserver.custombuttons.syncErrorMessage'
					} else {
						return null
					}
				}else{
					return 'default.error.problem'
				}
			}else{
				return 'default.error.problem'
			}
		}catch(e){
			e.printStackTrace()
			throw e
		}
	}

	def getCustomButtonNames(affectedLocations){

		HashMap<String, String> customButtonsInfo = new HashMap<String, String>()

		for(location in affectedLocations){
			for(customButton in location.customButtons) {
				customButtonsInfo.put(customButton.id.toString(), customButton.name)
			}
		}

		return  customButtonsInfo
	}

	def updateBoxesOfLocations(affectedLocations){

		HashMap<String, ArrayList<CustomButton>> hashData = new HashMap<String, ArrayList<CustomButton>>()

		for(location in affectedLocations){
			for(box in location.boxes){
				def listCustomButton = new ArrayList<CustomButton>()
				def customButtonEnables = findCustomButtonEnables(location);
				listCustomButton.addAll(customButtonEnables)
				hashData.put(box.serial, listCustomButton)
			}
		}

		List<HashMap> listCustomButtonToRequest = convertHashToHasJsonToConvertJsonUtil(hashData);
		return  restClientService.setCustomButtons(listCustomButtonToRequest)
	} // END updateBoxesOfLocations

	private String saveImageFile(tempPicture, String x, String y, String w, String h  ) {
		def response = null
		try {

				def storagePath = grailsApplication.config.basePathCustomButtonTempFiles
				def fileNameOrigin = tempPicture.getOriginalFilename()
				def fullPath = Utils.getPathFromFile(storagePath, fileNameOrigin)

				//verify that directory exists
				File directory = new File(storagePath);
				if (!directory.exists()){
					directory.mkdirs();
				}

				//create temp file
				tempPicture.transferTo( new File(fullPath) )
				File tempFile = new File(fullPath)

				BufferedImage image = ImageIO.read(tempFile)
				BufferedImage croppedImage
				//Make Javascript validations here -- Safari
				String ext = FileHelper.getFileExtension(fileNameOrigin)
				if(tempPicture.getSize() > Constants.CUSTOM_BUTTON_MAX_SIZE){
					throw new Exception ("The file size is not allowed [MaxSize: " + Constants.CUSTOM_BUTTON_MAX_SIZE +
						", FileSize: " + tempPicture.getSize() + "]");
				}else if(!ArrayUtils.contains( Constants.CUSTOM_BUTTON_ALLOWED_EXTENSIONS, ext.toLowerCase() )){
					throw new Exception ("The file extension is not allowed: " + ext.toLowerCase());
				}

				if(x != null && y != null && w != null && h != null){
					def posx = Utils.parseIntTheValueDoubleAsString(x)
					def posy = Utils.parseIntTheValueDoubleAsString(y)
					def width = Utils.parseIntTheValueDoubleAsString(w)
					def height = Utils.parseIntTheValueDoubleAsString(h)
					def position_y = Utils.calculatePositonHeight(height,posy,image.height)
					def position_x = Utils.calculatePositonWidth(width,posx,image.width)

					//crop the image
					croppedImage = image.getSubimage(position_x, position_y , width , height )

				}else{
					croppedImage = image
				}

				storagePath = grailsApplication.config.basePathCustomButtonImagesFiles

				//verify final destination directory exists
				directory = new File(storagePath);
				if (!directory.exists()){
					directory.mkdirs();
				}

				//save cropped image
				fullPath = Utils.getPathFromFile(storagePath, fileNameOrigin )
				File croppedPicture = new File(fullPath)

				ImageIO.write(croppedImage, Constants.SUFFIX, croppedPicture);

				//delete temp file
				FileUtils.deleteQuietly(tempFile)

				return fullPath
			} catch(e)	{
				e.printStackTrace()
				throw e
			}
		return response
	} // END saveImageFile




} // END CustomButtonService
