/**
 * UserSessionService.groovy
 */
package mycentralserver;

import mycentralserver.company.CompanyLocation;
import mycentralserver.user.User;
import mycentralserver.app.Affiliate;
import mycentralserver.company.CompanyIntegrator;
import mycentralserver.company.Company;
import mycentralserver.utils.Constants;
import mycentralserver.utils.PartnerEnum;
import mycentralserver.utils.RoleEnum;
import mycentralserver.utils.Utils;

import javax.servlet.http.HttpSession;

import org.junit.After;
import org.springframework.web.context.request.RequestContextHolder;
/**
 * Service that handles the logic related with the user session in
 * terms of validations and associate objects of current user
 * 
 * @author Cecropia Solutions
 *
 */
class UserSessionService {

	/* Inject required services */
	def springSecurityService
	
	private HttpSession getSession() {
		return RequestContextHolder.currentRequestAttributes().getSession()
	}
	
	/**
	 * Get current logged user
	 *
	 * @return	Domain object of the User, or null when is not into the system
	 */
	public User getCurrentUser() {
		if(springSecurityService.principal){
			return springSecurityService.currentUser;
		} else {
			return null;
		}
	}
	
	/**
	 * Get current logged user email
	 *
	 * @return	Email of the current user
	 */
	public String getCurrentUserEmail() {
		getCurrentUser().email;
	}
	
	/**
	 * Returns the list of locations that the current user can see
	 *
	 * @return	List of locations; null when fails
	 */
	public def getPermitedLocations() {
		def locations = null;
		try {
			if( isAdminOrHelpDesk() ){
				locations = CompanyLocation.findAll(sort:Constants.NAME, order:Constants.SORT_ASC);
			} else {
				  locations = CompanyLocation.getAllLocations(getCurrentUser(), null).listDistinct();
				  locations = Utils.sortByName(locations);
			}
					
		} catch(Exception ex) {
			log.error("Error getting the list of locations for current user", ex);
		}
		return locations;
	}
	
	/**
	 * Determines if current user has HELP_DESK role
	 * 
	 * @return	True when the user has the role Help Desk associated
	 */
	private boolean isHelpDesk() {
		return userHasRole(RoleEnum.HELP_DESK.value);
	}
	
	/**
	 * Determines if current user has HELP_DESK role
	 * 
	 * @return	True when the user has the role Admin associated
	 */
	protected boolean isAdmin() {
		return userHasRole(RoleEnum.ADMIN.value);
	}
	
	/**
	 * Determines if current user has HELP_DESK or ADMIN role
	 * 
	 * @return True when at least 1 of the roles is associated
	 */
	protected boolean isAdminOrHelpDesk(){
		return (userHasRole(RoleEnum.ADMIN.value) || userHasRole(RoleEnum.HELP_DESK.value));
	}
	
    /**
     * Determines if current user are only OWNER or OWNER_STAFF
     *
     * @return True if the user is only Owner or Owner Staff
     */
    public boolean isOwnerOrOwnerStaffOnly(){
        return (!userHasRole(RoleEnum.ADMIN.value) && !userHasRole(RoleEnum.HELP_DESK.value)
                && !userHasRole(RoleEnum.INTEGRATOR.value) && !userHasRole(RoleEnum.INTEGRATOR_STAFF.value));
    }

	/**
	 * This method checks if the current user has a Role associated
	 * 
	 * @param roleToCheck	Role code to find for the User
	 * 
	 * @return				True when it's associated
	 * 
	 */
	public boolean userHasRole(String roleToCheck) {
		
		def roles = springSecurityService.getPrincipal().getAuthorities();
		
		// Go over the list of associated roles and return true if is found
		for(role in roles) {
			if(role != null && role.equals(roleToCheck)) {
				return true;
			}
		}
		
		//Return false when the role was not found
		return false;
	}
	
	/**
	 * This method will return the current Affiliate according
	 * to the session and the request url; if the session dont have a
	 * associate Affiliate will set the Default by Code and return that one
	 *
	 * @return	Affiliate
	 */
	protected getCurrentSessionAffiliate(){
		if(getSession().affiliate == null){
			getSession().affiliate = Affiliate.findByCode(PartnerEnum.getDefaultPartnerCode());
		}
		return getSession().affiliate;
	}
	
	public getCompaniesForUser() {
		def user = getCurrentUser()
		def companyList = null
		
		if (isHelpDesk()){
			companyList = Company.findAll(sort:"name", order:"asc")
		} else {
			
			def temp = Company.getEnabledCompanies(user)
			companyList = temp.listDistinct(sort:"name", order:"asc").collect()
			def integrators = temp.typeIntegrator.list(sort:"name", order:"asc")
			
			for(i in integrators){
					CompanyIntegrator integrator = CompanyIntegrator.findByCompany(i)
					companyList.addAll(integrator.assignedCompanies.toList())
			}
		}
		return companyList.unique()
	} // End of getCompaniesForUser method
	
	
	public getCompaniesForAssociation(){
		getCompaniesForAssociation(false);
	}
	
	/**
	 * This method will return the list of associated companies depending
	 * of current user role; the companies need to be enable and has at least
	 * 1 enable location
	 * 
	 * @return Companies List
	 */
	public getCompaniesForAssociation(boolean ommitOwnerRole){
		User user = getCurrentUser();
		def companyList = null;
		
		if (isHelpDesk()){
			companyList = Company.findAll(sort:"name", order:"asc")
		} else {
			def temp = null;
			
			if( userHasRole(RoleEnum.INTEGRATOR.value) ||
				userHasRole(RoleEnum.INTEGRATOR_STAFF.value) ||
				userHasRole(RoleEnum.ADMIN.value) ||
				ommitOwnerRole ){
				
				temp = Company.getEnabledCompanies(user);
				
			} else {
				// Owner, OwnerStaff For this roles only enable to associate with premium companies
				temp = Company.getEnabledAndPremiumCompanies(user);
			}
			
			companyList = temp.listDistinct(sort:"name", order:"asc").collect();
			
			def integrators = temp.typeIntegrator.list(sort:"name", order:"asc");
			
			for(i in integrators){
					CompanyIntegrator integrator = CompanyIntegrator.findByCompany(i);
					companyList.addAll(integrator.assignedCompanies.toList());
			}
		}
		
		companyList = companyList.unique();
		
		int enabledLocationsCounter = 0
		for (Iterator<Company> iter = companyList.iterator(); iter.hasNext();) {
			Company c = iter.next();

			if (!c.enable || c.locations == null || c.locations.size() ==0){
				iter.remove()
			} else {
				enabledLocationsCounter = 0
				for(location in c.locations){
					if(location.enable){
						enabledLocationsCounter++;
					}
				}
				if(enabledLocationsCounter == 0) {
					iter.remove()
				}
			}
		}
		
		return companyList;
	} // End of getCompaniesForAssociation method
	
	/**
	 * This method will return the list of venue servers
	 * that the current user is allow to see
	 * @return
	 */
	public getUserAllowBoxes() {
		def locations = getPermitedLocations();
		if(locations != null && locations.size() == 0) {
			return null;
		} else {
			def locationsWithBoxes = locations.findAll{it.boxes.size() != 0};
			def boxes = locationsWithBoxes.collect(){it*.boxes}.flatten();
			if(boxes != null && boxes.size() > 0) {
				boxes = Utils.sortListBySerialOnly(boxes);
				return boxes;
			} else {
				return null;
			}
		}
	}
	
	/**
	 * This method will return the list of venue with servers
	 * that the current user is allow to see
	 * @return
	 */
	public getUserLocationsWithAllowBoxes() {
		def locations = getPermitedLocations();
		if(locations != null && locations.size() == 0) {
			return null;
		} else {
			def locationsWithBoxes = locations.findAll{it.boxes.size() != 0};
			return locationsWithBoxes;
		}
	}
	
} // End of Class
