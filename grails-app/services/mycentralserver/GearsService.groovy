/**
 * GearsService.groovy
 */
package mycentralserver

import grails.plugins.rest.client.RestResponse;
import mycentralserver.GearsController.ErrorCode
import mycentralserver.GearsController.Status
import mycentralserver.box.Box
import mycentralserver.box.BoxUnregistered
import mycentralserver.utils.Constants

/**
 * Service use by Gears Controller with most of the logic for their
 * requests.
 *
 * @author Cecropia Solutions
 *
 */
class GearsService {

	/* Required Services Injection */
	def boxMetricsService;
	def appSkinService;
	def welcomeAdService;
	def boxUserService;
	def documentService;
	def contentService;
	def boxConfigService;
	def configurationService;
	def deviceService;
	
	public static enum CommRequestAction {
		PROCESS_METRICS_UPLOAD("ProcessMetricsUpload"),
		GET_APP_INFO("GetAppInfo"),
		GET_WELCOME_AD("GetWelcomeAd"),
		GET_BOX_USERS("GetBoxUsers"),
		GET_DOCUMENTS("GetDocuments"),
		GET_CONTENTS("GetContents"),
		GET_OFFERS("GetOffers"),
		SAVE_USER_INFO("SaveUserInfo"),
		GET_CONFIGS("GetConfigs"),
		UPDATE_CONFIGURATION("UpdateConfiguration"),
		GET_DEVICES("GetDevices");
		
		private String value;
		
		private CommRequestAction(String value) {
			this.value = value;
		}
		
		public String toString(){
			return this.value;
		}
	}
	
	/**
	 * Returns the Json Object with received values
	 *
	 * @param jsonMap		JSonMap where to store the information
	 * @param isSuccessful	True or False depending of the operation result
	 * @param description	Most use when error for information
	 * @param error			True or False depending of the operation result
	 * @return				JSonMap updated
	 */
	protected HashMap<String, Object> setJsonMapResult(jsonMap, isSuccessful,  description, error ) {
		jsonMap.results = isSuccessful
		jsonMap.status  =  isSuccessful ? Status.STATUS_SUCCESSFUL.getStatus() : Status.STATUS_FAILED.getStatus()
		jsonMap.statusDescription = description
		jsonMap.error   = error
		return jsonMap;
	}
	
	/**
	 * This method does some general validations of the request
	 * related with the Box; return null when fail the validations or
	 * the Box object when is OK.
	 *
	 * @param serial	Serial number of the ExXtractor
	 * @param jsonMap	map where to set some information in case of error
	 * @return			Object domain when OK, null when not
	 */
	protected Box generalValidations(String serial, HashMap jsonMap){
		Box box = Box.findBySerial(serial);
		BoxUnregistered boxUnregistered = null;
		jsonMap.serialNumber = serial;
		if( !box ) {
			boxUnregistered = BoxUnregistered.findBySerial(serial);
			String statusDescription = "Box " + serial + ((!boxUnregistered)? " not found":" unregistered");
			jsonMap = setJsonMapResult(jsonMap, false,  statusDescription, ErrorCode.NO_DEVICE_FOUND.getErrorCode() )
			return null;
		} else {
			if(!box.location) {
				jsonMap = setJsonMapResult(jsonMap, false, "Box " + serial + " hasn't location", ErrorCode.GENERAL_EXCEPTION.getErrorCode() )
				return null;
			} else {
				return box;
			}
		}
	}
	
	/**
	 * General method that process some request from the Communication component,
	 * receives the serial number of the ExXtractor, the action to execute and the required
	 * parameters as a HashMap
	 *
	 * @param action		Action to execute
	 * @param serial		Serial number of the ExXtractor
	 * @param paramsMap		Map with required parameters
	 * @return				HashMap with the response to the request
	 * @throws Exception	When something crassh
	 */
	public HashMap<String, Object> processRequestFromComm(GearsService.CommRequestAction action, String serial, HashMap<String, Object> paramsMap) throws Exception {
		
		HashMap jsonMap = new HashMap();
		
		if(action == GearsService.CommRequestAction.GET_APP_INFO ) {
			jsonMap.serialNumber = serial;
			jsonMap.put(Constants.PARAM_SKIN, appSkinService.getBoxAppSettingsHashMapBySerial(serial));
			// Set the result information, in this case is success
			jsonMap = setJsonMapResult(jsonMap, true, serial + " " + action + " OK", ErrorCode.NO_ERROR.getErrorCode() );
		} else if (action == GearsService.CommRequestAction.GET_WELCOME_AD) {
			jsonMap.serialNumber = serial;
			jsonMap.put(Constants.PARAM_WELCOME_AD, welcomeAdService.getBoxWelcomeAdHashMapBySerial(serial));
			// Set the result information, in this case is success
			jsonMap = setJsonMapResult(jsonMap, true, serial + " " + action + " OK", ErrorCode.NO_ERROR.getErrorCode() );
		} else {
			// Applies general validations
			Box box = this.generalValidations(serial, jsonMap);
			
			if( box ) {
				// When the box exists and is ok must process the action
				switch(action){
					case GearsService.CommRequestAction.UPDATE_CONFIGURATION:
						boxConfigService.saveBoxConfigurations(box, paramsMap.get(Constants.PARAM_CODE), 
							paramsMap.get(Constants.PARAM_VALUE));
						break;
					case GearsService.CommRequestAction.PROCESS_METRICS_UPLOAD:
						boxMetricsService.processUploadNotification( box, paramsMap.get(Constants.PARAM_UPLOAD_RESULT) );
						break;
					/*case GearsService.CommRequestAction.GET_APP_INFO:
						jsonMap.put(Constants.PARAM_SKIN, appSkinService.getBoxAppSettingsHashMap(box.location, serial));
						break;
					case GearsService.CommRequestAction.GET_WELCOME_AD:
						jsonMap.put(Constants.PARAM_WELCOME_AD, welcomeAdService.getBoxWelcomeAdHashMap(box.location, serial));
						break;*/
					case GearsService.CommRequestAction.GET_DOCUMENTS:
						def listOfDocuments = documentService.findContentEnableAndConvertInHash(box.location);
						jsonMap.put(Constants.PARAM_DOCUMENTS, listOfDocuments);
						break;
					case GearsService.CommRequestAction.GET_BOX_USERS:
						def listOfButtons = boxUserService.getBoxUsersHashMap(box.location, serial);
						jsonMap.put(Constants.PARAM_LAST_UPDATE, (box.location.lastBoxUsersUpdate.getTime()/1000L));
						jsonMap.put(Constants.PARAM_SERIAL_NUMBER, serial);
						jsonMap.put(Constants.PARAM_USERS, listOfButtons);
						break;
					case GearsService.CommRequestAction.GET_CONTENTS:
						def listOfContents = contentService.findContentEnableAndConvertInHash(box.location, box);
						jsonMap.put(Constants.PARAM_CUSTOM_BUTTONS, listOfContents);
						break;
					case GearsService.CommRequestAction.GET_OFFERS:
						def listOfOffers = contentService.findOffersEnables(box.location);
						jsonMap.put(Constants.PARAM_OFFERS, listOfOffers);
						break;
					case GearsService.CommRequestAction.SAVE_USER_INFO:
						def saveResult = boxUserService.processUserActionFromBox(box.location, serial,
							paramsMap.get(Constants.PARAM_USERNAME), paramsMap.get(Constants.PARAM_PASSWORD),
							paramsMap.get(Constants.PARAM_LEVEL), paramsMap.get(Constants.PARAM_ACTION),
							paramsMap.get(Constants.PARAM_LAST_UPDATE) );
						jsonMap.put("result", saveResult);
						break;
					case GearsService.CommRequestAction.GET_CONFIGS:					
						jsonMap.put("isPABox", box.isPa());
						jsonMap.put("config", boxConfigService.getBoxJsonConfigValues(box));
						configurationService.sendConfigurationsWhenBoxConnect(serial);
						break;
					case GearsService.CommRequestAction.GET_DEVICES:
						jsonMap.put(Constants.PARAM_DEVICES, deviceService.getDevicesWithSmallerPackagesHashMap());
						break;
					default:
						break;
				}
				
				// Set the result information, in this case is success
				jsonMap = setJsonMapResult(jsonMap, true, serial + " " + action + " OK", ErrorCode.NO_ERROR.getErrorCode() );
			}
		}
		
		return jsonMap;
		
	} // End of processRequestFromComm method

	/**
	 * Receive the request from the Controller; applies general validations and redirect to specific service
	 *
	 * @param serial		Serial number of the ExXtractor executing the action
	 * @param uploadResult	Result of the upload process
	 * @return				JsonMap with result infomation
	 * @throws Exception	When something is wrong here or specific service
	 */
	public HashMap<String, Object> processMetricsUploadNotification(String serial, boolean uploadResult) throws Exception {

		HashMap jsonMap = new HashMap();

		// Applies general validations
		Box box = this.generalValidations(serial, jsonMap);
		
		if( box ) {
			// When the box exists and is ok must process the notification
			boxMetricsService.processUploadNotification( box, uploadResult );
			
			// Set the result information, in this case is success
			jsonMap = setJsonMapResult(jsonMap, true, serial + " processMetricsUploadNotification OK", ErrorCode.NO_ERROR.getErrorCode() );
		}
		
		return jsonMap;
	} // End of processMetricsUploadNotification method

    /**
     * Validates the response from the Communication Api
     * 
     * @param resp
     *              Response of the Api
     * @return True if the response is on expected class and with a status of 200
     */
    public boolean checkStatusSuccessful(resp) {
        if(resp && resp instanceof RestResponse) {
            return resp.getResponseEntity().statusCode.value == 200;
        }
        return false;
    }

    /**
     * Checks if the response body is successful or not
     * 
     * @param resp
     *              Response of Central server
     * @return True if certificate is valid and blacklist validation is ok, false if not
     */
    public boolean checkCertificateValidation(resp) {
        if (resp instanceof RestResponse) {
            def responseEntity = resp.getResponseEntity();
            def body = responseEntity.body;
            if (body.equals(Boolean.TRUE.toString())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Sets received variables to the response Map
     * 
     * @param jsonMap
     *              Response Map where to set the variables
     * @param isSuccessful
     *              Status of the request
     * @param description
     *              Description of the result of the request
     * @param error
     *              Indicates if there was an error or no
     * @return Same received Map with the new variables
     */
    public setResultInJsonMap(jsonMap, isSuccessful, String description, ErrorCode error ) {
        jsonMap.results = isSuccessful;
        jsonMap.status = isSuccessful ? Status.STATUS_SUCCESSFUL.getStatus() : Status.STATUS_FAILED.getStatus();
        jsonMap.statusDescription = description;
        jsonMap.error = error.getErrorCode();
        return jsonMap;
    }
}