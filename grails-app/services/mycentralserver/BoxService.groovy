/**
 * BoxService.groovy
 */
package mycentralserver;

import mycentralserver.GearsController.ErrorCode;
import mycentralserver.app.Affiliate;
import mycentralserver.box.Box;
import mycentralserver.box.software.BoxSoftware;
import mycentralserver.box.BoxBlackList;
import mycentralserver.box.BoxCertificate;
import mycentralserver.box.BoxEncryption;
import mycentralserver.box.BoxIp;
import mycentralserver.box.BoxChannel;
import mycentralserver.box.BoxManufacturer;
import mycentralserver.box.BoxNotification;
import mycentralserver.box.BoxStatus;
import mycentralserver.box.BoxUnregistered;
import mycentralserver.company.CompanyLocation;
import mycentralserver.custombuttons.CustomButton;
import mycentralserver.user.User;
import mycentralserver.utils.BoxStatusEnum;
import mycentralserver.utils.BoxVersionsEnum;
import mycentralserver.utils.CloudFilesPublish;
import mycentralserver.utils.Constants;
import mycentralserver.utils.FileHelper
import mycentralserver.utils.PartnerEnum;
import mycentralserver.utils.SerialNumberGeneratorHelper;
import mycentralserver.utils.Utils;
import mycentralserver.utils.beans.BoxConfigurationUpdates;
import mycentralserver.utils.enumeration.Architecture;
import mycentralserver.utils.enumeration.AudioCardGeneration;
import mycentralserver.utils.enumeration.AudioInputMode;
import mycentralserver.utils.enumeration.BoxConfigurationCode;
import mycentralserver.utils.enumeration.BoxRequestActionsEnum;
import mycentralserver.utils.enumeration.JumperConfiguration;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.codehaus.groovy.grails.web.json.*;
import org.junit.After;
import org.springframework.context.i18n.LocaleContextHolder;

import java.awt.Color;
import java.awt.GraphicsConfiguration.DefaultBufferCapabilities;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import grails.converters.JSON;
import grails.validation.ValidationException;
import groovy.sql.Sql;

/**
 * Service class with most of the Logic related with the ExXtractors
 * 
 * @author Cecropia Solutions
 */
class BoxService {
	
	/* Inject required services */
	def restClientService;
	def grailsApplication;
	def dataSource;
	def userSessionService;
	def messageErrorService;
	def messageSource;
	def boxConfigService;
    def rackspaceService;
    def boxSoftwareService;
    def gearsService;
	
	/**
	 * Return the list of ExXtractors for the current
	 * user depending of role and the parameters
	 *
	 * @return	[locations, totalCount, filteredCount]; null when error
	 */
	public def getExxtractorsOfUser(HashMap<String, Object> queryParams) {
			
		try {
			def exxtractors = new ArrayList();
			int totalCount = 0;
			int filteredCount = 0;
			
			if(userSessionService.isAdminOrHelpDesk()) {
				totalCount = Box.listAllExxtractorsForUITable(0, 0, queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), "", null, queryParams.get(Constants.COMPANY_ID), queryParams.get(Constants.LOCATION_ID), true).list()[0];
				filteredCount = Box.listAllExxtractorsForUITable(0, 0, queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH), null, queryParams.get(Constants.COMPANY_ID), queryParams.get(Constants.LOCATION_ID), true).list()[0];
				exxtractors = Box.listAllExxtractorsForUITable(
					queryParams.get(Constants.DATA_TABLES_START), queryParams.get(Constants.DATA_TABLES_LENGTH) ,
					queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME) ,
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH), null, queryParams.get(Constants.COMPANY_ID), queryParams.get(Constants.LOCATION_ID), false).list();
			} else {
				User user = userSessionService.getCurrentUser();				
				totalCount = Box.listAllExxtractorsForUITable(0, 0, queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), "", user, queryParams.get(Constants.COMPANY_ID), queryParams.get(Constants.LOCATION_ID), true).list()[0];
				filteredCount = Box.listAllExxtractorsForUITable(0, 0, queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH), user, queryParams.get(Constants.COMPANY_ID), queryParams.get(Constants.LOCATION_ID), true).list()[0];
				exxtractors = Box.listAllExxtractorsForUITable(
					queryParams.get(Constants.DATA_TABLES_START), queryParams.get(Constants.DATA_TABLES_LENGTH) , queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME) ,
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH), user, queryParams.get(Constants.COMPANY_ID), queryParams.get(Constants.LOCATION_ID), false).list();
			}
			
			return [exxtractors, totalCount, filteredCount]
		} catch(Exception e) {
			log.error("Error getting the list of exxtractors of the user", e);
			return null;
		} // End of catch
	} // End of getExxtractorsOfUser
	
	
	
	/**
	 * Return the list of ExXtractors for the current
	 * user depending of role and the parameters
	 *
	 * @return	[locations, totalCount, filteredCount]; null when error
	 */
	public def getExxtractorsStatusInfo(HashMap<String, Object> queryParams) {
			
		try {
			
			int offline_needs_upgrade, offline_upgraded,
				online_upgraded, online_needs_upgrade, outdated, unversioned;
			BoxSoftware lastVersionOS;
			BoxSoftware unsupportedVersion;
			//lastVersionOS = BoxSoftware.
			unsupportedVersion = BoxSoftware.findByVersionSoftwareLabel(Constants.MIN_SUPPORTED_VERSION);
			
			def boxSoftwareCriteria = BoxSoftware.createCriteria()
			lastVersionOS = boxSoftwareCriteria.list{
				or{
					eq("affiliate",userSessionService.getCurrentSessionAffiliate())
					isNull("affiliate")
				}
				eq("enable", true)
				eq("isSupported", true)
				maxResults(1)
				order("id","desc") //assuming auto increment just to make sure
			}[0]
			
			if(userSessionService.isAdminOrHelpDesk()) {				
				offline_needs_upgrade 	= Box.boxStatusData(unsupportedVersion,null, queryParams.get(Constants.COMPANY_ID), queryParams.get(Constants.LOCATION_ID),Constants.OFFLINE_NEEDS_UPGRADE, lastVersionOS).list()[0];
				offline_upgraded 		= Box.boxStatusData(unsupportedVersion,null, queryParams.get(Constants.COMPANY_ID), queryParams.get(Constants.LOCATION_ID),Constants.OFFLINE_UPGRADED, lastVersionOS).list()[0];
				online_upgraded		 	= Box.boxStatusData(unsupportedVersion,null, queryParams.get(Constants.COMPANY_ID), queryParams.get(Constants.LOCATION_ID),Constants.ONLINE_UPGRADED, lastVersionOS).list()[0];
				
				online_needs_upgrade 	= Box.boxStatusData(unsupportedVersion,null, queryParams.get(Constants.COMPANY_ID), queryParams.get(Constants.LOCATION_ID),Constants.ONLINE_NEEDS_UPGRADE, lastVersionOS).list()[0];
				outdated 				= Box.boxStatusData(unsupportedVersion,null, queryParams.get(Constants.COMPANY_ID), queryParams.get(Constants.LOCATION_ID),Constants.OUTDATED, lastVersionOS).list()[0];
				unversioned				= Box.boxStatusData(unsupportedVersion,null, queryParams.get(Constants.COMPANY_ID), queryParams.get(Constants.LOCATION_ID),Constants.UNVERSIONED, lastVersionOS).list()[0];
			} else {
				User user = userSessionService.getCurrentUser();
				offline_needs_upgrade 	= Box.boxStatusData(unsupportedVersion,user, queryParams.get(Constants.COMPANY_ID), queryParams.get(Constants.LOCATION_ID),Constants.OFFLINE_NEEDS_UPGRADE, lastVersionOS).list()[0];
				offline_upgraded 		= Box.boxStatusData(unsupportedVersion,user, queryParams.get(Constants.COMPANY_ID), queryParams.get(Constants.LOCATION_ID),Constants.OFFLINE_UPGRADED, lastVersionOS).list()[0];
				online_upgraded		 	= Box.boxStatusData(unsupportedVersion,user, queryParams.get(Constants.COMPANY_ID), queryParams.get(Constants.LOCATION_ID),Constants.ONLINE_UPGRADED, lastVersionOS).list()[0];
				online_needs_upgrade 	= Box.boxStatusData(unsupportedVersion,user, queryParams.get(Constants.COMPANY_ID), queryParams.get(Constants.LOCATION_ID),Constants.ONLINE_NEEDS_UPGRADE, lastVersionOS).list()[0];
				outdated 				= Box.boxStatusData(unsupportedVersion,user, queryParams.get(Constants.COMPANY_ID), queryParams.get(Constants.LOCATION_ID),Constants.OUTDATED, lastVersionOS).list()[0];
				unversioned 			= Box.boxStatusData(unsupportedVersion,user, queryParams.get(Constants.COMPANY_ID), queryParams.get(Constants.LOCATION_ID),Constants.UNVERSIONED, lastVersionOS).list()[0];
			}
			
			return [offline_needs_upgrade, offline_upgraded, online_upgraded, online_needs_upgrade, outdated, unversioned]
			
		} catch(Exception e) {
			log.error("Error getting the status data of exxtractors of the user", e);
			return null;
		} // End of catch
	} // End of getExxtractorsOfUser
	
	/** Add the new channel From MyBox into the DB
	 * 
	 * @param box
	 * @param channelStatusJson
	 * @return
	 */
	private def addNewChannelFromMyBox(Box box, def channelStatusJson,boolean updateExisting ) {
			try{
				def itChanneslStatusJson = channelStatusJson.iterator()
				
				// Add the new channels from MyBox
				while(itChanneslStatusJson.hasNext()) {
					def info = itChanneslStatusJson.next()
					def channelNum = info.channelNum
					
					if(!existChannelNumberIntoBoxChannels(channelNum, box )) {
						 BoxChannel channel = new BoxChannel()
						channel.setChannelNumber(info.channelNum)
						channel.setChannelLabel(info.channelLabel)
						channel.setCodec(info.codecParms)
						channel.setRmsVoltage(info.rmsVoltage)
						channel.setIsPa(info.pachannel)
						box.addToChannels(channel)
						log.info( "[**] SerialNumber: " + box.serial + " Adding  channel : " + channel.getChannelNumber())
					} // END if(!existChannelNumberIntoBoxChannels(channelNume, box ))
					else{
						if(updateExisting){
							BoxChannel channel = BoxChannel.findByBoxAndChannelNumber(box, channelNum)
							channel.setChannelLabel(info.channelLabel)					
							channel.save(flush: true, failOnError: true);
							log.info( "[**] SerialNumber: " + box.serial + " Updating  channel : " + channel.getChannelNumber())
						}
						//lets update the channel information
					}
				} // END while(itChanneslStatusJson.hasNext())	
			}catch(e){
				e.printStackTrace()
				throw e
		}
	} // END addNewChannelFromMyBox

	
	/** Verify if the channelNumber exists in the Box's Channels
	 * 
	 * @param channelNumber int
	 * @param box			domain
	 * @return
	 */
	private def existChannelNumberIntoBoxChannels(int channelNumber,Box box ) {
		def channels = box.channels
		
		if(channels) {
			def itChannels = channels.iterator()
			def channel
			
			if(itChannels) {
				while(itChannels.hasNext()) {
					channel = itChannels.next()
					
					if(channelNumber == channel.getChannelNumber()) {
						return true
					}
				} // END while(itChannels.hasNext())
			} // END if(itChannels)
		} // END if(channels)
		
		return false
		
	} // END existChannelNumberIntoBoxChannels	
	
	public Boolean removeBlackListedBox(long id) {
		try{	
				def box = BoxBlackList.get(id);
				def boxIpsList;
			
					if (!box) {
						return false;
					} else {
						def lastBoxIp = BoxIp.withCriteria{ // gets the most recent ip to dont delete it with the rest of the list
							eq('serial',box.serial)
							maxResults( 1 )
							order( 'dateCreated', 'desc' )
						}
						boxIpsList = BoxIp.findAllBySerial(box.serial); // get the list of all ip
						box.delete(flush: true, failOnError: true);
						
						for (boxIp in boxIpsList){
							if(lastBoxIp[0].id != boxIp.id) // deletes the ip only if is not the most recent
							{
								boxIp.delete(flush: true, failOnError: true);
							}
						}
						return true;
					}
		}catch(e){
			e.printStackTrace()
			throw e
		}
		
	} // END removeBlackListedBox
	
	def findBoxBySerialNumber(String serialNumber) {
		return Box.findBySerial(serialNumber)
	} // END getBoxBySerialNumber	
	
	def getChannelStatus(SerialNumber) {
		
	}

    /**
     * Handles the response from the Box with the channels information that also includes the current configurations
     * of PA and debug level
     * 
     * @param serialNumber
     *              Serial number of the box that retrieves the channels information
     * @param jsonResponse
     *              Response from the box with the channels and others information
     * @return Array with 3 objects [listChannels, isPA, debugLevel]
     */
    def updateChannels(String serialNumber, Object jsonResponseChannelStatus) {
        def listChannels = new ArrayList();
        final Box box = findBoxBySerialNumber(serialNumber);
        boolean isPA = false;
        int debugLevel = 4;
        if (box) {
            if (jsonResponseChannelStatus == null) {
                jsonResponseChannelStatus = restClientService.getChannelStatus(serialNumber);
            }
            // Add o Delete the Channel
            if (jsonResponseChannelStatus) {
                if (jsonResponseChannelStatus?.pa) {
                    isPA =  jsonResponseChannelStatus?.pa
                }
                if(jsonResponseChannelStatus?.debugLevel){
                    debugLevel = jsonResponseChannelStatus?.debugLevel;
                }
                def channelStatusJson = jsonResponseChannelStatus?.channelsStatus;
                if (channelStatusJson) {
                    def itChanneslStatusJson = channelStatusJson.iterator();
                    // Add the new channels from MyBox
                    while (itChanneslStatusJson.hasNext()) {
                        def info = itChanneslStatusJson.next();
                        BoxChannel channel = new BoxChannel();
                        channel.setChannelNumber(info.channelNum);
                        channel.setChannelLabel(info.channelLabel);
                        channel.setCodec(info.codecParms);
                        channel.setRmsVoltage(info.rmsVoltage);
                        channel.setIsPa(info.pachannel);
                        channel.setGain(info.gain);
                        channel.setDelay(info.delay? info.delay:0);
                        channel.setEnabledForApp(info.enable);
                        if (info.port) {
                            channel.setChannelPort(info.port);
                        }
                        if(info.description){
                            channel.setDescription(info.description);
                        }
                        listChannels.add(channel);
                    }
                } else {
                    log.error " updateChannels- Json Response from Communication is empty for serial " + serialNumber;
                }
            }
        }
        return [listChannels, isPA, debugLevel]
    }
			
	def boolean isBoxPA(String serialNumber) {
		
		def isPA = false;
		def	jsonResponseChannelStatus = restClientService.getChannelStatus(serialNumber);
		
		// Add o Delete the Channel
		if(jsonResponseChannelStatus) {		
			if(jsonResponseChannelStatus?.pa != null){
				isPA =  jsonResponseChannelStatus?.pa
			}								
		}  
		 
		return isPA
	} // END isBoxPA
	
	def setAllBoxChannelInMyBox(String serialNumber) {
		def box = findBoxBySerialNumber(serialNumber)
		def channels = box.channels
		def jsonResponse
		
		if(channels) {		
			def jsonString	
			for(def channel: channels) {
				jsonResponse = restClientService.setChannelStatus(serialNumber, channel.getChannelNumber(), channel.getChannelLabel(), 
					channel.getIsPa(), channel.getCodec(), channel.description, channel.imageURL, channel.largeImageURL, 
					channel.name, channel.subTitle, channel.gain, channel.enabledForApp);
				
				jsonString = (!jsonResponse)?"--":jsonResponse.toString()
				log.info "SetAllBoxChannelInMyBox serialNumber: " + serialNumber + " channelNumber: " + channel.getChannelNumber() + " jsonResponse: " + jsonString
			} // END for(def channel: channels)
		} // END if(channels)
	} // END setAllBoxChannelInMyBox
	
	def updateChannelsFromUsingJsonResponse(String serialNumber, def jsonResponseChannelStatus, boolean updateExisting) {
		try{
			def box = findBoxBySerialNumber(serialNumber)
			
			if(box) {			
							
				// Add o Delete the Channel
				if(jsonResponseChannelStatus) {
					def channelStatusJson = jsonResponseChannelStatus?.channelsStatus
					
					if(channelStatusJson) {
						addNewChannelFromMyBox(box, channelStatusJson, updateExisting)
						removeChannelFromDbBecauseDontExitInMyBox(box, channelStatusJson)
						box.save(flush: true, failOnError: true);
					} else {
						log.error " updateChannels- Json Response from Communication is empty serial " + serialNumber
					}
				} // END if(jsonResponseChannelStatus)	
			}
		}catch(e){
			e.printStackTrace()
			throw e
		}
	} // END updateChannels
	
	
	def getBoxChannels(String serialNumber){
		ArrayList channels = new ArrayList()
		try{
			def jsonResponseChannelStatus = restClientService.getChannelStatus(serialNumber);
			if (jsonResponseChannelStatus?.channelsStatus != null){
				
				jsonResponseChannelStatus = jsonResponseChannelStatus.channelsStatus
				
				def itChanneslStatusJson = jsonResponseChannelStatus.iterator();
				def channelNumberMyBox
				def info
				
				while(itChanneslStatusJson.hasNext()) {
					info = itChanneslStatusJson.next()			
					channels.add(info.channelNum)			
				} // END while(itChanneslStatusJson.hasNext())
			}
		}catch(Exception e){
			log.error("Could not retrieve channels:" + e);
		}		
		return channels;
	}
	
	public def returnJSon(listChannels) {
		return listChannels.collect {channel ->
			return [	 
				channelNum: 	 channel.channelNumber,
				channelLabel: 	 Utils.stringToUnicode(channel.channelLabel),
				codecParms: 	 channel.codec,
				currentSessions: channel.currentSessions,
				rmsVoltage: 	 channel.rmsVoltage,
				isPAChannel: 	 channel.isPa,
				imageURL:		 channel.imageURL?channel.imageURL: "",
				largeImageURL:	 channel.largeImageURL?channel.largeImageURL: "",
				description:	 channel.description?channel.description: "",
				name:			 channel.name?channel.name: "",
				subTitle:		 channel.subTitle?channel.subTitle: "",
				gain:			 channel.gain,
				enable:			 channel.enabledForApp
			]
		}
	}
	
	def updateDBChannels(Box box, List currentChannels, boolean isPA){
		updateDBChannels(box, currentChannels, isPA, -1);
	}
	
    /**
     * Receives the information of the channels and others from the bug and synchronizes on data base
     * 
     * @param box
     *              Box to update the information
     * @param currentChannels
     *              List of current channels on the box
     * @param isPA
     *              Configuration value of the box for the PA option
     * @param debugLevel
     *              Current debug level value on the box
     */
    def updateDBChannels(Box box, List currentChannels, boolean isPA, int debugLevel) {
        BoxChannel.withTransaction { status ->
            try {
                def boxDBChannels = box.channels.asList().sort{it.channelNumber};
                for (currentCh in currentChannels) { // For each channel from the box
                    if (boxDBChannels == null || boxDBChannels.size() == 0) { // DB is empty, add them all
                        currentCh.box = box;
                        currentCh.save(flush: true, failOnError: true);
                    } else {
                        if (currentCh.channelPort != null) { // Valid channels must has a port, ignore others
                            // Find the channel by port on the current list on data base
                            BoxChannel dbChannel = boxDBChannels.find{it.channelPort == currentCh.channelPort};
                            if (dbChannel) { // We found a channel with current port, update info
                                // Update channel on database
                                dbChannel.channelNumber = currentCh.channelNumber;
                                dbChannel.channelLabel = StringEscapeUtils.unescapeJava(currentCh.channelLabel);
                                dbChannel.isPa = currentCh.isPa;
                                dbChannel.enabled = true;
                                dbChannel.description = StringEscapeUtils.unescapeJava(currentCh.description);
                                dbChannel.gain = currentCh.gain;
                                dbChannel.enabledForApp = currentCh.enabledForApp;
                                dbChannel.delay = currentCh.delay;
                                dbChannel.save();
                                // Put the Id to avoid the delete later
                                currentCh.id = dbChannel.id;
                                currentCh.largeImageURL = dbChannel.largeImageURL;
                                currentCh.imageURL = dbChannel.imageURL;
                            } else { // The channel by port is not on data base, must be add it
                                currentCh.box = box;
                                currentCh.save(flush: true, failOnError: true);
                            }
                        }
                    }
                }
                // After updating the DB list, disable the channels that are not in the current list of channels
                boxDBChannels.each { dbChannel ->
                    if(!currentChannels.any { it.id == (dbChannel.id) }){
                        dbChannel.enabled = false;
                        dbChannel.save();
                    }
                }

                // If the debug level is part of the information update the value of the configuration
                if(debugLevel >= 0) {
                    boxConfigService.saveBoxConfigurations(box, BoxConfigurationCode.DEBUG_LEVEL.getCode(), ""+debugLevel);
                }

                // Set the PA mode of the Box updating others if this is the current PA on the venue
                setPAValue(box, isPA);
            } catch(Exception e) {
                log.error(e);
                status.setRollbackOnly();
            }
        }
    }

	public String saveChannelImage(BoxChannel channel , tempPicture , String x, String y, String w, String h  ){
		def fileFullPath
		String previousFile
		try{
			if(channel.validate()) {
				def fileNameOrigin = tempPicture.getOriginalFilename()
				if (!tempPicture.isEmpty() && !fileNameOrigin.isEmpty()){
					//save file on server
					try{
						
						previousFile = channel.imageURL
						
						fileFullPath = saveImageFile(tempPicture, x, y ,w, h, Constants.CHANNEL_IMAGE_SMALL_WIDTH, Constants.CHANNEL_IMAGE_SMALL_HEIGHT )
						//upload file to rackspace
						CloudFilesPublish cloudService = new CloudFilesPublish(fileFullPath, grailsApplication.config.rackspaceUser, grailsApplication.config.apiKey);
						String name = cloudService.uploadFileToRackspace()
						channel.imageURL = name;
						
						
						//now delete previous file
						if(previousFile != null && !previousFile.isEmpty()){
							try{
								cloudService = new CloudFilesPublish(fileFullPath, grailsApplication.config.rackspaceUser, grailsApplication.config.apiKey);
								cloudService.deleteFileToRackspace(previousFile)
							} catch(e){ //ignore if we could not delete the file
								e.printStackTrace()
							}
						}
					
					}catch(e){
						return 'image.incorrect.error'
					}
				}
				if(channel.save(flush: true, failOnError: true)){
					return null
				}else{
					return 'default.error.problem'
				}
			}else{
				throw new ValidationException("CustomButton is not valid", channel.errors)
			}
		}catch(e){
			e.printStackTrace()
			throw e
		}
	}
	
	public String saveChannelLargeImage(BoxChannel channel , tempPicture , String x, String y, String w, String h  ){
		def fileFullPath
		String previousFile
		try{
			if(channel.validate()) {
				def fileNameOrigin = tempPicture.getOriginalFilename()
				if (!tempPicture.isEmpty() && !fileNameOrigin.isEmpty()){
					//save file on server
					try{
						
						previousFile = channel.largeImageURL
						
						fileFullPath = saveImageFile(tempPicture, x, y ,w, h, Constants.CHANNEL_IMAGE_LARGE_WIDTH, Constants.CHANNEL_IMAGE_LARGE_HEIGHT )
						//upload file to rackspace
						CloudFilesPublish cloudService = new CloudFilesPublish(fileFullPath, grailsApplication.config.rackspaceUser, grailsApplication.config.apiKey);
						String name = cloudService.uploadFileToRackspace()
						channel.largeImageURL = name;
						
						
						//now delete previous file
						if(previousFile != null && !previousFile.isEmpty()){
							try{
								cloudService = new CloudFilesPublish(fileFullPath, grailsApplication.config.rackspaceUser, grailsApplication.config.apiKey);
								cloudService.deleteFileToRackspace(previousFile)
							} catch(e){ //ignore if we could not delete the file
								e.printStackTrace()
							}
						}
					}catch(e){
						return 'image.incorrect.error'
					}
				}
				if(channel.save(flush: true, failOnError: true)){
					return null
				}else{
					return 'default.error.problem'
				}
			}else{
				throw new ValidationException("CustomButton is not valid", channel.errors)
			}
		}catch(e){
			e.printStackTrace()
			throw e
		}
	}

	private String saveImageFile(tempPicture, String x, String y, String w, String h, int requiredWidth, int requiredHeight  ) {
		def response = null
		try {
								
				def storagePath = grailsApplication.config.basePathChannelTempFiles
				def fileNameOrigin = tempPicture.getOriginalFilename()
				def fullPath = Utils.getPathFromFile(storagePath, fileNameOrigin)
				
				//verify that directory exists
				File directory = new File(storagePath);
				if (!directory.exists()){
					directory.mkdirs();
				}
				
				//create temp file
				tempPicture.transferTo( new File(fullPath) )
				File tempFile = new File(fullPath)
				
				BufferedImage image = ImageIO.read(tempFile)
				BufferedImage croppedImage
				//Make Javascript validations here -- Safari
				int maxSizeAllowed = Constants.IMAGE_TO_CROP_MAX_SIZE;
				String ext = FileHelper.getFileExtension(fileNameOrigin);
				if(tempPicture.getSize() > maxSizeAllowed){
					FileUtils.deleteQuietly(tempFile);
					throw new Exception ("The file size is not allowed [MaxSize: " +
						mycentralserver.utils.Utils.getFileHumanSize(maxSizeAllowed) +
						", FileSize: " + mycentralserver.utils.Utils.getFileHumanSize(tempPicture.getSize()) + "]");
				}else if(!ArrayUtils.contains( Constants.CUSTOM_BUTTON_ALLOWED_EXTENSIONS, ext.toLowerCase() )){
					FileUtils.deleteQuietly(tempFile);
					throw new Exception ("The file extension is not allowed: " + ext.toLowerCase());
				}
				 
				if(x != null && y != null && w != null && h != null &&
					x != "" && y != "" && w != "" && h != ""){
					def posx = Utils.parseIntTheValueDoubleAsString(x);
					def posy = Utils.parseIntTheValueDoubleAsString(y);
					def width = Utils.parseIntTheValueDoubleAsString(w);
					def height = Utils.parseIntTheValueDoubleAsString(h);
					def position_y = Utils.calculatePositonHeight(height,posy,image.height);
					def position_x = Utils.calculatePositonWidth(width,posx,image.width);
						 
					//crop the image
					croppedImage = image.getSubimage(position_x, position_y , width , height );
				}else{
					croppedImage = image
				}
								
				storagePath = grailsApplication.config.basePathChannelImagesFiles
				
				//verify final destination directory exists
				directory = new File(storagePath);
				if (!directory.exists()){
					directory.mkdirs();
				}
				
				//save cropped image
				fullPath = Utils.getPathFromFile(storagePath, fileNameOrigin )
				File croppedPicture = new File(fullPath);
								
				/**************** // converts the image to jpg
				BufferedImage imageRGB = null;
				imageRGB = new BufferedImage(croppedImage.getWidth(),
					croppedImage.getHeight(), BufferedImage.TYPE_INT_RGB);
				
				// write data into an RGB buffered image, no transparency
				imageRGB.setData(croppedImage.getData());

				/********************/
				//ImageIO.write(g, "jpg", croppedPicture);// ext.toLowerCase()
				ImageIO.write(croppedImage, ext.toLowerCase(), croppedPicture);
				
				if(!validateImageDimensions(croppedPicture, requiredWidth, requiredHeight)){
					FileUtils.deleteQuietly(tempFile);
					FileUtils.deleteQuietly(croppedPicture);
					throw new Exception ("Image Dimensions are wrong: " + requiredWidth + ", " + requiredHeight);
				}
				
				//delete temp file
				FileUtils.deleteQuietly(tempFile);
				
				return fullPath
			} catch(e)	{
				e.printStackTrace()
				throw e
			}
		return response
	} // END saveImageFile

	public boolean validateImageDimensions(file, int w, int h){
		BufferedImage bimg = ImageIO.read(file);
		int width          = bimg.getWidth();
		int height         = bimg.getHeight();
		//println "Image Dimensiones [" + width + ":" + height + "], Required Dimensions: [" + w + "," + h + "] -:> " + (width == w && height == h);
		return (width == w && height == h);
	}
	
	public def setBoxConnected(Box box){
		try{	
				def status= BoxStatus.findByDescription(BoxStatusEnum.CONNECTED.value)
				box.lastUpdated = new Date()
				box.status = status
				box.save(flush: true, failOnError: true)
			}catch(e){
			e.printStackTrace()
			throw e
		}
	}	
	
	public BoxManufacturer changeBoxSerialFromLegacy(String legacySerial, String newSerial){
		BoxManufacturer box = BoxManufacturer.findBySerialNumber(legacySerial);
		if(box != null){
			box.serialNumber = newSerial;
			box.legacySerialNumber = legacySerial;
			Sql sql = new Sql(dataSource)
			def sqlCall = "call update_box_serial_from_legacy(:legacySerial, :newSerial)";
			final paramMap = [legacySerial: legacySerial, newSerial: newSerial];
			log.info "Running ${sqlCall} with params ${paramMap}"
			try{
				sql.execute(paramMap, sqlCall);
			}catch(Exception e){
				box = null; 
				log.warnEnabled "Could not execute ${sqlCall} with params ${paramMap}: ${e.getMessage()}", e;
			}
		}
		return box;
	}
	
	public void removePAFromBoxesByLocation(Box box){
		if(box.location != null){
			Box.executeUpdate("update Box b set b.pa = false where b.location = " + box.location.id + " and b.pa = true and b.id <> " + box.id);
		}
	}

    /**
     * Sets the PA option for a box executing raw queries to avoid synchronization 
     * 
     * @param box
     *          Box to update the value
     * @param isPa
     *          New value of the PA option
     */
    public void setPAValue(Box box, boolean isPa) {
        // Set the value of the box
        Box.executeUpdate("update Box b set b.pa = " + isPa + " where b.id = " + box.id);
        if ( isPa ) { // Set option to false for all others boxes on the Venue
            if(box.location != null){
                Box.executeUpdate("update Box b set b.pa = false where b.location = " + 
                    box.location.id + " and b.pa = true and b.id <> " + box.id);
            }
        }
    }

    /**
     * Executes a raw update to database to avoid the auto update of the last modification property
     * 
     * @param box
     *              Box to update
     * @param name
     *              Name to set to the box
     * @param PA
     *              PA value to set
     * @param audioCardGen
     *              Audio Card Generation value to set
     * @param jumperConfig
     *              Jumper configuration value to set
     * @return True when success
     */
    public boolean updateBasicBoxInfo(Box box, String name, boolean PA, String audioCardGen, String jumperConfig) {
        // For first generation always set jumper configuration to None
        if (audioCardGen.equals(AudioCardGeneration.FIRST.getValue().toString())) {
            jumperConfig = JumperConfiguration.NONE.getValue();
        } 
		String sql = "update Box b set b.name = '" + name + "', b.pa="+ PA;
        sql += ", b.audioCardGeneration = " + audioCardGen + ", b.jumperConfiguration=" + jumperConfig;
        sql += " where b.id = " + box.id;
		Box.executeUpdate(sql);
		return true;
	}
	
	/**
	 * This method will send the request to the Box to be reset
	 * 
	 * @param serialNumber	Serial Number of the ExXtractor to be reset
	 * @return				True or False depending of the  
	 */
	public boolean sendRemoteResetRequest(String serialNumber) throws Exception {
		
		try {
			def responseRest = restClientService.remoteReset(serialNumber);
			
			if(responseRest != null && responseRest.successful) {
				updateBoxStatus(serialNumber);
				return true;
			} else {
				if(responseRest != null){
					def messageErrorDescriptionHash  = messageErrorService.getMessageErrorDescription();
					throw new Exception(messageSource.getMessage(messageErrorDescriptionHash.get(responseRest.errorCode), null, null));
				} else {
					return false;
				}
			}
		} catch(Exception e) {
			throw e;
		}		
	} // End of sendRemoteResetRequest
	
	public boolean sendRestoreDataRequest(String serialNumber){

		def responseRest = restClientService.restoreData(serialNumber);

		if(responseRest != null && responseRest.successful) {
			return true;
		} else {
			return false;
	    }
	}
	
    /**
     * Executes the registration process of a ExXtractor including several validations and send the remote reset command
     * to the ExXtractor in order to force the update of the information
     * 
     * @param boxToRegister
     *                  Box object that will be register
     * @param includeDomainErrors
     *                  Determines if the response should include the detail of the errors if required
     * @return Result of the registration process
     */
    public String registerBox(Box boxToRegister, boolean includeDomainErrors) {
        return registerBox(boxToRegister, includeDomainErrors, true);
    }

    /**
     * Executes the registration process of a ExXtractor including several validations
     *
     * @param boxToRegister
     *                  Box object that will be register
     * @param includeDomainErrors
     *                  Determines if the response should include the detail of the errors if required
     * @param mustSendRemoteReset
     *                  If True then a request for a reset is send to the ExXtractor
     * @return Result of the registration process
     */
    public String registerBox(Box boxToRegister, boolean includeDomainErrors, boolean mustSendRemoteReset) {
        String errorMsg = "";
        try {
            final String serial = boxToRegister.serial;
            // put this so validation from status wont fail
            BoxStatus status = BoxStatus.findByDescription(BoxStatusEnum.DISCONNECTED.value);
            boxToRegister.status = status;
            boxToRegister.serial = "AVOID_SERIAL_VALIDATION";
            if (boxToRegister.validate()) {
                boxToRegister.serial = serial;
                final Box box = Box.findBySerial(serial);
                if (box != null) { // Already exists the box; just change the venue
                    box.location = boxToRegister.location;
                    box.save();
                } else {
                    BoxUnregistered boxUnregisteredInstance = BoxUnregistered.findBySerial(serial);
                    BoxManufacturer boxManu = BoxManufacturer.findBySerialNumber(serial);
                    if (boxManu) { //The box must exists at the Manufacturer table
                        if (boxUnregisteredInstance) {
                            // assign software version is exists
                            def softVersion = boxUnregisteredInstance.versionSoftwareLabel;
                            if (softVersion) {
                                final BoxSoftware boxSoftware =
                                    boxSoftwareService.findByLabelAndArchitecture(serial, softVersion);
                                if (boxSoftware) {
                                    boxToRegister.softwareVersion = boxSoftware;
                                }
                            }
                            boxToRegister.certificate = this.getCertificate(boxUnregisteredInstance);
                            boxToRegister.hasCertificate = (boxToRegister.certificate)? true: false;
                            if (boxToRegister.validate()) {
                                boxToRegister.pa = false;
                                boxToRegister.imageType = boxManu.architecture;
                                boxToRegister.audioCardGeneration = boxManu.audioCardGeneration;
                                boxToRegister.jumperConfiguration = boxManu.jumperConfiguration;
                                Box.withTransaction { boxStatus ->
                                    try {
                                        boxToRegister.save();
                                        boxUnregisteredInstance.delete();
                                        AudioInputMode audioInputMode = AudioInputMode.codeOf(boxManu.audioInputMode);
                                        if (audioInputMode == null) {
                                            // Set default AudioInputMode configuration depending or architecture
                                            final Architecture architecture =
                                                    SerialNumberGeneratorHelper.getArchitectureFromSerial(serial);
                                            audioInputMode = AudioInputMode.getDefaultByArchitecture(architecture);
                                        }
                                        boxConfigService.saveBoxConfigurations(boxToRegister,
                                            BoxConfigurationCode.AUDIO_INPUT_MODE.getCode(),
                                            audioInputMode.getCode());
                                    } catch(Exception dbEx) {
                                        boxStatus.setRollbackOnly();
                                        log.error("Error saving or reseting the ExXtractor: " + dbEx.getMessage(), dbEx);
                                        errorMsg = dbEx.getMessage();
                                    }
                                }
                            } else { //Validation errors
                                errorMsg = messageSource.getMessage('default.box.errorRegistered', null, null);
                                errorMsg += getRegistrationDomainErrors(boxToRegister, includeDomainErrors);
                            }
                        } else {
                            errorMsg = messageSource.getMessage('default.box.error.turnon', null, null);
                        }
                    } else {
                        errorMsg = messageSource.getMessage('box.not.manufacturer', null, null);
                    }
                }
                if (mustSendRemoteReset) {
                    sendRemoteResetRequest(boxToRegister.serial);
                }
            } else { // Validation error
                errorMsg = messageSource.getMessage('default.box.errorRegistered', null, null);
                errorMsg += getRegistrationDomainErrors(boxToRegister, includeDomainErrors);
            }
        } catch( Exception ex ) {
            log.error("Error registering box", ex);
            errorMsg = (errorMsg == "")? ex.getMessage():errorMsg;
        }
        return errorMsg;
    }

    /**
     * Returns a string with the detailed errors from a domain object
     * 
     * @param box
     *          Domain object with the errors
     * @param includeDomainErrors
     *          Boolean that tell if 
     * @return String with the list of errors in HTML
     */
    private String getRegistrationDomainErrors(Box box, boolean includeDomainErrors) {
        StringBuilder strBuilder = new StringBuilder(100);
        if (includeDomainErrors) {
            strBuilder.append('<ul>');
            box.errors.allErrors.each {
                strBuilder.append('<li');
                if (it in org.springframework.validation.FieldError) {
                    strBuilder.append(' data-field-id="').append(it.field).append('">');
                }
                strBuilder.append(messageSource.getMessage(it, null)).append('</li>');
            }
            strBuilder.append('</ul>');
        }
        return strBuilder.toString();
    }
	/**
	 * This method will return the Certificate record from BoxCertificate using
	 * the certificateLabel of the BoxUnregistered record
	 * 
	 * @param boxUnregistered	BoxUnregistered record to check the certificate
	 * @return					Certificate record, null when not exists.
	 */
	public BoxCertificate getCertificate(BoxUnregistered boxUnregistered) {
		String certificateLabel = boxUnregistered.certificateLabel;
		BoxCertificate certificate = null;

		if(certificateLabel) {
			certificate = BoxCertificate.findByLabel(certificateLabel);
			if(certificate) {
				return certificate;
			} else {
				log.warn(" Serial: ["+ boxUnregistered.serial +"] , the CertificationLabel: ["+ certificateLabel +"] doesn't exist" )
			}
		} else {
			log.warn(" Serial: ["+ boxUnregistered.serial +"] , the CertificationLabel: [EMPTY]" )
		}
		return null;
	} // End of getCertificate method

    /**
     * This method will try to send the update of the available box information
     * 
     * @param box
     *          Box to send the configuration values
     * @param pa
     *          Is the Box set as PA or not
     * @param flash
     *          Flash object to include messages with the result
     * @param boxCOnfigurationUpdates
     *          Object with the required changes on the configurations
     * @return Error message of the call or null is no message was sent or no error produced
     */
    public String sendBoxInformationUpdate(Box box, boolean pa, flash, BoxConfigurationUpdates boxConfigurationUpdates){
        final String jsonStr = boxConfigService.getBoxJsonConfigValues(box, boxConfigurationUpdates);
        if (jsonStr.size() > 0) {
            def resultOfSetPA = restClientService.setMyBoxInfo(box.serial, pa, jsonStr);
            if (resultOfSetPA == null) {
                java.lang.Object[] args = new java.lang.Object[1];
                args[0] = box.name != null ? box.name : box.serial;
                flash.warn = messageSource.getMessage('venue.server.sync.when.connect', null, LocaleContextHolder.locale);
            } else {
                resultOfSetPA = JSON.parse(resultOfSetPA);
                if (!resultOfSetPA.successful) {
                    java.lang.Object[] args = new java.lang.Object[1];
                    args[0] = box.name != null ? box.name : box.serial;
                    return messageSource.getMessage('default.boxes.notupdate.pa', args, LocaleContextHolder.locale);
                }
            }
        }
        return null;
    }
	
	/**
	 * This method will try to send the update of the basic box information
	 *
	 * @param box
	 * @param pa
	 * @return
	 */
	public sendBoxPA(Box box, boolean pa, flash ){

		def resultOfSetPA = restClientService.setMyBoxPa(box.serial, pa);

		if(null == resultOfSetPA) {
			java.lang.Object[] args = new java.lang.Object[1];
			args[0] = box.name != null ? box.name : box.serial;
			flash.warn = messageSource.getMessage('venue.server.sync.when.connect', null, null);
			//return messageSource.getMessage('default.boxes.notconnectupdate.pa', args, null);
		} else {
			resultOfSetPA = JSON.parse(resultOfSetPA)
		
			if(!resultOfSetPA.successful) {
				java.lang.Object[] args = new java.lang.Object[1];
				args[0] = box.name != null ? box.name : box.serial;
				return messageSource.getMessage('default.boxes.notupdate.pa', args, null);
			}
		}
		return null
	}// End of sendBoxInformationUpdate
	
	/**
	 * This method will return the list of available servers
	 * of the current user that are not at the black list
	 * @return
	 */
	public def getNotBlackListedBoxes() {
		
		def locations = null;
		
		locations = userSessionService.isAdminOrHelpDesk()? CompanyLocation.findAll():
			CompanyLocation.getAllLocations(userSessionService.getCurrentUser(), null).listDistinct();
		
		if(locations != null && locations.size() == 0) {
			return null;
		} else {

			def locationsWithBoxes = locations.findAll{it.boxes.size() != 0};
			def boxes = locationsWithBoxes.collect(){it*.boxes}.flatten();
			
			def blackListedBoxes = BoxBlackList.findAll();
			
			try {
				boxes = boxes.findAll{!blackListedBoxes*.serial.contains(it.serial)};
			}catch(Exception e){
				e.printStackTrace()
			}
			
			if(boxes != null && boxes.size() > 0) {
				boxes = Utils.sortBoxBySerial(boxes);
				return boxes;
			}else{
				return null;
			}
		}
	}
	
	public boolean updateBoxStatus(String serialNumber){
		BoxStatus status = BoxStatus.findByDescription(BoxStatusEnum.DISCONNECTED.value)
		String sql = "update Box b set b.status = " + status.id;
			sql += " where b.serial = '" + serialNumber + "'";
		Box.executeUpdate(sql);
		return true;
	}
    
    /**
     * This method will run the operations for the unregister process considering the version of the software of
     * the box
     * 
     * @param box
     *      Box that is been unregistered
     * @return True or False depending if everything was ok or not
     */
    public boolean unregister(Box box) {
        //Delete all related objects
        boolean result = false;
        final String serial = box.serial;
        final BoxSoftware softwareVersion = box.softwareVersion;
        Box.withTransaction {status ->
            try {
                def boxBlackList = BoxBlackList.findBySerial(box.serial);
                def boxIps = BoxIp.findAllBySerial(box.serial);
                def boxNotifications = BoxNotification.findAllByBox(box);
                def boxEncryption = BoxEncryption.findBySerial(box.serial);
                if(boxEncryption) {
                    boxEncryption.delete();
                }
                if(boxBlackList){
                    boxBlackList.delete();
                }
                if (boxIps) {
                    for (BoxIp ip : boxIps) {
                        ip.delete();
                    }
                }
                if (boxNotifications) {
                    for (BoxNotification notification : boxNotifications) {
                        notification.delete();
                    }
                }
                // Save channels images urls; if the process completes without error then the images will be deleted
                // from the storage
                def deleteWhenSuccess = [];
                for (BoxChannel channel : box.channels) {
                    if( Utils.isNotEmptyString(channel.largeImageURL) ) {
                        deleteWhenSuccess.add(channel.largeImageURL);
                    }
                    if( Utils.isNotEmptyString(channel.imageURL) ) {
                        deleteWhenSuccess.add(channel.imageURL);
                    }
                }
                box.delete();
                //The process was completed; delete channel images from storage
                rackspaceService.deleteMultipleFiles(deleteWhenSuccess);
            } catch(Exception e) {
                log.error(e);
                status.setRollbackOnly();
                return false;
            }
        }

        if(softwareVersion && softwareVersion.versionSoftware >= BoxVersionsEnum.V2_38.getVersion()) {
            // If the box version is equals or higher than 2.38 must send the unregister request
            restClientService.sendActionRequestToServer(serial, BoxRequestActionsEnum.UNREGISTER.getAction());
        } else {
            // If the box version is under 2.38 must only send the reset request to the box
            restClientService.remoteReset(serial);
        }
        return true;
    }

    /**
     * Delete all the channels of the box and delete all the associated images from the storage
     * 
     * @param box
     *              Box to delete the channels information
     */
    public void cleanChannelsInformation(Box box) {
        try {
            // Save channels images urls; if the process completes without error then the images will be deleted
            // from the storage
            def deleteWhenSuccess = [];
            for (BoxChannel channel : box.channels) {
                if( Utils.isNotEmptyString(channel.largeImageURL) ) {
                    deleteWhenSuccess.add(channel.largeImageURL);
                }
                if( Utils.isNotEmptyString(channel.imageURL) ) {
                    deleteWhenSuccess.add(channel.imageURL);
                }
            }
            box.channels.clear();
            box.save();
            // Delete channel images from storage
            rackspaceService.deleteMultipleFiles(deleteWhenSuccess);
        } catch(Exception e) {
            log.error(e);
        }
    }

    /**
     * Validates that a box can connect to the system using the serial number and the certificate information
     * 
     * @param serial
     *              Serial number of the box to validate
     * @param certificate
     *              Certificate value that will be use for the validation
     * @param certVersion
     *              Label version of the certificate
     * @return Map with the result of the validation
     */
    public Map<String, Object> validateBoxConnectionBySerial(final String serial, final String certificate, 
            final String certVersion) {
        Map<String, Object> jsonMap = new HashMap<String, Object>();
        if (StringUtils.isBlank(serial) || StringUtils.isBlank(certificate) || StringUtils.isBlank(certVersion)) {
            jsonMap.success = false;
            jsonMap = gearsService.setResultInJsonMap(jsonMap, false,
                messageSource.getMessage('api.box.validation.error.invalid.parameters', null, null), ErrorCode.NO_DEVICE_FOUND);        
            return jsonMap;
        }
        final BoxManufacturer boxManufacturer = BoxManufacturer.findBySerialNumber(serial);
        if (boxManufacturer) {
            final String certPath = boxManufacturer.certificateLocation;
            final BoxBlackList boxBlackList = BoxBlackList.findBySerial(serial);
            // In case that the box is in blacklist has to response the stop working date
            jsonMap.dateStopWorking = boxBlackList?.dateStopWorking.toString();
            if (boxBlackList && boxBlackList.dateStopWorking <= new Date()) {
                java.lang.Object[] args = new java.lang.Object[1];
                args[0] = serial;
                jsonMap = gearsService.setResultInJsonMap(jsonMap, false,
                    messageSource.getMessage('api.box.validation.error.blacklisted', args, null), ErrorCode.BLACK_LISTED);
            } else {
                def json = restClientService.validateCertificate(serial, certificate, certVersion, certPath);
                if (gearsService.checkStatusSuccessful(json)) {
                    // Checks if the communication body response was true or false
                    if (gearsService.checkCertificateValidation(json)) {
                        jsonMap.success = true;
                        jsonMap = gearsService.setResultInJsonMap(jsonMap, true, 
                             messageSource.getMessage('api.box.validation..success', null, null), ErrorCode.NO_ERROR);
                    } else {
                        jsonMap.success = false;
                        jsonMap = gearsService.setResultInJsonMap(jsonMap, false, 
                            messageSource.getMessage('api.box.validation.error.failed', null, null), ErrorCode.INVALID_CERTIFICATE);
                    }
                } else {
                    if (json) {
                        jsonMap = gearsService.setResultInJsonMap(jsonMap, false,
                            messageSource.getMessage('api.box.validation.error.failed', null, null), ErrorCode.GENERAL_EXCEPTION);
                    } else { // Not receive a response, service unavaliable                    
                        jsonMap = gearsService.setResultInJsonMap(jsonMap, false,
                            messageSource.getMessage('api.box.validation.error.comm', null, null), ErrorCode.GENERAL_EXCEPTION);
                    }
                }
            }
        } else {
            jsonMap.success = false;
            jsonMap = gearsService.setResultInJsonMap(jsonMap, false,
                messageSource.getMessage('api.box.validation.error.not.found', null, null), ErrorCode.NO_DEVICE_FOUND);
        }
        return jsonMap;
    }

    /**
     * Validates the connection of a ExXtractor and returns the box object if exists and the result of the connection
     * 
     * @param serial
     *                  Serial number of the box to check the connection
     * @param inBlackList
     *                  Flag that indicate if the box is in the black list or not
     * @param version
     *                  Version from the authentication box message
     * @return Array [box, results] with the related objects of the validation process
     */
    public validateBoxConnection(String serial, boolean inBlackList, String version) {
        boolean results = true;
        final Box box = Box.findBySerial(serial);
        if (box == null) {
            // Must verified that the box with oldSerial never generates a certificate
            log.info "Checking certificate doesn't exist on Manufacturer before unregister: "  + serial;
            BoxManufacturer boxManufacturer = BoxManufacturer.findByLegacySerialNumber(serial);
            log.info "Before unregister serial doesn't have Manufacturer: "  + serial;
            final BoxUnregistered unRegisteredbox = BoxUnregistered.findBySerial(serial);
            if (boxManufacturer == null) { // If there is not a Legacy record of serial number
                // If there is not at the unregistered table and is not in black list
                if (unRegisteredbox == null) {
                    log.info "Before unregister serial doesn't was unregistered: "  + serial;
                    unRegisteredbox = new BoxUnregistered(serial: serial, date: new Date(), versionSoftwareLabel: version);
                    if (unRegisteredbox.validate()) {
                        log.info "Creating unregister object after validate: "  + unRegisteredbox.serial +
                            ", date: " + unRegisteredbox.date+ ", Soft Version: " + unRegisteredbox.versionSoftwareLabel;
                        if (unRegisteredbox.save()) {
                            log.info "After unregistered Box Serial: "  + serial + " [UNREGISTERED successful]";
                            results = true;
                        } else {
                            log.info "After trying to save unregistered serial: " + serial + " [REGISTERED failed]";
                            results = false;
                        }
                    } else {
                        log.warn unRegisteredbox.errors.allErrors.join(' \n');
                        results = false;
                        log.info "After trying to save unregistered and having errors to save serial: "  + serial + " [REGISTERED failed]";
                    }
                }
                if (!inBlackList) {
                    log.info "The box is unregistered and not in the black list ["  + serial + "]";
                    unRegisteredbox.lastUpdated = new Date();
                    // save returns the instance if success or null when some erros are generated
                    results = unRegisteredbox.save()? true:false;
                    box = handleDXUnregisterConnection(serial);
                }
            } else {
                log.info "Registering on BlackList unregistered Box Serial: "  + serial;
                BoxBlackList boxBlackList = new BoxBlackList(serial:serial,dateCreated:new Date(),dateStopWorking:new Date())
                if (!boxBlackList.save()) {
                    log.error "Couldn't register serial in blackList->" + serial;
                }
            }
        }
        return [box, results];
    }

    /**
     * Will check if the box is from the DX series and registers to the configured Venue if it is
     * 
     * @param serial
     *                  Serial number to check and register if its a DX
     */
    public Box handleDXUnregisterConnection(String serial) {
        Box box = null;
        final String partnerCode = SerialNumberGeneratorHelper.getPartnerCodeFromSerial(serial);
        if (partnerCode == PartnerEnum.DXX.getCode()) { // Only register for DXX ExXtractors
            final long venueId = new Long(grailsApplication.config.wifiAudioVenueId);
            final CompanyLocation dxVenue = CompanyLocation.read(venueId);
            if (dxVenue) {
                box = new Box();
                box.serial = serial;
                box.location = dxVenue;
                box.name = StringUtils.EMPTY;
                final String errorMsg = registerBox(box, true, false);
                if (StringUtils.isNotBlank(errorMsg)) {
                    log.warn("Error registering a DX ExXtractor to the Venue: " + errorMsg);
                }
            } else {
                log.warn("The DX Venue can't be found using id: " + grailsApplication.config.maxDateForValidations);
            }
        }
        return box;
    }
}