package mycentralserver;

import java.util.HashMap;

import mycentralserver.box.Box;
import mycentralserver.generaldomains.Configuration;
import mycentralserver.utils.Constants;

/**
 * Service in charge of most of the logic related with the
 * general configurations of the system.
 * 
 * @author sdiaz
 *
 */
class ConfigurationService {

	/* Inject Required Services*/
	def restClientService;
	
	/**
	 * Will send all the available configurations for a box to the
	 * box with the serial number received.
	 * 
	 * @param serialNumber
	 * 			Serial number of the box that should receive the configurations
	 * @return
	 */
	def sendConfigurationsWhenBoxConnect(String serialNumber){
		def configurations = Configuration.findAllByBoxConfig(true);
		for(Configuration configuration:configurations){
			restClientService.sendConfigurationUpdate(serialNumber, configuration);
		}
	}
	
	/**
	 * This method receive a Configuration and send the information
	 * of the Configuration to all the connected boxes.
	 * 
	 * @param config
	 * 				Configuration object to be updated on the boxes
	 * @return
	 */
	def sendConfigurationUpdateToConnectedServers(Configuration config) {
		if( config.boxConfig){
			//Get the list of Connected ExXtractors by Status
			def exxtractors = Box.connectedByStatus(0, null).list();
	
			// Go over the list and add the Box if it is not in the list already
			for(exx in exxtractors){
				if(!RetrieveUsageDataService.boxInQueue.contains(exx.serial)){
					//If it's not in the list
					RetrieveUsageDataService.boxInQueue.add(exx.serial);
				}
			}
			
			(new ConfigurationUpdateThread(config)).start();
		}
	}
	
	/**
	 * This class implements a Thread that will send a Configuration
	 * to all the connected boxes; we are creating a Thread because
	 * we don't want that the use must wait all the time that can takes
	 * to send the message to all the boxes.
	 *  
	 */
	private class ConfigurationUpdateThread extends Thread {
		
		Configuration config = null;
		
		public ConfigurationUpdateThread(Configuration config){
			super();
			this.config = config;
		}
		
		public void run() {
			String currentSerial = "";
			while(RetrieveUsageDataService.boxInQueue.size() > 0) {
				//There are boxes in the queue
				currentSerial = RetrieveUsageDataService.boxInQueue.getFirst();
				log.info("Send the configuration update to: " + currentSerial + " with config value: " + config.value);
				restClientService.sendConfigurationUpdate(currentSerial, config);
				RetrieveUsageDataService.boxInQueue.remove(currentSerial);
			}
		}	
	}
}