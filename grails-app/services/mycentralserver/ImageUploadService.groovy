package mycentralserver;

import org.springframework.context.i18n.LocaleContextHolder;

import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;

import mycentralserver.app.AppSkin;
import mycentralserver.app.WelcomeAd;
import mycentralserver.beans.ImageUploadResult;
import mycentralserver.content.Content;
import mycentralserver.utils.Constants;
import mycentralserver.utils.Utils;
import mycentralserver.utils.exceptions.ImageValidationException;

/**
 * Service class that handles the upload process of the images from UI including the generation of images from others
 * 
 * @author sdiaz
 * @since 09/30/2015
 */
class ImageUploadService {

    public static final String DEFAULT_BACKGOUND_COLOR = "#000000";
    public static final String PARAM_BG_IMAGE_URL = "bgImageUrl";
    public static final String PARAM_BG_IMAGE_URL_TABLET = "bgImageUrlTablet";
    public static final String PARAM_DIALOG_IMAGE_FILE = "dialogImageFile";
    public static final String PARAM_DIALOG_IMAGE_URL_FILE = "dialogImageUrlFile";
    public static final String PARAM_DIALOG_TABLET_IMAGE_FILE = "dialogImageFileTablet";
    public static final String PARAM_FEATURED_IMAGE_FILE = "featuredImageFile";
    public static final String PARAM_LARGE_IMAGE_URL_FILE = "largeImageUrlFile";
    public static final String PARAM_SMALL_IMAGE_URL_FILE = "smallImageUrlFile";
    public static final String PARAM_TABLET_DIALOG_IMAGE_URL_FILE = "tabletDialogImageUrlFile";

    def grailsApplication;
    def messageSource;
    def rackspaceService;

    /**
     * This method will download an image using a URL
     *
     * @param url   URL of the file to download
     * @return      BufferedImage of the download image
     */
    public BufferedImage downloadImageFromUrl(String imageUrl){
        URL url =new URL(imageUrl);
        // Read the url
        BufferedImage image = ImageIO.read(url);
        return image;
    }

    /**
     * This method will check the parameters and execute the upload of the background images of the AppSkin
     *
     * @param appSkin
     *          AppSkin that is been processed
     * @param imagesUrlsToDelete
     *          When edit, odd images to delete when success
     * @param deleteWhenFails
     *          Save the URL in case the save process fails
     * @param params
     *          Request parameters
     * @param request
     *          Request object
     * @param flash
     *          Flash object for messages
     */
    public void uploadBackgroundImagesForAppSkin(AppSkin appSkin, def imagesUrlsToDelete, def deleteWhenFails, params, request, flash) {
        final String color = (params.primaryColor)? params.primaryColor:DEFAULT_BACKGOUND_COLOR;
        ImageUploadResult imgUploadResult = uploadImageFileUsingResult(
                messageSource.getMessage('default.field.ads.background.image', null, LocaleContextHolder.locale), PARAM_BG_IMAGE_URL,
                Constants.FEATURED_CONTENT_IMAGE_WIDTH, Constants.FEATURED_CONTENT_IMAGE_HEIGHT, params, request, flash);
        if(imgUploadResult != null) {
            if(imagesUrlsToDelete != null) imagesUrlsToDelete.add(appSkin.backgroundImageUrl);
            appSkin.backgroundImageUrl  =  imgUploadResult.getImageUrl();
            deleteWhenFails.add(imgUploadResult.getImageUrl());
        }

        String url;
        if(params.genFeaturedImgForTablets) {
            if(imgUploadResult != null) {
                url = rackspaceService.generateAndUploadImage(imgUploadResult.getTempImage(), imgUploadResult.getFileName(),
                        Constants.FEATURED_CONTENT_IMAGE_WIDTH, Constants.FEATURED_CONTENT_IMAGE_HEIGHT,
                        Constants.FEATURED_CONTENT_IMAGE_TABLET_WIDTH, Constants.FEATURED_CONTENT_IMAGE_TABLET_HEIGHT, color);
                if(url != null) {
                    if(imagesUrlsToDelete != null) imagesUrlsToDelete.add(appSkin.backgroundImageUrlTablet);
                    appSkin.backgroundImageUrlTablet  =  url;
                    deleteWhenFails.add(url);
                }
                deleteImageFromDisk(imgUploadResult);
            } else {
                //If the check is on but not new small image; verifies if the image is already uploaded;
                //if not, must download reference if exists and upload generated image
                if(Utils.isEmptyString(appSkin.backgroundImageUrlTablet)
                    && Utils.isNotEmptyString(appSkin.backgroundImageUrl) ){//Not upload yet and reference exists
                    url = downloadGenerateAndUpload(appSkin.backgroundImageUrl,
                            Constants.FEATURED_CONTENT_IMAGE_WIDTH, Constants.FEATURED_CONTENT_IMAGE_HEIGHT,
                            Constants.FEATURED_CONTENT_IMAGE_TABLET_WIDTH, Constants.FEATURED_CONTENT_IMAGE_TABLET_HEIGHT, color);
                    if(url != null) {
                        if(imagesUrlsToDelete != null) imagesUrlsToDelete.add(appSkin.backgroundImageUrlTablet);
                        appSkin.backgroundImageUrlTablet  =  url;
                        deleteWhenFails.add(url);
                    }
                }
            }
        } else {//Do not generate; try to upload specific image
            deleteImageFromDisk(imgUploadResult);
            url = uploadImageFile(messageSource.getMessage('default.field.ads.background.image.tablet', null, LocaleContextHolder.locale), PARAM_BG_IMAGE_URL_TABLET,
                    Constants.FEATURED_CONTENT_IMAGE_TABLET_WIDTH, Constants.FEATURED_CONTENT_IMAGE_TABLET_HEIGHT, params, request, flash);
            if(url != null) {
                if(imagesUrlsToDelete != null) imagesUrlsToDelete.add(appSkin.backgroundImageUrlTablet);
                appSkin.backgroundImageUrlTablet  =  url;
                deleteWhenFails.add(url);
            }
        }
    }

    /**
     * This method will check the parameters and execute the upload of the dialog images of the AppSkin
     *
     * @param appSkin
     *          AppSkin that is been processed
     * @param imagesUrlsToDelete
     *          When edit, odd images to delete when success
     * @param deleteWhenFails
     *          When edit, odd images to delete when success
     * @param params
     *          Request parameters
     * @param request
     *          Request object
     * @param flash
     *          Flash object for messages
     */
    public void uploadDialogImagesForAppSkin(AppSkin appSkin, def imagesUrlsToDelete, def deleteWhenFails,
            params, request, flash){

        final String color = (params.primaryColor)? params.primaryColor:DEFAULT_BACKGOUND_COLOR;
        ImageUploadResult imgUploadResult = uploadImageFileUsingResult(
                messageSource.getMessage('dialog.image', null, LocaleContextHolder.locale), PARAM_DIALOG_IMAGE_URL_FILE,
                Constants.TEXT_CONTENT_BACKGROUND_WIDTH, Constants.TEXT_CONTENT_BACKGROUND_HEIGHT, params, request, flash);

        if(imgUploadResult != null){
            if(imagesUrlsToDelete != null) imagesUrlsToDelete.add(appSkin.dialogImageUrl);
            appSkin.dialogImageUrl  =  imgUploadResult.getImageUrl();
            deleteWhenFails.add(imgUploadResult.getImageUrl());
        }

        String url;
        if(params.genDialogImgForTablets) {
            if(imgUploadResult != null) {
                url = rackspaceService.generateAndUploadImage(imgUploadResult.getTempImage(), imgUploadResult.getFileName(),
                        Constants.TEXT_CONTENT_BACKGROUND_WIDTH, Constants.TEXT_CONTENT_BACKGROUND_HEIGHT,
                        Constants.TEXT_CONTENT_BACKGROUND_TABLET_WIDTH, Constants.TEXT_CONTENT_BACKGROUND_TABLET_HEIGHT, color);
                if(url != null) {
                    if(imagesUrlsToDelete != null) imagesUrlsToDelete.add(appSkin.tabletDialogImageUrl);
                    appSkin.tabletDialogImageUrl =  url;
                    deleteWhenFails.add(url);
                }
                deleteImageFromDisk(imgUploadResult);
            } else {
                //If the check is on but not new small image; verifies if the image is already uploaded;
                //if not, must download reference if exists and upload generated image
                if(Utils.isEmptyString(appSkin.tabletDialogImageUrl)
                    && Utils.isNotEmptyString(appSkin.dialogImageUrl) ) {//Not upload yet and reference exists
                    url = downloadGenerateAndUpload(appSkin.dialogImageUrl,
                            Constants.TEXT_CONTENT_BACKGROUND_WIDTH, Constants.TEXT_CONTENT_BACKGROUND_HEIGHT,
                            Constants.TEXT_CONTENT_BACKGROUND_TABLET_WIDTH, Constants.TEXT_CONTENT_BACKGROUND_TABLET_HEIGHT, color);
                    if(url != null) {
                        if(imagesUrlsToDelete != null) imagesUrlsToDelete.add(appSkin.tabletDialogImageUrl);
                        appSkin.tabletDialogImageUrl  =  url;
                        deleteWhenFails.add(url);
                    }
                }
            }
        } else { //Do not generate; try to upload specific image
            deleteImageFromDisk(imgUploadResult);
            url = uploadImageFile(messageSource.getMessage('dialog.image.tablet', null, LocaleContextHolder.locale), PARAM_TABLET_DIALOG_IMAGE_URL_FILE,
                    Constants.TEXT_CONTENT_BACKGROUND_TABLET_WIDTH, Constants.TEXT_CONTENT_BACKGROUND_TABLET_HEIGHT,
                    params, request, flash);
            if(url != null) {
                if(imagesUrlsToDelete != null) imagesUrlsToDelete.add(appSkin.tabletDialogImageUrl);
                appSkin.tabletDialogImageUrl  =  url;
                deleteWhenFails.add(url);
            }
        }
    }

    /**
     * This method will check the parameteres and execute the upload of
     * the feature images of the Content
     *
     * @param content               Content that is been processed
     * @param imagesUrlsToDelete    When edit, odd images to delete when success
     * @param deleteWhenFails       Save the URL in case the save process fails
     */
    public void uploadFeatureImages(Content content, def imagesUrlsToDelete, def deleteWhenFails, params, request,
            flash, final String previousColor, boolean previousGenDialogImages, boolean previousGenTabletDialogImages)
            throws ImageValidationException {
        if((content.type==Constants.CONTENT_TYPE_IMAGE) || (content.type==Constants.CONTENT_TYPE_OFFER)) {
            ImageUploadResult imgUploadResult = uploadImageFileUsingResult(
                    messageSource.getMessage('featured.image', null, LocaleContextHolder.locale), PARAM_FEATURED_IMAGE_FILE,
                    Constants.FEATURED_CONTENT_IMAGE_WIDTH, Constants.FEATURED_CONTENT_IMAGE_HEIGHT, params,
                    request, flash);
            if(imgUploadResult != null) {
                if(imagesUrlsToDelete != null) imagesUrlsToDelete.add(content.featuredImage);
                content.featuredImage  =  imgUploadResult.getImageUrl();
                deleteWhenFails.add(imgUploadResult.getImageUrl());
            }

            String url;
            final String color = (params.color)? params.color:DEFAULT_BACKGOUND_COLOR;
            final boolean changeColor = (previousColor != content.color);
            if(params.genDialogImages) {
                // Must Autogenerate dialog images
                if(imgUploadResult != null) { //New Banner Image Upload
                    // Auto generate Dialog Image for Phones
                    generatesDialogImageFromBannerImage(content, imgUploadResult, imagesUrlsToDelete, deleteWhenFails, color);

                    // Auto generate Dialog Image for Tablets
                    generatesTabletDialogImageFromBannerImage(content, imgUploadResult,
                            imagesUrlsToDelete, deleteWhenFails, color);

                    deleteImageFromDisk(imgUploadResult);
                } else {
                    if (Utils.isNotEmptyString(content.featuredImage)
                        && (previousGenDialogImages == false || changeColor) ) {
                        // Generates and Upload the Dialog Image
                        downloadAndGeneratesDialogImageFromBannerImage(content, imagesUrlsToDelete, deleteWhenFails, color);
                        // Generates and Upload the Tablets Dialog Image
                        downloadAndGeneratesTabletsDialogImageFromBannerImage(content, imagesUrlsToDelete, deleteWhenFails, color);
                    }
                }
            } else {//Do not autogenerated dialog images; try to upload specific images
                deleteImageFromDisk(imgUploadResult);
                uploadDialogImages(content, imagesUrlsToDelete, deleteWhenFails, previousGenTabletDialogImages,
                        previousColor, params, request, flash);
            }
        }
    }
    
    /**
     * This method will check the params and execute the upload of the images of the WelcomeAd
     *
     * @param welcomeAd
     *          Welcome Ad that is been processed
     * @param imagesUrlsToDelete
     *          When edit, odd images to delete when success
     * @param deleteWhenFails
     *          When edit, odd images to delete when success
     * @param params
     *          Request parameters
     * @param request
     *          Request object
     * @param flash
     *          Flash object for messages
     */
    public void uploadImagesForWelcomeAd(WelcomeAd welcomeAd, def imagesUrlsToDelete, def deleteWhenFails,
            params, request, flash) {

        final String color = (params.primaryColor)? params.primaryColor:DEFAULT_BACKGOUND_COLOR;
        ImageUploadResult imgUploadResult = uploadImageFileUsingResult(
                messageSource.getMessage('welcome.message.file.1', null, LocaleContextHolder.locale), PARAM_SMALL_IMAGE_URL_FILE,
                Constants.IMAGE_PHONE_3_5_WIDTH, Constants.IMAGE_PHONE_3_5_HEIGHT, params, request, flash);

        if(imgUploadResult != null) {
            if(imagesUrlsToDelete != null) imagesUrlsToDelete.add(welcomeAd.smallImageUrl);
            welcomeAd.smallImageUrl  =  imgUploadResult.getImageUrl();
            deleteWhenFails.add(imgUploadResult.getImageUrl());
        }

        String url;
        if(params.genImgForTablets) {
            if(imgUploadResult != null) {
                url = rackspaceService.generateAndUploadImage(imgUploadResult.getTempImage(), imgUploadResult.getFileName(),
                        Constants.IMAGE_PHONE_3_5_WIDTH, Constants.IMAGE_PHONE_3_5_HEIGHT,
                        Constants.IMAGE_TABLET_WIDTH, Constants.IMAGE_TABLET_HEIGHT, color);

                if(url != null){
                    if(imagesUrlsToDelete != null) imagesUrlsToDelete.add(welcomeAd.largeImageUrl);
                    welcomeAd.largeImageUrl =  url;
                    deleteWhenFails.add(url);
                }

                deleteImageFromDisk(imgUploadResult);
            } else {
                //If the check is on but not new small image; verifies if the image is already uploaded;
                //if not, must download reference if exists and upload generated image
                if(Utils.isEmptyString(welcomeAd.largeImageUrl)
                    && Utils.isNotEmptyString(welcomeAd.smallImageUrl) ) {//Not upload yet and reference exists
                    url = downloadGenerateAndUpload(welcomeAd.smallImageUrl,
                            Constants.IMAGE_PHONE_3_5_WIDTH, Constants.IMAGE_PHONE_3_5_HEIGHT,
                            Constants.IMAGE_TABLET_WIDTH, Constants.IMAGE_TABLET_HEIGHT, color);
                    if(url != null) {
                        if(imagesUrlsToDelete != null) imagesUrlsToDelete.add(welcomeAd.largeImageUrl);
                        welcomeAd.largeImageUrl  =  url;
                        deleteWhenFails.add(url);
                    }
                }
            }
        } else {//Do not generate; try to upload specific image
            deleteImageFromDisk(imgUploadResult);
            url = uploadImageFile(messageSource.getMessage('welcome.message.file.3', null, LocaleContextHolder.locale), PARAM_LARGE_IMAGE_URL_FILE,
                    Constants.IMAGE_TABLET_WIDTH, Constants.IMAGE_TABLET_HEIGHT, params, request, flash);
            if(url != null){
                if(imagesUrlsToDelete != null) imagesUrlsToDelete.add(welcomeAd.largeImageUrl);
                welcomeAd.largeImageUrl  =  url;
                deleteWhenFails.add(url);
            }
        }
    }

    /**
     * This method will delete an unrequired file from the disk
     * @param imgUploadResult
     *          Object of the file to delete
     */
    private void deleteImageFromDisk(ImageUploadResult imgUploadResult) {
        if(imgUploadResult != null) {//Delete feature image since is not needed
            rackspaceService.deleteFileUsingPath(imgUploadResult.getPath());
        }
    }

    /**
     * Will download and generate the dialog image using the stored banner image of a content and sets the uploaded
     * file URL to the properties of the Content
     * 
     * @param content
     *          Content that is been updated
     * @param imagesUrlsToDelete
     *          Array list with the list of files URLs that must be deleted when success
     * @param deleteWhenFails
     *          Array list with the list of files URLs that must be deleted when the process failed
     * @param color
     *          Background color to be used for the generation of the image
     */
    private void downloadAndGeneratesDialogImageFromBannerImage(Content content,
            def imagesUrlsToDelete, def deleteWhenFails, final String color) {
        final String url = downloadGenerateAndUpload(content.featuredImage,
                Constants.FEATURED_CONTENT_IMAGE_WIDTH, Constants.FEATURED_CONTENT_IMAGE_HEIGHT,
                Constants.DIALOG_CONTENT_IMAGE_WIDTH, Constants.DIALOG_CONTENT_IMAGE_HEIGHT, color);
        if(url != null){
            if(imagesUrlsToDelete != null) imagesUrlsToDelete.add(content.dialogImage);
            //we have the auto generated image, then we set it as both dialog image and featured image for tablets
            content.dialogImage =  url;
            content.featuredImageTablet = url;
            deleteWhenFails.add(url);
        }
    }

    /**
     * Will download and generate the tablets dialog image using the stored banner image of a content and sets the 
     * uploaded file URL to the properties of the Content
     *
     * @param content
     *          Content that is been updated
     * @param imagesUrlsToDelete
     *          Array list with the list of files URLs that must be deleted when success
     * @param deleteWhenFails
     *          Array list with the list of files URLs that must be deleted when the process failed
     * @param color
     *          Background color to be used for the generation of the image
     */
    private void downloadAndGeneratesTabletsDialogImageFromBannerImage(Content content,
            def imagesUrlsToDelete, def deleteWhenFails, final String color) {
        final String url = downloadGenerateAndUpload(content.featuredImage,
                Constants.FEATURED_CONTENT_IMAGE_WIDTH, Constants.FEATURED_CONTENT_IMAGE_HEIGHT,
                Constants.DIALOG_CONTENT_IMAGE_TABLET_WIDTH, Constants.DIALOG_CONTENT_IMAGE_TABLET_HEIGHT, color);
        if(url != null){
            if(imagesUrlsToDelete != null) imagesUrlsToDelete.add(content.dialogImageTablet);
            content.dialogImageTablet =  url;
            deleteWhenFails.add(url);
        }
    }

    /**
     * This method will download a file from a URL and generates and upload a new one using the received parameters
     * 
     * @param imageUrl
     *      Url of the source image to be use for the generation
     * @param sourceWidth
     *      Width of the source image
     * @param sourceHeight
     *      Height of the source image
     * @param destWidth
     *      Width of the generated image
     * @param destHeight
     *      Height of the generated image
     * @param color
     *      Color used as background for the generation
     * @return Url of the generated file
     */
    private String downloadGenerateAndUpload(final String imageUrl, final int sourceWidth, final int sourceHeight,
            final int destWidth, final int destHeight, final String color){
        //Gets the file name from the URL
        final String[] urlParts = imageUrl.split("/");
        final String fileName = urlParts[urlParts.size() - 1];
        //Download the image
        BufferedImage tempImage = downloadImageFromUrl(imageUrl);
        //Generate and upload the new image
        final String url = rackspaceService.generateAndUploadImage(tempImage, fileName,
                sourceWidth, sourceHeight, destWidth, destHeight, color);
        return url;
    }

    /**
     * This method will generates the dialog image using the banner image on the local disc as reference and upload the
     * file to the storage
     * 
     * @param content
     *          Content that is been updated
     * @param imgUploadResult
     *          Object with the reference to the image to use for the generation
     * @param imagesUrlsToDelete
     *          Array list with the list of files URLs to be deleted when success
     * @param deleteWhenFails
     *          Array list with the list of files URLs to be deleted when the process fails
     * @param color
     *          Background color to be use for the generation of the image
     */
    private void generatesDialogImageFromBannerImage(Content content, ImageUploadResult imgUploadResult,
            def imagesUrlsToDelete, def deleteWhenFails, final String color) {
        final String url = rackspaceService.generateAndUploadImage(
                imgUploadResult.getTempImage(), imgUploadResult.getFileName(),
                Constants.FEATURED_CONTENT_IMAGE_WIDTH, Constants.FEATURED_CONTENT_IMAGE_HEIGHT,
                Constants.DIALOG_CONTENT_IMAGE_WIDTH, Constants.DIALOG_CONTENT_IMAGE_HEIGHT, color);
        if(url != null){
            if(imagesUrlsToDelete != null) imagesUrlsToDelete.add(content.dialogImage);
            content.dialogImage  =  url;
            content.featuredImageTablet  =  url;
            deleteWhenFails.add(url);
        }
    }

    /**
     * This method will generates the tablets dialog image using the banner image on the local disc as reference and 
     * upload the file to the storage
     *
     * @param content
     *          Content that is been updated
     * @param imgUploadResult
     *          Object with the reference to the image to use for the generation
     * @param imagesUrlsToDelete
     *          Array list with the list of files URLs to be deleted when success
     * @param deleteWhenFails
     *          Array list with the list of files URLs to be deleted when the process fails
     * @param color
     *          Background color to be use for the generation of the image
     */
    private void generatesTabletDialogImageFromBannerImage(Content content, ImageUploadResult imgUploadResult,
            def imagesUrlsToDelete, def deleteWhenFails, final String color) {
        // Auto generate Dialog Image for Tablets
        final String url = rackspaceService.generateAndUploadImage(
                imgUploadResult.getTempImage(), imgUploadResult.getFileName(),
                Constants.FEATURED_CONTENT_IMAGE_WIDTH, Constants.FEATURED_CONTENT_IMAGE_HEIGHT,
                Constants.DIALOG_CONTENT_IMAGE_TABLET_WIDTH, Constants.DIALOG_CONTENT_IMAGE_TABLET_HEIGHT, color);

        if(url != null){
            if(imagesUrlsToDelete != null) imagesUrlsToDelete.add(content.dialogImageTablet);
            content.dialogImageTablet  =  url;
            deleteWhenFails.add(url);
        }
    }

    /**
     * @param content
     *          Content that is been updated
     * @param imagesUrlsToDelete
     *          Array with the list of old urls that must be deleted after correct save
     * @param deleteWhenFails
     *          Array with the list of temporal urls that must be deleted if the complete process failed
     * @param previousGenDialogImgForTablets
     *          Previous value of the checkbox for the generation of the tablets dialog image
     * @param previousColor
     *          Previous color value of the Content
     * @param params
     *          Request parameters
     * @param request
     *          Request object
     * @param flash
     *          Flash object for error messages
     */
    private void uploadDialogImages(Content content, def imagesUrlsToDelete, def deleteWhenFails,
            final boolean previousGenDialogImgForTablets, final String previousColor, params, request, flash){

        if(content.type==Constants.CONTENT_TYPE_IMAGE || content.type==Constants.CONTENT_TYPE_OFFER ) {
            final String color = (params.color)? params.color:DEFAULT_BACKGOUND_COLOR;
            final boolean changeColor = (previousColor != content.color);

            ImageUploadResult imgUploadResult = uploadImageFileUsingResult(
                    messageSource.getMessage('dialog.image', null, LocaleContextHolder.locale), PARAM_DIALOG_IMAGE_FILE,
                    Constants.DIALOG_CONTENT_IMAGE_WIDTH, Constants.DIALOG_CONTENT_IMAGE_HEIGHT, params, request, flash);

            String url;
            if(imgUploadResult != null) {
                // There is a new image for Dialog Image
                if(imagesUrlsToDelete != null) imagesUrlsToDelete.add(content.dialogImage);
                content.dialogImage  =  imgUploadResult.getImageUrl();
                content.featuredImageTablet = imgUploadResult.getImageUrl();
                deleteWhenFails.add(imgUploadResult.getImageUrl());

                if(params.genDialogImgForTablets) {
                    // The checkbox for the generation of the Tablets Dialog Image is checked
                    url = rackspaceService.generateAndUploadImage(imgUploadResult.getTempImage(), imgUploadResult.getFileName(),
                            Constants.DIALOG_CONTENT_IMAGE_WIDTH, Constants.DIALOG_CONTENT_IMAGE_HEIGHT,
                            Constants.DIALOG_CONTENT_IMAGE_TABLET_WIDTH, Constants.DIALOG_CONTENT_IMAGE_TABLET_HEIGHT, color)

                    if(url != null) {
                        if(imagesUrlsToDelete != null) imagesUrlsToDelete.add(content.dialogImageTablet);
                        content.dialogImageTablet  =  url;
                        deleteWhenFails.add(url);
                    }

                    deleteImageFromDisk(imgUploadResult)
                } else {
                    // The checkbox for the generation of the Tablets Dialog Image is unchecked
                    deleteImageFromDisk(imgUploadResult);
                    //Try to upload the tablets img is comes on the request
                    uploadTabletsDialogImage(content, imagesUrlsToDelete, deleteWhenFails, params, request, flash);
                }
            } else {
                // It's not a new image for Dialog Image
                if(params.genDialogImgForTablets) {
                    if(Utils.isNotEmptyString(content.dialogImage) && 
                        (previousGenDialogImgForTablets == false || changeColor) ) {
                        url = downloadGenerateAndUpload(content.dialogImage,
                                Constants.DIALOG_CONTENT_IMAGE_WIDTH, Constants.DIALOG_CONTENT_IMAGE_HEIGHT,
                                Constants.DIALOG_CONTENT_IMAGE_TABLET_WIDTH, Constants.DIALOG_CONTENT_IMAGE_TABLET_HEIGHT, color);
                        if(url != null){
                            if(imagesUrlsToDelete != null) imagesUrlsToDelete.add(content.dialogImageTablet);
                            content.dialogImageTablet  =  url;
                            deleteWhenFails.add(url);
                        }
                    }
                } else {
                    // The checkbox for the generation of the Tablets Dialog Image is unchecked
                    deleteImageFromDisk(imgUploadResult);
                    //Try to upload the tablets img is comes on the request
                    uploadTabletsDialogImage(content, imagesUrlsToDelete, deleteWhenFails, params, request, flash);
                }
            }
        }
    }

    /**
     * This method will try to upload an Image calling the crop method and validating the dimensions on backend
     * 
     * @param fieldName
     *          Field name for error message
     * @param fileId
     *          Id of the Field used to get the extra parameters
     * @param requiredWidth
     *          Required width for validations
     * @param requiredHeight
     *          Required height for validations
     * @param params
     *          Request parameters
     * @param request
     *          Request object
     * @param flash
     *          Flash object for error messages
     * @return Url when upload is success
     */
    private String uploadImageFile(String fieldName, String fileId,
            int requiredWidth, int requiredHeight, params, request, flash){

        try{
            if(params[fileId + '-updated'] == '1'){
                def imgFile = request.getFile(fileId);
                if(imgFile != null) {
                    ImageUploadResult result = rackspaceService.cropAndSaveImage(imgFile, , params['x-' + fileId], params['y-' + fileId],
                            params['w-' + fileId], params['h-' + fileId],
                            requiredWidth, requiredHeight);
                    return result.getImageUrl();
                }
            } else {
                return null;
            }
        }catch(Exception e){
            if(e.getMessage().indexOf("Image Dimensions are wrong") > -1){
                java.lang.Object[] args = new java.lang.Object[3];
                args[0] = requiredWidth; args[1] = requiredHeight; args[2] = fieldName;
                flash.error = messageSource.getMessage('image.field.error.with.dimensions', args, LocaleContextHolder.locale);
            } else {
                java.lang.Object[] args = new java.lang.Object[1];
                args[0] = fieldName;
                flash.error = messageSource.getMessage('image.field.error.java.error', args, LocaleContextHolder.locale);
            }
            throw e;
        }
    }

    /**
     * This method will try to upload an Image calling the crop method and validating the dimensions on backend.
     * 
     * @param fieldName
     *          Field name for error message
     * @param fileId
     *          Id of the Field used to get the extra parameters
     * @param requiredWidth
     *          Required width for validations
     * @param requiredHeight
     *          Required height for validations
     * @param params
     *          Request parameters
     * @param request
     *          Request object
     * @param flash
     *          Flash object for messages
     * @return Url when upload is success
     */
    private ImageUploadResult uploadImageFileUsingResult(String fieldName, String fileId,
            int requiredWidth, int requiredHeight, params, request, flash) {
        try{
            if(params[fileId + '-updated'] == '1'){
                def imgFile = request.getFile(fileId);
                if(imgFile != null) {
                    ImageUploadResult result = rackspaceService.cropAndSaveImage(imgFile, , params['x-' + fileId], params['y-' + fileId],
                            params['w-' + fileId], params['h-' + fileId],
                            requiredWidth, requiredHeight);
                    return result;
                }
            }
            return null;
        }catch(Exception e){
            if(e.getMessage().indexOf("Image Dimensions are wrong") > -1){
                java.lang.Object[] args = new java.lang.Object[3];
                args[0] = requiredWidth; args[1] = requiredHeight; args[2] = fieldName;
                flash.error = messageSource.getMessage('image.field.error.with.dimensions', args, LocaleContextHolder.locale);
                throw new ImageValidationException(flash.error, e);
            } else {
                java.lang.Object[] args = new java.lang.Object[1];
                args[0] = fieldName;
                flash.error = messageSource.getMessage('image.field.error.java.error', args, LocaleContextHolder.locale);
                throw new ImageValidationException(flash.error, e);
            }
        }
    }

    /**
     * This method verify it the upload of the Tablets Dialog Image is required and completes the process
     *
     * @param content
     *          Content that is been updated
     * @param imagesUrlsToDelete
     *          Array with the list of old urls that must be deleted after correct save
     * @param deleteWhenFails
     *          Array with the list of temporal urls that must be deleted if the complete process failed
     * @param params
     *          Request params
     * @param request
     *          Request object
     * @param flash
     *          Flash object for error messages
     */
    private void uploadTabletsDialogImage(Content content, imagesUrlsToDelete, deleteWhenFails, params, request, flash) {
        final String url = uploadImageFile(messageSource.getMessage('dialog.image.tablet', null, LocaleContextHolder.locale),
                PARAM_DIALOG_TABLET_IMAGE_FILE, Constants.DIALOG_CONTENT_IMAGE_TABLET_WIDTH,
                Constants.DIALOG_CONTENT_IMAGE_TABLET_HEIGHT, params, request, flash);
        if(url != null){
            if(imagesUrlsToDelete != null) imagesUrlsToDelete.add(content.dialogImageTablet);
            content.dialogImageTablet  =  url;
            deleteWhenFails.add(url);
        }
    }
}