/**
 * WidgetService.groovy
 */
package mycentralserver;

import java.util.HashMap;

import com.sun.java.swing.plaf.windows.WindowsBorders.DashedBorder;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.i18n.LocaleContextHolder;

import grails.converters.JSON;
import mycentralserver.box.Box;
import mycentralserver.company.Company;
import mycentralserver.content.Content;
import mycentralserver.docs.Document;
import mycentralserver.company.CompanyLocation;
import mycentralserver.user.Employee;
import mycentralserver.user.User;
import mycentralserver.user.UserDashboard;
import mycentralserver.utils.Constants;
import mycentralserver.utils.DateUtils;
import mycentralserver.utils.RoleEnum;

/**
 * Service class with most of the logic related
 * with the widgets
 * 
 * @author Cecropia Solutions
 *
 */
class WidgetService {

	/* Inject required services */
	def boxService;
	def companyService;
	def employeeService;
	def locationService;
	def contentService;
	def documentService;
	def appSkinService;
	def welcomeAdService;
	def shortcutsService;
	def userSessionService;
	def dashboardService;
	def messageSource;
	
	/**
	 * Check if the current User can see the Widget
	 * @return
	 */
	public boolean userHasAccessToWidget(String widget){
		switch(widget){
			case 'exxtractors-info-widget':
			case 'shortcuts-widget':
			case 'waAppTheme-widget':
			case 'locations-widget':
			case 'exxtractors-widget':
			case 'welcomeList-widget':
			case 'company-widget':
			case 'location-widget':
			case 'appSkin-widget':
				return true;
				
			case 'companies-widget'://Integrator Staff and Owner Staff Cannot see this widget
				return !(userSessionService.userHasRole(RoleEnum.INTEGRATOR_STAFF.value) ||
					userSessionService.userHasRole(RoleEnum.OWNER_STAFF.value));
			case 'banners-widget': //Admin cannot see this widget
			case 'offers-widget': //Admin cannot see this widget
			case 'documents-widget': //Admin cannot see this widget
				return (!userSessionService.isAdmin());
							
			case 'employees-widget':
				// Employees Widget must be available only for Integrators and Owners Users
				return (userSessionService.userHasRole(RoleEnum.INTEGRATOR.value) ||
					userSessionService.userHasRole(RoleEnum.OWNER.value));			
			default:
				return false;
		}
	} // End of userHasAccessToWidget method
	
	/**
	 * Return a List of Widgets Id that are available depending of dashboard
	 *
	 * @param params		Request parameters
	 * @return				HashMap with the result
	 * @throws Exception	When something crash
	 */
	public HashMap getAvailableWidgets(params) throws Exception {
		
		HashMap jsonMap = new HashMap();
		User user = userSessionService.getCurrentUser();
		UserDashboard dashboardsInfo = UserDashboard.findByUser(user);
		if(!dashboardsInfo) {
			dashboardsInfo = new UserDashboard();
			dashboardsInfo.user = user;
		}
		def allAvailableWidgets = [];
		def config = null;
		switch(params.dash){
			case "mainDashboard":
				allAvailableWidgets = ['exxtractors-info-widget', 'shortcuts-widget', 'companies-widget', 
					'locations-widget', 'exxtractors-widget', 'banners-widget', 
					'offers-widget', 'documents-widget', 'appSkin-widget', 'welcomeList-widget'];
				config = dashboardsInfo.userDashboard;
				if(!config || config.trim() == "" || config.trim() == "{}"){
					config = dashboardService.getDefaultDashboardConfig(DashboardService.MAIN_DASHBOARD);
				}
				break;
			case "companyDashboard":
				allAvailableWidgets = ['exxtractors-info-widget', 'shortcuts-widget',
					'locations-widget','exxtractors-widget','employees-widget'];
				config = dashboardsInfo.companyDashboard;
				if(!config || config.trim() == "" || config.trim() == "{}"){
					config = dashboardService.getDefaultDashboardConfig(DashboardService.COMPANY_DASHBOARD);
				}
				break;
			case "locationDashboard":
				allAvailableWidgets = ['exxtractors-info-widget', 'shortcuts-widget', 'exxtractors-widget',
					'banners-widget', 'offers-widget', 'documents-widget', 'waAppTheme-widget'];
				config = dashboardsInfo.locationDashboard;
				if(!config || config.trim() == "" || config.trim() == "{}"){
					config = dashboardService.getDefaultDashboardConfig(DashboardService.LOCATION_DASHBOARD);
				}
				break;
			default:
				break;
		}
		
		def jsonConfig = JSON.parse(config); // Parse to JSON
		
		def widgetsIds = [];
		HashMap widgetMap = new HashMap();
		for(widget in allAvailableWidgets){
			def widgetInConfig = jsonConfig[widget];
			if(!widgetInConfig && userHasAccessToWidget(widget)) {
				widgetMap = new HashMap();
				widgetMap["id"] = widget;
				widgetMap["name"] = messageSource.getMessage('widgets.title.' + widget, null, LocaleContextHolder.locale);
				widgetMap["desc"] = messageSource.getMessage('widgets.desc.' + widget, null, LocaleContextHolder.locale);
				widgetsIds.add(widgetMap);
			}
		}		
		
		jsonMap["widgets"] = widgetsIds;				  
		return jsonMap;
		
	} // End of saveDashboardConfig method
	
	/**
	 * Will save to DB the configuration of a dashboard for the
	 * current user
	 * 
	 * @param params		Request parameters
	 * @return				HashMap with the result
	 * @throws Exception	When something crash
	 */
	public HashMap saveDashboardConfig(params) throws Exception {
		
		HashMap jsonMap = new HashMap();
		User user = userSessionService.getCurrentUser();
		UserDashboard dashboardsInfo = UserDashboard.findByUser(user);
		if(!dashboardsInfo) {
			dashboardsInfo = new UserDashboard();
			dashboardsInfo.user = user;
		}
		switch(params.dash){
			case "mainDashboard":
				dashboardsInfo.userDashboard = "{" + params.jsonConfig + "}";
				break;
			case "companyDashboard":
				dashboardsInfo.companyDashboard = "{" + params.jsonConfig + "}";
				break;
			case "locationDashboard":
				dashboardsInfo.locationDashboard = "{" + params.jsonConfig + "}";
				break;
			default:
				break;
		}
		if(dashboardsInfo.save()){
			jsonMap[Constants.DATA_TABLES_ERROR] =  false;
		} else {
			log.warn dashboardsInfo.errors.allErrors.join(' \n');
			jsonMap[Constants.DATA_TABLES_ERROR] =  true;
		}
				  
		return jsonMap;
		
	} // End of saveDashboardConfig method

	
	/**
	 * Return a list of objects depending of parameters for the DataTables.net
	 * component
	 * 
	 * @param params		Request params object
	 * @return				HashMap with required information
	 * @throws Exception	When something crash or the object type parameter is not expected
	 */
    public HashMap getListDataAjax(params) throws Exception {
	
		HashMap jsonMap = new HashMap();
		
		// Get params information
		int columnOrder = params[Constants.DATA_TABLES_ORDER_COLUMN]? params[Constants.DATA_TABLES_ORDER_COLUMN].toInteger():0;
		HashMap queryParams = new HashMap();
		queryParams.put(Constants.DATA_TABLES_START, 
			params[Constants.DATA_TABLES_START]? params[Constants.DATA_TABLES_START].toInteger():0);
		queryParams.put(Constants.DATA_TABLES_LENGTH, 
			params[Constants.DATA_TABLES_LENGTH]? params[Constants.DATA_TABLES_LENGTH].toInteger():10);
		queryParams.put(Constants.DATA_TABLES_ORDER_DIR, 
			params[Constants.DATA_TABLES_ORDER_DIR_PARAM]? params[Constants.DATA_TABLES_ORDER_DIR_PARAM]:"asc");
		queryParams.put(Constants.DATA_TABLES_SEARCH, (params[Constants.DATA_TABLES_SEARCH_PARAM])? params[Constants.DATA_TABLES_SEARCH_PARAM]:'');
		
		queryParams.put(Constants.COMPANY_ID,
			params[Constants.COMPANY_ID]? params[Constants.COMPANY_ID].toLong():new Long(0));
		queryParams.put(Constants.LOCATION_ID,
			params[Constants.LOCATION_ID]? params[Constants.LOCATION_ID].toLong():new Long(0));
		
		HashMap<String, Object> listResult = null;
		switch(params.object.toString()) {
			case "company":
				listResult = getCompaniesList(queryParams, columnOrder);
				break;
			case "location":
				listResult = getLocationsList(queryParams, columnOrder);
				break;
			case "banner":
				listResult = getBannersList(queryParams, columnOrder);
				break;				
			case "appSkin":
				listResult = getAppSkinList(queryParams, columnOrder);
				break;
			case "welcomeAd":
				listResult = getWelcomeAdList(queryParams, columnOrder);
				break;
			case "offer":
				listResult = getOffersList(queryParams, columnOrder);
				break;
			case "document":
				listResult = getDocumentsList(queryParams, columnOrder);
				break;
			case "exxtractor":				
				listResult = getExxtractorsList(queryParams, columnOrder);
				break;
            case "exxtractor-full":
                listResult = getExxtractorsListFull(queryParams, columnOrder);
                break;
			case "employee":
				listResult = getEmployeesList(queryParams, columnOrder);
				break;
			default:
				throw new Exception("Unknown Object Type: " + params.object.toString());
		}
				
		//Put data to json structure
		jsonMap[Constants.DATA_TABLES_DRAW] = params[Constants.DATA_TABLES_DRAW]? params[Constants.DATA_TABLES_DRAW]:1;
		jsonMap[Constants.DATA_TABLES_RECORDS_TOTAL] = listResult.get(Constants.DATA_TABLES_RECORDS_TOTAL);
		jsonMap[Constants.DATA_TABLES_RECORDS_FILTERED] = listResult.get(Constants.DATA_TABLES_RECORDS_FILTERED);
		jsonMap[Constants.DATA_TABLES_ERROR] =  false;
		jsonMap[Constants.DATA_TABLES_DATA] = listResult.get(Constants.DATA_TABLES_DATA);
				  
		return jsonMap;
	} // End of getListDataAjax method
	
	/**
	 * Return a list of objects depending of parameters for the DataTables.net
	 * component
	 *
	 * @param params		Request params object
	 * @return				HashMap with required information
	 * @throws Exception	When something crash or the object type parameter is not expected
	 */
	public HashMap getDetailedDataAjax(params) throws Exception {
		
		HashMap jsonMap = new HashMap();		
		HashMap<String, Object> dataResult = null;
		HashMap queryParams = new HashMap();
		
		queryParams.put(Constants.COMPANY_ID,
			params[Constants.COMPANY_ID]? params[Constants.COMPANY_ID].toLong():new Long(0));
		queryParams.put(Constants.LOCATION_ID,
			params[Constants.LOCATION_ID]? params[Constants.LOCATION_ID].toLong():new Long(0));
		
		switch(params.object.toString()) {
			case "exxtractor":
				def (offline_needs_upgrade, offline_upgraded,
				online_upgraded, online_needs_upgrade, outdated, unversioned) = boxService.getExxtractorsStatusInfo(queryParams);
			
				jsonMap[Constants.OFFLINE_NEEDS_UPGRADE] = offline_needs_upgrade;
				jsonMap[Constants.OFFLINE_UPGRADED]		 = offline_upgraded;
				jsonMap[Constants.ONLINE_UPGRADED]		 = online_upgraded;
				jsonMap[Constants.ONLINE_NEEDS_UPGRADE]	 = online_needs_upgrade;
				jsonMap[Constants.OUTDATED]				 = outdated;
				jsonMap[Constants.UNVERSIONED]			 = unversioned;
				break;
			case "company":
				jsonMap = companyService.getCompanyInformation(queryParams);
				break;
			case "location":
				jsonMap = locationService.getLocationInformation(queryParams);
				break;				
			case "locationStyle":
				jsonMap = locationService.getLocationStyleInformation(queryParams);
				break;
			case "shortcuts":
				jsonMap = shortcutsService.getUserShortcuts(queryParams);
				break;
			case "shortcutsall":
				jsonMap = shortcutsService.getAllUserShortcuts(queryParams);
				break;
			case "saveShortcuts":
			
				queryParams.put(Constants.SHORTCUTS,
					params[Constants.SHORTCUTS]? params[Constants.SHORTCUTS].toString().trim():'');
				jsonMap = shortcutsService.saveUserShortcuts(queryParams);
				break;
			case "getAvailableWidgetsToAdd":
				jsonMap = getAvailableWidgets(params);
				jsonMap["error"] = false;
				break;
			default:
				throw new Exception("Unknown Object Type: " + params.object.toString());
		}
				  
		return jsonMap;
	} // End of getDetailedDataAjax method
	
	/**
	 * Try to upload a file depending of the received parameters
	 *
	 * @param params		Request parameters object
	 * @param request		Request object
	 * @param flash			Flash object
	 * @return				HashMap with required information
	 * @throws Exception	When something crash or the object type parameter is not expected
	 */
	public HashMap uploadFile(params, request, flash) throws Exception {
		
		HashMap jsonMap = new HashMap();
		HashMap<String, Object> dataResult = null;
		HashMap queryParams = new HashMap();
		
		long companyId = 
			params[Constants.COMPANY_ID]? params[Constants.COMPANY_ID].toLong():new Long(0);
		long locationId = 
			params[Constants.LOCATION_ID]? params[Constants.LOCATION_ID].toLong():new Long(0);
		
		switch(params.imageId.toString()) {
			case "companyLogo":
				jsonMap["url"] = companyService.uploadLogo(companyId, params, request, flash);
				break;
			case "locationLogo":
				jsonMap["url"] = locationService.uploadLogo(locationId, params, request, flash);
				break;
			default:
				throw new Exception("Try to upload a file with unknown id: " + params.imageId.toString());
		}
				  
		return jsonMap;
	} // End of uploadFile method
	
	/**
	 * Returns a HashMap with the required information to render a part of the
	 * Companies table depending of parameters
	 * 
	 * @param queryParams	Parameters to execute the query (start, length, searchText, orderDir)
	 * @param columnOrder	Number of the column to order the table
	 * @return				HashMap with required information
	 * @throws Exception	When somethings goes wrong
	 */
	private HashMap<String, Object> getCompaniesList(HashMap queryParams, int columnOrder) throws Exception {
		
		String columnOrderName = "";
		switch(columnOrder){
			case 1:
				columnOrderName = "enable";
				break;
			default:
				columnOrderName = "name";
		} // End of switch: columnOrder
		
		queryParams.put(Constants.DATA_TABLES_COLUMN_ORDER_NAME, columnOrderName);
		
		HashMap jsonMap = new HashMap();
		def tableData = [];
		Company company = null;
		def (companies,companiesAssigned, totalCount, filteredCount)  = companyService.getCompaniesOfUser(queryParams);
		
		for(row in companies){
			tableData.add([row[0], row[1], row[2]]);
			//tableData.add([company.id, company.name, company.enable]);
		} // End of foreach in companies
		
		jsonMap.put(Constants.DATA_TABLES_RECORDS_TOTAL, totalCount);
		jsonMap.put(Constants.DATA_TABLES_RECORDS_FILTERED, filteredCount);
		jsonMap.put(Constants.DATA_TABLES_DATA, tableData);
		
		return jsonMap;
	} // End of getCompaniesList method
	
	/**
	 * Returns a HashMap with the required information to render a part of the
	 * Locations table depending of parameters
	 *
	 * @param queryParams	Parameters to execute the query (start, length, searchText, orderDir)
	 * @param columnOrder	Number of the column to order the table
	 * @return				HashMap with required information
	 * @throws Exception	When somethings goes wrong
	 */
	private HashMap<String, Object> getLocationsList(HashMap queryParams, int columnOrder) throws Exception {
		
		String columnOrderName = "";
		switch(columnOrder){
			case 1:
				columnOrderName = "enable";
				break;
			default:
				columnOrderName = "name";
		} // End of switch: columnOrder
		
		queryParams.put(Constants.DATA_TABLES_COLUMN_ORDER_NAME, columnOrderName);
		
		HashMap jsonMap = new HashMap();
		def tableData = [];
		CompanyLocation location = null;
		def (locations, totalCount, filteredCount)  = locationService.getLocationsOfUser(queryParams);
		
		for(row in locations){
			//location = row;
			//tableData.add([location.id, location.name, location.enable]);			
			tableData.add([row[0], row[1], row[2]]);
		} // End of foreach in companies
		
		jsonMap.put(Constants.DATA_TABLES_RECORDS_TOTAL, totalCount);
		jsonMap.put(Constants.DATA_TABLES_RECORDS_FILTERED, filteredCount);
		jsonMap.put(Constants.DATA_TABLES_DATA, tableData);
		
		return jsonMap;

	} // End of getLocationsList method

	/**
	 * Returns a HashMap with the required information to render a part of the
	 * Banners table depending of parameters
	 *
	 * @param queryParams	Parameters to execute the query (start, length, searchText, orderDir)
	 * @param columnOrder	Number of the column to order the table
	 * @return				HashMap with required information
	 * @throws Exception	When somethings goes wrong
	 */

	private HashMap<String, Object> getBannersList(HashMap queryParams, int columnOrder) throws Exception {
		
		String columnOrderName = "";
		switch(columnOrder){
			case 1:
				columnOrderName = "enabled";
				break;
			default:
				columnOrderName = "title";
		} // End of switch: columnOrder
		
		queryParams.put(Constants.DATA_TABLES_COLUMN_ORDER_NAME, columnOrderName);
		
		HashMap jsonMap = new HashMap();
		def tableData = [];
		//Content banner = null;
		def (banners, totalCount, filteredCount)  = contentService.getBannersOfUser(queryParams);
		
		for(banner in banners){
			//banner = row;
						// title,  featuredImage, enabled
			tableData.add([banner[1],banner[2], banner[3], banner[0]]);
		} // End of foreach in companies
		
		jsonMap.put(Constants.DATA_TABLES_RECORDS_TOTAL, totalCount);
		jsonMap.put(Constants.DATA_TABLES_RECORDS_FILTERED, filteredCount);
		jsonMap.put(Constants.DATA_TABLES_DATA, tableData);
		
		return jsonMap;
	} // End of getBannersList method
	
	
	/**
	 * Returns a HashMap with the required information to render a part of the
	 * Banners table depending of parameters
	 *
	 * @param queryParams	Parameters to execute the query (start, length, searchText, orderDir)
	 * @param columnOrder	Number of the column to order the table
	 * @return				HashMap with required information
	 * @throws Exception	When somethings goes wrong
	 */

	private HashMap<String, Object> getAppSkinList(HashMap queryParams, int columnOrder) throws Exception {
		
		String columnOrderName = "";
		switch(columnOrder){
			case 1:
				columnOrderName = "title";
				break;
			default:
				columnOrderName = "name";
		} // End of switch: columnOrder
		
		queryParams.put(Constants.DATA_TABLES_COLUMN_ORDER_NAME, columnOrderName);
		
		HashMap jsonMap = new HashMap();
		def tableData = [];
		//Content banner = null;
		def (skins, totalCount, filteredCount)  = appSkinService.getSkinsOfUser(queryParams);
		
		for(skin in skins){
			//banner = row;
						// name,  title, 	image
			tableData.add([skin[1],skin[2], skin[3], skin[0]]);
		} // End of foreach in skins
		
		jsonMap.put(Constants.DATA_TABLES_RECORDS_TOTAL, totalCount);
		jsonMap.put(Constants.DATA_TABLES_RECORDS_FILTERED, filteredCount);
		jsonMap.put(Constants.DATA_TABLES_DATA, tableData);
		
		return jsonMap;
	} // End of getAppSkinList method
	
	/**
	 * Returns a HashMap with the required information to render a part of the
	 * welcomeAd table depending of parameters
	 *
	 * @param queryParams	Parameters to execute the query (start, length, searchText, orderDir)
	 * @param columnOrder	Number of the column to order the table
	 * @return				HashMap with required information
	 * @throws Exception	When somethings goes wrong
	 */

	private HashMap<String, Object> getWelcomeAdList(HashMap queryParams, int columnOrder) throws Exception {
		
		String columnOrderName = "";
		switch(columnOrder){
			case 1:
				columnOrderName = "welcomeType";
				break;
			default:
				columnOrderName = "name";
		} // End of switch: columnOrder
		
		queryParams.put(Constants.DATA_TABLES_COLUMN_ORDER_NAME, columnOrderName);
		
		HashMap jsonMap = new HashMap();
		def tableData = [];
		//Content banner = null;
		def (welcomeList, totalCount, filteredCount)  = welcomeAdService.getWelcomeAdsOfUser(queryParams);
		
		for(ad in welcomeList){
			//banner = row;
						// name,  title, image
			tableData.add([ad[1],ad[2].name(), ad[3], ad[0], ad[4]]);
		} // End of foreach in list
		
		jsonMap.put(Constants.DATA_TABLES_RECORDS_TOTAL, totalCount);
		jsonMap.put(Constants.DATA_TABLES_RECORDS_FILTERED, filteredCount);
		jsonMap.put(Constants.DATA_TABLES_DATA, tableData);
		
		return jsonMap;
	} // End of getWelcomeAdList method
	
	
	
	/**
	 * Returns a HashMap with the required information to render a part of the
	 * Banners table depending of parameters
	 *
	 * @param queryParams	Parameters to execute the query (start, length, searchText, orderDir)
	 * @param columnOrder	Number of the column to order the table
	 * @return				HashMap with required information
	 * @throws Exception	When somethings goes wrong
	 */

	private HashMap<String, Object> getOffersList(HashMap queryParams, int columnOrder) throws Exception {
		
		String columnOrderName = "";
		switch(columnOrder){
			case 1:
				columnOrderName = "enabled";
				break;
			default:
				columnOrderName = "title";
		} // End of switch: columnOrder
		
		queryParams.put(Constants.DATA_TABLES_COLUMN_ORDER_NAME, columnOrderName);
		
		HashMap jsonMap = new HashMap();
		def tableData = [];
		//Content offer = null;
		def (offers, totalCount, filteredCount)  = contentService.getOffersOfUser(queryParams);
		
		for(offer in offers){			
						// title,  dialogImage, enabled
			tableData.add([offer[1],offer[2], offer[3], offer[0]]);
		} // End of foreach in companies
		
		jsonMap.put(Constants.DATA_TABLES_RECORDS_TOTAL, totalCount);
		jsonMap.put(Constants.DATA_TABLES_RECORDS_FILTERED, filteredCount);
		jsonMap.put(Constants.DATA_TABLES_DATA, tableData);
		
		return jsonMap;
	} // End of getOffersList method

    /**
     * Returns a HashMap with the required information to render a part of the Documents table 
     * depending of parameters
     *
     * @param queryParams
     *                     Parameters to execute the query (start, length, searchText, orderDir)
     * @param columnOrder
     *                     Number of the column to order the table
     * @return HashMap with required information
     * @throws Exception When something goes wrong
     */
	private HashMap<String, Object> getDocumentsList(HashMap queryParams, int columnOrder) throws Exception {
        String columnOrderName = "";
        switch(columnOrder) {
            case 0:
                columnOrderName = Constants.NAME;
                break;
            case 1:
                columnOrderName = Constants.TYPE;
                break;
            default:
                columnOrderName = Constants.NAME;
                break;
        }		
		queryParams.put(Constants.DATA_TABLES_COLUMN_ORDER_NAME, columnOrderName);
		HashMap jsonMap = new HashMap();
		def tableData = [];
		def (documents, totalCount, filteredCount)  = documentService.getDocumentsOfUser(queryParams);
		for (document in documents) {
			// name, id, category name, expiration date
            tableData.add([document[1], document[2].name, document[0], DateUtils.formatDate(document[3]) ]);
		}
		jsonMap.put(Constants.DATA_TABLES_RECORDS_TOTAL, totalCount);
		jsonMap.put(Constants.DATA_TABLES_RECORDS_FILTERED, filteredCount);
		jsonMap.put(Constants.DATA_TABLES_DATA, tableData);		
		return jsonMap;
	}

	
	private HashMap<String, Object> getExxtractorsList(HashMap queryParams, int columnOrder) throws Exception {
		String columnOrderName = "";
		switch(columnOrder){
			case 2:
				columnOrderName = "l.name";
			break;
			case 0:
				columnOrderName = "lastUpdated";
				break;
			default:
				columnOrderName = "serial";
		} // End of switch: columnOrder
		
		queryParams.put(Constants.DATA_TABLES_COLUMN_ORDER_NAME, columnOrderName);
		
		HashMap jsonMap = new HashMap();
		def tableData = [];
		Box box = null;
		def (boxes, totalCount, filteredCount)  = boxService.getExxtractorsOfUser(queryParams);
        final String format = messageSource.getMessage('default.text.messageFormat', null, LocaleContextHolder.locale);
		for(row in boxes){
			//box = row;
			//tableData.add([box.serial, box.name, box.location.name, box.connected()]);
            def (boolean isConnected, String lastConnection) = Box.checkConectivity(row[4], row[5], format);
			tableData.add([row[1], row[2], row[3], isConnected, row[0], lastConnection]);
		} // End of foreach in companies
		
		jsonMap.put(Constants.DATA_TABLES_RECORDS_TOTAL, totalCount);
		jsonMap.put(Constants.DATA_TABLES_RECORDS_FILTERED, filteredCount);
		jsonMap.put(Constants.DATA_TABLES_DATA, tableData);
		
		return jsonMap;
	} // End of getExxtractorsList method
	
	/**
	 * Returns a HashMap with the required information to render a part of the
	 * Employees table depending of parameters
	 *
	 * @param queryParams	Parameters to execute the query (start, length, searchText, orderDir)
	 * @param columnOrder	Number of the column to order the table
	 * @return				HashMap with required information
	 * @throws Exception	When somethings goes wrong
	 */
	private HashMap<String, Object> getEmployeesList(HashMap queryParams, int columnOrder) throws Exception {
		String columnOrderName = "";
		switch(columnOrder){
			case 0:
				columnOrderName = "u.enabled";
				break;
			case 1:
				columnOrderName = "u.firstName";
				break;
			case 2:
				columnOrderName = "u.email";
				break;
			default:
				columnOrderName = "u.enabled";
		} // End of switch: columnOrder
		
		queryParams.put(Constants.DATA_TABLES_COLUMN_ORDER_NAME, columnOrderName);
		
		HashMap jsonMap = new HashMap();
		def tableData = [];

		Employee employee = null;
		def (employees, totalCount, filteredCount)  = employeeService.getEmployeesOfUser(queryParams);
		
		for(row in employees){
			employee = row;
			tableData.add([employee.user.id, employee.user.firstName, employee.user.lastName, 
				employee.user.email, employee.user.enabled]);

		} // End of foreach in companies
		
		jsonMap.put(Constants.DATA_TABLES_RECORDS_TOTAL, totalCount);
		jsonMap.put(Constants.DATA_TABLES_RECORDS_FILTERED, filteredCount);
		jsonMap.put(Constants.DATA_TABLES_DATA, tableData);
		
		return jsonMap;

	} // End of getEmployeesList method
	
    private HashMap<String, Object> getExxtractorsListFull(HashMap queryParams, int columnOrder) throws Exception {
        String columnOrderName = "";
        def orderByColumns = ["serial", "name", "pa", "sv.versionSoftwareLabel", "lastUpdated"];
        if (columnOrder < orderByColumns.size()) {
            columnOrderName = orderByColumns[columnOrder];
        } else {
            columnOrderName = "serial";
        }
        queryParams.put(Constants.DATA_TABLES_COLUMN_ORDER_NAME, columnOrderName);
        HashMap jsonMap = new HashMap();
        def tableData = [];
        Box box = null;
        def (boxes, totalCount, filteredCount)  = boxService.getExxtractorsOfUser(queryParams);
        final String format = messageSource.getMessage('default.text.messageFormat', null, LocaleContextHolder.locale);
        for (row in boxes) {
            //['id', 'serial', '2: name', '3: l.name', '4: s.description', '5: lastUpdated', 6:isPa
            //, '7: versionSO', '8: sv.versionSoftwareLabel', '9: sv.versionSoftware', '10: model']
            def (boolean isConnected, String lastConnection) = Box.checkConectivity(row[4], row[5], format);
            final String isPAText = row[6]? 'default.field.yes':'default.field.no';
            String ver = messageSource.getMessage('default.field.empty', null, LocaleContextHolder.locale);
            if (row[8]) {
                ver = row[8].encodeAsHTML() + " v" + row[9].encodeAsHTML();
            } else if (StringUtils.isNotBlank(row[7])) {
                ver = "v" + row[7];
            }
            ver += " / " + (StringUtils.isNotBlank(row[10])? row[10]:'-');
            tableData.add([row[1] + " / " + row[3], row[2], 
                messageSource.getMessage(isPAText, null, LocaleContextHolder.locale), ver, lastConnection, isConnected, row[0]]);
        }
        jsonMap.put(Constants.DATA_TABLES_RECORDS_TOTAL, totalCount);
        jsonMap.put(Constants.DATA_TABLES_RECORDS_FILTERED, filteredCount);
        jsonMap.put(Constants.DATA_TABLES_DATA, tableData);
        return jsonMap;
    }
} // End of class
