package mycentralserver;

import java.security.InvalidParameterException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import grails.converters.JSON;
import mycentralserver.box.Box;
import mycentralserver.box.BoxSummaryData;
import mycentralserver.utils.Constants;
import mycentralserver.utils.DateUtils;
import mycentralserver.utils.FileHelper;
import mycentralserver.utils.beans.VenuesUsageData;
import mycentralserver.utils.enumeration.VenueServerEvent;
import mycentralserver.box.UsageEvent;
import mycentralserver.exception.UsageEventParsingException;

/**
 * Service class with some logic to handle the UsageEvent information
 * 
 * @author sdiaz
 * @since 11/12/2015
 */
class UsageEventService {

    /**
     * Injecting for retrieve some configuration values
     */
    def grailsApplication;

    /**
     * Service use for the validation of the current logged User
     */
    def userSessionService;

    /**
     * Auto injecting SessionFactory we can use it to get the current Hibernate session.
     */
    def sessionFactory;

    private final static String PARAM_LOCATION_ID = "locationId";
    private final static String PARAM_START_DATE = "startDate";
    private final static String PARAM_END_DATE = "endDate";

    /**
     * Expected field separator on the usage field records
     */
    private final static String DATA_SEPARATOR = "/";

    /**
     * This method will try to read and process a tar.gz file with the metrics data with the new definition
     *
     * @param fileName
     *         File name of the tar.gz file to process
     * @param txtFileName
     *         File name where to put the content
     * @param box
     *         Related box of the process
     * @throws UsageEventParsingException
     */
    public void readAndLoadFile(String fileName, String txtFileName, Box box) throws UsageEventParsingException {
        final String path = grailsApplication.config.basePathMyBoxMetricsFiles;
        final File file = new File(path + fileName);
        if (file.exists()) {
            FileHelper.decompressGzipFile(path + fileName, path + txtFileName);
            final File txtFile = new File(path + txtFileName);
            if (txtFile.exists()) {
                BufferedReader br = new BufferedReader(new FileReader(path + txtFileName));
                String line = null;
                List<String> errors = new ArrayList<String>();
                while ((line = br.readLine()) != null) {
                    try {
                        final String[] data = line.trim().split(UsageEventService.DATA_SEPARATOR);
                        if(data.length > 1) {
                            //Must at least contain the event type and serial number
                            final UsageEvent usageEvent = UsageEvent.getEvent(data, box);
                            if (!usageEvent.save()) {
                                throw new UsageEventParsingException("UsageEvent can't be saved to DB {" 
                                    + usageEvent.errors.allErrors.join('/') + "}");
                            }
                        } else {
                            throw new UsageEventParsingException("Invalid line");
                        }
                    } catch(UsageEventParsingException|Exception e) {
                        errors.add("[" + e.getMessage() + "] " + line);
                    }
                }
                br.close();
                processLoadResult(box, errors, file, txtFile);
                
            } else {
                throw new UsageEventParsingException("Decompressed file doesn't exists: " + path + txtFileName);
            }
        } else {
            throw new UsageEventParsingException("The file doesn't exists: " + path + fileName);
        }
    }

    /**
     * Retrieve the data required to render the usage report charts
     * 
     * @param params
     *          Request parameters object
     * @return Json string with all the required data for the generation of the charts
     */
    public String getChartsData(params) throws InvalidParameterException {
        // Get params information
        final long venueId = params[PARAM_LOCATION_ID]? params[PARAM_LOCATION_ID].toLong():0;
        // If must get the data from all Venues and it is not Help Desk or Admin must filter the data for
        // associated Venues
        def venues = (venueId == 0 && !userSessionService.isAdminOrHelpDesk())? 
                            userSessionService.getPermitedLocations():null;
        final Date startDate = DateUtils.isValidDateAtStartOfDay(params[PARAM_START_DATE]);
        final Date endDate = DateUtils.isValidDateAtEndOfDay(params[PARAM_END_DATE]);
        if (startDate && endDate) {
            VenuesUsageData venuesData = getUniqueAndStreamingChartData(venueId, startDate, endDate, venues);
            getDailyClientsDataOfOldRecords(venueId, startDate, endDate, venues, venuesData);
            getStreamingDataPerDayRecords(venueId, startDate, endDate, venues, venuesData)
            return venuesData.getCompleteJsonDataForCharts(startDate, endDate);
        } else {
            throw new InvalidParameterException("The entered dates must have the format MM/dd/YYYY");
        }
    }

    /**
     * Depending of the result of the read and load process executes the corresponding actions
     * 
     * @param box
     *          Venue Server that is processing the file
     * @param errors
     *          List of errors of the process
     * @param file
     *          File object of the tar.gz file
     * @param txtFile
     *          Extracted file object
     */
    private void processLoadResult(Box box, List<String> errors, File file, File txtFile) {
        txtFile.delete();
        if(errors.size() == 0) { // There was not errors
            file.delete();
        } else { // There was at least 1 error
            println("***** " + box.serial + " ERRORS *****");
            for (error in errors) {
                println error;
            }
            file.delete();
        }
    }

    /**
     * Return the records filtered by dates and Location of the BoxData table
     *
     * @param venueId
     *              Venue Id to filter; 0 when not filter must be applied
     * @param startDate
     *              Start Date with format MM/dd/yyyy
     * @param endDate
     *              End Date with format MM/dd/yyyy
     * @param venues
     *              List of allowed Venues for the current user in case that it's required for the filter
     * @param venuesData
     *              VenuesUsageData object with information from new table
     */
    private void getDailyClientsDataOfOldRecords(long venueId, Date startDate, Date endDate, def locations, 
            VenuesUsageData venuesData) {
        def dataResult = BoxSummaryData.listDailyClientsChartData(0, 0, Constants.DATE, Constants.SORT_ASC, "", venueId, 
                                startDate, endDate, locations).list();
        for (row in dataResult) {
            venuesData.setVenueUniqueClientsData(row[0], row[2], DateUtils.getChartsFormattedDate(row[1]), row[3], false);
        }
    }

    /**
     * Return the records filtered by dates and Venue from the BoxData table
     *
     * @param venueId
     *              Venue Id to filter; 0 when not filter must be applied
     * @param startDate
     *              Start Date with format MM/dd/yyyy
     * @param endDate
     *              End Date with format MM/dd/yyyy
     * @param venues
     *              List of allowed Venues for the current user in case that it's required for the filter
     * @param venuesData
     *              VenuesUsageData object with information from new table
     */
    private void getStreamingDataPerDayRecords(long venueId, Date startDate, Date endDate, def locations, 
            VenuesUsageData venuesData) {
        def dataResult = getStreamingDataPerDayFromDB(venueId, startDate, endDate, locations);
        for (row in dataResult) {
            venuesData.setVenueStreamingAndPeakData(row[0], row[1], DateUtils.getChartsFormattedDate(row[5]),
                row[2], row[3], false);
        }
    }

    
    /**
     * Gets from data base the unique clients streaming records of a Venue or All using the second data structure and
     * returns the object with the retrieved information already mapped
     * 
     * @param venueId
     *              Id of the venue to retrieve the data, if 0 returns the data for all allowed User venues
     * @param startDate
     *              Start date to filter the data
     * @param endDate
     *              End date to filter the date
     * @param venues
     *              List of allowed venues for the User, must be use when the venueId is 0
     * @return Object with the required already mapped
     */
    private VenuesUsageData getUniqueAndStreamingChartData(long venueId, Date startDate, Date endDate, def venues) {
        StringBuilder strBuilder = new StringBuilder(1024);
        strBuilder.append("SELECT ue.location_id as 'location', l.name as 'locName',");
        strBuilder.append(" COUNT(DISTINCT(ue.device_id)) as 'uniqueCounter',");
        strBuilder.append(" SUM(ue.event_duration) as 'streamingTime',");
        strBuilder.append(" DATE_FORMAT(CONVERT_TZ(FROM_UNIXTIME(ue.event_start),'Etc/UTC',l.timezone),'%m/%d') as 'locDate',");
        strBuilder.append(" MAX(current_streamings) as 'streamings'")
        strBuilder.append(" FROM usage_event ue");
        strBuilder.append(" INNER JOIN company_location l on ue.location_id=l.id");
        if(venueId != 0){
            strBuilder.append(" WHERE l.id = " + venueId);
            strBuilder.append(" AND CONVERT_TZ(FROM_UNIXTIME(ue.event_start),'Etc/UTC',l.timezone) ");
        } else {
            if (venues) {
                String locationsStr = "";
                for(loc in venues){
                    locationsStr += (locationsStr == "")? loc.id: "," + loc.id;
                }
                strBuilder.append(" WHERE l.id in (" + locationsStr + ")");
                strBuilder.append(" AND CONVERT_TZ(FROM_UNIXTIME(ue.event_start),'Etc/UTC',l.timezone) ");
            } else {
                strBuilder.append(" WHERE CONVERT_TZ(FROM_UNIXTIME(ue.event_start),'Etc/UTC',l.timezone) ");
            }
        }
        strBuilder.append(" BETWEEN :startDate AND :endDate");
        strBuilder.append(" GROUP BY ue.location_id, locDate");
        strBuilder.append(" ORDER BY locDate asc;");

        final results = executeQueryToCurrentSession(strBuilder.toString(), startDate, endDate);
        VenuesUsageData venuesData = new VenuesUsageData();
        for(row in results) {
            venuesData.setVenueDayData(row[0], row[1], row[4], row[2], row[3], row[5]);
        }
        return venuesData;
    }

    /**
     * Gets from data base the streaming records of a Venue or All using the first data structure
     * 
     * @param venueId
     *              Id of the venue to retrieve the data, if 0 returns the data for all allowed User venues
     * @param startDate
     *              Start date to filter the data
     * @param endDate
     *              End date to filter the date
     * @param venues
     *              List of allowed venues for the User, must be use when the venueId is 0
     * @return Results from Database with the required data
     */
    private def getStreamingDataPerDayFromDB(long venueId, Date startDate, Date endDate, def venues) {
        StringBuilder strBuilder = new StringBuilder(1024);
        strBuilder.append("SELECT l.id, l.name, ");
        strBuilder.append(" SUM(bd.time), MAX(bd.max_sessions), SUM(bd.total_sessions), ");
        strBuilder.append(" DATE_ADD(DATE_ADD(bd.date, INTERVAL bd.hour HOUR) , ");
        strBuilder.append("    INTERVAL TIMESTAMPDIFF(HOUR, UTC_TIMESTAMP(), CONVERT_TZ(UTC_TIMESTAMP(), 'Etc/UTC', l.timezone)) HOUR) as LocDate");
        strBuilder.append(" FROM box_data bd");
        strBuilder.append(" INNER JOIN company_location l ON bd.location_id=l.id ");
        if (venueId != 0) {
            strBuilder.append(" WHERE l.id = " + venueId);
        } else {
            if (venues) {
                String locationsStr = "";
                for (loc in venues) {
                    locationsStr += (locationsStr == "")? loc.id: "," + loc.id;
                }
                strBuilder.append(" WHERE l.id in (" + locationsStr + ")");
            }
        }
        strBuilder.append(" GROUP BY l.id, Date(LocDate)");
        strBuilder.append(" HAVING LocDate BETWEEN :startDate and :endDate");
        strBuilder.append(" ORDER BY LocDate asc");
        return executeQueryToCurrentSession(strBuilder.toString(), startDate, endDate);
    }

    /**
     * Executes a data usage query to the current hibernate session using the received paramters
     *  
     * @param query
     *              Query String to executes
     * @param startDate
     *              Start Date to use on the query
     * @param endDate
     *              End Date to use on the query
     * @return Results of the query execution
     */
    private executeQueryToCurrentSession(final String query, final Date startDate, final Date endDate) {
        // Create native SQL query using the current hibernate session
        final sqlQuery = sessionFactory.currentSession.createSQLQuery(query)
        // Use Groovy with() method to invoke multiple methods on the sqlQuery object.
        return sqlQuery.with {
            setString(PARAM_START_DATE, DateUtils.getMySqlFormattedDate(startDate))
            setString(PARAM_END_DATE, DateUtils.getMySqlFormattedDate(endDate))
            list()};
    }
}