/**
 * UserService.groovy
 */
package mycentralserver

import mycentralserver.company.CompanyLocation;
import mycentralserver.user.User;
import mycentralserver.utils.Constants;
import mycentralserver.utils.Utils;
import groovy.time.TimeCategory;

/**
 * Service in charge in most of the system User logic; including
 * some functions related with current session
 * 
 * @author Cecropia Solutions
 *
 */
class UserService {
	
	/* Inject services */
	def grailsApplication
			
	/** For new accounts the password expiration date 
	 *  is the same day.this is to change the first login.
	 * @return
	 */
    def getPassswordExpirationDateForNewAccount() {
		return (new Date()).clearTime();
    }
	
	/** this method return the password expiration date for 
	 *  account which is changing the password.
	 * @return
	 */
	def getPasswordExpirationDateForExistAccount() {
		def  days = grailsApplication.config.passwordValidDays
		def  passwordExpirationDate = null
		
		use(TimeCategory) {
			passwordExpirationDate = new Date() + days.days
		}
		
		return passwordExpirationDate.clearTime()
	}
	
	def isPasswordExpired(String email) {
		def user = User.findByEmail(email)
		return this.isPasswordExpired(user);		
	} // END isNotPasswordExpired
	
	def isPasswordExpired(User user) {
		if(user) {
			def dateNow = new Date()
			if(dateNow.clearTime() == user.passwordExpirationDate.clearTime()) {
				user.passwordExpired = true
				if(user.validate()) {
					user.save(flush: true, failOnError: true)
					return true
				}
			} // END if(dateNow == user.passwordExpirationDate)
			
			return false
		}// END if(user)
		
		return true
	}
}
