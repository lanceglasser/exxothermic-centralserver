/**
 * BoxUserService.groovy
 */
package mycentralserver

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.context.i18n.LocaleContextHolder;

import mycentralserver.app.AppSkin;
import mycentralserver.beans.BoxesSyncResult;
import mycentralserver.box.BoxUsers;
import mycentralserver.company.CompanyLocation;
import mycentralserver.user.User;
import mycentralserver.utils.Constants
import mycentralserver.utils.RoleEnum;
import mycentralserver.utils.Utils;

/**
 * Service with most of the Logic related with the 
 * ExXtractor Users
 */
class BoxUserService {

	/* Injection of required services */
	def restClientService
	def userSessionService;
	def grailsApplication
	def messageSource;
	
	private static final String ENABLED = "enabled";
	private static final String USER_TYPE_ADMIN = "admin";
	private static final String USER_TYPE_TECH = "tech";
	private static final String USER_TYPE_USER = "user";
	
	private static final String USER_ACTION_CREATE = "create";
	private static final String USER_ACTION_UPDATE = "update";
	private static final String USER_ACTION_DELETE = "delete";
	
	/**
	 * Update the boxes of a Location with all users or a single 1
	 * 
	 * @param location	Location to update
	 * @param boxUsers	If single update must be different than null
	 * @return			Result of the Update
	 */
	def updateBoxesOfLocations(CompanyLocation location, BoxUsers boxUsers ){
		
		BoxesSyncResult boxesSyncResult = new BoxesSyncResult("App Settings");
		
		HashMap<String, ArrayList<BoxUsers>> hashData = new HashMap<String, ArrayList<BoxUsers>>()

		for(box in location.boxes){
			def usersList = new ArrayList<BoxUsers>();
			if(boxUsers != null){//Update only 1 user
				usersList.add(boxUsers);
			} else { //Update all users
				usersList.addAll(location.boxUsers);
			}
			hashData.put(box.serial, usersList);
		}

		List<HashMap> listToRequest = convertHashToHasJsonToConvertJsonUtil( hashData );
		
		return restClientService.setBoxUsers(listToRequest);		
	} // END updateBoxesOfLocations
	
	/**
	 * Returns the HashMap to be Send to Communication by converting the Domain
	 * objects
	 * 
	 * @param hash
	 * @return
	 */
	public convertHashToHasJsonToConvertJsonUtil(HashMap<String, List<Object>> hash ) {
		List<Object> listContent;

		HashMap<HashMap> has = new HashMap<HashMap>()
		HashMap<String, Object> hasRequest = new HashMap<String, Object>()
		List<HashMap> listRequestSetContents = new ArrayList<HashMap>();

		for (String serialNumber : hash.keySet()) {
			hasRequest = new HashMap();
			listContent = hash.get(serialNumber);
			hasRequest.put("serialNumber", serialNumber);			
			def hashContent = convertListContentToListWithHashContentFields( listContent );
			hasRequest.put("users", hashContent);
			listRequestSetContents.add(hasRequest);
		}

		return listRequestSetContents;
	} // End convertHashToHasJsonToConvertJsonUtil
	
	/**
	 * This method receives a list of Domain objects and returs the hash map list
	 * 
	 * @param objectsList	Domain Objects List
	 * @return				HashMap List
	 */
	private convertListContentToListWithHashContentFields(List<BoxUsers> objectsList) {
		
		List<HashMap> hashList = new ArrayList<HashMap>()

		for (BoxUsers boxUser : objectsList) {
			hashList.add(getUserMap( boxUser ))
		}
		
		return hashList;
	}
	
    /**
	 * Creates the hash with the information of a User
	 * 
	 * @param boxUser	BoxUser with the information
	 * @return			Hash created
	 */
	public HashMap getUserMap( BoxUsers boxUser ){
		HashMap userHash = new HashMap();
		if(boxUser != null){
			userHash.put(Constants.PARAM_USERNAME, Utils.getStringValue(boxUser.username));
			userHash.put(Constants.PARAM_PASSWORD, Utils.getStringValue(boxUser.encryptedPassword));
			userHash.put(BoxUserService.ENABLED, boxUser.enable);
			userHash.put(Constants.PARAM_LAST_UPDATE, (boxUser.lastUpdated.getTime()/1000L));
			switch(boxUser.type){
				case Constants.BOX_USER_TYPE_TECH: 
					userHash.put(Constants.PARAM_LEVEL, BoxUserService.USER_TYPE_TECH);
					break;
				case Constants.BOX_USER_TYPE_ADMIN:
					userHash.put(Constants.PARAM_LEVEL, BoxUserService.USER_TYPE_ADMIN);
					break;
				case Constants.BOX_USER_TYPE_USER:
					userHash.put(Constants.PARAM_LEVEL, BoxUserService.USER_TYPE_USER);
					break;
				default:
					userHash.put(Constants.PARAM_LEVEL, 0);
					break;
			}
		}		
		return userHash;
	}
	
	/**
	 * This method receives a serial number of a Box and returns the hash map
	 * with the users associated
	 * 
	 * @param location		Location associated with the Box
	 * @param serial		Serial Number of the Box
	 * @return				HashMap List with the Information
	 */
	public List<HashMap> getBoxUsersHashMap(CompanyLocation location, String serial){		
		def boxUsers = BoxUsers.findAllByLocation(location);
		def hashContent = convertListContentToListWithHashContentFields( boxUsers );
		return hashContent;
	}
	
	/**
	 * This method will execute an action to a BoxUser from the ExXtractor;
	 * depending of the action must create/update/delete the user.
	 *  
	 * @param location		Affected Location
	 * @param serial		Serial number of the ExXtractor executing the action
	 * @param username		Username of the User
	 * @param password		Encrypted password for the user
	 * @param level			Level of the User
	 * @param action		Action to execute
	 * @param lastUpdate	Long value with the last update datetime
	 * @return				
	 */
	public HashMap<String, Object> processUserActionFromBox(CompanyLocation location, String serial, String username, 
		String password, String level, String action, long lastUpdate){
		
		HashMap<String, Object> processResponse = new HashMap<String, Object>();
		
		switch(action){
			case BoxUserService.USER_ACTION_CREATE:
			case BoxUserService.USER_ACTION_UPDATE:
				def adminUser = User.findByEmail(Constants.ADMIN_USER_EMAIL);
				//Check if the User exists
				BoxUsers user = BoxUsers.findByUsername(username);
				if( user == null){
					//The user dont exists, create the new entity
					user = new BoxUsers();
					user.username = username;
					user.location = location;
					user.createdBy = adminUser;
				}//If the User exists just need to update the values
				
				user.lastUpdatedBy = adminUser;
				user.type = BoxUsers.getTypeFromLevel(level);
				user.encryptedPassword = password;
				user.lastUpdated = new Date(lastUpdate*1000L);
				
				if(user.save(flush: true, failOnError: true)){
					processResponse.put('error', false);
					processResponse.put('msg', 'Success saving the info');
				} else {
					processResponse.put('error', true);
					processResponse.put('msg', 'Error saving the info');
				}
				break;
			case BoxUserService.USER_ACTION_DELETE:
				//Check if the User exists
				BoxUsers user = BoxUsers.findByUsername(username);
				if( user != null){
					if(user.delete()){
						processResponse.put('error', false);
						processResponse.put('msg', 'Success deleting the User');
					} else {
						processResponse.put('error', true);
						processResponse.put('msg', 'Error deleting the User');
					}
				} else {
					//The user dont exists, do nothing
					processResponse.put('error', false);
					processResponse.put('msg', 'Success deleting the User');
				}				
				break;
			default:
				break;
		}
		
		return processResponse;
	} 
		
	public syncronizeUsersByLocation(CompanyLocation location, flash){
		def syncResult = null;
		try{
			syncResult = updateBoxesOfLocations(location, null);
			if(syncResult != null){
				if(anyFailInSync(syncResult)){
					flash.error = "boxes.syncErrorMessage";
				}
			} else {
				flash.error = "general.no.communication.with.web.socket.api";
			}
		}catch(Exception ex){
			log.error("Error synchronizing Box Users",ex)
			flash.error = "general.no.communication.with.web.socket.api";
		}
		return syncResult;
	}
	
	public boolean anyFailInSync(syncResponse){
		def responseList = (syncResponse.usersStatus)? syncResponse.usersStatus:syncResponse.data;
		for(int i=0; i < responseList.size(); i++){
			if(!responseList[i].successful){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Verify if the current user has rights over a Location
	 * 
	 * @param location	Location to verify	 * 
	 * @return			True when allows
	 */
	public boolean allowToExecuteAction(CompanyLocation location){
		return (userSessionService.isAdminOrHelpDesk() || userSessionService.userHasRole(RoleEnum.INTEGRATOR.value));
	} // End of allowToExecuteAction method
	
	/**
	 * Delete an ExXtractor User from Database and send the action to
	 * related ExXtractors of the Location
	 * 
	 * @param id			Id of the ExXtractor User to delete
	 * @param params		Request params; must has the Location Id
	 * @param flash			Flash object to set message with result
	 * @param session		User session to set the sync result
	 * @return				Result option, use it to render
	 */
	public int deleteBoxUser(long id, params, flash, session) {
		try{
			//Gets the Location by the Id
			CompanyLocation location = CompanyLocation.get( params.locationId.toLong() );
									
			if (!location || !location.company) {
				//If not exists the Location or the Location Company
				flash.error = messageSource.getMessage('default.location.not.found', null, LocaleContextHolder.locale);
				return 1;
			} else {
				if(allowToExecuteAction(location)){
					//Gets the User by the Id
					BoxUsers boxUser = BoxUsers.get(id);
											
					if ( !boxUser ) {
						//If not exists the User
						flash.error = messageSource.getMessage('default.not.found.message'. null, LocaleContextHolder.locale);
						return 5;
					} else {				
						//Deletes from Database
						def syncResult = sendBoxUserUpdate("delete", boxUser, boxUser.location, flash);
						session.boxUsersSyncResult = syncResult;
						if( !boxUser.delete() ) {
							updateLocationLastUpdateMoment(location);							
							java.lang.Object[] args = new java.lang.Object[1];
							args[0] = messageSource.getMessage('default.user.label', null, null);
							flash.success = messageSource.getMessage('general.object.delete.success.male', args, LocaleContextHolder.locale);
						} else {
							flash.error = messageSource.getMessage("general.object.delete.error", null, null);
						}
						return 4;
					}
				} else {
					//Only HelpDesk and Admin are allow
					flash.error = messageSource.getMessage('default.not.found.message', null, LocaleContextHolder.locale);
					return 1;
				}
			}			
		} catch(Exception ex) {
			log.error(ex);
			flash.error = ex.getMessage();
			return 1;
		}
	} // End of deleteBoxUser method
		
	/**
	 * This method allows the creation or edition of a ExXtractor User;
	 * the method will send the action to the related ExXtractors after success
	 * 
	 * @param params	Request params
	 * @param flash		Flash object to set the result messages
	 * @return			Json map with the information to return
	 */
	public HashMap<String, Object> createOrUpdateUser(params, flash, session){
		
		HashMap jsonMap = new HashMap();
		
		boolean error = false;
		String msg = "";
		def syncResult = null;
		try {
			
			//Gets the Location by the Id
			CompanyLocation location = CompanyLocation.get(params.locationId);
																		
			if (!location || !location.company) {
				//If not exists the Location or the Location Company				
				msg = messageSource.getMessage('default.location.not.found', null, LocaleContextHolder.locale);
				error = true;
			} else {
				if(allowToExecuteAction(location)){
					User user = userSessionService.getCurrentUser();
					if(params.uId.toInteger() == 0) {						
						(error, msg, syncResult) = createUser(params, flash, location, error, msg, user);						
					} else {					
						(error, msg, syncResult) = editUser(params, flash, location, error, msg, user);
					}
				} else {
					//Only HelpDesk and Admin are allow
					msg = messageSource.getMessage('default.not.found.message', null, LocaleContextHolder.locale);
					error = true;
				}
			}
		} catch(Exception ex) {
			error = true;
			msg = ex.getMessage();
			log.error(ex);
		}
		session.boxUsersSyncResult = syncResult;
		jsonMap['error'] = error;
		jsonMap['msg'] = msg;
		return jsonMap;
		
	} // End of createOrUpdateUser
	
	/**
	 * Create a new BoxUser with the information and try to send the create action
	 * to all related ExXtractors of the Location
	 *
	 * @param params		Request params
	 * @param flash			Flash objecto to set messages
	 * @param location		Related CompanyLocation
	 * @param error			Save if there is an error or not
	 * @param msg			Save the result msg
	 * @param user			BoxUsers been update
	 * @return				Result of the sync process
	 * @throws Exception	When something fail
	 */
	private def createUser(params, flash, CompanyLocation location, boolean error, String msg, User user) throws Exception {
		BoxUsers boxUser = BoxUsers.findByUsernameAndLocation(params.u, location);
		def syncResult = null;
		if(boxUser == null) {
			// Do not exists a user with that username by Location
			if(location.boxUsers.size() >= Constants.MAX_BOX_USERS_FOR_LOCATION) {
				java.lang.Object[] args = new java.lang.Object[1];
				args[0] = Constants.MAX_BOX_USERS_FOR_LOCATION;
				msg = messageSource.getMessage('max.number.of.box.users.error', args, LocaleContextHolder.locale);
				error = true;
			} else {				
				boxUser = new BoxUsers();
				boxUser.username = params.u;
				boxUser.type = params.t.toInteger();
				boxUser.encryptedPassword = BoxUsers.encryptPassword(params.p);
				boxUser.enable = (params.ena == "true");
				boxUser.location = location;
				boxUser.createdBy = user;
				boxUser.lastUpdatedBy = user;
				
				//Save the Entity to DB
				if(boxUser.save()){
					updateLocationLastUpdateMoment(location);
					flash.success = messageSource.getMessage('box.user.creation.success.msg', null, LocaleContextHolder.locale);
					syncResult = sendBoxUserUpdate("create", boxUser, location, flash);
				} else {
					msg = messageSource.getMessage('default.not.found.message', null, LocaleContextHolder.locale);
					error = true;
				}
			}
		} else {
			//Already exists that user for the Location
			error = true;
			msg = messageSource.getMessage('box.user.username.duplicate', null, LocaleContextHolder.locale);
		}
		return [error, msg, syncResult];
	}
	
	/**
	 * Update the information of a ExXtractor User and try to send the update action
	 * to all related ExXtractors of the Location
	 * 
	 * @param params		Request params
	 * @param flash			Flash objecto to set messages
	 * @param location		Related CompanyLocation
	 * @param error			Save if there is an error or not
	 * @param msg			Save the result msg
	 * @param user			BoxUsers been update
	 * @return				Result of the sync process
	 * @throws Exception	When something fail
	 */
	private def editUser(params, flash, CompanyLocation location, boolean error, String msg, User user) throws Exception {
		BoxUsers boxUser = BoxUsers.get(params.uId.toLong());
		def syncResult = null;
		if(boxUser != null){
			boxUser.enable = (params.ena == "true");
			boxUser.type = params.t.toInteger();
			boxUser.lastUpdatedBy = user;
			//Check if must update the password
			if(params.p.toString().trim() != ""){
				boxUser.encryptedPassword = BoxUsers.encryptPassword(params.p);
			}
			//Save the Entity to DB
			if(boxUser.save()){
				updateLocationLastUpdateMoment(location);
				flash.success = messageSource.getMessage('box.user.edition.success.msg', null, LocaleContextHolder.locale);
				syncResult = sendBoxUserUpdate("update", boxUser, location, flash);				
			} else {
				msg = messageSource.getMessage('default.not.found.message', null, LocaleContextHolder.locale);
				error = true;
			}
		} else {
			msg = messageSource.getMessage('default.not.found.message', null, LocaleContextHolder.locale);
			error = true;
		}
		
		return [error, msg, syncResult];
	}
	
	/**
	 * The Location needs to know the last moment when a user is created; updated or deleted.
	 * 
	 * @param location		CompanyLocation to be updated
	 * @throws Exception
	 */
	private void updateLocationLastUpdateMoment(CompanyLocation location) throws Exception {
		location.lastBoxUsersUpdate = new Date();
		if(!location.save()){
			throw new Exception("It was not possible to update the Location last box users update.");
		}
		
	}
		
	/**
	 * Creates the required HashMap and execute the call to the Api
	 * 
	 * @param action	Action of the Update: create/update/delete
	 * @param boxUser	BoxUser that is been update
	 * @param location	Location where to send the information
	 * @param flash		Flash objecto to set message with result
	 * @return			Required HashMap
	 */
	protected def sendBoxUserUpdate(String action, BoxUsers boxUser, CompanyLocation location, flash) {
		HashMap<String, Object> userMap = new HashMap<String, String>();
		userMap.put(Constants.PARAM_ACTION,action);
		userMap.put(Constants.PARAM_USERNAME,boxUser.username);
		userMap.put(Constants.PARAM_PASSWORD,boxUser.encryptedPassword);
		userMap.put(Constants.PARAM_LEVEL,boxUser.getLevelDescription());
		userMap.put(Constants.PARAM_LAST_UPDATE, (boxUser.lastUpdated.getTime()/1000L) );
		def serials = [];
		for(box in location.boxes){
			serials.add(box.serial);
		}
		HashMap<String, Object> jsonMap = new HashMap<String, String>();
		jsonMap.put("serials", serials);
		jsonMap.put("boxUser", userMap);
		
		def syncResult = restClientService.sendActionToApi(jsonMap,
			RestClientService.ApiAction.UPDATE_BOX_USER);
		
		if(syncResult != null){
			if(anyFailInSync(syncResult)){
				flash.warn = messageSource.getMessage("boxes.syncErrorMessage", null, LocaleContextHolder.locale);
			}
		} else {
			flash.error = messageSource.getMessage("general.no.communication.with.web.socket.api", null, LocaleContextHolder.locale);
		}
		
		return syncResult;
	}
}
