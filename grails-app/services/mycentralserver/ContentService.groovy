package mycentralserver

import java.awt.image.BufferedImage;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.imageio.ImageIO;

import org.springframework.context.i18n.LocaleContextHolder;

import mycentralserver.box.Box;
import mycentralserver.company.CompanyLocation;
import mycentralserver.content.Content;
import mycentralserver.user.User;
import mycentralserver.utils.CloudFilesPublish;
import mycentralserver.utils.Constants;
import mycentralserver.utils.FileHelper;
import mycentralserver.utils.SchedulerUtils;
import mycentralserver.utils.Utils;
import mycentralserver.utils.catalog.DomainsCatalog;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.codehaus.groovy.grails.web.json.JSONObject;

class ContentService {

    private static final List<String> DAYS_OF_WEEK = 
            Arrays.asList("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
    private static final String CURRENT_DATE = "currentDate";
    private static final String UTC_FORMAT = "utcFormat";
    private static final String TIME_ZONE = "timezone";
    private static final String START_DATE = "startDate";
    private static final String END_DATE = "endDate";
    private static final SimpleDateFormat SIMPLE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    private static final SimpleDateFormat HOURS_FORMAT = new SimpleDateFormat("hh:mm:ss a");
    private static final OFFER_CODE_VALID_LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final OFFER_CODE_VALID_NUMBERS = "0123456789";

    RestClientService restClientService
    def userSessionService
    def grailsApplication
    def messageSource;

    /**
     * Return the list of banners for the current user depending of role and the parameters
     *
     * @return	[banners, totalCount, filteredCount]; null when error
     */
    public def getBannersOfUser(HashMap<String, Object> queryParams) {
        try {
            def banners = new ArrayList();
            int totalCount = 0;
            int filteredCount = 0;
            if(userSessionService.isHelpDesk()) {
                totalCount = Content.listAllBannersForUITable(0, 0, 
                    queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
                    queryParams.get(Constants.DATA_TABLES_ORDER_DIR), "", false, null, true, false,
                    userSessionService.getCurrentSessionAffiliate(), null,
                    queryParams.get(Constants.LOCATION_ID),true).list()[0];
                filteredCount = Content.listAllBannersForUITable(0, 0, 
                    queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
                    queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH), 
                    false, null, true, false,userSessionService.getCurrentSessionAffiliate(), null, 
                    queryParams.get(Constants.LOCATION_ID), true).list()[0];
                banners = Content.listAllBannersForUITable(
                    queryParams.get(Constants.DATA_TABLES_START), queryParams.get(Constants.DATA_TABLES_LENGTH),
                    queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME) ,
                    queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH), 
                    false, null, true, false, userSessionService.getCurrentSessionAffiliate(), null, 
                    queryParams.get(Constants.LOCATION_ID),false).list();
            } else if(userSessionService.isAdmin()) {
                totalCount = Content.listAllBannersForUITable(0, 0, 
                    queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
                    queryParams.get(Constants.DATA_TABLES_ORDER_DIR), "", false, null, false, true, null, null, 
                    queryParams.get(Constants.LOCATION_ID),true).list()[0];
                filteredCount = Content.listAllBannersForUITable(0, 0, 
                    queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
                    queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH), 
                    false, null, false, true, null, null, queryParams.get(Constants.LOCATION_ID),true).list()[0];
                banners = Content.listAllBannersForUITable(
                    queryParams.get(Constants.DATA_TABLES_START), queryParams.get(Constants.DATA_TABLES_LENGTH) ,
                    queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME) ,
                    queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH), 
                    false, null, false, true, null, null, queryParams.get(Constants.LOCATION_ID),false).list();
            } else {
                User user = userSessionService.getCurrentUser();
                def listCompanies = userSessionService.getCompaniesForUser();
                totalCount = Content.listAllBannersForUITable(0, 0, 
                    queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
                    queryParams.get(Constants.DATA_TABLES_ORDER_DIR), "", false, user, false, false, null, 
                    listCompanies.collect(){it.id}, queryParams.get(Constants.LOCATION_ID),true).list()[0];
                filteredCount = Content.listAllBannersForUITable(0, 0, 
                    queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
                    queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH), 
                    false, user, false, false, null, listCompanies.collect(){it.id}, 
                    queryParams.get(Constants.LOCATION_ID),true).list()[0];
                banners = Content.listAllBannersForUITable(
                        queryParams.get(Constants.DATA_TABLES_START), queryParams.get(Constants.DATA_TABLES_LENGTH),
                        queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
                        queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH),
                        false, user, false, false, null, listCompanies.collect(){it.id}, 
                        queryParams.get(Constants.LOCATION_ID),false).list();
            }
            return [banners, totalCount, filteredCount]
        } catch(Exception e) {
            log.error("Error getting the list of banners of the user", e);
            return null;
        }
    }

    /**
     * Returns the list of offers for the current user depending of role and the parameters
     *
     * @return	[offers, totalCount, filteredCount]; null when error
     */
    public def getOffersOfUser(HashMap<String, Object> queryParams) {
        try {
            def offers = new ArrayList();
            int totalCount = 0;
            int filteredCount = 0;
            if(userSessionService.isHelpDesk()) {
                totalCount = Content.listAllOffersForUITable(0, 0,
                        queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
                        queryParams.get(Constants.DATA_TABLES_ORDER_DIR), "", false, null, true, false,
                        userSessionService.getCurrentSessionAffiliate(), null,
                        queryParams.get(Constants.LOCATION_ID),true).list()[0];
                filteredCount = Content.listAllOffersForUITable(0, 0,
                        queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
                        queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH),
                        false, null, true, false, userSessionService.getCurrentSessionAffiliate(), null,
                        queryParams.get(Constants.LOCATION_ID),true).list()[0];
                offers = Content.listAllOffersForUITable(
                        queryParams.get(Constants.DATA_TABLES_START), queryParams.get(Constants.DATA_TABLES_LENGTH) ,
                        queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME) ,
                        queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH),
                        false, null, true, false, userSessionService.getCurrentSessionAffiliate(), null,
                        queryParams.get(Constants.LOCATION_ID),false).list();
            } else if(userSessionService.isAdmin()) {
                totalCount = Content.listAllOffersForUITable(0, 0, queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
                        queryParams.get(Constants.DATA_TABLES_ORDER_DIR), "", false, null, false, true, null, null,
                        queryParams.get(Constants.LOCATION_ID),true).list()[0];
                filteredCount = Content.listAllOffersForUITable(0, 0,
                        queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
                        queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH),
                        false, null, false, true, null, null, queryParams.get(Constants.LOCATION_ID),true).list()[0];
                offers = Content.listAllOffersForUITable(
                        queryParams.get(Constants.DATA_TABLES_START), queryParams.get(Constants.DATA_TABLES_LENGTH) ,
                        queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME) ,
                        queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH),
                        false, null, false, true, null, null, queryParams.get(Constants.LOCATION_ID),false).list();
            } else {
                User user = userSessionService.getCurrentUser();
                def listCompanies = userSessionService.getCompaniesForUser();
                totalCount = Content.listAllOffersForUITable(0, 0, queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
                        queryParams.get(Constants.DATA_TABLES_ORDER_DIR), "", false, user, false, false, null,
                        listCompanies.collect(){it.id}, queryParams.get(Constants.LOCATION_ID),true).list()[0];
                filteredCount = Content.listAllOffersForUITable(0, 0, queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
                        queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH),
                        false, user, false, false, null, listCompanies.collect(){it.id},
                        queryParams.get(Constants.LOCATION_ID),true).list()[0];
                offers = Content.listAllOffersForUITable(
                        queryParams.get(Constants.DATA_TABLES_START), queryParams.get(Constants.DATA_TABLES_LENGTH),
                        queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME) ,
                        queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH),
                        false, user, false, false, null, listCompanies.collect(){it.id},
                        queryParams.get(Constants.LOCATION_ID),false).list();
            }
            return [offers, totalCount, filteredCount]
        } catch(Exception e) {
            log.error("Error getting the list of offers of the user", e);
            return null;
        }
    }

    /**
     * Generates a new code using configured lengths
     * This method do not checks if the code was already used
     *
     * @return String with the generated code
     */
    public String generateOfferCode() {
        final String text = RandomStringUtils.random(
            grailsApplication.config.offersCodesLetterLength, OFFER_CODE_VALID_LETTERS.toCharArray());
        final String number = RandomStringUtils.random(
            grailsApplication.config.offersCodesNumberLength, OFFER_CODE_VALID_NUMBERS.toCharArray());
        return (text + number);
    }

    /** 
     * Converts a Map with the contents of multiple servers to a List of Maps using the serial number as key and the
     * contents for that server as value
     * 
     * @param serversContents
     *              Map with the format [serial:List<CustomButtons>]
     * @return List of Maps using the serial number as the key and the list of contents as the value
     */
    public convertHashToHasJsonToConvertJsonUtil(HashMap<String, List<Object>> serversContents ) {
        List<Map> listRequestSetContents = new ArrayList<HashMap>();
        for (String serialNumber : serversContents.keySet()) {
            Map<String, Object> serverContents = new HashMap();
            serverContents.put(DomainsCatalog.CommonProperties.SimpleFields.SERIAL_NUMBER.fieldName(), serialNumber);
            serverContents.put(DomainsCatalog.CommonProperties.SimpleFields.CONTENTS.fieldName(), 
                serversContents.get(serialNumber));
            listRequestSetContents.add(serverContents);
        }
        return listRequestSetContents;
    }

    /**
     * Returns the list with id and title of all the contents related to a list of Venues
     * 
     * @param affectedLocations
     *          List of Venues to retrieved all the contents names
     * @return Map with the id as key and title as value
     */
    public Map<String, String> getContentNames(affectedLocations) {
        Map<String, String> contentsInfo = new HashMap<String, String>();
        for(location in affectedLocations) {
            for(content in location.contents) {
                contentsInfo.put(content.id.toString(), content.title);
            }
        }
        return  contentsInfo;
    }

    /**
     * Will try to send the list of valid banners to all the venue servers associated that belongs to the venues
     * received on the list
     * 
     * @param affectedLocations
     *          List of Venues that will try to synchronize
     * @return [result of the synchronization, Map with the list of boxes]
     */
    def updateBoxesOfLocations(affectedLocations) {
        HashMap<String, ArrayList<Content>> hashData = new HashMap<String, ArrayList<Content>>();
        HashMap<String, CompanyLocation> boxLocationData = new HashMap<String, CompanyLocation>();
        for(location in affectedLocations){
            // Search the list of valid banners for the Venue
            final def contentEnables = findContentEnables(location);
            for(box in location.boxes){
                def listContent = new ArrayList<Content>();
                final String versionLabel = box.softwareVersion?.versionSoftwareLabel;
                final float versionFloat = versionLabel?.isFloat()? new Float(versionLabel):0;
                if(versionFloat < Constants.MIN_TEXT_BANNER_SUPPORTED) {
                    // if text banners are not supported, remove text contents from the list
                    listContent.addAll(contentEnables.findAll {it.type  != Constants.CONTENT_TYPE_TEXT});
                } else {
                    listContent.addAll(contentEnables);
                }
                hashData.put(box.serial, listContent);
                boxLocationData.put(box.serial, box.location);
            }
        }
        List<HashMap> listContentToRequest = convertHashToHasJsonToConvertJsonUtil(hashData);
        return  [restClientService.setContents(listContentToRequest), boxLocationData]
    }

    /**
     * Will try to send the list of valid offers to all the venue servers associated that belongs to the venues
     * received on the list
     * 
     * @param affectedLocations
     *          List of Venues that will try to synchronize
     * @return result of the synchronization, Map with the list of boxes
     */
    public def updateOffersBoxesOfLocations(affectedLocations){
        Map<String, ArrayList<Content>> hashData = new HashMap<String, ArrayList<Content>>();
        for(location in affectedLocations){
            // Only search the Contents ones per Location since is the same for all the Boxes
            def contentEnables = findOffersEnables(location);
            for(box in location.boxes) {
                def listContent = new ArrayList<Content>();
                listContent.addAll(contentEnables);
                hashData.put(box.serial, listContent);
            }
        }
        final List<HashMap> listContentToRequest = convertHashToHasJsonToConvertJsonUtil(hashData);
        return restClientService.setOffers(listContentToRequest);
    }

    /**
     * This method tries to send the Contents to a List of Locations; will set the flash messages according to result
     *
     * @param affectedLocations
     *          List of Locations to be sync
     * @param flash
     *          Flash Object use to set messages after sync
     * @return Result of the Sync
     */
    public def syncContentsToLocations(def affectedLocations, flash){
        def jsonTransactionStatus;
        def boxLocations;
        (jsonTransactionStatus, boxLocations) = updateBoxesOfLocations(affectedLocations);
        if(jsonTransactionStatus) {
            if(anyFailInSync(jsonTransactionStatus)) { // At least 1 Box failed to Sync
                flash.warn = messageSource.getMessage('boxes.syncErrorMessage', null, null);
            } else {
                flash.info = messageSource.getMessage('sync.full.success', null, null);
            }
        } else { // There are not communication with the API, impossible to sync
            flash.error = messageSource.getMessage('general.no.communication.with.web.socket.api', null, null);
        }
        return [jsonTransactionStatus, boxLocations];
    }

    /**
     * This method checks the result per Box and returns true if at least 1 box failed
     *
     * @param syncResponse
     *             Sync response from Communication
     * @return At least 1 failed or not
     */
    public boolean anyFailInSync(syncResponse){
        for(int i=0; i < syncResponse.contentsStatus.size(); i++){
            if(!syncResponse.contentsStatus[i].successful){
                return true;
            }
        }
        return false;
    }

    /**
     * Returns a HTML UL list with all the associated errors of the Domain
     *
     * @param content
     *          Content domain to extract all the errors
     * @return HTML code as a string with all associated errors
     */
    public String getErrorsAsUl(Content content) {
        StringBuilder errorListBuilder = new StringBuilder();
        errorListBuilder.append("<ul>");
        content.errors.allErrors.each {
            errorListBuilder.append('<li data-field-id="' + it.field + '">');
            try {
                errorListBuilder.append(messageSource.getMessage(it, LocaleContextHolder.locale));
            } catch(Exception e) {
                errorListBuilder.append(it.codes[-1]);
            }
            errorListBuilder.append('</li>');
        }
        errorListBuilder.append("</ul>");
        return errorListBuilder.toString();
    }

    /**
     * Returns the list of available Banners for a Venue, if it's for a specific server the text banners can be removed
     * 
     * @param location
     *          Venue to retrieve the list of banners
     * @param box
     *          If it's for a specific server
     * @return List of available banners
     */
    public def findContentEnableAndConvertInHash(CompanyLocation location, Box box) {
        def contentEnables = findContentEnables(location);
        if(box == null) {
            return contentEnables;
        } else {
            final String versionLabel = box.softwareVersion?.versionSoftwareLabel;
            final float versionFloat = versionLabel?.isFloat()? new Float(versionLabel):0;
            // if text banner are not supported, remove text contents from the list
            return (versionFloat < Constants.MIN_TEXT_BANNER_SUPPORTED)?
                contentEnables.findAll {it.type  != Constants.CONTENT_TYPE_TEXT} : contentEnables;
        }
    }

    /**
     * Returns the list of available Offers for a Venue
     * 
     * @param location
     *          Venue to retrieve the list of banners
     * @return List of available offers
     */
    public def findOffersEnables(CompanyLocation location) {
         def contentEnabled = getAvailableContents(location, false);
         def listOffers = contentEnabled.findAll{ it.schedule?.endDate  != null };
         listOffers = listOffers.sort{ it.schedule.endDate  };
         listOffers.addAll(contentEnabled.findAll{ it.schedule?.endDate  == null });
         List<Object> validContents = new ArrayList<Object>();
         for(Content cb: listOffers) {
             def jsonContent = mustSendTheContent(cb, location, false);
             if(jsonContent) {
                 validContents.add(jsonContent);
             }
         }
         println "validContents: " + validContents;
         return validContents;
     }

    /**
     * Find all the banners associated to a Venue that must be sent to the servers at the moment
     *
     * @param location
     *          Venue Server to review for the list of banners
     * @return List with the available banners as a Json
     */
    private def findContentEnables(CompanyLocation location) {
        def contentEnabled = this.sortContentList(location, getAvailableContents(location, true));
        final int maxNumberOfContents = (location.contentsLimit > 0)? location.contentsLimit:0;
        List<Object> validContents = new ArrayList<Object>();
        for(Content cb: contentEnabled) {
            if(maxNumberOfContents > 0 && validContents.size() == maxNumberOfContents) {
                break;
            }
            def jsonContent = mustSendTheContent(cb, location, true);
            if(jsonContent) {
                validContents.add(jsonContent);
            }
        }
        return validContents;
    }

    private def sortContentList(CompanyLocation location, def contentList) {
        List<Content> validContent = new ArrayList<Content>();
        List<Content> remainingContents = new ArrayList<Content>();
        Content content;
        if(!contentList.isEmpty()) {
            // converts the jsonString to json object to extract and find the contents in order
            // not saved contents in order will be at end of the list!
            if(location.contentsOrder != null) {
                // Converts to a Map for easier review and retrieve from the defined sort order
                Map<String, Content> contentsMap = new HashMap<String, Content>();
                for(Content cb: contentList) {
                    contentsMap.put(cb.id.toString(), cb);
                }
                // Try to retrieve the total number of required contents using the sort order
                if ( StringUtils.isNotBlank(location.contentsOrder) && !location.contentsOrder.equals("{}")) {
                    final JSONObject json = new JSONObject(location.contentsOrder);
                    for (int i = 0; i < location.contentsLimit; i++) {
                        try {
                            final String contentId = json.getString(Integer.toString(i));
                            if (contentId) {
                                final Content cb = contentsMap.get(contentId);
                                if (cb) {
                                    validContent.add(cb);
                                    contentsMap.remove(contentId);
                                }
                            }
                        } catch(org.codehaus.groovy.grails.web.json.JSONException e) {
                            // The object is not found; just ignore the order
                        }
                    }
                }
                final Iterator<Map.Entry<String, Content>> it = contentsMap.entrySet().iterator();
                // If the list is incomplete try to add not ordered contents
                while (validContent.size() < location.contentsLimit && it.hasNext()) {
                    // While we need more contents and still exists on the Map
                    // Get the first entry that the iterator returns
                    final Map.Entry<String, Content> entry = it.next();
                    final Content cb = entry.getValue();
                    validContent.add(cb);
                    contentsMap.remove(cb.id);
                }
            } else {
                validContent = contentList;
            }
        }
        return validContent;
    }

    /**
     * Review and retrieved the required objects for the validation of the Contents to send to the servers by using the
     * TimeZone defined for the Venue and the current data
     *
     * @param location
     *          Venue to retrieve the dates objects
     * @return Map with the current date, the UTC format using the venue time zone and the associated time zone of venue
     */
    private Map<String, Object> getDateObjects(CompanyLocation location) {
        final String timezone = (location.timezone != null) ? 
                    location.timezone : DomainsCatalog.SchedulerProperties.CommonFields.DEFAULT_TIME_ZONE.fieldName();
        final DateFormat utcFormat = new SimpleDateFormat(DomainsCatalog.SchedulerProperties.CommonFields.UTC_FORMAT.fieldName());
        DateFormat formatTimeZone = new SimpleDateFormat(DomainsCatalog.SchedulerProperties.CommonFields.UTC_FORMAT.fieldName());
        formatTimeZone.setTimeZone(TimeZone.getTimeZone(timezone));
        // Gets the current date by timeZone || default UTC.
        final Date currentDate = (Date) utcFormat.parse(formatTimeZone.format(new Date()));
        DateFormat timeZoneID = new SimpleDateFormat("z");
        timeZoneID.setTimeZone(TimeZone.getTimeZone(timezone));
        Map<String, Object> dateObjs = new HashMap<String, Object>();
        dateObjs.put(CURRENT_DATE, currentDate);
        dateObjs.put(UTC_FORMAT, utcFormat);
        dateObjs.put(TIME_ZONE, timezone);
        return dateObjs;
    }

    /**
     * Checks if a Content should be send to the venue servers by doing several checks like days and hours, and others
     *
     * @param content
     *          Content to check
     * @param location
     *          CompanyLocation related to the Content
     * @return If must sent the object will be different than null
     */
    private mustSendTheContent(Content content, CompanyLocation location, final boolean isBanner) {
        Map<String, Object> dateObjs = getDateObjects(location);
        final Date currentDate = (Date) dateObjs.get(CURRENT_DATE);
        final DateFormat utcFormat = (DateFormat) dateObjs.get(UTC_FORMAT);
        final int today = DAYS_OF_WEEK.indexOf(new SimpleDateFormat("EEEE").format(currentDate));
        final String[] days = StringUtils.isNotBlank(content.schedule.days)?
                content.schedule.days.split(","):[today.toString()];
        if (Arrays.asList(days).contains(today.toString())){ //Should I display the AD today?
            final String[] hours = StringUtils.isNotBlank(content.schedule.hours)?
                                        content.schedule.hours.split("-"):
                                        [DomainsCatalog.SchedulerProperties.CommonFields.DEFAULT_START_HOUR.fieldName(),
                                         DomainsCatalog.SchedulerProperties.CommonFields.DEFAULT_DURATION.fieldName()];
            final Map<String, Object> results = isBetweenHoursSchedule(hours, currentDate, utcFormat);
            if (results) {
                return convertAnyContentToJson(content, results.get(START_DATE), results.get(END_DATE),
                    dateObjs.get(TIME_ZONE), currentDate,
                    [HOURS_FORMAT.format(results.get(START_DATE)), HOURS_FORMAT.format(results.get(END_DATE))], 
                    days, isBanner);
            }
        }
        return null;
    }

    /**
     * Use the received parameters to verify it a Content must be sent to a Server by checking the configured days, the
     * start hour and the time to display
     *
     * @param hours
     *          String array with the configured values for start hour and duration of the Content in format ["12:00 AM", "24"]
     * @param currentDate
     *          Current date using the time zone of the Venue
     * @param utcFormat
     *          UTC Format object to be use for the validation of the hours agains the current one
     * @return Map with the validated start and end hours, or null when the content is not between the hours
     */
    private Map<String, Object> isBetweenHoursSchedule(final String[] hours,
            final Date currentDate, final DateFormat utcFormat) {
        final int duration = Integer.parseInt(hours[1]);
        final String[] initialParts = hours[0].split(" ");
        final String[] hourParts = initialParts[0].split(":");
        int startHour = Integer.parseInt(hourParts[0]);
        if(initialParts[1].equals(DomainsCatalog.CommonProperties.SimpleFields.AM.fieldName())) {
            startHour = (startHour == 12)? 0:startHour;
        } else {
            startHour = (startHour == 12)? 12:startHour + 12;
        }
        Calendar cal = Calendar.getInstance(); // creates calendar
        cal.setTime(currentDate); // sets calendar time/date
        final int currentHourOfDay = cal.get(Calendar.HOUR_OF_DAY);
        final Date startDate = (Date) utcFormat.parse(SIMPLE_FORMAT.format(currentDate)
            + " " + startHour + ":" + hourParts[1] + ":00");
        cal.setTime(startDate); // sets calendar time/date
        if (startHour > currentHourOfDay) {
            cal.add(Calendar.HOUR_OF_DAY, -24); // Restarts a full day because may be allow from yesterday
            startDate = cal.getTime();
        }
        cal.add(Calendar.HOUR_OF_DAY, duration); // adds the duration in hours
        final Date endDate = cal.getTime();
        Map<String, Object> results = null;
        if (startDate.before(currentDate) && endDate.after(currentDate)) { //It's show time
            results = new HashMap<String, Object>();
            results.put(START_DATE, startDate);
            results.put(END_DATE, endDate);
        }
        return results;
    }

    /**
     * This method receives a Content and returns the Json expected by Communication depending if it's a banner of an offer
     *
     * @param cb
     *          Content to retrieve the Json
     * @param startDate
     *          Scheduler start date
     * @param endDate
     *          Scheduler end date
     * @param timezone
     *          Related time zone of the Venue that is been use
     * @param currentDate
     *          Current date depending of the Venue and the time zone
     * @param hours
     *          Scheduler hours
     * @param days
     *          Scheduler days
     * @param isBanner
     *          Define if the Content is a banner or not
     * @return JSONObject with all required data for the Content
     */
    private convertAnyContentToJson(Content cb, startDate, endDate, timezone, currentDate, hours, days, boolean isBanner){
        DateFormat timeZoneID = new SimpleDateFormat("z");
        timeZoneID.setTimeZone(TimeZone.getTimeZone(timezone));
        Map<String, Object> schedulerData = new HashMap<String, Object>();
        schedulerData.put(DomainsCatalog.SchedulerProperties.JsonFields.HOURS.fieldName(), hours);
        schedulerData.put(DomainsCatalog.SchedulerProperties.JsonFields.DAYS.fieldName(), days);
        schedulerData.put(DomainsCatalog.SchedulerProperties.JsonFields.EXPIRATION_DATE.fieldName(), 
            SchedulerUtils.strDateToUnixTimestamp(cb.schedule.endDate, timezone));
        schedulerData.put(DomainsCatalog.SchedulerProperties.JsonFields.TIMEZONE.fieldName(), 
            timeZoneID.format(currentDate));

        Map<String, Object> data = new HashMap<String, Object>();
        data.put(DomainsCatalog.CommonProperties.SimpleFields.ID.fieldName(), cb.id);
        data.put(DomainsCatalog.ContentProperties.JsonFields.DESCRIPTION.fieldName(), Utils.getStringValue(cb.description));
        data.put(DomainsCatalog.ContentProperties.JsonFields.DIALOG_IMAGE.fieldName(), Utils.getStringValue(cb.dialogImage));
        data.put(DomainsCatalog.ContentProperties.JsonFields.TABLET_DIALOG_IMAGE.fieldName(), 
            Utils.getStringValue(cb.dialogImageTablet));
        data.put(DomainsCatalog.ContentProperties.JsonFields.START_DATE.fieldName(), 
            SchedulerUtils.strDateToUnixTimestamp(startDate, timezone));
        data.put(DomainsCatalog.ContentProperties.JsonFields.END_DATE.fieldName(), 
            SchedulerUtils.strDateToUnixTimestamp(endDate, timezone));
        data.put(DomainsCatalog.ContentProperties.JsonFields.SCHEDULE.fieldName(), schedulerData);
        
        if(isBanner) {
            data.put(DomainsCatalog.ContentProperties.JsonFields.CHANNEL_LABEL.fieldName(), Utils.getStringValue(cb.title));
            data.put(DomainsCatalog.ContentProperties.JsonFields.CHANNEL_CONTENT.fieldName(), Utils.getStringValue(cb.url));
            data.put(DomainsCatalog.ContentProperties.JsonFields.CHANNEL_COLOR.fieldName(), 
                Utils.getStringValue(cb.color).replace("#", ""));
            data.put(DomainsCatalog.ContentProperties.JsonFields.TYPE.fieldName(), cb.type);
            data.put(DomainsCatalog.ContentProperties.JsonFields.IMAGE_URL.fieldName(), Utils.getStringValue(cb.featuredImage));
            data.put(DomainsCatalog.ContentProperties.JsonFields.TABLET_FEATURED_IMAGE.fieldName(), 
                Utils.getStringValue(cb.featuredImageTablet));
            data.put(DomainsCatalog.ContentProperties.JsonFields.FEATURED.fieldName(), cb.featured);
        } else {
            data.put(DomainsCatalog.ContentProperties.JsonFields.TITLE.fieldName(), Utils.getStringValue(cb.title));
            data.put(DomainsCatalog.ContentProperties.JsonFields.URL.fieldName(), Utils.getStringValue(cb.url));
            data.put(DomainsCatalog.ContentProperties.JsonFields.BACKGROUND_COLOR.fieldName(), 
                Utils.getStringValue(cb.color).replace("#", ""));
            data.put(DomainsCatalog.ContentProperties.JsonFields.TYPE.fieldName(), cb.referenceType);
        }
        JSONObject json = new JSONObject();
        json.putAll(data);
        return json;
    }

    /**
     * Gets the list of available contents of a Venue depending of the type and the verification of the schedule start
     * and final dates
     *
     * @param location
     *          Venue to retrieve the list of contents
     * @param isBanner
     *          Defined if the type of contents to retrieved are banners or not
     * @return The list of all the associated and available contents
     */
    private def getAvailableContents(final CompanyLocation location, final boolean isBanner) {
        final Map<String, Object> dateObjs = getDateObjects(location);
        final Date currentDate = (Date) dateObjs.get(CURRENT_DATE);
        return Content.withCriteria {
            eq(DomainsCatalog.CommonProperties.SimpleFields.ENABLED.fieldName(), true)
            if(isBanner) {
                or{
                    eq(DomainsCatalog.ContentProperties.JsonFields.TYPE.fieldName(), 
                        DomainsCatalog.ContentProperties.CommonFields.TYPE_IMAGE.fieldName())
                    eq(DomainsCatalog.ContentProperties.JsonFields.TYPE.fieldName(), 
                        DomainsCatalog.ContentProperties.CommonFields.TYPE_TEXT.fieldName())
                    and{
                        eq(DomainsCatalog.ContentProperties.JsonFields.TYPE.fieldName(), 
                            DomainsCatalog.ContentProperties.CommonFields.TYPE_OFFER.fieldName())
                        eq(DomainsCatalog.ContentProperties.JsonFields.FEATURED.fieldName(), true)
                    }
                }
            } else {
                eq(DomainsCatalog.ContentProperties.JsonFields.TYPE.fieldName(), 
                    DomainsCatalog.ContentProperties.CommonFields.TYPE_OFFER.fieldName())
            }
            isNotNull(DomainsCatalog.ContentProperties.JsonFields.SCHEDULE.fieldName())
            schedule {
                isNotNull(DomainsCatalog.ContentProperties.JsonFields.START_DATE.fieldName())
                le(DomainsCatalog.ContentProperties.JsonFields.START_DATE.fieldName(), currentDate)
                or {
                    isNull(DomainsCatalog.ContentProperties.JsonFields.END_DATE.fieldName())
                    ge(DomainsCatalog.ContentProperties.JsonFields.END_DATE.fieldName(), currentDate)
                }
            }
            locations {
                'in'(DomainsCatalog.CommonProperties.SimpleFields.ID.fieldName(), location.collect {loc ->
                    return loc.id;
                })
            }
        };
    }
}