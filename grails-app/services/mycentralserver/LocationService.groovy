/**
 * LocationService.groovy
 */
package mycentralserver;

import java.util.HashMap;

import mycentralserver.company.Company;
import mycentralserver.company.CompanyIntegrator;
import mycentralserver.company.CompanyLocation;
import mycentralserver.user.User;
import mycentralserver.utils.Constants;

import org.springframework.context.i18n.LocaleContextHolder;

/**
 * Service class with most of the Logic related with the
 * Locations
 * 
 * @author Cecropia Solutions
 */
class LocationService {

	/* Inject required services */
	def userSessionService;
	def imageUploadService;
	def rackspaceService;
	def appSkinService;
	def grailsApplication;
	def messageSource;
	
	/**
	 * Check if a CompanyLocation exists by Id and if the user has access
	 *
	 * @param id	Id of the Company to review
	 * @return
	 */
	private CompanyLocation checkAccess (long id) {
		
		CompanyLocation location = CompanyLocation.get(id);
		
		if(location && location.company) {
			
			if(userSessionService.isAdminOrHelpDesk()){
				return location;	
			} else {
				def allowedCompanies = getPermitedCompanies(false);
				
				if(allowedCompanies.any { it.id == location.company.id }){
					return location;
				}
			}
		} // End of exists Company
		
		return null;
		
	} // End of checkCompanyAccess method
	
	/**
	 * Use from Controller to check the access to the Location Dashboard
	 *
	 * @param id			Id of the Location to render
	 * @param flash			Flash object to set the error message
	 * @return				Return CompanyLocation object; null when error after set error to flash
	 * @throws Exception	When something crash
	 */
	public CompanyLocation renderDashboardPage(long id, flash) throws Exception {
		
		CompanyLocation location = checkAccess(id);
		
		if(location){
			return location;
		} else {
			flash.error = messageSource.getMessage('general.object.not.found', null, null);
			return null;
		}
		
	} // End of renderDashboardPage method
	
	/**
	 * Return the list of locations for the current
	 * user depending of role and the parameters
	 *
	 * @return	[locations, totalCount, filteredCount]; null when error
	 */
	public def getLocationsOfUser(HashMap<String, Object> queryParams) {
			
		try {
			def locations = new ArrayList();
			int totalCount = 0;
			int filteredCount = 0;
			
			if(userSessionService.isAdminOrHelpDesk()) {				
				totalCount = CompanyLocation.listAllLocationsForUITable(0, 0, queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), "", false, null, queryParams.get(Constants.COMPANY_ID), true).list()[0];
				filteredCount = CompanyLocation.listAllLocationsForUITable(0, 0, queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH), false, null, queryParams.get(Constants.COMPANY_ID), true).list()[0];
				locations = CompanyLocation.listAllLocationsForUITable(
					queryParams.get(Constants.DATA_TABLES_START), queryParams.get(Constants.DATA_TABLES_LENGTH) ,
					queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME) ,
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH), false, null, queryParams.get(Constants.COMPANY_ID), false).list();
			} else {
				User user = userSessionService.getCurrentUser();
				
				totalCount = CompanyLocation.listAllLocationsForUITable(0, 0, queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), "", false, user, queryParams.get(Constants.COMPANY_ID), true).list()[0];
				filteredCount = CompanyLocation.listAllLocationsForUITable(0, 0, queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH), false, user, queryParams.get(Constants.COMPANY_ID), true).list()[0];
				locations = CompanyLocation.listAllLocationsForUITable(
					queryParams.get(Constants.DATA_TABLES_START), queryParams.get(Constants.DATA_TABLES_LENGTH) , queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME) ,
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH), false, user, queryParams.get(Constants.COMPANY_ID), false).list();
			}
			
			return [locations, totalCount, filteredCount]
		} catch(Exception e) {
			log.error("Error getting the list of locations of the user", e);
			return null;
		} // End of catch
	} // End of getLocationsOfUser
	
	/**
	 * Return the list of CompanyLocation that the current User has access
	 * 
	 * @param creation	If the List is for Creation
	 * @return			List of Companies
	 */
	public def getPermitedCompanies(boolean creation) {
		
		def companies = null;
		
		try {
										
			if (userSessionService.isHelpDesk()){
				companies = Company.findAll(sort:Constants.NAME, order: Constants.SORT_ASC);
			}else{
				User user = userSessionService.getCurrentUser();
				def temp = null;
				if (creation){
					temp = Company.getEnabledCompanies(user);
					companies = temp.listDistinct();
				}else{
					temp = Company.getAllCompanies(user);
					companies = temp.listDistinct();
				}
								 
				def integrators = temp.typeIntegrator.listDistinct();
				for(integrator in integrators){
						integrator = CompanyIntegrator.findByCompany(integrator);
						companies.addAll(integrator.assignedCompanies.toList());
				}
			}
		} catch(Exception e) {
			log.error("Error getting the list of permited companies", e);
		}
		return companies.unique()
	} // End of getPermitedCompanies method
	
	/**
	 * Return a jsonMap with the information of a Location if exists
	 * @param parameters	Parameters must contains the Location id to find information
	 * @return				HashMap with the Location information when exists
	 */
	public HashMap<String, Object> getLocationInformation(HashMap<String, Object> parameters){
		HashMap<String, Object> jsonMap = new HashMap<String, Object>();
		CompanyLocation location = CompanyLocation.read(parameters.get(Constants.LOCATION_ID));
		if( location ) { // The CompanyLocation exists
			String defaultFieldEmpty = messageSource.getMessage("default.field.empty", null, null);
			jsonMap[Constants.LOCATION_ID] = location.id;
			jsonMap[Constants.NAME] = location.name;
			jsonMap[Constants.NAME+"2"] = location.name;
			jsonMap[Constants.COMPANY_ID] = location.company? location.company.id:0;
			jsonMap[Constants.COMPANY] = location.company? location.company.name : defaultFieldEmpty;
			jsonMap[Constants.COMMERCIAL_INTEGRATOR] = location.commercialIntegrator? location.commercialIntegrator?.companyName : defaultFieldEmpty;
			
			jsonMap[Constants.MAX_OCCUPANCY] = location.maxOccupancy;
			jsonMap[Constants.NUMBER_OF_TV] = location.numberOfTv;
			jsonMap[Constants.NUMBER_OF_TV_WITH_EXXOTHERMIC_DEVICE] = location.numberOfTvWithExxothermicDevice;
					
			jsonMap[Constants.CREATED_BY] = location.createdBy? (location.createdBy?.firstName + " "  + location.createdBy?.lastName) : defaultFieldEmpty;
			jsonMap[Constants.GPS_LOCATION] = "(" +
				(location.latitude? (location.latitude + " " + messageSource.getMessage("default.field.latitude", null, null)) : defaultFieldEmpty) + ", " +
				location.longitude + " " + messageSource.getMessage("default.field.longitude", null, null) + ")";
			jsonMap[Constants.LATITUDE] = location.latitude;
			jsonMap[Constants.LONGITUDE] = location.longitude;
			if(location.otherType){
				jsonMap[Constants.TYPE] = location.otherType;
			} else {
				jsonMap[Constants.TYPE] = location.type?.name? messageSource.getMessage("typeOfEstablishment." + location.type?.code, null, null) : defaultFieldEmpty;
			}
			jsonMap[Constants.USE_COMPANY_LOGO] = location.useCompanyLogo == true? messageSource.getMessage("default.field.yes", null, null):
				messageSource.getMessage("default.field.no", null, null);
				
			jsonMap[Constants.TIMEZONE] = location.timezone? location.timezone : defaultFieldEmpty;
			jsonMap[Constants.ENABLED] = location.enable == true? messageSource.getMessage("default.field.yes", null, null):
				messageSource.getMessage("default.field.no", null, null);
				
			String fullAddress = "";
			// Country
			fullAddress += (location.state? messageSource.getMessage("country.code." + location.state.country.shortName, null, null) :
				messageSource.getMessage("country.code." + location.country.shortName, null, null)) + ", ";
			// State
			fullAddress += location.state? messageSource.getMessage("state.code." + location.state.name, null, null) :
				( location.stateName? location.stateName : defaultFieldEmpty);
			// City
			fullAddress += ", " + (location.city? location.city : defaultFieldEmpty);
			// Address
			fullAddress += ", " + (location.address? location.address : defaultFieldEmpty);
			
			jsonMap[Constants.ADDRESS] = fullAddress;
			jsonMap[Constants.CONTENTS_LIMIT] = location.contentsLimit;
						
			if(location.useCompanyLogo){
				if(location.company?.logoUrl){
					jsonMap[Constants.HAS_LOGO] = true;
					jsonMap[Constants.LOGO] = location.company.logoUrl;
				} else {
					jsonMap[Constants.HAS_LOGO] = false;
				}
			} else {
				if(location.logoUrl){
					jsonMap[Constants.HAS_LOGO] = true;
					jsonMap[Constants.LOGO] = location.logoUrl;
				} else {
					jsonMap[Constants.HAS_LOGO] = false;
				}
			}
			jsonMap[Constants.MUST_USE_COMPANY_LOGO] = location.useCompanyLogo;
			String pocInfo = (location.pocFirstName? location.pocFirstName:"") + " ";
			pocInfo += (location.pocLastName? location.pocLastName + ", ":"");
			pocInfo += (location.pocEmail? location.pocEmail + ", ":"");
			pocInfo += (location.pocPhone? location.pocPhone:"");
			jsonMap[Constants.POC] = pocInfo;
		} else { // The Company do not exists
			jsonMap[Constants.LOCATION_ID] = 0;
		}
		return jsonMap;
	} // End of getLocationInformation method
	
	/**
	 * Return a jsonMap with the information of a Location if exists
	 * @param parameters	Parameters must contains the Location id to find information
	 * @return				HashMap with the Location information when exists
	 */
	public HashMap<String, Object> getLocationStyleInformation(HashMap<String, Object> parameters){
		HashMap<String, Object> jsonMap = new HashMap<String, Object>();
		CompanyLocation location = CompanyLocation.read(parameters.get(Constants.LOCATION_ID));
		if( location ) { // The CompanyLocation exists
			String defaultFieldEmpty = messageSource.getMessage("default.field.empty", null, LocaleContextHolder.getLocale());
			
			jsonMap[Constants.LOCATION_ID] = location.id;
			if( location.welcomeAd ){
				jsonMap[Constants.WNAME] = location.welcomeAd.name;
				jsonMap[Constants.SKIP_ENABLE] = location.welcomeAd.skipEnable ? messageSource.getMessage("welcome.ad.widget.skip.enable", null, LocaleContextHolder.getLocale()) : messageSource.getMessage("welcome.ad.widget.skip.enable", null, LocaleContextHolder.getLocale());
				jsonMap[Constants.SKIP_TIME] = messageSource.getMessage("welcome.ad.widget.skip.time", location.welcomeAd.skipTime as String[], LocaleContextHolder.getLocale());
				jsonMap[Constants.WELCOME_TYPE_CODE] = "" + location.welcomeAd.welcomeType;
				jsonMap[Constants.WELCOME_TYPE] = messageSource.getMessage("welcome.message.type." + location.welcomeAd.welcomeType, null, LocaleContextHolder.getLocale());
				jsonMap[Constants.SMALL_IMAGE_URL] = location.welcomeAd.smallImageUrl;
				jsonMap[Constants.VIDEO_URL] = location.welcomeAd.videoUrl;
			} else {
				jsonMap[Constants.WNAME] =  "";
			}
			if( location.activeSkin ){
				jsonMap[Constants.ANAME] = location.activeSkin.name;
				jsonMap[Constants.PRIMARY_COLOR] = location.activeSkin.primaryColor;
				jsonMap[Constants.SECONDARY_COLOR] = location.activeSkin.secondaryColor;
				jsonMap[Constants.TITLE] = location.activeSkin.title;
				jsonMap[Constants.BACKGROUND_IMAGE_URL] = location.activeSkin.backgroundImageUrl;
			}
			
		} else { // The Company do not exists
			jsonMap[Constants.LOCATION_ID] = 0;
		}
		return jsonMap;
	} // End of getLocationInformation method
	
	/**
	 * Upload the Location Logo from the Widget
	 *
	 * @param locationId	Location to be updated
	 * @param params		Request parameters
	 * @param request		Request object
	 * @param flash			Flash object
	 * @return				Url of the uploaded image
	 * @throws Exception	When something is wrong with the image or something crash
	 */
	public String uploadLogo(locationId, params, request, flash) throws Exception {
		
		CompanyLocation location = CompanyLocation.get(locationId);
		
		if(location){
			// Check and upload the logo Image
			def imagesUrlsToDelete = [];
			def deleteWhenFails = [];
			String fileId = "bgLogoUrl";
			String url = imageUploadService.uploadImageFile(messageSource.getMessage("company.logo", null, null), fileId,
				Constants.COMPANY_LOGO_WIDTH, Constants.COMPANY_LOGO_HEIGHT, params, request, flash);
			if(url != null){
				imagesUrlsToDelete.add(location.logoUrl);
				location.logoUrl = url;
				deleteWhenFails.add(url);
			}
			location.save();
			rackspaceService.deleteMultipleFiles(imagesUrlsToDelete);
			// Update the new Logo to the associated exxtractors
			def affectedLocations = [];
			affectedLocations.add(location);
			def boxesSyncResult = appSkinService.updateBoxesOfLocations(affectedLocations, location.activeSkin);
			return url;
		} else {
			throw new Exception("Company do not exists.");
		}
	} // End of uploadLogo method
} // End of Class
