package mycentralserver

import org.springframework.context.i18n.LocaleContextHolder;

/**
 * This class will contain the list of boxes that are doing the upgrade process in order to check if the upgrade is
 * completed or not
 *
 * @author sdiaz
 * @since 02/15/2016
 */
class SoftwareUpdateControlService {

    private Map<String, Object> controlList = new HashMap<String, Object>();
    
    private static final int STATUS_IN_PROGRESS = 1;
    private static final int STATUS_DONE = 2;
    private static final int STATUS_FAILED = 3;

    def messageSource; // Inject for translation messages

    /**
     * Add the detail of a upgrade process for a box to the list
     * 
     * @param serial
     *              Serial number of the box to save
     * @param json
     *              Json with the response of the upgrade request
     * @param errorCode
     *              Error code if exists of the upgrade request
     */
    public void addUpgradeControl(final String serial, final def json, final String errorCode, final String versionLabel,
            final int status) {
        cleanList();
        controlList.put(serial, [json:json, errorCode:errorCode, versionLabel:versionLabel, status:status, 
            startedAt:new Date()]);
    }

    /**
     * After a box connects tries to update the status of the control in the list if exists with the result of the
     * update process depending of the software version of the box
     * 
     * @param serial
     *              Serial number of the box that it's connecting
     * @param currentLabelVersion
     *              Software version label of the box when connects
     */
    public void changeControlAfterBoxConnect(final String serial, final String currentLabelVersion) {
        def control = controlList.get(serial);
        if (control) {
            if (control.versionLabel.equals(currentLabelVersion)) {
                control.status = STATUS_DONE;
                control.msg = messageSource.getMessage('default.message.success', null, LocaleContextHolder.locale);
            } else {
                control.status = STATUS_FAILED;
                java.lang.Object[] args = new java.lang.Object[3];
                args[0] = currentLabelVersion; 
                args[1] = control.versionLabel;
                control.msg = messageSource.getMessage('box.software.upgrade.control.fail.no.updated', args,
                    LocaleContextHolder.locale);
            }
            control.finishedAt = new Date();
            controlList.put(serial, control);
        }
    }

    /**
     * Returns the detail of the upgrade for a specific box by the serial
     * 
     * @param serial
     *          Serial number of box to retrieve the information
     * @return Array containing the json and error code of the process, or null if not exists
     */
    public def getUpgradeDetail(final String serial) {
        cleanList();
        return controlList.get(serial);
    }

    /**
     * Returns the detail of the upgrade for a specific box by the serial and remove it from the list if its completed
     *
     * @param serial
     *          Serial number of box to retrieve the information
     * @return Array containing the json and error code of the process, or null if not exists
     */
    public def getUpgradeDetailAndRemoveIfCompleted(final String serial) {
        def controlList = controlList.get(serial);
        if (controlList && controlList.status != STATUS_IN_PROGRESS) {
             removeUpgradeControl(serial);
        }
        return controlList;
    }

    /**
     * Remove from the list the detail of the process for an specific box by the serial
     * 
     * @param serial
     *          Serial number of the box to remove the detail if exists
     */
    public void removeUpgradeControl(final String serial) {
        controlList.remove(serial);
        cleanList();
    }

    /**
     * Reviews the list of controls and remove all the controls with more than 4 four in the list
     */
    private void cleanList() {
        Date validationDate = new Date();
        validationDate.setTime(validationDate.getTime() - (4*60*60*1000)); // Current Date minus 4 hours
        def iterator = controlList.entrySet().iterator();
        while (iterator.hasNext()) {
          if (iterator.next().getValue().startedAt.before(validationDate)) {
            iterator.remove();
          }
        }
    }

    /**
     * Generates a string with the information at the time of the control list just to be use as a reference on
     * development or testing
     * 
     * @return Information of the control list
     */
    public String printControList() {
        def iterator = controlList.entrySet().iterator();
        StringBuffer strBuffer = new StringBuffer(256);
        strBuffer.append("Software Upgrade Control List at: " + new Date() + "\n");
        while (iterator.hasNext()) {
          def nextItem = iterator.next();
          def control = nextItem.getValue();
          strBuffer.append(nextItem.getKey() + ": [startedAt: " + control.startedAt + ", errorCode: " + control.errorCode
              + ", status: " + control.status + "]\n");
        }
        return strBuffer.toString();
    }
}