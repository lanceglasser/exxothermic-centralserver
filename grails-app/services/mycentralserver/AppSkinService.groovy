/**
 * AppSkinService.groovy
 */
package mycentralserver;

import java.util.HashMap;

import mycentralserver.app.Affiliate
import mycentralserver.app.AppSkin;
import mycentralserver.beans.BoxesSyncResult;
import mycentralserver.box.Box;
import mycentralserver.box.BoxUnregistered;
import mycentralserver.company.CompanyLocation;
import mycentralserver.utils.Constants;
import mycentralserver.utils.PartnerEnum;
import mycentralserver.utils.SerialNumberGeneratorHelper;
import mycentralserver.utils.Utils;
import mycentralserver.user.User;

/**
 * Service class that contains must of the Logic related with the
 * AppSettings
 */
class AppSkinService {

	RestClientService restClientService
	def grailsApplication
	def userSessionService
	
	private static final String PRIMARY_COLOR = "primaryColor";
	private static final String SECONDARY_COLOR = "secondaryColor";
	private static final String CHANNEL_INFO_ENABLED = "channelInfoEnabled";
	
	private static final String TITLE = "title";
	private static final String LOGO_URL = "logoUrl";
	private static final String ID = "id";
	private static final String BACKGROUND_URL = "backgroundUrl";
	private static final String LARGE_BACKGROUND_URL = "tabletBackgroundUrl";
	private static final String DIALOG_IMAGE_URL = "dialogImageUrl";
	private static final String TABLET_DIALOG_IMAGE_URL = "tabletDialogImageUrl";
	private static final String APP_IOS_URL = "iosDownloadUrl";
	private static final String APP_ANDROID_URL = "androidDownloadUrl";
	private static final String PARTNER_ID = "partnerId";
	private static final String FACEBOOK_ID = "facebookId";
	
	/**
	 * Return the list of banners for the current
	 * user depending of role and the parameters
	 *
	 * @return	[banners, totalCount, filteredCount]; null when error
	 */
	public def getSkinsOfUser(HashMap<String, Object> queryParams) {
			
		try {
			def skins = new ArrayList();
			int totalCount = 0;
			int filteredCount = 0;
			
			if(userSessionService.isHelpDesk() || userSessionService.isAdmin()) {
				totalCount = AppSkin.listAllSkinsForUITable(0, 0, queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), "", null,true,false,null, queryParams.get(Constants.LOCATION_ID),true).list()[0];
				filteredCount = AppSkin.listAllSkinsForUITable(0, 0, queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH), null,true,false,null, queryParams.get(Constants.LOCATION_ID), true).list()[0];
				skins = AppSkin.listAllSkinsForUITable(
					queryParams.get(Constants.DATA_TABLES_START), queryParams.get(Constants.DATA_TABLES_LENGTH) ,
					queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME) ,
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH), null,true,false, null, queryParams.get(Constants.LOCATION_ID),false).list();
			}else {
				User user = userSessionService.getCurrentUser();
				def listCompanies = userSessionService.getCompaniesForUser();
				
				totalCount = AppSkin.listAllSkinsForUITable(0, 0, queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), "", user,
					false, false, listCompanies.collect(){it.id}, queryParams.get(Constants.LOCATION_ID), true).list()[0];
				filteredCount = AppSkin.listAllSkinsForUITable(0, 0, queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH), user,
					false, false, listCompanies.collect(){it.id}, queryParams.get(Constants.LOCATION_ID),true).list()[0];
				skins = AppSkin.listAllSkinsForUITable(
					queryParams.get(Constants.DATA_TABLES_START), queryParams.get(Constants.DATA_TABLES_LENGTH) , 
					queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME) ,
					queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH), user,
					false, false, listCompanies.collect(){it.id}, queryParams.get(Constants.LOCATION_ID),false).list();
			}
			
			return [skins, totalCount, filteredCount]
		} catch(Exception e) {
			log.error("Error getting the list of locations of the user", e);
			return null;
		} // End of catch
	} // End of getSkinsofUser
	
	
	
	
	/**
	 * Updates the skin on boxes that have this skin as active
	 * @param affectedLocations
	 * @param appSkin
	 * @return
	 */
	public BoxesSyncResult updateBoxesOfLocations(affectedLocations, AppSkin appSkin){
		
		log.debug("--> Update Boxes of Locations <--");
		HashMap appSkinHash;
		HashMap<String, Object> hashData = new HashMap<String, AppSkin>();
		HashMap<String, Integer> boxesLocations = new HashMap<String, Integer>();
		
		List<String> serials = new ArrayList();
		List<String> serialsToDefault = new ArrayList();
				
		//Get hash for skin
		appSkinHash = getAppSkinHashMap(appSkin);
		String logoUrl = "";
		for(location in affectedLocations){
			logoUrl = getLocationLogoUrl(location);
			if(location.activeSkin == null){//Must set to default
				for(box in location.boxes){
					serialsToDefault.add(box.serial + "," + location.id + getExtraInfo(box.serial) + "," + logoUrl
						+ "," + (location?.facebookId? location.facebookId.trim():""));
				}
			} else { //New Association
				for(box in location.boxes){
					serials.add(box.serial + "," + location.id + getExtraInfo(box.serial) + "," + logoUrl
						+ "," + (location?.facebookId? location.facebookId.trim():""));
				}
			}
		}
		
		List<String> responsesWithError = new ArrayList();
		def result = null;
		BoxesSyncResult boxesSyncResult = new BoxesSyncResult("App Settings");
		boolean failApiCall = false;
		//Update the new associated boxes
		if(!serials.isEmpty()){
			hashData.put("serials", serials);
			hashData.put(Constants.PARAM_SKIN, appSkinHash);
			result = restClientService.sendUpdateAction("updateActiveSkin", hashData);
			boxesSyncResult.addBoxResult(result);
			if(result == null){
				failApiCall = true;
			}
		}
		if( !failApiCall ){
			//Only sends if initial do not fail
			failApiCall = this.sendDefaultSkinToBoxes( serialsToDefault, boxesSyncResult );
		}
		
		return (failApiCall)? null:boxesSyncResult;
	} // END updateBoxesOfLocations
	
	/**
	 * This method will find an return the related information to the
	 * Affiliate by serial number
	 *
	 * @param serial	Serial number to find info
	 * @return			Extra info
	 */
	protected String getExtraInfo(String serial) {
		String info = "";
		String partner = SerialNumberGeneratorHelper.getPartnerCodeFromSerial(serial);
		
		Affiliate affiliate = Affiliate.findByCode(partner);
		if(affiliate.useDefaultPartnerInfo){
			//Must get the default Affiliate
			affiliate = Affiliate.findByCode(PartnerEnum.getDefaultPartnerCode());
		}
		
		info = "," + affiliate.id + "," + affiliate.iosAppUrl + "," + affiliate.androidAppUrl;
		
		return info;
	}
	
	/**
	 * Updates the skin on boxes to the default
	 * @param affectedLocations		Locations to be update
	 * @return						Result of the update to the boxes
	 */
	public BoxesSyncResult updateBoxesToDefaultOfLocations(affectedLocations){
		
		log.debug("--> Update Boxes to default of Locations <--");
				
		List<String> serialsToDefault = new ArrayList();
				
		String logoUrl = "";
		for(location in affectedLocations){
			logoUrl = "-";
			logoUrl = (location.logoUrl != null && location.logoUrl.trim() != null)?
						location.logoUrl:
							(location.company.logoUrl != null && location.company.logoUrl.trim() != null)?
								location.company.logoUrl:"";
			for(box in location.boxes){
				serialsToDefault.add(box.serial + "," + location.id + getExtraInfo(box.serial) + "," + logoUrl);
			}
		}
				
		BoxesSyncResult boxesSyncResult = new BoxesSyncResult("App Settings");
		boolean failApiCom = this.sendDefaultSkinToBoxes( serialsToDefault, boxesSyncResult );
		
		return (failApiCom)? null:boxesSyncResult; //If the Api failed returns null
	} // END updateBoxesOfLocations
	
	
	/**
	 * This method sends the default skin to a list of boxes by serials
	 */
	private boolean sendDefaultSkinToBoxes(List<String> serialsToDefault, BoxesSyncResult boxesSyncResult){
		//List<String> responsesWithError = new ArrayList();
		
		if(!serialsToDefault.isEmpty()){
			def result = null;
			String partner = "";
			HashMap<String, Object> defaultSkins = new HashMap<String, HashMap>();
			AppSkin defaultSkin = null;
			List<String> serialsInSkin = new ArrayList();
			for(serial in serialsToDefault){
				partner = SerialNumberGeneratorHelper.getPartnerCodeFromSerial(serial);
				HashMap<String, Object> item = defaultSkins.get(partner);
				if(item == null){
					//If not exists, retrieve the skin and put to the list
					serialsInSkin = new ArrayList();
					serialsInSkin.add(serial);
					HashMap<String, Object> data = new HashMap<String, HashMap>();
					data.put("skinHash", getAppSkinHashMap(AppSkin.findByName('default_' + partner)));
					data.put("serials",serialsInSkin);
					defaultSkins.put(partner, data);
				} else {
					serialsInSkin = item.get('serials');
					serialsInSkin.add(serial);
					item.put("serials",serialsInSkin);
					defaultSkins.put(partner, item);
				}
			}
			
			HashMap<String, Object> hashData = new HashMap<String, AppSkin>();
			for(ds in defaultSkins){
				hashData = new HashMap<String, AppSkin>();
				hashData.put("serials", ds.value.get('serials'));
				hashData.put(Constants.PARAM_SKIN, ds.value.get('skinHash'));
				
				result = restClientService.sendUpdateAction("updateActiveSkin",hashData);
				if(result == null) return true;
				boxesSyncResult.addBoxResult(result);
			}
		}
	}
		
	/**
	 * Creates the hash with the information of a AppSkin
	 *
	 * @param appSkin	AppSkin with the information
	 * @return			Hash created
	 */
	public HashMap getAppSkinHashMap(AppSkin appSkin){
		HashMap appSkinHash = new HashMap();
		if(appSkin != null){
			appSkinHash.put(AppSkinService.PRIMARY_COLOR, Utils.getStringValue(appSkin.primaryColor));
			appSkinHash.put(AppSkinService.SECONDARY_COLOR, Utils.getStringValue(appSkin.secondaryColor));
			appSkinHash.put(AppSkinService.CHANNEL_INFO_ENABLED, appSkin.channelInfoEnable);
			appSkinHash.put(AppSkinService.TITLE, Utils.getStringValue(appSkin.title));
			appSkinHash.put(AppSkinService.LOGO_URL, '');
			appSkinHash.put(AppSkinService.BACKGROUND_URL, Utils.getStringValue(appSkin.backgroundImageUrl));
			appSkinHash.put(AppSkinService.LARGE_BACKGROUND_URL, Utils.getStringValue(appSkin.backgroundImageUrlTablet));
			appSkinHash.put(AppSkinService.DIALOG_IMAGE_URL, Utils.getStringValue(appSkin.dialogImageUrl));
			appSkinHash.put(AppSkinService.TABLET_DIALOG_IMAGE_URL, Utils.getStringValue(appSkin.tabletDialogImageUrl));
		} else {
			appSkinHash.put(AppSkinService.PRIMARY_COLOR, '#000000');
			appSkinHash.put(AppSkinService.SECONDARY_COLOR, '#000000');
			appSkinHash.put(AppSkinService.CHANNEL_INFO_ENABLED, false);
			appSkinHash.put(AppSkinService.TITLE, 'App Audio');
			appSkinHash.put(AppSkinService.LOGO_URL, '');
			appSkinHash.put(AppSkinService.BACKGROUND_URL, '');
			appSkinHash.put(AppSkinService.LARGE_BACKGROUND_URL, '');
			appSkinHash.put(AppSkinService.DIALOG_IMAGE_URL, '');
			appSkinHash.put(AppSkinService.TABLET_DIALOG_IMAGE_URL, '');
		}
		
		return appSkinHash;
	}
	
	/**
	 * This method receives a serial number of a Box and returns the hash map
	 * with the app configuration associated
	 *
	 * @param location		Location associated with the Box
	 * @param serial		Serial Number of the Box
	 * @return				HashMap with the App Settings Information
	 */
	public HashMap getBoxAppSettingsHashMap(CompanyLocation location, String serial) {		
		return getAppSettingsHashMapByAppSkin(serial, location.activeSkin, location);		
	} // End of getBoxAppSettingsHashMap method
	
	
	/**
	 * This method receives a serial number of a Box and returns the hash map
	 * with the app configuration associated
	 *
	 * @param serial		Serial Number of the Box
	 * @return				HashMap with the App Settings Information
	 */
	public HashMap getBoxAppSettingsHashMapBySerial(String serial) {
		final Box box = Box.findBySerial(serial);
		return getAppSettingsHashMapByAppSkin(serial, box?.location?.activeSkin, box?.location);
	} // End of getBoxAppSettingsHashMapBySerial method
	
	/**
	 * Return the hash map of related AppSkin including the possibility to send
	 * the default one by serial and related Affiliate
	 * 
	 * @param serial	Serial Number of the ExXtractor
	 * @param appSkin	App Skin to map; when null will try to find the default
	 * @param location	Related Location of ExXtractor; could be null when unregister
	 * @return			HashMap with App Setting Information
	 */
	protected HashMap getAppSettingsHashMapByAppSkin( String serial, AppSkin appSkin, CompanyLocation location ) {
		// Get related Affiliate to the Serial Number
		final String partner = SerialNumberGeneratorHelper.getPartnerCodeFromSerial(serial);		
		final Affiliate affiliate = Affiliate.findByCode(partner);
		if (affiliate.useDefaultPartnerInfo) { // Must get the default Affiliate
			affiliate = Affiliate.findByCode(PartnerEnum.getDefaultPartnerCode());
		}		
		if (appSkin == null) {
			// Must find the default of the Box
			appSkin = AppSkin.findByName('default_' + partner);
			if (appSkin == null) {
				// Must find the Exxo Default
				appSkin = AppSkin.findByName('default_' + PartnerEnum.getDefaultPartnerCode());
				if (appSkin == null) {
					//There're not a default skin, will send harcoded information
					log.info("There is not a app skin associated with the box: " + serial + ", not even a default.");
				}
			}
		}
		
		HashMap hashMap = getAppSkinHashMap(appSkin);
		hashMap.put(AppSkinService.PARTNER_ID, ""+affiliate.id);
		hashMap.put(AppSkinService.APP_IOS_URL, ""+affiliate.iosAppUrl);
		hashMap.put(AppSkinService.APP_ANDROID_URL, ""+affiliate.androidAppUrl);		
		hashMap.put(AppSkinService.LOGO_URL, getLocationLogoUrl(location));
		hashMap.put(AppSkinService.ID, (location? ""+location.id:"0") );
		hashMap.put(AppSkinService.FACEBOOK_ID, location?.facebookId? location.facebookId.trim():"");
		return hashMap;
	} // End of getAppSettingsHashMapByAppSkin method
	
	/**
	 * This method return the related Logo Url of a Location
	 * depending of if the Logo is defined by Location, Company or not.
	 *
	 * @param 	location	CompanyLocation object to find the related logo
	 *
	 * @return				Url of related log; empty when is not defined
	 */
	private String getLocationLogoUrl(CompanyLocation location){
		
		String logoUrl = Constants.DEFAULT_COMPANY_LOGO_URL;
		if( location ){
			if(location.useCompanyLogo){
				logoUrl = (location.company.logoUrl != null && location.company.logoUrl.trim() != null)?
				location.company.logoUrl:
					(location.logoUrl != null && location.logoUrl.trim() != null)?
						location.logoUrl:Constants.DEFAULT_COMPANY_LOGO_URL;
			} else {
				//If must use the location logo; will try to get the location logo; if not found, returns company logo
				logoUrl = (location.logoUrl != null && location.logoUrl.trim() != null)?
				location.logoUrl:
					(location.company.logoUrl != null && location.company.logoUrl.trim() != null)?
						location.company.logoUrl:Constants.DEFAULT_COMPANY_LOGO_URL;
			}
		}
				
		return (logoUrl == null || logoUrl.trim() == "")? Constants.DEFAULT_COMPANY_LOGO_URL:logoUrl;
		
	} // End of getLocationLogoUrl method
	
} // End of Class