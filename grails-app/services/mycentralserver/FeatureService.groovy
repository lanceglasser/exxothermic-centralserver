package mycentralserver;

import mycentralserver.app.Affiliate;
import mycentralserver.box.Box;
import mycentralserver.box.software.FeatureDetail;
import mycentralserver.utils.SerialNumberGeneratorHelper;
import mycentralserver.utils.enumeration.Architecture;
import mycentralserver.utils.enumeration.Feature;

/**
 * Service class that handles some logic related with the Features domain including the validation of a Feature
 * for a Venue Server
 * 
 * @author sdiaz
 * @since 11/18/2015
 */
class FeatureService {

    /**
     * Validates if a feature is allowed for a venue server depending only of the version and architecture
     * 
     * @param box
     *              Venue Server to validate
     * @param feature
     *              Feature to validate
     * @return True or False depending if the feature is allowed or not for the venue server
     */
    public boolean isFeatureAllowedForBox(Box box, Feature feature) {
        final FeatureDetail featureDomain = FeatureDetail.findByCode(feature.getCode());
        if (featureDomain) {
            final Architecture arch = SerialNumberGeneratorHelper.getArchitectureFromSerial(box.serial);
            final boolean isVersionAllowed = (box.softwareVersion && 
                box.softwareVersion.versionSoftware >= featureDomain.minimumVersion);
            final boolean isArchitectureAllowed = featureDomain.architectures.contains("[" + arch.getValue() + "]");
            return (isVersionAllowed && isArchitectureAllowed);
        }
        return false;
    }
}