package mycentralserver

import java.net.URLDecoder;

import grails.plugins.rest.client.ErrorResponse;
import grails.plugins.rest.client.RestBuilder
import grails.plugins.rest.client.RestResponse
import groovy.json.JsonSlurper
import groovy.json.StreamingJsonBuilder
import mycentralserver.app.AppSkin
import mycentralserver.box.Box;
import mycentralserver.box.BoxStatus;
import mycentralserver.generaldomains.Configuration;
import mycentralserver.utils.BoxStatusEnum;
import mycentralserver.utils.Constants;
import mycentralserver.utils.Utils;

import org.apache.commons.lang.StringUtils;
import org.codehaus.groovy.grails.web.json.*;
import org.codehaus.groovy.grails.web.mapping.ResponseCodeMappingData;
import org.springframework.http.ResponseEntity

import grails.converters.JSON;


class RestClientService {
	
	def grailsApplication;
	def messageErrorService;
	def messageSource;

	public final static enum ApiAction {
		UPDATE_BOX_USER("updateBoxUser");
		
		String action;
		
		public ApiAction(String action){
			this.action = action;
		}
		public String toString(){
			return this.action;
		}
	}
	
    def setChannelStatus(String serial, int  channelNum, String channelLabel, boolean isPAChannel, String codecParms,
            String description, String imageURL, String largeImageURL, String name, String subTitle, int gain, boolean enable,
            int delay) {
        final String baseUrl = grailsApplication.config.restUrlComunication + "setChannelInfo";
        codecParms = StringUtils.isBlank(codecParms)? "Opus" : codecParms;
        def rest = new RestBuilder();
        final String data = "?serialNumber=" + serial +
                "&channelNum=" + channelNum +
                "&channelLabel=" + Utils.getStringValue(channelLabel) +
                "&isPAChannel=" + isPAChannel  +
                "&codecParms=" + codecParms +
                "&description=" + Utils.getStringValue(description) +
                "&imageURL=" + imageURL  +
                "&largeImageURL=" + largeImageURL+
                "&name=" + Utils.getStringValue(name)  +
                "&subTitle=" + Utils.getStringValue(subTitle) +
                "&gain=" + gain +
                "&enable=" + enable +
                "&delay=" + delay;

        try {
            log.debug(baseUrl+data);
            def resp = rest.get(baseUrl + data);
            resp as JSON;
            log.info("SetChannelStatus url: " + baseUrl + "  JSON Response: " + resp.json.toString());
            return resp.json.toString();
        } catch(Exception ex) {
            log.error(" SetChannelStatus from Central Server " +  ex);
            return null;
        }
    }

	def getChannelStatus(String serial)
	{
		def action  = "getChannelStatus?serialNumber=" + serial
		def baseUrl = grailsApplication.config.restUrlComunication
		baseUrl += action
		try
		{
			def rest = new RestBuilder()
			def resp = rest.get(baseUrl)
			resp as JSON
			log.info("GetChannelStatus url: " + baseUrl + "  JSON Response: " + resp.json.toString())
			return resp.json

		}
		catch(Exception ex)
		{
			log.error(" GetChannelStatus from Central Server " +  ex)
			return null
		}
	}

	/** This method call the setCustomButtons from the CentraServer WebSocket Api. The service return a Json
	 * 	 wit the status of every transaction.
	 *
	 * @param listRequestSetCustomButtons every object has this format [serialNumber:String, customButtons: [ {id:number, channelLabel:"String", channelColor:"String", channelContent:"String"} ]]
	 * @return Json Object with the response
	 */
	def setCustomButtons(List<HashMap> listRequestSetCustomButtons) {
		def action	= "setCustomButtons"
		def baseUrl = grailsApplication.config.restUrlComunication
		baseUrl += action

		RestBuilder  rest = new RestBuilder()

		try {

			RestResponse resp = rest.put( baseUrl ) 	{
				contentType "application/json"
				json listRequestSetCustomButtons as JSON
			}

			String json = resp.json.toString();
			//def responseEntity = resp.getResponseEntity()
			//def statusCode = responseEntity.statusCode

			if( checkStatusSuccessful(resp) ) {
				log.info("Status Sucessful");
			}

			def jsonResponse = JSON.parse(json)
			return jsonResponse
		}
		catch(Exception ex)	{
			return null
		}

		return null
	} // END setCustomButtons

	/** This method call the setCustomButtons from the CentraServer WebSocket Api. The service return a Json
	 * 	 wit the status of every transaction.
	 *
	 * @param listRequestSetCustomButtons every object has this format [serialNumber:String, customButtons: [ {id:number, channelLabel:"String", channelColor:"String", channelContent:"String"} ]]
	 * @return Json Object with the response
	 */
	def setContents(List<HashMap> listRequestSetContents) {
		def action	= "setContents"
		def baseUrl = grailsApplication.config.restUrlComunication
		baseUrl += action

		RestBuilder  rest = new RestBuilder()

		try {
			log.info( "RestClientService :: setContents :: listRequestSetContents -> " + listRequestSetContents);
			RestResponse resp = rest.put( baseUrl ) 	{
				contentType "application/json"
				json listRequestSetContents as JSON
			}

			String json = resp.json.toString();
			//def responseEntity = resp.getResponseEntity()
			//def statusCode = responseEntity.statusCode

			if( checkStatusSuccessful(resp) ) {
				log.info( "Status Sucessful");
			}

			def jsonResponse = JSON.parse(json)
			return jsonResponse
		}
		catch(Exception ex)	{
			log.error(" Error on RestClientService.setContent : " +  ex);
		}

		return null;
	} // END setContents
	
	/** This method call the setOffers from the CentraServer WebSocket Api. The service return a Json
	 * 	 wit the status of every transaction.
	 *
	 * @param listRequestSetCustomButtons every object has this format [serialNumber:String, customButtons: [ {id:number, channelLabel:"String", channelColor:"String", channelContent:"String"} ]]
	 * @return Json Object with the response
	 */
	def setOffers(List<HashMap> listRequestSetContents) {
		def action	= "setOffers"
		def baseUrl = grailsApplication.config.restUrlComunication
		baseUrl += action

		RestBuilder  rest = new RestBuilder()

		try {
			log.info( "RestClientService :: setOffers :: listRequestSetContents -> " + listRequestSetContents);
			RestResponse resp = rest.put( baseUrl ) 	{
				contentType "application/json"
				json listRequestSetContents as JSON
			}

			String json = resp.json.toString();
			
			if( checkStatusSuccessful(resp) ) {
				log.info( "Status Sucessful");
			}

			def jsonResponse = JSON.parse(json)
			return jsonResponse
		}
		catch(Exception ex)	{
			log.error(" Error on RestClientService.setOffers : " +  ex);
		}

		return null;
	} // END setOffers



		/** This method call the setDocuments from the CentraServer WebSocket Api. The service return a Json
	 * 	 wit the status of every transaction.
	 *
	 * @param listRequestSetDocuments every object has this format [serialNumber:String, documents: [ {id:number, name:"String", url:"String", expirationDate:Long} ]]
	 * @return Json Object with the response
	 */
	def setDocuments(List<HashMap> listRequestSetDocuments) {
		def action	= "setDocuments"
		def baseUrl = grailsApplication.config.restUrlComunication
		baseUrl += action

		RestBuilder  rest = new RestBuilder()

		try {
			RestResponse resp = rest.put( baseUrl ) {
				contentType "application/json"
				json listRequestSetDocuments as JSON
			}

			String json = resp.json.toString();
			return JSON.parse(json);
		}
		catch(Exception ex)	{
			log.error(" Error on RestClientService.setDocuments", ex)
			return null;
		}

		return null;
	} // END setDocuments

		private checkStatusSuccessful(resp) {

		if(resp instanceof RestResponse) {
			def responseEntity = resp.getResponseEntity()
			def statusCode = responseEntity.statusCode

			if(statusCode.value == 200) {
				return true
			}
		}

		return false;
	} // END checkStatusSuccessful
	private checkvalidateCertificateStatusSuccessful(resp) {

		if(resp instanceof RestResponse) {
			def responseEntity = resp.getResponseEntity()
			def statusCode = responseEntity.statusCode
			def body=responseEntity.body

			if(statusCode.value == 200 && body) {
				return true
			}
		}

		return false;
	} // END checkStatusSuccessful

	/** This method call the updateSoftwareVersion from the CentraServer WebSocket Api. The service return a Json
	 * 	 wit the status of every transaction.
	 *
	 * @param listRequestUpdateSoftwareVersion every object has this format [packageUri:String, checksum:string , serialNumbers: [ "string-serialNumber" ]]
	 * @return Json Object with the response
	 */
	def updateSoftwareVersion(List<HashMap> listRequestUpdateSoftwareVersion) throws Exception{

		def action	= "updateSoftwareVersion"
		def baseUrl = grailsApplication.config.restUrlComunication
		baseUrl += action

		RestBuilder  rest = new RestBuilder()
		def jsonResponse = null


		try {
			def resp = rest.put( baseUrl ) 	{
				contentType "application/json"
				json listRequestUpdateSoftwareVersion as JSON
			}

			String json = resp.json.toString();

			if( checkStatusSuccessful(resp) ) {
				jsonResponse = JSON.parse(json)
			} else {
				jsonResponse = null
			}

			return jsonResponse
		}
		catch(Exception ex)	{
			throw ex
		}

		return null
	} // END updateSoftwareVersion

	private getLogsList(String serial)
	{
		def action="getLogsList?serialNumber="+serial
		def baseUrl = grailsApplication.config.restUrlComunication
		baseUrl+=action
		def jsonResponse = null

		try
		{
			def rest = new RestBuilder()
			def resp = rest.get(baseUrl)
			resp as JSON
			log.info("getLogsList url: " + baseUrl + "  JSON Response: " + resp.json.toString())

			String json = resp.json.toString();

			if( checkStatusSuccessful(resp) ) {
				jsonResponse = JSON.parse(json)
			} else {
				jsonResponse = null
			}

			return jsonResponse

		}
		catch(Exception ex)
		{
			log.error(" getLogsList from Central Server " +  ex)
			return null
		}

		return null
	} // END getLogsList

	private deleteLogFile(String serial,String fileName)
	{
		def action="deleteLogFile?serialNumber="+serial+"&fileName="+ fileName
		def baseUrl = grailsApplication.config.restUrlComunication
		baseUrl+=action
		def jsonResponse = null

		try
		{
			def rest = new RestBuilder()
			log.info("deleteLog url: " + baseUrl + "  before calling restful service")
			def resp = rest.get(baseUrl)
			resp as JSON
			log.info("deleteLog url: " + baseUrl + "  JSON Response: " + resp.json.toString())

			String json = resp.json.toString();

			if( checkStatusSuccessful(resp) ) {
				jsonResponse = JSON.parse(json)
			} else {
				jsonResponse = null
			}

			return jsonResponse

		}
		catch(Exception ex)
		{
			log.error(" getLogsList from Central Server " +  ex)
			return null
		}

		return null
	} // END getLogsList

	private getFile(String serial,String fileName, String urlToUploadTheLogFile)
	{
		def action="getFile?serialNumber=" + serial +
				"&fileName=" + fileName  +
				"&url=" + urlToUploadTheLogFile

		def baseUrl = grailsApplication.config.restUrlComunication + action;

		def jsonResponse = null

		try
		{
			def rest = new RestBuilder()
			def resp = rest.get(baseUrl)
			resp as JSON
			log.info("getFile url: " + baseUrl + "  JSON Response: " + resp.json.toString())

			String json = resp.json.toString();

			if( checkStatusSuccessful(resp) ) {
				jsonResponse = JSON.parse(json)
			} else {
				jsonResponse = null
			}

			return jsonResponse

		}
		catch(Exception ex)
		{
			log.error(" getFile from Central Server " +  ex)
			return null
		}

		return null
	} // END getLogsList

	private generateCertificate(String serial,String hmac)
	{

		// "/RestService/boxes/{serial}/certificates"

		def action= "boxes/" + serial +"/certificates?" +
				"action=" + Constants.ACTION_GENERATE_CERTIFICATE

		def baseUrl = grailsApplication.config.restUrlComunication + action;

		def jsonResponse = null

		try
		{
			def rest = new RestBuilder()
			def resp = rest.put(baseUrl) { headers.'hmac' = hmac        	 }

			resp as JSON
			log.info("generateCertificate url: " + baseUrl + "  JSON Response: " + resp.json.toString())

			String json = resp.json.toString();

			if( checkStatusSuccessful(resp) ) {
				jsonResponse = JSON.parse(json)
			} else {
				jsonResponse = null
			}

			return jsonResponse

		}
		catch(Exception ex)
		{
			log.error(" generateCertificate from Central Server " +  ex)
			return null
		}

		return null
	} // END getLogsList

	/**
	 * This method generate a certificate for legacyBoxes and doesn't validate with hmac
	 * @param serial
	 * @return json Response
	 */
	private generateCertificateLegacyBox(String serial)
	{

		// "/RestService/boxes/{serial}/certificates"

		def action= "boxes/" + serial +"/certificatesLegacyBox?" +
				"action=" + Constants.ACTION_GENERATE_CERTIFICATE

		def baseUrl = grailsApplication.config.restUrlComunication + action;

		def jsonResponse = null

		try
		{
			def rest = new RestBuilder()
			def resp = rest.put(baseUrl)

			resp as JSON
			log.info("generateCertificateLegacyBox url: " + baseUrl + "  JSON Response: " + resp.json.toString())

			String json = resp.json.toString();

			if( checkStatusSuccessful(resp) ) {
				jsonResponse = JSON.parse(json)
			} else {
				jsonResponse = null
			}

			return jsonResponse

		}
		catch(Exception ex)
		{
			log.error(" generateCertificateLegacyBox from Central Server " +  ex)
			return null
		}

		return null
	} // END getLogsList

    /**
     * Validates the certificate of a box calling the Communication service
     * 
     * @param serial
     *                  Serial number of the box to validate
     * @param certificate
     *                  Certificate to validate
     * @param certVersion
     *                  Version of the certificate to use for the validation
     * @param certPath
     *                  Path on disk where to find the certificate
     * @return Response of the communication service
     */
    public validateCertificate(String serial,String certificate, String certVersion, String certPath) {
        certificate = URLEncoder.encode(certificate,  "UTF-8");
        final String action = "boxes/validateCertificate?serial=" + serial +"&certificate=" +certificate+
                "&version=" + certVersion+"&certPath=" + certPath;
        final String baseUrl = grailsApplication.config.restUrlComunication + action;
        try {
            RestBuilder rest = new RestBuilder();
            def resp = rest.get(baseUrl);
            resp as JSON;
            log.info("validateCertificate url: " + baseUrl + "  JSON Response: " + resp.json.toString());
            return resp;
        } catch(Exception ex) {
            log.error(" validateCertificate from Central Server " +  ex);
            return null;
        }
    }

	private setMyBoxPa(String serial,boolean isPA)
	{
		def action="setMyBoxPa?serialNumber="+serial+"&isPa="+isPA
		def baseUrl =grailsApplication.config.restUrlComunication
		baseUrl+=action
		try
		{
			def rest=new RestBuilder()
			def resp=rest.get(baseUrl)
			resp as JSON
			log.info("setMyBoxPa url: " + baseUrl + "  JSON Response: " + resp.json.toString())
			return resp.json.toString()

		}
		catch(Exception ex)
		{
			log.error(" setMyBoxPa from Central Server " +  ex)
			return null
		}
	}
	
	private setMyBoxInfo(String serial, boolean isPA, String jsonConfig)
	{
		String action="setMyBoxInfo?serialNumber="+serial+"&isPa="+isPA+"&config=";
        if(jsonConfig != null && jsonConfig.trim() != "") {
            action += jsonConfig;
        } else {
            action += "none:none";
        }
		def baseUrl =grailsApplication.config.restUrlComunication;
		baseUrl+=action
		println baseUrl;
		try
		{
			def rest=new RestBuilder()
			def resp=rest.get(baseUrl)
			resp as JSON
			log.info("setMyBoxInfo url: " + baseUrl + "  JSON Response: " + resp.json.toString())
			return resp.json.toString()

		}
		catch(Exception ex)
		{
			log.error(" setMyBoxInfo from Central Server " +  ex)
			return null
		}
	}

	def remoteReset(String serial) throws Exception {
		def action="remoteReset?serialNumber="+ serial;
		def baseUrl =grailsApplication.config.restUrlComunication;
		baseUrl+=action;
		RestBuilder rest = new RestBuilder();
		try {
			def resp = rest.get(baseUrl);
			resp as JSON;
			def responseRest = resp.json.toString();
			log.info(" url: " + baseUrl + "  JSON Response: " + responseRest);
			responseRest =JSON.parse(responseRest);			
			return responseRest;
		} catch(Exception ex){
			throw ex;
		}
	}

	def restoreData (String serial){
		def action="restoreData?serialNumber="+ serial
		def baseUrl =grailsApplication.config.restUrlComunication
		baseUrl+=action
		RestBuilder rest = new RestBuilder()
		try{

			def resp = rest.get(baseUrl)
			resp as JSON

			def responseRest = resp.json.toString()

			log.info(" url: " + baseUrl + "  JSON Response: " + responseRest)

			responseRest =JSON.parse(responseRest)

			return responseRest
		}catch(Exception ex){
			throw ex
		}
		return null
	}

	def resetFactory(String serial){
		def action="resetToFactorySettings?serialNumber="+serial
		def baseUrl =grailsApplication.config.restUrlComunication
		baseUrl+=action
		RestBuilder rest = new RestBuilder()
		try{

			def resp = rest.get(baseUrl)
			resp as JSON

			def responseRest = resp.json.toString()

			log.info(" url: " + baseUrl + "  JSON Response: " + responseRest)

			responseRest =JSON.parse(responseRest)

			return responseRest
		}catch(Exception ex){
			throw ex
		}
		return null
	}

	/**
	 * This method is used to send message for the update of boxes
	 * @param action	Action to be executed at the Communication Service
	 * @param hashData	Hash data with the information
	 * @return			Response from the Communication Service
	 */
	def sendUpdateAction(String action, HashMap<String, Object> hashData) {

		def baseUrl = grailsApplication.config.restUrlComunication + action;

		RestBuilder  rest = new RestBuilder()
		try {
			def resp = rest.put( baseUrl ) 	{
				contentType "application/json"
				json hashData as JSON
			}

			if(resp instanceof RestResponse){
				String json = resp.json.toString();
				log.debug("-> " + action +  " response: " + json);
				def jsonResponse = JSON.parse(json);
				return jsonResponse;
			} else if(resp instanceof ErrorResponse){
				log.error "-> Action: " + action + ", ErrorResponse: " + resp.getError().getMessage();
			}
		}
		catch(Exception ex)	{
			log.error (ex);
		}

		return null;
	} // END updateWelcomeAd

	/** This method call the setBoxUsers from the CentraServer WebSocket Api. The service return a Json
	 * 	 wit the status of every transaction.
	 *
	 * @param listRequestSet every object has this format
	 * @return Json Object with the response
	 */
	def setBoxUsers(List<HashMap> listRequestSet) {
		def action	= "setBoxUsers"
		def baseUrl = grailsApplication.config.restUrlComunication
		baseUrl += action

		RestBuilder  rest = new RestBuilder()

		try {

			RestResponse resp = rest.put( baseUrl ) 	{
				contentType "application/json"
				json listRequestSet as JSON
			}

			String json = resp.json.toString();
			return JSON.parse(json);
		}
		catch(Exception ex)	{
			log.error(" Error on RestClientService.setBoxUsers : " +  ex)
			return null
		}

		return null
	} // END setBoxUsers

	/**
	 * This method will send the request to the Communication component for
	 * the request of the metrics data file to a ExXtractor
	 * 
	 * @param serial					Serial Number of the ExXtractor
	 * @param fileName					FileName to be use for the upload of the file
	 * @param urlToUploadTheDataFile	Url when to upload the file
	 * @return							Response from Communication; Null when error.
	 */
	private getMetricsDataFile(String serial, String fileName, String urlToUploadTheDataFile) {
		def action="getMetricsFile?serialNumber=" + serial +
				"&fileName=" + fileName  +
				"&url=" + urlToUploadTheDataFile;

		def baseUrl = grailsApplication.config.restUrlComunication + action;

		def jsonResponse = null;

		try {
			def rest = new RestBuilder();
			def resp = rest.get(baseUrl);
			resp as JSON;
			
			log.info("getMetricsDataFile url: " + baseUrl + "  JSON Response: " + resp.json.toString())

			String json = resp.json.toString();

			if( checkStatusSuccessful(resp) ) {
				jsonResponse = JSON.parse(json)
			} else {
				jsonResponse = null
			}

			return jsonResponse

		} catch(Exception ex) {
			ex.printStackTrace();
			log.error("Error executing GetMetricsDataFile for Box: " + serial, ex);
			return null
		}
	} // End GetMetricsDataFile method
	
	/**
	 * This method execute a call to the Api by an action and sending the parameters
	 * as a json object 
	 * 
	 * @param requestMap	Map with parameters
	 * @param action		Action to execute
	 * @return				Json with response
	 */
	def sendActionToApi(def requestMap, RestClientService.ApiAction action) {
		
		def baseUrl = grailsApplication.config.restUrlComunication
		baseUrl += action;

		RestBuilder  rest = new RestBuilder()

		try {

			RestResponse resp = rest.put( baseUrl ) 	{
				contentType "application/json"
				json requestMap as JSON
			}

			String json = resp.json.toString();
			
			log.info("SendActionApi :: " + action + " :: Response = " + json);
			
			return JSON.parse(json);
		}
		catch(Exception ex)	{
			log.error(" Error on sendActionToApi with Action: " + action, ex);
		}

		return null
	} // End sendActionToApi method
	
	/**
	 * This method will try to send a request to a Server only with a 
	 * specific action and without parameters.
	 * 
	 * @param serial
	 * @param actionParam
	 * @return
	 */
	private sendActionRequestToServer(String serial, String actionParam) {
		
		String action="sendActionRequest?serialNumber=" + serial +
				"&action=" + actionParam;
		String baseUrl = grailsApplication.config.restUrlComunication + action;
		def jsonResponse = null;
		try {
			def rest = new RestBuilder();
			def resp = rest.get(baseUrl);
			resp as JSON;
			
			String json = resp.json.toString();
			if( checkStatusSuccessful(resp) ) {
				jsonResponse = JSON.parse(json)
			} else {
				jsonResponse = null;
			}

			return jsonResponse;

		} catch(Exception ex) {
			log.error("Error executing sendActionRequestToServer for Box: " + serial, ex);
			return null;
		}
	}
	
	/**
	 * This method will send an update of a Configuration to a server by his serial
	 * 
	 * @param serial	Serial Number of the ExXtractor
	 * @param config	Configuration object to update on servers
	 * @return
	 */
	private sendConfigurationUpdate(String serial, Configuration config) {
		def action="sendConfigurationUpdate?serialNumber=" + serial +
				"&configCode=" + config.code  +
				"&configValue=" + config.value;

		def baseUrl = grailsApplication.config.restUrlComunication + action;

		def jsonResponse = null;

		try {
			def rest = new RestBuilder();
			def resp = rest.get(baseUrl);
			resp as JSON;
			
			log.info("SendConfigurationUpdate url: " + baseUrl + "  JSON Response: " + resp.json.toString())

			String json = resp.json.toString();

			if( checkStatusSuccessful(resp) ) {
				jsonResponse = JSON.parse(json);
			} else {
				jsonResponse = null
			}

			return jsonResponse

		} catch(Exception ex) {
			ex.printStackTrace();
			log.error("Error executing SendConfigurationUpdate for Box: " + serial, ex);
			return null;
		}
	}
	
	/** 
	 * This method call the setDevices from the CentraServer WebSocket Api. 
	 * The service return a Json with the status of every transaction.
	 *
	 * @param requestMap 
	 * 				contains the list of serial numbers to update and the list of devices 
	 * 				to send to each one of them: 
	 * 				[serials:['serial','serial'], devices: [ {manufacturer:String, buildFingerprint:"String"} ]]
	 * @return Json Object with the response
	 */
	def sendListOfDevicesWithSmallerPackages(HashMap<String, Object> requestMap) {

		String baseUrl = grailsApplication.config.restUrlComunication + "sendDevices";

		RestBuilder rest = new RestBuilder();

		try {

			RestResponse resp = rest.put( baseUrl ) {
				contentType "application/json"
				json requestMap as JSON
			}

			String json = resp.json.toString();

			if( checkStatusSuccessful(resp) ) {
				log.info("setDevices: Status Sucessful");
			}

			def jsonResponse = JSON.parse(json);
			return jsonResponse;
		} catch(Exception ex) {
			log.error("Error sending the list of devices to the API", ex);
			return null
		}
	}
} // End class
