/**
 * EmployeeService.groovy
 */
package mycentralserver;

import java.util.HashMap;

import mycentralserver.user.Employee;
import mycentralserver.user.User;
import mycentralserver.utils.Constants;

/**
 * Service class with logic related with the Employees
 * 
 * @author Cecropia Solutions
 */
class EmployeeService {

	/* Inject required services */
	def userSessionService;
	
	/**
	 * Return the list of Employees for the current
	 * user depending of role and the parameters
	 *
	 * @return	[employees, totalCount, filteredCount]; null when error
	 */
	public def getEmployeesOfUser(HashMap<String, Object> queryParams) {
			
		try {
			def employees = new ArrayList();
			int totalCount = 0;
			int filteredCount = 0;
			
			User user = userSessionService.getCurrentUser();
				
			totalCount = Employee.listAllEmployeesForUITable(0, 0, queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
				queryParams.get(Constants.DATA_TABLES_ORDER_DIR), "", user, queryParams.get(Constants.COMPANY_ID)).count();
			filteredCount = Employee.listAllEmployeesForUITable(0, 0, queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME),
				queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH), user, queryParams.get(Constants.COMPANY_ID)).count();
			employees = Employee.listAllEmployeesForUITable(
				queryParams.get(Constants.DATA_TABLES_START), queryParams.get(Constants.DATA_TABLES_LENGTH) , queryParams.get(Constants.DATA_TABLES_COLUMN_ORDER_NAME) ,
				queryParams.get(Constants.DATA_TABLES_ORDER_DIR), queryParams.get(Constants.DATA_TABLES_SEARCH), user, queryParams.get(Constants.COMPANY_ID)).listDistinct();
			
			return [employees, totalCount, filteredCount]
		} catch(Exception e) {
			log.error("Error getting the list of employees of the user", e);
			return null;
		} // End of catch
	} // End of getEmployeesOfUser
} // End of Class
