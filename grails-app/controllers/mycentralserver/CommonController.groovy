package mycentralserver

import mycentralserver.generaldomains.Country;

import mycentralserver.generaldomains.State;
import mycentralserver.utils.MessageUtils;
import grails.converters.JSON

class CommonController {
	def springSecurityService
	   
    def statebycountry()
	{
		HashMap jsonMap = new HashMap()
		try
		{	
			def country=Country.read(params.id)
			def states=State.findAllByCountry(country, [readOnly: true])
			/*for (state in states){
				String messageString =   message(code:"state.code." + state.name)
				state.name = messageString
			}*/
			 			
			jsonMap.states=states.collect{
				sts -> return [
					id:sts.id,
					name:message(code:"state.code." + sts.name),
					country:sts.country.name,
					code:sts.name]
				}
		}
		catch(Exception MySQLSyntaxErrorException){
			jsonMap.error=[error:"Invalid Parameter"]
		}
		render jsonMap as JSON
	}
		
}
