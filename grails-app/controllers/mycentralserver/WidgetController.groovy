/**
 * WidgetController.groovy
 */
package mycentralserver;

import grails.converters.JSON;

/**
 * Controller that handles the request related with the Widgets
 * 
 * @author Cecropia Solutions
 *
 */
class WidgetController {

	/* Inject required services */
	def widgetService;
	
	/**
	 * Ajax request that retrieve the list of some
	 * objects by a param
	 * 
	 * @return
	 */
    def getListDataAjax() {
		HashMap jsonMap = new HashMap()
		try {
			jsonMap = widgetService.getListDataAjax(params);
			jsonMap.msg = "Success";
		} catch(Exception e) {
			log.error("Error getting the list of objects with params: " + params , e);
			jsonMap.error =  false;
			jsonMap.msg = "Unexpected error: " + e.getMessage();
		}

		render jsonMap as JSON;
	} // End of method getListDataAjax
	
	/**
	 * Ajax request that retrieve the list of some
	 * objects by a param
	 *
	 * @return
	 */
	def getDataAjax() {
		
		HashMap jsonMap = new HashMap()
		try {
			jsonMap = widgetService.getDetailedDataAjax(params);
			jsonMap.msg = "Success";
		} catch(Exception e) {
			log.error("Error getting the list of objects with params: " + params , e);
			jsonMap.error =  true;
			jsonMap.msg = "Unexpected error: " + e.getMessage();
		}
		
		render jsonMap as JSON;
	} // End of method getDataAjax
	
	
	/**
	 * Ajax request that receives the configuration of a dashboard to be
	 * save for the current user
	 * @return
	 */
	def saveDashboardConfigAjax() {
		HashMap jsonMap = new HashMap();
		try {
			jsonMap = widgetService.saveDashboardConfig(params);
			jsonMap.msg = "Success";
		} catch(Exception e) {
			log.error("Error saving the dashboard configuration with params: " + params , e);
			jsonMap.error =  false;
			jsonMap.msg = "Unexpected error: " + e.getMessage();
		}
		render jsonMap as JSON;
	} // End of saveDashboardConfigAjax method
	
	/**
	 * Receive the post request to upload a file
	 * @return
	 */
	def uploadFile() {
		HashMap jsonMap = new HashMap();
		try {
			jsonMap = widgetService.uploadFile(params, request, flash);
			jsonMap.msg = "Success";
			jsonMap.error = false;
		} catch(Exception e) {
			log.error("Error uploading a file with params: " + params , e);
			jsonMap.error =  true;
			jsonMap.msg = e.getMessage();
		}
		render jsonMap as JSON;
	} // End of uploadFile method
	
	def getWidgetHtml() {
		String template = "";
		switch(params.widgetId){
			case "companies-widget":
				template = "companiesListWidget";	break;
			case "locations-widget":
				template = "locationsListWidget";	break;
			case "banners-widget":
				template = "bannersListWidget";	break;
			case "appSkin-widget":
				template = "appSkinsListWidget";	break;
			case "welcomeList-widget":
				template = "welcomeListWidget";	break;
			case "offers-widget":
				template = "offersListWidget"; break;
			case "documents-widget":
				template = "documentsListWidget";	break;
			case "exxtractors-widget":
				template = "exxtractorsListWidget";	break;
			case "exxtractors-info-widget":
				template = "exxtractorsInfoWidget";	break;
			case "employees-widget":
				template = "employeesListWidget";	break;
			case "company-widget":
				template = "companyWidget";	break;
			case "location-widget":
				template = "locationWidget";	break;
			case "waAppTheme-widget":
				template = "waAppThemeWidget"; break;
			case "shortcuts-widget":
				template = "shortcutsWidget"; break;
		}
		render(template:'/widget/' + template,
			model:getWidgetModel(params.widgetId))
	} // End of getWidgetHtml method
	
	def getWidgetModel(widgetId){
		int sizeY = 9;
		switch(widgetId){
			case "shortcuts-widget":
			case "company-widget":
			case "location-widget":
				sizeY = 5; break;
				
			case "exxtractors-info-widget":
			case "waAppTheme-widget":
				sizeY = 7; break;
			
			case "companies-widget":
			case "documents-widget":
			case "locations-widget":
			case "appSkin-widget":
			case "welcomeList-widget":
			case "banners-widget":
			case "offers-widget":
			case "exxtractors-widget":
			case "employees-widget":
				break;
				
			default:
				sizeY = 9;
				break;
		}
		return [row:1, col:1, sizex:1, sizey:sizeY, status:'max',
			maxH:0, maxX:0, maxY:0, refreshStatus:0, refreshTime:0];
	}
} // End of Class
