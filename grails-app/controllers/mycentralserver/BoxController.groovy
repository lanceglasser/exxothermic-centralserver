package mycentralserver

import java.awt.GraphicsConfiguration.DefaultBufferCapabilities;
import java.text.SimpleDateFormat

import org.apache.catalina.Session;

import mycentralserver.box.Box
import mycentralserver.box.BoxBlackList;
import mycentralserver.box.BoxCertificate;
import mycentralserver.box.BoxIp;
import mycentralserver.box.BoxSummaryData;
import mycentralserver.box.BoxManufacturer;
import mycentralserver.box.BoxNotification;
import mycentralserver.box.BoxStatus
import mycentralserver.box.BoxChannel
import mycentralserver.company.Company
import mycentralserver.company.CompanyLocation
import mycentralserver.company.CompanyIntegrator
import mycentralserver.generaldomains.Configuration;
import mycentralserver.user.User
import mycentralserver.user.Role

import org.springframework.validation.FieldError
import org.springframework.web.client.ResourceAccessException
import org.springframework.web.servlet.FlashMap;
import grails.converters.JSON


import java.util.regex.Matcher
import java.util.regex.Pattern

import javassist.bytecode.stackmap.BasicBlock.Catch;
import grails.plugins.springsecurity.Secured;
import mycentralserver.RestClientService
import grails.plugins.rest.client.RestBuilder
import groovy.json.JsonSlurper
import groovy.json.StreamingJsonBuilder

import org.codehaus.groovy.grails.web.json.*

import grails.converters.JSON;
import mycentralserver.box.BoxUnregistered
import mycentralserver.box.BoxChannel
import mycentralserver.box.BoxEncryption
import mycentralserver.utils.ActionNames;
import mycentralserver.utils.BoxVersionsEnum;
import mycentralserver.utils.Constants;
import mycentralserver.utils.Utils;
import mycentralserver.utils.BoxStatusEnum;
import mycentralserver.utils.beans.BoxConfigurationUpdates;
import mycentralserver.utils.enumeration.BoxConfigurationCode;
import mycentralserver.utils.enumeration.Feature;
import mycentralserver.utils.enumeration.SystemConfiguration;

import java.util.Arrays;
import java.util.Date;

import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.apache.commons.lang.StringUtils;


class BoxController extends BaseControllerMyCentralServerController {

	//def springSecurityService
	RestClientService  restClientService
	ContentService contentService
	MessageErrorService messageErrorService
	BoxService boxService
	def boxBlackListService;
	def boxConfigService;
	def grailsApplication;
	def rackspaceService;
	def userSessionService;
    def featureService;
    def mailService;
    def affiliateService;

    public static enum BoxActions implements ActionNames {
        INDEX("index"),
        REGISTER("register"),
        UNREGISTER("unregister"),
        LIST_ALL("listAll"),
        NOT_COMPANY("notcompany"),
        NOT_LOCATIONS("notlocations"),
        NOT_BOXES("notBoxes"),
        EDIT("edit"),
        NOT_LOG_FILES("notLogFiles"),
        LIST_LOG_FILES("listLogFiles");

        private String actionName;

        private BoxActions(String name) {
            this.actionName = name;
        }

        public String actionName() {
            return actionName;
        }

        public String toString() {
            return actionName;
        }
    }

    def register() {
        final def companyList= getPermitedCompanies();
        //this will be used to determine which locations boxes we can see
        final def locations = getPermitedLocations();
        final def locationToSelect = params.location;
        final String serialValue = params.serial;
        Box box = new Box();
        box.serial = serialValue;
        if (params.doNotRedirect != true) {
            if (companyList.size() > 0) {
                int count = 0;
                for (company in companyList) {
                    count += company.locations?.size();
                }
                if (count > 0) {
                    [box: box , company:companyList, comingFromWizard:false, locationId:locationToSelect, 
                        allowedLocations:locations,serialValue:serialValue]
                } else {
                    redirect(controller:"location", action: BoxActions.NOT_LOCATIONS.actionName())
                }
            } else {
                redirect(controller:"company", action: BoxActions.NOT_COMPANY.actionName());
            }
        } else {
            if (companyList.size() > 0) {
                [box: box , company:companyList, comingFromWizard:true, allowedLocations:locations, 
                    serialValue:serialValue]
            }
        }
    }

	def retireBox(long id){

		def box = Box.get(id)

		if (!box) {
			flash.error = message(code: 'default.not.found.message')
		} else {

			Box.withTransaction {status ->
				try{
					def boxManufacturer= BoxManufacturer.findBySerialNumber(box.serial)
					def boxBlackList= BoxBlackList.findBySerial(box.serial)
					def boxIps=BoxIp.findAllBySerial(box.serial)
					def boxNotifications=BoxNotification.findAllByBox(box)
					def boxEncryption = BoxEncryption.findBySerial(box.serial)

					if(boxEncryption){
						boxEncryption.delete()
					}
					if(boxManufacturer){
						boxManufacturer.delete()
					}
					if(boxBlackList == null || !boxBlackList){
						//must stay blacklisted
						def serial = box.serial
						boxBlackList = new BoxBlackList()
						boxBlackList.serial = serial
						boxBlackList.dateStopWorking = new Date()
						boxBlackList.save()
					}else{
						boxBlackList.dateStopWorking = new Date()
						boxBlackList.save()
					}
					if (boxIps) {
						for (BoxIp ip : boxIps) {
							ip.delete();

						}
					}
					if (boxNotifications) {
						for (BoxNotification notification : boxNotifications) {
							//log.info(" notification: ["+ notification.box +"]" )
							notification.delete();
							//log.info("notification notification after delete: ["+ notification.box +"]" )
						}
					}

					//Get the list of images url that needs to be deleted
					def channelImages = [];
					for(channel in box.channels){
						channelImages.add(channel.imageURL);
						channelImages.add(channel.largeImageURL);
					}

					rackspaceService.deleteMultipleFiles(channelImages);

					box.delete();
					flash.success = message(code: 'default.box.retire.success')
				}catch(Exception e){
					e.printStackTrace()
					status.setRollbackOnly()
					flash.error = message(code: 'default.error.problem')
				}
			}
		}
		redirect(controller:"box", action:BoxActions.LIST_ALL.actionName())
	}

	def notlocations(){}
	def notLogFiles(){

	}

	def doAskForRegisterOrList() {
		def previousLocation = params.location
		render(view:"askForRegisterOrList", model:[location: previousLocation])
	} // END doAskForRegisterOtherOrList

	/**
	 * This method will try to register a ExXtractor using the post
	 * parameters and return the result as a Json Object for Ajax
	 * manipulation.
	 * 
	 * @return	Json with the registration result
	 */
	def registerAjax() {
		Box boxRegistered=new Box(params);
		HashMap jsonMap = new HashMap();
		try {
			String errorMsg = boxService.registerBox(boxRegistered, true);
			if(errorMsg == "") { // There is not a error, registration success
				jsonMap.msg = message(code: 'default.box.successRegistered');
				jsonMap.error = false;
			} else {
				jsonMap.msg = errorMsg;
				jsonMap.error = true;
			}
		} catch(Exception e) {
			log.error("Error registrating a Box with params: " + params , e);
			jsonMap.error =  true;
			jsonMap.msg = e.getMessage();
		}
		render jsonMap as JSON;
	} // End of registerAjax method

    /**
     * Process the registration of an ExXtractor including the send of the notification email
     * when the process is success
     */
    def doregister() {
        Box box = new Box(params);
        box.serial = box.serial? box.serial.replaceAll("\\s",""): "";
        try {
            String errorMsg = boxService.registerBox(box, false);
            if (StringUtils.isBlank(errorMsg)) {//No error
                //Send Email
                try {
                    mailService.sendMail {
                        from    affiliateService.getCurrentAffiliateConcatEmail(session.affiliate)
                        to      Configuration.findByCode(SystemConfiguration.REGISTRATION_MAIL.getCode()).getValue()
                        subject message(code: 'box.register.email.subject', args:[box.serial])
                        html    g.render(template:"/templates/emails/boxRegistrationTemplate",
                        model:[user:userSessionService.getCurrentUser(), box:box])
                    }
                    flash.success = message(code: 'default.box.successRegistered');
                } catch(Exception e) {
                    // I don't want to fail if the mail can't be send, just log the error
                    log.error("Error sending email with the registration report", e);
                    flash.warn = message(code: 'default.box.successRegistered.but.email.failed');
                }
            } else {
                flash.error = errorMsg;
            }
        } catch(Exception ex) {
            log.error("Error registering a box: " + ex.getMessage(), ex);
            flash.error = message(code:'unexpected.error.please.try.again');
        }
        // Render depending if must redirect or not and if exists an error or not
        if (params.doNotRedirect) {
            [box, (flash.error == null)]
        } else {
            if (flash.error != null) {
                def companies = getPermitedCompanies();
                box = box? box:new Box();
                render(view:BoxActions.REGISTER.actionName(), model: [box:box, company:companies , comingFromWizard:false])
            } else {
                redirect(controller:"box", action:BoxActions.LIST_ALL.actionName(), params:[serialToFilter: box.serial])
            }
        }
    }

	def index()
	{
		try
		{
			def id=params.id?params.id:-1
			def location=CompanyLocation.get(params.id)
			def company = Company.get(location.company?.id)
			//
			if(location && company)
			{
				def boxes=Box.findAllByLocation(location)
				// if the boxes.size=0 maybe we can redirect to register box
				[boxes:boxes,location:location]
			}
			else
			{
				//maybe will be render to an error page " you don't have permission to see that box"
				flash.error=message(code: 'default.error.dontHavePermission')
			}
		}
		catch(Exception ex)
		{
			log.error( ex.message);
			flash.error=message(code: 'default.error.unableDisplayLocations')
		}
	}

    def notBoxes() {}

    def resetFactory(String id) {
        Box box = Box.findBySerial(id);
        def boxId = box.id;
        log.info("Version: " + box.softwareVersion?.versionSoftware);
        if (box.softwareVersion?.versionSoftware < 11) {
            flash.error = message(code: 'default.box.reset.versionError');
        } else {
            box.save(flush:true);
        }
        redirect(controller:"box", action:BoxActions.LIST_ALL.actionName())
    }

    def remoteReset(String id) {
        Box box = Box.read(id);
        if (box.softwareVersion?.versionSoftware < 11) {
            flash.error=message(code: 'default.box.reset.versionError');
        } else {
            try {
                //This removes the connection from CS and sets the device as disconnected
                if(boxService.sendRemoteResetRequest(box.serial)){
                    flash.success=message(code: 'default.box.remreset.success');
                } else {
                    flash.error=message(code: 'default.box.remreset.failed');
                }
            } catch(Exception ex) {
                log.error(session, ex);
                flash.error=message(code: 'default.box.remreset.failed');
            }
        }
        redirect(controller:"box",action:BoxActions.LIST_ALL.actionName());
    }

	def restoreData(String id) {
		def box = Box.read(id)
		log.info("Version: "+box.softwareVersion?.versionSoftware);

		if(box.softwareVersion?.versionSoftware < 11){
			flash.error=message(code: 'default.box.reset.versionError')
		}
		else{
			try{
				//This removes the connection from CS and sets the device as disconnected
				if(boxService.sendRestoreDataRequest(box.serial)){
					flash.success=message(code: 'default.box.remreset.success');
				} else {
					flash.error=message(code: 'default.box.remreset.failed');
				}
			} catch(Exception ex) {
				log.error(session, ex)
				flash.error=message(code: 'default.error.unableDisplayLocations')
			}
		}
		redirect(controller:"box",action:BoxActions.LIST_ALL.actionName())
	}

    /**
     * Renders the list of boxes depending of the current user and the assigned role
     */
    def listAll() {
        final User user = userSessionService.getCurrentUser();
        def locations = userSessionService.isAdminOrHelpDesk()?
            CompanyLocation.findAll():CompanyLocation.getAllLocations(user, null).listDistinct();
        if (locations && locations.size() > 0) {
            def locationsWithBoxes = locations.findAll{it.boxes.size() != 0};
            def boxes = locationsWithBoxes.collect(){it*.boxes}.flatten();
            if (boxes && boxes.size() > 0) {
                [serialToFilter:params.serialToFilter]
            } else {
                redirect(action:BoxActions.NOT_BOXES.actionName());
            }
        } else { // There are not boxes to display
            if (userSessionService.isAdmin()) {
                redirect(controller:"home", action:BoxActions.INDEX.actionName());
            } else {
                redirect(controller:"location", action:BoxActions.NOT_LOCATIONS.actionName());
            }
        }
    }

	private hasCompanies(List companies) {
		return (companies != null && companies.size() > 0);
	}

    /**
     * Render the view for the edition of a Box
     * 
     * @param id	Id of the Box to be edited
     * @return
     */
    def edit(long id) {
        Box box = Box.get(id);
        if(box) {
            def companyList = getPermitedCompanies();
            def listChannels = null;
            boolean isPA = false;
            boolean isConnected = false;
            int debugLevel = 4;
            if(box.connected()) {
                isConnected = true;
                (listChannels , isPA, debugLevel) = boxService.updateChannels(box.serial, null);
                //save or verify if channels are in BD
                boxService.updateDBChannels(box, listChannels, isPA, debugLevel);
                listChannels = listChannels.asList().sort{it.channelPort};
                if(box.softwareVersion && box.softwareVersion.versionSoftware >= BoxVersionsEnum.V2_38.getVersion()) {
                    listChannels = listChannels.findAll{!it.isPa};
                } else { // For previous or unknown versions of 2.38 must discard the channels that are not USB
                    listChannels = listChannels.findAll{!it.isPa && it.channelPort.toUpperCase().indexOf(Constants.VALID_CHANNEL_CONTENT) > -1};
                }
            } else {
                listChannels = box.channels;
                listChannels = listChannels.asList().sort{it.channelPort};
                if(box.softwareVersion && box.softwareVersion.versionSoftware >= BoxVersionsEnum.V2_38.getVersion()
                    && !box.pa) {
                    listChannels = listChannels.findAll{!it.isPa};
                } else { // For previous or unknown versions of 2.38, and when the box is PA must discard the channels that are not USB
                    listChannels = listChannels.findAll{!it.isPa && it.channelPort.toUpperCase().indexOf(Constants.VALID_CHANNEL_CONTENT) > -1};
                }
            }
            final BoxIp boxIp = BoxIp.getBoxLastIpConnected(box.serial).get();
            final BoxManufacturer boxManufacturer = BoxManufacturer.findBySerialNumber(box.serial);
            final boolean canBePA = featureService.isFeatureAllowedForBox(box, Feature.BOX_PA);
            final boolean canSetChannelsDelay = featureService.isFeatureAllowedForBox(box, Feature.CHANNELS_DELAY);
            [box: box, listChannels:listChannels, boxIp: boxIp, isConnected: isConnected, canBePA:canBePA,
                canSetChannelsDelay:canSetChannelsDelay, boxManufacturer: boxManufacturer]
        } else {
            flash.error = message(code: 'default.not.found.message');
            redirect(action: BoxActions.LIST_ALL.actionName(), controller:"home");
        }
    }


	private def sortChannelByChannelNumber(Box box) {
		return box.channels.asList().sort{it.channelNumber};
	}

	private def getChannelsSortedByNumber(Box box) {

		def listChannels  = null

		if(!box.channels.isEmpty()) {
			listChannels = sortChannelByChannelNumber(box)
		} else {
			flash.error = message(code: 'default.boxes.noconnect')
		}

		return listChannels
	}

    def update(String  id, Long version) {
        flash.error = null;
        if (params.name != null && params.audioCardGeneration != null && params.jumperConfiguration != null) { //Required parameters
            try {
                final Box box = Box.read(id);
                BoxConfigurationUpdates saveConfigurationResponse = new BoxConfigurationUpdates();
                if (box.connected()) { // Check and update box parameters
                    saveConfigurationResponse = boxConfigService.reviewBoxConfigurationChanges(box, params);
                }
                final boolean paValue = (params[Constants.PARAM_AUXILIAR_PA].equals(Constants.PARAM_UPDATED_VALUE));
                if (box.pa != paValue) { // Set the current box PA value if there was a change
                    // New request that updates all the configurations including the PA
                    def resultOfUpdatePABox =
                            boxService.sendBoxInformationUpdate(box, paValue, flash, saveConfigurationResponse);
                    if (resultOfUpdatePABox != null) {
                        flash.error = resultOfUpdatePABox;
                    } else { //  Legacy request for old servers
                        resultOfUpdatePABox = boxService.sendBoxPA(box, paValue, flash);
                    }
                } else {
                    if (saveConfigurationResponse != null && saveConfigurationResponse.hasChanged()) {
                        // New request that updates all the configurations including the PA
                        def resultOfUpdatePABox =
                                boxService.sendBoxInformationUpdate(box, paValue, flash, saveConfigurationResponse);
                        if (resultOfUpdatePABox != null) {
                            flash.error = resultOfUpdatePABox;
                        }
                    }
                }
                if (flash.error == null) { // There are not previous errors, generally from the requests
                    // Save the updated configurations if exists
                    boxConfigService.saveUpdatedConfigurations(box, saveConfigurationResponse);
                    // Checks if the audio input mode changed and require to remove all the information of the channels
                    if (saveConfigurationResponse != null &&
                            saveConfigurationResponse.configurationHasChanged(BoxConfigurationCode.AUDIO_INPUT_MODE)) {
                        boxService.cleanChannelsInformation(box);
                    }
                    // Do not use box.save() because is updating the lastUpdate field and this field is been use to
                    // calculate the LastSeen Information
                    flash.success = message(code: 'default.message.success');
                    if (boxService.updateBasicBoxInfo(box, params.name, paValue, params.audioCardGeneration,
                            params.jumperConfiguration)) {
                        boxService.setPAValue(box, paValue);
                        flash.success = message(code: 'default.message.success')
                    } else {
                        flash.error = message(code: 'default.message.error.update')
                    }
                }
            } catch(Exception e) {
                // This should'n happend but we capture the general exception for unexpected grails exceptions
                log.error(e);
                flash.error = e.getMessage();
            }
        } else {
            flash.error = message(code: 'wrong.request.because.invalid.parameters');
        }
        if (flash.error == null) {
            redirect(action:BoxActions.LIST_ALL.actionName());
        } else {
            redirect(action:BoxActions.EDIT.actionName(), params: [id: id]);
        }
    }

	def havepa(long location)
	{
		def locationInstace=CompanyLocation.get(location)
		def paBOX=Box.findByLocationAndPa(locationInstace,true)
		render(contentType: 'text/json') {
			[
				'result': paBOX?true:false,
			]}

	}

	/**
	 * results=Simple message to display
	 * Status:
	 * 		0->Not problem is ok the exXothermic box can change the PA 
	 * 		1->Not Found Box *
	 * 		2->PA is necessary for this location (Don't exist a PA )*
	 * 		3->The Location was left without PA, you can not update ( from true to false)*
	 * 		4->There is another PA, we need permission to overwrite ( from false to true)
	 * */

	def isvalidpa(String serial,boolean pa)
	{
		def status=0
		def results=message(code: 'default.message.ok')
		def box=Box.findBySerial(serial)
		if(!box)
		{
			status=1
			results=message(code: 'default.box.notfound',args:[serial])

		}
		else
		{
			def paBOX=Box.findByLocationAndPa(box.location,true)
			if(paBOX!=null)
			{
				if ((paBOX.serial==box.serial))
				{
					if(!pa)
					{
						status=3
						results=message(code: 'default.box.cannotupdate')
					}
				}
				else
				{
					if(pa)
					{
						status=4
						results=message(code: 'default.box.location.youneedpermission',args:[paBOX.name])
					}
				}
			}
			else
			{
				if(!pa)
				{
					status=2
					results=message(code: 'default.box.location.isneeded',args:[box.location])
				}
			}
		}

		render(contentType: 'text/json') {
			[
				'results': results,
				'status': status,
			]}
	}

	/** this method setChannel Info into the DB and send the request to myBox
	 * 
	 * @param serial
	 * @param channelNumber
	 * @param channelLabel
	 * @param isPA
	 * @param imageURL
	 * @param description
	 * @param largeImageURL
	 * @param busterLevel
	 * @return
	 */
	def setChannel(String serial,int channelNumber, String channelLabel,boolean isPA,
			String imageURL, String description, String largeImageURL, int gain, boolean enableForApp, int delay){

		def responseRest = null
        
		log.info( serial + "/" + channelNumber + "/" + channelLabel + "/" + isPA + "/" + imageURL + "/" +
				description + "/" + largeImageURL + "/" + gain + "/" + delay);

		Box box = boxService.findBoxBySerialNumber(serial);

        if(box && box.connected()) {
            String channelDataValidation = validateChannelData(box, channelLabel, description);
            
            log.info( "-> " + channelDataValidation);
    
            if(channelDataValidation != ""){
                //Return error because there is a error with the channel data
                responseRest = createMessageJsonWithErrorWithMessage("0", channelDataValidation);
            } else {
                //No data error, continue
                BoxChannel.withTransaction { status ->
                    try {
    
                        BoxChannel channel = BoxChannel.findByBoxAndChannelNumber(box, channelNumber)
                        if(channel) {
                            channel.setChannelLabel(channelLabel);
                            channel.setIsPa(isPA);
                            channel.setDescription(description);
                            channel.setImageURL(imageURL);
                            channel.setLargeImageURL(largeImageURL);
                            channel.setGain(gain);
                            channel.setEnabledForApp(enableForApp);
                            channel.setDelay(delay);
                            if(channel.validate()){
                                channel.save();
                            }else{
                                def erorMessage
                                def fieldError = channel.getErrors().getFieldError()
                                if(fieldError.getCode().equals("blank") && fieldError.getField().equals("channelLabel") ){
                                    erorMessage = message(code: "mycentralserver.box.BoxChannel.channelLabel.blank")
                                }else{
                                    erorMessage = message(code: fieldError.getDefaultMessage(), args: [fieldError.getField()])
                                }
    
                                responseRest = createMessageJsonWithErrorWithMessage("0", erorMessage)
                                return JSON.parse(responseRest) as JSON
                            }
                        } else {
                            log.info("There is no channel with channelNumber: " + channelNumber);
                        }
                        //after saving channel in DB
                        if(box != null && box.status.description.equals(BoxStatusEnum.CONNECTED.value)) {
                            responseRest = restClientService.setChannelStatus(serial,  channelNumber,channelLabel, isPA, "",
                                    description, imageURL, largeImageURL,"" , "", gain, enableForApp, delay);
                            responseRest = validateEmptyMessage(responseRest)
    
                            def message = getJsonMessageFormRawMessage(responseRest)
                            def isSucessful = message["successful"]
    
                            if(isSucessful == false) {
                                throw new Exception("Error Sending the change to myBox " + serial + " . Data: channel Number: " + channelNumber + " Label: " +  channelLabel + " isPa: " + isPA)
                            }
                        } else {
                            log.warn(" In SetChannel the box serial: " + serial + " is " + box.status + " the change will send when the box reconnect again." )
                            def msg = message(code: "default.box.channel.changest.not.send", args: [serial])
                            responseRest = createMessageJsonSucessfulWithMessage(msg);//createMessageJsonSucessful()
                        }
    
                    } catch(Exception dbEx) {
                        dbEx.printStackTrace()
                        status.setRollbackOnly()
                        log.error "Error: ${dbEx.message}", dbEx
                        responseRest = createMessageJsonWithError("")
                    }
                } // END BoxChannel.withTransaction
            }
        } else {
            responseRest = createMessageJsonWithErrorWithMessage("0",
                message(code: "device.lost.connection"));
        }
		render JSON.parse(responseRest) as JSON
	} // END setChannel

	def checkBoxChannelsAgaintsDB(String serial){
		def box = Box.findBySerial(serial)
		def responseRest = null;

		if(box.status.description.equals(BoxStatusEnum.CONNECTED.value)) {

			//current channels
			def boxChannels = boxService.getBoxChannels(serial).asList()
			def dbChannels = box.channels.asList().sort{it.channelNumber}.collect(){it.channelNumber};

			log.info("Box is connected and we are comparing DB with actual boxes channels. They are equal? " + boxChannels.equals(dbChannels) );

			if (!boxChannels.equals(dbChannels)){
				responseRest =  '''{
							"status": "",
							"errorCode": "0",
							"successful": false,
							"message" : ''' + message(code: 'error.page.outOfSync.box')+ '''}'''
			}else{
				responseRest = createMessageJsonSucessful();
			}

		}else{
			responseRest = createMessageJsonSucessful();
		}

		render JSON.parse(responseRest) as JSON

	}
	/** Convert the String Json to JSON Object
	 * 
	 * @param responseRest
	 * @return
	 */
	private def getJsonMessageFormRawMessage(responseRest) {
		return JSON.parse(responseRest);
	}

	private def validateEmptyMessage(responseRest) {

		if (!responseRest || responseRest=="{}" )
		{
			return createMessageJsonWithError()
		}

		return responseRest
	}

	/** Find the channel with the ChannelNumber
	 * 
	 * @param channels
	 * @param channelNumber
	 * @return BoxChannel
	 */
	private def findChannelByChannelNumber(channels, int channelNumber) {
		for (BoxChannel channel : channels) {
			if (channel.channelNumber == channelNumber){
				return channel;
			}
		}
		return null;
	}

    /**
     * Renders the show page of a box with the received id
     * @param id
     *          Id of the box to display information
     */
    def show(Long id) {
        final Box box = Box.get(id);
        if (box) {
            final boolean canBePA = featureService.isFeatureAllowedForBox(box, Feature.BOX_PA);
            [box:box, canBePA: canBePA]
        } else {
            flash.message = message(code: 'default.not.found.message', 
                args: [message(code: 'userInformation.label', default: 'User Information'), id]);
            redirect(action: BoxActions.LIST_ALL.actionName());
        }
    }

    /**
     * Process the request that renders the list of available log files; the box can be registered or unregistered
     * @param id
     *      id of the box; can be found on the registered or unregistered table
     */
    def listLogFiles(Long id) {
        final Box box = Box.get(id);
        final String serialNumber;
        if(box) {
            serialNumber = box.serial;
        } else {
            final BoxUnregistered boxUnregistered = BoxUnregistered.get(id);
            if(boxUnregistered) {
                serialNumber = boxUnregistered.serial;
            }
        }
        
        if(serialNumber) {
            def jsonWithLogFile = restClientService.getLogsList(serialNumber);
            if(jsonWithLogFile) {
                if(jsonWithLogFile.successful) {
                    if(jsonWithLogFile.data?.size() > 0) {
                        [boxId:id, jsonWithLogFile:jsonWithLogFile];
                    } else {
                        redirect(controller:"box", action:BoxActions.NOT_LOG_FILES.actionName());
                    }
                } else {
                    HashMap<String, String> errorsCode =  messageErrorService.getMessageErrorDescriptionForGetLog();
                    flash.error = message(code: errorsCode.get(jsonWithLogFile.errorCode));
                    redirect(controller:"box", action:BoxActions.LIST_ALL.actionName());
                }
            } else {
                flash.error = message(code: 'default.boxes.noconnect');
                redirect(controller:"box", action:BoxActions.LIST_ALL.actionName());
            }
        } else {
            flash.error = message(code: 'default.not.found.message');
            redirect(controller:"box", action:BoxActions.LIST_ALL.actionName());
        }
    }

    /**
     * Send action to Delete the MyBox's log file selected on the page
     * 
     * @param id MyBox identificator
     */
    def deleteLogFile(Long id) {
        Box box = Box.get(id);
        String serial;
        if(box) {
            serial = box.serial;
        } else {
            BoxUnregistered boxUnregistered = BoxUnregistered.get(id);;
            serial = boxUnregistered.serial;
        }
        final String fileName = params.file;
        def jsonResponse = restClientService.deleteLogFile(serial, fileName);
        if(jsonResponse == null || jsonResponse.sucessful == false) {
            log.info("Json for deleteLogFile was unsuccesful for serial: " + serial);
            flash.success = "Deleted Log File";
        }
        redirect(action:BoxActions.LIST_LOG_FILES.actionName(), id:id);
    }

    /**
     * Executes the download process of a log file from a Venue Server, the server can be registered or unregistered
     */
    def downloadLogFile(Long id) {
        Box box = Box.get(id);
        def fileName = params.file;
        String fileNameIntoTheServer;
        String serial;
        if(box) {
            fileNameIntoTheServer = box.id + "_" + box.serial + "_" + fileName;
            serial = box.serial;
        } else {
            BoxUnregistered boxUnregistered = BoxUnregistered.get(id);
            fileNameIntoTheServer = boxUnregistered.id + "_" + boxUnregistered.serial + "_" + fileName;
            serial = boxUnregistered.serial;
        }
        def timeToWait = grailsApplication.config.timeToWait;
        // Get the list of current files to delete them
        File dir = new File(grailsApplication.config.basePathMyBoxLogFiles);
        log.info("UploadFiles path: " + grailsApplication.config.basePathMyBoxLogFiles);
        File[] matches = dir.listFiles(new FilenameFilter() {
                    public boolean accept(File dire, String name) {
                        return name.startsWith(fileNameIntoTheServer);
                    }
                });
        for (File fileToDelete : matches) {
            fileToDelete.delete();
        }

        // sends the request to the box to upload the log
        def url = grailsApplication.config.urlServiceUploadLogFile;
        def jsonResponse = restClientService.getFile(serial, fileName, url);
        
        if(jsonResponse && jsonResponse.successful) {
            def arraySize = timeToWait.size();
            def time;
            boolean isfileUploadedIntoTheServer = false;
            File finalFile;
            // begin to wait for the uploaded file
            for (int i = 0; i < arraySize; i++) {
                time = timeToWait[i];
                Thread.currentThread().sleep(time * 1000);
                File[] uploadedFiles = dir.listFiles(new FilenameFilter() {
                            public boolean accept(File direc, String name) {
                                return name.startsWith(fileNameIntoTheServer);
                            }
                        });
                if(uploadedFiles && uploadedFiles.size() > 0) {
                    // if any new file have been found
                    Arrays.sort(uploadedFiles, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
                    finalFile = uploadedFiles[0]; // get the newest file
                    isfileUploadedIntoTheServer = true;
                    break;
                }
            }

            if(isfileUploadedIntoTheServer) {
                // or or image/JPEG or text/xml or whatever type the file is
                response.setContentType("application/octet-stream");
                response.setHeader("Content-disposition", ("attachment;filename="+finalFile.getName()));
                response.outputStream << finalFile.bytes;
            } else {
                render(view:"logFileWasNotReceivedFromExxtractorServer");
                return;
            }
        } else {
            render(view:"logFileWasNotReceivedFromExxtractorServer");
            return;
        }
    }

	def existsTheFile(String fileName) {
		def storageFullVirtualPathLocation = getFileLocation(fileName)
		def file = new File(storageFullVirtualPathLocation)
		return file.exists();
	}

	def getFileLocation(String fileName) {
		return (servletContext.getRealPath(grailsApplication.config.basePathMyBoxLogFiles)+ "/" + fileName);
	}

	def blackList()
	{
		def user = getCurrentUser()
		def locations = null
		def offset = this.getOffsetParameterToPagination()

		try
		{
			def roleAdmin = Role.findByAuthority("ROLE_ADMIN")
			def isAdmin = user.authorities?.contains(roleAdmin)

			def helpDesk = Role.findByAuthority("ROLE_HELP_DESK")
			def isHelpDesk = user.authorities?.contains(helpDesk)

			def boxes = BoxBlackList.findAll()

			def totalCount = boxes.size()
			def max = Math.min(params.max ? params.int('max') : 10, 100)
			params.max = max

			[boxes: Utils.paginateList(boxes, params), boxesCount:totalCount, offset: offset]
		}
		catch(Exception e)
		{
			e.printStackTrace()
			flash.error=message(code: 'default.error.unableDisplayLocations')
		}
	}//blackList

	def removeBoxFromBlackList(long id){
		try{

			if (!boxService.removeBlackListedBox(id)) {
				flash.error = message(code: 'default.not.found.message')
			} else {
				flash.success = message(code: 'mycentralserver.box.blackList.retire.success')
			}
			redirect(controller:"box",action: "blackList");
		}catch(e){
			flash.error = message(code: 'default.not.found.message')
			log.error(e.getMessage());
		}
	}

	/**
	 * This method will render the page with the list of registered
	 * Venue Servers where the user must select the desired servers to 
	 * unregister.
	 * @return
	 */
	def unregister() {
		def boxes = userSessionService.getUserAllowBoxes();
		def locations = userSessionService.getUserLocationsWithAllowBoxes();
		if(boxes != null) {
			[boxes: boxes, locations: locations];
		} else {
			redirect(action: BoxActions.NOT_BOXES.actionName());
		}
	}

    /**
     * This method will receive the request for the unregister of a list of venue servers and renders the page with the
     * result for each server
     */
    def applyUnregister() {
        try {
            def paramBoxes = params.list(Constants.PARAM_BOXES);
            def newList = paramBoxes.collect{ it.toLong() };
            if (newList.size() > 0) {
                def unregisterResult = [];
                for (b in newList) {
                    Box box = Box.read(b);
                    if(box) {
                        def boxInfo = [];
                        boxInfo.add(box.serial);
                        boxInfo.add(box.name);
                        boxInfo.add(box.location.name);
                        boxInfo.add(boxService.unregister(box));
                        unregisterResult.add(boxInfo);
                    }
                }

                def boxes = userSessionService.getUserAllowBoxes();
                if(boxes != null) {
                    def locations = userSessionService.getUserLocationsWithAllowBoxes();
                    render(view: BoxActions.UNREGISTER.actionName(), model:[boxes: boxes, unregisterResult: unregisterResult, locations: locations]);
                } else {
                    render(view: BoxActions.NOT_BOXES.actionName(), model:[unregisterResult: unregisterResult]);
                }
            } else {
                flash.error = message(code: 'default.box.unregister.noList');
                redirect(action: BoxActions.UNREGISTER.actionName());
            }
        } catch(Exception e) {
            redirect(action: BoxActions.UNREGISTER.actionName());
        }
    }

	def uploadImagePage(int id) {
		BoxChannel channel = BoxChannel.get(id)
		render(template:"uploadFileTemplate", model:[channel:channel])
	}

	def uploadLargeImagePage(int id) {
		//BoxChannel channel = BoxChannel.get(7)
		BoxChannel channel = BoxChannel.get(id)
		render(template:"uploadLargeFileTemplate", model:[channel:channel])
	}

	def uploadFile(String type){
		def channel
		String messageCode

		if ( params.size() > 2) { //if the only values are action and controller, then we are not coming from the form
			try
			{
				log.info( params);

				channel = BoxChannel.read(params.id)

				//TODO: not hardocded
				String fileId = "file";
				int requiredWidth = 0;
				int requiredHeight = 0;
				if(type == "small"){
					requiredWidth = Constants.CHANNEL_IMAGE_SMALL_WIDTH;
					requiredHeight = Constants.CHANNEL_IMAGE_SMALL_HEIGHT;
					messageCode = boxService.saveChannelImage(channel, params[fileId],
							params['x-'+fileId], params['y-'+fileId], params['w-'+fileId], params['h-'+fileId])
				}else{
					requiredWidth = Constants.CHANNEL_IMAGE_LARGE_WIDTH;
					requiredHeight = Constants.CHANNEL_IMAGE_LARGE_HEIGHT;
					messageCode=  boxService.saveChannelLargeImage(channel, params[fileId],
							params['x-'+fileId], params['y-'+fileId], params['w-'+fileId], params['h-'+fileId])
				}

				if (messageCode != null){
					String errorMessage = "";
					if(messageCode == "image.incorrect.error"){
						errorMessage = message(code: "image.incorrect.error.with.dimensions", args:[requiredWidth, requiredHeight])
					} else {
						errorMessage = message(code: messageCode);
					}
					//response.status = 400;
					render ([error:true, textStatus: errorMessage ] as JSON )
				}else{ // no error
					render(text: [error:false, success:true, channelNumber:channel.channelNumber ,
						imageURL:channel.imageURL, largeImageURL: channel.largeImageURL] as JSON, contentType:'text/json')
				}

			} catch( Exception ex) {
				log.error(ex)
				render ([error:true, textStatus: message(code: 'default.error.problem') ] as JSON )
			}
		}else{
			render ([error:true, textStatus: message(code: 'default.error.problem') ] as JSON )
		}
	}

	/**
	 * This method will receive a channel id and the image type
	 * to be deleted; delete the file and returns the result as JSON
	 * 
	 * @return 	Json object with {error, msg}
	 */
	def deleteChannelImage (){
		boolean error = false;
		String msg = "";

		try {
			//Gets the Channel by the Id
			BoxChannel channel = BoxChannel.read(params.id);
			println params;
			if (!channel) {
				//If not exists the Affiliate
				msg = message(code: 'default.not.found.message')
				error = true;
			} else {
				if(allowedAccess(channel)){
					String imageUrl = "";
					if(params.t == "large"){
						//Delete the large Image
						imageUrl = channel.largeImageURL;
						channel.largeImageURL = null;
					} else if(params.t == "small"){
						//Delete the small image
						imageUrl = channel.imageURL;
						channel.imageURL = null;
					}

					//Save the Entity to DB
					if(channel.save()){
						// if everything is ok, remove the image from de server
						rackspaceService.deleteFile(imageUrl);
						msg = message(code: 'affiliate.establishments.save.success');
					} else {
						msg = "Error updating the channel";
						error = true;
					}
				} else {//Not allow
					msg = message(code: 'default.not.found.message')
					error = true;
				}
			}
		}catch(Exception ex){
			error = true;
			msg = ex.getMessage();
			log.error(ex);
		}

		def result = [error: error, msg: msg];
		render result as JSON;
	}

	def allowedAccess(BoxChannel channel){return true;}

	def String validateChannelData(Box box, String name, String description){
		BoxChannel channel = new BoxChannel();
		channel.setChannelLabel(name);
		channel.setDescription(description);
		channel.setCodec("OPUS");
		channel.setBox(box);
		def erorMessage = "";
		if(!channel.validate()){
			channel.errors.allErrors.each {
				String fieldName = (it.getField() == "channelLabel")? message(code:'name'):it.getField();
				if(it.getCode().equals("blank") && it.getField().equals("channelLabel") ){
					erorMessage += message(code: "mycentralserver.box.BoxChannel.channelLabel.blank") + " / "
				}else{
					erorMessage += message(code: 'channel.field.validation.message', args: [fieldName]) + " / "
				}
			}
		}
		return erorMessage;
	}

    /**
     * Render the page that allows to move a venue server to the black list
     */
    def block() {
        def boxes = boxService.getNotBlackListedBoxes();
        if (boxes) {
            [boxes: boxes]
        } else {
            redirect(action: BoxActions.NOT_BOXES.actionName());
        }
    }
		
	/**
	 * Receive the request that allows to add a box to the black list
	 * @return
	 */
	def doBlock() {
		boolean error = true;
		String msg = "";
		try {
			msg = boxBlackListService.addToBlackListById(params.boxId.toLong(), params.reason, new Date());
			if ( msg == "") {
				msg = message(code: 'box.block.success');
				error = false;
			}
		} catch(Exception ex) {
			msg = ex.getMessage();
			log.error(ex);
		}

		def result = [error: error, msg: msg];
		render result as JSON;
	}

    /*
     * This method will receive the request for sending a message to a Venue Server, the request must contains the
     * action of the message
     */
    def sendActionToServer() {
        boolean error = true;
        String msg = "";
        try {
            restClientService.sendActionRequestToServer(params.serial, params.requestAction);
            msg = message(code:'box.action.request.status.success');
            error = false;
        } catch(Exception ex) {
            msg = ex.getMessage();
            log.error(ex);
        }
        def result = [error: error, msg: msg];
        render result as JSON;
    }
}
