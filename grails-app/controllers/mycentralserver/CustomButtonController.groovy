package mycentralserver

import java.awt.image.BufferedImage
import mycentralserver.box.Box
import mycentralserver.custombuttons.CustomButton
import mycentralserver.user.User
import mycentralserver.user.Role
import mycentralserver.company.Company
import mycentralserver.company.CompanyIntegrator
import mycentralserver.company.CompanyLocation
import mycentralserver.generaldomains.Scheduler
import grails.converters.JSON
import mycentralserver.utils.Utils
import javax.imageio.ImageIO;
import mycentralserver.utils.CustomButtonTypeEnum

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Date;


class CustomButtonController extends BaseControllerMyCentralServerController {

	MessageErrorService messageErrorService
	RestClientService restClientService	
	CustomButtonService customButtonService
	
    def index() { }
	
	def create() {
	
		def user = getCurrentUser()
		def id = params.id == null ? params.id : 0		
		def companyList =  getCompaniesFromCurrentUser()
	   		
		if(companyList.size() == 0) {
			redirect(controller:"company",action: "notcompany")
		}
		
		[customButton: new CustomButton(), companyList:companyList,  companyId: id]		
		
	} // END create
	
	/**
	 * This methods receives the information to create a new Custom Button or Content
	 * 
	 * @return
	 */
	def save()
	{
		
		def customButton
		println(params);
		
		if ( params.size() > 2) { //if the only values are action and controller, then we are not coming from the form
				try
				{					 
					customButton = new CustomButton(params)
					customButton.createdBy = getCurrentUser()	
					def mySchedule = new Scheduler();
					
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
					if (params.start_date != null && params.start_date != ""){
						mySchedule.startDate = dateFormat.parse(params.start_date);
					}
					if (params.end_date != null && params.end_date != ""){
						mySchedule.endDate = dateFormat.parse(params.end_date);
					}
					
					mySchedule.days = params._content_days;
					mySchedule.hours = params._content_hours;
															
					customButton.schedule = mySchedule;
					
					if (mySchedule.days  == "" || mySchedule.days == null) {
						flash.error= message(code: 'schedule.error.emptyDays');
					} else {
						if (params.end_date == null || params.end_date == "" || mySchedule.endDate.after(mySchedule.startDate) || mySchedule.startDate == mySchedule.endDate) {
							mySchedule.save();
							
							String messageCode = customButtonService.saveCustomButton(customButton, request.getFile('file'),  params.x, params.y, params.w, params.h)
							if (messageCode != null){
								flash.error=message(code: messageCode)			
							}else{ // no error
								flash.success = message(code: 'default.message.success')
							}
						} else {
							flash.error= message(code: 'schedule.error.earlyEndDate');
						}
					}		
				} catch( Exception ex)
				{			
					flash.error=ex
					log.error ex
				}
				
				if(flash.error != null){
					render(view: "create", model: getModelForSaveInErrorCase(customButton))
					return
				}else{				
					redirect(action:"assignLocation", id : customButton.id)
					return
				}
		}else{
			flash.error = message(code: 'default.not.found.message')
			redirect(action:"listAll")
			return
		}
				
	} // END save
 
	def listAll() {		
		def listCompanies = getCompaniesFromCurrentUser()	
		boolean checked = false
		def offset = this.getOffsetParameterToPagination()
		
		if(listCompanies.size() == 0) {
			redirect(controller:"company",action: "notcompany")
			return;
		}else{
			//check that at least one company has buttons
		 
			params.max = Math.min(params.max ? params.int('max') : 10, 100)
		  
		   def tempResults = CustomButton.getButtonsByCompanies(listCompanies.collect(){it.id}, getCurrentUser())
		   def allButtons = tempResults.listDistinct() 
		   def buttonsList = Utils.sortByName(tempResults.list().unique())
		   if(buttonsList.size() == 0){
				render(view: "noContentToManage", model:[noButtons:1, text:""])
			}else{
				[buttonsList: Utils.paginateList(buttonsList,params) , buttonsCount: buttonsList.size()]
			}	
		}															 
	} // END 
	
	def getEnableUserCustomButtons(listCompanies){
		def buttonsList = getUserCustomButtons(listCompanies)
		
		for (Iterator<CustomButton> iter = buttonsList.iterator(); iter.hasNext();) {
			CustomButton cb = iter.next();
			if(!cb.enabled){
				iter.remove()
			}
		}
		
		return buttonsList.unique().sort()
	}
	
	def getUserEnabledLocationsIds(listCompanies){
		def locations = []
		for (Iterator<Company> iter = listCompanies.iterator(); iter.hasNext();) {
			Company c = iter.next();			 
			for(location in c.locations){
				if(location.enable){
					locations += location.id 
				}
			}			 
		}
	
		return locations.unique()
	}
		
	def getUserCustomButtons(listCompanies){
		def buttonsList = []
		for (Iterator<Company> iter = listCompanies.iterator(); iter.hasNext();) {
			Company c = iter.next();
			if(c.enable){
				for(location in c.locations){
					if(location.enable){
						buttonsList += location.customButtons
					}
				}
			}
		}
		def user = getCurrentUser()
		for(customButton in user.createdCustomButtons){
			buttonsList += customButton
		}
		return buttonsList.unique().sort()
	}
		
	def noContentToManage(){
		
	}
	
	def assignLocation(){
				
			def buttonId = (params.id == null)? 0:params.id;
			
			def companyInstace = null
				
			def listCompanies = getContentCompanies()
			def offset = this.getOffsetParameterToPagination()
					
			if(listCompanies.size() == 0) {
				redirect(action: "noContentToManage")
				return
			} else {	
				if(listCompanies) {
					def buttonsList = getEnableUserCustomButtons(listCompanies)
					if(buttonsList == null || buttonsList.size() == 0){
						redirect(action: "noContentToManage")
						return
					}else{
						[companyList: listCompanies, buttonsList: buttonsList.unique(), buttonId: buttonId, offset:offset]
					}
				}
			}
		 
	}
		
	def ajaxAssignLocation(){
		
		println params
		
		def customButton=CustomButton.get(params.buttonId)
		
		if (!customButton) {
			flash.error = message(code: 'default.customButton.not.found')
			//redirect(action: "listAll")
		}else{
			
			def listCompanies = getContentCompanies()
						
			if(!allowCustomButtonAction(listCompanies, customButton)){
				//flash.error = message(code: 'default.not.found.message', args: [message(code: 'userInformation.label', default: 'User Information'), params.buttonId])
				render(text: '<div class="alert alert-error">' 
					+ message(code: 'default.not.found.message', args: [message(code: 'userInformation.label', default: 'User Information'), params.buttonId])
					+ '</div>', contentType: "text/html", encoding: "UTF-8")
			}else{
			
				String selectedLocations =""
				for(location in customButton.locations){
					selectedLocations += (selectedLocations.equals(""))? location.id:"," + location.id;
				}
					
				render (template:'companyButtonsTemplate', model:[customButton: customButton, companies: listCompanies, selectedLocations: selectedLocations])
				
			}
		}
	}
	
	def boolean allowCustomButtonAction(listCompanies, customButton){
		def relatedButtons = []
		
		for(company in listCompanies){
			if(company.enable){
				for(location in company.locations){
					if(location.enable) {
						relatedButtons += location.customButtons
					}
				}
			}
		}
		
		def user = getCurrentUser()
		for(userCustomButton in user.createdCustomButtons){
			relatedButtons += userCustomButton
		}
		
		return (relatedButtons.any { it in (customButton) })
	}
	
	def show(long id)
	{
		def offset = this.getOffsetParameterToPagination()
		def customButton=CustomButton.read(id)
	
		if (!customButton) {
			flash.error = message(code: 'default.customButton.not.found')
			render(view: "listAll", offset:offset)			
		}else{
			
			def listCompanies = getCompaniesFromCurrentUser()
			
			if(!allowCustomButtonAction(listCompanies, customButton)){
				flash.error = message(code: 'default.not.found.message', args: [message(code: 'userInformation.label', default: 'User Information'), id])
				render(view: "listAll", offset:offset)				
			}else{
				[customButton: customButton,  offset:offset]
			}
		}
	}
	
	def edit(long id)
	{
		def customButton=CustomButton.read(id)
		def offset = this.getOffsetParameterToPagination()
		
		if (!customButton) {
			flash.error = message(code: 'default.customButton.not.found')
			redirect(action: "listAll",  offset:offset)
		}else{
			
			def listCompanies = getCompaniesFromCurrentUser()
			
			if(!allowCustomButtonAction(listCompanies, customButton)){
				flash.error = message(code: 'default.not.found.message', args: [message(code: 'userInformation.label', default: 'User Information'), id])
				redirect(action: "listAll",  params: [offset: offset])
			}else{
				[customButton: customButton,companyList: listCompanies, offset:offset]
			}
		}
	}
	
	def update(long id, Long version)
	{
		
		def offset = this.getOffsetParameterToPagination()
		
		if ( params.size() > 3) { //if the only values are action and controller, then we are not coming from the form
			def customButton=null			
			
			try{
				customButton = CustomButton.read(id)
				if (customButton.version > version) {
					flash.error=message(code: 'default.content.edit.version.error')
					customButton.errors.rejectValue("version", 'default.content.edit.version.error')
				} else {				 		
					try
					{					 
						def mySchedule = Scheduler.read(customButton.schedule.id);
						
						DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
						if (params.start_date != null && params.start_date != ""){
							mySchedule.startDate = dateFormat.parse(params.start_date);
						}
						if (params.end_date != null && params.end_date != ""){
							mySchedule.endDate = dateFormat.parse(params.end_date);
						}
						
						mySchedule.days = params._content_days;
						mySchedule.hours = params._content_hours;
																
						customButton.properties = params
						customButton.schedule = mySchedule;
						
						
						if (params._content_days  == "" || params._content_days == null) {
							flash.error= message(code: 'schedule.error.emptyDays');
						} else {
							if (params.end_date == null || params.end_date == "" || mySchedule.endDate.after(mySchedule.startDate) || mySchedule.startDate == mySchedule.endDate) {
								
								String messageCode = customButtonService.updateCustomButton(customButton, request.getFile('file'),  params.x, params.y, params.w, params.h)
								if (messageCode != null){
									flash.error=message(code: messageCode)
								}else{ // no error
									flash.success = message(code: 'default.message.success')
								}		
							} else {
								flash.error= message(code: 'schedule.error.earlyEndDate');
							}
						}
					} catch( Exception ex) {
						flash.error=ex
						log.error ex
					}
				}					
			}catch(Exception ex){
				ex.printStackTrace()
				flash.error=message(code: 'default.error.problem')
			}
			if(flash.error != null){				 
				render(view: "edit", model: getModelForSaveInErrorCase(customButton))
			}else{
				redirect(action:"listAll",id:id, params: [offset: offset])
			}
		}else{
			flash.error = message(code: 'default.customButton.not.found')
			redirect(action: "listAll", params: [offset: offset])
		}
	}	
		
	def saveLocations()
	{				
		def customButton
		try{
			
			customButton = CustomButton.get(params.buttons)
			
			if (!customButton) {
				flash.error = message(code: 'default.customButton.not.found')
				redirect(action: "listAll")
			} else {
				def listCompanies = getContentCompanies()
			
				
				List currentLocations = new ArrayList<CustomButton>();
				def affectedLocations = []
				currentLocations.addAll(customButton.locations)
				
				def locationsToSave = (params.locationsToSave == null)? "":params.locationsToSave;
				def newLocations = locationsToSave.split(",");
				for(companyLocation in currentLocations){
					if(!newLocations.any { it == (companyLocation.id.toString()) }){
						if(companyLocation.enable && companyLocation.company.enable){
							customButton.removeFromLocations(companyLocation)
							affectedLocations.add(companyLocation)
						}
					}
				}	
				
				for (c in newLocations){
					if ( c != "" && !currentLocations.any { it.id.toString() == (c) }){
						CompanyLocation location = CompanyLocation.findById(c)
						customButton.addToLocations(location)
						affectedLocations.add(location)						
					}
				}				
				
				def jsonTransactionStatus = null
				HashMap<String, String> customButtonsInfo = new HashMap<String, String>()
				
				if(customButton.validate()) {
					CustomButton.withTransaction { status ->
						try {
							
							if(customButton.save()) {							
								
								if(!affectedLocations.isEmpty())
								{
									jsonTransactionStatus = customButtonService.updateBoxesOfLocations(affectedLocations)
									customButtonsInfo = customButtonService.getCustomButtonNames(affectedLocations);												
									
									if(jsonTransactionStatus == null) {
										flash.error = message(code: 'mycentralserver.custombuttons.syncErrorMessage')								
									}								
								}
							} else {
								flash.error = message(code: 'mycentralserver.custombuttons.syncErrorMessage')
							}				
							
						} catch(Exception dbEx) {
							flash.error = dbEx.getMessage()
							status.setRollbackOnly()														
						}
					}
				} else {
					flash.error = message(code: 'default.error.problem')
				}
				
				def buttonsList = getEnableUserCustomButtons(listCompanies)
				def messageErrorDescriptionHash  = messageErrorService.getMessageErrorDescription();				
				render(view: "assignLocation", model: [companyList: listCompanies, buttonsList: buttonsList.unique(), buttonId: customButton.id, jsonTransactionStatus:jsonTransactionStatus, customButtonsHashInfo:customButtonsInfo, messageErrorDescriptionHash:messageErrorDescriptionHash])
				return
				
			}
		}catch(Exception e){
			e.printStackTrace()		
			flash.error = message(code: 'default.error.problem')
			redirect(action: 'assignLocation', model: [buttonId: customButton.id])
		}		
		
	} // END saveLocations	
	
	
	private getContentCompanies() throws Exception{
		def listCompanies = getCompaniesFromCurrentUser()
		
		if(listCompanies.size() == 0) {
			//redirect(controller:"company",action: "notcompany")
			return listCompanies
		}else{
			int enabledLocationsCounter = 0
			for (Iterator<Company> iter = listCompanies.iterator(); iter.hasNext();) {
				Company c = iter.next();
				
				if (!c.enable || c.locations == null || c.locations.size() ==0){
					iter.remove()
				} else {
					enabledLocationsCounter = 0
					for(location in c.locations){
						if(location.enable){
							enabledLocationsCounter++;
						}
					}
					if(enabledLocationsCounter == 0) {
						iter.remove()
					}
				}
			}
		} 
		
		return listCompanies
		
	} // END getContentCompanies
	
	private getModelForSaveInErrorCase(CustomButton custumBottonParam) {
		def user = getCurrentUser()
		
		def companyList =  getCompaniesFromCurrentUser()
				
		//Return the view paramters
		return [customButton:custumBottonParam, companyList:companyList]		
	}

	
	
	public def syncronizeCustomButtons() {
		
		
		def id = params.customButtonId.toInteger();		
		def customButton = customButtonService.readCustomButtonById(id);
		def messageErrorDescriptionHash =  messageErrorService.getMessageErrorDescription()
		HashMap<String, String> customButtonsInfo
		def jsonTransactionStatus = null
		def affectedLocations = new ArrayList<CustomButton>()
		def locations = customButton.locations;
		
		if(!locations.isEmpty()) {			
			jsonTransactionStatus = customButtonService.updateBoxesOfLocations(locations)		
			customButtonsInfo = customButtonService.getCustomButtonNames(locations)
		}
		
		def listCompanies = getContentCompanies()
		def buttonsList = getEnableUserCustomButtons(listCompanies)
		
		if(jsonTransactionStatus == null && !locations.isEmpty()) {
			flash.error = message(code: 'mycentralserver.custombuttons.syncErrorMessage')
		} else {
			flash.success=message(code: 'default.message.success')
		}
		
		render(view: "assignLocation", model: [ companyList: listCompanies, 
												buttonsList: buttonsList.unique(), 
												buttonId: customButton.id, 
												jsonTransactionStatus:jsonTransactionStatus, 
												customButtonsHashInfo:customButtonsInfo, 
												messageErrorDescriptionHash:messageErrorDescriptionHash])
		return
		
	} // END syncronizeCustomButtons

} // END CustomButtonController
