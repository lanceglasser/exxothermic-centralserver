package mycentralserver;

import mycentralserver.app.AppSkin;
import mycentralserver.beans.BoxesSyncResult;
import mycentralserver.utils.Constants;
import mycentralserver.utils.Utils;
import mycentralserver.utils.ValidationHelper;
import mycentralserver.user.User;
import mycentralserver.company.Company;
import mycentralserver.company.CompanyLocation;
import mycentralserver.content.Content;
import mycentralserver.utils.DigestHelper;
import mycentralserver.utils.FileHelper;

import java.io.File;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import grails.converters.JSON;

/**
 * Controller that handles the related functionalities of the App Settings
 *
 * @author      Cecropia Solutions
 * @date     	08/18/2014
 */
class AppSkinController extends BaseControllerMyCentralServerController {

	def rackspaceService;
	def appSkinService;
	def imageUploadService;
    def userSessionService;
	
	/**
	 * This method get the list of associate Skins of the Current User
	 * and render the list page view or the No Skins View page.
	 * 
	 * @return
	 */
    def listAll() { 
		renderListViewWithSyncResult(null);
	}
	
	/**
	 * Renders the page that allow the creation of a new Skin
	 * 
	 * @return
	 */
	def create() {
		[entity: new AppSkin()]
	}

    /**
     * Receives the parameters for a new Skin, saves the entity to the database and upload the images.
     */
    def save() {
        def deleteWhenFails = [];
        final User user = userSessionService.getCurrentUser();
        AppSkin appSkin;
        try {
            appSkin = new AppSkin(params);
            appSkin.createdBy = user;
            appSkin.lastUpdatedBy = user;
            if (appSkin.validate() && checkExtraInfo(appSkin)) {
                AppSkin.withTransaction { status ->
                    try {
                        imageUploadService.uploadBackgroundImagesForAppSkin(appSkin, null, deleteWhenFails,
                                params, request, flash);
                        imageUploadService.uploadDialogImagesForAppSkin(appSkin, null, deleteWhenFails,
                                params, request, flash);
                        appSkin.save();
                        flash.success = message(code: 'default.message.success');
                    } catch(Exception dbEx) {
                        // Catching general exception because of unexpecting grails exceptions
                        if(flash.error == null || flash.error.trim() == ""){
                            log.error("Error Saving a new Skin: ${dbEx.message}", dbEx);
                            flash.error = (dbEx.getMessage())?dbEx.getMessage():message(code: 'default.error.box.software.savingTheFile');
                        }
                        status.setRollbackOnly();
                    }
                }
            } else {
                flash.error = message(code: 'default.error.problem')
            }
        } catch( Exception ex) {
            if (StringUtils.isBlank(flash.error)) {
                flash.error = ex.getMessage();
                log.error("Error saving a new App Setting", ex);
            }
        }
        if (flash.error != null) {
            appSkin.backgroundImageUrl = null;
            rackspaceService.deleteMultipleFiles(deleteWhenFails);
            render(view: Constants.VIEW_CREATE, model: [entity: appSkin]);
        } else {
            redirect(action:Constants.VIEW_ASSIGN_LOCATION, id : appSkin.id);
        }
    }
	
	/**
	 * This method renders the default message when there's not 
	 * skins to show
	 * 
	 * @return
	 */
	def noSkins(){}
	
	/**
	 * Renders the view that shows all the fields of the Skin
	 * 
	 * @param id	Id of the Skin to be render
	 * 
	 * @return
	 */
	def show(long id)
	{
		def offset = this.getOffsetParameterToPagination();
		def appSkin=AppSkin.read(id);
	
		if (!appSkin) {
			//If the AppSkin don't exists redirect to the list with the error message
			flash.error = message(code: 'appSkin.not.found');
			redirect(action: Constants.VIEW_LISTALL)
		} else {
			if(checkAppSkinAccess(appSkin)){
				//If the User can see the AppSkin renders the view page
				[entity: appSkin]
			} else {
				//If the User cannot see the AppSkin redirect to the list with the error message
				flash.error = message(code: 'appSkin.not.found');
				redirect(action: Constants.VIEW_LISTALL)
			}
		}
	}
	
	/**
	 * Deletes an AppSkin
	 *
	 * @param id	Id of the AppSkin to be deleted.
	 *
	 * @return
	 */
	def delete(long id)
	{
		try {
			AppSkin appSkin=AppSkin.read(id);
			BoxesSyncResult boxesSyncResult = null;
			if (!appSkin || !checkAppSkinAccess(appSkin) ) {
				//If the AppSkin do not exists or don't has access
				flash.error = message(code: 'appSkin.not.found')
				redirect(action:Constants.VIEW_LISTALL)
			}else{
				String[] imagesUrls = [appSkin.backgroundImageUrl, appSkin.backgroundImageUrlTablet];
				def locations = appSkin.locations;
				//Set all associated location boxes to the default
				
				if(!locations.isEmpty()) {					
					boxesSyncResult = appSkinService.updateBoxesToDefaultOfLocations(locations);
					if(boxesSyncResult == null) {
					   flash.warn = message(code: 'general.no.communication.with.web.socket.api');
					} else {
					   if(boxesSyncResult.getErrorCounter() > 0){
						   flash.warn = message(code: 'boxes.syncErrorMessage');
					   }
					}
				} else {
					//There are not Locations associated
				}
				
				appSkin.delete();
				flash.success = message(code: 'default.app.skin.remove.success');
				
				//If the object is deleted from DB, delete the associated images
				rackspaceService.deleteMultipleFiles(imagesUrls);
				renderListViewWithSyncResult(boxesSyncResult);
				return;
			}
		} catch(Exception e){
			log.error("Error deleting the Skin with id=" + id + ": ${e.message}", e);
			throw e;
		}
	}
	
	/**
	 * Renders the page to allow the edition of a AppSkin
	 * 
	 * @param id	Id of the AppSkin to be edited.
	 * 
	 * @return
	 */
	def edit(long id)
	{
		def appSkin=AppSkin.read(id)
		def offset = this.getOffsetParameterToPagination()
		
		if (!appSkin || !checkAppSkinAccess(appSkin) ) {
			//If the AppSkin do not exists or don't has access
			flash.error = message(code: 'appSkin.not.found')
			redirect(action:Constants.VIEW_LISTALL)
		}else{
			[entity: appSkin]
		}
	}
		
	/**
	 * This method updates the information of the App Settings and will
	 * send the updated information to the associated Locations
	 * 
	 * @param id		Id of the App Skin to update
	 * @param version	Version to verifies UI request
	 * @return
	 */
	def update(long id, Long version) {
		def jsonTransactionStatus = null
		def offset = this.getOffsetParameterToPagination()
		BoxesSyncResult boxesSyncResult = null;
		def imagesUrlsToDelete = [];
		def deleteWhenFails = [];
		
		if ( params.size() > 3) { //if the only values are action and controller, then we are not coming from the form
			
			AppSkin appSkin=null;
			
			try {
				appSkin = AppSkin.read(id);
				
				if (appSkin.version > version) {
					
					flash.error=message(code: 'default.skin.edit.version.error');
					appSkin.errors.rejectValue("version", 'default.skin.edit.version.error');
					
				} else {
									
					appSkin.properties = params;
					appSkin.lastUpdatedBy = getCurrentUser();
															
					if(!appSkin.hasErrors() && appSkin.validate() && checkExtraInfo(appSkin) ) {
						
						AppSkin.withTransaction { status ->
							try {
								if (appSkin.save()){
									
									imageUploadService.uploadBackgroundImagesForAppSkin(appSkin, imagesUrlsToDelete, deleteWhenFails, params, request, flash);
							
									imageUploadService.uploadDialogImagesForAppSkin(appSkin, imagesUrlsToDelete, deleteWhenFails, params, request, flash);
									
									rackspaceService.deleteMultipleFiles(imagesUrlsToDelete);
																		
									def locations = CompanyLocation.findAllByActiveSkin(appSkin);
																		
									if(!locations.isEmpty()) {
										
										flash.success = message(code: 'save.object.success');
									   	boxesSyncResult = appSkinService.updateBoxesOfLocations(locations, appSkin);
									    if(boxesSyncResult == null) {
										   flash.warn = message(code: 'general.no.communication.with.web.socket.api');
									    } else {
										   if(boxesSyncResult.getErrorCounter() > 0){
											   flash.warn = message(code: 'boxes.syncErrorMessage');
										   }
									    }
										
									} else {
										//There are not Locations associated
										flash.success = message(code: 'sync.no.affected.locations');
									}
									
								}else{
									flash.error=message(code: 'default.error.problem');
								}
							} catch(Exception dbEx) {
								if(flash.error == null || flash.error.trim() == ""){
									dbEx.printStackTrace();
									flash.error = dbEx.getMessage()
								}
								status.setRollbackOnly()
							}//End of Catch						
						}						
					} else {
						flash.error=message(code: 'default.error.problem')
					}
				}
			} catch(Exception ex) {
				if(flash.error == null || flash.error.trim() == ""){
					log.error("Error updating the AppSkin", ex);
					flash.error=message(code: 'default.error.problem')
				}
			}
			def messageErrorDescriptionHash = messageErrorService.getMessageErrorDescription();
			if(flash.error != null) {
				rackspaceService.deleteMultipleFiles(deleteWhenFails);
				render(view: "edit", model: [entity: appSkin, boxesSyncResult: boxesSyncResult, messageErrorDescriptionHash: messageErrorDescriptionHash]);
			} else{
				renderListViewWithSyncResult(boxesSyncResult);
				return;
			}
		}else{
			flash.error = message(code: 'appSkin.not.found')
			redirect(action: Constants.VIEW_LISTALL, params: [offset: offset])
		}
	}
	
	/**
	 * This methods receives the result of a Sync and render
	 * the list page with the required objects
	 * 
	 * @param updatedLocations
	 * @param jsonTransactionStatus
	 * @return
	 */
	def renderListViewWithSyncResult(def boxesSyncResult){
		try{
		   def skinsList = getListOfCurrentUser();
		   
		   if(skinsList.size() == 0){
			   render(view: Constants.VIEW_NO_SKINS)
		   }else{
		   	   def messageErrorDescriptionHash  = messageErrorService.getMessageErrorDescription();
			   render(view: Constants.VIEW_LISTALL,
				   model:[	list: Utils.paginateList(skinsList,params), 
				   			boxesSyncResult:boxesSyncResult, 
						   	messageErrorDescriptionHash:messageErrorDescriptionHash])
		   }
		} catch( Exception e) {
			log.error "Error getting the list of Skins: ${e.message}", e
			throw e
		}
	}
	
    /**
     * Renders the main page where to assign an AppSkin to the venues
     * 
     * @param id
     *          If receives an Id then will use it as the default selected AppSkin after the render
     */
    def assignLocation(Long id) {
        def skins = getListOfCurrentUser();
        if (skins.size() > 0) {
            AppSkin appSkin = null;
            if (id) {
                appSkin = AppSkin.read(id);
            } else { // If not default required, just get the first one in the list
                appSkin = skins.get(0);
            }
            [appSkin: appSkin,  skins:Utils.sortByName(skins)]
        } else { // There are not AppSkins to associate, redirecto to another view
            redirect(action: Constants.VIEW_NO_SKINS)
        }
    }

    /**
     * This method renders the list of companies/venues with the checked venues for a selected AppSkin and the
     * associated venues
     */
    def ajaxAssignSkin() {
        final AppSkin appSkin = AppSkin.read(params.skinId);
        if (appSkin) {
            // Get the list of enabled Companies associated for current user role with enabled locations
            def listCompanies = userSessionService.getCompaniesForAssociation();
            if (checkAppSkinAccess(appSkin)) { // Check that the use is allow to manage this AppSkin
                String selectedLocations = "";
                for (location in appSkin.locations) {
                    selectedLocations += (selectedLocations.equals(""))? location.id:"," + location.id;
                }
                render (template:Constants.ASSIGN_SKIN_TEMPLATE, model:[appSkin:appSkin, companies: listCompanies, 
                    selectedLocations: selectedLocations]);
            } else {
                render(text: '<div class="alert alert-error">'
                    + message(code: 'default.not.found.message', 
                        args: [message(code: 'userInformation.label', default: 'User Information'), params.skinId])
                    + '</div>', contentType: "text/html", encoding: "UTF-8");
            }
        } else {
            flash.error = message(code: 'default.appSkin.not.found');
        }
    }

    /**
     * This method receives the information of the Locations that a Skin needs to be associated, will synchronize all 
     * the locations and update the associated boxes with the corresponding Skin (Default if was removed). After all the
     * process will render the page with the result including synchronization result if it was required.
     */
    def assign() {
        AppSkin appSkin = AppSkin.read(params.activeSkin);
        BoxesSyncResult boxesSyncResult = null;
        def paramLocations = params.list(Constants.PARAM_LOCATIONS);
        AppSkin.withTransaction { status ->
            try {
                def locationsToSave = (params.locationsToSave == null)? "":params.locationsToSave;
                def newList = locationsToSave.split(",");
                def locations = []
                locations += appSkin.locations;
                // Will store all the Locations affected by the update
                def affectedLocations = [];
                locations.each { location ->
                    if (!newList.any { it == (""+location.id) }) {
                        appSkin.removeFromLocations(location);
                        location.activeSkin = null;
                        if (location.save()) {
                            affectedLocations.add(location);
                        } else {
                            flash.error = message(code:'venue.assign.update.failed', args:[location.name]);
                            throw new Exception(location.errors.allErrors.join(' \n'));
                        }
                    }
                }
                // Add new ones
                for (c in newList) {
                    if (!appSkin.locations.any { it.id == (c) }) {
                        CompanyLocation loc = CompanyLocation.read(c)   
                        if (loc != null) {
                            loc.activeSkin = appSkin;
                            if (loc.save()){
                                appSkin.addToLocations(loc);
                                affectedLocations.add(loc);
                            } else {
                                flash.error = message(code:'venue.assign.update.failed', args:[loc.name]);
                                throw new Exception(loc.errors.allErrors.join(' \n'));
                            }
                        }
                    }
                }
                if (appSkin.save()) {//If not error saving the Object
                    if (!affectedLocations.isEmpty()) {
                        boxesSyncResult = appSkinService.updateBoxesOfLocations(affectedLocations, appSkin);
                        if (boxesSyncResult == null) {
                            //Api Communication error since not response
                            flash.warn = message(code: 'general.no.communication.with.web.socket.api');
                        } else {
                            //Api Communication works, check the result
                            if(boxesSyncResult.getErrorCounter() > 0) {
                                flash.warn = message(code:'boxes.syncErrorMessage');
                            } else {
                                flash.success = message(code: 'default.upated.message', args: [appSkin.name]);
                            }
                        }
                    } else {//There are not associated locations
                        flash.success = message(code: 'sync.no.affected.locations');
                    }
                } else {
                    flash.error = message(code: 'save.object.error');
                }
            } catch(Exception dbEx) {
                status.setRollbackOnly();
                if (StringUtils.isBlank(flash.error)) {
                    log.error(dbEx);
                    flash.error = message(code:'unexpected.error.please.try.again');
                }
            }
        }
        def skins = getListOfCurrentUser();
        def messageErrorDescriptionHash =  messageErrorService.getMessageErrorDescription();
        render (view:Constants.VIEW_ASSIGN_LOCATION,
            model:[appSkin: appSkin,  skins:Utils.sortByName(skins), boxesSyncResult:boxesSyncResult,
                messageErrorDescriptionHash: messageErrorDescriptionHash])
    }
	
	/**
	 * Check if the Current User can execute an action 
	 * with the AppSkin
	 * 
	 * @param appSkin	AppSkin to be checked
	 * 
	 * @return			True if it's allowed or False when it's not.
	 */
	def boolean checkAppSkinAccess(appSkin){
		/*def relatedSkins = []
		
		for(company in listCompanies){
			if(company.enable){
				for(location in company.locations){
					if(location.enable) {
						relatedSkins += location.appSkins
					}
				}
			}
		}
		
		def user = getCurrentUser()
		for(userAppSkin in user.createdAppSkins){
			relatedSkins += userAppSkin
		}
		
		return (relatedSkins.any { it in (appSkin) })
		*/
		
		//def listCompanies = getCompaniesForUser()
		//if(!allowedAppSkingAction(listCompanies, appSkin)){
		return true;
	}
	/*
	private def getPermitedSkins(){
		//def companies = getCompaniesForUser()
		//def skins = AppSkin.getSkinsByCompanies(companies.collect(){it.id}, getCurrentUser()).listDistinct()
		def listCompanies = userSessionService.getCompaniesForUser();
		return AppSkin.getSkinsByUser(getCurrentUser(),listCompanies).list()
	}*/
	
	public def syncronize() {
		def appSkin = AppSkin.read(params.entityId.toInteger());
		def skins = getListOfCurrentUser();
		
		def jsonTransactionStatus = null;
		def locations = appSkin.locations;
		List<String> updatesWithError = null;
		log.info( "Locations: " + locations);
		if(!locations.isEmpty()) {
			//jsonTransactionStatus = customButtonService.updateBoxesOfLocations(locations)
			//customButtonsInfo = customButtonService.getCustomButtonNames(locations)
			updatesWithError = appSkinService.updateBoxesOfLocations(locations, appSkin);
		}
		
		if(updatesWithError == null && !locations.isEmpty()) {
			flash.error = message(code: 'mycentralserver.custombuttons.syncErrorMessage');
		} else {
			flash.success=message(code: 'default.message.success');
		}
						
		render(view: "assignLocation", model: [ appSkin: appSkin,
			skins: Utils.sortByName(skins),
			syncResults:updatesWithError]);
		
	} // END syncronize
	
	def getListOfCurrentUser(){
		def tempResults = null;
		def waList;
		if(isAdminOrHelpDesk()){
			//Admin will see All the List of Contents
			def query = AppSkin.where{
				};
			tempResults = query.list().unique{ it.id };
			waList = Utils.sortByName(tempResults);
		} else {
			//tempResults = Content.getContentsByCompanies(listCompanies.collect(){it.id}, getCurrentUser());
			def listCompanies = userSessionService.getCompaniesForUser();
			waList = Utils.sortByName(
				AppSkin.getSkinsByUser(userSessionService.getCurrentUser(), listCompanies.collect(){it.id}).list().unique{ it.id })
		}
		
		return waList;
	}
    
    /**
     * Validates some data of an AppSkin and assigns found errors to the fields or to the flash.error message
     *
     * @param appSkin
     *              AppSkin to do validations
     * @return True when no errors, False if any
     */
    def checkExtraInfo(AppSkin appSkin){
        boolean isOk = true;
        if (ValidationHelper.isInvalidColor(appSkin.primaryColor)) {
            isOk = false;
            appSkin.errors.rejectValue('primaryColor', message(code:'invalid.color', args:[message(code:'default.field.main.color')]));
        }
        if (ValidationHelper.isInvalidColor(appSkin.secondaryColor)) {
            isOk = false;
            appSkin.errors.rejectValue('secondaryColor', message(code:'invalid.color', args:[message(code:'default.field.secondary.color')]));
        }
        return isOk;
    }
}
