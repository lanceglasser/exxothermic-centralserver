package mycentralserver

import org.codehaus.groovy.grails.commons.GrailsApplication

import grails.util.GrailsUtil

/**
 * Controller that handles the error and the render that must be executed depending of the environment for some cases
 * 
 * @author Cecropia Solutions
 *
 */
class ErrorController {

    def index() {
        final Exception exception = request.exception;
        log.error(exception);
        if ( grails.util.Environment.PRODUCTION == grails.util.Environment.getCurrent()  ) {
            // Production: show a nice error message
            render(view:'production')
        } else {
            // Not it production? show an ugly, developer-focused error message
            render(view:'development')
        }
    }

    def notfound(){
        render(view:'404_dev')
    }

    def notauthorized(){
        render(view:'403_dev')
    }
}