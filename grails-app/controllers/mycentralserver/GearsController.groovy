package mycentralserver

import grails.converters.JSON
import grails.plugins.rest.client.RestBuilder
import grails.plugins.rest.client.RestResponse;

import java.awt.GraphicsConfiguration.DefaultBufferCapabilities;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path
import java.nio.file.Paths
import java.security.InvalidParameterException;
import java.security.MessageDigest
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import groovy.io.FileType;
import mycentralserver.app.Affiliate;
import mycentralserver.app.AppSkin;
import mycentralserver.box.Box;
import mycentralserver.box.BoxBlackList;
import mycentralserver.box.BoxIp;
import mycentralserver.box.BoxManufacturer;
import mycentralserver.box.BoxStatus
import mycentralserver.box.BoxUnregistered
import mycentralserver.box.software.BoxSoftware
import mycentralserver.company.CompanyLocation
import mycentralserver.custombuttons.CustomButton;
import mycentralserver.generaldomains.Configuration;
import mycentralserver.user.User;
import mycentralserver.utils.Constants;
import mycentralserver.utils.EncrypterHelper;
import mycentralserver.utils.FileHelper
import mycentralserver.utils.PartnerEnum;
import mycentralserver.utils.PasswordGeneratorHelper;
import mycentralserver.utils.SerialNumberGeneratorHelper;
import mycentralserver.utils.BoxStatusEnum;
import mycentralserver.utils.Utils;
import mycentralserver.utils.enumeration.Architecture;
import mycentralserver.utils.enumeration.AudioCardGeneration;
import mycentralserver.utils.enumeration.AudioInputMode;
import mycentralserver.utils.enumeration.JumperConfiguration;

import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile

import java.util.Iterator;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import mycentralserver.box.BoxChannel;
import mycentralserver.box.BoxNotification;

/**
 * This class controls the communication between Communication and CentralServer
 * @author David
 *
 */
class GearsController {

    /* Inject all required services */
    def grailsApplication
    def boxService
    def contentService
    //def customButtonService
    def restClientService
    def boxIpService
    def boxBlackListService
    def appSkinService
    def welcomeAdService
    def boxUserService
    def documentService
    def gearsService
    def softwareUpgradeService
    def boxConfigService
    def deviceService;
    def boxSoftwareService;
    def softwareUpdateControlService;

    def private STATUS_SUCCESSFUL = "Successful";
    private final static String DEFAULT_APPEND = "default_";
    private final static String DEFAULT_COLOR = '#000000';
    private final static String HASH_TEXT = "hash_";

    public static enum Status {
        STATUS_EXCEPTION("Exception"),
        STATUS_SUCCESSFUL("Successful"),
        STATUS_FAILED("Failed");

        private final String status;

        private Status(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }

        public static Status getByName(String name) {
            for (Status prop : values()) {
                if (prop.getStatus().equals(name)) {
                    return prop;
                }
            }
            throw new IllegalArgumentException(name + " is not a valid PropName");
        }
    }

    public static enum ErrorCode {
        NO_ERROR("0"),
        TIME_OUT("1"),
        GENERAL_EXCEPTION("2"),
        NO_CHANNELS_FOUND("3"),
        NO_DEVICE_FOUND("4"),
        UPDATE_SOFTWARE_FAILED("5"),
        BLACK_LISTED("6"),
        INVALID_PARAMETERS("7"),
        INVALID_CERTIFICATE("8")

        private final String errorCode;

        private ErrorCode(String errorCode) {
            this.errorCode = errorCode;
        }

        public String getErrorCode() {
            return errorCode;
        }

        public static ErrorCode getByErrorCode(String name) {
            for (ErrorCode prop : values()) {
                if (prop.getErrorCode().equals(name)) {
                    return prop;
                }
            }
            throw new IllegalArgumentException(name + " is not a valid PropName");
        }
    }

    static allowedMethods =[register:'POST',
        alertDeviceDisconnect:'POST',
        getContents:'POST',
        registerLastDeviceConnection:'POST',
        getChannelsSaved:'POST',
        registerChannels: 'POST',
        generateCertificate: 'GET']

    /**
     * This method handles the general process of a request from the Communication component
     *
     * @param action
     *              Action to execute by request
     * @param serial
     *              Serial of the ExXtractor executing the action
     * @param paramsMap
     *              Params in a Map depending of specific requirements
     * @return Result of the request
     */
    private def processCommRequest(GearsService.CommRequestAction action, String serial, HashMap<String, Object> paramsMap){
        log.debug("" + action + " :: " + serial + " :: [" + paramsMap + "]" );
        HashMap jsonMap = null;
        try {
            jsonMap = gearsService.processRequestFromComm(action, serial, paramsMap);
        } catch(Exception ex) {
            log.error("When execute " + action + " [" + serial + "]", ex);
            jsonMap = getJsonMapAfterException(serial, ex.getMessage());
        }
        def jsonToSend = (jsonMap as JSON);
        log.info("" + action + " sending jsonResponse: [" + jsonToSend + "]");
        render jsonToSend;
    }

    def register()  {
        HashMap jsonMap= new HashMap();
        jsonMap.registered = false;
        def results = true;
        def error = null;
        try {
            log.info " GearSoftware - Begin to register -> Serial: "  + params.serial + " Version" + params.version + " Bios" + params.bios
            //find if the box already exist
            final String serial = params.serial;
            Box box = null;
            final BoxSoftware boxSoftware = boxSoftwareService.findByLabelAndArchitecture(serial, params.version);
            final String model = params.model;
            final String bios = params.bios;
            final boolean inBlackList = boxBlackListService.isBlackListed(serial);
            softwareUpdateControlService.changeControlAfterBoxConnect(serial, params.version);
            (box, results) = boxService.validateBoxConnection(serial, inBlackList, params.version);
            if (box) {
                try {
                    jsonMap.registered = true;
                    box.status = BoxStatus.findByDescription(BoxStatusEnum.CONNECTED.value);
                    box.versionSO = params.version;
                    box.model = model;
                    box.bios = bios;
                    if (boxSoftware) {
                        box.softwareVersion = boxSoftware;
                    } else {
                        log.warn("GearController- Unknow version: " + params.version + " SerialNumber : " + serial);
                    }
                    Box.withTransaction { status->
                        try{
                            //check the blackList and dates if stopWorking date is greater than actual date the Box is going to be register
                            log.info " GearController - register check BoxBlackList-> Serial: "  + params.serial
                            box.lastUpdated = new Date();
                            if (box.save() && !inBlackList) {
                                log.info " GearSoftware - Saving registration -> Serial: "  + params.serial + " [REGISTERED sucessful]"
                                gearsService.setResultInJsonMap(jsonMap, true,  "Already Registered serial: " + serial, ErrorCode.NO_ERROR)
                                render(jsonMap as JSON);
                                return;
                            } else {
                                log.error " GearRegister - Saving registration -> Serial: "  + params.serial + " [REGISTERED fail]"
                                results = false;
                                gearsService.setResultInJsonMap(jsonMap, false,  "Can't register the" + serial, ErrorCode.GENERAL_EXCEPTION )
                                render(jsonMap as JSON);
                                return;
                            }
                        } catch(Exception dbEx) {
                            log.error("Error registering a box", dbEx);
                            status.setRollbackOnly();
                            flash.error = dbEx.getMessage();
                        }
                    }//End transaction
                } catch(Exception ex) {
                    log.error(params.serial, ex);
                    results = false;
                }
            }
        } catch(Exception ex) {
            results = false;
            error = ex.message;
            log.error("Error registering a box connection: " + params.serial, ex)
        }
        gearsService.setResultInJsonMap(jsonMap, results,
            results ? "Registered Serial->" + params.serial : "General Exception Serial->" + params.serial,
            results ? ErrorCode.NO_ERROR : ErrorCode.GENERAL_EXCEPTION);
        def jsonToSend = (jsonMap as JSON);
        log.info("register sending jsonResponse: [" + jsonToSend + "]");
        render jsonToSend;
    }

    /**
     * Checks if a Box is already connected
     *
     * @param box
     *          Box to check the status
     * @return True or false depending if the box is connected or not
     */
    def checkBoxStatus(Box box){
        if (box) {
            return box.status.equals(BoxStatus.findByDescription(BoxStatusEnum.CONNECTED.value))
        }
        return false
    }

    def stats() {
    }

    /**
     * Updates the status of the box to disconnected
     *
     * @param serial
     *          Serial number of the box to disconnect
     */
    def alertDeviceDisconnect(String serial) {
        try {
            Box box = Box.findBySerial(serial);
            if ( box ) {
                if(boxService.updateBoxStatus(serial)) {
                    renderAlertDeviceDisconnectResponse("true", Status.STATUS_SUCCESSFUL.getStatus(),
                        "Already Registered", ErrorCode.NO_ERROR.getErrorCode());
                } else {
                    renderAlertDeviceDisconnectResponse("false", Status.STATUS_FAILED.getStatus(),
                        "Can't disconnect the serial: " +  serial, ErrorCode.GENERAL_EXCEPTION.getErrorCode());
                }
            } else { // Not found on Box table, try to find on Unregistered
                BoxUnregistered boxUnregistered = BoxUnregistered.findBySerial(serial);
                if (boxUnregistered) {
                    boxUnregistered.lastUpdated =  new Date();
                    boxUnregistered.save();
                    renderAlertDeviceDisconnectResponse("false", Status.STATUS_FAILED.getStatus(),
                        "Box " + serial +" unregistered", ErrorCode.NO_DEVICE_FOUND.getErrorCode());
                } else { // Not found on BoxUnregistered table neither
                    renderAlertDeviceDisconnectResponse("false", Status.STATUS_FAILED.getStatus(),
                        "Box " + serial + "not found", ErrorCode.NO_DEVICE_FOUND.getErrorCode());
                }
            }
        } catch(Exception e) {
            log.error("Exception", e);
            renderAlertDeviceDisconnectResponse("false", Status.STATUS_FAILED.getStatus(),
                "Can't disconnect the serial: " +  serial, ErrorCode.GENERAL_EXCEPTION.getErrorCode());
        }
    }
    
    private renderAlertDeviceDisconnectResponse(String result, String status, String msg, String errorCode) {
        render(contentType: 'text/json') {[
            'results': result,
            'status': status,
            'statusDescription': msg,
            'error': errorCode
        ]}
    }

    /**
     * Register the HB of a ExXtractor
     */
    def registerLastDeviceConnection(String serial, String pamode, String debugLevel) {
        HashMap jsonMap = new HashMap();
        Box box = Box.findBySerial(serial);
        def boxUnregistered;
        def statusDescription;
        boolean results = false;
        if( box ) { // The Box exists
            box.lastUpdated = new Date();
            box.status = BoxStatus.findByDescription(BoxStatusEnum.CONNECTED.value);
            try {
                if (pamode.equals(Constants.PARAM_MASTER)) {
                    //Set all the other boxes of the same Location as relay and this one as PA
                    boxService.removePAFromBoxesByLocation(box);
                    box.pa = true;
                } else if (pamode == Constants.PARAM_RELAY || pamode == Constants.PARAM_OFF) {
                    box.pa = false;
                }
                if (box.save(flush:true)) {
                    boxConfigService.saveBoxConfigurations(box, Constants.BOX_CONF_CODE_DEBUG_LEVEL, debugLevel);
                    softwareUpgradeService.checkSoftwareUpgradeStatus(box);
                    log.info("RegisterLastDeviceConnection -> Serial: "  +
                        params.serial + " [status update successful]");
                    results = true;
                } else {
                    log.error("RegisterLastDeviceConnection -> Serial: " + params.serial + " [status update FAIL]");
                }
                jsonMap = gearsService.setResultInJsonMap(jsonMap, results,
                    "Registered LastDeviceConnection", ErrorCode.GENERAL_EXCEPTION );
            } catch( Exception ex ) {
                log.error("RegisterLastDeviceConnection -> Serial: "  + params.serial + " [status update FAIL]", ex);
                jsonMap = gearsService.setResultInJsonMap(jsonMap, results,
                    "Error Registered LastDeviceConnection", ErrorCode.GENERAL_EXCEPTION );
            }
        } else { // The box is not registered; review the Unregistered table
            log.info(" GearSoftware - registerLastDeviceConnection -> Serial: "  + params.serial +
                " [IS NOT REGISTERED]");
            boxUnregistered = BoxUnregistered.findBySerial(serial);
            if (boxUnregistered) {
                boxUnregistered.lastUpdated = new Date();
                boxUnregistered.save();
            }
            statusDescription = "Box " + serial + (boxUnregistered? " unregistered":" not found");
            jsonMap = gearsService.setResultInJsonMap(jsonMap, results, statusDescription,
                ErrorCode.GENERAL_EXCEPTION );
            jsonMap.serialNumber = serial;
            jsonMap.customButtons = null;
        }
        def jsonToSend = (jsonMap as JSON);
        render jsonToSend;
    }

    /**
     * This method Upload myBox'Log File sent to mycentralserver the request must be MultipartHttpServletRequest.
     */
    def uploadLogFile() {
        try {
            log.info("Uploading log file for " + params.serial);
            final String serial = params.serial;
            String fileName;
            Box box = Box.findBySerial(serial);
            def file = request.getFile('sendfile');
            final String fileNameOrigin = file.getOriginalFilename();
            if(box) {
                fileName = box.id + "_" + box.serial + "_" + fileNameOrigin;
            } else {
                BoxUnregistered boxUnregistered = BoxUnregistered.findBySerial(serial);
                if(boxUnregistered) {
                    fileName = boxUnregistered.id + "_" + boxUnregistered.serial + "_" + fileNameOrigin;
                } else {
                    response.sendError(404, 'Not Found');
                    return;
                }
            }
            final String storagePath = grailsApplication.config.basePathMyBoxLogFiles;
            final String path = storagePath + "/" + fileName;
            file.transferTo( new File(path) );
            response.sendError(200, 'Done');
        } catch (Exception ex){
            log.error(ex);
            response.sendError(500, 'Error uploading the log file')
        }
    }

    /**
     * Register the list of channels of a Box synchronizing the list againts the list on DB
     */
    def registerChannels() {
        HashMap jsonMap = new HashMap();
        try {
            def listChannels = null;
            def isPA = false;
            def jsonMessage = request.JSON;
            log.info "**** RegisterChannels: " + jsonMessage;
            def serialNumber = jsonMessage.serialNumber;
            def box = boxService.findBoxBySerialNumber(serialNumber);
            if (box) {
                (listChannels , isPA) = boxService.updateChannels(serialNumber, jsonMessage);
                listChannels = listChannels.asList().sort{it.channelNumber};
                // Synchronize the channels agains DB
                boxService.updateDBChannels(box, listChannels, isPA, -1)
            }
            jsonMap.channelsStatus = boxService.returnJSon(listChannels);
            response.setStatus(HttpStatus.OK.value);
        } catch(Exception ex) {
            log.error("Error registering the channels of a box", ex);
            response.setStatus(HttpStatus.BAD_REQUEST.value)
        }
        render( jsonMap as JSON);
    }

    /**
     * This method receives all the require parameters for the generation of an ExXtractor certificate; if all is
     * correct generates the certificate by calling the communication component and returns the new certificate
     * string as part of the JSON response.
     */
    def generateCertificate() {
        HashMap jsonMap = new HashMap();
        try {
            final String secretValue = grailsApplication.config.encryptionKey;
            final String ivValue = grailsApplication.config.encryptionIv;
            def hmac = request.getHeader(Constants.PARAM_HMAC);
            def token = params.token;
            def serial = null;
            def legacySerial = params.serial;
            // If Partner, Architecture, AudioCardGeneration and JumperConfiguration parameters are received must be
            // valid values; if not then the default for each one is used
            final String partnerCode = StringUtils.isBlank(params.partner)?
                        PartnerEnum.getDefaultPartnerCode():params.partner.trim();
            final Affiliate affiliate = Affiliate.findByCode(partnerCode.toUpperCase());
            final Architecture architecture = StringUtils.isBlank(params.architecture)?
                        Architecture.getDefault():Architecture.codeOf(params.architecture);
            final AudioCardGeneration audioCardGeneration = StringUtils.isBlank(params.audioCardGeneration)?
                        AudioCardGeneration.getDefault():AudioCardGeneration.findByStringValue(params.audioCardGeneration);
            final JumperConfiguration jumperConfiguration = StringUtils.isBlank(params.jumperConfiguration)?
                    JumperConfiguration.getDefaultByAudioGeneration(audioCardGeneration):
                    JumperConfiguration.findByName(params.jumperConfiguration);
            final AudioInputMode audioInputMode = StringUtils.isBlank(params.audioInputMode)?
                    AudioInputMode.getDefaultByArchitecture(architecture):AudioInputMode.codeOf(params.audioInputMode);
            if (hmac == null || token == null) {
                jsonMap = gearsService.setResultInJsonMap(jsonMap,
                    false, message(code:'generate.certificate.error.hmac.token'), ErrorCode.INVALID_PARAMETERS);
                response.setStatus(Constants.BAD_REQUEST);
            } else if (architecture == null) {
                jsonMap = gearsService.setResultInJsonMap(jsonMap,
                    false, message(code:'generate.certificate.error.invalid.parameter',
                        args:[Constants.PARAM_ARCHITECTURE, params.architecture, Architecture.printNamesList()]),
                    ErrorCode.INVALID_PARAMETERS);
                response.setStatus(Constants.BAD_REQUEST);
            } else if (affiliate == null) {
                jsonMap = gearsService.setResultInJsonMap(jsonMap,
                    false, message(code:'generate.certificate.error.invalid.parameter',
                        args:[Constants.PARAM_PARTNER, params.partner, PartnerEnum.getCodesListAsString()]),
                    ErrorCode.INVALID_PARAMETERS);
                response.setStatus(Constants.BAD_REQUEST);
            } else if (audioCardGeneration == null) {
                // If received but not valid audio card generation must return an error
                jsonMap = gearsService.setResultInJsonMap(jsonMap, false,
                    message(code:'generate.certificate.error.invalid.parameter',
                        args:[Constants.PARAM_AUDIO_CARD_GENERATION, params.audioCardGeneration, AudioCardGeneration.printValuesList()]),
                    ErrorCode.INVALID_PARAMETERS );
                response.setStatus(Constants.BAD_REQUEST);
            } else if (jumperConfiguration == null) { // The jumper configuration can't be set, invalid parameter
                jsonMap = gearsService.setResultInJsonMap(jsonMap, false,
                    message(code:'generate.certificate.error.invalid.parameter',
                        args:[Constants.PARAM_JUMPER_CONFIGURATION, params.jumperConfiguration, JumperConfiguration.printNamesList()]),
                    ErrorCode.INVALID_PARAMETERS );
                response.setStatus(Constants.BAD_REQUEST);
            } else if (audioInputMode == null) { 
                jsonMap = gearsService.setResultInJsonMap(jsonMap, false,
                    message(code:'generate.certificate.error.invalid.parameter',
                        args:[Constants.PARAM_AUDIO_INPUT_MODE, params.audioInputMode, AudioInputMode.printNamesList()]),
                    ErrorCode.INVALID_PARAMETERS );
                response.setStatus(Constants.BAD_REQUEST);
            } else { // All the parameters are ok
                final boolean itsLegacy = (legacySerial && StringUtils.isNotBlank(legacySerial));
                final String hmacEncrypt = EncrypterHelper.encryptWithAes(secretValue, ivValue, token);
                if (hmacEncrypt.equals(hmac)) {
                    boolean existSerial = true;
                    while (existSerial) {
                        serial = SerialNumberGeneratorHelper.generateSerial(affiliate.getCode(), architecture.getValue());
                        existSerial = existSerialNumber(serial);
                    }
                    log.info("generateCertificate the New Serial: [" + serial + "] " + " for HMAC[" + hmac +"] TOKEN:[" + token + "]" );
                    final String hmacCS = EncrypterHelper.encryptWithAes(secretValue, ivValue, serial);
                    def json = restClientService.generateCertificate(serial, hmacCS);
                    if(!json) {
                        jsonMap = gearsService.setResultInJsonMap(jsonMap, false,
                                message(code:'generate.certificate.error.restfull.failed'),
                                ErrorCode.GENERAL_EXCEPTION );
                        response.setStatus(Constants.INTERNAL_ERROR);
                    } else {
                        final User user = User.findByEmail(Constants.ADMIN_USER_EMAIL);
                        BoxManufacturer boxManufacturer = null;
                        if (itsLegacy) {
                            //If it's legacy, must get the Box by serial and update the serial values over
                            //all the DB
                            boxManufacturer = boxService.changeBoxSerialFromLegacy(legacySerial.trim(), serial);
                            if(boxManufacturer == null){
                                throw new Exception(message(code:'generate.certificate.error.legagy.move.failed'));
                            }
                            boxManufacturer.certificateLocation = json.certificationLocation;
                            boxManufacturer.certificateFileName = json.certificationFileName;
                            boxManufacturer.architecture = architecture.getValue();
                        } else {
                            boxManufacturer = new BoxManufacturer( serialNumber:serial,
                                certificateLocation: json.certificationLocation,
                                certificateFileName:json.certificationFileName,
                                architecture:architecture.getValue(),
                                createdBy:user);
                        }
                        boxManufacturer.audioCardGeneration = audioCardGeneration.getValue();
                        boxManufacturer.jumperConfiguration = jumperConfiguration.getValue();
                        boxManufacturer.audioInputMode = audioInputMode.getCode();
                        boxManufacturer.managementPasscode = SerialNumberGeneratorHelper.generateManagementPasscode();
                        if (boxManufacturer.validate()) {
                            BoxManufacturer.withTransaction { status ->
                                try {
                                    boxManufacturer.save()
                                    jsonMap.certificate = json.certificate;
                                    jsonMap.certificationLabel = json.certificationLabel;
                                    jsonMap.serial = serial;
                                    jsonMap.audioCardGeneration = audioCardGeneration.getValue();
                                    jsonMap.jumperConfiguration = jumperConfiguration.getName();
                                    jsonMap.audioInputMode = audioInputMode.getCode();
                                    jsonMap.managementPasscode = boxManufacturer.managementPasscode;
                                    jsonMap = gearsService.setResultInJsonMap(jsonMap, true,  null, ErrorCode.NO_ERROR)
                                    response.setStatus(Constants.OK);
                                } catch(Exception dbEx) {
                                    // We are not expecting an exception but grails can trigger unexpected exceptions some times
                                    if(itsLegacy){//If it's legacy must revert the update of the serial
                                        boxService.changeBoxSerialFromLegacy(serial, legacySerial.trim());
                                    }
                                    log.error dbEx;
                                    status.setRollbackOnly()
                                    jsonMap = gearsService.setResultInJsonMap(jsonMap, false, null, ErrorCode.GENERAL_EXCEPTION)
                                    response.setStatus(Constants.INTERNAL_ERROR);
                                }
                            } // END  BoxManufactor.withTransaction
                        } else {
                            boxManufacturer.errors.allErrors.each {log.error it}
                            jsonMap = gearsService.setResultInJsonMap(jsonMap, false,
                                message(code:'generate.certificate.error.box.cannot.entered'),
                                ErrorCode.GENERAL_EXCEPTION)
                            response.setStatus(Constants.INTERNAL_ERROR);
                        }
                    }
                } else {
                    jsonMap = gearsService.setResultInJsonMap(jsonMap, false,
                        message(code:'generate.certificate.error.hmac.token.invalid'),
                        ErrorCode.INVALID_PARAMETERS);
                    response.setStatus(Constants.BAD_REQUEST);
                }
            }
        } catch (Exception ex) {
            // We are not expecting an exception but grails can trigger unexpected exceptions some times
            log.error(ex);
            jsonMap = gearsService.setResultInJsonMap(jsonMap, false,  null, ErrorCode.GENERAL_EXCEPTION);
            response.setStatus(Constants.INTERNAL_ERROR);
        }
        def jsonToSend = (jsonMap as JSON);
        log.info(" generateCertificate sending jsonResponse: [" + jsonToSend + "]");
        render jsonToSend;
    }

    /**
     * This method returns a Json message with the information of the default
     * skin and some other configuration data using the partner parameter.
     */
    def getImageConfig() {
        HashMap jsonMap = new HashMap();
        String partnerCode = "";
        AppSkin appSkin = null;
        def configurations = null;
        def jsonToSend = null;
        if (StringUtils.isNotBlank(params.partner)) {
            partnerCode = params.partner.trim();
            appSkin = AppSkin.findByName(DEFAULT_APPEND + partnerCode);
            if (appSkin == null) {//Not found for partner; try to get EXX default
                appSkin = AppSkin.findByName(DEFAULT_APPEND + PartnerEnum.getDefaultPartnerCode());
            }
        } else { // No partner parameter, try to get the Default skin
            appSkin = AppSkin.findByName(DEFAULT_APPEND + PartnerEnum.getDefaultPartnerCode());
        }
        jsonMap = generateJsonResponseOfSkin(appSkin);
        // Try to find the Affiliate by Code, if not exists will use the default
        Affiliate affiliate = Affiliate.findByCode(partnerCode);
        affiliate = affiliate? affiliate:Affiliate.findByCode(PartnerEnum.getDefaultPartnerCode());
        jsonMap.put(Constants.PARAM_TECH_USER_NAME, affiliate.boxUsername);
        jsonMap.put(Constants.PARAM_PARTNER_ID, ""+affiliate.id);
        jsonMap.put(Constants.PARAM_ANDROID_DOWNLOAD_URL, affiliate.androidAppUrl);
        jsonMap.put(Constants.PARAM_IOS_DOWNLOAD_URL, affiliate.iosAppUrl);
        configurations = Configuration.findAllByBoxConfig(true);
        for(Configuration configuration:configurations){
            jsonMap.put(configuration.code, configuration.value);
        }
        jsonMap.put(Constants.PARAM_DEVICES, deviceService.getDevicesWithSmallerPackagesHashMap());
        jsonToSend = (jsonMap as JSON);
        log.info(" getDefaultSkin jsonResponse: [" + jsonToSend + "]");
        render jsonToSend;
    }

    /**
     * Includes into a Map the information of the received AppSkin
     *
     * @param appSkin
     *              AppSkin to be use for the generation of the Map
     * @return Map with the AppSkin information including primary and secondary color and ChannelInfoEnable
     */
    private HashMap generateJsonResponseOfSkin(AppSkin appSkin){
        HashMap skinMap = new HashMap();
        if(appSkin != null){ // Exists the Default Skin
            skinMap.put(AppSkinService.PRIMARY_COLOR, appSkin.primaryColor);
            skinMap.put(AppSkinService.SECONDARY_COLOR, appSkin.secondaryColor);
            skinMap.put(AppSkinService.CHANNEL_INFO_ENABLED, appSkin.channelInfoEnable);
        } else { // This shouldn't happen but we included some defaults just in case
            skinMap.put(AppSkinService.PRIMARY_COLOR, DEFAULT_COLOR);
            skinMap.put(AppSkinService.SECONDARY_COLOR, DEFAULT_COLOR);
            skinMap.put(AppSkinService.CHANNEL_INFO_ENABLED, true);
        }
        return skinMap;
    }

    /**
     * Executes the getExXtractorImage method; this is just for legacy.
     */
    def getLatestExXtractorImage() {
        redirect(action:'getExXtractorImage', params : params);
    }

    def getExXtractorImage() {
        HashMap jsonMap = new HashMap();
        def isSuccessful = false;
        def statusResponse = null;
        def description = null;
        def error = null;
        try {
            // Validate parameters
            // Revision: The default is 0 for latests version
            int revision = 0;
            if (params.revision) {
                try {
                    revision = Integer.parseInt(params.revision);
                    if(revision > 0){
                        throw new NumberFormatException(message(code:'manufacturing.script.invalid.revision'));
                    }
                } catch(NumberFormatException e) {
                    description = message(code:'manufacturing.script.invalid.revision') + ", received: " + params.revision;
                    throw new InvalidParameterException(description);
                }
            }
            //Arch: The default is X32
            String archPath = "/" + Architecture.X32.toString();
            if (params.architecture) {
                Architecture architecture = Architecture.codeOf(params.architecture);
                if (architecture) {
                    archPath = "/" + architecture.toString();
                } else {
                    description = message(code:'manufacturing.script.invalid.architecture') + ", received: " + params.architecture;
                    log.info('Invalid value of arch parameter: ' + params.architecture);
                    throw new InvalidParameterException(description);
                }
            }
            // Partner: The default is EXX
            String partnerPath = "/" + PartnerEnum.getDefaultPartnerCode();
            if (params.partner) {
                final Affiliate affiliate = Affiliate.findByCode(params.partner);
                if(affiliate){
                    partnerPath = "/" + params.partner;
                } else {
                    //If not exists must throw an error
                    description = message(code:'partner.not.exists', args:[params.partner]);
                    throw new InvalidParameterException(description);
                }
            }
            // Obtain data for the latest available image
            int latestImageVersion = -1;
            String path = grailsApplication.config.basePathImageFiles + partnerPath + archPath;
            File directory = new File(path);
            List<Integer> versionsList = new ArrayList<Integer>();
            HashMap<Integer, String> versionFiles = new HashMap<Integer, String>();
            def fileName;
            //For each file in the directory:
            directory.eachFileRecurse(FileType.FILES) { file ->
                fileName = file.getName();
                //Get version
                if (fileName.startsWith(HASH_TEXT)) {
                    log.debug("Ignoring hashfile: [" + fileName + "]");
                    return; // Works as a continue
                }
                log.debug(" getExXtractorImage checking file: [" + fileName + "]");
                def arraySplit = fileName.split('_');
                if (arraySplit.size() == 2) {
                    def (name, imageVersion) = arraySplit;
                    // Add to the list of versions for later check of desire revision
                    versionsList.add(imageVersion.toInteger());
                    versionFiles.put(imageVersion.toInteger(), fileName);
                    // If latest, get hash
                    if (imageVersion.toInteger() > latestImageVersion.toInteger()) {
                        latestImageVersion = imageVersion.toInteger();
                    }
                } else {
                    log.debug("Ignoring file: [" + fileName + "] because doesn't comply with convention");
                }
            }
            Collections.sort(versionsList);
            final int imageIndex = versionsList.size() - 1 + revision;
            if (imageIndex < 0) {
                // Throw error because the revision do not exists.
                description = message(code:'revision.not.exists');
                throw new Exception(message(code:'revision.not.exists'));
            } else {//Exists, get information.
                log.debug "List of versions: " + versionsList + ", must get revision " + revision + " and will be: " + versionsList.get( imageIndex );
                // Generate hash
                fileName = versionFiles.get(versionsList.get(imageIndex));
                String filePath = path+"/" + HASH_TEXT + fileName;
                def fileHash
                new File(filePath).withReader { fileHash = it.readLine() };
                jsonMap.imageHash = fileHash;
                jsonMap.imageFile = partnerPath + archPath + "/" + fileName;
            }
        } catch (InvalidParameterException e) {
            jsonMap = gearsService.setResultInJsonMap(jsonMap, false, description, ErrorCode.INVALID_PARAMETERS);
            response.setStatus(Constants.BAD_REQUEST);
        } catch (Exception ex) {
            log.error ex;
            jsonMap = gearsService.setResultInJsonMap(jsonMap, false, description, ErrorCode.GENERAL_EXCEPTION);
            response.setStatus(Constants.INTERNAL_ERROR);
        }
        def jsonToSend = (jsonMap as JSON);
        log.info(" getExXtractorImage sending jsonResponse: [" + jsonToSend + "]");
        render jsonToSend;
    }

    /**
     * Generates a certificate for old Boxes, then saves its old serialNumber with the new serialNumber
     */
    def generateCertificateLegacyBox() {
        HashMap jsonMap = new HashMap();
        def isSuccessful = false;
        def statusResponse = null;
        def description = null;
        def error = null;
        try {
            String serial = null;
            String legacySerial = params.serial;
            boolean existSerial = true;
            log.info("generateCertificateLegacyBox the Legacy Serial: [" + legacySerial + "] ");
            Box box = Box.findBySerial(legacySerial);
            BoxManufacturer boxManufacturer = BoxManufacturer.findBySerialNumberOrLegacySerialNumber(legacySerial,legacySerial);
            // If the serial was a certificate
            if (boxManufacturer) {
                if (box) {
                    BoxBlackList boxBlackList = new BoxBlackList();
                    boxBlackList.serial = boxManufacturer.serialNumber;
                    boxBlackList.dateStopWorking = new Date();
                    boxBlackList.dateCreated = new Date();
                    boxBlackList.save();
                    log.warn("generateCertificateLegacyBox  is incorrect because it was generated Legacy Serial:[" + legacySerial + "] ")
                }
                jsonMap = gearsService.setResultInJsonMap(jsonMap, false,
                    "Can't execute the call to refullService in CentralServer", ErrorCode.GENERAL_EXCEPTION);
                response.setStatus(Constants.INTERNAL_ERROR);
            } else {
                def json = null;
                if ( box) {
                    while ( existSerial) {
                        serial = SerialNumberGeneratorHelper.generateSerial()
                        existSerial = existSerialNumber(serial)
                    }
                    log.info("generateCertificateLegacyBox New Serial["+serial+"]" +" Legacy Serial: [" + legacySerial + "] ");
                    json= restClientService.generateCertificateLegacyBox(serial);
                }
                if (!json) {
                    jsonMap = gearsService.setResultInJsonMap(jsonMap, false,
                        "Can't execute the call to refullService in CentralServer", ErrorCode.GENERAL_EXCEPTION);
                    response.setStatus(Constants.NOT_FOUND);
                } else {
                    log.info("generateCertificateLegacy Box New Serial["+serial+"]" +" was created the certificate");
                    jsonMap.certificate = json.certificate;
                    jsonMap.certificationLabel = json.certificationLabel;
                    jsonMap.serial = serial;
                    User user = User.findByEmail(Constants.ADMIN_USER_EMAIL);
                    boxManufacturer = new BoxManufacturer(serialNumber:serial,
                        certificateLocation: json.certificationLocation,
                        certificateFileName:json.certificationFileName,
                        createdBy:user,legacySerialNumber:legacySerial);
                    // If the box doesn't exists We doesn't have to save any changes
                    if (box || boxManufacturer.validate()) {
                        BoxManufacturer.withTransaction { status ->
                            try {
                                log.info("generateCertificateLegacy Box New Serial["+serial+"]" +" saving Manufacturer");
                                box.serial = serial;
                                box.hasCertificate = true;
                                box.save();
                                log.info("updateBoxIPSerials serial["+serial+"]" +" legacy serial["+legacySerial+"]");
                                def boxIps = BoxIp.findAllBySerial(legacySerial);
                                List<BoxIp> ips = new ArrayList<BoxIp>();
                                for(BoxIp ip:boxIps) {
                                    log.info("updateBoxIPSerials ip: ["+ ip.serial +"]" );
                                    ip.serial = serial;
                                    ips.add(ip);
                                    log.info("updateBoxIPSerials ip after save: ["+ ip.serial +"]" );
                                }
                                BoxIp.saveAll(ips);
                                boxBlackListService.updateBoxBlackListSerial(serial, legacySerial);
                                boxManufacturer.save();
                                jsonMap = gearsService.setResultInJsonMap(jsonMap, true,  null, ErrorCode.NO_ERROR);
                                response.setStatus(Constants.OK);
                            } catch(Exception dbEx) {
                                log.error dbEx;
                                status.setRollbackOnly();
                                jsonMap = gearsService.setResultInJsonMap(jsonMap, false,  null, ErrorCode.GENERAL_EXCEPTION);
                                response.setStatus(Constants.INTERNAL_ERROR);
                            }
                        }
                    } else {
                        jsonMap = gearsService.setResultInJsonMap(jsonMap, false,   null, ErrorCode.GENERAL_EXCEPTION);
                        response.setStatus(Constants.INTERNAL_ERROR);
                    }
                }
            }
        } catch (Exception ex) {
            log.error ex
            jsonMap = gearsService.setResultInJsonMap(jsonMap, false,  null, ErrorCode.GENERAL_EXCEPTION)
            response.setStatus(Constants.INTERNAL_ERROR)
        }
        def jsonToSend = (jsonMap as JSON)
        log.info(" generateCertificateLegacyBox sending jsonResponse: [" + jsonToSend + "]")
        render jsonToSend
    }

    /**
     * Validates the certificate that myApp sends
     *
     * @return Render the json response to myApp
     */
    def validateCertificate() {
        HashMap jsonMap = new HashMap();
        def jsonMessage = request.JSON;
        try {
            def certificate = jsonMessage?.cert;
            def serial = jsonMessage?.serial;
            def certVersion=jsonMessage?.certLabel;
            log.info("Validate certificate of Box with Serial: [" + serial + ":" + certVersion + "]")
            jsonMap = boxService.validateBoxConnectionBySerial(serial, certificate, certVersion);
            response.setStatus(Constants.OK);
        } catch (Exception ex) {
            log.error("Error validating certificate", ex);
            jsonMap = gearsService.setResultInJsonMap(jsonMap, false,   null, ErrorCode.GENERAL_EXCEPTION)
            response.setStatus(Constants.INTERNAL_ERROR)
        }
        def jsonToSend = (jsonMap as JSON);
        log.info("Validate Certificate result sending jsonResponse: [" + jsonToSend + "]");
        render jsonToSend;
    }

    /**
     * Validate myBox Certificate
     *
     * @return  Render the json response to gears
     */
    def validateCertificateMyBox() {
        log.info("ValidateCertificateMyBox :: Validate certificate of Box");
        validateCertificate();
    }

    /**
     * Gets the notifications sent by MyBox and saves the info to the Database
     */
    def registerMyBoxNotifications () {
        HashMap jsonMap = new HashMap();
        HashMap responseMap = new HashMap();
        def jsonMessage = request.JSON;
        jsonMap = jsonMessage;
        def serialNumber = jsonMap.get(Constants.PARAM_SERIAL_NUMBER);
        def action = jsonMap.get(Constants.PARAM_ACTION);
        log.info(" registerMyBoxNotifications for serial number: [" + serialNumber + "]");
        HashMap<String, Object> data;
        HashMap<String, Object> notificationInfo;
        Box box= Box.findBySerial(serialNumber);
        List<HashMap<String, Object>> notificationsList=jsonMap.get(Constants.PARAM_NOTIFICATIONS);
        for (Iterator<HashMap<String, Object>> iter = notificationsList.iterator(); iter.hasNext();) {
            log.info(" registerMyBoxNotifications iterator - serial number: [" + serialNumber + "]")
            //if there are more than 2 kinds of notifications put a Switch Case
            try {
                data = iter.next();
                notificationInfo = data.get(Constants.PARAM_NOTIFICATIONS_INFO);
                String time = notificationInfo.get(Constants.PARAM_TIME);
                String channelNumber = notificationInfo.get(Constants.PARAM_CHANNEL);
                BoxChannel boxChannel = BoxChannel.getButtonByBox(box, Integer.parseInt(channelNumber)).get();
                BoxNotification boxNotification = new BoxNotification();
                boxNotification.action = data.get(Constants.PARAM_ACTION);
                boxNotification.time = Integer.parseInt(time);
                boxNotification.user = notificationInfo.get(Constants.PARAM_USER);
                boxNotification.boxChannel = boxChannel;
                boxNotification.box = box;
                boxNotification.dateCreated = new Date();
                boxNotification.save();
            } catch(Exception e) {
                log.error("RegisterMyBoxNotifications iterator - can not register notification serial number: [" + serialNumber + "]")
                response.setStatus(Constants.INTERNAL_ERROR);
            }
        }
        response.setStatus(HttpStatus.OK.value);
        render( responseMap as JSON);
    }

    /**
     * Validates if exists a box registered or unregistered with the received serial number
     *
     * @param serialNumber
     *              Serial number to find
     * @return True or False depending if exists a box or not
     */
    private def existSerialNumber(serialNumber) {
        Box box = Box.findBySerial(serialNumber);
        if (box) {
            return true;
        }
        BoxUnregistered boxUnregistered = BoxUnregistered.findBySerial(serialNumber);
        if (boxUnregistered) {
            return true;
        }
        return false;
    }

    /**
     * Called by Communication to verify myBox's connection, save ipAddres and macAddress
     * @param serial
     */
    def localVerify(){
        HashMap jsonMap = new HashMap();
        String serialNumber = params.serialNumber;
        log.info " GearController - localVerify-> Serial: "+serialNumber;
        def myBoxIpAddress = params.myBoxIpAddress;
        def myBoxMacAddress = params.myBoxMacAddress;
        log.info " localverify - checkMacAddressAndSerialConnections - Serial: "  + serialNumber + " Ip Address " + myBoxIpAddress + " MacAddress " + myBoxMacAddress
        try {
            Box box = Box.findBySerial(serialNumber);
            if (box) {
                boxIpService.checkMacAddressAndSerialConnections(serialNumber,myBoxIpAddress, myBoxMacAddress);
                jsonMap = gearsService.setResultInJsonMap(jsonMap, true,  null, ErrorCode.NO_ERROR);
            } else {
                jsonMap = gearsService.setResultInJsonMap(jsonMap, false, null, ErrorCode.NO_DEVICE_FOUND);
            }
        } catch(Exception e) {
            log.error(" localverify - Didn't do localVerify action: [" + serialNumber + "]", e);
            jsonMap = gearsService.setResultInJsonMap(jsonMap, false, null, ErrorCode.GENERAL_EXCEPTION);
        }
        render (jsonMap as JSON);
    }

    /**
     * This method is used by the Manufacturing Script and returns the list
     * of channels of the Box by his serial.
     *
     * @return
     */
    def getBoxChannels() {
        HashMap jsonMap = new HashMap();
        String serial = params.serial;
        String description = "";
        boolean success = false;
        try {
            if (StringUtils.isNotBlank(serial)) {
                Box box = boxService.findBoxBySerialNumber(serial);
                if (box){
                    def listChannels = new ArrayList()
                    def channels = BoxChannel.findAllByBox(box);
                    HashMap channelMap;
                    channels.each { channel ->
                        channelMap =  new HashMap();
                        channelMap.put(Constants.PARAM_PORT, channel.channelPort);
                        channelMap.put(Constants.PARAM_LABEL, channel.channelLabel);
                        channelMap.put(Constants.PARAM_IS_PA, channel.isPa);
                        channelMap.put(Constants.PARAM_DESCRIPTION, channel.description);
                        channelMap.put(Constants.PARAM_IMAGE_URL, channel.imageURL);
                        channelMap.put(Constants.PARAM_LARGE_IMAGE_URL, channel.largeImageURL);
                        listChannels.add(channelMap);
                    }
                    jsonMap.put(Constants.PARAM_NUMBER_OF_CHANNELS,channels.size());
                    jsonMap.put(Constants.PARAM_PA_MODE, box.pa? Constants.PARAM_MASTER:Constants.PARAM_RELAY);
                    jsonMap.put(Constants.PARAM_CHANNELS, listChannels);
                    jsonMap = gearsService.setResultInJsonMap(jsonMap, true,  description, ErrorCode.NO_ERROR );
                } else {
                    description = "Box with serial " + serial + " not found";
                    jsonMap = gearsService.setResultInJsonMap(jsonMap, false,  description, ErrorCode.NO_DEVICE_FOUND);
                }
            } else { // If not Serial; returns error
                description = "Required parameter serial not found.";
                jsonMap = gearsService.setResultInJsonMap(jsonMap, false,  description, ErrorCode.INVALID_PARAMETERS);
            }
        } catch(Exception e) {
            description = "Unexpected error: " + e.getMessage();
            jsonMap = gearsService.setResultInJsonMap(jsonMap, false,  description, ErrorCode.GENERAL_EXCEPTION);
        }
        def jsonToSend = (jsonMap as JSON);
        log.info "getBoxChannels response for " + params.serial + ": " + jsonToSend;
        render jsonToSend;
    }

    /**
     * Public method used by the Communication component to retrieve the associated App Theme information
     * of a ExXtractor by Serial
     *
     * @param serial
     *              Serial number of the ExXtractor
     * @return Json with the list of App Theme Settings
     */
    def getAppSettings(String serial) {
        processCommRequest(GearsService.CommRequestAction.GET_APP_INFO, serial, null);
    }

    /**
     * Public method used by the Communication component to retrieve the associated Welcome Ad
     * information of a ExXtractor by Serial
     *
     * @param serial
     *                  Serial number of the ExXtractor
     * @return Json with the list of documents
     */
    def getWelcomeAd(String serial) {
        processCommRequest(GearsService.CommRequestAction.GET_WELCOME_AD, serial, null);
    }

    /**
     * Public method used by the Communication component to retrieve the list of box users of an
     * ExXtractor by Serial
     *
     * @param serial
     *                  Serial number of the ExXtractor
     * @return Json with the list of documents
     */
    def getBoxUsers(String serial) {
        processCommRequest(GearsService.CommRequestAction.GET_BOX_USERS, serial, null);
    }

    /**
     * Public method used by the Communication component to retrieve the list of documents
     * associate to a ExXtractor by Serial
     *
     * @param serial
     *                  Serial number of the ExXtractor
     * @return Json with the list of documents
     */
    def getDocuments(String serial) {
        processCommRequest(GearsService.CommRequestAction.GET_DOCUMENTS, serial, null);
    }

    /**
     * Public method used by the Communication component to retrieve the list of sliding
     * banners associate to a ExXtractor by Serial
     *
     * @param serial
     *                  Serial number of the ExXtractor
     * @return Json with the list of documents
     */
    def getContents(String serial) {
        processCommRequest(GearsService.CommRequestAction.GET_CONTENTS, serial, null);
    }

    /**
     * Public method used by the Communication component to retrieve the list of offers
     * associate to a ExXtractor by Serial
     *
     * @param serial
     *                  Serial number of the ExXtractor
     * @return Json with the list of documents
     */
    def getOffers(String serial) {
        processCommRequest(GearsService.CommRequestAction.GET_OFFERS, serial, null);
    }

    /**
     * Public method used by the Communication component to retrieve the list of configurations
     * associate to a ExXtractor by Serial
     *
     * @param serial
     *                  Serial number of the ExXtractor
     * @return Json with the list of configurations
     */
    def getConfigs(String serial) {
        processCommRequest(GearsService.CommRequestAction.GET_CONFIGS, serial, null);
    }
    
    /**
     * Public method used by the Communication component to retrieve the list of Devices to
     * an ExXtractor by Serial
     *
     * @param serial
     *                  Serial number of the ExXtractor
     * @return Json with the list of configurations
     */
    def getDevices(String serial) {
        processCommRequest(GearsService.CommRequestAction.GET_DEVICES, serial, null);
    }

    /**
     * This service is use by Communication in order to report the upload process of the metrics
     * file from an ExXtractor
     *
     * @param serial
     *                      Serial number of the ExXtractor
     * @param uploadResult
     *                      Result of the upload process
     * @return Render a Json object with the result
     */
    def notifyMetricsUploadResult(String serial, boolean uploadResult) {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put(Constants.PARAM_UPLOAD_RESULT, uploadResult);
        processCommRequest(GearsService.CommRequestAction.PROCESS_METRICS_UPLOAD, serial, paramsMap);
    }

    /**
     * This service is use by Communication in order to update the configuration value of a
     * single configuration for a specific ExXtractor
     *
     * @param serial
     *                  Serial number of the ExXtractor
     * @param code
     *                  Code of the configuration
     * @param value
     *                  New Value of the configuration
     */
    def updateBoxConfiguration(String serial, String code, String value) {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put(Constants.PARAM_CODE, code);
        paramsMap.put(Constants.PARAM_VALUE, value);
        processCommRequest(GearsService.CommRequestAction.UPDATE_CONFIGURATION, serial, paramsMap);
    }

    /**
     * This service is use by Communication in order to execute an Action related with a
     * single ExXtractor User.
     *
     * @param serial
     *                  ExXtractor serial number executing the action
     * @param username
     *                  Username of the User
     * @param password
     *                  Encrypted password of the User
     * @param level
     *                  Level of the User
     * @param userAction
     *                  Action to be execute
     * @param lastUpdate
     *                  long with last update date time
     */
    def saveUserInfo(String serial, String username, String password, String level,
            String userAction, long lastUpdate) {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put(Constants.PARAM_USERNAME, username);
        paramsMap.put(Constants.PARAM_PASSWORD, password);
        paramsMap.put(Constants.PARAM_LEVEL, level);
        paramsMap.put(Constants.PARAM_ACTION, userAction);
        paramsMap.put(Constants.PARAM_LAST_UPDATE, lastUpdate);
        processCommRequest(GearsService.CommRequestAction.SAVE_USER_INFO, serial, paramsMap);
    }

    /**
     * Gets the JsonMap object when an exception occurs
     */
    def getJsonMapAfterException(String serial, String msg){
        HashMap jsonMap = new HashMap();
        jsonMap = gearsService.setJsonMapResult(jsonMap, false, msg, true);
        jsonMap.serialNumber = serial;
        return jsonMap;
    }
}
