package mycentralserver;

import mycentralserver.generaldomains.Configuration;

/**
 * Controller class use it for the management of the general configurations of the system; the intends
 * of this class is to receive the request, trigger to a service and render the response.
 * 
 * @author Selim
 *
 */
class ConfigurationController {

	/* Inject required services */
	def configurationService;

	def index() {
		[configurations: Configuration.findAll()]
	}

	def edit(long id) {
		Configuration configuration = Configuration.read(id);
		if (configuration) {
			[configuration: configuration]
		} else {
			// The configuration do not exists
			flash.error = message(code: 'default.not.found.message')
			redirect(action: "index", controller:"configuration")
		}
	}

	def update(long id) {
		try {
			Configuration configuration = Configuration.read(id);

			if (configuration) {
				String oldValue = configuration.value;
				configuration.properties = params;
				if(configuration.validate()) {
					configuration.save();
					if(!configuration.value.equals(oldValue)) {
						configurationService.sendConfigurationUpdateToConnectedServers(configuration);
					}
					flash.success = message(code: 'default.updated.message', args: [
						message(code: 'configuration', default: 'Configuration'),
						configuration.code
					])
				} else {
					flash.error=message(code: 'default.not.upated.message', args: [message(code: 'configuration', default: 'Configuration')])
				}
			} else {
				flash.error = message(code: 'default.not.found.message');
				redirect(action: 'index');
			}
		} catch(Exception ex) {
			// Generic catch because we are not expecting an specific type but
			// grails can trigger unexpected exceptions some times
			log.error("Error updating a Configuration entity with: " + params, ex);
			flash.error = ex.getMessage();
		}

		if (flash.error){
			render(view: 'edit', model: [configuration: configuration]);
		} else {
			redirect(action: 'index');
		}
	}
}