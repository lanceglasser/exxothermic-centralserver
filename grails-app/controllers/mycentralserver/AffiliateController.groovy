package mycentralserver

import mycentralserver.app.Affiliate;
import mycentralserver.box.BoxUsers;
import mycentralserver.company.Company;
import mycentralserver.company.CompanyCatalogType;
import mycentralserver.app.Affiliate;
import mycentralserver.user.User;
import mycentralserver.utils.Utils;
import grails.converters.JSON;
import mycentralserver.utils.Constants;

class AffiliateController extends BaseControllerMyCentralServerController {

    def userSessionService;

    def establishments(Long id) {
		def affiliate;
		def affiliates = Affiliate.findAll();
		if (affiliates.size() > 0) {
			if(id != null){
				affiliate = Affiliate.read(id);
			}else{
				affiliate = affiliates.get(0);
			}
			[affiliate: affiliate,  affiliates:Utils.sortByName(affiliates)]
		}else{
			redirect(action: "emptyList");
		}
	}
	
	def listAll () {
		def affiliate;
		def affiliates = Affiliate.findAll();
		if (affiliates.size() > 0) {
			[affiliates:Utils.sortByName(affiliates)]
		}else{
			redirect(action: "emptyList");
		}
	}

	def showAffiliates (Long id) {
		def affiliate;
		def affiliates = Affiliate.findAll();
		if (affiliates.size() > 0) {
			if(id != null){
				affiliate = Affiliate.read(id);
			}else{
				affiliate = affiliates.get(0);
			}
			[affiliate: affiliate,  affiliates:Utils.sortByName(affiliates)]
		}else{
			redirect(action: "emptyList");
		}
	}
	
	def create () {
		Affiliate newAffiliate=new Affiliate(params)
		//def roles=Role.list()

		[affiliate: newAffiliate] //roles:roles]
	}

	def edit(Long id) {

		Affiliate affiliate=null
		def offset = this.getOffsetParameterToPagination()
		def affiliateInstance = Affiliate.get(id)
		if (!affiliateInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'userInformation.label', default: 'User Information'),
				id
			])
			redirect(action:  Constants.VIEW_LISTALL, params: [offset: offset])
		}else{
			[affiliate: affiliateInstance, offset:offset]
		}
	}

	def show(Long id) {

		def offset = this.getOffsetParameterToPagination()

		def affiliateInstance = Affiliate.get(id)

		if (!affiliateInstance) {
			flash.error = message(code: 'default.not.found.message', args: [
				message(code: 'affiliateInformation.label', default: 'AffiliateInformation'),
				id
			])
			redirect(action: Constants.VIEW_LISTALL, params: [offset: offset])
			return
		}else{
			[affiliate: affiliateInstance, offset:offset]
		}
	}

    def save() {
        Affiliate affiliate = null;
        try {
            affiliate = new Affiliate(params);
            final User currentUser = userSessionService.getCurrentUser();
            affiliate.createdBy = currentUser;
            affiliate.lastUpdatedBy = currentUser;
            if (affiliate.validate()) {
                try {
                    affiliate.save(flush:true, failOnError:true)
                } catch(Exception dbEx) {
                    flash.error=dbEx.getMessage();
                }
            } else {
                flash.error = message(code: 'user.error.saving');
            }
        } catch(Exception exp) {
            flash.error=exp.getMessage();
        }

        if (flash.error != null) {
            flash.error=message(code: 'user.error.saving');
            render(view: Constants.VIEW_CREATE, model: [affiliate: affiliate]);
        } else {
            flash.success = message(code: 'default.created.message',
                args: [message(code: 'default.affiliate.label'), affiliate.name]);
            redirect(action: Constants.VIEW_LISTALL);
        }
    }

    /**
     * Receives the information to update an Affiliate, executes the process and renders the response depending of the
     * result
     * 
     * @param id
     *          Id of the Affiliate to be updated
     */
    def update(Long id) {
        final Affiliate affiliateInstance = Affiliate.read(id);;
        if (affiliateInstance) {
            affiliateInstance.lastUpdatedBy = userSessionService.getCurrentUser();
            affiliateInstance.properties = params;
            if (affiliateInstance.validate()) {
                affiliateInstance.save();
                flash.success = message(code: 'default.updated.message',
                    args: [message(code: 'default.affiliate.label'), affiliateInstance.name]);
                redirect(action: Constants.VIEW_LISTALL);
            } else {
                flash.error = message(code: 'default.not.upated.message', 
                    args: [message(code: 'default.affiliate.label')]);
                render(view: Constants.VIEW_EDIT, model: [affiliate: affiliateInstance]);
            }
        } else {
            flash.error = message(code: 'default.not.found.message',
                args: [message(code: 'affiliateInformation.label', default: 'Affiliate'), id]);
            redirect(action: Constants.VIEW_LISTALL);
        }
    }

	def getAffiliateEstablishments() {
		Affiliate affiliate = Affiliate.read(params.affiliateId);
		
		if (!affiliate) {
			flash.error = message(code: 'default.appSkin.not.found');
		}else{
			if(!allowedAccess(affiliate)){
				//flash.error = message(code: 'default.not.found.message', args: [message(code: 'userInformation.label', default: 'User Information'), params.buttonId])
				render(text: '<div class="alert alert-error">'
					+ message(code: 'default.not.found.message', args: [message(code: 'userInformation.label', default: 'User Information'), params.affiliateId])
					+ '</div>', contentType: "text/html", encoding: "UTF-8")
			}else{
			
				def typesCompany=CompanyCatalogType.list();
					
				render (template:'establishmentsAssociationTableTemplate', 
					model:[affiliate:affiliate, typesCompany:typesCompany])
				
			}
		}
	}
	
	def associateEstablishments(){
		boolean error = false;
		String msg = "";
		
		try {						
			//Gets the Affiliate by the Id
			Affiliate affiliate = Affiliate.get(params.a);
																		
			if (!affiliate) {
				//If not exists the Affiliate
				msg = message(code: 'default.not.found.message')
				error = true;
			} else {
				if(allowedAccess(affiliate)){
					affiliate.associateEstablishments = params.s;
					
					//Save the Entity to DB
					if(affiliate.save()){
						msg = message(code: 'affiliate.establishments.save.success');
					} else {
						msg = message(code: 'affiliate.establishments.save.error')
						error = true;
					}
				} else {//Not allow
					msg = message(code: 'default.not.found.message')
					error = true;
				}
			}
		}catch(Exception ex){
			error = true;
			msg = ex.getMessage();
			log.error(ex);
		}
		
		def result = [error: error, msg: msg];
		render result as JSON;
	}
	
	def allowedAccess(Affiliate affiliate){
		return true;
	}





	def delete(long id){
		try{

			def affiliate = Affiliate.get(id)
			println(affiliate)
			if ( !affiliate ) {
				//If affiliate does not exist
				flash.error = message(code: 'default.not.found.message')
				redirect(controller: "affiliate", action: "show", params: [id: affiliate.id] )
			} else {


			    def deleteResult = null;
				//The delete function is currently disabled, as it might alter functionality of other tables
				//def deleteResult = deleteAffiliate(affiliate);
				if(Affiliate.contents.size() > 0){
					//Return an error because cannot delete Affiliates with contents
					flash.error = message(code:'doc.category.delete.error.because.documents');
				}else{
					if(deleteResult == null){
						redirect(controller: "affiliate", action: "listAll" )
						flash.success = message(code: 'general.object.delete.success.male', args:[message(code:'default.affiliate.label')]);
					} else {
						flash.error = message(code:"general.object.delete.error");
					}

				}
			}
		}catch(Exception ex){
			log.error(ex)
			flash.error = ex.getMessage()
			redirect(controller: "affiliate", action: Constants.VIEW_LISTALL)
		}
	}

	def deleteAffiliate(Affiliate affiliate){
		try {
			if(affiliate.delete()){
				return null;
			}
		}catch(Exception ex){
			throw ex;
		}
	}

    /**
     * Enable/Disable the Affiliate refered by the received id
     * 
     * @param id
     *          Id of the Affiliate to enable or disable
     */
    def disabled(Long id) {
        Affiliate affiliateInstance = Affiliate.read(id);
        if (affiliateInstance) {
            affiliateInstance.enabled = !affiliateInstance.enabled;
            if (affiliateInstance.validate()) {
                affiliateInstance.save();
                flash.success = message(code: 'default.upated.message', args: [affiliateInstance.name]);
                redirect(action: Constants.VIEW_LISTALL);
            }else{
                flash.error = message(code: 'default.not.upated.message', args: [affiliateInstance.name]);
                render(view: Constants.VIEW_SHOW, model: [id: id, affiliate:affiliateInstance]);
            }
        } else {
            flash.error = message(code: 'default.not.found.message', args: [
                message(code: 'affiliateInformation.label', default: 'AffiliateInformation'), id]);
            redirect(action:Constants.VIEW_LISTALL);
        }
    }
}