package mycentralserver

import mycentralserver.docs.DocCategory;
import mycentralserver.docs.Document;
import mycentralserver.utils.Constants;
import mycentralserver.utils.DateUtils;
import mycentralserver.utils.Utils;
import mycentralserver.user.User;
import mycentralserver.user.Role;
import mycentralserver.company.Company;
import mycentralserver.company.CompanyIntegrator;
import mycentralserver.company.CompanyLocation;

import java.nio.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

import groovy.io.FileType;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

/**
 * Controller that handles the related functionalities of the Documents
 *
 * @author      Cecropia Solutions
 * @date     	09/11/2014
 */
class DocumentController extends BaseControllerMyCentralServerController {

	/* Inject required services */
	MessageErrorService messageErrorService
	DocumentService documentService
	def userSessionService;

    /**
     * Renders the page with the list of documents that the user can see
     */
    def listAll() {
        def documents = getDocumentsToList();
        if(documents.size() > 0) {
            [list: Utils.paginateList(documents,params)]
        } else {
            render(view: "emptyList");
        }
    }


	/**
	 * Renders the page that allow the creation of a new Document
	 *
	 * @return
	 */
	def create() {
		[document: new Document(), type: DocCategory.list()]
	}

	def edit(long id) {
		def entity=Document.read(id);
		if (!entity) {
			flash.error = message(code: 'default.document.not.found')
			redirect(action: "listAll")
		}else{
			[document: entity, type: DocCategory.list()]
		}
	}

	/**
	 * Receives the parameter for a new Skin, save the entity to the
	 * database and upload the images.
	 *
	 * @return
	 */
	def save() {
		Document doc = new Document();
		if ( params.size() > 2) {
			try {
				String dateF = "MM/dd/yyyy";
				DateFormat dateFormat = new SimpleDateFormat(dateF + " HH:mm:ss");
				doc.name = params.name;
				if(params.type){
					doc.type = DocCategory.get(params.type.id);
					doc.filename = params.file;
					doc.url = params.file; // adds the file name to the url temporally to skip the validation error
					boolean isValidExpirationDate = true;
					String strMaxDate = grailsApplication.config.maxDateForValidations;
					if(params.expirationDate!="") {
						Date date = Utils.isLegalDate( params.expirationDate, dateF, false);
						if(date != null){
							doc.expirationDate = dateFormat.parse(params.expirationDate + " 23:59:59");
							DateFormat utcFormat = new SimpleDateFormat(dateF + " HH:mm:ss");
							DateFormat formatTimeZone = new SimpleDateFormat(dateF + " HH:mm:ss");
							Date currentDate = (Date) utcFormat.parse(formatTimeZone.format(new Date()));
							
							Date maxDate = dateFormat.parse(strMaxDate + " 23:59:59");
													
							if(doc.expirationDate < currentDate || doc.expirationDate > maxDate){//Date cannot be before current
								isValidExpirationDate = false;
								flash.error = message(code:'expiration.date.before.current.error', args:[strMaxDate]);
							}
						} else {
							isValidExpirationDate = false;
							flash.error = message(code:'expiration.date.before.current.error', args:[strMaxDate]);
						}
					} else {
						doc.expirationDate=null;
						//isValidExpirationDate = false;
						//flash.error = message(code:'expiration.date.before.current.error', args:[strMaxDate]);
					}
									
					doc.createdBy = getCurrentUser();
					if(isValidExpirationDate) {
						if(params[Constants.PARAM_FILE_UPDATED] == Constants.PARAM_UPDATED_VALUE){
							//Valid File selected
							if(doc.validate()) {
								String messageCode = documentService
									.saveDocument(doc, request.getFile(Constants.PARAM_FILE));
								if (messageCode != null) { 
									flash.error=message(code: messageCode); 
								} else {	
									flash.success = message(code: 'document.create.message.success'); 
								}
							} else {
								flash.error = message(code: 'default.error.problem')
							}
						} else {
							flash.error = message(code:'invalid.document.message', args:['5MB']);
						}
					}
				} else {//You need a Category in order to save the Document
					flash.error = message(code: 'document.not.category.error')
				}
			} catch ( grails.validation.ValidationException valEx) {
				flash.error = message(code: 'default.error.problem');
			} catch( Exception ex) {
				flash.error = message(code: 'default.error.problem');
				log.error( "Eror when save the document", ex);
			}
			
			if(flash.error != null){				
				doc.filename = "";
				doc.url = "";
				render(view: "create", model: getModelForSaveInErrorCase(doc));
			} else {
				redirect(action:"assignLocation", id : doc.id);
			}
			return;
		}else{
			flash.error = message(code: 'default.not.found.message')
			redirect(action:"listAll")
			return;
		}
	} // END save

	def show(long id) {
		def offset = this.getOffsetParameterToPagination();
		def doc = Document.read(id);

		if (!doc) {
			//If the AppSkin don't exists redirect to the list with the error message
			flash.error = message(code: 'document.not.found');
			redirect(action: Constants.VIEW_LISTALL)
		}
		[document: doc]
	}

    /**
     * This method updates the information of a Document
     * 
     * @param id
     *              Id of the Document to be updated
     * @param version
     *              Version before save in order to verify that nobody else changes the info before
     */
    def update(long id, Long version) {
        Document doc = null;
        try {
            doc = Document.read(id);
            doc.name = params.name;
            doc.type = DocCategory.get(params.type.id);
            if (StringUtils.isBlank(params.expirationDate)) { // Set the Expiration Date to Null
                doc.expirationDate = null;
            } else {
                final String strMaxDate = grailsApplication.config.maxDateForValidations;
                final Date maxAllowedDate = DateUtils.getMaxAllowedDate(strMaxDate);
                final Date expirationDate = DateUtils.isValidDateAtEndOfDay(params.expirationDate.trim());
                final Date currentDate = DateUtils.getCurrentDate();
                if (expirationDate == null || expirationDate < currentDate || expirationDate > maxAllowedDate) {
                    flash.error = message(code:'expiration.date.before.current.error', args:[strMaxDate]);
                } else {
                    doc.expirationDate = expirationDate;
                }
            }

            if (flash.error == null ) {
                if (doc.validate()) {
                    final String messageCode = documentService.updateDocument(doc,
                            ((params[Constants.PARAM_FILE_UPDATED].equals(Constants.PARAM_UPDATED_VALUE))? 
                                request.getFile(Constants.PARAM_FILE):null));
                    if (messageCode != null) {
                        flash.error = message(code: messageCode); 
                    }
                } else {
                    flash.error = message(code: 'default.error.problem')
                }
            }
        } catch( Exception ex) {
            flash.error = message(code:'unexpected.error.please.try.again');
            log.error("Error updating a Document: " + params, ex);
        }
        if (flash.error == null) {
            flash.success = message(code: 'default.message.success');
            redirect(action:"listAll");
        } else {
            render(view: "edit", model: getModelForSaveInErrorCase(doc));
        }
    }

	/**
	 * Deletes a Document; must delete from DB, from File Container and
	 * must update the associated ExXtractors.
	 *
	 * @param id	Id of the Document to be deleted.
	 *
	 * @return
	 */
	def delete(long id)
	{
		try {
			Document doc = Document.read(id);

			if ( !doc ) {
				//If the Document do not exists or don't has access
				flash.error = message(code: 'document.not.found');
				redirect(action:Constants.VIEW_LISTALL);
			}else{
				String messageCode = documentService.deleteDocument(doc);
				if (messageCode != null) {
					flash.error=message(code: messageCode);
				} else {
					//If the object is deleted from DB, delete the associated images
					flash.success = message(code: 'default.document.remove.success');
				}

				redirect(action:Constants.VIEW_LISTALL);
			}
		} catch(Exception e){
			log.error("Error deleting the document with id=" + id + ": ${e.message}", e);
			throw e;
		}
	}

    def assignLocation() {
        def documentId = (params.id == null)? 0:Integer.parseInt(params.id);
        //Get the list of enabled Companies associated for current user role with enabled locations
        def listCompanies = userSessionService.getCompaniesForAssociation();
        if (listCompanies.size() > 0) {
            def documentList = getDocumentsToList();
            if (documentList == null || documentList.size() == 0) {
                redirect(action: "noContentToManage");
            } else {
                [companyList: listCompanies, documentList: documentList.unique(), documentId: documentId]
            }
        } else {
            if (userSessionService.isOwnerOrOwnerStaffOnly()) {
                flash.warn = message(code: 'default.document.no.user.premium.companies');
            } else {
                flash.warn = message(code: 'default.document.no.user.companies');
            }
            redirect(action: "listAll");
        }
    }

	def noContentToManage(){ }

	def ajaxAssignLocation(){
		def document=Document.get(params.buttonId);

		if (!document) {
			flash.error = message(code: 'default.customButton.not.found')
		}else{
			//Get the list of enabled Companies associated for current user role with enabled locations
			def listCompanies = userSessionService.getCompaniesForAssociation();

			if(!allowDocumentAction(listCompanies, document)){
				render(text: '<div class="alert alert-error">'
					+ message(code: 'default.not.found.message', args: [message(code: 'userInformation.label', default: 'User Information'), params.buttonId])
					+ '</div>', contentType: "text/html", encoding: "UTF-8")
			}else{
				String selectedLocations =""
				for(location in document.locations) { selectedLocations += (selectedLocations.equals(""))? location.id:"," + location.id; }

				render (template:'companyButtonsTemplate', model:[document: document, companies: listCompanies, selectedLocations: selectedLocations]);
			}
		}
	}

	private getModelForSaveInErrorCase(Document documentParam) {
		//Return the view paramters
		return [document:documentParam, type:DocCategory.list()];
	}

	def getUserDocuments(listCompanies){
		def docList = []
		for (Iterator<Company> iter = listCompanies.iterator(); iter.hasNext();) {
			Company c = iter.next();
			if(c.enable){
				def locations = CompanyLocation.findAllByCompany(c);				
				for(location in locations){
					if(location.enable){ 
						docList += location.documents;
					}
				}
			}
		}

		return docList;
	}

	def getEnableUserDocuments(listCompanies){
		def docList = getUserDocuments(listCompanies);
		def user = getCurrentUser();
		
		for(document in user.createdDocuments){ docList += document; }
		
		DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat formatTimeZone = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date currentDate = (Date) utcFormat.parse(formatTimeZone.format(new Date()));
		docList = docList.findAll{it.expirationDate == null || it.expirationDate >= currentDate}
				 
		return docList.unique().sort();
	}

	def boolean allowDocumentAction(listCompanies, document){
		
		if(isHelpDesk()){
			return true;	
		} else {
			def relatedDocs = [];
		
			for(company in listCompanies){
				if(company.enable){
					for(location in company.locations){
						if(location.enable) { relatedDocs += location.documents; }
					}
				}
			}
	
			def user = getCurrentUser();
			for(doc in user.createdDocuments){ relatedDocs += doc }
	
			return (relatedDocs.any { it in (document) });
		}
	}

	public def syncronizeContent() {
		def id = params.customButtonId.toInteger();
		def doc = documentService.readDocumentById(id);
		def messageErrorDescriptionHash =  messageErrorService.getMessageErrorDescription();
		HashMap<String, String> contentsInfo;
		def jsonTransactionStatus = null
		def locations = doc.locations;
		
		if(!locations.isEmpty()) {
			//Must sync some Locations
			jsonTransactionStatus = syncDocumentsToLocations(locations);
			contentsInfo = documentService.getDocumentNames(locations);
			
		} else {//There are not affected Locations, nothing to Sync
			flash.success = message(code: 'sync.no.affected.locations')
		}

		//Get the list of enabled Companies associated for current user role with enabled locations
		def listCompanies = userSessionService.getCompaniesForAssociation();
		def docsList = getEnableUserDocuments(listCompanies);

		render(view: "assignLocation", model: [ companyList: listCompanies,
												documentList: docsList.unique(),
												documentId: doc.id,
												jsonTransactionStatus:jsonTransactionStatus,
												documentsHashInfo:contentsInfo,
												messageErrorDescriptionHash:messageErrorDescriptionHash])
		return
	}

	def saveLocations(){
		def doc
		try{

			doc = Document.get(params.buttons);

			if (!doc) {
				flash.error = message(code: 'default.document.not.found');
				redirect(action: "listAll");
			} else {
				//Get the list of enabled Companies associated for current user role with enabled locations
				def listCompanies = userSessionService.getCompaniesForAssociation();
				def affectedLocations = []

				List currentLocations = new ArrayList<Document>();
				currentLocations.addAll(doc.locations);

				def locationsToSave = (params.locationsToSave == null)? "":params.locationsToSave;
				def newLocations = locationsToSave.split(",");
				for(companyLocation in currentLocations){
					if(!newLocations.any { it == (companyLocation.id.toString()) }){
						if(companyLocation.enable && companyLocation.company.enable){
							doc.removeFromLocations(companyLocation);
							affectedLocations.add(companyLocation);
						}
					}
				}

				for (c in newLocations){
					if ( c != "" && !currentLocations.any { it.id.toString() == (c) }){
						CompanyLocation location = CompanyLocation.findById(c)
						doc.addToLocations(location)
						affectedLocations.add(location)
					}
				}

				def jsonTransactionStatus = null
				HashMap<String, String> contentsInfo = new HashMap<String, String>()

				if(doc.validate()) {
					doc.withTransaction { status ->
						try {
							if (doc.save(flush:true, failOnError: true)) {
								if (affectedLocations.isEmpty()) { //There are not affected Locations, nothing to Sync
									flash.success = message(code: 'sync.no.affected.locations');
								} else {//There are affected Locations, sync with all
									flash.success = message(code: "save.object.success");
									jsonTransactionStatus = syncDocumentsToLocations(affectedLocations);
									contentsInfo = documentService.getDocumentNames(affectedLocations);
								}
							} else {
								flash.error = message(code: 'save.object.error'); 
							}
						} catch(Exception dbEx) {
							//dbEx.printStackTrace();
							log.error(dbEx.getMessage(), dbEx);
							flash.error = message(code: 'save.object.error');
							status.setRollbackOnly()
						}
					}
				} else { 
					flash.error = message(code: 'default.error.problem');
				}

				
				def docsList = getDocumentsToList();
				def messageErrorDescriptionHash  = messageErrorService.getMessageErrorDescription();

				render(view: "assignLocation", model: [companyList: listCompanies,
					documentList: docsList.unique(),
					documentId: doc.id,
					jsonTransactionStatus:jsonTransactionStatus,
					documentsHashInfo:contentsInfo,
					messageErrorDescriptionHash:messageErrorDescriptionHash]);

				return;
			}
		}catch(Exception e){
			e.printStackTrace()
			flash.error = message(code: 'default.error.problem')
			redirect(action: 'assignLocation', model: [documentId: doc.id])
		}

	} // END saveLocations
	
	/**
	 * This method will try to send the Documents to a List of Locations;
	 * will set the flash messages according to result
	 *
	 * @param affectedLocations		List of Locations to be sync
	 * @return						Result of the Sync
	 */
	private syncDocumentsToLocations(def affectedLocations){
		def jsonTransactionStatus = documentService.updateBoxesOfLocations(affectedLocations);

		if(jsonTransactionStatus == null) {
			//There are not communication with the API, impossible to sync
			flash.error = message(code: 'general.no.communication.with.web.socket.api');
		} else {
			if(anyFailInSync(jsonTransactionStatus)){
				//At least 1 Box failed to Sync
				flash.warn = message(code:'boxes.syncErrorMessage');
			} else {
				flash.info = message(code: 'sync.full.success');
			}
		}
		return jsonTransactionStatus;
	}
	
	/**
	 * This method checks the result per Box and returns
	 * true if at least 1 box failed
	 *
	 * @param syncResponse	Sync response from Communication
	 * @return				At least 1 failed or not
	 */
	boolean anyFailInSync(syncResponse){
		for(int i=0; i < syncResponse.documentsStatus.size(); i++){
			if(!syncResponse.documentsStatus[i].successful){
				return true;
			}
		}
		return false;
	}
	
	protected def getDocumentsToList(){		
		def tempResults = null;
		def documents;
		if(isUserRoleAdmin()){
			//Admin will see All the List of Documents
			def query = Document.where{
				};
			tempResults = query.list().unique{ it.id };
			documents = Utils.sortByName(tempResults);
		} else {
			if(isUserRoleHelpDesk()){
				//Help Desk will see All the List of Contents
				def query = Document.where{
				};
				tempResults = query.list().unique{ it.id };
				documents = Utils.sortByName(tempResults);
			} else {
				def listCompanies = getCompaniesFromCurrentUser();
				tempResults = Document.getByCompanies(listCompanies.collect(){it.id}, 
					userSessionService.getCurrentUser());
				documents = Utils.sortByName(tempResults.list().unique{ it.id });
			}
		}
		
		return documents;
	}
	
	/**
	 * Action that allow to unassing a Document from a Location,
	 * must received the Document and the Location Id, realize the
	 * unassign process in DB, update the Location ExXtractors and
	 * render the Location Info Page at the Contents Tab
	 *
	 * @return
	 */
	def unassignOfLocation(){
		long locationId = 0;
		try {
			locationId = params.locId.toLong();
			CompanyLocation location = CompanyLocation.read(locationId);
			if(location){
				Document document = Document.read(params.id);
				if(document){
					if(location.enable && location.company.enable){
						document.removeFromLocations(location);
						if(document.save(flush: true, failOnError: true)){
							flash.success = message(code:'unassign.content.of.location.success');
						} else {
							flash.error = message(code:'general.object.not.found');
						}
					} else {
						flash.error = message('company.or.location.not.enable');
					}
				} else {
					//If the Content do not exists returns to the Location Info Page
					flash.warn = message(code:'general.object.not.found');
				}
			} else {
				//If the Location do not exists must go to the Location Page
				flash.warn = message(code:'general.object.not.found');
				locationId = 0;
			}
			
		} catch(Exception e) {
			log.error("Error unassigning a Document", e);
			flash.error = message(code:'default.error.problem');
		}
		if(locationId == 0){
			//There are not Location; must go to the Location List
			redirect(controller: "location", action: "listAll");
		} else {
			//If there is a Location must render the Location Show Page
			redirect(controller: "location", action: "show",  params: [id: locationId, tab: 'exx-document', a: 'syncDocument' ]);
		}
	}
}
