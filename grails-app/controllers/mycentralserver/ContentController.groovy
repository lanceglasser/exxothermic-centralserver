package mycentralserver

import java.awt.image.BufferedImage

import mycentralserver.app.Affiliate;
import mycentralserver.app.WelcomeAd;
import mycentralserver.beans.ImageUploadResult;
import mycentralserver.box.Box
import mycentralserver.content.Content
import mycentralserver.user.User
import mycentralserver.user.Role
import mycentralserver.company.Company
import mycentralserver.company.CompanyIntegrator
import mycentralserver.company.CompanyLocation
import mycentralserver.generaldomains.Scheduler
import grails.converters.JSON;
import mycentralserver.utils.AjaxResponse;
import mycentralserver.utils.PartnerEnum;
import mycentralserver.utils.SchedulerUtils;
import mycentralserver.utils.Utils
import mycentralserver.utils.UtilsResponse;
import mycentralserver.utils.ValidationHelper;

import javax.imageio.ImageIO;

import org.apache.commons.lang.StringUtils;

import mycentralserver.utils.Constants
import mycentralserver.utils.exceptions.ImageValidationException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Date;


class ContentController  extends BaseControllerMyCentralServerController {

	MessageErrorService messageErrorService
	RestClientService restClientService
	ContentService contentService
	def rackspaceService;
	def imageUploadService;
	def grailsLinkGenerator;
	def messageSource;

    def index() { }

	def create() {
		
		def user = getCurrentUser()
		def id = params.id == null ? params.id : 0
		def companyList =  getCompaniesFromCurrentUser()

		if(companyList.size() == 0) {
			redirect(controller:"company",action: "notcompany")
		}

		[content: new Content(), companyList:companyList,  companyId: id, affiliates: getAffiliateListByUser() ]
		

	} // END create

    /**
     * Ajax method that receives all the information to create a new Banner
     *
     * @return AjaxResponse object with the information of the process result
     */
    def save() {
        AjaxResponse ajaxResponse = new AjaxResponse(true);
        Content content;
        def deleteWhenFails = []; //Store the Urls of the Uploaded Images in case that the process fails
        try {
            content = new Content(params);
            content.type = params.type.id;
            content.url = params.urlReference;
            content.createdBy = getCurrentUser();
            final String maxDateAllowed = grailsApplication.config.maxDateForValidations;
            final UtilsResponse schedulerValidationResponse =
                    SchedulerUtils.validateSchedulerInformation(params, maxDateAllowed, content.schedule);
            final Scheduler mySchedule = schedulerValidationResponse.getObject(SchedulerUtils.SCHEDULER);
            content.schedule = mySchedule;
            if(schedulerValidationResponse.getResult()) {
                /** Load the featuredImage if is needed **/
                imageUploadService.uploadFeatureImages(content, null, deleteWhenFails, params, request, flash,
                        "", false, false);
                if(content.validate()) {
                    if(checkExtraInfo(content)){
                        mySchedule.save();
                        if (!content.save()){
                            ajaxResponse.setMsg(message(code: 'default.error.problem'));
                        } else { // no error
                            flash.success = message(code: 'default.message.success')
                        }
                    } else {
                        ajaxResponse.setMsg(message(code:'validation.error'));
                    }
                } else {
                    checkExtraInfo(content);
                    ajaxResponse.setMsg(message(code:'validation.error'));
                }
            } else {
                ajaxResponse.setMsg(message(code: schedulerValidationResponse.getMsgCodeToTranslate()));
            }
        } catch (ImageValidationException iex){
            log.error(iex);
            ajaxResponse.setMsgOnlyIfEmpty(iex.getMessage());
        } catch( Exception ex) {
            log.error ex;
            ajaxResponse.setMsgOnlyIfEmpty(message(code: 'default.error.problem'));
        }

        if(!ajaxResponse.hasEmptyMsg()){
            rackspaceService.deleteMultipleFiles(deleteWhenFails);
            ajaxResponse.addToMsg("<br>" + contentService.getErrorsAsUl(content));
        } else if(!content.enabled){
            ajaxResponse.setFullInformation(false, "", true,
                    grailsLinkGenerator.link(controller: 'content', action: 'listAll', absolute: true));
        } else {
            ajaxResponse.setFullInformation(false, "", true,
                    grailsLinkGenerator.link(controller:'content', action:'assignLocation', absolute:true, id:content.id));
        }
        render ajaxResponse.getFullResponseMap() as JSON;
    }

    /**
     * Renders the page with the list of allowed banners depending of the current logged user
     */
    def listAll() {
        def listCompanies = getCompaniesFromCurrentUser();
        if(listCompanies.size() > 0) {
            //check that at least one company has buttons
            def contentsList = getListOfContentsOfCurrentUserFromCompanyList(listCompanies);
            if(contentsList.size() > 0) {
                [contentsList:contentsList, contentsCount:contentsList.size(), TYPE_TEXT:Constants.CONTENT_TYPE_TEXT]

            } else {
                render(view: "noContentToManage", model:[noButtons:1, text:""]);
            }
        } else {
            redirect(controller:"company",action: "notcompany");
        }
    }

	def getEnableUserContents(listCompanies){
		def contentsList = getUserContents(listCompanies);
		Affiliate myAffiliate = getCurrentSessionAffiliate();
		
		for (Iterator<Content> iter = contentsList.iterator(); iter.hasNext();) {
			
			Content cb = iter.next();
			
			if(!cb.enabled){
				iter.remove();
			} else {
				if(isHelpDesk()){
					//Remove the Default Contents of Others Affiliates
					if(cb.affiliate != null && cb.affiliate != myAffiliate){
						//It's a default Content of Other Affiliate
						iter.remove();
					}
				} else {//If not HelpDesk will remove all the Default Contents of Affiliates
					if(cb.affiliate != null){
						//It's a default Content
						iter.remove()
					}
				}
			}
		}

		//return contentsList.unique().sort()
		return Utils.sortByTitle(contentsList.unique());
	}

	def getUserEnabledLocationsIds(listCompanies){
		def locations = []
		for (Iterator<Company> iter = listCompanies.iterator(); iter.hasNext();) {
			Company c = iter.next();
			for(location in c.locations){
				if(location.enable){
					locations += location.id
				}
			}
		}

		return locations.unique()
	}

	def getUserContents(listCompanies){
		if(isHelpDesk()){
			def contentList = Content.findAll();
			return contentList.unique().sort();
		} else {
			def contentsList = []
			for (Iterator<Company> iter = listCompanies.iterator(); iter.hasNext();) {
				Company c = iter.next();
				if(c.enable){
					for(location in c.locations){
						if(location.enable){
							contentsList += location.contents
						}
					}
				}
			}
			def user = getCurrentUser()
			for(content in user.createdContents){
				contentsList += content
			}
			return contentsList.unique().sort();
		}
	}

	def noContentToManage(){

	}

	def assignLocation(){

			def contentId = (params.id == null)? 0:params.id;

			def companyInstace = null

			//Get the list of enabled Companies associated for current user role with enabled locations
			def listCompanies = userSessionService.getCompaniesForAssociation();

			if(listCompanies.size() == 0) {
				redirect(action: "noContentToManage")
				return
			} else {
				if(listCompanies) {										
					def contentList = getListOfContentsOfCurrentUserFromCompanyList(listCompanies);
					def ids = contentList*.id;
					boolean allowToSeeContent = (contentId == 0 || ids.contains(new Long(contentId)));
					if(allowToSeeContent){
						if(contentList == null || contentList.size() == 0){
							redirect(action: "noContentToManage")
							return
						}else{
							[companyList: listCompanies, contentList: contentList.unique(), contentId: contentId]
						}
					} else {
						flash.error = "You are not allow to manage the Content with id: " + contentId;
						redirect(action: "listAll")
						return
					}
				}
			}

	}

	/**
	 * Loads and render the list of Locations with the
	 * associations of the selected Locations
	 * 
	 * @return
	 */
	def loadAssociatedLocations(){

		Content content=Content.get(params.buttonId)

		if (!content) {
			
			flash.error = message(code: 'default.customButton.not.found');
			
		} else {

			//Get the list of enabled Companies associated for current user role with enabled locations
			def listCompanies = userSessionService.getCompaniesForAssociation();

			if(!allowContentAction(listCompanies, content)){
				
				render(text: '<div class="alert alert-error">'
					+ message(code: 'default.not.found.message', args: [message(code: 'userInformation.label', default: 'User Information'), params.buttonId])
					+ '</div>', contentType: "text/html", encoding: "UTF-8");
				
			} else {

				String selectedLocations =""
				for(location in content.locations){
					selectedLocations += (selectedLocations.equals(""))? location.id:"," + location.id;
				}

				render (template:'companyButtonsTemplate', model:[content: content, companies: listCompanies, selectedLocations: selectedLocations])

			}
		}
	}

	def boolean allowContentAction(listCompanies, content){
		if(isHelpDesk()){
			return true;
		} else {
			def relatedButtons = []
	
			for(company in listCompanies){
				if(company.enable){
					for(location in company.locations){
						if(location.enable) {
							relatedButtons += location.contents
						}
					}
				}
			}
	
			def user = getCurrentUser()
			for(userContent in user.createdContents){
				relatedButtons += userContent
			}
	
			return (relatedButtons.any { it in (content) });
		}
	}

    /**
     * Renders the page with the information as read only of a Banner
     * @param id
     *      Id of the banner to display information; if the banner don't exists will redirect to the list
     */
    def show(long id) {
        Content content = Content.read(id);
        if(content) {
            def listCompanies = getCompaniesFromCurrentUser();
            if(allowContentAction(listCompanies, content)){
                [content: content]
            } else {
                flash.error = message(code: 'default.not.found.message',
                args: [message(code: 'userInformation.label', default: 'User Information'), id]);
                redirect(action: "listAll");
            }
        } else {
            flash.error = message(code: 'default.customButton.not.found');
            redirect(action: "listAll");
        }
    }

    /**
     * Renders the page with the information for edition of a Banner
     * @param id
     *      Id of the banner to display information; if the banner don't exists will redirect to the list
     */
    def edit(long id) {
        Content content = Content.read(id);
        if(content) {
            def listCompanies = getCompaniesFromCurrentUser();
            if(allowContentAction(listCompanies, content)) {
                [content: content,companyList: listCompanies, affiliates: getAffiliateListByUser()]
            } else {
                flash.error = message(code: 'default.not.found.message',
                args: [message(code: 'userInformation.label', default: 'User Information'), id]);
                redirect(action: "listAll",  params: []);
            }
        } else {
            flash.error = message(code: 'default.customButton.not.found');
            redirect(action: "listAll");
        }
    }

    /**
     * This method updates the information of a Banner, it will also sends the update to the related Venue Servers
     * 
     * @param id
     *         Id of the Content to be updated
     * @param version
     *         Version from UI to verifies previous update
     * @return HashMap<String, Object> with the result of the operation
     */
    def update(long id, Long version) {
        AjaxResponse ajaxResponse = new AjaxResponse(true);
        def imagesUrlsToDelete = [];
        def deleteWhenFails = [];
        Content content = null;
        String previousColor = "";
        try {
            content = Content.read(id);
            if (content.version > version) {
                ajaxResponse.setMsg(message(code: 'default.content.edit.version.error'));
            } else {
                final boolean previousGenDialogImages = content.genDialogImages;
                final boolean previousGenTabletDialogImages = content.genDialogImgForTablets;
                final String maxDateAllowed = grailsApplication.config.maxDateForValidations;
                final UtilsResponse schedulerValidationResponse =
                        SchedulerUtils.validateSchedulerInformation(params, maxDateAllowed, content.schedule);
                final Scheduler mySchedule = schedulerValidationResponse.getObject(SchedulerUtils.SCHEDULER);
                content.schedule = mySchedule;
                previousColor = content.color; // needed to check if
                content.properties = params;
                if(schedulerValidationResponse.getResult()) {
                    ImageUploadResult imgUploadResult = null;

                    /** Load the featuredImage if is needed **/
                    imageUploadService.uploadFeatureImages(content, imagesUrlsToDelete, deleteWhenFails,
                            params, request, flash, previousColor, previousGenDialogImages, previousGenTabletDialogImages);

                    content.url = params.urlReference;
                    if(content.validate()) {
                        if(checkExtraInfo(content)){
                            content.schedule.save();
                            if (!content.save()){
                                ajaxResponse.setMsg(message(code: 'default.error.problem'));
                            }
                        } else {
                            ajaxResponse.setMsg(message(code:'validation.error'));
                        }
                    } else {
                        checkExtraInfo(content);
                        ajaxResponse.setMsg(message(code:'validation.error'));
                    }
                } else {
                    ajaxResponse.setMsg(message(code: schedulerValidationResponse.getMsgCodeToTranslate()));
                }
            }
        } catch (ImageValidationException iex){
            log.error(iex);
            ajaxResponse.setMsgOnlyIfEmpty(iex.getMessage());
        } catch(Exception ex) {
            log.error ex;
            ajaxResponse.setMsgOnlyIfEmpty(message(code: 'default.error.problem'));
        }

        if(ajaxResponse.hasEmptyMsg()) {
            rackspaceService.deleteMultipleFiles(imagesUrlsToDelete);
            final String redirectUrl = content.locations.isEmpty()?
                grailsLinkGenerator.link(controller: 'content', action: 'listAll', absolute: true) :
                grailsLinkGenerator.link(controller: 'content', action: 'sync', absolute: true, id: content.id);
            ajaxResponse.setFullInformation(false, "", true, redirectUrl);
        } else {
            rackspaceService.deleteMultipleFiles(deleteWhenFails);
            ajaxResponse.addToMsg("<br>" + contentService.getErrorsAsUl(content));
        }
        render ajaxResponse.getFullResponseMap() as JSON;
    }
	
	
	def renderListViewWithSyncResult(def updatedLocations, def jsonTransactionStatus, def boxLocationData){
		def contentsInfo = contentService.getContentNames(updatedLocations);
		def messageErrorDescriptionHash  = messageErrorService.getMessageErrorDescription();
		
		def listCompanies = getCompaniesFromCurrentUser();		
		def contentsList = getListOfContentsOfCurrentUserFromCompanyList(listCompanies);
		
		if(contentsList.size() == 0) {
			render(view: "noContentToManage", model:[noButtons:1, text:"",jsonTransactionStatus:jsonTransactionStatus,
					contentsHashInfo:contentsInfo, messageErrorDescriptionHash:messageErrorDescriptionHash])
		} else {
			render(view: Constants.VIEW_LISTALL,
				model:[contentsList: Utils.paginateList(contentsList,params) , contentsCount: contentsList.size(),
					TYPE_TEXT: Constants.CONTENT_TYPE_TEXT, jsonTransactionStatus:jsonTransactionStatus, boxLocationData:boxLocationData,
					contentsHashInfo:contentsInfo, messageErrorDescriptionHash:messageErrorDescriptionHash])
		}
	}
	
	/**
	 * Deletes a Content from DB and will sends the Content
	 * information to the affected Locations
	 *
	 * @param id	Id of the Content to be deleted.
	 *
	 * @return
	 */
	def delete(long id)
	{
		def jsonTransactionStatus = null;
		def boxLocationData = null
		
		try {
			Content content=Content.read(id);
			String featuredImageUrl
			String dialogImageUrl
			String featuredImageUrlTablet;
			String dialogImageUrlTablet;
			if (!content ) {
				//If the Content do not exists or don't has access
				flash.error = message(code: 'content.not.found');
				redirect(action:Constants.VIEW_LISTALL);
			} else {
					// back up the images URLs to delete from the server when the content have been deleted correctly
					featuredImageUrl=content.featuredImage
					dialogImageUrl=content.dialogImage
					featuredImageUrlTablet=content.featuredImageTablet
					dialogImageUrlTablet=content.dialogImageTablet
					
					def locations = content.locations;
					def updatedLocations = CompanyLocation.findAllByIdInList(content.locations.collect {loc ->		   
							return loc.id;
					});
					content.delete();
					flash.success = message(code: 'general.object.delete.success.male', args:[message(code:'banner')]);
					
					// if everything is ok, remove the images from de server
					String[] imagesUrls = [featuredImageUrl, dialogImageUrl, featuredImageUrlTablet, dialogImageUrlTablet];
					rackspaceService.deleteMultipleFiles(imagesUrls);
										
					//If the object is deleted from DB, try to sync the ex associated locations
					if(!updatedLocations.isEmpty()) {
						//Must sync some Locations						
						(jsonTransactionStatus, boxLocationData) =
							syncContentsToLocations(updatedLocations);
					}
					renderListViewWithSyncResult(updatedLocations, jsonTransactionStatus, boxLocationData);
			}
		} catch(Exception e){
			log.error("Error deleting the Content with id=" + id + ": ${e.message}", e);
			throw e;
		}
	}

	/**
	 * This method receives a list of Location with ids to associate to a 
	 * Content; the method will also sends the Contents information to the
	 * affected Locations (remove or add the Content)
	 * 
	 * @return
	 */
	def saveLocations()
	{
		Content content;
		//Get the list of enabled Companies associated for current user role with enabled locations
		def listCompanies = userSessionService.getCompaniesForAssociation();
		def contentList = getListOfContentsOfCurrentUserFromCompanyList(listCompanies);
		
		try {

			content = Content.get(params.buttons)

			if (!content) {
				flash.error = message(code: 'default.customButton.not.found')
				redirect(action: "listAll")
			} else {

				List currentLocations = new ArrayList<Content>();
				def affectedLocations = []
				currentLocations.addAll(content.locations)

				def locationsToSave = (params.locationsToSave == null)? "":params.locationsToSave;
				def newLocations = locationsToSave.split(",");
				for(companyLocation in currentLocations){
					if(!newLocations.any { it == (companyLocation.id.toString()) }){
						if(companyLocation.enable && companyLocation.company.enable){
							content.removeFromLocations(companyLocation)
							affectedLocations.add(companyLocation.id)
						}
					}
				}

				for (c in newLocations){
					if ( c != "" && !currentLocations.any { it.id.toString() == (c) }){
						CompanyLocation location = CompanyLocation.findById(c)
						content.addToLocations(location)
						affectedLocations.add(location.id)
					}
				}

				def jsonTransactionStatus = null
				def boxLocationData = null
				HashMap<String, String> contentsInfo = new HashMap<String, String>()

				if(content.validate()) {					
					if(content.save(flush:true)) {
														
						if(!affectedLocations.isEmpty()) {
							flash.success = message(code: "save.object.success");
							//Must sync some Locations
							def updatedLocations = CompanyLocation.findAllByIdInList(affectedLocations);
							
							(jsonTransactionStatus, boxLocationData) = syncContentsToLocations(updatedLocations);
							contentsInfo = contentService.getContentNames(updatedLocations);
							
						} else {//There are not affected Locations, nothing to Sync
							flash.success = message(code: 'sync.no.affected.locations')
						}
						
					} else {//Error Saving the Domain
						flash.error = message(code: 'save.object.error');
					}
				} else {
					//Error validating the Domain? This should not happend but ok
					flash.error = message(code: 'save.object.error');
				}
				
				def messageErrorDescriptionHash  = messageErrorService.getMessageErrorDescription();
				render(view: "assignLocation", model: [companyList: listCompanies, boxLocationData:boxLocationData, contentList: contentList.unique(), contentId: content.id, jsonTransactionStatus:jsonTransactionStatus, contentsHashInfo:contentsInfo, messageErrorDescriptionHash:messageErrorDescriptionHash])
				return

			}
		} catch(Exception e) {
			e.printStackTrace();
			log.error("Error saving Contents association", e);
			flash.error = message(code: 'save.object.error');
			render(view: "assignLocation", model: [companyList: listCompanies, contentList: contentList.unique(), contentId: content.id])
		}

	} // END saveLocations

    /**
     * This method synchronizes the Contents information with the boxes of the associate Locations and renders the 
     * assignLocation view with the information of the synchronization result
     */
    def syncronizeContent(long customButtonId) {
        Content content = Content.read(customButtonId);
        def messageErrorDescriptionHash =  messageErrorService.getMessageErrorDescription();
        def jsonTransactionStatus = null;
        def boxLocationData = null;
        def affectedLocations = new ArrayList<Content>();
        def locations = content.locations;
        //Gets the list of enabled Companies associated for current user role with enabled locations
        def listCompanies = userSessionService.getCompaniesForAssociation();
        def contentsList = getEnableUserContents(listCompanies);
        HashMap<String, String> contentsInfo = contentService.getContentNames(locations);
        if(locations.isEmpty()) { //There are not affected Locations, nothing to Sync
            flash.success = message(code: 'sync.no.affected.locations');
        } else {
            //Must sync some Locations
            (jsonTransactionStatus, boxLocationData) = syncContentsToLocations(locations);
        }
        render(view: "assignLocation", model: [ companyList: listCompanies,
            contentList: contentsList.unique(),
            contentId: customButtonId,
            jsonTransactionStatus:jsonTransactionStatus,
            boxLocationData:boxLocationData,
            contentsHashInfo:contentsInfo,
            messageErrorDescriptionHash:messageErrorDescriptionHash])
    }

    /**
     * This method will try to send the Banners to a List of Venues and set the flash messages according to result
     *
     * @param affectedLocations
     *     List of Locations to be sync
     * @return Result of the Sync
     */
    private syncContentsToLocations(def affectedLocations){
        def jsonTransactionStatus;
        def boxLocations;
        (jsonTransactionStatus, boxLocations) = contentService.updateBoxesOfLocations(affectedLocations);
        if(jsonTransactionStatus) {
            if(anyFailInSync(jsonTransactionStatus)){ // At least 1 Box failed to Sync
                flash.warn = message(code:'boxes.syncErrorMessage');
            } else {
                flash.info = message(code: 'sync.full.success');
            }
        } else {
            // There is not communication with the API, impossible to sync
            flash.error = message(code: 'general.no.communication.with.web.socket.api');
        }
        return [jsonTransactionStatus, boxLocations];
    }

    /**
     * This method checks the result per Box and returns true if at least 1 box failed
     * 
     * @param syncResponse
     *         Synchronization response from Communication Component, will never be null
     * @return True or False depending if at least 1 failed or not
     */
    private boolean anyFailInSync(syncResponse) {
        for(int i=0; i < syncResponse.contentsStatus.size(); i++){
            if(!syncResponse.contentsStatus[i].successful){
                return true;
            }
        }
        return false;
    }

    /**
     * This method will return the List of allowed Contents of the current session user
     * @return List of Contents associated to the allowed Companies
     */
    def getListOfContentsOfCurrentUser(){
        def listCompanies = getCompaniesFromCurrentUser();
        return getListOfContentsOfCurrentUserFromCompanyList(listCompanies);
    }
		
	def getListOfContentsOfCurrentUserFromCompanyList(listCompanies){
		def tempResults = null;
		def contentsList;
		if(isUserRoleAdmin()){
			//Admin will see All the List of Contents
			def query = Content.where{
				ne('type', 'offer')
			};
			tempResults = query.list().unique{ it.id };				
			contentsList = Utils.sortByTitle(tempResults);
		} else {
			if(isUserRoleHelpDesk()){
				//Help Desk will see All the List of Contents except the Default of others Affiliates
				Affiliate myAffiliate = getCurrentSessionAffiliate();
				def query = Content.where{
					ne('type', 'offer')
				};
				if(myAffiliate.code != PartnerEnum.getDefaultPartnerCode()){
					//Other affiliates can se only their Default Contents
					query = Content.where{
						ne('type', 'offer')
						or{
							isNull('affiliate')
							eq('affiliate', myAffiliate)
						}
					};
				}
				
				tempResults = query.list().unique{ it.id };				
				contentsList = Utils.sortByTitle(tempResults);
			} else {
				tempResults = Content.getNotOffersContentsByCompanies(listCompanies.collect(){it.id}, getCurrentUser());
				def allButtons = tempResults.listDistinct();
				contentsList = Utils.sortByTitle(tempResults.list().unique());
				//if not admin or help desk remove the default Contents of Affiliates
				contentsList = contentsList.findAll{it.affiliate == null };
			}
		}		
		
		return contentsList;
		
	}
	
	/**
	 * Action that allow to unassing a Content from a Location,
	 * must received the Content and the Location Id, realize the 
	 * unassign process in DB, update the Location ExXtractors and
	 * render the Location Info Page at the Contents Tab
	 * 
	 * @return
	 */
	def unassignOfLocation(){
		long locationId = 0;
		try {
			locationId = params.locId.toLong();
			CompanyLocation location = CompanyLocation.read(locationId);
			if(location){
				Content content = Content.read(params.id);
				if(content){
					if(location.enable && location.company.enable){
						content.removeFromLocations(location);
						if(content.save()){
							flash.success = message(code:'unassign.content.of.location.success');
						} else {
							flash.error = message(code:'general.object.not.found');
						}
					} else {
						flash.error = message('company.or.location.not.enable');
					}
				} else {
					//If the Content do not exists returns to the Location Info Page
					flash.warn = message(code:'general.object.not.found');
				}
			} else {
				//If the Location do not exists must go to the Location Page
				flash.warn = message(code:'general.object.not.found');
				locationId = 0;
			}
			
		} catch(Exception e) {
			log.error("Error unassigning a Content", e);
			flash.error = message(code:'default.error.problem');
		}
		if(locationId == 0){
			//There are not Location; must go to the Location List
			redirect(controller: "location", action: "listAll");
		} else {
			//If there is a Location must render the Location Show Page
			redirect(controller: "location", action: "show",  params: [id: locationId, tab: 'exx-content', a: 'syncContent' ]);
		}
	}
	
	/**
	 * This method will do some validation data of a Content
	 * and assign found errors to the fields or to the flash.error message
	 * 
	 * @param content	Content to be validated
	 * @return			True when Ok, False if not
	 */
    def checkExtraInfo(Content content){
        boolean isOk = true;
        if (content.type == Constants.CONTENT_TYPE_TEXT && StringUtils.isBlank(content.description)) {
            // For text banners the Description is required
            isOk = false;
            content.errors.rejectValue('description',
                message(code:'field.not.empty',args:[message(code:'description')]));
        }
        if (StringUtils.isNotBlank(content.url) && ValidationHelper.isInvalidUrl(content.url)) {
            // Url is Invalid
            isOk = false;
            content.errors.rejectValue('url', message(code:'invalid.url'));
        }
        if(ValidationHelper.isInvalidColor(content.color)) { // Color is Invalid
            isOk = false;
            content.errors.rejectValue('color', message(code:'invalid.color', args:[message(code:'default.field.customeButtons.channelColor')]));            
        }
        return isOk;
    }

    /**
     * This method will try to synchronizes the list of banners to the associated venues of a banner
     * 
     * @param id
     *      Id of the Banner to check for Venues to synchronizes
     */
    def sync(long id){
        Content content = Content.read(id);
        def jsonTransactionStatus;
        def boxLocationData = null;
        def contentsInfo = null;
        if(content) {
            //If the object is updated at DB, try to sync the associated locations
            if(content.locations.size() > 0) { // Must sync some Locations
                (jsonTransactionStatus, boxLocationData) = syncContentsToLocations(content.locations);
            }
            contentsInfo = contentService.getContentNames(content.locations);
        }
        def messageErrorDescriptionHash = messageErrorService.getMessageErrorDescription();
        def listCompanies = getCompaniesFromCurrentUser();
        def contentsList = getListOfContentsOfCurrentUserFromCompanyList(listCompanies);
        if(contentsList.size() == 0) {
            render(view: "noContentToManage", model:[noButtons:1, text:"",jsonTransactionStatus:jsonTransactionStatus,
                contentsHashInfo:contentsInfo, messageErrorDescriptionHash:messageErrorDescriptionHash])
        } else {
            render(view: Constants.VIEW_LISTALL,
            model:[contentsList: Utils.paginateList(contentsList,params) , contentsCount: contentsList.size(),
                TYPE_TEXT: Constants.CONTENT_TYPE_TEXT, jsonTransactionStatus:jsonTransactionStatus, boxLocationData:boxLocationData,
                contentsHashInfo: contentsInfo, messageErrorDescriptionHash: messageErrorDescriptionHash])
        }
    }

    private getModelForSaveInErrorCase(Content contentParam) {
        def user = userSessionService.getCurrentUser();
        def companyList =  getCompaniesFromCurrentUser();
        //Return the view paramters
        return [content:contentParam, companyList:companyList, affiliates: getAffiliateListByUser()]
    }
} // END ContentController