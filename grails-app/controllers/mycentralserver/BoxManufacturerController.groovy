package mycentralserver

import mycentralserver.box.BoxDeviceType
import mycentralserver.box.BoxManufacturer;
import mycentralserver.generaldomains.Catalog;
import mycentralserver.generaldomains.CatalogCategory;
import mycentralserver.utils.Constants;
import mycentralserver.utils.Utils;

class BoxManufacturerController extends BaseControllerMyCentralServerController {


	def listAll(){
		def offset = getOffsetParameterToPagination()
		try {
			def manufacturers = BoxManufacturer.list()
			if (manufacturers != null && manufacturers.size()  == 0) {
				redirect(controller: Constants.CONTROLLER_BOX_MANUFACTURER,action: Constants.VIEW_NOT_MANUFACTURER, params: [offset: offset])
				return
			}else{
				def max = Math.min(params.max ? params.int(Constants.PARAM_MAX) : 10, 100)
				params.max = max				
				[manufacturers:Utils.paginateList(manufacturers, params), manufacturersCount:manufacturers.size(), offset:offset]
			}
		}
		catch(Exception ex) {
			ex.printStackTrace()
			log.error(ex);
			flash.error=message(code: 'default.error.unableDisplayLocations')
		}
	}// END listAll

	def notManufacturer()
	{

	}//END notManufacturers

	def edit(long id)
	{
		def manufacturer=BoxManufacturer.read(id)
		if(!manufacturer.deviceType)
		{
			manufacturer.deviceType=new BoxDeviceType()
		}
		def brands= Catalog.findAllByCatalogCategory(CatalogCategory.findByName(Constants.BRANDS))
		def memoryTypes= Catalog.findAllByCatalogCategory(CatalogCategory.findByName(Constants.MEMORIES_TYPE))
		def memories=Catalog.findAllByCatalogCategory(CatalogCategory.findByName(Constants.MEMORIES))
		def offset = getOffsetParameterToPagination()

		if (!manufacturer) {
			flash.error = message(code: 'default.certificate.not.found')
			redirect(action: Constants.VIEW_LISTALL, controller:Constants.CONTROLLER_BOX_MANUFACTURER, params: [offset: offset])
			return
		}
		[manufacturer: manufacturer,machineBrands:brands,processorBrands:brands,memoryTypes:memoryTypes,memories:memories, offset: offset]
	}//END edit

	def update(long id, Long version)
	{
		def offset = getOffsetParameterToPagination()
		if ( params.size() > 3) { //if the only values are action and controller, then we are not coming from the form
			def manufacturer=null
			manufacturer = BoxManufacturer.read(id)
			if (manufacturer.version > version) {
				flash.error=message(code: 'default.location.edit.version.error')
				manufacturer.errors.rejectValue("version", 'default.location.edit.version.error')
			}else{
				try{
					//if the BoxManufacturer doesn't have a BoxDeviceType. we must create the object
					if (manufacturer.deviceType==null) {
						manufacturer.deviceType=new BoxDeviceType()
					}
					manufacturer.deviceType.properties=params
					manufacturer.deviceType.createdBy=getCurrentUser()
					manufacturer.properties=params

					if(manufacturer.deviceType.validate() && manufacturer.validate())
					{
						BoxManufacturer.withTransaction {status ->
							try{
								manufacturer.deviceType.save()
								manufacturer.save()
								flash.success=message(code: 'default.message.success')
							}catch(Exception dbEx){
								dbEx.printStackTrace()
								status.setRollbackOnly()
								flash.error=dbEx.getMessage()
							}
						}
					}
					else
					{
						flash.error=message(code: 'default.error.problem')
					}
				}

				catch(Exception ex)
				{
					ex.printStackTrace()
					flash.error=message(code: 'default.error.problem')
				}

			}

			if(flash.error != null){
				def brands= Catalog.findAllByCatalogCategory(CatalogCategory.findByName(Constants.BRANDS))
				def memoryTypes= Catalog.findAllByCatalogCategory(CatalogCategory.findByName(Constants.MEMORIES_TYPE))
				def memories=Catalog.findAllByCatalogCategory(CatalogCategory.findByName(Constants.MEMORIES))
				render(view: Constants.VIEW_EDIT, model:[manufacturer: manufacturer,machineBrands:brands,processorBrands:brands,memoryTypes:memoryTypes,memories:memories, offset: offset]	)
				return
			}else{
				redirect(action: Constants.VIEW_EDIT,id:id, params: [offset: offset])
				return
			}
		}else{
			flash.error = message(code: 'default.not.found.message')
			redirect(action: Constants.VIEW_LISTALL,params: [offset: offset])
			return
		}

	}//END update

	def show(long id)
	{
		def offset = getOffsetParameterToPagination()

		try{
			def manufacturer=BoxManufacturer.get(id)

			if (!manufacturer) {
				flash.error = message(code: 'default.certificate.not.found')
				redirect(action: Constants.VIEW_LISTALL, controller:"boxManufacturer", params: [offset: offset])
				return
			}else{
				[manufacturer: manufacturer, offset: offset]
			}
		}catch(Exception ex){
			flash.error = ex.getMessage()
			ex.printStackTrace()
			redirect(action: Constants.VIEW_LISTALL, params: [offset: offset])
			return
		}
	}//End Show
}//END BoxManufacturer

