package mycentralserver

import grails.converters.JSON;
import mycentralserver.box.BoxManufacturer;
import mycentralserver.company.CompanyLocation;
import mycentralserver.user.User;
import mycentralserver.utils.Constants;
import mycentralserver.utils.EncrypterHelper;
import mycentralserver.utils.FileHelper
import mycentralserver.utils.PasswordGeneratorHelper;
import mycentralserver.utils.SerialNumberGeneratorHelper;
import mycentralserver.utils.enumeration.Architecture;

class LoadStressController {

	def imageUploadService;
	def private STATUS_SUCCESSFUL = "Successful"
	
	public static enum Status {
		STATUS_EXCEPTION("Exception"),
		STATUS_SUCCESSFUL("Successful"),
		STATUS_FAILED("Failed");

		private final String status;

		private Status(String status) {
			this.status = status;
		}

		public String getStatus() {
			return status;
		}

		public static Status getByName(String name) {
			for (Status prop : values()) {
				if (prop.getStatus().equals(name)) {
					return prop;
				}
			}

			throw new IllegalArgumentException(name + " is not a valid PropName");
		}
	} // ENUM Status
	
	public static enum ErrorCode {
		NO_ERROR("0"),
		TIME_OUT("1"),
		GENERAL_EXCEPTION("2"),
		NO_CHANNELS_FOUND("3"),
		NO_DEVICE_FOUND("4"),
		UPDATE_SOFTWARE_FAILED("5"),
		BLACK_LISTED("6"),
		INVALID_PARAMETERS("7")

		private final String errorCode;

		private ErrorCode(String errorCode) {
			this.errorCode = errorCode;
		}

		public String getErrorCode() {
			return errorCode;
		}

		public static ErrorCode getByErrorCode(String name) {
			for (ErrorCode prop : values()) {
				if (prop.getErrorCode().equals(name)) {
					return prop;
				}
			}

			throw new IllegalArgumentException(name + " is not a valid PropName");
		}
	} // END ErrorCode

	static allowedMethods =[
		generateCertificateForSerial: 'GET',
		getLocationList: 'GET'
		]
	
	def manufacture(){
		
	}
	
    def generateCertificateForSerial() {
		
		HashMap jsonMap = new HashMap()
		def isSuccessful = false
		def statusResponse = null
		def description = null
		def error = null
		String secretValue = grailsApplication.config.encryptionKey;
		String ivValue = grailsApplication.config.encryptionIv;
		
		try {
			def serial = params.serial;
			def mustManufactured = (params.manu)? ((params.manu == 1)? true:false):false;
			
			if(!serial) {

				jsonMap = setVariablesSuccessfulStatusDescriptionAndErrorInJsonMap(jsonMap, false, null, ErrorCode.GENERAL_EXCEPTION.getErrorCode() )
				response.setStatus(Constants.INTERNAL_ERROR)
				log.warn("In generateCertificate any paramerts is NULL")

			} else {
			
					/* For dummy generation */
					def json = new HashMap();
					json.certificate = "mycertificateof" + serial;
					json.certificationLabel = "certLabel";
					json.certificationLocation = "C:/cecropiaSolutions/temp/certifications/boxCertificateSerial_" + serial;
					json.certificationFileName = "boxCertificateSerial_" + serial;
					
					/*
					 String hmacCS = EncrypterHelper.encryptWithAes( secretValue, ivValue, serial); 
					 def json = restClientService.generateCertificate(serial, hmacCS);
					 */

					if(!json) {
						jsonMap = setVariablesSuccessfulStatusDescriptionAndErrorInJsonMap(jsonMap, false,  "Can not execute the call to refullService in CentralServer", ErrorCode.GENERAL_EXCEPTION.getErrorCode() )
						response.setStatus(Constants.NOT_FOUND)
					} else {

						jsonMap.certificate = json.certificate
						jsonMap.certificationLabel = json.certificationLabel
						jsonMap.serial = serial
						
						jsonMap = setVariablesSuccessfulStatusDescriptionAndErrorInJsonMap(jsonMap, true,  null, ErrorCode.NO_ERROR.getErrorCode() )
									response.setStatus(Constants.OK)
					}
					
					int arch = SerialNumberGeneratorHelper.getArchitectureReferenceFromCode(Architecture.X32.toString());
					def user = User.findByEmail(Constants.ADMIN_USER_EMAIL)
					BoxManufacturer boxManufacturer = BoxManufacturer.findBySerialNumber(serial);
					
					if(boxManufacturer){
						//if the record exists, update the fields
						boxManufacturer.certificateLocation = json.certificationLocation;
						boxManufacturer.certificateFileName = json.certificationFileName;
						boxManufacturer.architecture = Architecture.X32.getValue();
					} else {
						//If the record do not exists, must create a new one
						boxManufacturer = new BoxManufacturer( serialNumber:serial,
							certificateLocation: json.certificationLocation,
							certificateFileName:json.certificationFileName,
							architecture:Architecture.X32.getValue(),
							createdBy:user);
					}
					
					if(boxManufacturer.validate()) {

						BoxManufacturer.withTransaction { status ->
							try {
								boxManufacturer.save()
								jsonMap = setVariablesSuccessfulStatusDescriptionAndErrorInJsonMap(jsonMap, true,  null, ErrorCode.NO_ERROR.getErrorCode() )
								response.setStatus(Constants.OK)
							} catch(Exception dbEx) {
								log.error dbEx
								status.setRollbackOnly()
								jsonMap = setVariablesSuccessfulStatusDescriptionAndErrorInJsonMap(jsonMap, false, null, ErrorCode.GENERAL_EXCEPTION.getErrorCode() )
								response.setStatus(Constants.INTERNAL_ERROR)
							}
						} // END  BoxManufactor.withTransaction
					} else {
						jsonMap = setVariablesSuccessfulStatusDescriptionAndErrorInJsonMap(jsonMap, false,  null, ErrorCode.GENERAL_EXCEPTION.getErrorCode() )
						response.setStatus(Constants.INTERNAL_ERROR)
					}
			}
		} catch (Exception ex) {
			log.error ex
			jsonMap = setVariablesSuccessfulStatusDescriptionAndErrorInJsonMap(jsonMap, false,  null, ErrorCode.GENERAL_EXCEPTION.getErrorCode() )
			response.setStatus(Constants.INTERNAL_ERROR)
		}
		def jsonToSend = (jsonMap as JSON)
		log.info(" generateCertificate sending jsonResponse: [" + jsonToSend + "]")
		render jsonToSend
	}
	
	def getLocationList() {
		
		HashMap jsonMap = new HashMap()
		def isSuccessful = false
		def statusResponse = null
		def description = null
		def error = null
		
		try {
			def locationsList = []

			def locations = CompanyLocation.findAll();
			for(location in locations){
				jsonMap = new HashMap();
				jsonMap.id = location.id;
				jsonMap.name = location.name;
				locationsList.add(jsonMap);
			}
			
			jsonMap = new HashMap();
			jsonMap.locations = locationsList;
			jsonMap = setVariablesSuccessfulStatusDescriptionAndErrorInJsonMap(jsonMap, true,  null, ErrorCode.NO_ERROR.getErrorCode() )
			response.setStatus(Constants.OK);
			
		} catch (Exception ex) {
			log.error ex
			jsonMap = setVariablesSuccessfulStatusDescriptionAndErrorInJsonMap(jsonMap, false,  null, ErrorCode.GENERAL_EXCEPTION.getErrorCode() )
			response.setStatus(Constants.INTERNAL_ERROR)
		}
		def jsonToSend = (jsonMap as JSON)
		log.info(" generateCertificate sending jsonResponse: [" + jsonToSend + "]")
		render jsonToSend
	}
	
	private def setVariablesSuccessfulStatusDescriptionAndErrorInJsonMap(jsonMap, isSuccessful,  description, error ) {
		jsonMap.results = isSuccessful
		jsonMap.status  =  isSuccessful ? Status.STATUS_SUCCESSFUL.getStatus() : Status.STATUS_FAILED.getStatus()
		jsonMap.statusDescription = description
		jsonMap.error   = error
		return jsonMap;
	}
	
	def testImageDownload(){
		HashMap jsonMap = new HashMap();
		
		String url = "http://8f82346845b644ed24d0-32f0335e394c808f4b71e9bb2b1fe369.r93.cf1.rackcdn.com/1416493030730.jpg";
		imageUploadService.downloadImageFromUrl(url);
		jsonMap.success = true;
		jsonMap.msg = "OK";
		def jsonToSend = (jsonMap as JSON);
		render jsonToSend;
	}
}
