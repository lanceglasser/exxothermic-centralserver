package mycentralserver

import grails.converters.JSON

import java.security.MessageDigest
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;

import org.apache.commons.lang.StringUtils;

import mycentralserver.app.Affiliate;
import mycentralserver.box.Box
import mycentralserver.box.BoxManufacturer;
import mycentralserver.box.software.BoxSoftware
import mycentralserver.user.User
import mycentralserver.utils.DigestHelper
import mycentralserver.utils.FileHelper
import mycentralserver.utils.Constants
import mycentralserver.utils.SerialNumberGeneratorHelper;
import mycentralserver.utils.ValidationHelper;
import mycentralserver.box.BoxStatus
import sun.misc.BASE64Encoder
import mycentralserver.user.Role
import mycentralserver.utils.RoleEnum
import mycentralserver.utils.BoxStatusEnum

class BoxSoftwareController extends BaseControllerMyCentralServerController {

    /* Constants */
    private static final String VIEW_NO_SOFTWARE_TO_UPGRADE = "noSoftwareUpdate";
    private static final String DEFAULT_NOT_EMPTY_VALUE = ".";
    private static final String DEFAULT_UNIQUE_VALUE = 0;

    /* Inject required services */
	def grailsApplication;
    def boxSoftwareService;
	def softwareUpgradeService;
    def softwareUpdateControlService;
    def userSessionService;
	RestClientService restClientService;
	MessageErrorService messageErrorService;
	
    def index() { 
		
	}
	
	def register() {	
		def boxSoftware = new BoxSoftware(); 
		def listSoftwareVersion = BoxSoftware.findAll([max: 1, sort: "versionSoftware", order: "desc"])
	
		[boxSoftware:boxSoftware]
	}
	
	def listAll() {
		
		def listSoftwareVersion = null
		def softwareCount 
		def offset = this.getOffsetParameterToPagination()
		
		if(isAdminOrHelpDesk()){
			
			//params.max = Math.min(params.max ? params.int('max') : 10, 100)
			params.sort = "name"
			params.order="asc"
			listSoftwareVersion = BoxSoftware.findAll(params);
			softwareCount = BoxSoftware.count()

		} else {
		

			//def lastObject = BoxSoftware.listOrderByDateCreated(max: 1, order: "desc")[0]		
			def lastObject = BoxSoftware.getLastSoftwareVersionEnabled.list(max: 1)[0]
			listSoftwareVersion = new ArrayList()
			listSoftwareVersion.add(lastObject)			
			softwareCount = listSoftwareVersion.size()
		}
				
		if(listSoftwareVersion == null || listSoftwareVersion.isEmpty()) {
			render(view:"noSoftwareUpdate")
			return
		}
		
		[listSoftwareVersion: listSoftwareVersion, softwareCount: softwareCount, offset:offset]
	} // END listAll
	
	def show(Long id) {
		def boxSoftware = BoxSoftware.get(id)
		def offset = this.getOffsetParameterToPagination()
		
		if (!boxSoftware) {
			flash.message = message(code: 'default.not.found.message')
			redirect(action: "listAll", params: [offset: offset])
			return
		}
				
		[boxSoftware:boxSoftware, offset: offset]
	} // END show
	
	def edit(Long id) {
		def boxSoftware = BoxSoftware.read(id)
		def offset = this.getOffsetParameterToPagination()
		
		if (!boxSoftware) {
			flash.error = message(code: 'default.not.found.message')
			redirect(action: "listAll", params: [offset: offset])
			return
		}else if (!boxSoftware.isSupported){
			flash.error = message(code: 'default.version.software.notSupported', args:[boxSoftware.name])
			redirect(action: "listAll", params: [offset: offset])
			return
		}
				
		[boxSoftware:boxSoftware, offset: offset]
	}
	
	def noSoftwareUpdate() {
	
	}

    /**
     * Action method that handles the creation of a new Software Version
     */
    def uploadSoftwareUpdate() {
        def fileName;
        BoxSoftware boxSoftware;
        try {
            if (ValidationHelper.isValidInteger(params.versionSoftware) ) {
                boxSoftware = new BoxSoftware(params);
                if (boxSoftwareService.isValidVersionLabelToSave(null, params.versionSoftwareLabel,
                        params.associatedArchitectures)) {
                    boxSoftware.createdBy = userSessionService.getCurrentUser();
                    boxSoftware.lastUpdatedBy = userSessionService.getCurrentUser();
                    boxSoftware.isSupported = true;
                    def file = request.getFile(Constants.PARAM_FILE);
                    boxSoftware = setFileInformationInBoxSoftware( file, boxSoftware);
                    def fileNameOrigin = file.getOriginalFilename();
                    if (file.isEmpty() && !fileNameOrigin.isEmpty()) {
                        flash.error = message(code: 'validation.error');
                    } else {
                        final boolean isTarGz = FileHelper.isFileTypeTarGz(fileNameOrigin);
                        if (boxSoftware.validate()) {
                            if (isTarGz) {
                                final String path = getPathFromFile(fileNameOrigin, boxSoftware.versionSoftwareLabel);
                                this.saveSoftwareVersionAndFile(boxSoftware, path, file, true);
                            } else {
                                flash.error = message(code: 'validation.error');
                            }
                        } else {
                            flash.error = message(code: 'validation.error');
                        }
                    }
                } else {
                    flash.error = message(code: 'software.version.label.already.in.use');
                }
            } else {
                params.versionSoftware = DEFAULT_UNIQUE_VALUE;
                boxSoftware = new BoxSoftware(params);
                boxSoftware.createdBy = userSessionService.getCurrentUser();
                boxSoftware.lastUpdatedBy = userSessionService.getCurrentUser();
                boxSoftware.fileNameOrigin = DEFAULT_NOT_EMPTY_VALUE;
                boxSoftware.pathLocation = DEFAULT_NOT_EMPTY_VALUE;
                boxSoftware.checksum = DEFAULT_NOT_EMPTY_VALUE;
                boxSoftware.validate();
                flash.error = message(code: 'validation.error');
                boxSoftware.errors.rejectValue('versionSoftware', message(code:'invalid.integer.value', args:[message(code:"application.version")]));
            }
        } catch (Exception ex){
            //General exception to handle unexpected grails exceptions
            flash.error = ex.getMessage();
            log.error(ex);
        }
        render(view: "register", model: [boxSoftware: boxSoftware]);
    }
	
	private def setFileInformationInBoxSoftware(def file, BoxSoftware boxSoftware ) {
		def fileNameOrigin = file.getOriginalFilename()
		def isTarGz = FileHelper.isFileTypeTarGz(fileNameOrigin)
		def fileName
		def path
		
		if(isTarGz){
			boxSoftware.checksum = DigestHelper.generateMD5Checksum(file.getBytes())
			//grailsAttributes.getApplicationContext().getResource("/data/jak.csv")
			def storagePath = servletContext.getRealPath(grailsApplication.config.basePathMyBoxSoftwareUpdate)
			def extention = FileHelper.getFileExtension(fileNameOrigin)
			def fileNameWithout = FileHelper.getFileNameWithoutExtension(fileNameOrigin)
			fileName = fileNameWithout + "_" + params.versionSoftwareLabel + "_." + extention
			
			boxSoftware.fileNameOrigin =  fileNameOrigin
			boxSoftware.pathLocation = storagePath //path
			boxSoftware.fileName = fileName
			path = storagePath + "/" + fileName
		}else{
			//set dummy values on properties so they wont appear as errors when validate is called
			if(!fileNameOrigin.toString().isEmpty()){
				boxSoftware.fileName=fileNameOrigin
			}else{
				//since its empty, dont validate if is tar
				isTarGz = true
			}
			boxSoftware.fileNameOrigin = "-"
			boxSoftware.pathLocation = "-"
			boxSoftware.checksum = "-"
		}
		
		return boxSoftware
	} // END setFileInformationInBoxSoftware
	
	private String getPathFromFile(String fileNameOrigin, def versionSoftwareVersion) {
		//def storagePath = servletContext.getRealPath(grailsApplication.config.basePathMyBoxSoftwareUpdate)
		def storagePath = grailsApplication.config.basePathMyBoxSoftwareUpdate
		def extention = FileHelper.getFileExtension(fileNameOrigin)
		def fileNameWithout = FileHelper.getFileNameWithoutExtension(fileNameOrigin)
		def fileName = fileNameWithout + "_" + versionSoftwareVersion + "_." + extention
		def path = storagePath + "/" + fileName
		return path
	}
	
    /**
     * This method gets the id and tries to find an available software version depending of the current version and renders
     * the page with the software version returned for confirmation of the upgrade
     * 
     * @param id
     *      Id of the Box to upgrade
     */
    def updateSoftwareVersion(Long id) {
        final Box box = Box.findById(id);
        final int versionId = (box.softwareVersion)? box.softwareVersion.versionSoftware : 0;
        final Affiliate affiliate = SerialNumberGeneratorHelper.getPartnerFromSerial(box.serial);
        def listSoftwareVersion = BoxSoftware.getAllByVersionSoftwareGreaterThanEnableAndSupported(versionId,
                box.imageType, affiliate).list([max: 1, sort: BoxSoftware.SimpleFields.VERSION_SOFTWARE.fieldName(),
                    order: Constants.SORT_DESC]);
        final BoxSoftware boxSoftwareOld = box.softwareVersion;
        if(listSoftwareVersion.size() > 0) {
            final BoxSoftware boxSoftware = listSoftwareVersion.get(0);
            final boolean isUpdateAvalaible = (boxSoftware.id > box.softwareVersion?.id)? true : false;
            [boxSoftware:boxSoftware, boxSoftwareOld:boxSoftwareOld, box:box, isUpdateAvalaible:isUpdateAvalaible]
        } else {
            render(view:VIEW_NO_SOFTWARE_TO_UPGRADE);
        }
    }
	
	/**
	 * This method will try to send the software upgrade request to the ExXtractor and will render a page with the 
	 * result of the request
	 */
    def applyUpdateSoftwareVersion() {
        final BoxSoftware boxSoftware = BoxSoftware.findById(params.boxSoftwareId);
        final Box box = Box.findById(params.boxId);
        String errorCode = "";
        def json = "";
        try {
            def listRequest = softwareUpgradeService.createRequestUpdate(boxSoftware, box);
            json = restClientService.updateSoftwareVersion(listRequest);
            if (json) { //service is down or something went wrong
                def statusUpdatingBox = BoxStatus.findByDescription(BoxStatusEnum.UPDATING.value);
                box.status = statusUpdatingBox;
                box.save();
                softwareUpdateControlService.addUpgradeControl(box.serial, json, errorCode, boxSoftware.versionSoftwareLabel,
                    SoftwareUpdateControlService.STATUS_IN_PROGRESS);
            } else {
                errorCode = MessageErrorService.ERROR_CODE_NO_CONNECTION;
                softwareUpdateControlService.addUpgradeControl(box.serial, json, errorCode, boxSoftware.versionSoftwareLabel,
                    SoftwareUpdateControlService.STATUS_FAILED);
            }
        } catch(Exception e) {
            // Catching general exception because of unexpected grails errors
            errorCode = MessageErrorService.ERROR_CODE_NO_CONNECTION;
            softwareUpdateControlService.addUpgradeControl(box.serial, json, errorCode, boxSoftware.versionSoftwareLabel,
                SoftwareUpdateControlService.STATUS_FAILED);
        }
        redirect(uri: '/boxSoftware/softwareUpdateStatus?serial='+box.serial);
    }

    /**
     * Renders a page with the current status of a software upgrade process for a single box if exists
     */
    def softwareUpdateStatus() {
        if (StringUtils.isNotBlank(params.serial)) {
            def controlDetail = softwareUpdateControlService.getUpgradeDetail(params.serial);
            render(view:"softwareUpdateStatus",
                model: [serial: params.serial, controlDetail:controlDetail,
                    messageErrorDescriptionHash:messageErrorService.getMessageErrorDescription()]);
        } else {
            redirect(control:"box", action: Constants.VIEW_LISTALL);
        }
    }

	private def theTransactionWasSucessful(json, serial) {
		
		for(def record :json.updateSoftwareVersionStatus){		
			if(serial == record.serialNumber) {
				//record.successful
				return true;
			}			
		}			
		return false
	}
	
    def saveEdit(Long id) {
        BoxSoftware boxSoft = BoxSoftware.read(id);
        if (boxSoft) {
            if (boxSoft.isSupported) {
                if (boxSoftwareService.isValidVersionLabelToSave(boxSoft, 
                        params.versionSoftwareLabel, params.associatedArchitectures)) {
                    boxSoft.lastUpdatedBy = userSessionService.getCurrentUser();
                    boxSoft.properties = params;
                    def file = request.getFile(Constants.PARAM_FILE);
                    def fileNameOrigin = file.getOriginalFilename();
                    if (fileNameOrigin.isEmpty()){ // if no new file is going to be uploaded just save the other fields
                        if (boxSoft.validate()) { // validate fields
                            this.saveSoftwareVersionAndFile(boxSoft, "", file, false); //NO UPDATE
                        } else {
                            flash.error = message(code: 'default.error.problem');
                        }
                    } else { //we are uploading a new file
                        if(file.isEmpty()) { //if file selected is empty
                            flash.error = message(code: 'default.error.problem');
                            boxSoft.errors.rejectValue(Constants.FILE_NAME, 'default.error.box.software.emptyFile');
                        } else {
                            final boolean isTarGz = FileHelper.isFileTypeTarGz(fileNameOrigin);
                            if(isTarGz) {
                                //only do this when is tar, if not we dont want to clear the info
                                boxSoft = setFileInformationInBoxSoftware( file, boxSoft);
                            }
                            if(boxSoft.validate()) { // validate fields
                                if(isTarGz) {
                                    final String path= getPathFromFile(fileNameOrigin, boxSoft.versionSoftwareLabel);
                                    this.saveSoftwareVersionAndFile(boxSoft, path, file, true);
                                } else {
                                    flash.error = message(code: 'default.error.problem');
                                    boxSoft.errors.rejectValue(Constants.FILE_NAME, 'default.error.box.software.wrongExtension');
                                }
                            } else {
                                flash.error = message(code: 'default.error.problem');
                                if(!isTarGz) { //need to put it here to make sure we add all possible validation errors
                                    boxSoft.errors.rejectValue(Constants.FILE_NAME, 'default.error.box.software.wrongExtension');
                                }
                            }
                        }
                    }
                } else {
                    flash.error = message(code: 'software.version.label.already.in.use');
                }
                if (flash.error){
                    render(view: Constants.VIEW_EDIT, model: [boxSoftware: boxSoft]);
                } else {
                    redirect(action: Constants.VIEW_LISTALL);
                }
            } else {
                flash.error = message(code: 'default.box.software.cannotEditUnsupportedVersion');
                redirect(action: Constants.VIEW_LISTALL);
            }
        } else {
            flash.error = message(code: 'default.not.found.message');
            redirect(action: Constants.VIEW_LISTALL);
        }
    }
	
	/** save the software version into the DB and the File using transaction 
	 * 
	 * @param boxSoft
	 * @param fullPath		full path location of the file. example: /home/user/file.tar.gz
	 * @param file  		multiparts File
	 * @return
	 */
	private def saveSoftwareVersionAndFile(BoxSoftware boxSoft, String fullPath, def file, boolean transfer) {
		BoxSoftware.withTransaction { status ->
			try {
				boxSoft.save()
				if (transfer){
							
					def storagePath = grailsApplication.config.basePathMyBoxSoftwareUpdate
					File f = new File(storagePath);
					if (!f.exists()){
						f.mkdirs();
					}
					file.transferTo( new File(fullPath) ) //new File(path + fileNameOrigin)
			    }
				flash.success = message(code: 'default.message.success')
			} catch(Exception dbEx)	{
				dbEx.printStackTrace()
				status.setRollbackOnly()
				flash.error = (dbEx.getMessage())?dbEx.getMessage():message(code: 'default.error.box.software.savingTheFile')
			}
		}
	} // END saveSoftwareVersionAndFile	

    /**
     * Returns if a box exists and if it's connected
     * 
     * @param serialNumber
     *              Serial number of the box to verifies
     * @return JSON with the status of the box
     */
    def asReconnected(String serialNumber) {
        def controlList = softwareUpdateControlService.getUpgradeDetail(serialNumber);
        if (controlList) {
            if (controlList.finishedAt) {
                final DateFormat format = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
                controlList.finishedAtAsString = format.format(controlList.finishedAt);
            }
            render controlList as JSON;
        } else {
            render(text: [status:0 ] as JSON, contentType:'text/json')
        }
    }
}