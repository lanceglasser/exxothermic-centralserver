/**
 * HomeController.groovy
 */
package mycentralserver;

import java.awt.print.Printable;
import java.util.Date;
import java.util.logging.Logger;

import org.springframework.web.servlet.FlashMap

import grails.plugins.springsecurity.Secured
import mycentralserver.generaldomains.Country
import mycentralserver.generaldomains.State
import mycentralserver.generaldomains.CompanyType
import mycentralserver.app.Affiliate;
import mycentralserver.company.Company
import mycentralserver.company.CompanyIntegrator
import mycentralserver.company.CompanyCatalogSubType
import mycentralserver.company.CompanyCatalogType
import grails.converters.JSON
import mycentralserver.user.User
import mycentralserver.user.UserDashboard;
import mycentralserver.user.UserRole
import mycentralserver.user.Role
import mycentralserver.utils.RoleEnum
import mycentralserver.utils.CompanyTypeEnum
import mycentralserver.utils.Constants

/**
 * Controller that handles some UI request
 * 
 * @author Cecropia Solutions
 */
class HomeController extends BaseControllerMyCentralServerController {

	/* Inject required services */
	def mailService
	def userService
	def affiliateService

	/**
	 * Renders the Home Page of the User
	 * 
	 * @return
	 */
	def index() {
		User user = this.getCurrentUser();

		if(userService.isPasswordExpired(user)) {
			this.doLogoutManual();
			flash.message = message(code: 'springSecurity.errors.login.passwordExpired');
			redirect( controller:"login",action:"changePassword");
		}
	} // End of index method

	def register() {
		redirect(controller:'home', action: 'index');
	}

	def registerAccount()
	{
		if ( params.size() > 2) { //if the only values are action and controller, then we are not coming from the form
			def user = new User(params)
			def company=new Company(params)
			def integrator = new CompanyIntegrator(params)
			try{

				def typesCompany = getCompanyTypesOfAffiliate();
						
				//User
				CompanyType integratorCompany = CompanyType.findByName(CompanyTypeEnum.INTEGRATOR.value)

				Role role = (params.typeOfCompany.id == Long.toString(integratorCompany.id)) ? Role.findByAuthority(RoleEnum.INTEGRATOR.value) : Role.findByAuthority(RoleEnum.OWNER.value)

				//create first the user
				//basic information

				def anonymosUser=User.findById(1)
				user.createdBy =anonymosUser
				user.lastUpdatedBy=anonymosUser
				user.enabled=true
				user.passwordExpirationDate = userService.getPasswordExpirationDateForExistAccount()

				integrator = (params.typeOfCompany.id == Long.toString(integratorCompany.id)) ? integrator : null
				///Company

				company.createdBy=user
				company.owner=user
				if(params.otherType == null || params.otherType.isEmpty() ||
						 (company.type.code != Constants.ESTABLISHMENT_TYPE_OTHER && company.type.code != Constants.ESTABLISHMENT_TYPE_WAITING_ROOM))
				{
					company.otherType=null
				}
				if(!company.country.hasStates()){
					company.state = null
				}

				if(integrator != null){
					integrator.approved=true
					integrator.company = company
				}

				boolean userValidation = user.validate()
				boolean companyValidation = company.validate()
				boolean integratorValidation = true
				if(integrator != null){
					integratorValidation =integrator.validate()
				}

				if (userValidation && companyValidation && (integrator == null || integratorValidation ))
				{
					if (params.password != params.confirmPassword) {
						flash.error=message(code: 'default.home.register.problem')
						user.errors.reject(
								'user.password.doesnotmatch',
								['password', 'class User'] as Object[],
								'[Property [{0}] of class [{1}] does not match confirmation]')

					}else{

						User.withTransaction { status ->
							try{
								user.save()
								UserRole.create user,role
								company.save()

								if(integrator != null){
									integrator.save()
								}

								try{
									//Send Email
									mailService.sendMail {
										from	affiliateService.getCurrentAffiliateConcatEmail(session.affiliate)
										to  	user.email
										subject message(code: 'default.home.register.success')
										html 	g.render(template:"registerTemplate", model:[user:user, password:params.password])
									}

									//Send Email to Support
									mailService.sendMail {
										from	affiliateService.getCurrentAffiliateConcatEmail(session.affiliate)
										to 		grailsApplication.config.grails.mail.default.from
										subject message(code: 'default.support.register.success')
										html 	g.render(template:"supportRegisterTemplate", model:[user:user, company:company, integrator:integrator])
									}
								}catch(Exception e){
									//if user is able to register, not getting an email should not be a problems
									log.error( "Could not send email", e)
								}


								flash.success=message(code: 'default.home.register.success')

							}catch(Exception dbEx){
								dbEx.printStackTrace()
								status.setRollbackOnly()
								flash.error=dbEx.getMessage()
							}
						}
					}
				}else{
					flash.error=message(code: 'default.home.register.problem')
				}
			}catch(Exception e){
				e.printStackTrace()
				flash.error=e.getMessage()
			}

			if(flash.error != null){
				def countries=Country.list()
				def states=State.findAllByCountry(company.country)
				def typesCompany = getCompanyTypesOfAffiliate();
				def typeofAccount= CompanyType.findAll()
				 
				render(view:"register",model: [user:user,company:company,integrator:integrator,countries:countries,typesCompany:typesCompany,typeofAccount:typeofAccount, states:states])
				return
			}else{
				redirect (controller:"login",action:"auth")
				return
			}
		}else{
			redirect (controller:"login",action:"auth")
		}
	}
	
	/**
	 * Renders a partial view with some help information or container
	 * @return
	 */
	def showHelp() {
		String template = params.main? "main":params.template; 
		render(template:"helps/" + template, model:[cat:(params.cat? params.cat:'start'),template:params.template]);
	} // End of showHelp method

} // End of class
