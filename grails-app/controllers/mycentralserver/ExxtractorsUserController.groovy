/**
 * ExxtractorsUserController.groovy
 */
package mycentralserver

import grails.converters.JSON;
import mycentralserver.box.BoxUsers;
import mycentralserver.company.CompanyLocation;
import mycentralserver.user.User;
import mycentralserver.utils.Constants;
import mycentralserver.utils.Utils;

/**
 * Controller class that handles the request related with the ExXtractor Users
 * 
 * @author Cecropia Solution
 *
 */
class ExxtractorsUserController  extends BaseControllerMyCentralServerController {
	
	/* Injection of required services */
	def boxUserService;
	
	/**
	 * Renders the page that allow the creation of a new ExXtractor User,
	 * if receives a Location Id must verify that the user has rights over the Location
	 *
	 * @return
	 */
	def create() {
		long locationId = (params.id != null )? Long.parseLong( params.id ):0;
		def locations = userSessionService.getPermitedLocations();
		def ids = locations*.id;
		boolean allowToSeeContent = (locationId == 0 || ids.contains(new Long(locationId)));
		if(!allowToSeeContent){
			flash.error = message(code: 'default.not.found.message');
			redirect(controller: "location", action: Constants.VIEW_LISTALL);
		} else {
			if(locationId != 0) {
				// Get the Location object because needs to check the maximum number of Users 
				CompanyLocation location = CompanyLocation.read(locationId);
				if(location.boxUsers.size() >= Constants.MAX_BOX_USERS_FOR_LOCATION) {
					flash.warn = message(code: 'max.number.of.box.users.error', args:[Constants.MAX_BOX_USERS_FOR_LOCATION]);
					redirect(controller: "location", action: "show", params: [id: locationId, tab: 'exx-users'] );
				} else {
					[entity: new BoxUsers(), locationId: locationId, locations: locations]
				}
			}
			[entity: new BoxUsers(), locationId: locationId, locations: locations]
		}
	} // End of create method
	
	/**
	 * Ajax method that receives the information for a new User or to edit,
	 * creates or edit the User at DB only
	 *
	 * @return	Json with result
	 */
	def createUser(){
		HashMap<String, Object> jsonMap = boxUserService.createOrUpdateUser(params, flash, session);
		render jsonMap as JSON;		
	} // End of createUser method
		
	/**
	 * Renders the page to allow the edition of a ExXtractor User
	 *
	 * @param id	Id of the ExXtractor User to be edited.
	 *
	 * @return
	 */
	def edit(long id) {
		BoxUsers boxUser = BoxUsers.read(id);
		
		if (!boxUser || !boxUserService.allowToExecuteAction(boxUser.location) ) {
			flash.error = message(code: 'default.not.found.message');
			redirect(controller: "location", action: Constants.VIEW_LISTALL);
		} else {
			[entity: boxUser, locations: userSessionService.getPermitedLocations(), locationId: boxUser.location.id]
		}
	} // End of edit method
			
	/**
	 * This method will delete the ExXtractor User from DB and will try
	 * to send the users information to the Boxes
	 * 
	 * @param id	Id of the ExXtractor User to be deleted
	 * @return
	 */
	def delete(long id){
		int result = boxUserService.deleteBoxUser(id, params, flash, session);
		switch(result){
			case 1:
				redirect(controller: "location", action: Constants.VIEW_LISTALL);
				break;
			case 4:
				redirect(controller: "location", action: "show", params: [id: params.locationId.toLong(), a: 'sync', tab: 'exx-users'] )
				break;
			case 5:
				redirect(controller: "location", action: "show", params: [id: params.locationId.toLong(), tab: 'exx-users'] );
				break;
			default:
				redirect(controller: "location", action: Constants.VIEW_LISTALL);;
		}
	} // End of delete method
	
} // End of class