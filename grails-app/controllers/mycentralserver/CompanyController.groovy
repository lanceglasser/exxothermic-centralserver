package mycentralserver;

import mycentralserver.generaldomains.Country
import mycentralserver.generaldomains.State
import mycentralserver.user.User
import mycentralserver.user.Role
import mycentralserver.user.UserRole
import mycentralserver.utils.Constants
import mycentralserver.utils.CompanyTypeEnum
import mycentralserver.utils.RoleEnum
import mycentralserver.company.CompanyCatalogSubType
import mycentralserver.company.CompanyCatalogType
import mycentralserver.company.Company
import mycentralserver.company.CompanyLocation
import mycentralserver.generaldomains.CompanyType
import mycentralserver.company.CompanyIntegrator
import mycentralserver.utils.Utils

import grails.converters.JSON
import grails.gorm.DetachedCriteria
import grails.orm.HibernateCriteriaBuilder
import grails.plugins.springsecurity.Secured

import org.apache.commons.lang.StringUtils
import org.hibernate.criterion.CriteriaSpecification
import org.hibernate.Criteria
import org.springframework.web.servlet.FlashMap

import java.io.File;
import java.util.Date

/**
 * Controller that recives the request related with the Companies
 * 
 * @author Cecropia Solutions
 */
class CompanyController extends BaseControllerMyCentralServerController{

    /* Inject required services */
    def companyService;
    def rackspaceService;
    def pocService;
    def userSessionService;

    /**
     * Renders the dashboard view of a Company
     */
    def dashboard(long id) {
        try {
            Company company = companyService.renderDashboardPage(id, flash);
            if(company) {
                [company: company]
            } else {
                redirect(action: Constants.VIEW_INDEX,  params: [])
            }
        } catch(Exception e){
            log.error("Error rendering the company dashboard", e);
        }
    }

    /**
     * Renders the page for the creation of a new Company
     */
    def create() {
        def countries = Country.list();
        def states = State.findAllByCountry(countries[0]);
        def typesCompany = getCompanyTypesOfAffiliate();
        Company company = new Company();
        company.state = states[0];
        [company:company, countries:countries, typesCompany:typesCompany, states:states, comingFromWizard:false]
    }

    /**
     * Receives the information for a new Company; validates the data, creates the object and renders the result of the
     * operation
     */
    def save() {
        Company company = new Company(params);
        def deleteWhenFails = [];
        try {
            final User currentUser = userSessionService.getCurrentUser();
            company.owner = currentUser;
            company.createdBy = currentUser;
            company.typeOfCompany = CompanyType.findByName(CompanyTypeEnum.OWNER.value());
            if (params.otherType == null || params.otherType.isEmpty() ||
                    (company.type.code != Constants.ESTABLISHMENT_TYPE_OTHER 
                        && company.type.code != Constants.ESTABLISHMENT_TYPE_WAITING_ROOM)) {
                company.otherType = null;
            }
            // If the User is an Integrator (Only HelpDesk and Integrator can create), then self assign to new company
            // Don't validate if it's Integrator because some HelpDesk are also Integrators and generates issues
            if (!userSessionService.userHasRole(RoleEnum.HELP_DESK.value())) {
                def integrators = currentUser.companies.findAll {
                    it.typeOfCompany.name == CompanyTypeEnum.INTEGRATOR.value() }.collect();
                if (integrators.size() > 0) {
                    company.companyWideIntegrator= CompanyIntegrator.findByCompany(integrators.get(0));
                }
            }
            if (!company.country || !company.country.hasStates()) {
                company.state = null
            }
            if (company.validate()) {
                try {
                    //Check and upload the logo Image
                    final String url = uploadImageFile(message(code:'company.logo'), Constants.PARAM_BG_LOGOUT_URL,
                                        Constants.COMPANY_LOGO_WIDTH, Constants.COMPANY_LOGO_HEIGHT);
                    if (url != null) {
                        company.logoUrl = url;
                        deleteWhenFails.add(url);
                    }
                    company.save();
                    if (!userSessionService.userHasRole(RoleEnum.OWNER.value())) {
                        final Role role = Role.findByAuthority(RoleEnum.OWNER.value());
                        UserRole.create currentUser, role;
                    }
                    flash.success=message(code: 'default.message.success');
                } catch(Exception dbEx)	{
                    log.error(dbEx);
                    if(flash.error == null || flash.error.trim() == "") {
                        flash.error = dbEx.getMessage();
                    }
                }
            } else {
                flash.error = message(code: 'company.error.saving');
            }
        } catch(Exception ex) {
            log.error(ex);
            if (flash.error == null || flash.error.trim() == "") {
                flash.error = ex.getMessage();
                if (flash.error == null) {
                    flash.error = message(code: 'default.error.problem');
                }
            }
        }
        if (params.doNotRedirect) {
            if (flash.error != null) {
                [company , false]
            } else {
                [company, true]
            }
        } else {
            if (flash.error == null) {
                flash.success = message(code: 'default.message.success');
                redirect(action: Constants.VIEW_INDEX);
            } else {
                def countries = Country.list();
                def states = State.findAllByCountry(company.country);
                def typesCompany = getCompanyTypesOfAffiliate();
                def typeofAccount = CompanyType.findAll();
                if (company.state != null) {
                    company.state.country = company.country;
                }
                rackspaceService.deleteMultipleFiles(deleteWhenFails);
                def model = [company:company, countries:countries, typeofAccount:typeofAccount, typesCompany:typesCompany,
                    states:states, comingFromWizard:false];
                render (view: Constants.VIEW_CREATE,model:model);
            }
        }
    }

	def index()
	{		
		try
		{			
			def ( companies,companiesAssigned)  = getPermitedCompanies()				
			if(companies.size()==0) 
			{
				redirect(action: Constants.VIEW_NOT_COMPANY)
				return
			} else {		
				def max = Math.min(params.max ? params.int(Constants.PARAM_MAX) : 100, 100)
				params.max = max				
				[companies: Utils.paginateList( companies, params), companiesAssigned:companiesAssigned, companiesCount:companies.size()]
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace()
			log.error("Exception:" + ex);
			flash.error=message(code: 'default.error.unableDisplayCompanies')
		}
		
	}
	
	def getPermitedCompanies(){
		
		def companies = new ArrayList()
		def companiesAssigned = new ArrayList()
		 
		try{
			 
			def user = getCurrentUser() 
			def authorities =  user.authorities						
			
			if (isUserRoleHelpDesk() || isUserRoleAdmin()){
				companies = Company.findAll()
			}else{
	
				def ownerStaffRole = Role.findByAuthority(RoleEnum.OWNER_STAFF.value)
				def integratorStaffRole = Role.findByAuthority(RoleEnum.INTEGRATOR_STAFF.value)
					
				def isStaff =authorities.contains(ownerStaffRole) ||authorities.contains(integratorStaffRole)
				def temp = null
				 
				 if (isStaff){
					 temp = Company.getEnabledCompanies(user)
					 companies = temp.listDistinct()
				 }else{
					 temp = Company.getAllCompanies(getCurrentUser())
					 companies = temp.listDistinct()
				 }	
				 				 														
				def integrators = temp.typeIntegrator.listDistinct()
				for(i in integrators){
						CompanyIntegrator integrator = CompanyIntegrator.findByCompany(i)
						
						def assigned = integrator.assignedCompanies.toList()
						for(a in assigned){
							a.locations = CompanyLocation.getAllLocations(getCurrentUser(), a).listDistinct()
						}
						companiesAssigned.addAll(assigned)
				}
				 
				//remove from assigned those where user is the owner
				companiesAssigned = companiesAssigned.findAll { it.owner != getCurrentUser() }.collect()
			}
		}catch(Exception e){
		    log.info(e.getMessage());
			e.printStackTrace()
		}
		return [companies , companiesAssigned.sort{it.name}]
	}

    /**
     * Renders the edit page of a the Company reference it by the id parameter
     * @param id
     *          Id of the Company to edit
     */
    def edit(long id) {
        Company company = Company.get(id);
        def (companies,companiesAssigned)  = getPermitedCompanies();
        if (company && (companies.any { it == company } || companiesAssigned.any { it == company }) ) {
            def countries = Country.list();
            def states = new ArrayList();
            // If there is a associated State; find the list of States for the State's Country
            if (company.state != null) {
                states = State.findAllByCountry(company.state.country);
                company.country = company.state.country;
            }
            def typesCompany = getCompanyTypesOfAffiliate();
            def integrators = CompanyIntegrator.findAll();
            [company:company,countries:countries,typesCompany:typesCompany, states:states, integrators:integrators]
        } else {
            flash.error = message(code: 'default.not.found.message');
            redirect(action: Constants.VIEW_INDEX,  params: []);
        }
    }

    /**
     * This method update the information of a Company
     * 	
     * @param id
     *          Id of Company to be updated
     * @param version
     *          Version in order to verified previous update
     */
    def update(long id, Long version) {
        Company company = null;
        def imagesUrlsToDelete = [];
        def deleteWhenFails = [];
        Map<String, String> originalImages = new HashMap<String, String>();
        try {
            company = Company.read(id);
            final boolean wasPocAccountCreated = company.pocAccountCreated;
            originalImages.put("logoUrl", company.logoUrl);
            if (company.version > version) {
                flash.error=message(code: 'default.company.edit.version.error');
                company.errors.rejectValue("version", "default.company.edit.version.error");
            }else{
                company.properties = params;
                if (params.otherType == null || params.otherType.isEmpty() ||
                        (company.type.code != Constants.ESTABLISHMENT_TYPE_OTHER 
                            && company.type.code != Constants.ESTABLISHMENT_TYPE_WAITING_ROOM)) {
                    company.otherType = null;
                }
                if (!company.country.hasStates()) {
                    company.state = null;
                }
                // Set the CompanyIntegrator
                company.companyWideIntegrator = CompanyIntegrator.read(params.companyIntegrator);
                if (company.validate()) {
                    if (pocService.handlePOCChange(wasPocAccountCreated, company, g)) {
                        // Check and upload the logo Image
                        final String url = uploadImageFile(message(code:'company.logo'), Constants.PARAM_BG_LOGOUT_URL,
                                                    Constants.COMPANY_LOGO_WIDTH, Constants.COMPANY_LOGO_HEIGHT);
                        if (url != null) {
                            imagesUrlsToDelete.add(company.logoUrl);
                            company.logoUrl = url;
                            deleteWhenFails.add(url);
                        }
                        company.save();
                        rackspaceService.deleteMultipleFiles(imagesUrlsToDelete);
                        flash.success=message(code: 'default.message.success');
                    } else {
                        company.pocAccountCreated = false;
                        flash.error = message(code: 'default.error.problem');
                    }
                } else {
                    flash.error = message(code: 'default.error.problem');
                }
            }
        } catch(Exception ex) {
            if (StringUtils.isBlank(flash.error)) {
                log.error("Error updating the information of a Company", ex);
                flash.error = message(code: 'default.error.unableDisplayCompanies');
            }
        }
        if (flash.error != null) {
            def countries = Country.list();
            company.logoUrl = originalImages.get("logoUrl");
            rackspaceService.deleteMultipleFiles(deleteWhenFails);
            //Get by default state and cities, maybe the default can will be in a config file
            def states=State.findAllByCountry(company.country)
            def typesCompany = getCompanyTypesOfAffiliate();
            def typeofAccount= CompanyType.findAll();
            render(view:Constants.VIEW_EDIT, 
                model:[company:company,countries:countries,typeofAccount:typeofAccount,typesCompany:typesCompany, states:states])
        } else {
            redirect(action: Constants.VIEW_INDEX, id:id, params: []);
        }
    }

    /**
     * Renders the show page with all the information of the Company reference it by the Id 
     * @param id
     *          Id of the page to display the information
     */
    def show(long id) {
        final Company company = Company.get(id);
        def (companies,companiesAssigned)  = getPermitedCompanies();
        if (company && (companies.any { it == company } || companiesAssigned.any { it == company })) {
            [company:company]
        } else {
            flash.error = message(code: 'default.not.found.message');
            redirect(action: Constants.VIEW_INDEX,  params: []);
        }
    }

    /**
     * Only render the not company page
     */
    def notcompany() {
    }

	/**
	 * This method will return the list of Companies depending of the Role
	 * of the current User
	 * @return
	 */
	def getCompaniesListForAssignIntegrator(){
		def user = getCurrentUser()
		def companies = null
		
		def helpDeskRole = Role.findByAuthority(RoleEnum.HELP_DESK.value)
		def isHelpDesk= user.authorities?.contains(helpDeskRole)
		def type = CompanyType.findByName(CompanyTypeEnum.OWNER.value)
		
		if (isHelpDesk){
			companies = Company.findAllByTypeOfCompany(type)
		}else{
			
			companies = Company.getAllCompaniesByType(user, type).listDistinct()
		}
		return companies;
	}
	
    /**
     * This method will renders the page for the association of a Company Integrator to the regular Companies
     */
    def assignIntegrator() {
        def integrators = CompanyIntegrator.getEnabledAndApprovedCompanies().listDistinct();
        if(integrators.size() > 0 ) {
            def companies = getCompaniesListForAssignIntegrator();
            if(companies.size() > 0) {
                // If there are Integrator and Companies; render the page
                [companies:companies, integrators:integrators]
            } else { // If there are not Companies to associate
                flash.error = message(code: 'default.no.companies.assign');
                redirect(action:Constants.VIEW_INDEX, controller: Constants.CONTROLLER_HOME);
            }
        } else {// If there are not Integrators
            flash.error=message(code: 'default.cant.assign');
            redirect(action:Constants.VIEW_INDEX, controller: Constants.CONTROLLER_HOME);
        }
    }

	/**
	 * This method will return a list of all the Company Ids
	 * that the received Integrator has associated
	 * @return
	 */
	def showAssociatedCompanies(){
		try{
			def integrator = CompanyIntegrator.findById(params.integratorId);
			def ids = integrator.assignedCompanies*.id;
			
			String selectedCompanies = "";
			for(id in ids){
				selectedCompanies += (selectedCompanies.equals(""))? id:"," + id;
			}
			
			def companies = getCompaniesListForAssignIntegrator();
				
			render (template:"companiesOfIntegrator", model:[companies: companies, selectedCompanies: selectedCompanies])
			
		}catch(Exception e){
			logger.error("Error getting associated Companies of a Integrator", e);
		}
	}
	
	def assign(){
		
		def helpDeskRole = Role.findByAuthority(RoleEnum.HELP_DESK.value)
		def ownerStaffRole = Role.findByAuthority(RoleEnum.OWNER_STAFF.value)
		
		try{
			def integrator = CompanyIntegrator.read(params.integrator.toLong());
			def associatedCompanies = integrator.assignedCompanies;
			def companies = params.list(Constants.PARAM_COMPANIES); //This is the List of Companies that must be associated
			
			Company.withTransaction { status ->
				try {
					//Remove
					associatedCompanies.each { company ->
						if(!companies.any { it == (""+company.id) }){
							//integrator.removeFromAssignedCompanies(company);
							company.companyWideIntegrator = null;
							if(company.save()){
							} else {
								company.errors.each {
									println it
								}
							}
							
						}
					}
					
					//Add new ones
					for (c in companies){
						long cId = Long.parseLong("" + c);
						if(!associatedCompanies.any { it.id == (cId) }){
							Company company = Company.get(cId);
							if(company != null){
								company.companyWideIntegrator = integrator;								
								integrator.assignedCompanies.add(company);
								if(company.save()){
								} else {
									company.errors.each {
										println it
									}
								}
							}
						}
					}
					
					if(!integrator.save()){
					}
					status.flush();
				}catch(Exception dbEx){
					dbEx.printStackTrace()
					status.setRollbackOnly();
					flash.error=dbEx.getMessage();
				}
			}
					
		}catch(Exception e){
			flash.error=e.getMessage();
			logger.error("Error associating companies to Integrator", e);
		}	
			
		def companies = getCompaniesListForAssignIntegrator();
		def integrators = CompanyIntegrator.getEnabledAndApprovedCompanies().listDistinct();
		render (view:Constants.VIEW_ASSIGN_INTEGRATOR,model:[companies:companies, integrators:integrators, integratorId: params.integrator.toLong()]);
		return;
	}
	
	/**
	 * Renders the page that allows to assign an Owner to a Company
	 */
	def ownedCompanies() {
		try {
			def companies = Company.getAllOwnedCompanies().listDistinct();
			def roles = Role.findAllByAuthorityInList([RoleEnum.HELP_DESK.value, RoleEnum.OWNER.value, 
				RoleEnum.INTEGRATOR.value]);
			def owners = User.listAllOwners(roles).list();
			if(companies.size()==0) {
				redirect(action: Constants.VIEW_NOT_COMPANY);
			} else {
				[companies: companies, owners: owners]
			}
		} catch(Exception ex) {
			ex.printStackTrace()
			log.error("Exception:" + ex);
			flash.error=message(code: 'default.error.unableDisplayCompanies')
		}
	}
	
	/**
	 * Receives an ajax request with the information of a Company and the new owner
	 * to assign
	 * @return Ajax Response with the result of the operation: {error: false, msg: ''}
	 */
	def changeCompanyOwner() {
		HashMap jsonMap = new HashMap()
		try {
			int companyId = params[Constants.PARAM_COMPANY_ID]? params[Constants.PARAM_COMPANY_ID].toInteger():0;
			int ownerId = params[Constants.PARAM_OWNER_ID]? params[Constants.PARAM_OWNER_ID].toInteger():0;
			if( companyId == 0 || ownerId == 0 ) {
				jsonMap.msg = message(code:'missing.required.parameters');
				jsonMap.error =  true;
			} else {
				Company company = Company.get(companyId);
				User ownerUser = User.read(ownerId);
				if( company && ownerUser ) {
					company.owner = ownerUser;
					if(company.save()) {
						jsonMap.owner = ownerUser.firstNameAndLastName;
						jsonMap.msg = message(code: 'owner.change.success');
						jsonMap.error =  false;
					} else {
						jsonMap.msg = message(code: 'owner.change.fails');
						jsonMap.error =  true;
					}					
				} else {
					jsonMap.msg = message(code: 'invalid.required.parameters');
					jsonMap.error =  true;
				}
			}
		} catch(NumberFormatException nfe) {
			jsonMap.error =  true;
			jsonMap.msg = message(code:'invalid.required.parameters');
		} catch(Exception e) {
			// General exception is been catch it to avoid unexpected Grails errors during runtime 
			log.error("Error changing the Owner of a Company: " + params , e);
			jsonMap.error =  true;
			jsonMap.msg = "Unexpected error: " + e.getMessage();
		}
		render jsonMap as JSON;
	}
}
