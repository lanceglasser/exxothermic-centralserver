package mycentralserver

import mycentralserver.generaldomains.Country
import mycentralserver.generaldomains.State
import mycentralserver.user.User
import mycentralserver.user.Role
import mycentralserver.user.UserRole
import mycentralserver.company.CompanyCatalogSubType
import mycentralserver.company.CompanyCatalogType
import mycentralserver.company.Company
import mycentralserver.generaldomains.CompanyType
import mycentralserver.company.CompanyIntegrator
import org.springframework.web.servlet.FlashMap
import grails.plugins.springsecurity.Secured
import grails.gorm.DetachedCriteria
import mycentralserver.utils.CompanyTypeEnum
import mycentralserver.utils.Constants
import mycentralserver.utils.RoleEnum

class CompanyIntegratorController extends BaseControllerMyCentralServerController {

    def rackspaceService;

    /**
     * Will render the list of Integrator Companies only for Admin and HelpDesk
     */
    def index() {
        def companiesIntegrators = null;
        params.sort = Constants.COMPANY_NAME;
        params.order= Constants.SORT_ASC;
        companiesIntegrators =  CompanyIntegrator.findAll(params);
        if(companiesIntegrators.size() > 0) {
            [integrators:companiesIntegrators];
        } else {
            redirect(action: Constants.VIEW_NOT_INTEGRATOR);
        }
    }

    /**
     * Will only renders the notintegrator view
     */
    def notintegrator(){}

    /**
     * Renders the page for the creation of a new Integrator Company
     * @return
     */
    def create() {
        def countries = Country.list();
        def states = State.findAllByCountry(countries[0]);
        def users = this.findUsersByRolesIntegratorAndOwner();
        def company = new Company();
        company.state = states[0];
        [integrator:new CompanyIntegrator(), company:company, users:users, countries:countries, states:states]
    }

    /**
     * Renders the page with the Integrator Company information as read only
     * @param id
     *      Id of the Integrator Company to review information or the associated Company
     */
    def show(long id) {
        CompanyIntegrator integrator = CompanyIntegrator.get(id);
        if (!integrator) {
            Company company = Company.get(id);
            if(company) {
                integrator = CompanyIntegrator.findByCompany(company);
            }
        }
        if (integrator) {
            [integrator:integrator]
        } else {
            flash.error = message(code: 'default.not.found.message');
            redirect(action: Constants.VIEW_INDEX, params: []);
        }
    }

    /**
     * This method will change the approve value from true to false depending of the current value
     * @param id
     *      Id of the Integrator Company to update
     */
    def approve(Long id) {
        CompanyIntegrator integrator = CompanyIntegrator.read(id);
        if(integrator) {
            integrator.approved = !integrator.approved;
            if (integrator.save()) {
                flash.success = message(code: 'default.upated.message', args: [integrator.company?.name]);
                redirect(action: Constants.VIEW_INDEX, params: []);
            } else {
                flash.error = message(code: 'default.not.upated.message', args: [integrator.company?.name]);
                redirect(action: Constants.VIEW_SHOW, id: id, params: []);
            }
        } else {
            flash.error = message(code: 'default.not.found.message', args: [message(code: 'integrator.label', default: 'User Information'), integrator.company?.name])
            redirect(action:  Constants.VIEW_INDEX, params: [])
        }
    }

    def Save() {
        Company company = null;
        CompanyIntegrator integrator = null;
        def deleteWhenFails = [];
        try {
            company = new Company(params);
            integrator = new CompanyIntegrator(params);
            company.createdBy = getCurrentUser();
            company.typeOfCompany = CompanyType.findByName(CompanyTypeEnum.INTEGRATOR.value());
            integrator.company = company;

            if(!company.country.hasStates()){
                company.state = null;
            }

            if(company.validate() &&  integrator.validate() ) {
                Company.withTransaction { status ->
                    try{
                        company.save();
                        integrator.save();
                        Role role = Role.findByAuthority(RoleEnum.INTEGRATOR.value());
                        def isIntegrator = company.owner.authorities?.contains(role);
                        if(!isIntegrator){
                            UserRole.create company.owner,role;
                        }
                        //Check and upload the logo Image
                        String fileId = "bgLogoUrl";
                        String url = uploadImageFile(message(code:'company.logo'), fileId,
                                            Constants.COMPANY_LOGO_WIDTH, Constants.COMPANY_LOGO_HEIGHT);
                        if(url != null) {
                            company.logoUrl = url;
                            deleteWhenFails.add(url);
                            company.save();
                        }
                    } catch(Exception dbEx) {
                        if(flash.error == null || flash.error.trim() == "") {
                            log.error(dbEx);
                            flash.error = dbEx.getMessage();
                        }
                        status.setRollbackOnly();
                    }
                }
            } else {
                flash.error=message(code: 'integrator.error.saving')
            }
        } catch(Exception ex) {
            log.error(ex);
            if(flash.error == null || flash.error.trim().equals("")) {
                flash.error = ex.getMessage();
            }
        }

        if(flash.error != null){
            //flash.error=message(code: 'integrator.error.saving')
            def countries=Country.list();
            def states=State.findAllByCountry(company.country);
            if(company.state != null){
                company.state.country = company.country
            }
            def typesCompany=CompanyCatalogType.list();
            def typeofAccount= CompanyType.findAll();
            def users = this.findUsersByRolesIntegratorAndOwner();
            rackspaceService.deleteMultipleFiles(deleteWhenFails);
            def model = [company:company, integrator:integrator, users:users, countries:countries, 
                typeofAccount:typeofAccount,typesCompany:typesCompany, states:states];
            render (view:Constants.VIEW_CREATE, model:model);
        } else {
            flash.success = message(code: 'default.message.success');
            redirect(action: Constants.VIEW_INDEX);
        }
    }

	def editIntegratorCompany(Long id){
		def company = Company.get(id)
		def integrator = CompanyIntegrator.findByCompany(company)
		if(integrator != null){
			redirect ( action:"edit" , id: integrator.id , params:params) 
		}else{
			flash.error = message(code: 'default.not.found.message')
			redirect(action: Constants.VIEW_INDEX, params: [])
		}
	}
	
	def edit(long id)
	{

		def integrator=CompanyIntegrator.get(id)
				
		if (!integrator) {
			
			def company = Company.get(id)
			if(!company){
				flash.error = message(code: 'default.not.found.message')
				redirect(action: Constants.VIEW_INDEX, params: [])
				return
			}else{
				integrator = CompanyIntegrator.findByCompany(company)
			}
		}
		if (integrator != null){			  
			def countries=Country.list()
			def states= new ArrayList()		
			if(integrator.company.state != null){
				states=State.findAllByCountry(integrator.company.state.country)
				integrator.company.country = integrator.company.state.country
			}
			 
			def typesCompany=CompanyCatalogType.list()
			def users = this.findUsersByRolesIntegratorAndOwner()
			
			[integrator:integrator,countries:countries, users:users, typesCompany:typesCompany, states:states]
		}
		else{
			flash.error=message(code: 'default.error.problem')
			redirect(action: Constants.VIEW_INDEX, controller:"home")
			return
		}
	}

	def update(long id, Long version)
	{
		def integrator = null
		def imagesUrlsToDelete = [];
		def deleteWhenFails = [];
		Map<String, String> originalImages = new HashMap<String, String>();
		try{
			integrator=CompanyIntegrator.read(id)
			originalImages.put("logoUrl", integrator.company.logoUrl);
			if(integrator != null){
				if (integrator.version > version) {
						flash.error=message(code: 'default.company.edit.version.error')
						//integrator.errors.rejectValue("version", "default.company.edit.version.error")
				}else{
					integrator.properties = params				
					integrator.company.properties = params
							
					if(!integrator.company.country.hasStates()){
						integrator.company.state = null
					}
					
					if(integrator.validate() && integrator.company.validate())
					{
						CompanyIntegrator.withTransaction { status ->
							try{							
								integrator.save();
								
								//Check and upload the logo Image
								String fileId = "bgLogoUrl";
								String url = uploadImageFile(message(code:'company.logo'), fileId,
									Constants.COMPANY_LOGO_WIDTH, Constants.COMPANY_LOGO_HEIGHT);
								if(url != null){
									imagesUrlsToDelete.add(integrator.company.logoUrl);
									integrator.company.logoUrl = url;
									deleteWhenFails.add(url);
								}								
								flash.success=message(code: 'default.message.success')
							}catch(Exception dbEx){
								if(flash.error == null || flash.error.trim() == ""){
									dbEx.printStackTrace();
									flash.error=dbEx.getMessage();
								}
								
								status.setRollbackOnly();								
							}
						}
					}
					else
					{
						flash.error=message(code: 'default.not.updated.message', args: [integrator.company?.name])
					}
				}
			}else{
				flash.error=message(code: 'default.error.problem')
				redirect(action: Constants.VIEW_INDEX, controller:"home")
				return
			}
		}catch(Exception ex) {
			if(flash.error == null || flash.error.trim() == ""){
				ex.printStackTrace()
				flash.error=message(code: 'default.not.updated.message', args: [integrator.company?.name])
			}
		}
		
		if(flash.error != null){
			integrator.company.logoUrl = originalImages.get("logoUrl");
			rackspaceService.deleteMultipleFiles(deleteWhenFails);
			def countries=Country.list()
			//Get by default state and cities, maybe the default can will be in a config file
			def states=State.findAllByCountry(integrator.company.country)
			def typesCompany=CompanyCatalogType.list()
			def typeofAccount= CompanyType.findAll()
			def role = Role.findByAuthority(RoleEnum.INTEGRATOR.value)
			def users = UserRole.findAllByRole(role).user
			render(view:Constants.VIEW_EDIT, model:[integrator:integrator, users:users,countries:countries,typeofAccount:typeofAccount,typesCompany:typesCompany, states:states])
			return
		}else{
			rackspaceService.deleteMultipleFiles(imagesUrlsToDelete);
			redirect(controller:"companyIntegrator",action:Constants.VIEW_INDEX,id:id, params: [])
			return
		}
	}

    /**
     * Returns the list of unique Users with the role Integrator or Owner
     * @return List of Users
     */
    private def findUsersByRolesIntegratorAndOwner() {
        def roles = Role.findAllByAuthorityInList([RoleEnum.INTEGRATOR.value(), RoleEnum.OWNER.value()])
        return UserRole.findAllByRoleInList(roles.collect())*.user.unique()
    }
}