package mycentralserver

import mycentralserver.device.Device;
import mycentralserver.utils.Constants;
import mycentralserver.utils.Utils;

/**
 * Controller that receives the requests related
 * with the management of the Devices
 *
 * @author      sdiaz
 * @date     	08/13/2015
 */
class DeviceController {

	/* Inject required services */
	def deviceService;
	
	/**
	 * Renders the list page.
	 */
	def index() {
		try {
			def devices = Utils.sortByName(Device.list());

			if(devices.size() == 0){
				render(view: "emptyList")
			} else {
				[devices: devices]
			}
		} catch( Exception e) {
			// Generic catch because we are not expecting an specific type but
			// grails can trigger unexpected exceptions some times
			log.error("Error getting the list of Devices: ${e.message}", e);
			throw e;
		}
	}

	/**
	 * Renders the page that allow the creation of a new Device
	 *
	 * @return
	 */
	def create() {
		[entity: new Device()]
	}

	/**
	 * Receives the parameters for a new Device, validates and save the entity to the database
	 */
	def save() {
		Device device = null;
		try {
			device = new Device(params);
			if(device.validate()) {
				device.save();
				deviceService.sendListOfDevicesWithSmallerPackages();
				flash.success = message(code: 'device.create.success');
			} else {
				flash.error = message(code: 'default.error.problem');
			}
		} catch( Exception ex) {
			// Generic catch because we are not expecting an specific type but
			// grails can trigger unexpected exceptions some times
			flash.error = message(code: 'default.error.problem');
			log.error("Error saving a Device", ex);
		}

		if(flash.error != null){
			render(view: Constants.VIEW_CREATE, model: [entity: device]);
		} else {
			redirect(action:Constants.VIEW_INDEX);
		}
	}

	/**
	 * Receives an Identifier and renders the page
	 * that will show all the information of the device
	 * 
	 * @param id	Id of the Device to see
	 */
	def show(long id) {
		Device device = Device.read(id);
		if (!device) {
			// Device do not exists
			flash.error = message(code: 'device.not.found');
			redirect(action: Constants.VIEW_LISTALL);
		}
		[entity: device]
	}

	/**
	 * Renders the page to allow the edition of a Device
	 *
	 * @param id	Id of the Device to be edited.
	 */
	def edit(long id) {
		Device device = Device.read(id);
		if (device) {
			[entity: device]
		} else {
			// The device don't exists
			flash.error = message(code: 'device.not.found');
			redirect(action:Constants.VIEW_LISTALL);
		}
	}

	def update(long id, Long version) {

		Device device = null;

		try {
			device = Device.read(id);
			device.properties = params;

			if(!device.hasErrors() && device.validate()) {
				if (device.save()) {
					deviceService.sendListOfDevicesWithSmallerPackages();
					flash.success=message(code: 'default.message.success');
				} else {
					flash.error=message(code: 'default.error.problem');
				}
			} else {
				flash.error=message(code: 'default.error.problem')
			}
		} catch(Exception ex) {
			// Generic catch because we are not expecting an specific type but
			// grails can trigger unexpected exceptions some times
			log.error("Error updating a Device", ex);
			flash.error=message(code: 'default.error.problem')
		}

		if (flash.error != null) {
			render(view: Constants.VIEW_EDIT, model:[entity: device])
		} else{
			redirect(action: Constants.VIEW_INDEX)
		}
	}

	/**
	 * Deletes an Device
	 *
	 * @param id	Id of the Device to be deleted.
	 */
	def delete(long id) {
		try {
			Device device = Device.read(id);
			if (device) {
				device.delete();
				deviceService.sendListOfDevicesWithSmallerPackages();
				flash.success = message(code: 'device.remove.success');
			} else { // The device don't exists
				flash.error = message(code: 'device.not.found');
			}
		} catch(Exception e){
			// Generic catch because we are not expecting an specific type but
			// grails can trigger unexpected exceptions some times
			log.error("Error deleting the Device with id=" + id + ": ${e.message}", e);
			flash.error = message(code:'default.error.problem');
		}

		redirect(action:Constants.VIEW_INDEX);
	}
}