package mycentralserver

import mycentralserver.box.BoxBlackList;
import mycentralserver.box.BoxManufacturer;
import grails.converters.JSON;

/**
 * Controller that receives the requests for the Public Api
 * 
 * @author sdiaz
 * @since 01/29/2015
 */
class ApiController {

    /* Inject required services */
    def boxService;
    def restClientService;

    static allowedMethods = [
        validateBoxConnection:'POST']

    /**
     * Validates if a Box is not in the blacklist and if the certificate is correct
     */
    def validateBoxConnection() {
        HashMap jsonMap = new HashMap()
        try {
            final String serial = params.serial;
            final String certificate = params.cert;
            final String certVersion = params.certVersion;
            final Map<String, Object> validationMap = boxService.validateBoxConnectionBySerial(serial, certificate, certVersion);            
            jsonMap.msg = validationMap.statusDescription;
            jsonMap.valid = validationMap.results;
        } catch (Exception e) {
            // Catching general exception because unexpected grails errors; the message shouldn't be displayed
            // to a final user
            log.error("Error validating the box connection with params: " + params , e);
            jsonMap.error =  true;
            jsonMap.msg = "Unexpected error: " + e.getMessage();
        }
        render jsonMap as JSON;
    }
}