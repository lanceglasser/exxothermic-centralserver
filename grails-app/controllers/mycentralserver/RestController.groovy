package mycentralserver

import grails.converters.JSON
import grails.plugins.rest.client.RestBuilder
import grails.plugins.rest.client.RestResponse;

import java.awt.GraphicsConfiguration.DefaultBufferCapabilities;
import java.nio.file.Files;
import java.nio.file.Path
import java.nio.file.Paths
import java.security.MessageDigest
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import mycentralserver.content.Content
import mycentralserver.user.User
import mycentralserver.user.Role
import mycentralserver.app.AppUser
import mycentralserver.app.AppEvent
import mycentralserver.app.AppDevice
import mycentralserver.app.AppData
import mycentralserver.company.Company
import mycentralserver.company.CompanyIntegrator
import mycentralserver.company.CompanyLocation
import mycentralserver.generaldomains.Scheduler
import groovy.io.FileType;
import mycentralserver.utils.Utils

import java.util.Date
import java.text.DateFormat
import java.text.SimpleDateFormat

import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile

import java.util.Iterator;


/**
 * This class controls the communication between Communication and CentralServer
 * @author David
 *
 */
class RestController {

	def grailsApplication
	//def boxService
	def contentService
	def mobileService

	def private STATUS_SUCCESSFUL = "Successful"

	public static enum Status {
		STATUS_EXCEPTION("Exception"),
		STATUS_SUCCESSFUL("Successful"),
		STATUS_FAILED("Failed");

		private final String status;

		private Status(String status) {
			this.status = status;
		}

		public String getStatus() {
			return status;
		}

		public static Status getByName(String name) {
			for (Status prop : values()) {
				if (prop.getStatus().equals(name)) {
					return prop;
				}
			}

			throw new IllegalArgumentException(name + " is not a valid PropName");
		}
	} // ENUM Status

	public static enum ErrorCode {
		NO_ERROR("0"),
		TIME_OUT("1"),
		GENERAL_EXCEPTION("2"),
		NO_CHANNELS_FOUND("3"),
		NO_DEVICE_FOUND("4"),
		UPDATE_SOFTWARE_FAILED("5"),
		BLACK_LISTED("6"),
		INVALID_PARAMETERS("7")

		private final String errorCode;

		private ErrorCode(String errorCode) {
			this.errorCode = errorCode;
		}

		public String getErrorCode() {
			return errorCode;
		}

		public static ErrorCode getByErrorCode(String name) {
			for (ErrorCode prop : values()) {
				if (prop.getErrorCode().equals(name)) {
					return prop;
				}
			}

			throw new IllegalArgumentException(name + " is not a valid PropName");
		}
	} // END ErrorCode

	static allowedMethods =[getUserOffers:'POST',
		registerEvent:'POST',
		offers:'GET']

	 
	 def subscribeUser() {
						 
		log.info(params);
		def error=0;
		def status="unknown";		
		def code;	// can be an error code or a deviceId	
				
		(error , status, code) = mobileService.registerAppUser(params.deviceId, params.socialId, params.socialToken, params.socialKey, params.socialType);
		
		if(error)
		{
			render( [error:error, status:status,code:code ] as JSON);
		}else{
			render( [error:error, status:status,deviceId:code ] as JSON);
		}
		
		
	} 	// END subscribeUser
	 
	 def registerEvent() {
		 
		log.info(params);
		def error=false;
		
		def responseCode = mobileService.registerAppEvent(params.deviceId, params.locationId, params.event, params.details);
		if(responseCode!=0)
		{
			error=true;
		}
		
		render( [error:error,responseCode:responseCode] as JSON);

	 } 	// END registerEvent
	 
	 def offers() {
		 
		log.info(params);
		def offers, offersList, error=false, message="success";		
		AppDevice appDevice;
		AppUser appUser;
		appDevice = AppDevice.read(params.deviceId);
		if(appDevice)
		{
			if(appDevice.appUser!= null)
			{
				appUser = appDevice.appUser;
				offers = mobileService.getUserOffers(appUser);				
			}else{ // just find the device offers
			//getDeviceOffers
				offers = mobileService.getDeviceOffers(appDevice);
			}
			offersList = mobileService.findOffers(offers,"UTC", params.device);
			
		}else{
			error=true;
			message="device not found!";
		}		
		render( [error:error, message:message, offers:offersList] as JSON);

	 } 	// END registerEvent
	 
	 
	 def networkChannels() { // testing to simulate AppClients
		 
		
		render( [
				number:1412130789, 
				port:"", 
				title:"Channel 0",
				backgroundColor: "FF99FF",
				isPA: false,
				isAvailable: true,
				ipAddress: "192.168.0.117"
				] as JSON
			);

	 } 	// END networkChannels
	 
	 def stream() { // testing to simulate AppClients
		 
		
		render(params);

	 } 	// END networkChannels
	 

} // END GearsController
