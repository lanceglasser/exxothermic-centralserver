/**
 * BoxMetricsController.groovy
 */
package mycentralserver;

import java.security.InvalidParameterException;

import mycentralserver.box.Box;
import mycentralserver.company.CompanyLocation;
import mycentralserver.utils.ExcelGeneratorUtil;
import grails.converters.JSON;

/**
 * Controller that handles all the request related with the 
 * ExXtractors Metrics; initially created for the Usage Reports
 * 
 * @author Cecropia Solutions
 *
 */
class BoxMetricsController {

	/* Injection of required services */
	def boxMetricsService;
	def userSessionService;
	def retrieveUsageDataService;
    def usageEventService;
	
	def requestDataAllBoxes(){
		HashMap jsonMap = new HashMap()
		try {
			retrieveUsageDataService.retrieveExXtractorsData();
			jsonMap.error =  false;
			jsonMap.msg = "Success";
		} catch(Exception e) {
			log.error("Error testing the proccess to retrieve the usage data files.", e);
			jsonMap.error =  false;
			jsonMap.msg = "Unexpected error: " + e.getMessage();
		}

		def jsonToSend = (jsonMap as JSON);
		log.debug("BoxMetricsController :: test :: response: " + jsonToSend);
		render jsonToSend;
		
	}
	
	/**
	 * Receive a serial number to request the upload of the metrics data file,
	 * created for testing at the beggining
	 * 
	 */
	def requestData() {
		HashMap jsonMap = new HashMap()
		try {
			String serial = params.serial? params.serial:'EXX1-476E-2B96-4620';
			jsonMap.serial = serial;
			def metricsRequestResult = boxMetricsService.requestMetricsDataFromBox(serial);
			if(metricsRequestResult){ //If true the request was O.K.
				jsonMap.error =  false;
				jsonMap.msg = "Success";
			} else {
				jsonMap.error =  true;
				jsonMap.msg = "Error requesting the upload of the metrics file";
			}
		} catch(Exception e) {
			log.error("Error rendering the view for the generation of the usage report", e);
			jsonMap.error =  false;
			jsonMap.msg = "Unexpected error: " + e.getMessage();
		}

		def jsonToSend = (jsonMap as JSON);
		log.debug("BoxMetricsController :: report :: response: " + jsonToSend);
		render jsonToSend;
	} // End of requestData method
	
	/**
	 * Render the view where the user can generate
	 * the usage report
	 */
    def detailedReport() {
		[locations: userSessionService.getPermitedLocations()]
	} // End of report method
	
	/**
	 * Render the view where the user can generate
	 * the usage report
	 */
	def tables() {
		[locations: userSessionService.getPermitedLocations()]
	} // End of report method
	
	/**
	 * Render the view where the user can generate
	 * the usage report charts: important stuff
	 */
	def report() {
		[locations: userSessionService.getPermitedLocations()]
	} // End of report method
	
	/**
	 * This method will find the data results depending of parameters 
	 * and return the data as json to be rendered at the table
	 * 
	 * @return	Data as Json
	 */
	def getDataAjax() {
		HashMap jsonMap = new HashMap();
		def data = [];
		try {			
			jsonMap = boxMetricsService.getData(params);			
			jsonMap.error =  false;
		} catch(Exception e) {
			jsonMap = new HashMap();
			log.error("Error getting the table data", e);
			jsonMap.error = false;
			jsonMap.msg = "Unexpected error: " + e.getMessage();
		}
		def jsonToSend = (jsonMap as JSON);
		render jsonToSend;
	} // End of getDataAjax method
	
	/**
	 * This method will find the data results depending of parameters
	 * and return the data as json to be rendered at the table
	 *
	 * @return	Data as Json
	 */
	def getHourClientsDataAjax() {
		HashMap jsonMap = new HashMap();
		def data = [];
		try {
			jsonMap = boxMetricsService.getHourClientsData(params);
			jsonMap.error =  false;
		} catch(Exception e) {
			jsonMap = new HashMap();
			log.error("Error getting the table data", e);
			jsonMap.error =  false;
			jsonMap.msg = "Unexpected error: " + e.getMessage();
		}
		def jsonToSend = (jsonMap as JSON);
		render jsonToSend;
	} // End of getDataAjax method
	
	/**
	 * This method will find the data results depending of parameters
	 * and return the data as json to be rendered at the table
	 *
	 * @return	Data as Json
	 */
	def getDailyClientsDataAjax() {
		HashMap jsonMap = new HashMap();
		def data = [];
		try {
			jsonMap = boxMetricsService.getDailyClientsData(params);
			jsonMap.error =  false;
		} catch(Exception e) {
			jsonMap = new HashMap();
			log.error("Error getting the table data", e);
			jsonMap.error =  false;
			jsonMap.msg = "Unexpected error: " + e.getMessage();
		}
		def jsonToSend = (jsonMap as JSON);
		render jsonToSend;
	} // End of getDataAjax method
	
	/**
	 * This method will find the charts data results depending of parameters
	 * and return the data as json
	 *
	 * @return	Data as Json
	 */
	def getChartsDataAjax() {
		HashMap jsonMap = new HashMap();
		def data = [];
		try {
			jsonMap = boxMetricsService.getChartData(params);
			jsonMap.error =  false;
		} catch(Exception e) {
			log.error("Error gettign the charts data", e);
			jsonMap.error =  false;
			jsonMap.msg = "Unexpected error: " + e.getMessage();
		}
		def jsonToSend = (jsonMap as JSON);
		render jsonToSend;
	} // End of getChartsDataAjax method
	
	def getHourClientsChartsDataAjax() {
		HashMap jsonMap = new HashMap();
		def data = [];
		try {
			jsonMap = boxMetricsService.getHourClientsChartData(params);
			jsonMap.error =  false;
		} catch(Exception e) {
			log.error("Error gettign the charts data", e);
			jsonMap.error =  false;
			jsonMap.msg = "Unexpected error: " + e.getMessage();
		}
		def jsonToSend = (jsonMap as JSON);
		render jsonToSend;
	} // End of getChartsDataAjax method
	
	def getMainChartsDataAjax() {
		HashMap jsonMap = new HashMap();
		def data = [];
		try {
			jsonMap = boxMetricsService.getMainChartsData(params);
			jsonMap.error =  false;
		} catch(Exception e) {
			log.error("Error getting the main charts data", e);
			jsonMap.error =  false;
			jsonMap.msg = "Unexpected error: " + e.getMessage();
		}
		def jsonToSend = (jsonMap as JSON);
		render jsonToSend;
	} // End of getMainChartsDataAjax method
	
	/**
	 * This method will find the charts data results depending of parameters
	 * and return the data as json
	 *
	 * @return	Data as Json
	 */
	def getRangeChartsDataAjax() {
		HashMap jsonMap = new HashMap();
		def data = [];
		try {
			jsonMap = boxMetricsService.getRangeChartsData(params);
			jsonMap.error =  false;
		} catch(Exception e) {
			log.error("Error gettign the charts data", e);
			jsonMap.error =  false;
			jsonMap.msg = "Unexpected error: " + e.getMessage();
		}
		def jsonToSend = (jsonMap as JSON);
		render jsonToSend;
	} // End of getRangeChartsDataAjax method
	
	
	/**
	 * This method will generate the excel file required by the user,
	 * including the data by Location and Dates
	 * @return
	 */
	def generateMetricsReportFile(){
		
		log.debug("*** Generate Metrics Report File: " + params + " ***");
		
		try {
			//Call the service for the generation of the file
			boxMetricsService.generateExcelFile(response, params);
		} catch (Exception ex){
			log.error ("Error generating metrics data file: " + params, ex);
			response.sendError(500, ex.getMessage());
		}
        
	} // End of generateMetricsReportFile method
	
	/**
	 * This method will generate the excel file required by the user,
	 * including the data by Location and Dates
	 * @return
	 */
	def generateMainMetricsFile(){		
		try {
			//Call the service for the generation of the file
			boxMetricsService.generateMainMetricsExcelFile(response, params);
		} catch (Exception ex){
			log.error ("Error generating metrics data file: " + params, ex);
			response.sendError(500, ex.getMessage());
		}
	} // End of generateMainMetricsFile method
	
	/**
	 * This is the service use it by the ExXtractors for the Upload
	 * of the Metrics data file. The request must be MultipartHttpServletRequest.
	 * 
	 * Must receive the params: serial, sendfile
	 */
	def uploadFile() {
		try {						
			boxMetricsService.uploadDataFile(request, response);			
		} catch (Exception ex){
			log.error ("Error uploading metrics data file: " + params, ex);
			response.sendError(500, ex.getMessage());
		}
	} // End of uploadFile method

    /**
     * This method will find the data results depending of parameters and return the data as json to be rendered at the
     * charts including the information from the first and second usage data structure
     *
     * @return  Data as Json
     */
    def getUsageEventDataAjax() {
        try {
            render usageEventService.getChartsData(params);
        } catch(InvalidParameterException e) {
            HashMap jsonMap = new HashMap();
            jsonMap.error = true;
            // Sending exception message because this is not displayed to the final User
            jsonMap.msg = e.getMessage();
            render jsonMap as JSON;
        } catch(Exception e) {
            // We are not expecting an Exception but we catch here for unexpected Grails execution exceptions
            log.error("Error getting the charts data", e);
            HashMap jsonMap = new HashMap();
            jsonMap.error =  true;
            jsonMap.msg = "Unexpected error getting the data";
            render jsonMap as JSON;
        }
    }
}