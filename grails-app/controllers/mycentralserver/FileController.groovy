package mycentralserver

import java.security.MessageDigest

import mycentralserver.box.Box
import mycentralserver.box.software.BoxSoftware
import mycentralserver.user.User
import mycentralserver.utils.DigestHelper
import mycentralserver.utils.FileHelper
import mycentralserver.utils.Constants
import mycentralserver.utils.PartnerEnum;
import mycentralserver.box.BoxStatus
import sun.misc.BASE64Encoder
import mycentralserver.user.Role;
import mycentralserver.utils.RoleEnum;

class FileController extends BaseControllerMyCentralServerController {


	def grailsApplication
	RestClientService restClientService
	MessageErrorService messageErrorService
	
    def downloadSoftwareVersion() { 
		def boxSoft = BoxSoftware.read(params.id)
		if(boxSoft){
			def path = getPathFromFile(boxSoft.fileName);
			def file = new File(path)
			if (file.exists())
			{
				response.setContentType("application/octet-stream") // or or image/JPEG or text/xml or whatever type the file is
				response.setHeader("Content-disposition", "attachment;filename=${file.name}")
				response.status = 200
				response.contentLength = file.length()
				
				def inputStream = file.newInputStream()
				response.outputStream << inputStream
			}
			else {
				response.status = 404
				render "The file not exists."
			}
		} else {
			response.status = 404
			render "The file not exists."
		}
			
	}
	
	/**
	 * This method is used from the Box for the download of the 
	 * complete image.
	 * 
	 * Receives the filename including the path with the partner and architecture.
	 * 
	 * @return
	 */
	def downloadSoftwareVersionImage() {
		def path = getPathFromImageFile(params.file);
		def file = new File(path)
		if (file.exists())
		{
			response.setContentType("application/octet-stream") // or or image/JPEG or text/xml or whatever type the file is
			response.setHeader("Content-disposition", "attachment;filename=${file.name}")
			response.status = 200
			response.contentLength = file.length()
			
			def inputStream = file.newInputStream()
			response.outputStream << inputStream
		}
		else {
			response.status = 404
			render "The file not exists."
		}	
	}
	
	/**
	 * This method is used from the Box for the download of the
	 * image configuration file.
	 *
	 * Receives the filename including the path with the partner and architecture,
	 * the file will be located at the partner/client root.
	 *
	 * @return
	 */
	def downloadImageConfigFile() {
		def path = getImageConfigFilePath(params.file);
		def file = new File(path)
		if (file.exists())
		{
			response.setContentType("application/octet-stream") // or or image/JPEG or text/xml or whatever type the file is
			response.setHeader("Content-disposition", "attachment;filename=${file.name}")
			response.status = 200
			response.contentLength = file.length()
			
			def inputStream = file.newInputStream()
			response.outputStream << inputStream
		}
		else {
			response.status = 404
			render "The file not exists."
		}
	}
	
	private String getSingleFileName(String fileName){
		def words = fileName.split('/');
		return words[words.length-1];
	}
	
	private String getImageConfigFilePath(String fileName){
		def storagePath = grailsApplication.config.basePathImageFiles;
		def path = "";
		def words = fileName.split('/');
		//fileName = "conf_" + words[words.length - 1];//The last word is the file name
		def partnerCode = PartnerEnum.getDefaultPartnerCode();
		if(words.length > 1){
			partnerCode = words[1];
		}
		
		path = storagePath + partnerCode + "/enc.partner.key";
	}
	
	private String getPathFromImageFile(String fileNameOrigin) {
		def storagePath = grailsApplication.config.basePathImageFiles
		def path = storagePath + "/" + fileNameOrigin
		return path
	}
	
	private String getPathFromFile(String fileNameOrigin) {
		def storagePath = grailsApplication.config.basePathMyBoxSoftwareUpdate
		def path = storagePath + "/" + fileNameOrigin
		return path
	}
	
	
} // END FileController
