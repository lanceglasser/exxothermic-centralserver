package mycentralserver

import grails.converters.JSON;

/**
 * This controller will handle the requests and method related
 * with vars in session; created for the Pagination issue
 * 
 * @author Selim
 *
 */
class SessionController extends BaseControllerMyCentralServerController {

	/**
	 * Ajax request; must receive the pagination info and the
	 * page code in order to save the information.
	 * 
	 * @return
	 */
    def savePaginationDetail(){
		boolean error = false;
		String msg = "";
		try {
			HashMap<String, String> paginationsInfo = (session.paginationInfo)? session.paginationInfo:new HashMap<String, String>();
			String jsonStr = 
				'{"rows":' + params.rows.toInteger() + 
				',"page":' + (params.page.toInteger()-1) + 
				',"columnOrder":' + (params.columnOrder.toInteger()) +
				',"orderDir":"' + params.orderDir +
				'","text":"' + params.search+'"}';
			paginationsInfo.put(params.code, jsonStr);
			session.paginationInfo = paginationsInfo;
			msg = "OK";			
		}catch(Exception ex){
			error = true;
			msg = ex.getMessage();
			log.warn("Error saving table configuration: " + ex.getMessage() + " with params: " + params);
		}
		
		def result = [error: error, msg: msg];
		render result as JSON;
	}
}
