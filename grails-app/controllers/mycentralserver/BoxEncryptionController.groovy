package mycentralserver

import java.security.KeyFactory
import java.security.PublicKey
import java.security.spec.RSAPublicKeySpec

import mycentralserver.box.BoxEncryption

class BoxEncryptionController extends BaseControllerMyCentralServerController 
{	
	
    def index() { }
	
	def create() {
		BoxEncryption boxEncryption = new BoxEncryption();
		[boxEncryption:boxEncryption]
	}
	
	def save() {
		def serial 	  			= params.serial
		def publicKeyExponent 	= params.publicKeyExponent
		def publicKeyModule 	= params.publicKeyModule
		
		
		def fileName  = "publicKey_serial_" + serial
		BoxEncryption boxEncryption = new BoxEncryption();
		def fullPathPublicKey = grailsApplication.config.basePathPublicKeyFiles + fileName //getPathFromFile(grailsApplication.basePathPublicKeyFiles, fileName)
		
		boxEncryption.serial = params.serial
		boxEncryption.publicKeyFile = fullPathPublicKey
		
		if(boxEncryption.validate()) {
			
			BoxEncryption.withTransaction { status ->
				
				try {
					boxEncryption.save();
					def publicKeyString = getPublicKey( publicKeyModule, publicKeyExponent)
					
					if(savePublickKeyInFile(fullPathPublicKey, publicKeyString)) {
						flash.success = message(code: 'default.message.success')
					} else {
						status.setRollbackOnly()
						flash.error = message(code: 'default.box.enctyption.error.savingTheFile')
					}
				} catch(Exception dbEx)	{
					dbEx.printStackTrace()
					status.setRollbackOnly()
					flash.error = (dbEx.getMessage())?dbEx.getMessage():message(code: 'default.box.enctyption.error.savingTheFile')
				}
				
			}
		} else {
			flash.error = message(code: 'default.error.problem')
		}
		
		//flash.message = message(code: 'default.not.found.message')
		//redirect(action: "create", model)
		render(view:"create", model: [boxEncryption:boxEncryption])
		return
	}
	
	private def savePublickKeyInFile(String fullPath, String publicKey) {
		boolean isSaved = false;
		try {
			
			File fileToCreate = new File(fullPath);
			if(!fileToCreate.exists()) {
				fileToCreate.createNewFile();
			} else {
				fileToCreate.delete()
			}		
			
			FileOutputStream fileStore = new FileOutputStream(fileToCreate, false)			
			fileStore.write(publicKey.getBytes())
			fileStore.close()
			isSaved = true
		} catch(Exception ex) {
			log.error  ((" cann't save the public key file " + fullPath + " publicKey: " + publicKey)  , ex)
		}
		
		return isSaved
	}
	
	private def  getPublicKeyFromExponentAndModule(String module,String exponent)
	 {	
		try {
			BigInteger m = new BigInteger("7245167342676344317906934030302544552432079991256588369257926164425751465534242483204954676132059996724165172896153776444688381927937426790249978563155939") //(BigInteger) oin.readObject();
			BigInteger e = new BigInteger("65537")//(BigInteger) oin.readObject();
			RSAPublicKeySpec keySpec = new RSAPublicKeySpec(m, e)
			KeyFactory fact = KeyFactory.getInstance("RSA")
			PublicKey pubKey = fact.generatePublic(keySpec)
			return pubKey
		} catch (Exception e) {
		  throw new RuntimeException("Spurious serialisation error", e)
		  log.error e
		} 
	}
	 
	 private def getPublicKey(String module,String exponent) {
		 PublicKey pubKey = getPublicKeyFromExponentAndModule( module, exponent)
		 return pubKey.getEncoded().encodeBase64().toString();
	 }
	 
	private def readFileData(String file) {
		
		String sCurrentLine;
		StringBuilder stringBuffer = new StringBuilder();
		BufferedReader br = new BufferedReader(new FileReader(file));
  
		 while ((sCurrentLine = br.readLine()) != null) {
			 stringBuffer.append(sCurrentLine);
		 }
		 br.close();
		 return stringBuffer.toString();
	}
	
	def listAll() {
		
	}
	
	def edit(Long id) {
		
	}
	
	def update() {
		
	}
	
	def show(Long id) {
		
	}
	
	
	private getPathFromFile(String virtualPath, String fileName) {
		def storagePath = servletContext.getRealPath(virtualPath) + "/" + fileName
	}
}
