/**
 * BoxSoftwareUpgradeJobController.groovy
 */
package mycentralserver

import mycentralserver.box.software.BoxSoftwareUpgradeJob;
import mycentralserver.utils.Constants;
import grails.converters.JSON;

/**
 * Controller that handles the request related with the
 * Software Upgrade Job of the ExXtractors
 * 
 * @author Cecropia Solutions
 *
 */
class BoxSoftwareUpgradeJobController {

	/* Inject required services */
	def userSessionService;
	def softwareUpgradeService;
	
	/**
	 * Render the view with the list of Jobs 
	 */
    def list() {
		[jobs:softwareUpgradeService.getJobList()]
	} // End of list method
	
	/**
	 * Render the view with the detail of a Job
	 * 
	 * @param id	Id of the Job to render
	 */
	def show(long id) {
		int result = 0;
		BoxSoftwareUpgradeJob job = null;
		(result, job) = softwareUpgradeService.showJobDetails( id, flash );
		if(result == Constants.RESULT_OK){
			[job: job]
		} else {
			redirect(action: 'list');
		}
	} // End of show method
	
	/**
	 * Renders the view that allows the creation
	 * of a Software Upgrade Job
	 */
	def create() {
		[locations: userSessionService.getPermitedLocations()]
	} // End of create method
			
	/**
	 * This method will find the list of ExXtractors that required
	 * a software upgrade by Location or for all the locations,
	 * and return the data as json to be rendered at the table
	 *
	 * @return	Data as Json
	 */
	def getExXtractorsToUpgradeAjax() {
		HashMap jsonMap = new HashMap();
		try {
			jsonMap = softwareUpgradeService.getExXtractorsToUpgrade(params);
			jsonMap["error"] =  false;
		} catch(Exception e) {
			log.error("Error getting the list of ExXtractors to Upgrade", e);
			jsonMap["error"] =  true;
			jsonMap["msg"] = "Unexpected error: " + e.getMessage();
		}
		def jsonToSend = (jsonMap as JSON);
		render jsonToSend;
	} // End of getExXtractorsToUpgradeAjax method
	
	/**
	 * This method will receive a list of exxtractors to upgrade a will
	 * create the Job with all the steps for each exxtractor
	 * 
	 * @return
	 */
	def createJobAjax() {
		
		HashMap jsonMap = softwareUpgradeService.createUpgradeJob(params);
		def jsonToSend = (jsonMap as JSON);
		render jsonToSend;
		
	} // End of createJobAjax method
	
	/**
	 * This method will return a Json object with the details of the steps
	 * of a BoxSoftwareJob
	 * 
	 * @param id	Id of the BoxSoftwareJob
	 * @return		HashMap with the steps information
	 */
	def getStepsDetailAjax(long id) {
		HashMap jsonMap = softwareUpgradeService.getJobStepsDetail(id);
		def jsonToSend = (jsonMap as JSON);
		render jsonToSend;
	} // End of getStepsDetailAjax method
	
} // End of class