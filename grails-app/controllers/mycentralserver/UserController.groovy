package mycentralserver

import org.hibernate.SessionFactory
import org.springframework.dao.DataIntegrityViolationException
import mycentralserver.user.User
import mycentralserver.user.UserRole
import mycentralserver.user.Role
import mycentralserver.utils.PasswordGeneratorHelper
import mycentralserver.company.Company
import org.springframework.web.servlet.FlashMap
import grails.plugins.springsecurity.Secured
import mycentralserver.utils.Constants

class UserController  extends BaseControllerMyCentralServerController {

	def mailService
	def userService
    def userSessionService
	def affiliateService
 
	def listAll()
	{
		params.max = Math.min(params.max ? params.int(Constants.PARAM_MAX) : 500, 500)
		params.sort = Constants.EMAIL
		params.order=Constants.SORT_ASC
		def userList =  User.list(params)	
		def offset = this.getOffsetParameterToPagination()
		
				
		if(userList.size()==0) 
		{
			redirect(controller:Constants.CONTROLLER_USER,action: Constants.VIEW_NOT_USER)
		}
			
		[userList:userList, usersCount:User.count(), offset:offset]
	}	
		
    def create() {
	
		User newUser = new User(params)
		def roles = Role.list()
		
        [user: newUser, roles:roles]
    }
		
    def save() {		
		if ( params.size() > 2) { //if the only values are action and controller, then we are not coming from the form
			User user = null	
			UserRole userRole = null
				try{
					def passAutogenerated = PasswordGeneratorHelper.generatePassword()
					//params.password = passAutogenerated
					user = new User(params)					
					user.createdBy = getCurrentUser()
					user.lastUpdatedBy = getCurrentUser()
					user.passwordExpirationDate = userService.getPassswordExpirationDateForNewAccount()
					user.password = passAutogenerated
					userRole = new UserRole(params)
					userRole.user = user
					
					if(user.validate() && userRole.validate()){

						User.withTransaction { status ->
							try{
								user.enabled = true
								user.save()								
								 
								userRole.save()
								
								//Send Email
								mailService.sendMail {
									from	affiliateService.getCurrentAffiliateConcatEmail(session.affiliate)
									to  	user.email
									subject message(code: 'user.created')										
									html 	g.render(template:"/templates/emails/accountCreatedTemplate", model:[user:user, password:passAutogenerated])
								 }
								
							}catch(Exception dbEx){
								dbEx.printStackTrace()
								status.setRollbackOnly()
								flash.error=dbEx.getMessage()

							}
						}						
					}else{
						flash.error=message(code: 'user.error.saving')
					}
				}catch(Exception exp){				
					exp.printStackTrace()					
					flash.error=exp.getMessage()											
				}		
			
			if(flash.error !=null){
				flash.error=message(code: 'user.error.saving')	
				def roles = Role.list()
				render(view: Constants.VIEW_CREATE, model: [user: user, roles:roles])
				return
			}else{
				flash.success = message(code: 'default.created.message', args: [message(code: 'default.user.label', default: 'User Information'), user.email])
				redirect(action: Constants.VIEW_LISTALL)
				return
			}
		}else{
			flash.error = message(code: 'default.not.found.message')
			redirect(action: Constants.VIEW_LISTALL)
			return
		}		
    }
	 	
    def edit(Long id) {
		
		def offset = this.getOffsetParameterToPagination()
        def userInstance = User.get(id)		
	    if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'userInformation.label', default: 'User Information'), id])
            redirect(action:  Constants.VIEW_LISTALL, params: [offset: offset])
            return
        }else{        
			[user: userInstance, offset:offset]
		}
    }
		
	def show(Long id) {
		
		def offset = this.getOffsetParameterToPagination()
		
		def userInstance = User.get(id)			
		
		if (!userInstance) {
			flash.error = message(code: 'default.not.found.message', args: [message(code: 'userInformation.label', default: 'User Information'), id])
			redirect(action: Constants. Constants.VIEW_LISTALL, params: [offset: offset])
			return
		}else{
		
		 	[user: userInstance, offset:offset]
			 
		}
	}
		
	def myaccount()
	{		
		def userTemp=getCurrentUser()
		User user = User.read(userTemp.id)
		[user:user]
	}
	
	def updatemyaccount()
	{
		def user=getCurrentUser()
		user.properties = params
		
		if(!user.hasErrors() && user.save(flush: true))
		{
			flash.success=message(code: 'default.message.success')
		}
		else
		{
			user.discard()
			flash.error=message(code: 'default.error.problem')
		}
		render(view: Constants.VIEW_MYACCOUNT,  model: [user: user])
	}
	
	def changeUserPassword(Long id)
	{
		def user = User.read(id)
		def offset = this.getOffsetParameterToPagination()
		
		if (!user) {
			flash.error = message(code: 'default.not.found.message', args: [message(code: 'userInformation.label', default: 'User Information'), id])
			redirect(action: Constants.VIEW_LISTALL, params: [offset: offset])
			return
		}else{
			[user: user, redirectAction:Constants.VIEW_CHANGE_USER_PASSWORD, offset: offset]
		}
	}
	
	def changepassword(long id)
	{			
		if ( params.size() > 3) { //if the only values are action and controller, then we are not coming from the form
			def user=User.read(id)
			
			if (params.newPassword != params.confirmPassword)			
			{				
				flash.error= message(code: 'user.error.saving')
				user.errors.rejectValue("password", 'user.password.doesnotmatch')
			}else{
				 
				user.password = (params.newPassword == null ? "" : params.newPassword)
				
				if(user.validate()){
					user.setNewPassword(params.newPassword == null ? "" : params.newPassword)
					
					if(user.save()){
											
						flash.success=message(code: 'current.user.password.updated.mail')						
						
					}else{
						flash.error= message(code: 'user.error.saving')						
					}
				}else{
					flash.error= message(code: 'user.error.saving')
				}				
			}			 
			render(view: Constants.VIEW_MYACCOUNT, model: [user: user])			
			return
			
		}else{
			flash.error = message(code: 'default.not.found.message')
			redirect(action: Constants.VIEW_LISTALL)
			return
		}		 		
	}

    /**
     * Receives the request for the reset of the password of a User; will set the new password and send an email to the
     * user with the information to access to the system.
     * 
     * @param id
     *          Id of the User that require the reset of the password
     */
    def resetPassword(long id) {
        User user = User.read(id);
        if (user) {
            def passAutogenerated = PasswordGeneratorHelper.generatePassword();
            user.lastUpdatedBy = userSessionService.getCurrentUser();
            user.setNewPassword(passAutogenerated? passAutogenerated:"");
            user.passwordExpired = true;
            user.passwordExpirationDate = new Date();
            if (user.validate()) {
                User.withTransaction { status ->
                    try{
                        if (user.save(failOnError:true)) {    
                            //Send Email
                            mailService.sendMail {
                                from    affiliateService.getCurrentAffiliateConcatEmail(session.affiliate)
                                to      user.email
                                subject message(code: 'user.password.updated')
                                html    g.render(template:"/templates/emails/passwordUpdatedTemplate", model:[user:user, password:passAutogenerated])
                            };
                            flash.success = message(code: 'user.password.updated.mail', args: [user.email]);
                        } else {
                            flash.error = message(code: 'user.error.saving')
                        }
                    } catch(Exception dbEx) {
                        status.setRollbackOnly();
                        flash.error = dbEx.getMessage();
                    }
                }
            } else {
                flash.error= message(code: 'user.error.saving');
            }
            redirect(action: Constants.VIEW_LISTALL, model: [user: user, currentUser:userSessionService.getCurrentUser()]);
        } else {
            flash.error = message(code: 'default.not.found.message', 
                args: [message(code: 'userInformation.label', default: 'User Information'), id]);
            redirect(action:Constants.VIEW_LISTALL, params: []);
        }
    }
		
    def update(Long id, Long version) {
        User userInstance = User.read(id);
        if (userInstance) {
            // Use version != null because version can be 0 and if you use only version will be false
            if (version != null && userInstance.version == version) {
                userInstance.lastUpdatedBy = userSessionService.getCurrentUser();
                userInstance.properties = params;
                if (userInstance.validate()) {
                    try {
                        userInstance.save()
                        flash.success = message(code: 'default.updated.message',
                            args: [message(code: 'default.user.label'), userInstance.email])
                    } catch(Exception dbEx) {
                        log.error("Error Updating a User", dbEx);
                        flash.error = message(code:'unexpected.error.please.try.again');
                    }
                } else {
                    flash.error=message(code: 'default.not.upated.message', 
                        args: [message(code: 'default.user.label')])
                }
            } else {
                userInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'userInformation.label', default: 'User Information')] as Object[],
                        "Another user has updated this UserInformation while you were editing");
                flash.error = message(code: 'default.not.upated.message',
                    args: [message(code: 'default.user.label')]);
            }
            if (flash.error) {
                render(view: Constants.VIEW_EDIT, model: [user: userInstance]);
            } else {
                redirect(action: Constants.VIEW_LISTALL);
            }
        } else {
            flash.error = message(code: 'default.not.found.message');
            redirect(action: Constants.VIEW_LISTALL);
        }
    }
	
    def disabled(Long id) {
		
		def userInstance = User.read(id)
		def offset = this.getOffsetParameterToPagination()
		
		try {		
			
	        if (!userInstance) {
	            flash.error = message(code: 'default.not.found.message', args: [message(code: 'userInformation.label', default: 'User Information'), id])
	            redirect(action:Constants.VIEW_LISTALL, params: [offset: offset])
	            return
	        }else{
        	
				userInstance.enabled=!userInstance.enabled
				
				if (userInstance.validate()) {				
					userInstance.save()
					flash.success = message(code: 'default.upated.message', args: [ userInstance.firstName + " " + userInstance.lastName])
					redirect(action: Constants.VIEW_LISTALL, params: [offset: offset])					
				}else{
					flash.error = message(code: 'default.not.upated.message', args: [ userInstance.firstName + " " + userInstance.lastName])							            
					render(view: Constants.VIEW_SHOW, model: [id: id, user:userInstance, offset: offset])
				}
			}
        }
        catch (Exception e) {
			e.printStackTraace()
            flash.error = message(code: 'default.not.updated.message', args: [userInstance.firstName + " " + userInstance.lastName])
            redirect(action: Constants.VIEW_SHOW, id: id, params: [offset: offset])
        }
    }	
}
