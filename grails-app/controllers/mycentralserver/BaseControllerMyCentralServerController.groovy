package mycentralserver

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import mycentralserver.beans.ImageUploadResult;
import mycentralserver.company.CompanyLocation
import mycentralserver.user.User
import mycentralserver.user.Role
import mycentralserver.company.Company
import mycentralserver.company.CompanyCatalogSubType;
import mycentralserver.company.CompanyCatalogType;
import mycentralserver.company.CompanyIntegrator
import mycentralserver.utils.PartnerEnum;
import mycentralserver.utils.RoleEnum
import mycentralserver.utils.Utils
import mycentralserver.utils.Constants
import mycentralserver.app.Affiliate;
import mycentralserver.app.AppSkin
import mycentralserver.RestClientService

abstract class BaseControllerMyCentralServerController {

	def springSecurityService
	def grailsApplication
	def securityContextLogoutHandler
	def userSessionService;
	MessageErrorService messageErrorService
	RestClientService restClientService
	
	/**
	 * Get current logged user
	 * @return
	 */
	protected getCurrentUser()
	{
		byte[] data = new byte[64 / 2];
		for (int i = 0; i < 64; i += 2) {		
			data[ (int)(i/2)] = (byte) 5;
		}
		if(springSecurityService.principal){
			return User.get(springSecurityService.principal.id)
		}else{
			doLogoutManual()
		}
	}
	
	protected def getOffsetParameterToPagination() {
		def offset = 0
		if(params.offset) {
			offset = params.offset
		}
		
		return offset
	}
	
	protected def doLogoutManual() {
		Authentication auth = SecurityContextHolder.context.authentication
		securityContextLogoutHandler.logout(request,response,auth)
	}
	
	private getCompaniesFromCurrentUser(){
		
		
		return getCompaniesForUser()
	}
	
	private getCompaniesForUser() {
		def user = getCurrentUser()
		def companyList = null
		
		if (isUserRoleHelpDesk()){
			companyList = Company.findAll(sort:"name", order:"asc")
		}else{
			
			def temp = Company.getEnabledCompanies(user)
			companyList = temp.listDistinct(sort:"name", order:"asc").collect()
			def integrators = temp.typeIntegrator.list(sort:"name", order:"asc")
			
			for(i in integrators){
					CompanyIntegrator integrator = CompanyIntegrator.findByCompany(i)
					companyList.addAll(integrator.assignedCompanies.toList())
			}
		}
		return companyList.unique()
	}
		
	/**
	 * Returns the list of locations that the current user can see
	 * @return
	 */
	private def getPermitedLocations(){
		def locations = null
		try
		{									
			if(isUserRoleAdmin() || isUserRoleHelpDesk()){
				locations = CompanyLocation.findAll(sort:Constants.NAME, order:Constants.SORT_ASC)
			}else{
				  locations = CompanyLocation.getAllLocations(getCurrentUser(), null).listDistinct()
				  locations = Utils.sortByName(locations)
			}
					
		}catch(Exception e){
			e.printStackTrace()
		}
		return locations
	}
	
	
	
	def getPermitedCompanies(){

		def user = getCurrentUser()
		def companies = null

	
		if (isUserRoleHelpDesk()){
			companies = Company.findAll(sort:Constants.NAME, order: Constants.SORT_ASC)
		}else{

			def ownerStaffRole = Role.findByAuthority("ROLE_OWNER_STAFF")
			def integratorStaffRole = Role.findByAuthority("ROLE_INTEGRATOR_STAFF")

			def isStaff = user.authorities?.contains(ownerStaffRole) || user.authorities?.contains(integratorStaffRole)
			def temp = null
			if (isStaff){
				temp = Company.getEnabledCompanies(user)
				companies = temp.listDistinct()
			}else{
				temp = Company.getAllCompanies(user)
				companies = temp.listDistinct()
			}

			def integrators = temp.typeIntegrator.list()
			for(i in integrators){
				CompanyIntegrator integrator = CompanyIntegrator.findByCompany(i)
				companies.addAll(integrator.assignedCompanies.toList())
			}
		}
		return companies.unique()
	}
	
	/**
	 * Determines if current user has HELP_DESK role
	 * @return
	 */
	private boolean isUserRoleHelpDesk(){		
		return userHasRole(RoleEnum.HELP_DESK.value);
	}	
	
	/**
	 * Determines if current user has HELP_DESK role
	 * @return
	 */
	protected boolean isUserRoleAdmin(){
		
		return userHasRole(RoleEnum.ADMIN.value);
		//def roleAdmin = Role.findByAuthority(RoleEnum.ADMIN.value)
		//return getCurrentUser().authorities?.contains(roleAdmin)
	}
	
	/**
	 * Determines if current user has HELP_DESK role
	 * @return
	 */
	private boolean isHelpDesk(){
		return userHasRole(RoleEnum.HELP_DESK.value);
	}
	
	/**
	 * Determines if current user has HELP_DESK role
	 * @return
	 */
	protected boolean isAdmin() {
		return userHasRole(RoleEnum.ADMIN.value);
	}
	
	protected boolean isAdminOrHelpDesk(){
		return (userHasRole(RoleEnum.ADMIN.value) || userHasRole(RoleEnum.HELP_DESK.value));
	}
	
	private boolean userHasRole(String roleToCheck){
		def roles = springSecurityService.getPrincipal().getAuthorities();
		for(role in roles){
			if(role != null && role.equals(roleToCheck)) {
				return true;
			}
		}
		return false;
	}
	
	
	
	/**
	 * Determines if current user has OWNER_STAFF role
	 * @return
	 */
	private boolean isUserRoleOwnerStaff(){
		def ownerStaffRole = Role.findByAuthority(RoleEnum.OWNER_STAFF.value)		
		return  getCurrentUser().authorities?.contains(ownerStaffRole)
	}
	
	private def createMessageJsonWithError(String status) {
		return 		'''{
						"status": "",
						"errorCode": "0",						
						"successful": false 					
	 				  }'''			 	 
	}
	
	private def createMessageJsonSucessful() {
		return 		'''{
						"status": "",
						"errorCode": "0",
						"successful": true
	 				  }'''
	}
	
	private def createMessageJsonWithErrorWithMessage(String status, String message) {
		return 		"{" +
				   " \"status\": \" " + status +" \" ," +
				   " \"errorCode\": \"0\", " +
				   " \"successful\": false ," +
				   " \"message\": \" " + message +  " \" " +
				  "}"
							
	}


	
	private def createMessageJsonSucessfulWithMessage(def message) {
		return 		"{" +
		" \"status\": \"\", " +
		" \"errorCode\": \"0\", " +
		" \"successful\": true ," +
		" \"message\": \" " + message +  " \" " +
		"}"
	}
	
	protected getCompanyTypesOfAffiliate(){
		if(session.affiliate == null){
			session.affiliate = Affiliate.findByCode(PartnerEnum.getDefaultPartnerCode());
		}
		Affiliate aff = Affiliate.read(session.affiliate.id);
		String str = (aff && aff.associateEstablishments)? 
						aff.associateEstablishments:"";
		String [] establishments = str.split(',');
		
		List<Long> estList = new ArrayList<Long>();
		for (int i = 0; i < establishments.length; i++) {
			estList.add(new Long(Long.parseLong(establishments[i])));
		}
		
		//return CompanyCatalogType.list();
		return CompanyCatalogSubType.findAllByIdInList(estList)
	}
	
	/**
	 * 
	 * @return
	 */
	protected getAffiliateListByUser(){
		if(session.affiliate == null){
			session.affiliate = Affiliate.findByCode(PartnerEnum.getDefaultPartnerCode());
		}
		if(session.affiliate.code == PartnerEnum.getDefaultPartnerCode()){
			//If it's default must return All the Affiliates
			return Affiliate.findAll();
		} else {
			//If it's not default must return only the associate affiliate
			return Affiliate.findAllById(new Long(session.affiliate.id));
		}		
	}
	
	/**
	 * This method will return the current Affiliate according
	 * to the session and the request url; if the session dont have a 
	 * associate Affiliate will set the Default by Code and return that one
	 * 
	 * @return	Affiliate
	 */
	protected getCurrentSessionAffiliate(){
		if(session.affiliate == null){
			session.affiliate = Affiliate.findByCode(PartnerEnum.getDefaultPartnerCode());
		}
		return session.affiliate;
	}
	
	/**
	 * This method will try to upload an Image calling the crop method
	 * and validating the dimensions on backend.
	 * 
	 * @param fieldName			Field name for error message
	 * @param imgFile			Field from the Browser
	 * @param fileId			Id of the Field used to get the extra parameters
	 * @param requiredWidth		Required Width
	 * @param requiredHeight	Required Height
	 * @return					Url when upload is success
	 */
	protected String uploadImageFile(String fieldName, String fileId,
		int requiredWidth, int requiredHeight){
		try{			
			if(params[fileId + '-updated'] == '1'){
				def imgFile = request.getFile(fileId);
				if(imgFile != null) {					
					ImageUploadResult result = rackspaceService.cropAndSaveImage(imgFile, , params['x-' + fileId], params['y-' + fileId],
						params['w-' + fileId], params['h-' + fileId],
						requiredWidth, requiredHeight);
					return result.getImageUrl();
				}
			} else {
				return null;
			}
		}catch(Exception e){
			if(e.getMessage().indexOf("Image Dimensions are wrong") > -1){
				flash.error = message(code:'image.field.error.with.dimensions', args:[requiredWidth, requiredHeight, fieldName]);
			} else {
				flash.error = e.getMessage();
			}
			throw e;
		}
	}
		
	/**
	 * This method will try to upload an Image calling the crop method
	 * and validating the dimensions on backend.
	 *
	 * @param fieldName			Field name for error message
	 * @param imgFile			Field from the Browser
	 * @param fileId			Id of the Field used to get the extra parameters
	 * @param requiredWidth		Required Width
	 * @param requiredHeight	Required Height
	 * @return					Url when upload is success
	 */
	protected ImageUploadResult uploadImageFileUsingResult(String fieldName, String fileId,
		int requiredWidth, int requiredHeight){
		try{
			if(params[fileId + '-updated'] == '1'){
				def imgFile = request.getFile(fileId);
				if(imgFile != null) {
					ImageUploadResult result = rackspaceService.cropAndSaveImage(imgFile, , params['x-' + fileId], params['y-' + fileId],
						params['w-' + fileId], params['h-' + fileId],
						requiredWidth, requiredHeight);
					return result;
				}
			} else {
				return null;
			}
		}catch(Exception e){
			if(e.getMessage().indexOf("Image Dimensions are wrong") > -1){
				flash.error = message(code:'image.field.error.with.dimensions', args:[requiredWidth, requiredHeight, fieldName]);
			} else {
				flash.error = e.getMessage();
			}
			throw e;
		}
	}
		
	/**
	 * Verifies that all the images are available
	 * @param fileIds
	 * @return
	 */
	protected boolean areAllImages(String[]fileIds){
		for(int i=0; i < fileIds.size(); i++){
			if(params[fileIds[i] + '-updated'] == '0'){
				return false;
			}
		}
		return true;
	}
}