package mycentralserver

import mycentralserver.docs.DocCategory;
import mycentralserver.utils.Constants;
import mycentralserver.utils.Utils;
import mycentralserver.user.User;



/**
 * Controller that handles the related functionalities of the Documents Categories
 *
 * @author      Cecropia Solutions
 * @date     	08/18/2014
 */
class DocCategoryController extends BaseControllerMyCentralServerController {

    /**
     * Renders the page with the list of categories of documents
     */
    def listAll() {
        def categoriesList = Utils.sortByName(DocCategory.list());
        if(categoriesList.size() > 0) {
            [list: categoriesList]
        } else {
            render(view: "emptyList")
        }
    }

	/**
	 * Renders the page that allow the creation of a new Category
	 *
	 * @return
	 */
	def create() {
		[entity: new DocCategory()]
	}
	
	/**
	 * Receives the parameter for a new Skin, save the entity to the
	 * database and upload the images.
	 *
	 * @return
	 */
	def save()
	{
		
		if ( params.size() > 2) { //if the only values are action and controller, then we are not coming from the form
			User user = getCurrentUser();
			def DocCategory
			try
			{
				DocCategory = new DocCategory(params)
				DocCategory.createdBy = user
				DocCategory.lastUpdatedBy = getCurrentUser()
				
				
				if(DocCategory.validate()) {
					
					DocCategory.withTransaction { status ->
						try {
							//Checks if the ads background file exists and upload if must
														
							DocCategory.save();
							flash.success = message(code: 'default.message.success');
							
						} catch(Exception dbEx)	{
							log.error "Error Saving a new Doc Category: ${dbEx.message}", dbEx
							status.setRollbackOnly()
							flash.error = (dbEx.getMessage())?dbEx.getMessage():message(code: 'doc.error.saving')
						}
					}
					
				} else {
					flash.error = message(code: 'default.error.problem')
				}
				
			} catch( Exception ex)
			{
				flash.error=ex
				log.error ex
				ex.printStackTrace()
			}
			
			if(flash.error != null){
				render(view: Constants.VIEW_CREATE, model: [entity: DocCategory])
				return
			}else{
				redirect(action:Constants.VIEW_LISTALL, id : DocCategory.id)
				return
			}
		}else{
			flash.error = message(code: 'default.not.found.message')
			redirect(action: Constants.VIEW_LISTALL)
			return
		}
	} // END save
	
	def show(long id)
	{
		def offset = this.getOffsetParameterToPagination();
		def docCategory=DocCategory.read(id);
	
		if (!docCategory) {
			//If the AppSkin don't exists redirect to the list with the error message
			flash.error = message(code: 'doc.not.found');
			redirect(action: Constants.VIEW_LISTALL)
		} 
		[entity: docCategory]
	}
	
	/**
	 * Renders the page to allow the edition of a AppSkin
	 *
	 * @param id	Id of the AppSkin to be edited.
	 *
	 * @return
	 */
	def edit(long id)
	{
		def docCategory=DocCategory.read(id)
		def offset = this.getOffsetParameterToPagination()
		
		if (!docCategory ) {
			//If the AppSkin do not exists or don't has access
			flash.error = message(code: 'doc.not.found')
			redirect(action:Constants.VIEW_LISTALL)
		}else{
			[entity: docCategory]
		}
	}
		
	def update(long id, Long version)
	{
		def jsonTransactionStatus = null
		def offset = this.getOffsetParameterToPagination()
		
		if ( params.size() > 3) { //if the only values are action and controller, then we are not coming from the form
			
			def docCategory=null;
			
			try {
				docCategory = DocCategory.read(id);
					
					docCategory.properties = params
					docCategory.lastUpdatedBy = getCurrentUser()
															
					if(!docCategory.hasErrors() && docCategory.validate()  )
					{
						
						docCategory.withTransaction { status ->
							try {
								if (docCategory.save()){
									
									flash.success=message(code: 'default.message.success');
																	
								}else{
									flash.error=message(code: 'default.error.problem')
								}
							}catch(Exception dbEx) {
								flash.error = dbEx.getMessage()
								status.setRollbackOnly()
							}
							
						}
						
					}
					else
					{
						flash.error=message(code: 'default.error.problem')
					}
					
			} catch(Exception ex) {
				ex.printStackTrace()
				flash.error=message(code: 'default.error.problem')
			}
			
			if(flash.error != null){
				render(view: Constants.VIEW_EDIT, model:[entity: docCategory])
			}else{
				//render(view:Constants.VIEW_EDIT_RESULTS, model:[jsonTransactionStatus:jsonTransactionStatus])
				redirect(action: Constants.VIEW_LISTALL, params: [offset: offset])
			}
		}else{
			flash.error = message(code: 'doc.not.found')
			redirect(action: Constants.VIEW_LISTALL, params: [offset: offset])
		}
	}
	
	/**
	 * Deletes an AppSkin
	 *
	 * @param id	Id of the AppSkin to be deleted.
	 *
	 * @return
	 */
	def delete(long id)
	{
		try {
			def docCategory=DocCategory.read(id)
			def offset = this.getOffsetParameterToPagination()
			
			if (!docCategory ) {
				//If the AppSkin do not exists or don't has access
				flash.error = message(code: 'doc.not.found')
			} else {
				if(docCategory.documents.size() > 0){
					//Return an error because cannot delete Categories with documents
					flash.error = message(code:'doc.category.delete.error.because.documents');
				} else {
					docCategory.delete();
					//If the object is deleted from DB, delete the associated images
					flash.success = message(code: 'default.doc.remove.success');
				}
			}
		} catch(Exception e){
			log.error("Error deleting the Document with id=" + id + ": ${e.message}", e);
			flash.error = message(code:'default.error.problem');
		}
		
		redirect(action:Constants.VIEW_LISTALL);
	}
	
	
}
