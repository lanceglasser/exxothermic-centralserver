package mycentralserver
import grails.converters.JSON

import javax.servlet.http.HttpServletResponse

import mycentralserver.user.User;

import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils

import org.springframework.security.authentication.AccountExpiredException
import org.springframework.security.authentication.CredentialsExpiredException
import org.springframework.security.authentication.DisabledException
import org.springframework.security.authentication.LockedException
import org.springframework.security.core.context.SecurityContextHolder as SCH
import org.springframework.security.web.WebAttributes
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter

import mycentralserver.utils.PasswordGeneratorHelper;

class LoginController {

	/**
	 * Dependency injection for the authenticationTrustResolver.
	 */
	def authenticationTrustResolver

	/**
	 * Dependency injection for the springSecurityService.
	 */
	def springSecurityService
	
	def mailService
	def affiliateService
	
	def userService
	
	/**
	 * Default action; redirects to 'defaultTargetUrl' if logged in, /login/auth otherwise.
	 */
	def index = {
		if (springSecurityService.isLoggedIn()) {
			redirect uri: SpringSecurityUtils.securityConfig.successHandler.defaultTargetUrl
		}
		else {
			redirect action: 'auth', params: params
		}
	}

	/**
	 * Show the login page.
	 */
	def auth = {
		
		def config = SpringSecurityUtils.securityConfig

		if (springSecurityService.isLoggedIn()) {
			redirect uri: config.successHandler.defaultTargetUrl
			return
		}

		String view = 'auth'
		String postUrl = "${request.contextPath}${config.apf.filterProcessesUrl}"
		render view: view, model: [postUrl: postUrl,
		                           rememberMeParameter: config.rememberMe.parameter]
	}

	/**
	 * The redirect action for Ajax requests.
	 */
	def authAjax = {	
		response.setHeader 'Location', SpringSecurityUtils.securityConfig.auth.ajaxLoginFormUrl
		response.sendError HttpServletResponse.SC_UNAUTHORIZED
		response.status = HttpServletResponse.SC_UNAUTHORIZED 
	}

	/**
	 * Show denied page.
	 */
	def denied = {
		if (springSecurityService.isLoggedIn() &&
				authenticationTrustResolver.isRememberMe(SCH.context?.authentication)) {
			// have cookie but the page is guarded with IS_AUTHENTICATED_FULLY
			redirect action: 'full', params: params
		}
	}

	/**
	 * Login page for users with a remember-me cookie but accessing a IS_AUTHENTICATED_FULLY page.
	 */
	def full = {
		def config = SpringSecurityUtils.securityConfig
		def  postUrl =  "${request.contextPath}${config.apf.filterProcessesUrl}"
		render view: 'auth', params: params,
			model: [hasCookie: authenticationTrustResolver.isRememberMe(SCH.context?.authentication),
			        postUrl: "${request.contextPath}${config.apf.filterProcessesUrl}"]
	}

	/**
	 * Callback after a failed login. Redirects to the auth page with a warning message.
	 */
	def authfail = {

		def username = session[UsernamePasswordAuthenticationFilter.SPRING_SECURITY_LAST_USERNAME_KEY]
		String msg = ''
		def exception = session[WebAttributes.AUTHENTICATION_EXCEPTION]
		def isPasswordExpired = false
		
		if (exception) {
			if (exception instanceof AccountExpiredException) {
				msg = g.message(code: "springSecurity.errors.login.expired")
			}
			else if (exception instanceof CredentialsExpiredException) {
				msg = g.message(code: "springSecurity.errors.login.passwordExpired")
				isPasswordExpired = true
			}
			else if (exception instanceof DisabledException) {
				msg = g.message(code: "springSecurity.errors.login.disabled")
			}
			else if (exception instanceof LockedException) {
				msg = g.message(code: "springSecurity.errors.login.locked")
			}
			else {
				msg = g.message(code: "springSecurity.errors.login.fail")
			}
		}

		if (springSecurityService.isAjax(request)) {
			render([error: msg] as JSON)
		}
		else {
			flash.message = msg
			
			if(isPasswordExpired) {
				redirect action: 'changePassword', params: params
			} else {
				redirect action: 'auth', params: params
			}
		}
	}
	
	/**
	 * The Ajax success redirect url.
	 */
	def ajaxSuccess = {
		render([success: true, username: springSecurityService.authentication.name] as JSON)
	}

	/**
	 * The Ajax denied redirect url.
	 */
	def ajaxDenied = {
		render([error: 'access denied'] as JSON)
	}
	
	def forgotPassword(){}
	
	def doResetPassword(){
		def email = params.username
		def user = User.findByEmail(email)
		def error = false;
		def msg = "";
		
		if(user){//Check if user exists
			
			if(user.resetPasswordToken == null || user.resetPasswordTime == null || System.currentTimeMillis() - user.resetPasswordTime.time > (30*60*1000)) {
				//if(user.resetPasswordToken != null && user.resetPasswordToken != "" && user.resetPasswordTime != null && )
				
				//def newPassword = PasswordGeneratorHelper.generatePassword()
				//user.password = newPassword
				//user.passwordExpirationDate = userService.getPassswordExpirationDateForNewAccount()
				user.resetPasswordTime = new Date()
				user.resetPasswordToken = PasswordGeneratorHelper.generateTokenForPasswordReset()
				
				if(user.validate()){
										
					User.withTransaction { status ->
						try{
							
							user.save()
							//Send Email
							mailService.sendMail {
								from	affiliateService.getCurrentAffiliateConcatEmail(session.affiliate)
								to  	user.email
								subject message(code: 'default.home.forgot.password.email.subject')
								//body 'Your account has been created: ' + user.email
								html 	g.render(template:"/templates/emails/passwordResetRequestTemplate", model:[user:user])
							 }
							
							msg = message(code: 'default.home.forgot.password.success.request')
							error = false
						}catch(Exception dbEx){
							dbEx.printStackTrace()
							status.setRollbackOnly()
							error = true
							msg = dbEx.getMessage()
						}
					}
				}else{
					error = true
					msg = message(code: 'user.error.saving')
				}
			} else {
				error = true
				msg = message(code: 'user.forgotPassword.pending.request')
			}
		} else {
			error = true
			msg = message(code: 'default.home.forgot.password.user.not.found')
		}
		
		render([error: error, msg: msg] as JSON)
	}
	
	def resetMyPassword(){
		
		if(params.token){
			def user = User.findByResetPasswordToken(params.token)
			if(user){
				if(user.resetPasswordTime != null && System.currentTimeMillis() - user.resetPasswordTime.time < (24*60*60*1000)) {//Only valid for 24 hours
					
					def newPassword = PasswordGeneratorHelper.generatePassword()
					user.password = newPassword
					user.passwordExpirationDate = userService.getPassswordExpirationDateForNewAccount()
					user.resetPasswordTime = null
					user.resetPasswordToken = null
					
					if(user.validate()){
											
						User.withTransaction { status ->
							try{
								
								user.save()
										
								//Send Email
								mailService.sendMail {
									from	affiliateService.getCurrentAffiliateConcatEmail(session.affiliate)
									to  	user.email
									subject message(code: 'default.home.forgot.password.email.subject')
									//body 'Your account has been created: ' + user.email
									html 	g.render(template:"/templates/emails/passwordResetTemplate", model:[user: user, password: newPassword])
								 }
								
								flash.success = message(code: 'default.home.forgot.password.success.update')
							}catch(Exception dbEx){
								dbEx.printStackTrace()
								
								status.setRollbackOnly()
								flash.error=dbEx.getMessage()
							}
						}
					} else {
						flash.error=message(code: 'user.error.saving')
					}
				} else {
					flash.error =  message(code: 'default.home.forgot.password.invalid.token.expires')
				}
			} else {
				flash.error =  message(code: 'default.home.forgot.password.invalid.token.not.exists')
			}
		} else {
			redirect(action: 'auth')
		}
		
		if(flash.error) {
			redirect(action: 'forgotPassword')
		} else {
			redirect(action: 'auth')
		}
	}
	
	def changePassword() {
		
	}
	
	def doChangePassword() {		
		
		def email = params.username
		def oldPassword = params.password
		def newPassword = params.newPassword
		def confirmPassword = params.confirmPassword
		
		if(oldPassword)
		{
			oldPassword = springSecurityService.encodePassword(oldPassword)
		}
		
		def user = User.findByEmail(email)
		user = User.read(user.id)
		
		if(oldPassword != user.getPassword()) {
			flash.error = message(code: 'springSecurity.errors.login.fail')
			user.errors.rejectValue("password", 'user.password.incorrect')
		} else {
					
			if (newPassword != confirmPassword)
			{
				flash.error= message(code: 'user.error.saving')
				user.errors.rejectValue("password", 'user.newPassword.doesnotmatch')
			}else{
				 
				user.password = newPassword
				user.passwordExpired= false
				user.passwordExpirationDate =  userService.getPasswordExpirationDateForExistAccount()
				
				if(user.validate()){
					//to encrypt password
					user.setNewPassword(params.newPassword == null ? "" : params.newPassword)
					
					if(user.save(failOnError:true)){
											
						flash.success = message(code: 'user.password.updated.success')
						
					}else{
						flash.error = message(code: 'user.error.saving')
					}
				}else{
					flash.error = message(code: 'user.error.saving')
				}
			}
		}
		
		if(flash.error) {
			render(view: 'changePassword', model :[user:user])
		} else {
			redirect(action: 'auth')
		}
		return
	} // END doChangePassword
}
