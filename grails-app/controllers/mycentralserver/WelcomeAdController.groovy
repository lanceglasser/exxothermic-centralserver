package mycentralserver

import mycentralserver.app.AppSkin;
import mycentralserver.app.WelcomeAd;
import mycentralserver.app.WelcomeAd.WelcomeType;
import mycentralserver.beans.BoxesSyncResult;
import mycentralserver.user.User;
import mycentralserver.utils.Utils;
import mycentralserver.company.Company;
import mycentralserver.company.CompanyLocation;
import mycentralserver.utils.DigestHelper;
import mycentralserver.utils.FileHelper;
import mycentralserver.utils.Constants;
import mycentralserver.utils.ValidationHelper;

import java.io.File;

import org.apache.commons.lang.StringUtils;

import grails.converters.JSON;

/**
 * Controller that handles the related functionalities of the Welcome Ads
 *
 * @author      Cecropia Solutions
 * @date     	08/18/2014
 */
class WelcomeAdController extends BaseControllerMyCentralServerController {

    /* Inject required services */
    def rackspaceService
    def welcomeAdService
    def imageUploadService
    def userSessionService

    private final static int DEFAULT_MIN_SKIP_VALUE = 10;

    /**
     * Gets the list of associate Welcome Ad of the Current User and render the list
     * page view or the No Skins View page.
     */
    def listAll() {
        renderListViewWithSyncResult(null);
    }

    /**
     * Receives the result of a Sync and renders the list page with the required objects
     *
     * @param boxesSyncResult
     *                 Object with the result of the synchronization
     */
    def renderListViewWithSyncResult(def boxesSyncResult){
        def entityList = getListOfCurrentUser();
        if (entityList.size() > 0) {
            def messageErrorDescriptionHash  = messageErrorService.getMessageErrorDescription();
            render(view: Constants.VIEW_LISTALL, model:[ 
                list: Utils.paginateList(entityList, params),
                boxesSyncResult: boxesSyncResult,
                messageErrorDescriptionHash: messageErrorDescriptionHash]);
        } else {
            render(view: 'emptyList');
        }
    }

    /**
     * Renders the page that allow the creation of a new Welcome Ad
     */
    def create() {
        [entity: new WelcomeAd()]
    }

    /**
     * Receives the parameter for a new Welcome Ad, save the entity to the database and upload the images.
     */
    def save() {
        User user = userSessionService.getCurrentUser();
        WelcomeAd entity;
        def deleteWhenFails = [];
        try {
            if (ValidationHelper.isValidInteger(params.skipTime)) {
                entity = new WelcomeAd(params);
                entity.createdBy = user;
                entity.lastUpdatedBy = user;
                entity.welcomeType = (params.welcomeType.id.equals(Constants.PARAM_VIDEO))? 
                                            WelcomeAd.WelcomeType.VIDEO:WelcomeAd.WelcomeType.IMAGE;
                if (entity.validate()) {
                    WelcomeAd.withTransaction { status ->
                        try {
                            //Checks if must upload some files
                            if (entity.welcomeType == WelcomeAd.WelcomeType.IMAGE) {
                                String[] fileIds = [Constants.PARAM_SMALL_IMAGE_URL_FILE];
                                if (!params.genImgForTablets) {
                                    fileIds = [Constants.PARAM_SMALL_IMAGE_URL_FILE, 
                                        ImageUploadService.PARAM_LARGE_IMAGE_URL_FILE];
                                }
                                if (areAllImages(fileIds)) {
                                    imageUploadService.uploadImagesForWelcomeAd(entity, null, deleteWhenFails, params, request, flash);
                                } else {
                                    flash.error = message(code:'all.images.required.error');
                                }
                            } else {
                                if (params[Constants.PARAM_WELCOME_VIDEO_URL_UPDATED].equals(Constants.PARAM_UPDATED_VALUE)) {
                                    def welcomeVideoFile = request.getFile(Constants.PARAM_WELCOME_FILE_VIDEO);
                                    if(welcomeVideoFile != null && !welcomeVideoFile.isEmpty() &&
                                            !welcomeVideoFile.getOriginalFilename().isEmpty()) {
                                        final String url = rackspaceService.uploadFile(welcomeVideoFile);
                                        entity.videoUrl =  url;
                                        entity.videoFileName = params[Constants.PARAM_FAKEUPLOAD];
                                    }
                                } else {
                                    flash.error = message(code:'invalid.mp4.file');
                                }
                            }
                            if (flash.error == null) {
                                entity.save();
                                flash.success = message(code: 'default.message.success');
                            }
                        } catch(Exception dbEx) {
                            if(flash.error == null || flash.error.trim() == ""){
                                log.error "Error Saving a new Welcome Ad: ${dbEx.message}", dbEx
                                flash.error = (dbEx.getMessage())?dbEx.getMessage():message(code: 'default.error.box.software.savingTheFile');
                            }
                            status.setRollbackOnly();
                        }
                    }
                } else {
                    flash.error = message(code: 'validation.error')
                }
            } else {
                params.skipTime = DEFAULT_MIN_SKIP_VALUE;
                entity = new WelcomeAd(params);
                flash.error = message(code: 'validation.error');
                entity.errors.rejectValue('skipTime', message(code:'invalid.skip.time'));
            }
        } catch( Exception ex) {
            if ( StringUtils.isBlank(flash.error)) {
                flash.error=ex.getMessage();
                log.error("Error saving new Welcome Ad", ex);
            }
        }

        if (flash.error != null) {
            entity.smallImageUrl = null;
            entity.mediumImageUrl = null;
            entity.largeImageUrl = null;
            entity.videoUrl =  null;
            entity.videoFileName = "";
            rackspaceService.deleteMultipleFiles(deleteWhenFails);
            render(view: Constants.VIEW_CREATE, model: [entity: entity]);
        } else {
            redirect(action:Constants.VIEW_ASSIGN_LOCATION, id : entity.id);
        }
    }

    /**
     * Renders the view that shows all the fields of the Welcome Ad
     * 
     * @param id
     *             Id of the WelcomeAd to be render
     */
    def show(long id) {
        final WelcomeAd entity = WelcomeAd.read(id);
        if (entity) {
            if (checkAccess(entity)) { // If the User can see the WelcomeAd renders the view page
                [entity: entity]
            } else { // If the User cannot see the Welcome Ad redirect to the list with the error message
                flash.error = message(code: 'general.object.not.found');
                redirect(action: Constants.VIEW_LISTALL);
            }
        } else { // If the Entity don't exists redirect to the list with the error message
            flash.error = message(code: 'general.object.not.found');
            redirect(action: Constants.VIEW_LISTALL);
        }
    }

    /**
     * Deletes a WelcomeAd and renders the result
     *
     * @param id
     *             Id of the Welcome Ad to be deleted
     */
    def delete(long id) {
        final WelcomeAd entity = WelcomeAd.read(id);
        if (entity && checkAccess(entity) ) {
            final String[] imagesUrls = [entity.smallImageUrl, entity.mediumImageUrl, entity.largeImageUrl, entity.videoUrl];
            def locations = entity.locations;
            //Set all associated location boxes to the default
            BoxesSyncResult boxesSyncResult = null;
            if(!locations.isEmpty()) {
                boxesSyncResult = welcomeAdService.updateBoxesToDefaultOfLocations(locations);
                if (boxesSyncResult == null) {
                   flash.warn = message(code: 'general.no.communication.with.web.socket.api');
                } else {
                   if (boxesSyncResult.getErrorCounter() > 0){
                       flash.warn = message(code: 'boxes.syncErrorMessage');
                   }
                }
            }
            entity.delete();
            flash.success = message(code: 'welcome.ad.remove.success');
            // If the object is deleted from DB, delete the associated images
            rackspaceService.deleteMultipleFiles(imagesUrls);
            renderListViewWithSyncResult(boxesSyncResult);
        } else { // If the Entity do not exists or don't has access
            flash.error = message(code: 'general.object.not.found');
            redirect(action:Constants.VIEW_LISTALL);
        }
    }

    /**
     * Renders the page to allow the edition of a WelcomeAd
     * 
     * @param id
     *             Id of the WelcomeAd to be edited
     */
    def edit(long id) {
        final WelcomeAd entity = WelcomeAd.read(id);
        if (entity && checkAccess(entity) ) {
            [entity: entity]
        } else { // If the Entity do not exists or don't has access
            flash.error = message(code: 'general.object.not.found');
            redirect(action:Constants.VIEW_LISTALL);
        }
    }

    /**
     * This method will update the information of a Welcome Ad and will sends the data to the associated Boxes
     * 
     * @param id
     *             Id of the Welcome Ad to be updated
     * @param version
     *             Version for UI control
     */
    def update(long id, Long version) {
        BoxesSyncResult boxesSyncResult = null;
        WelcomeAd entity = null;
        WelcomeAd originalEntity;
        Map<String, String> originalImages = new HashMap<String, String>();
        def imagesUrlsToDelete = [];
        def deleteWhenFails = [];
        try {
            entity = WelcomeAd.read(id);
            originalImages.put("smallImageUrl",entity.smallImageUrl);
            originalImages.put("largeImageUrl",entity.largeImageUrl);
            originalImages.put("videoFileName",entity.videoFileName);

            if (entity.version > version) {
                flash.error = message(code: 'default.skin.edit.version.error');
                entity.errors.rejectValue("version", 'default.skin.edit.version.error');
            } else {
                if (ValidationHelper.isValidInteger(params.skipTime) ) {
                    entity.properties = params;
                    entity.lastUpdatedBy = userSessionService.getCurrentUser();
                    entity.welcomeType = (params.welcomeType.id == Constants.PARAM_VIDEO)?
                            WelcomeAd.WelcomeType.VIDEO:WelcomeAd.WelcomeType.IMAGE;
                    if ( !entity.hasErrors() && entity.validate() ) {
                        WelcomeAd.withTransaction { status ->
                            try {
                                if (entity.save()) {
                                    // Checks if needs to upload a file
                                    def welcomeImgFile = null;
                                    if(entity.welcomeType == WelcomeAd.WelcomeType.VIDEO){
                                        if(params['welcomeVideoUrl-updated'] == '1'){
                                            rackspaceService.deleteFile(entity.videoUrl);
                                            welcomeImgFile = request.getFile(Constants.PARAM_WELCOME_FILE_VIDEO);
                                            if(welcomeImgFile != null) {
                                                String url = rackspaceService.uploadFile(welcomeImgFile);
                                                entity.videoUrl =  url;
                                                entity.videoFileName = params['fakeupload'];
                                            }
                                        }
                                    }
                                    if(entity.welcomeType == WelcomeAd.WelcomeType.IMAGE){
                                        imageUploadService.uploadImagesForWelcomeAd(entity, imagesUrlsToDelete, deleteWhenFails, params, request, flash);
                                    }
    
                                    entity.save();
    
                                    //Delete the old images from container
                                    rackspaceService.deleteMultipleFiles(imagesUrlsToDelete);
    
                                    def locations = entity.locations;
    
                                    if(!locations.isEmpty()) {
                                        flash.success = message(code: 'save.object.success');
                                        boxesSyncResult = welcomeAdService.updateBoxesOfLocations(locations, entity);
                                        if(boxesSyncResult == null) {
                                            flash.warn = message(code: 'general.no.communication.with.web.socket.api');
                                        } else {
                                            if(boxesSyncResult.getErrorCounter() > 0){
                                                flash.warn = message(code: 'boxes.syncErrorMessage');
                                            }
                                        }
                                    } else {
                                        //There are not Locations associated
                                        flash.success = message(code: 'sync.no.affected.locations');
                                    }
                                }else{
                                    flash.error=message(code: 'default.error.problem')
                                }
                            }catch(Exception dbEx) {
                                if(flash.error == null || flash.error.trim() == ""){
                                    flash.error = dbEx.getMessage();
                                    log.error("Error updating an Welcome Ad", dbEx);
                                }
                                status.setRollbackOnly();
                            }
                        }
                    } else { // Entity has errors
                        flash.error = message(code: 'validation.error');
                    }
                } else { // Invalid MinSkip value
                    params.skipTime = DEFAULT_MIN_SKIP_VALUE;
                    entity.properties = params;
                    flash.error = message(code: 'validation.error');
                    entity.errors.rejectValue('skipTime', message(code:'invalid.skip.time'));
                }
            }
        } catch(Exception ex) {
            log.error("Error updating a WelcomeAd", ex);
            if (StringUtils.isBlank(flash.error)) {
                flash.error=message(code: 'default.error.problem')
            }
        }

        def messageErrorDescriptionHash = messageErrorService.getMessageErrorDescription();
        if (flash.error == null) {
            rackspaceService.deleteMultipleFiles(imagesUrlsToDelete);
            renderListViewWithSyncResult(boxesSyncResult);
        } else {
            entity.smallImageUrl = originalImages.get("smallImageUrl");
            entity.largeImageUrl = originalImages.get("largeImageUrl");
            entity.videoFileName = originalImages.get("videoFileName");
            rackspaceService.deleteMultipleFiles(deleteWhenFails);
            render(view: Constants.VIEW_EDIT, model: [entity: entity, boxesSyncResult: boxesSyncResult, messageErrorDescriptionHash: messageErrorDescriptionHash]);
        }
    }
	
	/**
	 * Returns interface for assigning locations to a specific entity
	 * 
	 * @param id
	 * @return
	 */
	def assignLocation(Long id){
		
		def entityList = getListOfCurrentUser();
		
		if (entityList.size() > 0) {
			def entity = null;
			if(id != null) {
				entity = WelcomeAd.read(id)
			} else {
				entity = entityList.get(0)
			}
			[entity: entity, entityList:Utils.sortByName(entityList)]
		} else {
			render(view: 'emptyList')
		}
	}
	
	def getAssociatedLocationsOfWelcomeAd(){
		if(params.action != null && params.action == "getAssociatedLocationsOfWelcomeAd"){
			//Verify that the action is the same metthod; do nothing when not
			if(params.entityId){
				def entity = WelcomeAd.read(params.entityId);
				
				if (!entity) {
					flash.error = message(code: 'general.object.not.found');
				}else{
					def listCompanies = getCompaniesForAssociation(entity)
								
					if(!checkAccess(entity)){
						//flash.error = message(code: 'default.not.found.message', args: [message(code: 'userInformation.label', default: 'User Information'), params.buttonId])
						render(text: '<div class="alert alert-error">'
							+ message(code: 'general.object.not.found', args: [message(code: 'userInformation.label', default: 'User Information'), params.entityId])
							+ '</div>', contentType: "text/html", encoding: "UTF-8")
					}else{
					
						String selectedLocations =""
						for(location in entity.locations){
							selectedLocations += (selectedLocations.equals(""))? location.id:"," + location.id;
						}
							
						render (template:Constants.ASSIGN_SKIN_TEMPLATE, model:[entity:entity, companies: listCompanies, selectedLocations: selectedLocations])
					}
				}
			}  else {
				flash.error = message(code: 'general.object.not.found');
			}
		}
	}
	
	private getCompaniesForAssociation(WelcomeAd wa) throws Exception{
		if(wa.welcomeType == WelcomeType.VIDEO){
			// For Video Type only retrieve premium companies
			//Get the list of enabled Companies associated for current user role with enabled locations
			return userSessionService.getCompaniesForAssociation();
		} else {
			//For Image Type retrieve all companies
			return userSessionService.getCompaniesForAssociation(true);
		}		
	} // End of getCompaniesForAssociation method
	
	/**
	 * This method receives the information of the Locations that a Welcome Ad needs to be
	 * associated.
	 * 
	 * The method will synchronize all the locations and update the associated boxes with
	 * the corresponding Welcome Ad (Default if was removed).
	 * 
	 * @return
	 */
	def assign(){
		def entity = WelcomeAd.read(params.activeSkin);
		BoxesSyncResult boxesSyncResult = null;
		
		try {
			def paramLocations = params.list(Constants.PARAM_LOCATIONS);
			WelcomeAd.withTransaction { status ->
				try{
					def locationsToSave = (params.locationsToSave == null)? "":params.locationsToSave;
					def newList = locationsToSave.split(",");
					
					def locations = []
					locations += entity.locations
					
					//Will store all the Locations affected by the update
					def affectedLocations = []
					
					//Remove 						
					locations.each { location ->
						if(!newList.any { it == (""+location.id) }){
							entity.removeFromLocations(location)
							location.welcomeAd = null
							if(location.save()){
								affectedLocations.add(location);
							} else {
								log.warn location.errors.allErrors.join(' \n');
							}							
						}
					}
					
					//Add new ones
					for (c in newList){							
						if(!entity.locations.any { it.id == (c) }){
							CompanyLocation loc = CompanyLocation.read(c) 	
							if(loc != null){
								loc.welcomeAd = entity;
								if(loc.save()){
									entity.addToLocations(loc);
									affectedLocations.add(loc);
								} else {
									log.warn loc.errors.allErrors.join(' \n');
								}
							}
						}
					}
					
					if(entity.save()){//If not error saving the Object
						
						flash.success = message(code: 'default.upated.message', args: [entity.name]);
						
						if(!affectedLocations.isEmpty())
						{							
							boxesSyncResult = welcomeAdService.updateBoxesOfLocations(affectedLocations, entity);
							if(boxesSyncResult == null){
								//Api Communication error since not response
								flash.error = message(code: 'general.no.communication.with.web.socket.api')
							} else {
								//Api Communication works, check the result
								if(boxesSyncResult.getErrorCounter() > 0) {
									flash.warn = message(code:'boxes.syncErrorMessage');
								}
							}
						} else {//There are not associated locations
							flash.success = message(code: 'sync.no.affected.locations');
						}
					}else{
						flash.error= message(code: 'save.object.error');
					}
					//////////////
					/*if(entity.save()){//If not error saving the Object
						
						if(!locations.isEmpty())
						{
							boxesSyncResult = welcomeAdService.updateBoxesOfLocations(locations, entity);
							
							if(boxesSyncResult == null || boxesSyncResult.getErrorCounter() > 1 || boxesSyncResult.getTotalCounter() == 0) {
								flash.error = message(code: 'boxes.syncErrorMessage')
							} else {
								flash.success = message(code: 'default.upated.message', args: [entity.name])
							}
						} else {
							flash.success = message(code: 'default.upated.message', args: [entity.name])
						}
					}else{
						flash.error= message(code: 'default.not.upated.message', args: [entity.name])
					}*/
					/////////////////
				}catch(Exception dbEx){
					dbEx.printStackTrace()
					status.setRollbackOnly()
					flash.error=dbEx.getMessage()
				}
			}
				 			
		}catch(Exception e){
			e.printStackTrace()
			flash.error=e.getMessage()
		}
		
		def entityList = getListOfCurrentUser();
		
		def messageErrorDescriptionHash =  messageErrorService.getMessageErrorDescription();
		render (view:Constants.VIEW_ASSIGN_LOCATION,
			model:[entity: entity,  entityList:Utils.sortByName(entityList), boxesSyncResult:boxesSyncResult,
				messageErrorDescriptionHash: messageErrorDescriptionHash])
	}
	
	
	/**
	 * Check if the Current User can execute an action 
	 * with the Entity
	 * 
	 * @param entity	Entity to be checked
	 * 
	 * @return			True if it's allowed or False when it's not.
	 */
	def boolean checkAccess(entity){
		/*def relatedSkins = []
		
		for(company in listCompanies){
			if(company.enable){
				for(location in company.locations){
					if(location.enable) {
						relatedSkins += location.appSkins
					}
				}
			}
		}
		
		def user = getCurrentUser()
		for(userAppSkin in user.createdAppSkins){
			relatedSkins += userAppSkin
		}
		
		return (relatedSkins.any { it in (appSkin) })
		*/
		
		//def listCompanies = getCompaniesForUser()
		//if(!allowedAppSkingAction(listCompanies, appSkin)){
		return true;
	}
	
	private def getPermitedList(){
		//def companies = getCompaniesForUser()
		//def skins = AppSkin.getSkinsByCompanies(companies.collect(){it.id}, getCurrentUser()).listDistinct()
		return WelcomeAd.getFullListByUser(getCurrentUser()).list()
	}
	
	def getListOfCurrentUser(){
		def tempResults = null;
		def waList;
		if(isAdminOrHelpDesk()){
			//Admin will see All the List of Contents
			def query = WelcomeAd.where{
				};
			tempResults = query.list().unique{ it.id };
			waList = Utils.sortByName(tempResults);
		} else {
			//tempResults = Content.getContentsByCompanies(listCompanies.collect(){it.id}, getCurrentUser());
			waList = Utils.sortByName(WelcomeAd.getFullListByUser(getCurrentUser()).list())
		}
		
		return waList;
	}
}
