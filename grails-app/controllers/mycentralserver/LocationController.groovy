package mycentralserver

import mycentralserver.generaldomains.Country
import mycentralserver.generaldomains.State
import mycentralserver.company.Company
import mycentralserver.company.CompanyIntegrator
import mycentralserver.company.CompanyLocation
import mycentralserver.content.Content
import mycentralserver.generaldomains.CompanyType
import mycentralserver.user.User
import mycentralserver.user.Role
import mycentralserver.utils.RoleEnum
import mycentralserver.utils.CompanyTypeEnum
import mycentralserver.utils.Constants
import mycentralserver.utils.ValidationHelper;
import mycentralserver.company.CompanyCatalogSubType

import org.apache.commons.lang.StringUtils;
import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject
import org.springframework.web.servlet.FlashMap

import grails.plugins.springsecurity.Secured
import grails.plugins.springsecurity.SpringSecurityService
import mycentralserver.company.CompanyCatalogType
import grails.converters.JSON
import mycentralserver.utils.Utils
import mycentralserver.utils.entity.EntitiesCatalog.VenueProperties;
import mycentralserver.app.Affiliate;
import mycentralserver.app.AppSkin
class LocationController  extends BaseControllerMyCentralServerController {
	
    def rackspaceService
    def contentService
    def documentService
    def boxUserService
    def appSkinService
    def locationService;
    def pocService;
    def userSessionService;
	
	/**
	 * Renders the dashboard view of a Location
	 * @return
	 */
	def dashboard(long id) {
		
		try {
			CompanyLocation location = locationService.renderDashboardPage(id, flash);
			if(location) {
				[location: location]
			} else {
				redirect(action: Constants.VIEW_INDEX,  params: [])
			}
		} catch(Exception e){
			log.error("Error rendering the location dashboard", e);
		}
		
	} // End of dashboard method
	
    /**
     * Renders the creation page of a new Venue
     */
    def create() {	
		def location = new CompanyLocation();
		def countries = Country.findAll();
		def states = State.findAllByCountry(countries[0]);
		location.state = states[0];
		def typesCompany = getCompanyTypesOfAffiliate();
		def companies = getPermitedCompanies(true);
        if (companies.size() > 0) {
            [location:location, companies:Utils.sortByName(companies), countries:countries, typesCompany:typesCompany,
                states:states,comingFromWizard:false]
        } else {
            redirect (controller:Constants.CONTROLLER_COMPANY,action: Constants.VIEW_NOT_COMPANY);
        }
	}
	
	def ajaxUpdateLocationIntegrator(){
		
		def company = Company.read(params.companyId)	
		def systemIntegrator = CompanyIntegrator.read(params.integratorId)
		def integrators = CompanyIntegrator.getEnabledAndApprovedCompanies().listDistinct()
		def location = new CompanyLocation()
		
		def role = Role.findByAuthority(RoleEnum.INTEGRATOR.value())
		def isIntegrator = getCurrentUser().authorities?.contains(role)
		
		if(systemIntegrator == null) {
			systemIntegrator = company.companyWideIntegrator
			
			def equal = integrators.any { it == (systemIntegrator) }
			
			if ( company.companyWideIntegrator == null){
				if ( company.typeOfCompany.name.equals(CompanyTypeEnum.INTEGRATOR.value())){
					
					systemIntegrator = CompanyIntegrator.findByCompany(company)
					
				}else if (!integrators.any { it == (systemIntegrator) }){
					
					
					if (isIntegrator){
						def tempSearch = CompanyIntegrator.getByUserByTypeAndApproved(getCurrentUser(), CompanyType.findByName(CompanyTypeEnum.INTEGRATOR.value()))
						def integratorCompanies = tempSearch.listDistinct()
						
						if (integratorCompanies.size() > 0){
							systemIntegrator =  integratorCompanies.get(0)
						}
					}
				}
			} 
		}
		location.commercialIntegrator = systemIntegrator
		integrators = Utils.sortIntegratorByName(integrators)
		render(template:"integratorTemplate", model:[location:  location , integrators: integrators])
	}
			
	def getPermitedCompanies(boolean creation){
		def companies = null
		
		try{
			def user = getCurrentUser()			
			
			def helpDesk = Role.findByAuthority(RoleEnum.HELP_DESK.value)
			def isHelpDesk = user.authorities?.contains(helpDesk)		
										
			if (isHelpDesk){
				companies = Company.findAll(sort:Constants.NAME, order: Constants.SORT_ASC)			
			}else{
				
				def temp = null
				if (creation){
					temp = Company.getEnabledCompanies(user)
					companies = temp.listDistinct()
				}else{
					temp = Company.getAllCompanies(getCurrentUser())
					companies = temp.listDistinct()
				}
				 				
				def integrators = temp.typeIntegrator.listDistinct()	
				for(integrator in integrators){					
						integrator = CompanyIntegrator.findByCompany(integrator)
						companies.addAll(integrator.assignedCompanies.toList())											 
				}
			}
		}catch(Exception e){					
			e.printStackTrace()			
		}
		return companies.unique()
	}
	
    def save() {
        def deleteWhenFails = [];
        CompanyLocation location = new CompanyLocation();
        final boolean isValid = previousParamsValidation(location, true);
        try {
            if (isValid) {
                if (!location.country.hasStates()) {
                    location.state = null;
                }
                if (params.otherType == null || params.otherType.isEmpty() ||
                    (location.type.code != Constants.ESTABLISHMENT_TYPE_OTHER &&
                        location.type.code != Constants.ESTABLISHMENT_TYPE_WAITING_ROOM)) {
                    location.otherType = null;
                }
                final String url = uploadImageFile(message(code:'company.logo'), Constants.PARAM_BG_LOGOUT_URL,
                    Constants.COMPANY_LOGO_WIDTH, Constants.COMPANY_LOGO_HEIGHT);
                if (url != null) {
                    location.logoUrl = url;
                    deleteWhenFails.add(url);
                }
                if (location.save()) {
                    associateDefaultContents(location);
                }
                flash.success = message(code: 'default.message.success');
            }
        } catch( Exception ex) {
            if ( StringUtils.isBlank(flash.error)) {
                flash.error=ex.getMessage();
                log.error("Error saving new Location", ex);
            }
        }
        if (StringUtils.isBlank(flash.error)) {
            redirect(action: Constants.VIEW_LISTALL);
        } else {
            def countries = Country.list();
            //Get by default state and cities, maybe the default can will be in a config file
            def states = State.findAllByCountry(location.country);
            def companies = getPermitedCompanies(true);
            def typesCompany = getCompanyTypesOfAffiliate();
            render(view: Constants.VIEW_CREATE, model: [location: location, companies:companies, countries:countries,
                typesCompany:typesCompany, states:states,comingFromWizard:false]);
        }
    }

	def edit(long id)
	{
		
		def allowedCompanies = getPermitedCompanies(false)
		def location=CompanyLocation.findById(id)
	 
		if (!location || !location.company) {
			flash.error = message(code: 'default.location.not.found')
			redirect(action: Constants.VIEW_INDEX, controller: Constants.CONTROLLER_COMPANY)
			return
		}			
		if(!allowedCompanies.any { it.id == location.company.id }){
			
			flash.error = message(code: 'default.not.found.message')
			redirect(action: Constants.VIEW_LISTALL)
			return
		}else{				
			def countries=Country.list()
			//Get by default state and cities, maybe the default can will be in a config file
			def states= new ArrayList()
			//Get by default state and cities, maybe the default can will be in a config file
			if(location.state != null){
				states=State.findAllByCountry(location.state.country)
				location.country = location.state.country
			}
			def integrators = CompanyIntegrator.getEnabledAndApprovedCompanies().listDistinct()
			integrators = Utils.sortIntegratorByName(integrators)
			def typesCompany = getCompanyTypesOfAffiliate();
			[location: location,countries:countries,typesCompany:typesCompany, states:states,integrators:integrators]

		}
	}
	
	def show(long id) {
		
		try{
			
			def location=CompanyLocation.get(id)
			
			def contents = Content.withCriteria {
				eq('enabled', true)
				or {
					ne('type', 'offer')
					and {
						eq('type', 'offer')
						eq('featured', true)
					}
				}
				locations {
					  'in'('id', location.collect {loc ->
						return loc.id;
					})
				}
			  }
						
			List<Content> locationContents = contentService.sortContentList(location, contents);
									
			if (!location || !location.company) {
				flash.error = message(code: 'default.location.not.found')
				redirect(action: Constants.VIEW_LISTALL)			
			} else {
				def offers = Content.withCriteria {
					eq('enabled', true)
					eq('type', 'offer')
					locations {
						  'in'('id', location.collect {loc ->
							return loc.id;
						})
					}
				  }
				
				def messageErrorDescriptionHash =  messageErrorService.getMessageErrorDescription();
				def syncResultContent = null;
				def syncResultDocument = null;
				def boxLocationData = null;
				def contentsInfo = null;
				HashMap<String, String> documentsInfo = new HashMap<String, String>()
				//Check if must sync Content
				if((params.a == "syncContent")) {
					def updatedLocations = [];
					updatedLocations.add(location);
					(syncResultContent, boxLocationData) = syncContentsToLocations(updatedLocations);
					contentsInfo = contentService.getContentNames(updatedLocations);
				} else {
					if((params.a == "syncDocument")) {
						def updatedLocations = [];
						updatedLocations.add(location);
						syncResultDocument = documentService.updateBoxesOfLocations(updatedLocations);						
						documentsInfo = documentService.getDocumentNames(updatedLocations);
						//contentsInfo = contentService.getContentNames(updatedLocations);
					}
				}
				
				if(isAdminOrHelpDesk()){
					//Check if must sync ExXtractor Users
					def syncResult = null;
					if((params.a == "sync")){
						/*flash.error = null;
						syncResult = boxUserService.syncronizeUsersByLocation(location, flash);
						if(flash.error != null){
							flash.error = message(code: flash.error);
						}*/
						syncResult = session.boxUsersSyncResult;
						session.boxUsersSyncResult = null;
					}
					[location: location,locationContents:locationContents, offers: offers,
						syncResultExXUsers: syncResult, messageErrorDescriptionHash:messageErrorDescriptionHash,
						syncResultContent: syncResultContent, contentsHashInfo: contentsInfo,
						syncResultDocument: syncResultDocument, documentsHashInfo: documentsInfo, boxLocationData:boxLocationData]
				}else{
					def allowedCompanies = getPermitedCompanies(false);
					
					if(!allowedCompanies.any { it.id == location.company.id }){
						
						flash.error = message(code: 'default.not.found.message');
						redirect(action: Constants.VIEW_LISTALL);
						
					} else {
						[location: location, locationContents:locationContents, offers: offers,
							syncResultContent: syncResultContent, contentsHashInfo: contentsInfo, 
							messageErrorDescriptionHash:messageErrorDescriptionHash,
							syncResultDocument: syncResultDocument, documentsHashInfo: documentsInfo, boxLocationData:boxLocationData]
					}
				}
			}
		}catch(Exception ex){
			flash.error = ex.getMessage()
			ex.printStackTrace()
			redirect(action: Constants.VIEW_LISTALL)		
		}
	}
	
	/**
	 * This method will try to send the Contents to a List of Locations;
	 * will set the flash messages according to result
	 *
	 * @param affectedLocations		List of Locations to be sync
	 * @return						Result of the Sync
	 */
	private syncContentsToLocations(def affectedLocations) {
		def jsonTransactionStatus;
		def boxLocations;
		(jsonTransactionStatus, boxLocations) = contentService.updateBoxesOfLocations(affectedLocations);

		if(jsonTransactionStatus == null) {
			//There are not communication with the API, impossible to sync
			flash.error = message(code: 'general.no.communication.with.web.socket.api');
		} else {
			if(anyFailInSyncContent(jsonTransactionStatus)){
				//At least 1 Box failed to Sync
				flash.warn = message(code:'boxes.syncErrorMessage');
			} else {
				flash.info = message(code: 'sync.full.success');
			}
		}
		return [jsonTransactionStatus, boxLocations];
	}
	/**
	 * This method checks the result per Box and returns
	 * true if at least 1 box failed
	 *
	 * @param syncResponse	Sync response from Communication
	 * @return				At least 1 failed or not
	 */
	boolean anyFailInSyncContent(syncResponse){
		for(int i=0; i < syncResponse.contentsStatus.size(); i++){
			if(!syncResponse || !syncResponse.contentsStatus[i] || 
				!syncResponse.contentsStatus[i].successful){
				return true;
			}
		}
		return false;
	}
	
    /**
     * Handles the update venue request including the validation of all the information, upload of the logo and the
     * synchronization of the information with the related ExXtractors
     */
    def update() {
        CompanyLocation location = null;
        def imagesUrlsToDelete = [];
        def deleteWhenFails = [];
        Map<String, String> originalImages = new HashMap<String, String>();
        try {
            location = CompanyLocation.read(Long.parseLong(params.id));
            boolean wasPocAccountCreated = location.pocAccountCreated;
            boolean mustUpdateTheAppSkinAtBoxes = false;
            int previousLimitOfContents = location.contentsLimit;
            final String previousFacebookId = location.facebookId? location.facebookId:"";
            originalImages.put("logoUrl", location.logoUrl);
            if (location.version > Long.parseLong(params.version)) {
                flash.error = message(code: 'default.location.edit.version.error');
                location.errors.rejectValue("version", 'default.location.edit.version.error');
            } else {
                if (previousParamsValidation(location, false)) {
                    def integrator = params.commercialIntegrator.id;
                    if (integrator == null  || integrator.empty) {
                        location.commercialIntegrator = null;
                    }
                    if (params.otherType == null || params.otherType.isEmpty() ||
                            (location.type.code != Constants.ESTABLISHMENT_TYPE_OTHER
                                && location.type.code != Constants.ESTABLISHMENT_TYPE_WAITING_ROOM)) {
                        location.otherType = null;
                    }
                    if (!location.country.hasStates()) {
                        location.state = null;
                    }
                    if (pocService.handlePOCChange(wasPocAccountCreated, location, g)) {
                        //Check and upload the logo Image
                        final String url = uploadImageFile(message(code:'company.logo'), Constants.PARAM_BG_LOGOUT_URL,
                            Constants.COMPANY_LOGO_WIDTH, Constants.COMPANY_LOGO_HEIGHT);
                        if (url != null) {
                            imagesUrlsToDelete.add(location.logoUrl);
                            location.logoUrl = url;
                            deleteWhenFails.add(url);
                            mustUpdateTheAppSkinAtBoxes = true;
                        }
                        if (location.save()) {
                            flash.success = message(code: 'default.message.success');
                            rackspaceService.deleteMultipleFiles(imagesUrlsToDelete);
                            def affectedLocations = [];
                            affectedLocations.add(location);
                            try {
                                // If the limit changes needs to update the Content List to the Box
                                if (previousLimitOfContents != location.contentsLimit) {
                                    //If the object is updated at DB, try to sync the associated locations
                                    contentService.updateBoxesOfLocations(affectedLocations);
                                }
                                if (previousFacebookId != location.facebookId?.trim()) {
                                    mustUpdateTheAppSkinAtBoxes = true;
                                }
                                if (mustUpdateTheAppSkinAtBoxes) {
                                    appSkinService.updateBoxesOfLocations(affectedLocations, location.activeSkin);
                                }
                            } catch(Exception e) {
                                log.error("Error syncronizing ExXtractors", e);
                                flash.warn = message(code: 'boxes.sync.information.failed');
                            }
                        } else {
                            flash.error = message(code: 'default.error.problem');
                        }
                    } else {
                        location.pocAccountCreated = false;
                        flash.error = message(code: 'default.error.problem');
                    }
                }
            }
        } catch(Exception ex) {
            if (StringUtils.isBlank(flash.error)) {
                log.error("Error Updating a Venue", ex);
                flash.error = message(code: 'default.error.problem');
            }
        }
        if (flash.error) {
            rackspaceService.deleteMultipleFiles(deleteWhenFails);
            render(view: Constants.VIEW_EDIT, model:[location: location, countries:Country.list(),
                typesCompany:getCompanyTypesOfAffiliate(), states:State.findAllByCountry(location.country)]);
        } else {
            redirect(action: Constants.VIEW_LISTALL);
        }
    }
	
	def index()
	{
		def locations
		def user = getCurrentUser()
		def id = params.id
		if(!id)
		{
			redirect(action:Constants.VIEW_LISTALL)
			return
		}		
		def company=Company.findById (id)
				
		if (isUserRoleHelpDesk() || isUserRoleAdmin()){
			locations= company.locations
		}else{
			locations=CompanyLocation.getAllLocations(getCurrentUser(), company).listDistinct()
		}
		if(0==locations.size())
		{
			flash.error = message(code: 'default.location.not.locations')

			redirect(controller:Constants.CONTROLLER_COMPANY,action: Constants.VIEW_INDEX)
			return
		}
		[locations:locations,company:company]

		
	}

    /**
     * Only renders the search view
     */
    def search(){}
    
    def delete() {}

    /**
     * Only renders the notlocations view
     */
    def notlocations(){}

    /**
     * Renders the page with the list of authorized Venues depending of the logged user
     */
    def listAll() {
        def locations;
        if(userSessionService.isAdminOrHelpDesk()) {
            locations = CompanyLocation.findAll(sort:Constants.NAME, order:Constants.SORT_ASC);
        } else {
            User user = userSessionService.getCurrentUser();
            locations = CompanyLocation.getAllLocations(user, null).listDistinct();
        }
        if(locations && locations.size() > 0) {
            locations = Utils.sortByName(locations);
            [companyLocations: locations]
        } else {
            redirect(controller:Constants.CONTROLLER_LOCATION, action: Constants.VIEW_NOT_LOCATIONS);
        }
    }

	def ajaxGetCompanyInformation(long companyId) {
	
		def allowedCompanies = getPermitedCompanies(true)
		def company = null
		def state = null
		def mapJson = null
		
		for (Company companyIt : allowedCompanies) {			
			if(companyIt.id == companyId) {
				company = companyIt				
				state = company.state
				break
			}
		}
		
		if(company == null) {
			mapJson = [
						"isSuccessful": false,
						"status": "Company Not found"
					  ]				
		} else {
					
			def country = company.state?.country? company.state.country : company.country
			def states	   =	State.findAllByCountry(country)		 			
			def statesFilter =    states.collect{sts -> return [id:sts.id,name:message(code:"state.code." + sts.name),country:sts.country.name]}
			
			
			def typeOfCompany = CompanyType.findByName(CompanyTypeEnum.OWNER.value)
			def typeId = null
			
			if(company.typeOfCompany.id == typeOfCompany.id) {
				typeId = company.type.id
			}
 								
 			mapJson = [
				"isSuccessful":true,
				"status": "successful",
				"address": company.address,
				"stateId": state != null ? state.id : "",				
				"countryId": country.id,
				"city":	company.city,	
				"stateName" : company.stateName != null ? company.stateName : "" ,
				"typeId": typeId,
				"other": company.otherType,
				"states":statesFilter,
				"timezone": (company.timezone != null)? company.timezone:"",
				"logoUrl": (company.logoUrl != null)? company.logoUrl:""
			]
		}		
		render (mapJson as JSON)
			
	}
	
	def SaveContentsOrder(){
		try
		{			
			log.info(params.contents);
			
			if ( params.size() > 2) { //if the only values are action and controller, then we are not coming from the form
				if(Integer.parseInt(params.count) > 0)
				{
					
					CompanyLocation location = CompanyLocation.read(params.locationId);
					if(location!=null)
					{
						location.contentsOrder = params.contents;
						location.totalContents = Integer.parseInt(params.count);
						location.save(failOnError:true);
												
						def affectedLocations = new ArrayList<CompanyLocation>();
						affectedLocations.add(location);
						def jsonTransactionStatus = contentService.updateBoxesOfLocations(affectedLocations)
						
						if(jsonTransactionStatus == null) {
							//There are not communication with the API, impossible to sync
							//flash.error = message(code: 'content.order.no.communication.socket.api');
							render(text: [error:false, success:true,  textStatus: message(code: 'content.order.no.communication.socket.api')] as JSON, contentType:'text/json');
						} else {							
							render(text: [error:false, success:true,  textStatus: message(code: 'default.message.success')] as JSON, contentType:'text/json');							
						}
						
						//render(text: [error:false, success:true,  textStatus: message(code: 'default.message.success')] as JSON, contentType:'text/json');
					}else{
						render ([error:true, textStatus: message(code: 'default.error.problem') ] as JSON );
					}
					
				}
				else{
					render ([error:true, textStatus: message(code: 'default.error.no.contents') ] as JSON );
					}
				
				
									
				
			}else{
				render ([error:true, textStatus: message(code: 'default.error.problem') ] as JSON );
			}
		} catch( Exception ex) {
			log.error(ex)
			render ([error:true, textStatus: message(code: 'default.error.problem') ] as JSON );
		}
	}
	
	/**
	 * This method checks the result per Box and returns
	 * true if at least 1 box failed
	 *
	 * @param syncResponse	Sync response from Communication
	 * @return				At least 1 failed or not
	 */
	boolean anyFailInSync(syncResponse){
		for(int i=0; i < syncResponse.contentsStatus.size(); i++){
			if(!syncResponse.contentsStatus[i].successful){
				return true;
			}
		}
		return false;
	}

	def assignSkin(Long id){
		def location		
		def locations =  getPermitedLocations()

		if (locations.size() > 0) {
			if(id != null){
				location = CompanyLocation.read(id)
			}else{
				location = locations.get(0)
			}
		}else{
			redirect(action: Constants.VIEW_NOT_LOCATIONS)
		}
		
		[location: location, locations:Utils.sortByName(locations)]
	}
	
	def ajaxAssignSkin(){
		def location = CompanyLocation.read(params.locationId)
		def skins = Utils.sortByName(location.appSkins)
		render (template:'assignSkinTemplate', model:[location: location, skins:Utils.sortByName(skins)])
	}

    def setActiveSkin(){
        CompanyLocation location = CompanyLocation.read(params.location);
        AppSkin appSkin = AppSkin.read(params.skin);
        if(location && appSkin) {
            location.activeSkin = appSkin;
            if(location.save()) {
                flash.success = message(code: 'default.upated.message', args: [location.name])
            } else {
                flash.error = message(code: 'default.not.upated.message', args: [location.name])
            }
        } else {
            flash.error=message(code: 'content.not.found');
        }
        redirect(action: Constants.VIEW_LISTALL);
    }

    /**
     * This method will search the default Contents by Affiliate and associated to the received Location; this method 
     * will be used only when a new Location is created
     * 
     * @param location	
     *         Location created
     */
    def associateDefaultContents(CompanyLocation location){
        //Find Default Contents
        Affiliate affiliate = getCurrentSessionAffiliate();
        def contents = Content.findAllByAffiliateAndIsDefaultOfLocationAndEnabled(affiliate, true, true);
        for(content in contents) {
            location.addToContents(content);
        }
        location.save(flush:true);
    }

    /** This method returns the company type, if the company is Integrator the type id Null, integrator has not type. 
     * Only exists 2 CompanyType "OWNER" or "INTEGRATOR"
     *
     * @param company
     *      Company to review the type
     * @return
     *      Id of the type of the Company if it's a regular Company or null if it's an Integrator Company
     */
    private getCompanyType(Company company) {
        def types = CompanyType.findAllByName(CompanyTypeEnum.OWNER.value);
        def typeOfCompany = (types != null && types.size() > 0)? types.first():null;
        def typeId = null;
        if(company.typeOfCompany != null && company.typeOfCompany.id == typeOfCompany.id) {
                typeId = ((company.type != null )? company.type.id:null);
        }
        return typeId;
    }

    /**
     * Executes the validation of some of the parameters previous to set of the properties to the object to avoid issues
     * specially with the numbers
     * 
     * @return boolean If the company location object is valid or not and the created object
     */
    private def previousParamsValidation(CompanyLocation location, final boolean isCreating) {
        def fieldsWithError = [];
        if (!ValidationHelper.isValidInteger(params.contentsLimit)) {
            params.contentsLimit = VenueProperties.SimpleFields.CONTENTS_LIMIT.getDefault();;
            fieldsWithError.add(VenueProperties.SimpleFields.CONTENTS_LIMIT.fieldName());
        }
        if (!ValidationHelper.isValidInteger(params.numberOfTvWithExxothermicDevice)) {
            params.numberOfTvWithExxothermicDevice = VenueProperties.SimpleFields.NUMBER_OF_TV_WITH_DEVICES.getDefault();
            fieldsWithError.add(VenueProperties.SimpleFields.NUMBER_OF_TV_WITH_DEVICES.fieldName());
        }
        if (!ValidationHelper.isValidInteger(params.numberOfTv)) {
            params.numberOfTv = VenueProperties.SimpleFields.NUMBER_OF_TV.getDefault();
            fieldsWithError.add(VenueProperties.SimpleFields.NUMBER_OF_TV.fieldName());
        }
        if (!ValidationHelper.isValidInteger(params.maxOccupancy)) {
            params.maxOccupancy = VenueProperties.SimpleFields.MAX_OCCUPANCY.getDefault();
            fieldsWithError.add(VenueProperties.SimpleFields.MAX_OCCUPANCY.fieldName());
        }
        location.properties = params;
        if (isCreating) {
            location.administrator = location.company.owner;
            location.createdBy = userSessionService.getCurrentUser();
        }
        boolean isValid = true;
        if (!location.validate() || fieldsWithError.size() > 0) {
            isValid = false;
        }
        for (field in fieldsWithError) {
            location.errors.rejectValue(field, message(code:'invalid.integer.value', 
                args:[message(code:"domain.venue."+field)]));
        }
        if (!isValid) {
            flash.error = message(code: 'validation.error');
        }
        return isValid;
    }
}