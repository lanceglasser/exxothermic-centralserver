package mycentralserver

import mycentralserver.box.BoxUnregistered
import mycentralserver.utils.Utils

class BoxUnregisteredController extends BaseControllerMyCentralServerController {

    def index()
	{
		def user = getCurrentUser()
		def boxes = null
		def offset = this.getOffsetParameterToPagination()

		try
		{
				boxes = BoxUnregistered.findAll()
				if(boxes != null && boxes.size() == 0) {
					redirect(action: "notBoxes", controller:"box")
				}else{
					def totalCount = boxes.size()
					def max = Math.min(params.max ? params.int('max') : 10, 100)
					params.max = max
					
					boxes = Utils.sortunregisteredBoxBySerial(boxes)
					[boxes: Utils.paginateList(boxes, params), boxesCount:totalCount, offset: offset]
				}
			
		}
		catch(Exception e)
		{
			e.printStackTrace()
			flash.error=message(code: 'default.error.unableDisplayLocations')
			redirect(action: "notBoxes", controller:"box")
		}		
	}
	
	
	def removeBox(long id){
		
		def box = BoxUnregistered.get(id)

		if (!box) {
			flash.error = message(code: 'default.not.found.message')
		} else {
			box.delete();
			flash.success = message(code: 'mycentralserver.box.blackList.retire.success')
		}
		redirect(action: "index")
	}
 
	
}
