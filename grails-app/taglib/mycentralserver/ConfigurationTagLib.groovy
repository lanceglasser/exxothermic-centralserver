package mycentralserver;

import mycentralserver.generaldomains.Configuration;
import mycentralserver.utils.Constants;

/**
 * This tagLib allows the render of differents tips of Forms
 * for a Configuration depending of the Type between others tags
 * 
 * @author Selim
 *
 */
class ConfigurationTagLib {
	static namespace = "configuration";

	def renderConfigForm = {attrs ->
		Configuration config = attrs.configuration;
		StringBuilder sb = new StringBuilder();
		try {
			if (config) {
				String value = config.value;
				value = (value != null && !value.isEmpty())? value:'';
				sb << '<span>';
				sb << '<div class="col-xs-4 text-to-right">';
					sb << '<label for="value"> ' + message(code:'value') +  '</label>';
				sb << '</div>';
								
				switch(config.type){
					case Constants.CONFIGURATION_TYPE_TEXT:
						String cProperties = """ id="value"
								 name="value"
								 placeholder="${ message(code:'configuration.value.placeholder') }"
								 style="height:150px;" """;
							 
						sb << '<div class="col-xs-8 text-to-left">';
						String max = (attrs.max != null)? attrs.max:"1000";
						String min = (attrs.min != null)? attrs.min:"0";
						String pattern = 'pattern=".{' + min + ',' + max + '}"';
						sb << """<textarea ${cProperties} ${pattern}>${value}</textarea>""";
						sb << '</div>';
						break;
					case Constants.CONFIGURATION_TYPE_LIST:
						sb << '<div class="col-xs-8 text-to-left configuration-container">';
						sb << '<input type="hidden" id="value" name="value" value="${value}">';
						def values = value.split(",");
						sb << '<table id="values-list" border="1" class="display dataTable configuration-list">';
						sb << '<tbody>';
						int counter = 1;
						for(item in values){
							sb << '<tr id="item-' + counter + '" class="odd"><td><input type="text" class="item" value="' + item + '"></td><td><a href="#" rel="' + counter + '" class="action-icons delete" title="Delete"></a></td></tr>';
							counter++;
						}						
						sb << '</tbody>';
						sb << '</table>';
						sb << """<button id="add-item-btn">${ message(code:'add.item') }</button>'""";
						sb << '</div>';
						break;
					default:
						sb << '<div class="col-xs-8 text-to-left exxo-label">';
						sb << '----';
						sb << '</div>';
						break;
				}
				
				sb << '</span>';
				sb << '<span class="clearfix"></span>';
			}
		} catch(Exception ex) {
			log.error(ex);
			sb << "";
		}
		out << sb
	}
}
