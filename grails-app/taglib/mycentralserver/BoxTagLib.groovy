/**
 * BoxTagLib.groovy
 */
package mycentralserver;

import org.apache.commons.lang.StringUtils;

import grails.converters.JSON;
import mycentralserver.box.Box;
import mycentralserver.box.BoxConfig;
import mycentralserver.box.BoxConfigOption;
import mycentralserver.box.BoxConfigValue;
import mycentralserver.box.software.BoxSoftware;
import mycentralserver.utils.BoxVersionsEnum;
import mycentralserver.utils.Constants;
import mycentralserver.utils.enumeration.AudioCardGeneration;
import mycentralserver.utils.enumeration.JumperConfiguration;

/**
 * TagLib with how to render special content related with the Exxtractors
 * 
 * @author Cecropia Solutions
 */
class BoxTagLib {
	static namespace = "box";
	
	/* Inject required services */
	def boxConfigService;
	def translatorService;
    def userSessionService;
	
    /*
     * Render all the available configuration options of a Box depending of the version of the box and the minimum
     * required versions of each configuration
     * @param locale
     *      locale that is been use on the session for translations
     * @param box
     *      box that is rendering the configurations
     * @param readOnly
     *      Optional: If true will show the configurations as labels only
     * @return HTML code with the inputs for the available configurations
     */
    def renderConfigs = {attrs ->
        final String locale = attrs.locale;
        final Box box = attrs.box;
        final boolean readOnly = (attrs.readOnly != null
                && (attrs.readOnly == 'true' || attrs.readOnly == true))? true:false;
        StringBuilder sb = new StringBuilder(500);
        try {
            //Load all configs for the Box
            def boxConfigs = BoxConfigValue.findAllByBox(box);
            HashMap<Long, String> configsMap = boxConfigService.getBoxConfigValues(box);
            def configs = BoxConfig.findAll();
            for(BoxConfig config: configs){
                if(boxConfigService.configAllowedForBox(config, box)) { // The config must has a valid box version
                    if( config.enableForUI ) { // Only render the configs enabled for UI
                        sb << '<div class="col-xs-4 text-to-right">';
                        sb << ' <label for="type">' + translatorService.translate(config.labelCode, locale) + '</label>';
                        sb << '</div>';
                        sb << '<div class="col-xs-8 text-to-left">';
                        renderInput(sb, config, configsMap, locale, readOnly );
                        if( !readOnly ) {
                            if(config.helpMsgLabelCode && config.helpMsgLabelCode != ""){
                                sb << '<span class="liten-up">' + translatorService.translate(config.helpMsgLabelCode, locale);
                                if(config.validBoxVersion != null && config.validBoxVersion.trim() != ""){
                                    sb << ". " + message(code:'box.config.initial.box.version.help.msg', args:[config.validBoxVersion]);
                                }
                                sb << '</span>';
                            } else {
                                if(config.validBoxVersion != null && config.validBoxVersion.trim() != ""){
                                    sb <<'<span class="liten-up">' + message(code:'box.config.initial.box.version.help.msg', args:[config.validBoxVersion]);
                                    sb << '</span>';
                                }
                            }
                        }
                        sb << '</div>';
                        sb << '<span class="clearfix"></span>';
                    } else { //Render the configurations as information only
                        sb << '<div class="col-xs-4 text-to-right">';
                        sb << ' <label for="type">' + translatorService.translate(config.labelCode, locale) + '</label>';
                        sb << '</div>';
                        sb << '<div class="col-xs-8 text-to-left' + (readOnly? '':' exxo-label') + '">';
                        renderInput(sb, config, configsMap, locale, readOnly );
                        sb << '</div>';
                        sb << '<span class="clearfix"></span>';
                    }
                }
            }
        } catch(Exception ex) {
            log.error(ex);
            sb << "";
        }
        out << sb
    }
	
    /**
     * Manages the render of the input, depending of the type will call the specific method with the received parameters
     *  
     * @param sb
     *          String builder that is been use for the general render
     * @param boxConfig
     *          Configuration to be rendered
     * @param boxConfigs
     *          Map that contains the configured values
     * @param locale
     *          Locale that is been use
     * @param readOnly
     *          Indicates if the configuration is only to read
     * @return
     */
    protected def renderInput(StringBuilder sb, BoxConfig boxConfig, HashMap<Long, String> boxConfigs,
            String locale, boolean readOnly) {
        if (readOnly || boxConfig.inputType == Constants.INPUT_TYPE_LABEL) {
            renderLabel(sb, boxConfig, boxConfigs.get(boxConfig.id), locale);
        } else {
            switch(boxConfig.inputType){
                case Constants.INPUT_TYPE_SELECT:
                    renderSelect(sb, boxConfig, boxConfigs.get(boxConfig.id), locale);
                    break;
                case Constants.INPUT_TYPE_NUMBER:
                    renderNumberInput(sb, boxConfig, boxConfigs.get(boxConfig.id), locale);
                    break;
                default:
                    break;
            }
        }
    }

    protected void renderSelect(StringBuilder sb, BoxConfig boxConfig, String currentValue, String locale){
        sb << '	<select id="CFG-' + boxConfig.code + '" name="CFG-' + boxConfig.code + '" class="config-select-input">';
        // Load Options of Option Select
        def options = BoxConfigOption.findAllByBoxConfig(boxConfig);
        for(BoxConfigOption option : options){
            sb << '<option value="' + option.value + '" ' + (option.value == currentValue? 'selected="selected"':'')
            sb << '>' + translatorService.translate(option.labelCode, locale) + '</option>';
        }
        sb << '	</select>';
    }

    /**
     * Renders an input of type number with the defined options
     * 
     * @param sb
     *              String builder to append the value
     * @param boxConfig
     *              Related Box Configuration object
     * @param currentValue
     *              Current value of the configuration for the box
     * @param locale
     *              Locale to use for translations
     * @return Updated String Builder with the value appended
     */
    protected void renderNumberInput(StringBuilder sb, BoxConfig boxConfig, String currentValue, String locale) {
        sb << ' <input type="text" id="CFG-' + boxConfig.code + '" name="CFG-' + boxConfig.code;
        sb << '" value="' + currentValue + '"';
        if (StringUtils.isNotBlank(boxConfig.configOptions)) {
            def jsonConfig = JSON.parse(boxConfig.configOptions);
            if (jsonConfig.min) {
                sb << ' min="' + jsonConfig.min + '"';
            }
            if (jsonConfig.max) {
                sb << ' max="' + jsonConfig.max + '"';
            }
            if (jsonConfig.step) {
                sb << ' step="' + jsonConfig.step + '"';
            }
        }
        sb << ' class="numberspinner onlyNumbers"/>';
    }
	
    /**
     * Add to the string builder the value of the configuration as simple text
     * 
     * @param sb
     *              String builder to append the value
     * @param boxConfig
     *              Related Box Configuration object
     * @param currentValue
     *              Current value of the configuration for the box
     * @param locale
     *              Locale to use for translations
     * @return Updated String Builder with the value appended
     */
	protected void renderLabel(StringBuilder sb, BoxConfig boxConfig, String currentValue, String locale) {
        BoxConfigOption option = BoxConfigOption.findByBoxConfigAndValue(boxConfig, currentValue);
        if (option) {
            sb << translatorService.translate(option.labelCode, locale);
        } else {
            sb << currentValue;
        }
	}

    /**
     * Returns the HTML Code of a Select with the list of the AudioCardGeneration values
     */
    def renderAudioCardInfoSelect = {attrs ->
        StringBuilder sb = new StringBuilder(1024);
        final Box box = attrs.box;
        final boolean isEditPage = (attrs.isEditPage != null
            && (attrs.isEditPage == 'true' || attrs.isEditPage == true))? true:false;
        final boolean readOnly = (attrs.readOnly != null
                && (attrs.readOnly == 'true' || attrs.readOnly == true))? true:false;
        final int leftSize = (attrs.leftSize != null)? Integer.parseInt(attrs.leftSize):4;
        final String labelCssClass = (attrs.labelCssClass != null)? attrs.labelCssClass:'';
        // If it's the edit page must check if the user is HelpDesk or not, if not only can read the info
        if (isEditPage) {
            readOnly = !userSessionService.isHelpDesk(); // If not HelpDesk is reanOnly
        }
        labelCssClass = (readOnly)? labelCssClass:"";
        final Integer audioCardGeneration = (box.audioCardGeneration != null)? 
                            box.audioCardGeneration:0;
        sb << '<div class="col-xs-' + leftSize + ' text-to-right">';
        sb << ' <label for="company">' + message(code: 'audio.card.generation') + '</label>';
        sb << '</div>';
        sb << '<div class="col-xs-' + (12-leftSize) + ' text-to-left ' + labelCssClass + '">';
        if (isEditPage && !readOnly) {
            sb << ' <select id="audioCardGeneration" name="audioCardGeneration" class="config-select-input">';
            for (AudioCardGeneration configuration : AudioCardGeneration.values()) {
                sb << '<option value="' + configuration.getValue() + '" '
                sb << (configuration.getValue().equals(audioCardGeneration)? 'selected="selected"':'')
                sb << '>' + message(code: configuration.toString()) + '</option>';
            }
            sb << '</select>';
        } else {
            final AudioCardGeneration configuration = 
                        AudioCardGeneration.findByValue(new Integer(box.audioCardGeneration));
            sb << (configuration == null? '---':message(code: configuration.getName()));
            sb << '<input type="hidden" id="audioCardGeneration" name="audioCardGeneration" value="' + box.audioCardGeneration + '">';
        }
        sb << '</div>';
        sb << '<span class="clearfix"></span>';

        // Only shows the jumper configuration when the audio card generation is "Second"
        final Integer jumperConfigurationValue = (box.jumperConfiguration != null)? box.jumperConfiguration:0;
        String style = "";
        if (!audioCardGeneration.equals(AudioCardGeneration.SECOND.getValue())) {
            style = ' style="display: none;"';
        }
        sb << '<div id="jumperConfigurationContainer"' + style + '>';
        sb << '<div class="col-xs-' + leftSize + ' text-to-right">';
        sb << ' <label for="company">' + message(code: 'jumper.configuration') + '</label>';
        sb << '</div>';
        sb << '<div class="col-xs-' + (12-leftSize) + ' text-to-left ' + labelCssClass + '">';
        if (isEditPage && !readOnly) {
            sb << ' <select id="jumperConfiguration" name="jumperConfiguration" class="config-select-input">';
            for (JumperConfiguration configuration : JumperConfiguration.values()) {
                // Only add the option when is not NONE because that option is invalid for 2nd Generation
                if (configuration.getValue() != JumperConfiguration.NONE.getValue()) {
                    sb << '<option value="' + configuration.getValue() + '" ';
                    sb << (configuration.getValue().equals(jumperConfigurationValue)? 'selected="selected"':'');
                    sb << '>' + configuration.toString() + '</option>';
                }
            }
            sb << '</select>';
        } else {
            final JumperConfiguration jumperConfiguration = 
                                JumperConfiguration.findByValue(new Integer(box.jumperConfiguration));
            sb << (jumperConfiguration == null? '---':jumperConfiguration.getName());
            sb << '<input type="hidden" id="jumperConfiguration" name="jumperConfiguration" value="' + box.jumperConfiguration + '">';
        }
        sb << '</div>';
        sb << '<span class="clearfix"></span>';
        sb << '</div>';
        out << sb;
    }
}