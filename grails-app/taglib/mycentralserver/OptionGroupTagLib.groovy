package mycentralserver

import mycentralserver.company.CompanyCatalogSubType;

import org.springframework.beans.SimpleTypeConverter
import org.springframework.web.servlet.support.RequestContextUtils as RCU
import org.codehaus.groovy.grails.commons.DomainClassArtefactHandler


class OptionGroupTagLib {
	
	static namespace = "custom"
	def selectWithOptGroupCompany= {attrs ->
		def company=attrs.from
		def value=null!=attrs.value?attrs.value:0
		out << g.render(template: '/layouts/tags/selectWithOptGroupCompany', model: [id:attrs.id,company:company,value:value])
    }
	
	def selectWithOptGroupCompanyType= {attrs ->
		def subtypes=attrs.from;
		HashMap<String, List<CompanyCatalogSubType>> types = 
			new HashMap<String, List<CompanyCatalogSubType>>();
		for( subtype in subtypes){
			HashMap<String, Object> typeStruct = (HashMap<String, Object>)types.get(subtype.type.code);;
			
			List<CompanyCatalogSubType> subtypesList = null;
			if(typeStruct == null){
				typeStruct = new HashMap<String, Object>();
				subtypesList = new ArrayList<CompanyCatalogSubType>();				
			} else {
				subtypesList = (List<CompanyCatalogSubType>) typeStruct.get('subtypes');
			}
			
			subtypesList.add(subtype);
			typeStruct.put('subtypes', subtypesList);
			typeStruct.put('isSubgroup', (subtype.type.catalog.size() > 1));
			
			types.put(subtype.type.code, typeStruct);			
		}
		Map<String, List<CompanyCatalogSubType>> map = new TreeMap<String, List<CompanyCatalogSubType>>(types);
		def value=null!=attrs.value?attrs.value:0;
		out << g.render(template: '/layouts/tags/selectCompanyType', model: [id:attrs.id,types:map,value:value])
	}
	
	
	/*def selectWithOptGroupCompanyType= {attrs ->
		def types=attrs.from
		def value=null!=attrs.value?attrs.value:0
		out << g.render(template: '/layouts/tags/selectWithOptGroupCompanyType', model: [id:attrs.id,types:types,value:value])
	}*/
	
}

