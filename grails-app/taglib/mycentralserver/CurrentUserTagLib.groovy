/**
 * CurrentUserTagLib.groovy
 */
package mycentralserver;

import mycentralserver.user.User
import mycentralserver.user.UserDashboard;
import mycentralserver.utils.RoleEnum;
import grails.converters.JSON;
import grails.plugins.springsecurity.Secured;

import org.springframework.web.servlet.FlashMap;

import sun.org.mozilla.javascript.internal.json.JsonParser;

/**
 * Special Tags related with the User
 * 
 * @author Cecropia Solutions
 */
class CurrentUserTagLib {
	
	/* Inject required services */
	def springSecurityService;
	def userSessionService;
	def dashboardService;
	def widgetService;
	
	static namespace = "user";
	
	public static final int WIDGET_STATS = 1;
	public static final int WIDGET_SHORTCUTS = 2;
	public static final int WIDGET_COMPANIES = 3;
	public static final int WIDGET_LOCATIONS = 4;
	public static final int WIDGET_EXXTRACTORS = 5;
	public static final int WIDGET_BANNERS = 6;
	public static final int WIDGET_OFFERS = 7;
	public static final int WIDGET_DOCUMENTS = 8;
	public static final int WIDGET_APP_THEME_SETTIGS = 9;
	public static final int WIDGET_WELCOME_ADS = 10;
	public static final int WIDGET_COMPANY_INFO = 11;
	public static final int WIDGET_LOCATION_INFO = 12;
	public static final int WIDGET_EMPLOYEES = 13;
	public static final int WIDGET_WA_APP_THEME = 14;
	
	/**
	 * This method returns an array with the required data by the
	 * view
	 * @param widget
	 * @return
	 */
	def getWidgetModel(widget){
		int autoRefreshStatus = (widget.refresh)? widget.refresh:0;
		int autoRefreshTime = (widget.refreshTime)? widget.refreshTime:0;
		
		return [row:widget.r, col:widget.c, sizex: widget.x, sizey: widget.y, status: widget.s,
			maxH: widget.maxH, maxX: widget.maxX, maxY:widget.maxY, refreshStatus:autoRefreshStatus, refreshTime:autoRefreshTime];
	} // End of getWidgetModel method
	
	private void checkAndAddWidget(int widgetCode, out, jsonConfig) {
		switch(widgetCode){
			case CurrentUserTagLib.WIDGET_STATS:
				def widget = jsonConfig['exxtractors-info-widget'];
				if(widget) {
					if(widget.v && widget.v){
						out << g.render(template:'/widget/exxtractorsInfoWidget',
							model:getWidgetModel(widget));
					}
				}
				break;
			case CurrentUserTagLib.WIDGET_SHORTCUTS:
				def widget = jsonConfig['shortcuts-widget'];
				if(widget) {
					if(widget.v && widget.v){
						out << g.render(template:'/widget/shortcutsWidget',
							model:getWidgetModel(widget));
					}
				}
				break;
				
			case CurrentUserTagLib.WIDGET_WA_APP_THEME:
				def widget = jsonConfig['waAppTheme-widget'];
				if(widget) {
					if(widget.v && widget.v){
						out << g.render(template:'/widget/waAppThemeWidget',
							model:getWidgetModel(widget));
					}
				}
				break;
			case CurrentUserTagLib.WIDGET_COMPANIES:
				def widget = jsonConfig['companies-widget'];
				if(widget) {
					if(widget.v && widget.v){
						out << g.render(template:'/widget/companiesListWidget',
							model:getWidgetModel(widget));
					}
				}
				break;
			case CurrentUserTagLib.WIDGET_LOCATIONS:
				def widget = jsonConfig['locations-widget'];
				if(widget) {
					if(widget.v && widget.v){
						out << g.render(template:'/widget/locationsListWidget',
							model:getWidgetModel(widget));
					}
				}
				break;
			case CurrentUserTagLib.WIDGET_EXXTRACTORS:
				def widget = jsonConfig['exxtractors-widget'];
				if(widget) {
					if(widget.v && widget.v){
						out << g.render(template:'/widget/exxtractorsListWidget',
							model:getWidgetModel(widget));
					}
				}
				break;
			case CurrentUserTagLib.WIDGET_BANNERS:
				def widget = jsonConfig['banners-widget'];
				if(widget) {
					if(widget.v && widget.v){
						out << g.render(template:'/widget/bannersListWidget',
							model:getWidgetModel(widget));
					}
				}
				break;
			case CurrentUserTagLib.WIDGET_OFFERS:
				def widget = jsonConfig['offers-widget'];
				if(widget) {
					if(widget.v && widget.v){
						out << g.render(template:'/widget/offersListWidget',
							model:getWidgetModel(widget));
					}
				}
				break;
			case CurrentUserTagLib.WIDGET_DOCUMENTS:
				def widget = jsonConfig['documents-widget'];
				if(widget) {
					if(widget.v && widget.v){
						out << g.render(template:'/widget/documentsListWidget',
							model:getWidgetModel(widget));
					}
				}
				break;
			case CurrentUserTagLib.WIDGET_WELCOME_ADS:
				def widget = jsonConfig['welcomeList-widget'];
				if(widget) {
					if(widget.v && widget.v){
						out << g.render(template:'/widget/welcomeListWidget',
							model:getWidgetModel(widget));
					}
				}
				break;
			case CurrentUserTagLib.WIDGET_COMPANY_INFO:
				def widget = jsonConfig['company-widget'];
				if(widget) {
					if(widget.v && widget.v){
						out << g.render(template:'/widget/companyWidget',
							model:getWidgetModel(widget));
					}
				}
				break;
			case CurrentUserTagLib.WIDGET_LOCATION_INFO:
				def widget = jsonConfig['location-widget'];
				if(widget) {
					if(widget.v && widget.v){
						out << g.render(template:'/widget/locationWidget',
							model:getWidgetModel(widget));
					}
				}
				break;				
			case CurrentUserTagLib.WIDGET_EMPLOYEES:
				if(widgetService.userHasAccessToWidget('employees-widget')){
					def widget = jsonConfig['employees-widget'];
					if(widget) {
						if(widget.v && widget.v){
							out << g.render(template:'/widget/employeesListWidget',
								model:getWidgetModel(widget));
						}
					}
				}
				break;
			case CurrentUserTagLib.WIDGET_APP_THEME_SETTIGS:
				def widget = jsonConfig['appSkin-widget'];
				if(widget) {
					if(widget.v && widget.v){
						out << g.render(template:'/widget/appSkinsListWidget',
							model:getWidgetModel(widget));
					}
				}
				break;	
			default:
				break;
		}
	} // End of checkAndAddWidget method
		
	def getCurrentUserName={attr,body->
		
		if (springSecurityService.isLoggedIn()) {
			if(springSecurityService.principal?.id ){
				def user=User.get(springSecurityService.principal.id)
				if (user.toString().length() < 25){
					out << user.encodeAsHTML().toString()
				}else{
					out << user.encodeAsHTML().toString().substring(0, 25)
				}
			}else{
				out << "TimeOut Session"
			}
		}else{
			out << "TimeOut Session"
		}
	}
	
	/**
	 * Render the main dashboard of the system
	 */
	def mainDashboard = { attrs ->
		
		User user = User.read(springSecurityService.principal.id);
		UserDashboard dashboardInfo = UserDashboard.findByUser(user);
		def jsonConfig = null;
		String configuration = "{}";
		
		if(dashboardInfo && dashboardInfo.userDashboard && dashboardInfo.userDashboard.trim() != '{}' ){
			configuration = dashboardInfo.userDashboard;
		} else { //If not exists or is empty load the default
			configuration = dashboardService.getDefaultDashboardConfig(DashboardService.MAIN_DASHBOARD);
		}
		
		jsonConfig = JSON.parse(configuration); // Parse to JSON
		
		out << '<div class="gridster">'
		out << '<ul>'
		
		if(jsonConfig){
			
			checkAndAddWidget(CurrentUserTagLib.WIDGET_STATS, out, jsonConfig);
			
			checkAndAddWidget(CurrentUserTagLib.WIDGET_SHORTCUTS, out, jsonConfig);
			
			checkAndAddWidget(CurrentUserTagLib.WIDGET_COMPANIES, out, jsonConfig);
			
			checkAndAddWidget(CurrentUserTagLib.WIDGET_LOCATIONS, out, jsonConfig);
			
			checkAndAddWidget(CurrentUserTagLib.WIDGET_EXXTRACTORS, out, jsonConfig);
			
			checkAndAddWidget(CurrentUserTagLib.WIDGET_BANNERS, out, jsonConfig);
			
			checkAndAddWidget(CurrentUserTagLib.WIDGET_OFFERS, out, jsonConfig);
			
			checkAndAddWidget(CurrentUserTagLib.WIDGET_DOCUMENTS, out, jsonConfig);
			
			checkAndAddWidget(CurrentUserTagLib.WIDGET_APP_THEME_SETTIGS, out, jsonConfig);
			
			checkAndAddWidget(CurrentUserTagLib.WIDGET_WELCOME_ADS, out, jsonConfig);			
		
		}
		
		out << '</ul>'
		out << '</div>'
	}
	
	/**
	 * Render the company dashboard of the system
	 */
	def companyDashboard = { attrs ->
		
		User user = User.read(springSecurityService.principal.id);
		UserDashboard dashboardInfo = UserDashboard.findByUser(user);
		
		def jsonConfig = null;
		String configuration = "{}";
		
		if(dashboardInfo && dashboardInfo.companyDashboard && dashboardInfo.companyDashboard.trim() != '{}' ){
			configuration = dashboardInfo.companyDashboard;
		} else { //If not exists or is empty load the default
			configuration = dashboardService.getDefaultDashboardConfig(DashboardService.COMPANY_DASHBOARD);
		}
		
		jsonConfig = JSON.parse(configuration); // Parse to JSON
		
		out << '<div class="gridster">'
		out << '<ul>'
		
		if(jsonConfig){
			
			checkAndAddWidget(CurrentUserTagLib.WIDGET_STATS, out, jsonConfig);
			
			checkAndAddWidget(CurrentUserTagLib.WIDGET_SHORTCUTS, out, jsonConfig);
			
			checkAndAddWidget(CurrentUserTagLib.WIDGET_COMPANY_INFO, out, jsonConfig);
						
			checkAndAddWidget(CurrentUserTagLib.WIDGET_LOCATIONS, out, jsonConfig);
			
			checkAndAddWidget(CurrentUserTagLib.WIDGET_EXXTRACTORS, out, jsonConfig);
			
			checkAndAddWidget(CurrentUserTagLib.WIDGET_EMPLOYEES, out, jsonConfig);
			
		}
		
		out << '</ul>'
		out << '</div>'
	}
	
	/**
	 * Render the location dashboard of the system
	 */
	def locationDashboard = { attrs ->
		
		User user = User.read(springSecurityService.principal.id);
		UserDashboard dashboardInfo = UserDashboard.findByUser(user);
		def jsonConfig = null;
		String configuration = "{}";
		
		if(dashboardInfo && dashboardInfo.locationDashboard && dashboardInfo.locationDashboard.trim() != '{}'){
			configuration = dashboardInfo.locationDashboard;
		} else { //If not exists or is empty load the default
			configuration = dashboardService.getDefaultDashboardConfig(DashboardService.LOCATION_DASHBOARD);
		}
		
		jsonConfig = JSON.parse(configuration); // Parse to JSON
		
		out << '<div class="gridster">'
		out << '<ul>'
		
		if(jsonConfig){
			
			checkAndAddWidget(CurrentUserTagLib.WIDGET_STATS, out, jsonConfig);
			
			checkAndAddWidget(CurrentUserTagLib.WIDGET_SHORTCUTS, out, jsonConfig);
			
			checkAndAddWidget(CurrentUserTagLib.WIDGET_LOCATION_INFO, out, jsonConfig);
						
			checkAndAddWidget(CurrentUserTagLib.WIDGET_EXXTRACTORS, out, jsonConfig);
			
			checkAndAddWidget(CurrentUserTagLib.WIDGET_BANNERS, out, jsonConfig);
			
			checkAndAddWidget(CurrentUserTagLib.WIDGET_OFFERS, out, jsonConfig);
			
			checkAndAddWidget(CurrentUserTagLib.WIDGET_DOCUMENTS, out, jsonConfig);

			checkAndAddWidget(CurrentUserTagLib.WIDGET_WA_APP_THEME, out, jsonConfig);
	
		}
		
		out << '</ul>'
		out << '</div>'
	} // End of locationDashboard
	
	def listUserRoles = {attr ->
		def user = attr.get('user');
		def roles = user.getAuthorities();
		String rolesStr = "";
		for(role in roles){
			rolesStr += ((rolesStr == "")? '':',') + message(code:role.authority);
		}
		out << rolesStr;
	}
}
