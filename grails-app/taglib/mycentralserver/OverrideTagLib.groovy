package mycentralserver

import java.text.SimpleDateFormat;

import groovy.time.*;
import java.text.*;

import org.codehaus.groovy.grails.commons.GrailsControllerClass
import org.codehaus.groovy.grails.plugins.web.taglib.ValidationTagLib;
import org.codehaus.groovy.grails.web.mapping.UrlCreator
import org.codehaus.groovy.grails.commons.ControllerArtefactHandler
import org.springframework.web.context.request.RequestContextHolder

import grails.artefact.Artefact
import groovy.xml.MarkupBuilder

import java.beans.PropertyEditor
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

import org.apache.commons.lang.StringEscapeUtils
import org.codehaus.groovy.grails.plugins.codecs.HTMLCodec
import org.codehaus.groovy.grails.web.taglib.GroovyPageAttributes
import org.springframework.beans.PropertyEditorRegistry
import org.springframework.context.MessageSourceResolvable
import org.springframework.context.NoSuchMessageException
import org.springframework.context.support.DefaultMessageSourceResolvable
import org.springframework.validation.Errors
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.servlet.support.RequestContextUtils as RCU

class OverrideTagLib extends ValidationTagLib {

	/**
	 * Resolves a message code for a given error or code from the resource bundle.
	 *
	 * @emptyTag
	 *
	 * @attr error The error to resolve the message for. Used for built-in Grails messages.
	 * @attr message The object to resolve the message for. Objects must implement org.springframework.context.MessageSourceResolvable.
	 * @attr code The code to resolve the message for. Used for custom application messages.
	 * @attr args A list of argument values to apply to the message, when code is used.
	 * @attr default The default message to output if the error or code cannot be found in messages.properties.
	 * @attr encodeAs The name of a codec to apply, i.e. HTML, JavaScript, URL etc
	 * @attr locale override locale to use instead of the one detected
	 */
	Closure message = { attrs ->
		messageImpl(attrs);
	}
	
	def messageImpl(attrs) {
		ValidationTagLib validationTagLib = grailsAttributes.applicationContext.getBean('org.codehaus.groovy.grails.plugins.web.taglib.ValidationTagLib')
		//validationTagLib.message.call(attrs)
		
		def messageSource = grailsAttributes.applicationContext.messageSource
		def locale = attrs.locale ?: RCU.getLocale(request)
		def tagSyntaxCall = (attrs instanceof GroovyPageAttributes) ? attrs.isGspTagSyntaxCall() : false

		def text
		def error = attrs.error ?: attrs.message
		if (error) {
			if (!attrs.encodeAs && error instanceof MessageSourceResolvable && error.arguments) {
				error = new DefaultMessageSourceResolvable(error.codes, encodeArgsIfRequired(error.arguments, tagSyntaxCall) as Object[], error.defaultMessage)
			}
			try {
				text = messageSource.getMessage(error , locale)
			}
			catch (NoSuchMessageException e) {
				if (error instanceof MessageSourceResolvable) {
					text = error?.code
				}
				else {
					text = error?.toString()
				}
			}
		}
		else if (attrs.code) {
			def code = attrs.code
			def args = []
			if(attrs.args) {
				args = attrs.encodeAs ? attrs.args : encodeArgsIfRequired(attrs.args, tagSyntaxCall)
			}
			def defaultMessage
			if (attrs.containsKey('default')) {
				defaultMessage = attrs['default']
			} else {
				defaultMessage = code
			}

			/*def message = messageSource.getMessage(code, args == null ? null : args.toArray(),
				defaultMessage, locale)*/
			
			def message = getMessageWithTheme(code, args, defaultMessage, locale)
			
			if (message != null) {
				text = message
			}
			else {
				text = defaultMessage
			}
		}
		if (text) {
			return (attrs.encodeAs && !attrs.encodeAs.equalsIgnoreCase('none')) ? text."encodeAs${attrs.encodeAs}"() : text
		}
		''
	}
	
	private getMessageWithTheme(String code, args, String defaultMessage, locale){
		
		def message = "";
		
		def messageSource = grailsAttributes.applicationContext.messageSource
		
		try{
			if(session.theme != null && session.theme != "" && session.theme != "default"){//There is
				message = messageSource.getMessage(session.theme + "." + code, args == null ? null : args.toArray(),
					defaultMessage, locale)
							
				if(message == null || message == code){//If there is no special message for the theme, try to load the default
					message = messageSource.getMessage(code, args == null ? null : args.toArray(),
						defaultMessage, locale);
				}
			} else {
				message = messageSource.getMessage(code, args == null ? null : args.toArray(),
					defaultMessage, locale);
			}
		}catch(Exception e){
			message = messageSource.getMessage(code, args == null ? null : args.toArray(),
				defaultMessage, locale);
		}
		
		return message
	}
	
	private encodeArgsIfRequired(arguments, boolean tagSyntaxCall) {
		if(arguments && (tagSyntaxCall || HTMLCodec.shouldEncode())) {
			arguments.collect { value ->
				if(value == null || value instanceof Number || value instanceof Date) {
					value
				} else {
					value.toString().encodeAsHTML()
				}
			}
		} else {
			arguments
		}
	}
}
