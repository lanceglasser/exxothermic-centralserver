package mycentralserver

import mycentralserver.company.CompanyLocation;

import org.springframework.beans.SimpleTypeConverter
import org.springframework.web.servlet.support.RequestContextUtils as RCU
import org.codehaus.groovy.grails.commons.DomainClassArtefactHandler
import org.codehaus.groovy.grails.web.mapping.LinkGenerator;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Date;

import javax.swing.text.DateFormatter;

class CustomFieldTagLib {
	static namespace = "custom"

	/* Inject required services */
	def contentService;
	def documentService;
    
    // Inject link generator
    LinkGenerator grailsLinkGenerator
	
	def editField = {attrs ->
		def name = attrs.name
		def width = attrs.width
		def align = attrs.align
		def required = attrs.required
		def src = (attrs.src != null)? ""+attrs.src:"";
		def visible = (attrs.visible != null)? attrs.visible:true;
		def value = (attrs.value != null)? ""+ attrs.value:"";
		value = StringEscapeUtils.escapeHtml(value);
		def readonly = (attrs.readonly != null)? ""+attrs.readonly:"false";
		def autocomplete = (attrs.autocomplete != null)? 'autocomplete="'+attrs.autocomplete+'"':"";
		def label = attrs.label
		def type = attrs.type
		def note =  attrs.note
		def disabled = attrs.disabled
		def tooltip = attrs.tooltip
		def cssclass = (attrs.cssclass != null)? attrs.cssclass+" ":"";
		def min = (attrs.min != null)? attrs.min:"";
		def max = (attrs.max != null)? attrs.max:"";
		def inputStep = (attrs.step != null)? attrs.step:"";
		def leftColumnSize = (attrs.leftColumnSize != null)? Integer.parseInt(attrs.leftColumnSize):4;
		def leftColumnPosition = (attrs.leftColumnPosition != null)? attrs.leftColumnPosition:"right";
		def rightColumnPosition = (attrs.rightColumnPosition != null)? attrs.rightColumnPosition:"left";
		def number = (attrs.number != null)? attrs.number:"1";
		def placeholder = (attrs.placeholder != null)? attrs.placeholder:"";

		StringBuilder sb = new StringBuilder();
		String input = "";
		String pattern = "";

		String cProperties = """ id="${name}"
								 name="${name}"								 
								 class="${cssclass}${required? "required":""}"
								 value="${(value != null && !value.isEmpty()) ? value:''}"
								 placeholder="${placeholder}"
								 ${autocomplete}
								 ${(required) ? 'required="required"':''}
								 ${(disabled) ?  'disabled="disabled"':''} """;
		switch (type) {
			case "text":
				max = (attrs.max != "")? max:"50";
				input = """<input type="text" ${cProperties} maxlength="${max}"/>""";
				break;
			case "password":
				max = (attrs.max != "")? max:"50";
				input = """<input type="password" ${cProperties} maxlength="${max}"/>""";
				break;
			case "email":
				input = """<input type="email" ${cProperties} />""";
				break;
			case "phoneNumber":
				//pattern = '^[a-zA-Z0-9]*$';
                max = (attrs.max != "")? 'maxlength="' + max + '"':"";
				input = """<input type="text"  ${cProperties} ${max} placeholder="${message(code: "default.field.example")} ${message(code: "field.phone.number.format")}" />""";
				break;
			case "tax": 
				pattern = '^([07][1-7]|1[0-6]|2[0-7]|[35][0-9]|[468][0-8]|9[0-589])-?\\d{7}$';
				input = """<input type="text"  ${cProperties} pattern="${pattern}"/>""";
				break;
			case "number":
				min = (min != "")? ' min="' + min + '"':"";
				max = (max != "")? ' max="' + max + '"':"";
				inputStep = (min != "")? ' step="' + inputStep+ '"':"";
				pattern = '[0-9]*'
				input = """<input type="number" step="any" ${cProperties} ${min + max + inputStep} pattern="${pattern}" />""";
				break;
			case "select":
				input = "";
				break;
			case "textarea":
				max = (attrs.max != null)? attrs.max:"100";
				min = (attrs.min != null)? attrs.min:"0";				
				pattern = 'pattern=".{' + min + ',' + max + '}"';
				input = """<textarea ${pattern} ${cProperties}>${(value != null && !value.isEmpty()) ? value:''}</textarea>""";
				break;
			case "checkbox":
				def readOnlyTxt = (disabled == "true")? 'disabled="disabled"':"";
				if(disabled) {
					input = """<input type="hidden" id="_${name}" name="_${name}" value="${(value=="true")? '1':'0'}">
								<input type="checkbox" id="${name}" name="${name}" ${readOnlyTxt} 
									class="${cssclass}" ${(value=="true")? 'checked="checked"':''}>""";
				} else {
					input = """<input type="hidden" name="_${name}"><input type="checkbox" id="${name}" name="${name}" ${readOnlyTxt} class="${cssclass}" ${(value=="true")? 'checked="checked"':''}>""";
				}
				break;
			case "url":
				input = """<input type="url" ${cProperties} maxlength="100"/>""";
				break;
			case "file-pdf":
				input = """<input type="file" ${cProperties} title="${message(code: placeholder)}" accept='application/pdf' />""";
				break;
			case "date":
				DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
				String dval ="";
				if (value != null && !value.isEmpty()) {
					int minusDays = (attrs.minusDays != null)? Integer.parseInt(attrs.minusDays):0;
					Date d = dateFormat.parse(value);
					
					GregorianCalendar gc = new GregorianCalendar();
					gc.setTime(d);
					gc.add(Calendar.DAY_OF_YEAR, minusDays);
					Date result = gc.getTime();
					dval = dateFormat.format(result);
				}
                boolean isReadOnly = readonly.equals(Boolean.TRUE.toString());
				input = """<input type="text"
						id="${name}"
						name="${name}"
						class="${cssclass}${required? "required":""} dateInput"
						value="${dval}"
						pattern="\\d{1,2}/\\d{1,2}/\\d{4}"
                        ${(isReadOnly) ? 'readonly="readonly"':''}
						${(required) ? 'required="required"':''}
						${(disabled) ?  'disabled="disabled"':''} />""";
				break;
			case "zipCode":
				input = """<input type="text" ${cProperties} pattern=".{5,9}" maxlength="9" title="${message(code:'default.invalid.string.lenght', args:[5, 9])}"/>""";
				break;
			//case "date":
			//	input = """<input type="date" class="dateInput" ${cProperties} pattern="\\d{1,2}/\\d{1,2}/\\d{4}" />""";
			//	break;
			case "colorSelect":
				def currentColor = (value=="")? '#354cf4':value;
				input = """<div class="input-append color" data-color="${currentColor}" data-color-format="hex" id="cp${number}" data-colorpicker-guid="${number}">
							<div class="color-wrapper">
								<div class="input"><input type="text" maxlength="10" required="required" name="${name}" id="${name}" value="${currentColor}"></div>
								<div class="color-box">
									<span class="add-on add-on-fix">
										<i class="input-color-selector-box" style="width: 33px; height: 28px; background-color: ${currentColor};">
										</i>
									</span>
								</div>
							</div>
						</div>""";
				break;
			case "featureGenExample":
				def exampleTitle = message(code:'image.generation.example');
				input = """<div id="feature-gen-example">
					<span class="liten-up">${ exampleTitle }</span>
					<img src="${src}"/>
				</div>""";
				break;
			case "dialogGenExample":
				def exampleTitle = message(code:'image.generation.example');
				input = """<div id="dialog-gen-example">
					<span class="liten-up">${ exampleTitle }</span>
					<img src="${src}"/>
				</div>""";
				break;
			case "waGenExample":
				def exampleTitle = message(code:'image.generation.example');
				input = """<div id="wa-gen-example">
					<span class="liten-up">${ exampleTitle }</span>
					<img src="${src}"/>
				</div>""";
				break;
            case "venueLabelLink":
                if (StringUtils.isBlank(value)) {
                    value = message(code: 'default.field.empty');
                    input = """<span class="field-value">${value}</span>""";
                } else {
                    input = """<a href="${grailsLinkGenerator.link(controller: 'location', action: 'edit', id: attrs.venueId, absolute: true)}">
                            ${value}</a>""";
                }
                break;
			default :
				value = (value != "")? value:message(code: 'default.field.empty');
				input = """<span class="field-value">${value}</span>""";
				break;
		}

		tooltip = (tooltip != null && tooltip != "") ? '<div class="questionToolTip" title="' + message(code: tooltip) + '"></div>' : "";

		sb << """<span id="${name}Div" ${!visible? 'style="display:none;"':''} >
	            <div class="col-xs-${leftColumnSize} text-to-${leftColumnPosition}">
	              <label for="${name}">${tooltip} ${message(code:label)} ${required? "*":""}</label>
	            </div>
	            <div class="col-xs-${12-leftColumnSize} text-to-${rightColumnPosition}">${input} ${(note &&  note != null) ? '<span class="liten-up">'+note+'</span>' : '' }</div>
			</span>"""

		out << sb << "<span class='clearfix'></span>"
	}


	def scheduler = {attrs ->
		def name = (attrs.name != null)? attrs.name : 'schedule';
		def type = attrs.type
		def allowEdit = (attrs.allowEdit != null)? (attrs.allowEdit=="true") : true;
		def startdate = (attrs.start != null)? attrs.start : new Date();
		def endate = (attrs.end != null)? attrs.end : '';
		def selected_days = (attrs.days != null)? attrs.days.split(",") : "0,1,2,3,4,5,6".split(",");
		def selected_hours = StringUtils.isNotBlank(attrs.hours)? attrs.hours.split("-") : ["12:00 AM","24"];
        final String initialTime = selected_hours[0];
        final String durationHours = selected_hours[1];
		StringBuilder sb = new StringBuilder();
		String hours = "";
		String hoursEnd = ""
		String daysOfWeek = "";
		def days =  [message(code:"schedule.sun"), message(code:"schedule.mon"), message(code:"schedule.tue"), message(code:"schedule.wed"), message(code:"schedule.thu"), message(code:"schedule.fri"), message(code:"schedule.sat")];
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

		for(int i=1; i<=24; i++){
			hoursEnd += "<option ${Integer.parseInt(durationHours) == i ? 'selected' : ''} >${i}</option>";
		}

		for(int i=0; i < days.size(); i++){ daysOfWeek += "<li ${selected_days.contains(i.toString()) ? 'class=\"selected\"' : ''} >${days[i]}</li>"; }

		sb << """
			<div class="scheduler ${name}" style="margin-bottom: 0px;">
				<ul class="range">
					<li><label>${message(code:"schedule.start.on")}</label><input type="text" id="start_date" name="start_date" ${!allowEdit? 'disabled="disabled"':''} value="${dateFormat.format(startdate)}" pattern="\\d{1,2}/\\d{1,2}/\\d{4}" /></li>
					<li><label>${message(code:"schedule.end")}</label><input type="text" id="end_date" name="end_date" ${!allowEdit? 'disabled="disabled"':''} value="${(endate != '') ? dateFormat.format(endate) : ''}" pattern="\\d{1,2}/\\d{1,2}/\\d{4}"/></li>
				</ul>
				<span class="date-format">* ${message(code:"schedule.date.format")}</span>
				<ul class="days">${daysOfWeek}</ul>
                <ul class="hours_range">
                 <li><label>${message(code:"schedule.start")}</label>
                     <input name="scheduler-start-time" id="scheduler-start-time" value="${initialTime}" ${!allowEdit? 'disabled="disabled"':''} readonly/>
                     <label>${message(code:"schedule.duration")}</label>
                     <select id="hoursToDisplay" name="hoursToDisplay" class="end" ${!allowEdit? 'disabled="disabled"':''}>${hoursEnd}</select>
                     <span> ${message(code:"hours")}</span>
                 </li>
                </ul>
				<span><strong id="final-scheduler-time" class="final-scheduler-msg">*</strong><br/><strong>* ${message(code:"schedule.time.msg")} <br/>* ${message(code:"schedule.info.timezone")}</strong> <br/>* ${message(code:"schedule.error.emptyDays")}</span>

				<input type="hidden" id="_${name}_days" name="_${name}_days" value="${selected_days.toString().replace("[", "").replace("]", "").replace(" ", "");}" />
				<input type="hidden" id="_${name}_hours" name="_${name}_hours" value="${initialTime+"-"+durationHours}" />
			</div>""";

		sb << """
		<script>
			var parent = \$('.${name}');
			var allowEdit = ${!allowEdit? 'false':'true'};
			Exxo.UI.vars['allowScheduleEdition'] = allowEdit;
			
			parent.find('ul li select').on('change', function(e) {
				if(!allowEdit){return;}
				var parent = \$(this).parent().parent();
		    	e.stopPropagation();

				\$('#_${name}_hours').val(parent.find('select.start').val()+"-"+parent.find('select.end').val());
			});

		    parent.find('ul li').on('click', function(e) {
				if(!allowEdit){return;}
				var item = \$(this),
				 parent = item.parent(),
				 selected = [];
		    	e.stopPropagation();

				!item.hasClass('selected') ? item.addClass('selected') : item.removeClass('selected');
				parent.find("li").each(function(index) { if(\$(this).attr("class") == "selected"){selected= selected.concat(index)} });

				if (parent.attr("class") == "days") { \$('#_${name}_days').val(selected); }
			});
		</script>""";

		out << sb
	}

	def timeZoneSelect = {attrs ->
		def thelist = TimeZone.getAvailableIDs().findAll{
						it.matches("^(Africa|America|Asia|Atlantic|Australia|Europe|Indian|Pacific)/.*|^(UTC).*")
		}
		attrs['from'] = thelist;
		attrs['value'] = (attrs['value'] ? attrs['value'] : TimeZone.getDefault())
		def date = new Date()

		// set the option value as a closure that formats the TimeZone for display
		attrs['optionValue'] = { TimeZone tz = TimeZone.getTimeZone(it);
				def shortName = tz.getDisplayName(tz.inDaylightTime(date), TimeZone.SHORT);
				def longName = tz.getDisplayName(tz.inDaylightTime(date), TimeZone.LONG);

				def offset = tz.rawOffset;
				def offsetSign = offset<0?'-':'+'
				Integer hour = Math.abs(offset / (60 * 60 * 1000))%24;
				Integer min = Math.abs(offset / (60 * 1000)) % 60;
				return "(UTC$offsetSign$hour:${min<10? '0'+min:min}) $tz.ID "

		}
		out << select(attrs)
	}
	
	def paginationInfo = {attrs ->
		StringBuilder sb = new StringBuilder();
		String defaultConfiguration = '{"rows":10,"page":0,"text":"","columnOrder":0,"orderDir":"asc"}';
		try {
			if(session.paginationInfo && session.paginationInfo.get(attrs['code'])){
				sb << session.paginationInfo.get(attrs['code']);
				//sb << defaultConfiguration;
			} else {
				sb << defaultConfiguration;
			}
		}catch(Exception ex){
			log.error(ex);
			sb << defaultConfiguration;
		}
		
		out << sb
	}
	
	/**
	 * Render the App Preview
	 */
	def appPreview = { attrs ->
		CompanyLocation location = attrs.location;
		
		HashMap jsonMap = null;
		
		/* Get Location Documents for Preview */
		def documents = null;
		HashMap<String, Object> docCategories = new HashMap<String, Object>();
		try {
			documents = documentService.findContentEnableAndConvertInHash(location);
			def docList = null;
			for(document in documents){
				if(docCategories.containsKey(document.type)){
					docList = docCategories.get(document.type);
					docList.add(document);
					docCategories.put(document.type, docList);
				} else { // New Category; create the list
					docList = [];
					docList.add(document);
					docCategories.put(document.type, docList);
				}
			} //End of for documents
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		
		DateFormat hoursFormat = new SimpleDateFormat("h:mm a");
		DateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat fullDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		
		//Get Location Offers for preview
		def offers = null;
		try {
			offers = contentService.findOffersEnables(location);
			
			
			for(offer in offers){
				String days = "";
				for(day in offer.schedule.days){
					days += (days == "")? day:","+day;
				}
				offer.schedule.days = days;
				def hours = offer.schedule.hours;
				Date startDate = fullDateFormat.parse(simpleDateFormat.format(new Date()) + " " + hours[0]);
				Date endDate = fullDateFormat.parse(simpleDateFormat.format(new Date()) + " " + hours[1]);
				offer.schedule.hours = hoursFormat.format(startDate) + " and " + hoursFormat.format(endDate);
			}
			
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		
		def banners = null;
		try {
			banners = contentService.findContentEnableAndConvertInHash(location, null);
			for(banner in banners){
				if(banner.featured){
					String days = "";
					for(day in banner.schedule.days){
						days += (days == "")? day:","+day;
					}
					banner.schedule.days = days;
					def hours = banner.schedule.hours;
					Date startDate = fullDateFormat.parse(simpleDateFormat.format(new Date()) + " " + hours[0]);
					Date endDate = fullDateFormat.parse(simpleDateFormat.format(new Date()) + " " + hours[1]);
					banner.schedule.hours = hoursFormat.format(startDate) + " and " + hoursFormat.format(endDate);
					banner.expirationText = getExpirationText(banner.schedule.expirationDate*1000, location.timezone);
				}
			}
		} catch(Exception e){
		}
		
		out << g.render(template:'/layouts/tags/appViewExampleTemplate',
			model:[isFullPreview: true, location: location, appTheme: location?.activeSkin, 
				docCategories: docCategories, offers: offers, banners: banners]);
	}
	
	def expirationDate = { attrs ->
		long expiration = attrs.expiration * 1000;
		
		out << getExpirationText(expiration, attrs.locationTimeZone);
		
		/*
		if(expiration == 0){
			out << "Never"	
		} else {
			String locationTimeZone = attrs.locationTimeZone;
			Date expirationDate = new Date(expiration);
		
			DateFormat dformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			DateFormat timeZoneID = new SimpleDateFormat("Z");
			Date utcDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z").parse(dformat.format(expirationDate) +" "+  timeZoneID.format(expirationDate));
						
			DateFormat simpleFormat = new SimpleDateFormat("MM/dd/yyyy");
			simpleFormat.setTimeZone(TimeZone.getTimeZone(locationTimeZone));
			
			out << "" + simpleFormat.format(utcDate);
		}*/
	}
	
	public String getExpirationText(long expiration, String locationTimeZone){
		
		if(expiration == 0){
			return "Never";
		} else {
			Date expirationDate = new Date(expiration);
		
			DateFormat dformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			DateFormat timeZoneID = new SimpleDateFormat("Z");
			Date utcDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z").parse(dformat.format(expirationDate) +" "+  timeZoneID.format(expirationDate));
						
			DateFormat simpleFormat = new SimpleDateFormat("MM/dd/yyyy");
			simpleFormat.setTimeZone(TimeZone.getTimeZone(locationTimeZone));
			
			return "" + simpleFormat.format(utcDate);
		}
	}
}