<html>
<head>
	<meta name='layout' content='anonymous' />
	<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_register.js')}"></script>
	<script>
		Exxo.module = "register";
		Exxo.UI.urls['getStatesByCountry'] = '<g:createLink controller="common" action="statebycountry"/>';
		Exxo.UI.vars['validationDetail'] = "${message(code:'password.validation.description')}";
		Exxo.UI.vars['confirmationFails'] = "${message(code:'password.validation.confirmation.fail')}";
	</script> 
</head>

<body>
	<div id="first">
		<h1>${message(code: 'sign.up.title')}</h1>
		<div id="SuppressScrollX" class="contentHolder">
			<div class="content">
				<g:form action="registerAccount" name="register-form" method="post" class="exxo-form">
					<g:if test="${flash.error}">
			      	 	<div class="alert alert-error" style="margin-right: 15px;">  
							  <a class="close" data-dismiss="alert">×</a>
			      	 			${flash.error}
			      	 				<ul class="error-list">
				      	 			<g:hasErrors bean="${user}">	       		  
								  		<g:eachError bean="${user}" var="error">
												<li class="breakWord" <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
											</g:eachError>
										
									</g:hasErrors>						
						 			<g:hasErrors bean="${company}">	        			
								  		
											<g:eachError bean="${company}" var="error">
												<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
											</g:eachError>
															  
						 			</g:hasErrors>						 
						 			<g:hasErrors bean="${integrator}">	        			
								  		
											<g:eachError bean="${integrator}" var="error">
												<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
											</g:eachError>
															  	
						 			</g:hasErrors>
						 			</ul>
						 		${flash.error=null}
						 	</div>			 
			         </g:if> 
			          
		      		 <g:if test="${flash.success}">
			        		<div class="alert alert-success"  style="margin-right: 15px;">  
							  <a class="close" data-dismiss="alert">×</a>  
							  ${flash.success} 
							</div> 
							 ${flash.success=null} 
		      		</g:if>
		      		
					<!-- Account Information -->
					<h2>${message(code: 'sign.up.subtitle')}</h2>
					
					<!-- Email  -->				
					<custom:editField type="email" name="email" required="${true}" value="${user.email}" 
						label="field.email" placeholder="${message(code: 'field.email')}" leftColumnSize="5" leftColumnPosition="left"/>
						
					<!-- Password  -->				
					<custom:editField type="password" name="password" required="${true}" value="" 
						label="field.password" placeholder="${message(code: 'field.password')}" leftColumnSize="5" leftColumnPosition="left"/>
						
					<!-- Retype Password  -->				
					<custom:editField type="password" name="confirmPassword" required="${true}" value="" 
						label="field.password.confirm" placeholder="${message(code: 'field.password.confirm')}" leftColumnSize="5" leftColumnPosition="left"/>
						
					<!-- First Name  -->				
					<custom:editField type="text" name="firstName" required="${true}" value="${user.firstName}" 
						label="field.first.name" placeholder="${message(code: 'field.first.name')}" leftColumnSize="5" leftColumnPosition="left"/>
						
					<!-- Last Name  -->				
					<custom:editField type="text" name="lastName" required="${true}" value="${user.lastName}" 
						label="field.last.name" placeholder="${message(code: 'field.last.name')}" leftColumnSize="5" leftColumnPosition="left"/>
					
					<!-- Type of Account -->
					<div class="col-xs-5 text-to-left">
		              <label for="email"> ${message(code:'field.account.tupe')} *</label>
		            </div>
		            <div class="col-xs-7 text-to-left">
		            	<g:select name="typeOfCompany.id" id="typeOfCompany" from="${typeofAccount}" value="${company.typeOfCompany?.id}"  
							optionKey="id" optionValue="name" />
		            </div>
		            <span class="clearfix"></span>
					
					<!-- Company Information -->
					<div class="form-block-title">${message(code: 'sign.up.company.information')}</div>
					
					<!-- Company Name  -->				
					<custom:editField type="text" name="name" required="${true}" value="${company.name}" 
						label="field.company.name" placeholder="${message(code: 'field.company.name')}" leftColumnSize="5" leftColumnPosition="left"/>
					
					<!-- Web Site  -->				
					<custom:editField type="url" name="webSite" required="${false}" value="${company.webSite}" max="100"
						label="field.web.site" placeholder="${message(code: 'field.web.site')}" leftColumnSize="5" leftColumnPosition="left"/>
						
					<!-- Phone Number  -->				
					<custom:editField type="phoneNumber" name="phoneNumber" required="${true}" value="${company.phoneNumber}"
						label="field.phone.number" placeholder="${message(code: 'field.phone.number.example')}" leftColumnSize="5" leftColumnPosition="left"/>
						
					<!-- Type of Company -->
					<div class="control-group" id="controlTypeOfCompany">
						<div class="col-xs-5 text-to-left">
			              <label for="email"> ${message(code:'default.field.typeOfCompany')} *</label>
			            </div>
			            <div class="col-xs-7 text-to-left">
			            	<custom:selectWithOptGroupCompanyType id="type" name="type" from="${typesCompany}" value="${company.type?.code}" />
			            </div>
			            <span class="clearfix"></span>
					</div>
					
					<!-- Detail of Other Type of Company -->
					<div class="control-group" id="controlCompanyOtherType" >
						<custom:editField type="text" name="otherType" required="${true}" value="${company.otherType}" max="30"
						label="field.other.type" placeholder="${message(code: 'field.other.type')}" leftColumnSize="5" leftColumnPosition="left"/>
					</div>
					
					<!-- Address  -->				
					<custom:editField type="text" name="address" required="${true}" value="${company.address}" max="100"
						label="field.address" placeholder="${message(code: 'field.address')}" leftColumnSize="5" leftColumnPosition="left"/>
					
					<!-- Country -->
					<div class="col-xs-5 text-to-left">
		              <label for="email"> ${message(code:'default.field.country')} *</label>
		            </div>
		            <div class="col-xs-7 text-to-left">
		            	<g:select name="country.id" id="country" from="${countries}" optionKey="id"  
						optionValue="${ {shortName->g.message(code:'country.code.'+shortName) } }" value="${company.country?.id}"/>
		            </div>
		            <span class="clearfix"></span>
					
					<!-- State -->
					<g:if test="${company.state}">
					<div id="stateDiv" class='control-group ${hasErrors(bean:company,field:'state','field-error')}','field-error')}' >
					</g:if>
					<g:else>
					<div id="stateDiv" class='control-group ${hasErrors(bean:company,field:'state','field-error')}' style="display:none;">
					</g:else>
						<div class="col-xs-5 text-to-left">
			              <label for="email"> ${message(code:'default.field.stateOrProvince')} *</label>
			            </div>
			            <div class="col-xs-7 text-to-left">
			            	<g:select name="state.id" id="state" from="${states}" optionKey="id" 
							 optionValue="${ {name->g.message(code:'state.code.'+name) } }" value="${company.state?.id}" />
			            </div>
			            <span class="clearfix"></span>
					</div>
					
					<g:if test="${company.state}">
						<div id="stateNameDiv" class='control-group ${hasErrors(bean:company,field:'stateName','field-error')}' style="display:none;">
					</g:if>
					<g:else>
						<div id="stateNameDiv" class='control-group ${hasErrors(bean:company,field:'stateName','field-error')}'>
					</g:else>						
						<custom:editField type="text" name="stateName" required="${false}" value="${company.stateName}" max="20"
							label="default.field.stateOrProvince" placeholder="${message(code: 'default.field.stateOrProvince')}" leftColumnSize="5" leftColumnPosition="left"/>
					</div>
					
					<custom:editField type="text" name="city"  max="100" required="${true}" value="${company.city}"
							label="field.city" placeholder="${message(code: 'field.city')}" leftColumnSize="5" leftColumnPosition="left"/>
							
					<custom:editField type="zipCode" name="zipCode" required="${true}" value="${company.zipCode}"
							label="field.zip.code" cssclass="onlyNumbers" placeholder="${message(code: 'field.zip.code')}" leftColumnSize="5" leftColumnPosition="left"/>
											
					<!-- Extra Information -->
					<div class="form-extrainformation">
						<div class="form-block-title">${message(code: 'sign.up.extra.information')}</div>
					
						<!-- Number Of Employees -->
						<div class="col-xs-5 text-to-left">
			              <label for="email"> ${message(code:'field.number.employees')} *</label>
			            </div>
			            <div class="col-xs-7 text-to-left">
			            	<g:select name="numberOfEmployees"  from="${['1-5','6-12','13-25','more']}"
									value="${integrator?.numberOfEmployees}" />	
			            </div>
			            <span class="clearfix"></span>
			            
						<!-- cediaNumber -->
						<custom:editField type="text" name="cediaNumber" required="${false}" value="${integrator?.cediaNumber}"
								label="field.cedia.number" placeholder="${message(code: 'field.cedia.number')}" leftColumnSize="5" leftColumnPosition="left"/>
						
						<!-- contractorLicense -->
						<custom:editField type="text" name="contractorLicense" required="${false}" value="${integrator?.contractorLicense}"
								label="field.contractor.license.number" tooltip="${message(code:'contractor.licence.number.help')}" placeholder="${message(code: 'field.contractor.license.number')}" leftColumnSize="5" leftColumnPosition="left"/>
						
						<!-- reseller -->
						<custom:editField type="text" name="reseller" required="${false}" value="${integrator?.reseller}"
								label="field.reseller" placeholder="${message(code: 'field.reseller')}" leftColumnSize="5" leftColumnPosition="left"/>
								
						<!-- taxId -->
						<custom:editField type="tax" name="taxId" required="${false}" value="${integrator?.taxId}" max="10" pattern="" 
								label="field.tax.id" placeholder="${message(code: 'field.tax.id.example')}" leftColumnSize="5" leftColumnPosition="left"/>
						
					</div>
					
					<input type="submit" class="btn-primary" value="${message(code: 'btn.register')}" />
				</g:form>
			</div>
		</div>
		<hr>
		<p id="twoa">
			<g:link controller="login" action="auth" class="signin">
					<g:message code="home.AlreadyhaveAnAccount" />
				</g:link>
		</p>
	</div>
</body>
</html>
