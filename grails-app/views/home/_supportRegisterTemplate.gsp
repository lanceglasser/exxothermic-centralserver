<%@ page contentType="text/html"%>

<!DOCTYPE html>
<html>
	<head>
				
	</head>
	<body>
			<p><g:message code="email.support.register.account"/> 
			<br>
			<p><b><g:message code="user.name"/>:</b>${user.firstName}  ${user.lastName}</p>		
			<p><b><g:message code="default.field.companyName"/>:</b>${company.name}</p>			
			<p><b><g:message code="default.field.typeOfAccount"/>:</b>${company.typeOfCompany.name}</p>			
			<p><b><g:message code="default.field.country"/>:</b>${message(code:"country.code." + company.country.shortName.encodeAsHTML())}</p>		
			<g:if test="${company.state}">
				<p><b><g:message code="default.field.stateOrProvince"/>:</b>${g.message(code:"state.code." + company.state.name)}</p>	
			</g:if>	
			<g:else>
				<p><b><g:message code="default.field.stateOrProvince"/>:</b>${company.stateName.encodeAsHTML()}</p>	
			</g:else>
					
			<p><b><g:message code="default.field.city"/>:</b>${company.city}</p>			
			<p><b><g:message code="default.field.zipCode"/>:</b>${company.zipCode}</p>			
			<g:if test="${integrator}">
				<p><b><g:message code="default.field.numberOfEmployees"/>:</b>${integrator.numberOfEmployees}</p>
			</g:if>						
	</body>
</html>
