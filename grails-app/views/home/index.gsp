<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title>ExXothermic Cloud Service</title>

<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'widgets.css')}">

<script
	src="${resource(dir: 'themes/default/js/widget', file: 'jquery.exxo.widget.js')}"></script>
<script type="text/javascript"
	src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1','packages':['corechart']}]}"></script>
<!-- Gridster Start -->
<script
	src="${resource(dir: 'themes/default/js', file: 'jquery.draggable.js')}"></script>
<script
	src="${resource(dir: 'themes/default/js', file: 'jquery.coords.js')}"></script>
<script
	src="${resource(dir: 'themes/default/js/widget', file: 'jquery.collision.js')}"></script>
<script
	src="${resource(dir: 'themes/default/js/widget', file: 'utils.js')}"></script>
<script
	src="${resource(dir: 'themes/default/js/widget', file: 'jquery.gridster.js')}"></script>
<script
	src="${resource(dir: 'themes/default/js/widget', file: 'jquery.gridster.extras.js')}"></script>

<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.gridster.css')}">
<!-- Gridster End -->

<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
<script
	src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
<script
	src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
<script
	src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_user_dashboard.js')}"></script>

<g:render template="/widget/dashboardGeneralConfig"></g:render>

<script>
	Exxo.module = "index";
	Exxo.UI.vars["dashboard-name"] = "mainDashboard";
</script>

</head>

<body>
	<section class="introp">
		<div class="intro-body">
			<div class="container full-width">
				<div class="row" style="max-width: 1535px;">
					<div class="col-md-12" id="internal-content">

						<div class="col-md-12 dashboard-header-line">
							<!-- <h1>						  		
						  			<g:link class="title-dash" controller="home" action="index">
										<img class="title-icon" src="${resource(dir: 'themes/default/img/icons', file: 'home-blue.png')}" alt="Home"/> 
							  			${ message(code:'dashboard.title', args:[message(code:'main')]) }
									</g:link>						  			
						  		</h1>  -->
							<div id="crumbs">
								<ul class="left">
									<li><a href="#" class="current"><img
											class="title-icon"
											src="${resource(dir: 'themes/default/img/icons', file: 'home-white.png')}"
											alt="Home" /></a></li>
									<!--  <li><a href="#2">Company Dashboard</a></li>
											<li><a href="#3" class="current">Location Dashboard</a></li>  -->
								</ul>
								
								<span class="add-widget circle left" title="${ message(code:'add.widget') }"></span>
								<span id="how-use-widgets-btn" class="help-widget circle left"  title="${ message(code:'how.use.widgets') }"></span>
								
							</div>
							 
							 
							<span class="clearfix"></span>
						</div>
						<span class="clearfix"></span>

<div style="width: 1500px; position: relative;">
						<!-- Renders the Main Dashboard for Current User -->
						<user:mainDashboard />
</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</body>
</html>

