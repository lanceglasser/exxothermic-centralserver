<!DOCTYPE html>
<html>
	<head>
		<title></title>
		
		<script>

		Exxo.HelpCenter.initHelpPage();
		
		</script>
	</head>
	<body>
		<h1 class="category-title">${ message(code:'mobile.app.components') }</h1>
		<ul class="category-list">
		<li>
	      <h3>
	        <a href="#" class="subcat-link" ref="how">
	          <span aria-hidden="true" class="smf-icon expand open"></span>
	          ${ message(code: 'app.theme.settings') }
	        </a>
	      </h3>
	      <ul id="subcat-list-how" class="subcat tutorials" style="display: block;">
	        <li>
	          <a href="#" class="link help-link" template="appThemeSettings" cat="app">
	            <span aria-hidden="true" class="smf-icon right link"></span>
	            ${ message(code: 'background.images.and.colors') }
	          </a>
	        </li>
	      </ul>
	    </li>
	    <li>
	      <h3>
	        <a href="#" class="subcat-link" ref="how">
	          <span aria-hidden="true" class="smf-icon expand open"></span>
	          ${ message(code: 'banners') }
	        </a>
	      </h3>
	      <ul id="subcat-list-how" class="subcat tutorials" style="display: block;">
	        <li>
	          <a href="#" class="link help-link" template="banner" cat="app">
	            <span aria-hidden="true" class="smf-icon right link"></span>
	            ${ message(code: 'background.images.and.others') }
	          </a>
	        </li>
	      </ul>
	    </li>
	    <li>
	      <h3>
	        <a href="#" class="subcat-link" ref="how">
	          <span aria-hidden="true" class="smf-icon expand open"></span>
	          ${ message(code: 'offers') }
	        </a>
	      </h3>
	      <ul id="subcat-list-how" class="subcat tutorials" style="display: block;">
	        <li>
	          <a href="#" class="link help-link" template="offer" cat="app">
	            <span aria-hidden="true" class="smf-icon right link"></span>
	            ${ message(code: 'background.images.and.others') }
	          </a>
	        </li>
	      </ul>
	    </li>	    
		</ul>
	</body>
</html>

