<!DOCTYPE html>
<html>
	<head>
		<title></title>
		
		<script>

		Exxo.HelpCenter.initHelpPage();
		
		</script>
	</head>
	<body>
		<h1 class="category-title">
			<a href="#" class="link help-link" template="app" cat="app">
	            <span aria-hidden="true" class="smf-icon right link"></span>
	            ${ message(code: 'mobile.app.components') }
	         </a>
	         >
			<a href="#">
	            <span aria-hidden="true" class="smf-icon right link"></span>
	            ${ message(code: 'app.theme.settings') }
	          </a>
		</h1>
		
		<h2>${ message(code: 'background.images.and.colors') }</h2>
		
		<div class="col-md-12">
			<div class="featuredHelp" style="float: left;" >   
		   		<span>${message(code:'default.field.appskin.help.ads.background')}
		   		<br/>
		   		<img src="${ resource(dir: 'themes/default/img', file: 'appSkin2.png') }" style="width: 245px;">	
		   		</span>
			</div>
			
			<div class="helpAppSkinPColorArrow" >   
		   		<span>	   		
		   		<img style="float: left;" src="${ resource(dir: 'themes/default/img', file: 'arrow-left-icon.png') }">
		   		${message(code:'default.field.title')} & ${message(code:'default.field.main.color')}	
		   		</span>
		   		<br/>
		   		
		   		<div class="helpsColorSec" > 		   		
			   		<div class="helpAppSkinSColorArrow" >   		
				   		<img src="${ resource(dir: 'themes/default/img', file: 'arrow-left-long.png') }">
				   	</div>	
			   		<div class="helpURL" >
				   		${message(code:'default.field.secondary.color')}			   		
				   	</div>
		   		</div>
		   		
		   		<span class="mainColorBottom">
		   			<div class="centerText" >
		   				${message(code:'default.field.appskin.help.pcolor.info')}
		   			</div>
		   			<img src="${ resource(dir: 'themes/default/img', file: 'arrow-both.png') }">	
		   		</span>	
		   		
			</div>
				
			<div class="dialogHelp" style="float: left; padding-left: 70px;">   
		   		<span>${message(code:'default.field.appskin.help.dialog')}
		   		<br/>
		   		<img src="${ resource(dir: 'themes/default/img', file: 'appSkin1.png') }" style="width: 245px;">	
		   		</span>
			</div>
			<span class="clearfix"></span>
		</div>
		<!-- 
		<div>
		
		<article class="tabs">
			<section id="tab1">
				
				<div class="helpAppSkinPColorArrow" >   
			   		<span>	   		
			   		<img style="float: left;" src="${ resource(dir: 'themes/default/img', file: 'arrow-left-icon.png') }">
			   		${message(code:'default.field.title')} & ${message(code:'default.field.main.color')}	
			   		</span>
			   		<br/>
			   		
			   		<div class="helpsColorSec" > 		   		
				   		<div class="helpAppSkinSColorArrow" >   		
					   		<img src="${ resource(dir: 'themes/default/img', file: 'arrow-left-long.png') }">
					   	</div>	
				   		<div class="helpURL" >
					   		${message(code:'default.field.secondary.color')}			   		
					   	</div>
			   		</div>
			   		
			   		<span class="mainColorBottom">
			   			<div class="centerText" >
			   				${message(code:'default.field.appskin.help.pcolor.info')}
			   			</div>
			   			<img src="${ resource(dir: 'themes/default/img', file: 'arrow-both.png') }">	
			   		</span>	
			   		
				</div>
				
				<div class="dialogHelp" >   
			   		<span>Info:	${message(code:'default.field.appskin.help.dialog')}
			   		<br/>
			   		<img src="${ resource(dir: 'themes/default/img', file: 'appSkin1.png') }">	
			   		</span>
				</div>				
			</section>		
		</article>
		</div> -->
		<span class="clearfix"></span>
	</body>
</html>

