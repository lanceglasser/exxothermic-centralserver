<!DOCTYPE html>
<html>
	<head>
		<title></title>
		
		<script>

		Exxo.HelpCenter.initHelpPage();
		
		</script>
	</head>
	<body>
		<h1 class="category-title">${ message(code:'widgets') }</h1>
		<ul class="category-list">
		<li>
	      <h3>
	        <a href="#" class="subcat-link" ref="how">
	          <span aria-hidden="true" class="smf-icon expand open"></span>
	          ${ message(code:'how.it.works') }
	        </a>
	      </h3>
	      <ul id="subcat-list-how" class="subcat tutorials" style="display: block;">
	        <li>
	          <a href="#" class="link help-link" template="widgetsUse" cat="widgets">
	            <span aria-hidden="true" class="smf-icon right link"></span>
	            ${ message(code: 'widgets.use') }
	          </a>
	        </li>
	        <li>
	          <a href="#" class="link help-link" template="widgetsAdd" cat="widgets">
	            <span aria-hidden="true" class="smf-icon right link"></span>
	            ${ message(code: 'widgets.how.add') }
	          </a>
	        </li>
	      </ul>
	    </li>
	    <li>
	      <h3>
	        <a href="#" class="subcat-link" ref="stats">
	          <span aria-hidden="true" class="smf-icon expand open"></span>
	          ${ message(code:'stats.widgets') }
	        </a>
	      </h3>
	      <ul id="subcat-list-stats" class="subcat tutorials">
	        <li>
	          <a href="#" class="link help-link" template="widgetsVenueServersStats" cat="widgets">
	            <span aria-hidden="true" class="smf-icon right link"></span>
	            ${ message(code:'venue.servers.status') }
	          </a>
	        </li>
	      </ul>
	    </li>
	    <li>
	      <h3>
	        <a href="#" class="subcat-link" ref="lists">
	          <span aria-hidden="true" class="smf-icon expand open"></span>
	          ${ message(code:'list.widgets') }
	        </a>
	      </h3>
	      <ul id="subcat-list-lists" class="subcat tutorials">
	        <li>
	          <a href="#" class="link help-link" template="widgetsVenueServersList" cat="widgets">
	            <span aria-hidden="true" class="smf-icon right link"></span>
	            ${ message(code:'help.venue.servers') }
	          </a>
	        </li>
	        <li>
	          <a href="#" class="link help-link" template="widgetsCompaniesList" cat="widgets">
	            <span aria-hidden="true" class="smf-icon right link"></span>
	            ${ message(code:'help.companies') }
	          </a>
	        </li>
	        <li>
	          <a href="#" class="link help-link" template="widgetsVenuesList" cat="widgets">
	            <span aria-hidden="true" class="smf-icon right link"></span>
	            ${ message(code:'help.venues') }
	          </a>
	        </li>
	      </ul>
	    </li>
	    <li>
	      <h3>
	        <a href="#" class="subcat-link" ref="others">
	          <span aria-hidden="true" class="smf-icon expand open"></span>
	          ${ message(code:'other.widgets') }
	        </a>
	      </h3>
	      <ul id="subcat-list-others" class="subcat tutorials">
	        <li>
	          <a href="#" class="link help-link" template="widgetsShortcuts" cat="widgets">
	            <span aria-hidden="true" class="smf-icon right link"></span>
	            ${ message(code:'shortcuts') }
	          </a>
	        </li>
	        <li>
	          <a href="#" class="link help-link" template="widgetsCompany" cat="widgets">
	            <span aria-hidden="true" class="smf-icon right link"></span>
	            ${ message(code:'widgets.company.info') }
	          </a>
	        </li>
	        <li>
	          <a href="#" class="link help-link" template="widgetsVenue" cat="widgets">
	            <span aria-hidden="true" class="smf-icon right link"></span>
	            ${ message(code:'widgets.venue.info') }
	          </a>
	        </li>
	      </ul>
	    </li>
		</ul>
	</body>
</html>

