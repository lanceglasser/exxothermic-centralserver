<!DOCTYPE html>
<html>
	<head>
		<title></title>
		
		<script>

		Exxo.HelpCenter.initHelpPage();
		
		</script>
	</head>
	<body>
		<h1 class="category-title">${ message(code:'getting.started') }</h1>
		<ul class="category-list">
		<li>
	      <h3>
	        <a href="#" class="subcat-link" ref="how">
	          <span aria-hidden="true" class="smf-icon expand open"></span>
	          ${ message(code: 'main.options') }
	        </a>
	      </h3>
	      <ul id="subcat-list-how" class="subcat tutorials" style="display: block;">
	        <li>
	          	<a href="#" class="link help-link" template="app" cat="app">
	            	<span aria-hidden="true" class="smf-icon right link"></span>
	            	${ message(code: 'mobile.app.components') }
	         	</a>
	        </li>
	        <li>
	          	<a href="#" class="link help-link" template="widgets" cat="widgets">
	            	<span aria-hidden="true" class="smf-icon right link"></span>
	            	${ message(code: 'widgets') }
	         	</a>
	        </li>
	      </ul>
	    </li>
		</ul>
	</body>
</html>