<!DOCTYPE html>
<html>
	<head>
		<title></title>
		
		<script>

		Exxo.HelpCenter.initHelpPage();
		
		</script>
	</head>
	<body>
		<h1 class="category-title">
			<a href="#" class="link help-link" template="app" cat="app">
	            <span aria-hidden="true" class="smf-icon right link"></span>
	            ${ message(code: 'mobile.app.components') }
	         </a>
	         >
			<a href="#">
	            <span aria-hidden="true" class="smf-icon right link"></span>
	            ${ message(code: 'banners') }
	          </a>
		</h1>
		
		<h2>${ message(code: 'background.images.and.others') }</h2>
		
		<div class="col-md-12">
			<div class="featuredHelp" style="float: left;" >
		   		<img src="${ resource(dir: 'themes/default/img', file: 'featured.png') }" style="width: 245px;">	
		   		</span>
			</div>
			
			<div class="helpAppSkinPColorArrow" style="top: 20px;"> 
				<span class="helpRight">${message(code:'dialog.image')} </span>
		   		<span>	   		
		   		<img style="float: right;" src="${ resource(dir: 'themes/default/img', file: 'arrow-right-long.png') }">
		   		</span>
		   		<br/>
		   		
		   		<span class="helpLeft">${message(code:'featured.image')} </span>
		   		<span>	   		
		   		<img style="float: left;" src="${ resource(dir: 'themes/default/img', file: 'arrow-left-semi-long.png') }">
		   		</span>
		   		<br/>
		   		
		   		<div class="helpMoreInfo" style="margin-top: 133px;"> 
			   		<div class="helpURL" >
				   		${message(code:'default.field.content.help.url')}			   		
				   	</div>	
				   	<div class="helpURLArrow" >   		
				   		<img src="${ resource(dir: 'themes/default/img', file: 'arrow-right-long.png') }">
				   	</div>	
		   		</div>
			</div>
				
			<div class="dialogHelp" style="float: left; padding-left: 70px;"> 
		   		<img src="${ resource(dir: 'themes/default/img', file: 'dialog1.png') }" style="width: 245px;">	
		   		</span>
			</div>
			<span class="clearfix"></span>
		</div>
		<span class="clearfix"></span>
	</body>
</html>

