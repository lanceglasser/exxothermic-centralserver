<!DOCTYPE html>
<html>
	<head>
		<title></title>
		
		<script>
			Exxo.UI.vars["cat"] = "${cat}";
			Exxo.UI.urls["show-help-page-url"] = "${g.createLink(controller: 'home', action: 'showHelp', absolute: true)}";
		</script>
	</head>
	<body>
	<!-- Hidden Link for Initial Call -->
	<g:remoteLink controller="home" action="showHelp" id="initialLoadLink" update="main-help-container" style="display: none;"
    					params="[template:template]" asynchronous="true" after="Exxo.HelpCenter.initHelpPage()" ></g:remoteLink>
    					
		<div class="col-md-12" id="help-container" style="display: none;">
		  	<nav>
		  		<ul>
				  <li>
				    <g:remoteLink controller="home" action="showHelp" id="startLink" class="helpCategoryLink firstChild" update="main-help-container"
    					params="[template:'start']" asynchronous="true" after="Exxo.HelpCenter.initHelpPage()">${ message(code:'getting.started') }</g:remoteLink>
				  </li>
				  <li>
				    <g:remoteLink controller="home" action="showHelp" id="appLink" update="main-help-container" class="helpCategoryLink"
    					params="[template:'app']" asynchronous="true" after="Exxo.HelpCenter.initHelpPage()">${ message(code:'mobile.app.components') }</g:remoteLink>
				  </li>
				  <li>
				    <g:remoteLink controller="home" action="showHelp" id="widgetsLink" update="main-help-container" class="helpCategoryLink"
    					params="[template:'widgets']" asynchronous="true" after="Exxo.HelpCenter.initHelpPage()">
    					${ message(code:'widgets') }</g:remoteLink>
				  </li>
				</ul>
			</nav>
			<!-- End of Menu -->
				
			<!-- Main Content -->
			<div class="main-content">
				<div class="module standard no-inset min-height" style="min-height: 522px;">
					<div class="category" style="height: 100%; position: relative; padding: 15px;">
				    	<div id="main-help-container" style="max-height: 548px; overflow: auto;">
				    		<!-- 
				  			
				  			 -->
						</div>
					</div>
				</div>
			</div>
			<!-- End of Main Content -->
		</div>
	</body>
</html>

