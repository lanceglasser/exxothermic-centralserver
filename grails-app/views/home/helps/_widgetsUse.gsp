<!DOCTYPE html>
<html>
	<head>
		<title></title>
		
		<script>

		Exxo.HelpCenter.initHelpPage();
		
		</script>
	</head>
	<body>
		<h1 class="category-title">
			<a href="#" class="link help-link" template="widgets" cat="widgets">
	            <span aria-hidden="true" class="smf-icon right link"></span>
	            ${ message(code: 'dashboard.widgets', args:[message(code:'widgets')]) }
	         </a>
	         >
			<a href="#">
	            <span aria-hidden="true" class="smf-icon right link"></span>
	            ${ message(code: 'widgets.use') }
	          </a>
		</h1>
		
		<div class="col-md-12">
			<div class="col-md-6" style="text-align: justify;">
			${ message(code:'widgets.user.1') }
			${ message(code:'widgets.user.2') }
			${ message(code:'widgets.user.3') }
			
			</div>
			
			<div class="col-md-6">
				${ message(code:'widgets.user.4') }
				${ message(code:'widgets.user.5') }
			</div>
			
			<span class="clearfix"></span>
		</div>
		<span class="clearfix"></span>
		<br/>
		<div class="col-md-12" style="text-align: center;">
			<img src="${resource(dir: 'themes/default/img/helps', file: 'main_dashboard.png')}"/>
		</div>
		<span class="clearfix"></span>
	</body>
</html>

