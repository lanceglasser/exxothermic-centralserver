<!DOCTYPE html>
<html>
	<head>
		<title></title>
		
		<script>

		Exxo.HelpCenter.initHelpPage();
		
		</script>
	</head>
	<body>
		<h1 class="category-title">
			<a href="#" class="link help-link" template="widgets" cat="widgets">
	            <span aria-hidden="true" class="smf-icon right link"></span>
	            ${ message(code: 'dashboard.widgets', args:[message(code:'widgets')]) }
	         </a>
	         >
			<a href="#">
	            <span aria-hidden="true" class="smf-icon right link"></span>
	            ${ message(code: 'shortcuts') }
	          </a>
		</h1>
		
		<div class="col-md-12">
			<div class="col-md-6" style="text-align: justify;">
				${ message(code:'widgets.shortcuts.1', args:[mycentralserver.utils.Constants.MAXIMUM_SHORTCUTS]) }
				${ message(code:'widgets.shortcuts.2') }
			</div>
			
			<div class="col-md-6">
				<img src="${resource(dir: 'themes/default/img/helps', file: 'widget_shortcuts.png')}" width="350px;">
			</div>
			
			<span class="clearfix"></span>
		</div>
		<span class="clearfix"></span>
		<br/>
		<div class="col-md-12">
			<div class="col-md-6" style="text-align: justify;">
				${ message(code:'widgets.shortcuts.3') }
			</div>
			
			<div class="col-md-6">
				<img src="${resource(dir: 'themes/default/img/helps', file: 'widgets_shortcuts_config.png')}" width="350px;">
			</div>
			
			<span class="clearfix"></span>
		</div>
		<span class="clearfix"></span>
	</body>
</html>

