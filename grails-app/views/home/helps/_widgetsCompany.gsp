<!DOCTYPE html>
<html>
	<head>
		<title></title>
		
		<script>

		Exxo.HelpCenter.initHelpPage();
		
		</script>
	</head>
	<body>
		<h1 class="category-title">
			<a href="#" class="link help-link" template="widgets" cat="widgets">
	            <span aria-hidden="true" class="smf-icon right link"></span>
	            ${ message(code: 'dashboard.widgets', args:[message(code:'widgets')]) }
	         </a>
	         >
			<a href="#">
	            <span aria-hidden="true" class="smf-icon right link"></span>
	            ${ message(code: 'widgets.company.info') }
	          </a>
		</h1>
		
		<div class="col-md-12">
			<div class="col-md-6" style="text-align: justify;">
				${ message(code:'widgets.company.1') }				
			</div>
			
			<div class="col-md-6">
				${ message(code:'widgets.company.2') }
			</div>
			
			<span class="clearfix"></span>
		</div>
		<span class="clearfix"></span>
		<br/>
		<div class="col-md-12">
			<img src="${resource(dir: 'themes/default/img/helps', file: 'widget_company.png')}" style="width: 100%;">
			<span class="clearfix"></span>
		</div>
		<span class="clearfix"></span>
		<br/>
		<div class="col-md-12">
			<img src="${resource(dir: 'themes/default/img/helps', file: 'change_company_logo.png')}" style="width: 100%;">
			<span class="clearfix"></span>
		</div>
		<span class="clearfix"></span>
	</body>
</html>

