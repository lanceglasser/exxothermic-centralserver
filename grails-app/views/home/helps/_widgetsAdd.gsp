<!DOCTYPE html>
<html>
	<head>
		<title></title>
		
		<script>

		Exxo.HelpCenter.initHelpPage();
		
		</script>
	</head>
	<body>
		<h1 class="category-title">
			<a href="#" class="link help-link" template="widgets" cat="widgets">
	            <span aria-hidden="true" class="smf-icon right link"></span>
	            ${ message(code: 'dashboard.widgets', args:[message(code:'widgets')]) }
	         </a>
	         >
			<a href="#">
	            <span aria-hidden="true" class="smf-icon right link"></span>
	            ${ message(code: 'widgets.how.add') }
	          </a>
		</h1>
		
		<div class="col-md-12">
			<div class="col-md-6" style="text-align: justify;">
				${ message(code:'widgets.how.add.1') }
				<img alt="" src="${resource(dir: 'themes/default/img/helps', file: 'add.png')}" width="16px" height="16px;">
				${ message(code:'widgets.how.add.2') }
			</div>
			
			<div class="col-md-6">
				${ message(code:'widgets.how.add.3') }
			</div>
			
			<span class="clearfix"></span>
		</div>
		<span class="clearfix"></span>
		<br/>
		<div class="col-md-12" style="text-align: center;">
			<img src="${resource(dir: 'themes/default/img/helps', file: 'add_widget.png')}">
		</div>
		<span class="clearfix"></span>
	</body>
</html>

