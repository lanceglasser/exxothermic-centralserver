<!DOCTYPE html>
<html>
	<head>
		<title></title>
		
		<script>

		Exxo.HelpCenter.initHelpPage();
		
		</script>
	</head>
	<body>
		<h1 class="category-title">
			<a href="#" class="link help-link" template="widgets" cat="widgets">
	            <span aria-hidden="true" class="smf-icon right link"></span>
	            ${ message(code: 'dashboard.widgets', args:[message(code:'widgets')]) }
	         </a>
	         >
			<a href="#">
	            <span aria-hidden="true" class="smf-icon right link"></span>
	            ${ message(code: 'widgets.list.general', args:[message(code:'help.venues')]) }
	          </a>
		</h1>
		
		<div class="col-md-12">
			<div class="col-md-6" style="text-align: justify;">
				${ message(code:'widgets.venues.list.1') }
				${ message(code:'widgets.venues.list.2') }
			</div>
			
			<div class="col-md-6">
				<img src="${resource(dir: 'themes/default/img/helps', file: 'widget_venues_list.png')}" width="350px;">
			</div>
			
			<span class="clearfix"></span>
		</div>
		<span class="clearfix"></span>
		<br/>
		<div class="col-md-12" style="text-align: center;">			
		</div>
		<span class="clearfix"></span>
	</body>
</html>

