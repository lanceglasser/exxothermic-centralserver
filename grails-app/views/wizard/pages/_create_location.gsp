<%
/**
 * second wizard page / tab
 *
 * @author	Jeroen Wesbeek <work@osx.eu>
 * @package AjaxFlow
 */
%>

<af:page >
	<h1><g:message code="wizard.label.step2" /></h1>
	
	<p>
	<g:message code="wizard.text.step2" />
	
	</p>
	 
	<p></p>
	 
	 <g:render template="/location/createTemplate"></g:render>
</af:page>

<script type="text/javascript">
	Exxo.UI.vars['useCompanyLogo'] = true;
	Exxo.UI.vars['lat'] = ${location.latitude? location.latitude: -1};
	Exxo.UI.vars['lng'] = ${location.longitude? location.longitude : -1};
	getCompanyInfoURL = "${g.createLink(controller: 'location', action: 'ajaxGetCompanyInformation', absolute: true)}";  
  	loadStatesByCountryUrl = "${g.createLink(controller: 'common', action: 'statebycountry', absolute: true)}";
	Exxo.UI.urls["loadStatesByCountryUrl"] = "${g.createLink(controller: 'common', action: 'statebycountry', absolute: true)}";
	Exxo.UI.urls["getCompanyInfoURL"] = "${g.createLink(controller: 'location', action: 'ajaxGetCompanyInformation', absolute: true)}";
	Exxo.Location.Create.defaultTimezone = 'UTC';  
	var dimensionsImageError = "${message(code:'customButton.image.dimension.error')}";
	var invalidImageError = "${message(code:'image.invalid.error')}";
	var tooBigImageError = "${message(code:'image.big.error')}";
	Exxo.UI.translates[Exxo.Constants.ERROR_FILE_INVALID] = "${message(code:'file.invalid.error')}";
	Exxo.UI.translates[Exxo.Constants.ERROR_FILE_SIZE] = "${message(code:'file.big.error')}";	 

	Exxo.Wizzard.initComponents();
  	Exxo.Location.Create.init();
  	$("a.close").click(function(e){
		e.preventDefault();
		$(this).parent().hide();
	});
</script>
