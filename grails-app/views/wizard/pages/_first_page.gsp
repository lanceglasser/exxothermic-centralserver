<%
/**
 * error page
 *
 * @author	Jeroen Wesbeek <work@osx.eu>
 * @package AjaxFlow
 */
%>
<af:page>
<h1><g:message code="wizard.label.welcome" /></h1>

<p>
	<g:message code="wizard.text.welcome" />
	
</p>
	

</af:page>
