<%
/**
 * last wizard page / tab
 *
 * @author	Jeroen Wesbeek <work@osx.eu>
 * @package AjaxFlow
 */
%>
<af:page>
<h1><g:message code="wizard.label.finalPage" /></h1>
<p>
<g:message code="wizard.text.conclusion" /> <g:link action="index"><g:message code="wizard.text.here" /></g:link> <g:message code="wizard.text.restartWizard" />
</p>
<p> <g:message code="wizard.text.continue" /> <g:link action="index" controller="home" ><g:message code="wizard.text.work"/></g:link></p>
</af:page>
