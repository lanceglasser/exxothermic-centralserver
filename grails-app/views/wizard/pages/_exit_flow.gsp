<%
/**
 * error page
 *
 * @author	Jeroen Wesbeek <work@osx.eu>
 * @package AjaxFlow
 */
%>
<af:page>
<h1>Oops!</h1>
<p>
	You cancel the Wizard, please click here to go to the   <g:link action="index" controller="home">Welcome Page</g:link>
	 
</p>
</af:page>
