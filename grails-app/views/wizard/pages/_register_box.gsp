<%
/**
 * third wizard page / tab
 *
 * @author	Jeroen Wesbeek <work@osx.eu>
 * @package AjaxFlow
 */
%>
<af:page >
	<h1><g:message code="wizard.label.step3" /></h1>
	
	<p>
	<g:message code="wizard.text.step3" />
	
	</p>
	 
	<p></p>
	 <g:render template="/box/registerTemplate"></g:render>
	 <span class="clearfix"></span>
</af:page>

<script type="text/javascript">
	Exxo.Wizzard.initComponents();
	Exxo.Box.Register.init();
  loadStatesByCountryUrl = "${g.createLink(controller: 'common', action: 'statebycountry', absolute: true)}";
  getCompanyInfoURL = "${g.createLink(controller: 'location', action: 'ajaxGetCompanyInformation', absolute: true)}"; 
  $("a.close").click(function(e){
		e.preventDefault();
		$(this).parent().hide();
	});  
</script>
