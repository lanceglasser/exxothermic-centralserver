<%
/**
 * first wizard page / tab
 *
 * @author	Jeroen Wesbeek <work@osx.eu>
 * @package AjaxFlow
 */
%>


<af:page  >
	<h1><g:message code="wizard.label.step1" /></h1>
	
	<p>
	<g:message code="wizard.text.step1" />
	
	</p>
	<p></p>
	 <g:render template="/company/createTemplate"></g:render>
</af:page>

<script type="text/javascript">
  //Cecropia.Company.Create.initComponents();
  Exxo.Wizzard.initComponents();
  Exxo.Company.Create.init();
  loadStatesByCountryUrl = "${g.createLink(controller: 'common', action: 'statebycountry', absolute: true)}";
  getCompanyInfoURL = "${g.createLink(controller: 'location', action: 'ajaxGetCompanyInformation', absolute: true)}";  
  $("a.close").click(function(e){
		e.preventDefault();
		$(this).parent().hide();
	});
</script>

