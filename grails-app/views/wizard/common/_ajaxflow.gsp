<%
/**
 * main ajax flow template
 *
 * @author	Jeroen Wesbeek <work@osx.eu>
 * @package AjaxFlow
 */
%>
<div id="ajaxflow">
<af:flow name="wizard" class="ajaxFlow" commons="common" partials="pages" spinner="ajaxFlowWait"
	controller="[controller: 'wizard', action: 'pages']">
	<%	/**
	 	 * The initial rendering of this template will result
	 	 * in automatically triggering the 'next' event in
	 	 * the webflow. This is required to render the initial
	 	 * page / partial and done by using af:triggerEvent
		 */ %>
	<af:triggerEvent name="next" afterSuccess="onPage();" />
</af:flow>

</div>
<g:render template="common/on_page"/>
