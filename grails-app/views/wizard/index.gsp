<!DOCTYPE html>
<%
/**
 * Wizard index page
 *
 * @author	Jeroen Wesbeek <work@osx.eu>
 * @package AjaxFlow
 */
%>
<html>
<head>
<meta name="layout" content="main" />
<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=AIzaSyB_3H-gp22pdmUYCfpFZG04crqtTDE8nwk"></script> 
<script src="${resource(dir: 'js/pages/companylocation', file: 'create.js')}"></script>	
    	<script src="${resource(dir: 'themes/default/js', file: 'application.js')}"></script>
    	
<!-- Image Crop Required Files -->
<link rel="stylesheet" href="${resource(dir: 'css/jcrop', file: 'jquery.Jcrop.css')}" type="text/css">
<script src="${resource(dir: 'js/jcrop', file: 'jquery.Jcrop.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_images_upload.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_utils.js')}"></script>

<!-- Current Page Required Files -->
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_company_create.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_location_create.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_box_register.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_form_show.js')}"></script>
<link rel="stylesheet" href="${resource(dir: 'themes/default/css', file: 'wizzard.css')}"/>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'wizard.js')}"></script>
<script>
	Exxo.module = "company-create";
	Exxo.UI.urls["loadStatesByCountryUrl"] = "${g.createLink(controller: 'common', action: 'statebycountry', absolute: true)}";
	Exxo.Company.Create.defaultTimezone = 'UTC';
	var dimensionsImageError = "${message(code:'customButton.image.dimension.error')}";
	var invalidImageError = "${message(code:'image.invalid.error')}";
	var tooBigImageError = "${message(code:'image.big.error')}";
	Exxo.UI.translates[Exxo.Constants.ERROR_FILE_INVALID] = "${message(code:'file.invalid.error')}";
	Exxo.UI.translates[Exxo.Constants.ERROR_FILE_SIZE] = "${message(code:'file.big.error')}";
	baseURL = "${createLink(uri: '', absolute : 'true')}";
</script>
</head>
<body>
	<section class="intro">
		<div class="intro-body">
			<div class="container full-width">
				<div class="row">
					<div class="col-md-12" id="internal-content">
						<g:render template="common/ajaxflow"/>
					</div>
				</div>
			</div>
		</div>
	</section>
</body>
</html>