<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title><g:message code="default.title.newAffiliate" /></title>
		<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_affiliate.js')}"></script>
		<script> Exxo.module = "affiliate-create"; </script>	
	</head>
	<body>
	
		<g:render template="/layouts/internalContentTemplate">
			<g:form action="save" name="createForm"  class="exxo-form" >

				<h1><g:message code="default.title.newAffiliate" /></h1>
				<div class="content">		     		     
	         		<g:render template="/layouts/messagesAndErrorsTemplate"
						model="[entity: affiliate]" />
	     		     		
	     		    <g:render template="affiliateCommonFieldsTemplate">
	     		 
	            		
	            		<g:submitButton class="form-btn btn-primary" name="save" value="${message(code:'default.field.buttonSave')}" />
						<g:link action="index" controller="home" class="back-button">        
					       <span class="form-btn btn-primary"> ${message(code:'default.field.buttonCancel')}</span>       
					  	</g:link>
	            		
	     		    </g:render>
				  </div>
			</g:form>
		</g:render>		
	</body>
</html>
