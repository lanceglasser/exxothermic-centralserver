<g:each var="type" in="${typesCompany}">
	<g:if test="${type.catalog.size()>0 &&  type.enable}">
		<g:if test="${type.catalog.size()==1}">
			<g:each in="${type.catalog}" var="subtype">
				<g:if test="${subtype.code==value}">
					<div class="company">
						<div class="header">
							<label>
								<g:checkBox name="type_${subtype.id}" class="cbType" typeId="${subtype.id}"  
									text="M: ${message(code:"typeOfEstablishment." + subtype.code.encodeAsHTML())}" 
									value="${false}" /> 
								${message(code:"typeOfEstablishment." + subtype.code.encodeAsHTML())}
							</label>
						</div>		
					</div> <!-- End of Company -->
					<!-- <option id="${subtype.code}" value="${subtype.id}" selected >${message(code:"typeOfEstablishment." + subtype.code.encodeAsHTML())}</option> -->
					<!-- <option id="${subtype.code}" value="${subtype.id}"          >${message(code:"typeOfEstablishment." + subtype.code.encodeAsHTML())}</option> -->
				</g:if>
				<g:else>
					<div class="company">
						<div class="header">
							<label>
								<g:checkBox name="type_${subtype.id}" class="cbType" 
									text="V: ${message(code:"typeOfEstablishment." + subtype.code.encodeAsHTML())}" 
								typeId="${subtype.id}"  
									value="${false}" /> 
								${message(code:"typeOfEstablishment." + subtype.code.encodeAsHTML())}
							</label>
						</div>		
					</div> <!-- End of Company -->
					<!-- <option id="${subtype.code}" value="${subtype.id}" >${message(code:"typeOfEstablishment." + subtype.code.encodeAsHTML())}</option> -->
				</g:else>
			</g:each>
		</g:if>
		<g:else> <!-- More than One: Father with Children -->
			<div class="company">
				<div class="header">
					<label>
						<g:checkBox name="allType_${type.id}" class="cbType" typeId="${type.id}"  
							value="${false}" hasChilds="1" /> 
						${message(code:"typeOfEstablishment." + type.code.encodeAsHTML())}
					</label>
					<div class="minus" id="minus_${type.id}" typeId="${type.id}"></div>
					<div class="plus" id="plus_${type.id}" typeId="${type.id}"></div>
					<span class="rowHelp"></span>
					<div class="clearfix"></div>
				</div>
				<!-- Childrens -->
				<div class="locationContainer" id="subtypesContainer_${type.id}" name="subtypesContainer_${type.id}">
				<g:each in="${type.catalog}" var="subtype" status="i">
					<g:if test="${subtype.code==value}">
						<div class="location ${(i % 2) == 0 ? ' even' : ' odd'}"><label for="location_${subtype.id}">
								<g:checkBox name="type_${subtype.id}" class="cbSubtype cb_${type.id}"
									text="X:: ${message(code:"typeOfEstablishment." + subtype.code.encodeAsHTML())}" 
									locationId="${subtype.id}" companyId="${type.id}"  value="${false}" />
								${message(code:"typeOfEstablishment." + subtype.code.encodeAsHTML())}
							</label>
						</div>
						<div class="clearfix"></div>
						<!-- <option id="${subtype.code}" value="${subtype.id}" selected >${message(code:"typeOfEstablishment." + subtype.code.encodeAsHTML())}</option> -->
					</g:if>
					<g:else>
						<div class="location ${(i % 2) == 0 ? ' even' : ' odd'}"><label for="location_${subtype.id}">
								<g:checkBox name="type_${subtype.id}" class="cbSubtype cb_${type.id}" subTypeId="${subtype.id}" 
									text="Z: ${message(code:"typeOfEstablishment." + subtype.code.encodeAsHTML())}"
									typeId="${type.id}"  value="${false}" />
								${message(code:"typeOfEstablishment." + subtype.code.encodeAsHTML())}
							</label>
						</div>
						<div class="clearfix"></div>
						
						<!-- <option id="${subtype.code}" value="${subtype.id}" >${message(code:"typeOfEstablishment." + subtype.code.encodeAsHTML())}</option> -->
					</g:else>
				</g:each>
				<!-- End of Childrens -->
				</div>
			</div> <!-- End of Company -->
			
			<!--<optgroup label="${message(code:"typeOfEstablishment." + type.code.encodeAsHTML())}">
				<g:each in="${type.catalog}" var="subtype">
					<g:if test="${subtype.code==value}">
						<option id="${subtype.code}" value="${subtype.id}" selected >${message(code:"typeOfEstablishment." + subtype.code.encodeAsHTML())}</option>
					</g:if>
					<g:else>
						<option id="${subtype.code}" value="${subtype.id}" >${message(code:"typeOfEstablishment." + subtype.code.encodeAsHTML())}</option>
					</g:else>
				</g:each>
			</optgroup> -->
		</g:else>
		
	       
	</g:if>
</g:each>
<script>
	Exxo.UI.vars['selectedLocations'] = "${affiliate.associateEstablishments}";
	//Exxo.UI.vars['selectedLocations'] = "1,3,5,6,7,14";
	$("#entityId").val(${affiliate.id});
	Exxo.Affiliates.Establishments.initAssociationTableEvents();
	Exxo.hideSpinner();
</script>