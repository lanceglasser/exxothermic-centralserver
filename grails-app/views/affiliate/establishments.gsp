<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="affiliates.establishment.title" /></title>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_affiliates.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_utils.js')}"></script>
<script>
	Exxo.module = "affiliate-establishments";
</script>

</head>
<body>
<g:render template="/layouts/internalContentTemplate">
	<h1>
		${ message(code:'affiliates.establishment.title') }
	</h1>
	<p class="muted"> <g:message code="assign.skin.note" /></p>
	
	<div class="content">
		<g:render template="/layouts/messagesAndErrorsTemplate"/>
		
		<g:form action="associateEstablishments" name="saveForm" id="saveForm" class="exxo-form">
			<input type="hidden" id="locationsToSave" name="locationsToSave" value=""/>
			<input type="hidden" id="entityId" name="entityId" value=""/>
			
			<div class="col-xs-12 resize-formleft">
				<div class="col-xs-2">
				</div>
				<div class="col-xs-5">
					<div id="successMsgContainer" class="alert alert-success" style="display: none;">
						<a class="close" data-dismiss="alert">×</a>
						<div id="successMsg"></div>
					</div>
					<div id="errorMsgContainer" class="alert alert-error hide" style="display: none;">
						<a class="close" data-dismiss="alert">×</a>
						<div id="errorMsg"></div>
					</div>
				</div>
			</div>
			<span class="clearfix"></span>	
			
			<div class="col-xs-12 resize-formleft">
				<div class="col-xs-2">
					<label class="control-label" for="user">
						<g:message code="default.field.affiliate" /> *
					</label>
				</div>
				<div class="col-xs-10">
					<g:select name="affiliates" id="affiliates"  from="${affiliates}" style="width:50%" optionKey="id" optionValue="name" value="${affiliate.id}" 
							onchange="Exxo.showSpinner();${remoteFunction(
					            action:'getAffiliateEstablishments', 
					            params:'\'affiliateId=\' + escape(this.value)',
								update : 'infor')}"/>
				</div>
			</div>
			<span class="clearfix"></span>							
			
			<div class="col-xs-12 resize-formleft">
				<div class="col-xs-2">
				</div>
				<div class="col-xs-10">
					<div class="exxo-table-container" id="infor"></div>
				</div>
			</div>
			<span class="clearfix"></span>							
										
			<div class="col-xs-12 resize-formleft">
				<div class="col-xs-2">
				</div>
				<div class="col-xs-10 text-to-left">
					<input type="hidden" id="locations" name="locations" value=""/>
					<g:submitButton class="form-btn btn-primary" name="save"
						value="${message(code:'default.field.buttonSave')}" />
					
					<input type="button" id="resetButton" name="resetButton" class="form-btn btn-primary" value="${message(code:'default.field.buttonReset')}"/>	
					
				</div>
			</div>
			<span class="clearfix"></span>
		</g:form>
	</div>
</g:render>


</body>
</html>