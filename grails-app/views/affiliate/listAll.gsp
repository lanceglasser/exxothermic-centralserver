<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title><g:message code="default.title.listStores" /></title>
		<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
		<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
		<script src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
		<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
		<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_affiliate_list.js')}"></script>
		<%-- <script src="${resource(dir: 'js/pages/affiliate', file: 'listAll.js')}"></script> --%>
		<script>
		Exxo.module = "affiliate-list";
		Exxo.UI.vars["page-code"] = "affiliate-list";
		Exxo.Tables.loadPaginationInitValuesFromString('<custom:paginationInfo code="affiliate-list"/>');
		Exxo.UI.translates['okButton'] = "${message(code:'default.field.buttonOk')}";
		Exxo.UI.translates['cancelButton'] = "${message(code:'default.field.buttonCancel')}";
		Exxo.UI.translates['disableMessage'] = "${message(code:'mycentralserver.user.User.resetPassword')}";
		</script>
	</head>
	<body>
		<section class="intro">
		  <div class="intro-body">
		    <div class="container full-width">
		      <div class="row">
		        <div class="col-md-12" id="internal-content">
		          <h1><g:message code="default.title.listStores" /></h1>
						<g:if test="${flash.error}">
							<div class="alert alert-error">
								<a class="close" data-dismiss="alert">×</a>
								${flash.error}
								<g:hasErrors bean="${affiliate}">
									<ul>
										<g:eachError bean="${affiliate}" var="error">
											<li><g:if
													test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"
												</g:if></li>
											<g:message error="${error}" />
											</li>
										</g:eachError>
									</ul>
								</g:hasErrors>
								${flash.error=null}
							</div>
						</g:if>

						<g:if test="${flash.success}">
							<div class="alert alert-success">
								<a class="close" data-dismiss="alert">×</a>
								${flash.success}
								${flash.success=null}
							</div>
						</g:if>
						<div class="content">
					<g:if test="${affiliates.size()>0}">
		            <table id="table-list" class="display" cellspacing="0" width="100%">
		              <thead>
		                <tr>
					     <th>${message(code: 'affiliate.code.label')}</th>
					     <th>${message(code: 'affiliate.domain.label')}</th>
					     <th>${message(code: 'affiliate.name.label')}</th>
					     <th>${message(code: 'affiliate.androidUrl.label')}</th>
					     <th>${message(code: 'affiliate.iosUrl.label')}</th>
					     <th>${message(code: 'default.field.enabled')}</th>
					     <th>${message(code: 'default.table.action')}</th>
					    </tr>
		              </thead>
		              <tfoot>
		                <tr>
		                 <th>${message(code: 'affiliate.code.label')}</th>
					     <th>${message(code: 'affiliate.domain.label')}</th>
					     <th>${message(code: 'affiliate.name.label')}</th>
					     <th>${message(code: 'affiliate.androidUrl.label')}</th>
					     <th>${message(code: 'affiliate.iosURL.label')}</th>
					     <th>${message(code: 'default.field.enabled')}</th>
					     <th>${message(code: 'default.table.action')}</th>
		                </tr>
		              </tfoot>

		              <tbody>
                        <g:each in="${affiliates}" status="i" var="affiliateInstance">
						<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
							<td>
								<%-- <g:link action="edit" id="${affiliates.id}"> ${affiliateInstance.code.encodeAsHTML()}</g:link>--%>
								<div class="value" id="${affiliates.id}">${affiliateInstance.code.encodeAsHTML()}</div>
								<%-- ${fieldValue(bean: affiliateInstance, field: "code").encodeAs HTML} }--%>
							</td>
							<td>
								<div class="value" id="${affiliates.id}">${affiliateInstance.domain.encodeAsHTML()}</div>
							</td>
							<td>
								<div class="value" id="${affiliates.id}">${affiliateInstance.name.encodeAsHTML()}</div>
							</td>
							<td>
								<div class="value" id="${affiliates.id}">${affiliateInstance.androidAppUrl.encodeAsHTML()}</div>
							</td>
							<td>
								<div class="value" id="${affiliates.id}">${affiliateInstance.iosAppUrl.encodeAsHTML()}</div>
							</td>
							<td>
								<g:if test="${affiliateInstance.enabled == true}">
									<g:message code="default.field.yes" />
								</g:if>
								<g:else>
									<g:message code="default.field.no" />
								</g:else>
							</td>
							<td>
								<g:link action="show" id="${affiliateInstance.id}" class="action-icons view" params="[offset: offset]" title="${message(code:'default.action.show')}"  ></g:link>
								<g:link action="edit" id="${affiliateInstance.id}"  class="action-icons edit" title="${message(code:'default.action.edit')}"  params="[offset: offset]"></g:link>
								<g:if test="${affiliateInstance.enabled}">
									<g:link action="disabled" id="${affiliateInstance.id}" class="action-icons block" title="${message(code:'default.action.disabled')}"></g:link> 
								</g:if>
								<g:else>
								    <g:link action="disabled" id="${affiliateInstance.id}" class="action-icons unblock" title="${message(code:'default.action.enabled')}" ></g:link>
							    </g:else>
							</td>
						</tr>
					</g:each>
	              </tbody>
	            </table>
	            </g:if>
	          </div> <%-- class="content"--%>
	        </div>
	      </div>
	    </div>
	  </div>
	</section>
	<div id="confirm-delete-dialog" title="${message(code:'mycentralserver.affiliate.deleteConfirmation.title')}" style="display:none;">
	  ${message(code:'mycentralserver.affiliate.deleteConfirmation')}
	</div>
	</body>
</html>
