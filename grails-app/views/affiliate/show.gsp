<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.affiliate" /></title>
<script> Exxo.module = "affiliate-show"; </script>
</head>
<body>
	<g:render template="/layouts/internalContentTemplate">
		<h1>
			${affiliate.name.encodeAsHTML()}
		</h1>
		<g:render template="/layouts/messagesAndErrorsTemplate"
			model="[entity: affiliate]" />
		<div class="content form-info">
			<!-- Left Column -->
			<div class="col-xs-6 resize-formleft">
				<custom:editField name="code"
					value="${affiliate.code ? affiliate.code.encodeAsHTML() : ''}"
					label="default.field.code" />
				<custom:editField name="Domain"
					value="${affiliate.domain ? affiliate.domain.encodeAsHTML() : ''}"
					label="affiliate.domain.label" />
				<custom:editField name="name"
					value="${affiliate.name ? affiliate.name.encodeAsHTML() : ''}"
					label="affiliate.name.label" />
				<custom:editField name="Android URL"
					value="${affiliate.androidAppUrl ? affiliate.androidAppUrl.encodeAsHTML() : ''}"
					label="default.field.androidAppUrl" />
				<custom:editField name="IOS URL"
					value="${affiliate.iosAppUrl ? affiliate.iosAppUrl.encodeAsHTML() : ''}"
					label="default.field.iosAppUrl" />
				<custom:editField name="lastUpdatedBy"
					value="${affiliate.lastUpdatedBy ? affiliate?.lastUpdatedBy?.firstName?.encodeAsHTML() + " " + affiliate?.lastUpdatedBy?.lastName?.encodeAsHTML() : ''}"
					label="user.lastUpdatedBy.label" />
					
			</div>
			<!-- Right Column -->
			<%-- <div class="col-xs-6 resize-formright">
				<custom:editField name="accountExpired"
					value="${(user.accountExpired == true) ? message(code:"default.field.yes") : message(code:"default.field.no")}"
					label="default.field.accountExpired" />
				<custom:editField name="accountLocked"
					value="${(user.accountLocked == true) ? message(code:"default.field.yes") : message(code:"default.field.no")}"
					label="default.field.accountLocked" />
				<custom:editField name="passwordExpired"
					value="${(user.passwordExpired == true) ? message(code:"default.field.yes") : message(code:"default.field.no")}"
					label="default.field.passwordExpired" />
				<custom:editField name="enabled"
					value="${(user.enabled == true) ? message(code:"default.field.yes") : message(code:"default.field.no")}"
					label="default.field.enabled" />
			</div>--%>
			<span class="clearfix"></span>

			<!-- Buttons -->
			<g:link action="listAll" controller="affiliate" params="[offset: offset]">
				<input type="button" class="form-btn btn-primary"
					value="${message(code:'default.field.buttonBack')}" />
			</g:link>
		</div>
	</g:render>


</body>
</html>

