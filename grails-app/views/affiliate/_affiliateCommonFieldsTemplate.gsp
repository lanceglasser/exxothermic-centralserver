<!-- Column Left -->
 <div class="col-xs-6 resize-formleft">
	<custom:editField type="text" name="code" max="255" required="${true}" value="${affiliate?.code}" label="default.field.code" />
	<custom:editField type="text" name="domain" max="255" required="${true}" value="${affiliate?.domain}" label="default.field.domain" />
	<custom:editField type="text" name="name" max="255" required="${true}" value="${affiliate?.name}" label="default.field.name" />
	<custom:editField type="url" name="androidAppUrl" max="255" required="${true}" value="${affiliate?.androidAppUrl}" label="default.field.androidAppUrl" />
	<custom:editField type="url" name="iosAppUrl" max="255" required="${true}" value="${affiliate?.iosAppUrl}" label="default.field.iosAppUrl" />
	<custom:editField type="checkbox" name="enabled" required="${false}" value="${affiliate?.enabled}"
		label="affiliate.field.enabled.status" cssclass="input-checkbox"/>
	${body()}
</div>