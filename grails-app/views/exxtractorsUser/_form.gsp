<p class="muted"> <g:message code="location.user.details"/> </p>

<!-- Left Column -->
<div class="col-xs-12 resize-formleft">
	<div class="alert alert-error" id="divError" style="display: none;">
		<a class="close" data-dismiss="alert">×</a>
		<div id="errorMessage">
		
		</div>
	</div>
		
	<!-- Location -->
	<div class="col-xs-4 text-to-right">
		<label for="location">
			${ message(code:'default.field.location') } *
		</label>
	</div>
	<div class="col-xs-8 text-to-left">
		<select id="location" name="location">
			<g:each in="${locations}" status="i" var="location">
			<option value="${ location.id }" ${ location.id == locationId? 'selected="selected"':'' }>${ location.encodeAsHTML() }</option>
			</g:each>f
		</select>
	</div>
	<span class='clearfix'></span>
	
	<!-- Type -->
	<div class="col-xs-4 text-to-right">
		<label  for="userType">
			${ message(code:'default.field.type') } *
		</label>
	</div>
	<div class="col-xs-8 text-to-left">
		<select id="userType" name="userType">
			<option value="${ mycentralserver.utils.Constants.BOX_USER_TYPE_ADMIN }"
				${ entity.type == mycentralserver.utils.Constants.BOX_USER_TYPE_ADMIN? 'selected="selected"':'' }>
				${ message(code:'box.user.type.'+mycentralserver.utils.Constants.BOX_USER_TYPE_ADMIN) }
			</option>
			<option value="${ mycentralserver.utils.Constants.BOX_USER_TYPE_TECH }"
				${ entity.type == mycentralserver.utils.Constants.BOX_USER_TYPE_TECH? 'selected="selected"':'' }>
				${ message(code:'box.user.type.'+mycentralserver.utils.Constants.BOX_USER_TYPE_TECH) }
			</option>
			<option value="${ mycentralserver.utils.Constants.BOX_USER_TYPE_USER }"
				${ entity.type == mycentralserver.utils.Constants.BOX_USER_TYPE_USER? 'selected="selected"':'' }>
				${ message(code:'box.user.type.'+mycentralserver.utils.Constants.BOX_USER_TYPE_USER) }
			</option>
		</select>
	</div>
	<span class='clearfix'></span>
	
	<!-- Username  -->				
	<custom:editField type="text" name="username" required="${true}" value="${ entity.username.encodeAsHTML() }"
		label="username" cssclass="lowercaseOnly"/>
	
	<!-- Password  -->
	<custom:editField type="password" name="userPassword" required="${true}" value="" 
		label="field.password" />
	
	<!-- Confirm Password  -->
	<custom:editField type="password" name="confirmUserPassword" required="${true}" value="" 
		label="field.password.confirm" />
		
	<div id="edit-password-note" style="display: none;">
		<div class="col-xs-4 text-to-left">
		</div>
		<div class="col-xs-8 text-to-left">
			<span class="liten-up">${ message(code:'box.user.edit.password.note') }</span>
		</div>
		<span class='clearfix'></span>
	</div>
</div>
<span class='clearfix'></span>
