<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title><g:message code="new.exxtractor.user" /></title>		
    	<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_exxtractors_users.js')}"></script>
    	<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_validation.js')}"></script>	
	    <script>
			Exxo.module = "exxtractor-user-create";
			Exxo.UI.vars['locationId'] = ${locationId};
			Exxo.UI.translates['usernameError'] = "${ message(code:'box.user.username.error', args:[4, 20]) }"; 
			Exxo.UI.translates['passwordError'] = "${ message(code:'box.user.password.error', args:[4, 8]) }";
			Exxo.UI.translates['confirmError'] = "${ message(code:'box.user.confirm.error') }";
			Exxo.UI.urls["exxtractor-users-with-sync"] = '<g:createLink controller="location" action="show" params="[a:'sync', id:'replaceLocationId', tab:'exx-users']"/>';
			console.debug(Exxo.UI.urls["exxtractor-users-with-sync"]);
		</script>
	</head>
	<body>
		<g:render template="/layouts/internalContentTemplate">
			<h1>
				<g:message code="new.exxtractor.user" />
			</h1>
			<div class="content">
				<!-- Left Column -->
				<div class="col-xs-8 resize-formleft">
					<g:form action="createUser" name="formCreate" class="exxo-form">
						<input type="hidden" id="userId" name="userId" value="0"/>
						
						<g:render template="form"></g:render>
						
						<!-- Buttons -->
						<g:submitButton class="form-btn btn-primary" name="save" value="${message(code:'default.field.buttonSave')}" />								
						<g:link action="index" controller="home" class="back-button">        
					       <span class="form-btn btn-primary"> ${message(code:'default.field.buttonCancel')} </span>       
				    	</g:link>
			    	
			    	</g:form>
				</div>
			</div>
		</g:render>
	</body>	
</html>
