<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="exxtractors.users" /></title>
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
<script
	src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
<script
	src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>

<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_validation.js')}"></script>	
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_exxtractors_users.js')}"></script>
<script>
	Exxo.module = "exxtractors-users";
	Exxo.UI.vars['locationId'] = ${location.id};
	Exxo.UI.translates['okButton'] = "${message(code:'default.field.buttonOk')}";
	Exxo.UI.translates['saveButton'] = "${message(code:'default.field.buttonSave')}";
	Exxo.UI.translates['cancelButton'] = "${message(code:'default.field.buttonCancel')}";
	Exxo.UI.translates['disableMessage'] = "${message(code:'mycentralserver.custombuttons.CustomButton.isDisabled')}";
	Exxo.UI.translates['usernameError'] = "${ message(code:'box.user.username.error', args:[4, 20]) }"; 
	Exxo.UI.translates['passwordError'] = "${ message(code:'box.user.password.error', args:[4, 8]) }";
	Exxo.UI.translates['confirmError'] = "${ message(code:'box.user.confirm.error') }";
	Exxo.UI.urls["sync"] = '<g:createLink controller="exxtractorsUser" action="management" params="[a:'sync', id:location.id]"/>';
</script>
</head>
<body>
<g:render template="/layouts/internalContentTemplate">
	<h1>
		${ location.name.encodeAsHTML() } - ${ message(code:'exxtractors.users') }
	</h1>
	<div class="content">
		<g:render template="/layouts/messagesAndErrorsTemplate" model="[entity: location]"/>
		
		<div class="col-xs-12 resize-formright text-to-right" style="padding-right: 0px;">
			<g:actionSubmit id="createBtn" class="form-btn btn-primary" name="create" value="${ message(code:'default.button.create.label') }" />
			<g:actionSubmit id="syncBtn" class="form-btn btn-primary" name="create" value="${ message(code:'default.field.buttonSyncronize') }" />
		</div>
		<span class="clearfix"></span>
		<br>
		<table id="table-list" class="display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>${message(code: 'username')}
					<th>${ message(code:'default.field.type') }</th>
					<th>${ message(code:'default.table.action') }</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th>${message(code: 'username')}
					<th>${ message(code:'default.field.type') }</th>
					<th>${ message(code:'default.table.action') }</th>
				</tr>
			</tfoot>
			<tbody>
				<g:each in="${location.boxUsers}" status="i" var="user">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">						
						<td id="username-${ user.id }">${user.username}</td>						
						<td>${ message(code:'box.user.type.'+user.type)}
							<input type="hidden" id="type-${ user.id }" value="${ user.type }"/>
						</td>
						<td>
							<g:link ref="${user.id}" class="action-icons edit"
								title="${message(code:'default.action.edit')}"></g:link>
							<g:link action="delete" id="${user.id}"  class="action-icons delete" 
								title="${message(code:'default.action.delete')}" params="[locationId: location.id]"></g:link>
						</td>
					</tr>
				</g:each>
			</tbody>
		</table>
		
		<!-- Information with the status of every transaction -->
		<g:if test="${syncResult}" >
			<br/>
   			<legend>
        		<g:message code="boxes.sync.results" />
			</legend>      					         			    
    		<table id="table-list-two" class="display" cellspacing="0" width="100%">
    			<thead>
					<tr>
						<th>${message(code: 'default.field.serial')}</th>
		    			<th>${message(code: 'default.field.status')}</th>
		    			<th>${message(code: 'default.field.errorDescription')}</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>${message(code: 'default.field.serial')}</th>
		    			<th>${message(code: 'default.field.status')}</th>
		    			<th>${message(code: 'default.field.errorDescription')}</th>
					</tr>
				</tfoot>	
  				<tbody style="text-align='center'">
      			<g:each in="${syncResult?.usersStatus}" var="record">
     				<tr>
   						<td class="">${record.serialNumber}</td>
   						<td class="">${message(code:'boxes.sync.result.status.'+record.successful)}</td>
   						<td>${record.successful? '':message(code:messageErrorDescriptionHash.get(record.errorCode))}</td>	          					
   					</tr>
      			</g:each>
    			</tbody>
    		</table>	          			  			     	
        </g:if>
				
		<!-- Buttons -->										
		<g:link action="listAll" controller="location"  > 
	       <span class="form-btn btn-primary">
	          ${message(code:'default.field.buttonBack')}
	       </span>       
    	</g:link>
    	
    	
	</div>
</g:render>
<div id="user-form" title="${message(code:'default.confirm.title')}" style="display:none;">
	<g:render template="/exxtractorsUser/form"/>
</div>
<div id="confirm-delete-dialog" title="${message(code:'default.confirm.title')}" style="display:none;">
  ${message(code:'general.object.confirm.delete')}
</div>
</body>
</html>