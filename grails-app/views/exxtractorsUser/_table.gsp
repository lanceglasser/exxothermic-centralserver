<table class="display entity-table table-list" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>${message(code: 'username')}
			<th>${ message(code:'default.field.type') }</th>
			<th>${ message(code:'default.table.action') }</th>
		</tr>
	</thead>
	<tbody>
		<g:each in="${location.boxUsers}" status="i" var="user">
			<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">						
				<td id="username-${ user.id }">${user.username}</td>						
				<td>${ message(code:'box.user.type.'+user.type)}
					<input type="hidden" id="type-${ user.id }" value="${ user.type }"/>
				</td>
				<td>
				    <sec:ifAnyGranted roles="ROLE_HELP_DESK,ROLE_INTEGRATOR">
						<g:link controller="exxtractorsUser" action="edit" id="${user.id}" class="action-icons edit"
							title="${message(code:'default.action.edit')}"></g:link>
						<g:link controller="exxtractorsUser" action="delete" id="${user.id}"  class="action-icons delete deleteExxtractorUser" 
							title="${message(code:'default.action.delete')}" params="[locationId: location.id]"></g:link>
					</sec:ifAnyGranted>
						
				</td>
			</tr>
		</g:each>
	</tbody>
</table>