<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title><g:message code="default.title.updateLocation" /></title>	
		<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=AIzaSyB_3H-gp22pdmUYCfpFZG04crqtTDE8nwk"></script> 
		<!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>--> 	
    	<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_location_create.js')}"></script>
    	<script src="${resource(dir: 'themes/default/js', file: 'google_maps.js')}"></script>
    	<script src="${resource(dir: 'themes/default/js', file: 'application.js')}"></script>
    	<!-- Image Crop Required Files -->
		<link rel="stylesheet" href="${resource(dir: 'css/jcrop', file: 'jquery.Jcrop.css')}" type="text/css">
		<script src="${resource(dir: 'js/jcrop', file: 'jquery.Jcrop.js')}"></script>
		<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_images_upload.js')}"></script>
		<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_utils.js')}"></script>
	    <script>
			Exxo.module = "location-create";
			Exxo.UI.vars['useCompanyLogo'] = ${location.useCompanyLogo};
			Exxo.UI.vars["companyLogo"] = "${location.company?.logoUrl}";
			Exxo.UI.vars['lat'] = ${location.latitude? location.latitude: -1};
			Exxo.UI.vars['lng'] = ${location.longitude? location.longitude : -1};
			getCompanyInfoURL = "${g.createLink(controller: 'location', action: 'ajaxGetCompanyInformation', absolute: true)}";  
	        loadStatesByCountryUrl = "${g.createLink(controller: 'common', action: 'statebycountry', absolute: true)}";
			Exxo.UI.urls["loadStatesByCountryUrl"] = "${g.createLink(controller: 'common', action: 'statebycountry', absolute: true)}";
			Exxo.UI.urls["getCompanyInfoURL"] = "${g.createLink(controller: 'location', action: 'ajaxGetCompanyInformation', absolute: true)}";
			Exxo.Location.Create.defaultTimezone = '${(location.timezone!=null)? location.timezone:'UTC'}';  
			var dimensionsImageError = "${message(code:'customButton.image.dimension.error')}";
			var invalidImageError = "${message(code:'image.invalid.error')}";
			var tooBigImageError = "${message(code:'image.big.error')}";
			Exxo.UI.translates[Exxo.Constants.ERROR_FILE_INVALID] = "${message(code:'file.invalid.error')}";
			Exxo.UI.translates[Exxo.Constants.ERROR_FILE_SIZE] = "${message(code:'file.big.error')}";
		</script>
	</head>
	<body>
		<g:render template="/layouts/internalContentTemplate">
			<h1>
				<g:message code="default.title.updateLocation" />
			</h1>
			<div class="content">
				
				<g:render template="/layouts/messagesAndErrorsTemplate" model="[entity: location]"/>
				
				<g:uploadForm action="update" name="updateForm" class="exxo-form">
					<g:hiddenField name="id" value="${location.id}" />
					<g:hiddenField name="version" value="${location.version}" />
					<g:hiddenField name="offset" value="${offset}" />
					<g:hiddenField name="latitude" value="${location.latitude}"/>
					<g:hiddenField name="longitude" value="${location.longitude}"/>
					
					<!-- Left Column -->
					<div class="col-xs-6 resize-formleft">
						<custom:editField type="text" name="name" max="50" required="${true}" value="${location.name}" 
							label="default.field.locationName" tooltip="tooltip.location.name" />
						
						<span id="companyDiv">
                            <div class="col-xs-4 text-to-right">
                              <label for="companyName">
                                  <div class="questionToolTip" title="${message(code:'tooltip.location.name')}">
                                  </div> ${message(code:'default.field.companyName')} 
                              </label>
                            </div>
                            <div class="col-xs-8 text-to-left exxo-label">
                                <g:link controller="company" action="dashboard" id="${location.company.id}">
                                    ${ location.company.encodeAsHTML() }
                                </g:link>
                            </div>
                        </span>
                        <span class="clearfix"></span>
						
						<div class="col-xs-4 text-to-right">
							<label  for="commercialIntegrator">
								<div class="questionToolTip" title="${message(code:'tooltip.select.systemIntegrator')}"></div>
								<g:message code="default.field.commercialntegrator" />
							</label>
						</div>				
						<div class="col-xs-8 text-to-left">			
							<g:select name="commercialIntegrator.id" id="commercialIntegrator" from="${integrators}" optionKey="id" optionValue="companyName" 
								value="${location.commercialIntegrator?.id}" noSelection="['':'']"  placeholder="${message(code:'default.field.selectIntegrator')}" />
						</div>
						<span class="clearfix"></span>	
						
						<div class="col-xs-4 text-to-right">		
							<label class="control-label" for="type"><g:message code="default.field.typeOfEstablishment" />*</label>
						</div>					
						<div class="col-xs-8 text-to-left">
							<custom:selectWithOptGroupCompanyType id="type" name="type" from="${typesCompany}" value="${location.type?.code}" />
						</div>			 
					 	<span class="clearfix"></span>
					 	
						<div id="controlCompanyOtherType">
							<custom:editField type="text" name="otherType" required="${true}" value="${location.otherType}" 
								label="default.field.specify" max="30" />
						</div>
							
						<div class="col-xs-4 text-to-right">
							<label for="country"><g:message code="default.field.country" />*</label>
						</div>	
						<div class="col-xs-8 text-to-left">
							<select name="country.id" id="country" tabindex="-1" title="Country*">
								<g:each in="${countries}" status="i" var="country">
								<option ${ (country.id == location.country?.id)? 'selected="selected"':'' } value="${ country.id }" code="${ country.code }">${ message(code:'country.code.'+ country.shortName) }</option>
								</g:each>
							</select>					 				 
						</div>
						<span class="clearfix"></span>		
						
						<g:if test="${location.state}">
							<div id="stateDiv" >
						</g:if>
						<g:else>
							<div id="stateDiv" style="display:none;">
						</g:else>						
							<div class="col-xs-4 text-to-right">
								<label for="state"><g:message code="default.field.stateOrProvince" />*</label>
							</div>	
							<div class="col-xs-8 text-to-left">			
								<select name="state.id" id="state" tabindex="-1" title="State*" value="${location.state?.id}">
									<g:each in="${states}" status="i" var="state">
									<option ${ (state.id == location.state?.id)? 'selected="selected"':'' } value="${ state.id }" code="${ state.name }">${ message(code:'state.code.'+ state.name) }</option>
									</g:each>
								</select>						 
							</div>
							<span class="clearfix"></span>
						</div>	
						
						<g:if test="${location.state}">
							<div id="stateNameDiv"  style="display:none;">
						</g:if>
						<g:else>
							<div id="stateNameDiv">
						</g:else>
							<div class="col-xs-4 text-to-right">
				              <label for="stateName"> ${ message(code:'default.field.stateOrProvince') } * </label>
				            </div>
				            <div class="col-xs-8 text-to-left">
				            	<input type="text" id="stateName" name="stateName" class="" 
				            	   value="${ location.stateName ? location.stateName.encodeAsHTML():'' }" placeholder="" maxlength="50"
				            		${location.state == null? ' required="required"':''}> 
				            </div>
				            <span class="clearfix"></span>
						</div>
							
						<custom:editField type="text" name="city" required="${true}" value="${location.city}" 
							label="default.field.city" max="100" />
						
						<div class="col-xs-4 text-to-right">
						  <label for="type"><g:message code="default.field.timezone" /> *</label>
						</div>
						<div class="col-xs-8 text-to-left">
						  <custom:timeZoneSelect name="timezone" value="${location.timezone}" />
						</div>
						<span class="clearfix"></span>
			
						<custom:editField type="textarea" name="address" required="${true}" value="${location.address}" 
							max="100" label="default.field.address" cssclass="input-textarea"/>
												
						<!-- useCompanyLogo -->
						<custom:editField type="checkbox" name="useCompanyLogo" required="${false}" value="${location.useCompanyLogo}" 
							label="location.use.company.logo" rightColumnPosition="right" leftColumnSize="10" cssclass="input-checkbox" tooltip="location.use.company.logo.help"/>
						
						<div id="companyLogo" style="display: none;">
							<div class="col-xs-4 text-to-right">
								<label>${message(code:'company.logo')}</label>
							</div>
							<div class="col-xs-8 text-to-left">
								<g:if test="${location.company?.logoUrl}">
									<img id="companyLogoImg" style="width:300px; height:auto; padding: 5px ; border: 1px #bbb solid;" src="${location.company?.logoUrl}"/>
								</g:if>
								<g:else>
									<p></p>
									<span class="liten-up"> ${ message(code:'company.logo.not.found') } </span>
								</g:else>
								
							</div>
						</div>
						<span class="clearfix"></span>
						
						<div id="uploadOwnLogo" style="display: none;">
							<!-- Logo Image -->
							<g:render template="/layouts/tags/imageUploadSelectorTemplate" 
								model="[
									id:				'bgLogoUrl',
									imgValue: 		location.logoUrl,
									label:			message(code:'location.logo'),
									cropImage:		true,
									minWidth: 		mycentralserver.utils.Constants.COMPANY_LOGO_WIDTH,
									minHeight: 		mycentralserver.utils.Constants.COMPANY_LOGO_HEIGHT]"
							/>
						</div>			
						<span class="clearfix"></span>
			
						
					</div>
					
					<!-- Right Column -->
					<div class="col-xs-6 resize-formright">
						<custom:editField type="text" name="pocEmail" required="${false}" value="${location.pocEmail}" 
							label="poc.email" disabled="${ location.pocAccountCreated }" max="255"/>
						<custom:editField type="text" name="pocFirstName" required="${false}" value="${location.pocFirstName}" 
							label="poc.firstname" max="150"/>
						<custom:editField type="text" name="pocLastName" required="${false}" value="${location.pocLastName}" 
							label="poc.lastname" max="150"/>
						<custom:editField type="text" name="pocPhone" required="${false}" value="${location.pocPhone}" 
							label="poc.phone" max="50"/>
						<custom:editField type="checkbox" name="pocAccountCreated" required="${false}" 
							value="${location.pocAccountCreated}" label="poc.create.account" rightColumnPosition="right" 
							cssclass="input-checkbox" tooltip="poc.create.account.help" leftColumnSize="11"
							readonly="${ location.pocAccountCreated }" disabled="${ location.pocAccountCreated }"/>
						
						<div class="col-xs-12 text-to-right">
							<span class="liten-up full-width text-to-right">${ message(code:'poc.create.account.note') }</span>
						</div>
						<br><br>
						
						<sec:ifAnyGranted roles="ROLE_HELP_DESK">
				    	<span id="mediaInfoDiv" class="col-xs-12 text-to-right">
				    		<label>${ message(code:'venue.social.information') }</label>
						</span>
						<span class="clearfix"></span>
				            
				    	<custom:editField type="text" name="facebookId" required="${false}" value="${location.facebookId}" 
				            label="default.field.facebook.id" tooltip="tooltip.location.facebook.id" />
				         
				        <span id="mediaInfoDiv" class="col-xs-12 text-to-right">
				    		<label>${ message(code:'extra.information') }</label>
						</span>
						<span class="clearfix"></span>
						</sec:ifAnyGranted>

						<custom:editField type="number" name="maxOccupancy" required="${true}" value="${location.maxOccupancy}" 
							label="default.field.maxOccupancy" cssclass="onlyNumbers" min="0" step="1" leftColumnSize="8"/>
						
						<custom:editField type="number" name="numberOfTv" required="${true}" value="${location.numberOfTv}" 
							label="default.field.numberOfTv" cssclass="onlyNumbers" min="0" step="1" leftColumnSize="8"/>
							
						<custom:editField type="number" name="numberOfTvWithExxothermicDevice" required="${true}" value="${location.numberOfTvWithExxothermicDevice}" 
							label="default.field.numberOfTvWithExxothermiceDevice" cssclass="onlyNumbers" min="0" step="1" leftColumnSize="8"/>
							
						<custom:editField type="number" name="contentsLimit" required="${true}" value="${location.contentsLimit}" 
							label="default.field.contentsLimit" cssclass="onlyNumbers" min="1" max="6" step="1" leftColumnSize="8"/>
						
						<br/>
						
						<div class="control-group">
							<div id="map-canvas"  style="width: 100%; height: 300px;"></div>
						</div>
					</div>
					<span class="clearfix"></span>
					
					<!-- Buttons -->
					<g:submitButton class="form-btn btn-primary" name="save"
						value="${message(code:'default.field.buttonSave')}" />
										
					<g:link action="listAll" controller="location" class="back-button" >        
				       <span class="form-btn btn-primary">
				          ${message(code:'default.field.buttonCancel')}
				       </span>       
			    	</g:link>
						
						
				</g:uploadForm>
				
			</div>
		</g:render>
	</body>	
</html>
