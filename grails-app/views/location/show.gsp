<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.Location" /></title>
<script src="${resource(dir: 'themes/default/js', file: 'video.js')}"></script>
<link rel="stylesheet" href="${resource(dir: 'themes/default/css', file: 'video-js.min.css')}" type="text/css">
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script
	src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
	
<script src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.rowReordering.js')}"></script>
<script
	src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_utils.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_location_show.js')}"></script>
<script
    src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_metrics_charts.js')}"></script>
<script>
	Exxo.module 		= "location-show";
	Exxo.locationID 	= "${ location.id }";	
	Exxo.UI.translates['okButton'] = "${message(code:'default.field.buttonOk')}";
	Exxo.UI.translates['saveButton'] = "${message(code:'default.field.buttonSave')}";
	Exxo.UI.translates['cancelButton'] = "${message(code:'default.field.buttonCancel')}";
	/* Required for charts */
	Exxo.UI.urls["get-main-charts-data-url"] = "${g.createLink(controller: 'boxMetrics', action: 'getUsageEventDataAjax', absolute: true)}";
    Exxo.UI.translates["total-day"] = "${ message(code:'total.day')}";
    Exxo.UI.urls["gen-file-url"] = "${g.createLink(controller: 'boxMetrics', action: 'generateMainMetricsFile', absolute: true)}";
    google.load("visualization", "1", {packages:["corechart"]});
</script>
</head>
<body>
<g:render template="/layouts/internalContentTemplate">
	<h1 id="locationTitle">
		${ location.name.encodeAsHTML() }
	</h1>
		
	<div class="content">
		<g:render template="/layouts/messagesAndErrorsTemplate" model="[entity: location]"/>
		<!-- Start of Tabs -->
		<ul id="tabs">
		    <li><a href="#info">${ message(code:'info') }</a></li>
		    <li><a href="#exXtractors">${ message(code:'exxtractors') }</a></li>
		    <li><a href="#skin">${ message(code:'app.setting') }</a></li>
		    <li><a href="#welcomeAd">${ message(code:'welcome.ad') }</a></li>
		    <li><a id="boxContentLink" href="#contents">${ message(code:'sliding.banners') }</a></li>
		    <li><a id="boxOffersLink" href="#offers">${ message(code:'offers') }</a></li>
		    <li><a id="boxDocumentsLink" href="#documents">${ message(code:'documents') }</a></li>
		    <li><a id="boxUsersLink" href="#boxUsers">${ message(code:'exxtractor.users') }</a></li>
		    <li><a id="usageReportLink" href="#usageReport">${ message(code:'usage.report') }</a></li>
		</ul>
		 
		<div id="info" class="tab-section">
		    <div class="content form-info" style="margin-bottom: 0px;">
				<!-- Left Column -->			
				
				<div class="col-xs-6 resize-formleft">
					<span id="companyDiv">
		                <div class="col-xs-4 text-to-right">
		                  <label for="company">${ message(code:'default.field.companyName') } </label>
		                </div>
		                <div class="col-xs-8 text-to-left">
		                  <g:if test="${ location.company }">
		                      <g:link controller="company" action="dashboard" id="${location.company.id}">
	                            ${ location.company.encodeAsHTML() }
	                          </g:link>
		                  </g:if>
		                  <g:else>
		                      ${ message(code:"default.field.empty") }
		                  </g:else>
		                  
		                </div>
		            </span>
		            <span class="clearfix"></span>
		            <span id="commercialIntegratorDiv">
                        <div class="col-xs-4 text-to-right">
                          <label for="commercialIntegrator">${ message(code:'default.field.commercialntegrator') } </label>
                        </div>
                        <div class="col-xs-8 text-to-left">
                          <g:if test="${ location.commercialIntegrator }">
                              <g:link controller="companyIntegrator" action="show" id="${location.commercialIntegrator.id}">
                                ${ location.commercialIntegrator?.companyName?.encodeAsHTML() }
                              </g:link>
                          </g:if>
                          <g:else>
                              ${ message(code:"default.field.empty") }
                          </g:else>
                          
                        </div>
                    </span>
                    <span class="clearfix"></span>
					<custom:editField type="venueLabelLink" venueId="${location.id}" name="locationType" value="${location.type.name ? message(code:"typeOfEstablishment." + location.type?.code?.encodeAsHTML()): message(code:"default.field.empty")}" label="default.field.typeOfEstablishment" />
					<g:if test="${ location.otherType != null }">
						<custom:editField name="otherType" value="${location.otherType ? location.otherType?.encodeAsHTML() : message(code:"default.field.empty")}" label="default.field.specify" />
					</g:if>
					<custom:editField type="venueLabelLink" venueId="${location.id}" name="address" value="${location.address ? location.address?.encodeAsHTML() : message(code:"default.field.empty")}" label="default.field.address" />
					<g:if test="${location.state != null}">
						<custom:editField type="venueLabelLink" venueId="${location.id}" name="country" value="${location.state ? message(code:"country.code." + location.state?.country?.shortName?.encodeAsHTML()) : message(code:"default.field.empty")}" label="default.field.country" />
					</g:if>
					<g:else>
						<custom:editField type="venueLabelLink" venueId="${location.id}" name="country" value="${location.state==null ? message(code:"country.code." + location.country?.shortName?.encodeAsHTML()) : message(code:"default.field.empty")}" label="default.field.country" />
					</g:else>
					
					<custom:editField type="venueLabelLink" venueId="${location.id}" name="stateOrProvince" value="${location.state ? message(code:"state.code." + location.state?.name): (location.stateName ? location.stateName.encodeAsHTML():message(code:"default.field.empty"))}" label="default.field.stateOrProvince" />
					<custom:editField type="venueLabelLink" venueId="${location.id}" name="city" value="${location.city ? location.city?.encodeAsHTML() : message(code:"default.field.empty")}" label="default.field.city" />
					<custom:editField type="venueLabelLink" venueId="${location.id}" name="timezone" value="${location.timezone ? location.timezone?.encodeAsHTML() : message(code:"default.field.empty")}" label="default.field.timezone" />
					
					<custom:editField name="useCompanyLogo" value="${location.useCompanyLogo == true ? message(code:"default.field.yes") : message(code:"default.field.no")}" label="location.use.company.logo" />
					<div class="col-xs-4 text-to-right">
						<label><g:message code="logo" />
						</label>
					</div>				
					<div class="col-xs-8 text-to-left">
						<g:if test="${ location.useCompanyLogo }">
							<g:if test="${ location.company?.logoUrl }">
								<i><img class="ad-background-img" src="${location.company?.logoUrl}" style="width:150px; height:150px;"></i>
							</g:if>
							<g:else>
								${ message(code:'company.logo.not.found') }
							</g:else>
						</g:if>
						<g:else>
							<g:if test="${ location.logoUrl }">
								<i><img class="ad-background-img" src="${location.logoUrl}" style="width:150px; height:150px;"></i>
							</g:if>
							<g:else>
								${ message(code:'location.logo.not.found') }
							</g:else>
							
						</g:else>
					</div>
					<span class="clearfix"></span>
				</div>
				
				<!-- Right Column -->
				<div class="col-xs-6 resize-formright">
				<custom:editField type="venueLabelLink" venueId="${location.id}" name="facebookId" value="${location.facebookId}" label="default.field.facebook.id" />
					<custom:editField type="venueLabelLink" venueId="${location.id}" name="pocEmail" value="${location.pocEmail}" label="poc.email" />
					<custom:editField type="venueLabelLink" venueId="${location.id}" name="pocPhone" value="${location.pocPhone}" label="poc.phone" />
					<custom:editField type="venueLabelLink" venueId="${location.id}" name="pocFirstName" value="${location.pocFirstName}" label="poc.firstname" />
					<custom:editField type="venueLabelLink" venueId="${location.id}" name="pocLastName" value="${location.pocLastName}" label="poc.lastname" />
					<custom:editField name="pocAccountCreated" value="${location.pocAccountCreated == true ? message(code:"default.field.yes") : message(code:"default.field.no")}" label="poc.account.created" />
				
					<custom:editField type="venueLabelLink" venueId="${location.id}" name="maxOccupancy" value="${location.maxOccupancy}" label="default.field.maxOccupancy" />
					<custom:editField type="venueLabelLink" venueId="${location.id}" name="numberOfTv" value="${location.numberOfTv}" label="default.field.numberOfTv" />
					<custom:editField type="venueLabelLink" venueId="${location.id}" name="numberOfTvWithExxothermicDevice" value="${location.numberOfTvWithExxothermicDevice}" label="default.field.numberOfTvWithExxothermiceDevice" />
					<custom:editField type="venueLabelLink" venueId="${location.id}" name="contentsLimit" value="${location.contentsLimit}" label="default.field.contentsLimit" />
					
					<custom:editField type="venueLabelLink" venueId="${location.id}" name="createdBy" value="${location.createdBy ? location.createdBy?.firstName.encodeAsHTML() + " "  + location.createdBy?.lastName.encodeAsHTML() : message(code:"default.field.empty")}" label="default.field.createdBy" />
					<custom:editField type="venueLabelLink" venueId="${location.id}" name="latitude" value="${location.latitude ? location.latitude?.encodeAsHTML() + ' ' + message(code:"default.field.latitude") : message(code:"default.field.empty")}" label="default.field.gpsLocation" />
					<custom:editField type="venueLabelLink" venueId="${location.id}" name="longitude" value="${location.longitude ? location.longitude?.encodeAsHTML() + ' ' + message(code:"default.field.longitude"): message(code:"default.field.empty")}" label="" />
					<custom:editField type="venueLabelLink" venueId="${location.id}" name="enabled" value="${location.enable == true ? message(code:"default.field.yes") : message(code:"default.field.no")}" label="default.field.enabled" />
				</div>
				<span class="clearfix"></span>				
			</div>
		</div>
		<div id="exXtractors" class="tab-section">
		    <g:if test="${ location.boxes.size() > 0}">
		    	<g:render template="/box/boxesTableTemplate" model="[boxes: location.boxes]"/>
		    </g:if>
		    <g:else>
		    	<div class="content form-info" style="margin-bottom: 0px;">
			    	<g:message code="location.no.boxes"/>
			    </div>
		    </g:else>
		</div>
		<div id="skin" class="tab-section">
			<div class="content form-info" style="margin-bottom: 0px;">
			<g:if test="${ location.activeSkin != null }">
		    	<g:render template="/appSkin/showTemplate" model="[entity: location.activeSkin]"/>
		    </g:if>
		    <g:else>
		    	<g:message code="location.no.app.setting"/>
		    </g:else>
		    </div>
		</div>
		<div id="welcomeAd" class="tab-section">
		    <div class="content form-info" style="margin-bottom: 0px;">
			<g:if test="${ location.welcomeAd != null }">
		    	<g:render template="/welcomeAd/showTemplate" model="[entity: location.welcomeAd]"/>
		    </g:if>
		    <g:else>
		    	<g:message code="location.no.welcome.ad"/>
		    </g:else>
		    </div>
		</div>
		<div id="contents" class="tab-section">
		    <g:if test="${ location.contents.size() > 0}">
		    	<g:render template="/content/contentsTableTemplate" model="[tableId:'contentOrderedList', contentsList: locationContents, orderEnabled: true, locationId: location.id]"/>
		    	<span id="contentsOrder" class="form-btn btn-primary  btn-primary-large">
		          ${message(code:'default.location.save.contents.order')}
		       </span>   
		    </g:if>
		    <g:else>
		    	<div class="content form-info" style="margin-bottom: 0px;">
			    	<g:message code="location.no.contents"/>
			    </div>
		    </g:else>
		    
		    <!-- Information with the status of every transaction -->
			<g:render template="/content/contentsSyncResultTemplate" model="[jsonTransactionStatus: syncResultContent]" />
		</div>
		<div id="offers" class="tab-section">
		    <g:if test="${ offers.size() > 0}">
		    	<g:render template="/offer/contentsTableTemplate" model="[contentsList:  offers, locationId: location.id]"/>
		    </g:if>
		    <g:else>
		    	<div class="content form-info" style="margin-bottom: 0px;">
			    	<g:message code="location.no.offers"/>
			    </div>
		    </g:else>
		    
		    <!-- Information with the status of every transaction -->
			<g:render template="/content/contentsSyncResultTemplate" model="[jsonTransactionStatus: syncResultContent]" />
		</div>
		<div id="documents" class="tab-section">
		    <g:if test="${ location.documents.size() > 0}">
		    	<g:render template="/document/documentsTableTemplate" model="[documentsList:  location.documents, locationId: location.id]"/>
		    </g:if>
		    <g:else>
		    	<div class="content form-info" style="margin-bottom: 0px;">
			    	<g:message code="location.no.documents"/>
			    </div>
		    </g:else>
		    
		    <!-- Information with the status of every transaction -->
			<g:render template="/document/syncResultTemplate" model="[jsonTransactionStatus: syncResultDocument]" />
		</div>
		
		<div id="boxUsers" class="tab-section" >
			<div class="content form-info" style="margin-bottom: 0px;">
					<g:message code="location.user.details"/>
		    </div>
		
			<g:if test="${ location.boxUsers.size() > 0}">
		    	<g:render template="/exxtractorsUser/table" model="[location: location]"/>
		    </g:if>
		    <g:else>
		    	<div class="content form-info" style="margin-bottom: 0px;">
					<g:message code="location.no.exxtractor.users"/>
			    </div>
		    </g:else>
		    
		    <!-- Information with the status of every transaction for ExXtractor Users Sync -->
			<g:if test="${syncResultExXUsers}" >
				<br/>
	   			<legend>
	        		<g:message code="boxes.sync.results" />
				</legend>      					         			    
	    		<table id="table-list-two" class="table-list display" cellspacing="0" width="100%">
	    			<thead>
						<tr>
							<th>${message(code: 'default.field.serial')}</th>
			    			<th>${message(code: 'default.field.status')}</th>
			    			<th>${message(code: 'default.field.errorDescription')}</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>${message(code: 'default.field.serial')}</th>
			    			<th>${message(code: 'default.field.status')}</th>
			    			<th>${message(code: 'default.field.errorDescription')}</th>
						</tr>
					</tfoot>	
	  				<tbody style="text-align='center'">
	      			<g:each in="${syncResultExXUsers?.usersStatus}" var="record">
	     				<tr>
	   						<td class="">${record.serialNumber}</td>
	   						<td class="">${message(code:'boxes.sync.result.status.'+record.successful)}</td>
	   						<td>${record.successful? '':message(code:messageErrorDescriptionHash.get(record.errorCode))}</td>	          					
	   					</tr>
	      			</g:each>
	      			<g:each in="${syncResultExXUsers?.data}" var="record">
	     				<tr>
	   						<td class="">${record.serialNumber}</td>
	   						<td class="">${message(code:'boxes.sync.result.status.'+record.successful)}</td>
	   						<td>${record.successful? '':message(code:messageErrorDescriptionHash.get(record.errorCode))}</td>	          					
	   					</tr>
	      			</g:each>
	    			</tbody>
	    		</table>	          			  			     	
	        </g:if>
		</div>
		<div id="usageReport" class="tab-section" >
            <g:form controller="boxMetrics" action="getData" name="getDataForm" class="exxo-form">
	            <div class="col-md-12 form-bordered" style="padding-bottom: 25px;">
	                
	                <!-- Start Date  -->
	                <div class="col-xs-6 resize-formleft">
	                    <custom:editField type="date" name="startDate" required="${true}" value="${ formatDate(format:'MM/dd/yyyy', date:new java.util.Date()) }" 
	                        minusDays="-7" label="start.date" leftColumnPosition="left" note="${ message(code:'schedule.date.format') }" readonly="true"/>
	                </div>
	                
	                <!-- End Date  -->
	                <div class="col-xs-6 resize-formright">
	                    <custom:editField type="date" name="endDate" required="${true}" value="${ formatDate(format:'MM/dd/yyyy', date:new java.util.Date()) }" 
	                        label="end.date" minusDays="-1" leftColumnPosition="left" note="${ message(code:'servers.usage.reports.yesterday.note') }"  readonly="true"/>
	                </div>
	                <span class='clearfix'></span>
	                
	                <!-- Location -->
	                <input type="hidden" id="location" name="location" value="${ location.id }"/>
	            </div>
	            <!-- Buttons -->
	                    <g:submitButton class="form-btn btn-primary" name="save" value="${message(code:'default.table.sSearch')}" />
	                    <a href="" id="genFileButton">
	                       <span class="form-btn btn-primary"> ${message(code:'generate.file')} </span>       
	                    </a>
            </g:form>
            <span class='clearfix'></span>
            <div id="main-charts-container" class="col-md-12" style="margin-top: 25px; display: none;">
                <div class="clientChart col-md-6">
                    <p><b>Number of unique people who sign on per day per venue</b></p>
                    <div id="clients-per-day-charts-no-data">
                        <div class="alert alert-warning">
                            ${ message(code:'no.data.for.range') }
                        </div>
                    </div>
                    <div id="clients-per-day-charts"></div>
                </div>
                <div class="clientChart col-md-6">
                    <p><b>Total number of minutes used per day per venue</b></p>
                    <div id="time-per-day-charts-no-data">
                        <div class="alert alert-warning">
                            ${ message(code:'no.data.for.range') }
                        </div>
                    </div>
                    <div id="time-per-day-charts"></div>
                </div>
                <div class="clientChart col-md-6">
                    <p><b>Peak number of simultaneous users per day per venue</b></p>
                    <div id="max-concurrent-per-day-charts-no-data">
                        <div class="alert alert-warning">
                            ${ message(code:'no.data.for.range') }
                        </div>
                    </div>
                    <div id="max-concurrent-per-day-charts"></div>
                </div>                
            </div>
            <span class='clearfix'></span>
        </div>
		<!-- End of Tabs -->
			        
		<!-- Buttons -->										
		<g:link action="listAll" controller="location"  > 
	       <span class="form-btn btn-primary">
	          ${message(code:'default.field.buttonBack')}
	       </span>       
    	</g:link>
	</div>
</g:render>
<div id="confirm-delete-exxtractor-user-dialog" title="${message(code:'default.confirm.title')}" style="display:none;">
  ${message(code:'general.object.confirm.delete')}
</div>
<div id="confirm-delete-dialog" title="${message(code:'default.confirm.title')}" style="display:none;">
  ${message(code:'general.object.confirm.delete')}
</div>
<div id="confirm-unassign-dialog" title="${message(code:'default.confirm.title')}" style="display:none;">
${ message(code:'confirm.unassign.content.of.location') }
</div>
</body>
</html>