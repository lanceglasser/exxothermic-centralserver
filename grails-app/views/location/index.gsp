<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.listCompanyLocation" /> -
	${company?.name.encodeAsHTML()}</title>
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
<script
	src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
<script
	src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
<script>
			Exxo.module = "location-list";			
		</script>
</head>
<body>
	<g:render template="/layouts/internalContentTemplate">
		<h1>
			<g:message code="default.title.listCompanyLocation" />
			-
			${company?.name.encodeAsHTML()}
		</h1>
		<g:render template="/layouts/messagesAndErrorsTemplate"/>
		<div class="content">
			<g:if test="${locations.size()>0}">
				<table id="table-list" class="display" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>
								${message(code: 'default.field.locationName')}
							</th>
							<th>
								${message(code: 'default.field.commercialntegrator')}
							</th>
							<th>
								${message(code: 'default.field.stateOrProvince')}
							</th>
							<th>
								${message(code: 'default.field.city')}
							</th>
							<%--					<th class="width10 sorter ">${message(code: 'default.field.activeSkin')}</th>--%>
							<th>
								${message(code: 'default.field.enabled')}
							</th>
							<th>
								${message(code: 'default.table.action')}
							</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>
								${message(code: 'default.field.locationName')}
							</th>
							<th>
								${message(code: 'default.field.commercialntegrator')}
							</th>
							<th>
								${message(code: 'default.field.stateOrProvince')}
							</th>
							<th>
								${message(code: 'default.field.city')}
							</th>
							<%--					<th class="width10 sorter ">${message(code: 'default.field.activeSkin')}</th>--%>
							<th>
								${message(code: 'default.field.enabled')}
							</th>
							<th>
								${message(code: 'default.table.action')}
							</th>
						</tr>
					</tfoot>
					<tbody>
						<g:each in="${locations}" status="i" var="locationInstance">
							<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
								<td>
									<sec:ifAnyGranted roles="ROLE_INTEGRATOR, ROLE_HELP_DESK">
										<g:link action="edit" id="${locationInstance.id}">
											${locationInstance.name.encodeAsHTML()}
										</g:link>
									</sec:ifAnyGranted>
									
									<sec:ifNotGranted roles="ROLE_INTEGRATOR, ROLE_HELP_DESK">
										${locationInstance.name.encodeAsHTML()}
									</sec:ifNotGranted>
								</td>
								<td><g:if test="${locationInstance.commercialIntegrator}">
										${locationInstance.commercialIntegrator?.companyName?.encodeAsHTML()}
									</g:if> <g:else>
										${locationInstance.company?.companyWideIntegrator?.companyName?.encodeAsHTML()}
									</g:else></td>
								<td><g:if test="${locationInstance.state}">
										${g.message(code:"state.code." + locationInstance.state.name)}
									</g:if> <g:else>
										${locationInstance.stateName.encodeAsHTML()}
									</g:else></td>
								<td class="widthTdRow15">
									${locationInstance.city?.encodeAsHTML()}
								</td>
								<%--						<td class="breakWord widthTdRow15">--%>
								<%--								<g:if test="${location.activeSkin}">--%>
								<%--									${location.activeSkin.encodeAsHTML()}--%>
								<%--								</g:if>--%>
								<%--								<g:else>--%>
								<%--									<g:message code="default.field.empty" />--%>
								<%--								</g:else>	--%>
								<%--						</td>--%>
								<td><g:if test="${locationInstance.enable == true}">
										<g:message code="default.field.yes" />
									</g:if> <g:else>
										<g:message code="default.field.no" />
									</g:else></td>
								<td><g:link action="show" id="${locationInstance.id}"
										class="action-icons view"
										" title="${message(code:'default.action.show')}"></g:link> 
									
									<sec:ifAnyGranted roles="ROLE_INTEGRATOR, ROLE_HELP_DESK">
										<g:link action="edit" id="${locationInstance.id}"
											class="action-icons edit"
											title="${message(code:'default.action.edit')}"></g:link>
										<g:link controller="box" action="register"
											params="${["location":locationInstance.id]}"
											class="action-icons addBox"
											title="${message(code:'default.box.register')}"></g:link>
									</sec:ifAnyGranted>
									
									<g:if test="${locationInstance.boxes?.size() > 0}">
										<g:link controller="box" action="index"
											class="action-icons boxInfo" id="${locationInstance.id}"
											title="${ message(code:'default.box.box') + "(" + locationInstance.boxes?.size() + ")" }"></g:link>
									</g:if></td>
							</tr>
						</g:each>
					</tbody>
				</table>
			</g:if>
		</div>
	</g:render>
</body>
</html>