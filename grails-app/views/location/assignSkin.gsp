<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title><g:message code="default.title.assignSkin" /></title>
		<link rel="stylesheet" href="${resource(dir: 'css/pluggins/select2', file: 'select2.css')}" type="text/css">
		<script src="${resource(dir: 'js/pluggins/select2', file: 'select2.js')}"></script>
		<script src="${resource(dir: 'js/pages/companylocation', file: 'assignSkin.js')}"></script>
	</head>
	<body>
		<g:form action="setActiveSkin" name="createForm" class="form-horizontal">		
		 <fieldset>  
          <legend><g:message code="default.title.assignSkin" /></legend>			 
      	 
          <g:if test="${flash.error}">
       		<div class="alert alert-error">  
			  <a class="close" data-dismiss="alert">×</a>  
			  ${flash.error} 
			  
			<g:hasErrors bean="${location}">
			  		<ul>
						<g:eachError bean="${location}" var="error">
							<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
						</g:eachError>
					</ul>
			  </g:hasErrors>	
			  
			  	
				</div> 
				 ${flash.error=null}
      		</g:if>
      		
      		 <g:if test="${flash.success}">
	        		<div class="alert alert-success">  
					  <a class="close" data-dismiss="alert">×</a>  
					  ${flash.success} 
					  ${flash.success=null}
					</div> 
      		</g:if>
			
			<div class="control-group">
				<label class="control-label" for="user">
					<g:message code="default.field.location" />*
				</label>
				<div class="controls">					
					<g:select name="location"  from="${locations}" optionKey="id"  optionValue="name" required="required" value="${location.id}"  noSelection="['':'']" 
					   placeholder="${message(code:'default.field.selectLocation')}" 
					   onchange="${remoteFunction(
					            action:'ajaxAssignSkin', 
					            params:'\'locationId=\' + escape(this.value)',
								update : 'infor')}"/>							
				</div>					 
			</div>
			
			<div class="control-group table" id="infor"></div>
			
			
				
			
						 			
			<div class="form-actions">
				<g:submitButton class="btn btn-primary"
					name="setActiveSkin"
					value="${message(code:'default.field.buttonSave')}" />		
			
				<g:link action="listAll" class="back-button">        
			       <span class="btn btn-primary">
			          ${message(code:'default.field.buttonCancel')}
			       </span>       
			    </g:link>
				    
				    		 			
			</div>  
			</fieldset> 
		</g:form>
	</body>
</html>