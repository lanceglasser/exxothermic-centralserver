	<div class="control-group">
				<label class="control-label" for="email"><g:message code="default.field.activeSkin" />*</label>
				<div class="controls">
					<g:textField class="input-xlarge"   disabled="true" name="activeSkin"   value="${location.activeSkin?.encodeAsHTML()}"/>
				</div>
			</div>
			

<div class="control-group">


		<label class="control-label" for="skins"><g:message code="default.field.appSkin" />*</label>
		
		<g:if test="${skins.size() == 0 }">
			<div class="controls" style="">
				<tr>
					<div class="hero-unit special-box-login">
						<g:message code="default.associate.skins.dont.exists"/> 
						<g:link controller="appSkin" action="assign"><g:message code="default.title.assignSkin"/></g:link>			
					</div>
				</tr>
			
			</div>
		</g:if>	
		<g:else>	
			
			<div class="controls vertical-scroll max-height-400" style="">
				<table class='table table-striped'>
				
				
				
					<g:each in="${skins}" status="i" var="appSkin">
						
						<g:if test="${(i % 2) == 0}">
							<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						</g:if>									
							  <td>
								<g:radio checked="${location?.activeSkin?.id==appSkin.id}"  name="skin" value="${appSkin.id}" /> ${appSkin?.name?.encodeAsHTML()}
							   </td>
							   
							  <g:if test="${(i % 2) != 0}">									 		 
								</tr>
							  </g:if>								  		
		
					 
						<g:if test="${(skins.size()-1) == i}">
							</tr>		
						</g:if> 							
					</g:each>	
										
				</table>
			</div>
		</g:else>
</div>