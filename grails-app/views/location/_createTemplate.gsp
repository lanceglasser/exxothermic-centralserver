
<g:render template="/layouts/messagesAndErrorsTemplate" model="[entity: location]"/>

<p class="muted"> <g:message code="create.location.note" /></p>

<g:uploadForm action="save" name="wizardForm" class="exxo-form">  
	<g:hiddenField name="integratorId" value="${location.commercialIntegrator?.id}"/>
    <g:hiddenField name="latitude" value="${location.latitude}"/>
    <g:hiddenField name="longitude" value="${location.longitude}"/>

<!-- Left Column -->
    <div class="col-xs-6 resize-formleft">
        <custom:editField type="text" name="name" required="${true}" value="${location.name}" 
            label="default.field.locationName" tooltip="tooltip.location.name" />
                        
        <div class="col-xs-4 text-to-right">
            <label  for="company">
                <div class="questionToolTip" title="${message(code:'tooltip.select.company')}"></div>
                <g:message code="default.field.companyName" />
            </label>
        </div>              
        <div class="col-xs-8 text-to-left">         
            <g:select name="company.id" id="company" from="${companies}" optionKey="id" optionValue="${{it.name}}" value="${location.company?location.company.id:params.company}" 
                      onchange="${remoteFunction(
                            controller: 'location',
                            action:'ajaxUpdateLocationIntegrator',                          
                            params:'\'integratorId=\'+document.getElementById(\'integratorId\').value+\'&companyId= \' + this.value',
                            update : 'select_integrator'
                            )}"/>
        </div>
        <span class="clearfix"></span>
    
        <div  id="select_integrator">
        </div>      
        <span class="clearfix"></span>
        
        <!-- Use the Company Information -->
        <custom:editField type="checkbox" name="useCompanyinformationCheckbox" required="${false}" value="${false}" 
            label="default.field.useCompanyinformation" rightColumnPosition="right" cssclass="input-checkbox" leftColumnSize="10"/>

        <div class="col-xs-4 text-to-right">        
            <label class="control-label" for="type"><g:message code="default.field.typeOfEstablishment" /> *</label>
        </div>                  
        <div class="col-xs-8 text-to-left">
            <custom:selectWithOptGroupCompanyType id="type" name="type" from="${typesCompany}" value="${location.type?.code}" />
        </div>  
        <span class="clearfix"></span>
            
        <div id="controlCompanyOtherType">
            <custom:editField type="text" name="otherType" required="${true}" value="${location.otherType}" 
                label="default.field.specify"  />
        </div>
        
        <div class="col-xs-4 text-to-right">
            <label for="country"><g:message code="default.field.country" /> *</label>
        </div>  
        <div class="col-xs-8 text-to-left">
            <select name="country.id" id="country" tabindex="-1" title="Country*" value="${location.country?.id}">
                <g:each in="${countries}" status="i" var="country">
                <option value="${ country.id }" code="${ country.code }">${ message(code:'country.code.'+ country.shortName) }</option>
                </g:each>
            </select>   
        </div>
        <span class="clearfix"></span>      
        
        <g:if test="${location.state}">
            <div id="stateDiv" >
        </g:if>
        <g:else>
            <div id="stateDiv" style="display:none;">
        </g:else>
            <div class="col-xs-4 text-to-right">
                <label for="state"><g:message code="default.field.stateOrProvince" /> *</label>
            </div>  
            <div class="col-xs-8 text-to-left">                  
                <select name="state.id" id="state" tabindex="-1" title="State*" value="${location.state?.id}">
                    <g:each in="${states}" status="i" var="state">
                    <option value="${ state.id }" code="${ state.name }">${ message(code:'state.code.'+ state.name) }</option>
                    </g:each>
                </select>
            </div>
            <span class="clearfix"></span>
        </div>

        <g:if test="${location.state}">
            <div id="stateNameDiv"  style="display:none;">
        </g:if>
        <g:else>
            <div id="stateNameDiv">
        </g:else>
            <div class="col-xs-4 text-to-right">
              <label for="stateName"> ${ message(code:'default.field.stateOrProvince') } * </label>
            </div>
            <div class="col-xs-8 text-to-left">
                <input type="text" id="stateName" name="stateName" class="" value="" placeholder="" maxlength="50"
                    ${location.state == null? ' required="required"':''}> 
            </div>
            <span class="clearfix"></span>
        </div>

        <custom:editField type="text" name="city" max="100" required="${true}" value="${location.city}" 
            label="default.field.city" />

        <div class="col-xs-4 text-to-right">
          <label for="type"><g:message code="default.field.timezone" /> *</label>
        </div>
        <div class="col-xs-8 text-to-left">
          <custom:timeZoneSelect name="timezone" value="${location.timezone}" />
        </div>
        <span class="clearfix"></span>
 
        <custom:editField type="textarea" name="address" required="${true}" value="${location.address}" 
            label="default.field.address" cssclass="input-textarea"/>

        <!-- useCompanyLogo -->
        <custom:editField type="checkbox" name="useCompanyLogo" required="${false}" value="${location.useCompanyLogo}" 
            label="location.use.company.logo" rightColumnPosition="right" leftColumnSize="10" 
            cssclass="input-checkbox" tooltip="location.use.company.logo.help"/>

        <div id="companyLogo" style="display: none;">
            <div class="col-xs-4 text-to-right">
                <label>${message(code:'company.logo')}</label>
            </div>
            <div class="col-xs-8 text-to-left">
                <img id="companyLogoImg" style="width:300px; height:auto; padding: 5px ; border: 1px #bbb solid;" src="${location.company?.logoUrl}"/>
                <div id="notCompanyLogoMessage">
                    <p></p>
                    <span class="liten-up"> ${ message(code:'company.logo.not.found') } </span>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>

        <div id="uploadOwnLogo" style="display: none;">
            <!-- Logo Image -->
                <g:render template="/layouts/tags/imageUploadSelectorTemplate" 
                    model="[
                        id:             'bgLogoUrl',
                        ref:            '1',
                        imgValue:       location.logoUrl? location.logoUrl:'',
                        label:          message(code:'location.logo'),
                        cropImage:      true,
                        minWidth:       mycentralserver.utils.Constants.COMPANY_LOGO_WIDTH,
                        minHeight:      mycentralserver.utils.Constants.COMPANY_LOGO_HEIGHT,
                        cropWidth:      mycentralserver.utils.Constants.DIALOG_CONTENT_CROP_TABLET_WIDTH,
                        cropHeight:     mycentralserver.utils.Constants.DIALOG_CONTENT_CROP_TABLET_HEIGHT
                        ]"
                />
        </div>
        <span class="clearfix"></span>
    </div>

    <!-- Right Column -->
    <div class="col-xs-6 resize-formright">
    	<sec:ifAnyGranted roles="ROLE_HELP_DESK">
    	<span id="mediaInfoDiv" class="col-xs-12 text-to-right">
    		<label>${ message(code:'venue.social.information') }</label>
		</span>
		<span class="clearfix"></span>
            
    	<custom:editField type="text" name="facebookId" required="${false}" value="${location.facebookId}" 
            label="default.field.facebook.id" tooltip="tooltip.location.facebook.id" />
         
        <span id="mediaInfoDiv" class="col-xs-12 text-to-right">
    		<label>${ message(code:'extra.information') }</label>
		</span>
		<span class="clearfix"></span>
		</sec:ifAnyGranted>

        <custom:editField type="number" name="maxOccupancy" value="${location.maxOccupancy}" 
            label="default.field.maxOccupancy" cssclass="onlyNumbers" min="0" step="1" leftColumnSize="8"/>
        
        <custom:editField type="number" name="numberOfTv" value="${location.numberOfTv}" 
            label="default.field.numberOfTv" cssclass="onlyNumbers" min="0" step="1" leftColumnSize="8"/>
            
        <custom:editField type="number" name="numberOfTvWithExxothermicDevice" required="${true}" value="${location.numberOfTvWithExxothermicDevice}" 
            label="default.field.numberOfTvWithExxothermiceDevice" cssclass="onlyNumbers" min="0" step="1" leftColumnSize="8"/>
        
        <g:if test="${location.contentsLimit == 0}">
            <g:set var="climit" value="${6}" />
        </g:if>
        <g:else>
            <g:set var="climit" value="${location.contentsLimit}" />
        </g:else>    
        
        
        <custom:editField type="number" name="contentsLimit"  
        value="${climit}" label="default.field.contentsLimit" tooltip="tooltip.location.contentsLimit" cssclass="onlyNumbers" min="1" max="6" step="1" leftColumnSize="8"/>
            
                            
        <g:if test="${comingFromWizard == false}">
            <!-- Enable -->
            <custom:editField type="checkbox" name="enable" required="${false}" value="${location.enable}" 
                label="default.field.enabled" leftColumnSize="8" cssclass="input-checkbox" tooltip="tooltip.location.enable"/>
        </g:if>             
    
        <!--  
        <div class="control-group">
            <label class="control-label" for="gpsLocation"  ><g:message code="default.field.gpsLocation" /> <g:message code="default.field.optionaL" /></label>
            <div class="controls">
                <g:textField  class="input-xlarge"  maxlength="15" name="latitude" value="${location.latitude}" />  <g:message code="default.field.latitude" />
                <p></p>
                <g:textField  class="input-xlarge"  maxlength="15" name="longitude" value="${location.longitude}" />  <g:message code="default.field.longitude" />
            </div>                               
        </div>
        -->
        
        <!-- CS-16: Add Map (Google) to Location-->
        
        <div class="control-group">
            <div id="map-canvas"  style="width: 100%; height: 300px;"></div>
        </div>
    </div>
    <span class="clearfix"></span>

	<!-- Buttons -->
	<g:if test="${comingFromWizard == false}">
		<g:submitButton class="form-btn btn-primary" name="save" value="${message(code:'default.field.buttonSave')}" />								
		<g:link action="index" controller="home" class="back-button">        
	       <span class="form-btn btn-primary"> ${message(code:'default.field.buttonCancel')} </span>       
    	</g:link>		    	
 	</g:if>
 	<g:else>		    	 
 		<g:if test="${location.id != null}" >
 			<g:submitButton disabled="true"  name="continue" value="${message(code:'default.field.buttonContinue')}"  class="form-btn btn-primary" />
 		</g:if>
 		<g:else>
 			<g:submitButton name="continue" value="${message(code:'default.field.buttonContinue')}"  class="form-btn btn-primary" />
 		</g:else>
 		
    	<af:ajaxButton name="cancel" value="${message(code:'default.field.buttonCancel')}" afterSuccess="onPage();" class="form-btn btn-primary" />	  
	</g:else>
</g:uploadForm>