<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title><g:message code="default.title.newLocation" /></title>		
		<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=AIzaSyB_3H-gp22pdmUYCfpFZG04crqtTDE8nwk"></script> 
		<!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>--> 	
    	<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_location_create.js')}"></script>
    	<script src="${resource(dir: 'themes/default/js', file: 'google_maps.js')}"></script>	
    	<script src="${resource(dir: 'themes/default/js', file: 'application.js')}"></script>
    	<!-- Image Crop Required Files -->
		<link rel="stylesheet" href="${resource(dir: 'css/jcrop', file: 'jquery.Jcrop.css')}" type="text/css">
		<script src="${resource(dir: 'js/jcrop', file: 'jquery.Jcrop.js')}"></script>
		<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_images_upload.js')}"></script>
		<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_utils.js')}"></script>
	    <script>
			Exxo.module = "location-create";
			Exxo.UI.vars['useCompanyLogo'] = ${location.useCompanyLogo};
			Exxo.UI.vars['lat'] = ${location.latitude? location.latitude: -1};
			Exxo.UI.vars['lng'] = ${location.longitude? location.longitude : -1};
			getCompanyInfoURL = "${g.createLink(controller: 'location', action: 'ajaxGetCompanyInformation', absolute: true)}";  
	        loadStatesByCountryUrl = "${g.createLink(controller: 'common', action: 'statebycountry', absolute: true)}";
			Exxo.UI.urls["loadStatesByCountryUrl"] = "${g.createLink(controller: 'common', action: 'statebycountry', absolute: true)}";
			Exxo.UI.urls["getCompanyInfoURL"] = "${g.createLink(controller: 'location', action: 'ajaxGetCompanyInformation', absolute: true)}";
			Exxo.Location.Create.defaultTimezone = 'UTC';  
			var dimensionsImageError = "${message(code:'customButton.image.dimension.error')}";
			var invalidImageError = "${message(code:'image.invalid.error')}";
			var tooBigImageError = "${message(code:'image.big.error')}";
			Exxo.UI.translates[Exxo.Constants.ERROR_FILE_INVALID] = "${message(code:'file.invalid.error')}";
			Exxo.UI.translates[Exxo.Constants.ERROR_FILE_SIZE] = "${message(code:'file.big.error')}";
		</script>
	</head>
	<body>
		<g:render template="/layouts/internalContentTemplate">
			<h1>
				<g:message code="default.title.newLocation" />
			</h1>
			<div class="content">
				<g:render template="createTemplate"></g:render>
			</div>
		</g:render>
	</body>	
</html>
