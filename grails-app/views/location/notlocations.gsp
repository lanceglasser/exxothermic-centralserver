<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.listLocation" /></title>
<script>
	Exxo.module = "empty-list";
</script>
</head>
</head>
<body>
	<section class="introp">
			<div class="intro-body">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2" id="home-salute">
							<div id="child-salute">
								<div class="message">
									<sec:ifAnyGranted roles="ROLE_INTEGRATOR, ROLE_HELP_DESK">
										<p><g:message code="default.location.dont.exists"/></p>
										<g:link controller="location" action="create"><g:message code="default.companies.newLocation"/></g:link>
									</sec:ifAnyGranted>		
									<sec:ifNotGranted roles="ROLE_INTEGRATOR, ROLE_HELP_DESK">
										<p><g:message code="default.no.location.exists"/></p>
									</sec:ifNotGranted>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
</body>
</html>
