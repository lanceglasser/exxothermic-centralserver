<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.listCompanyLocation" /></title>
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
<script
	src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
<script
	src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
<script>
	Exxo.module = "location-list";
	Exxo.UI.vars["page-code"] = "location-list";
	Exxo.Tables.loadPaginationInitValuesFromString('<custom:paginationInfo code="location-list"/>');
</script>
</head>
<body>
	<g:render template="/layouts/internalContentTemplate">
		<h1>
			<g:message code="default.title.listCompanyLocation" />
		</h1>
		<g:render template="/layouts/messagesAndErrorsTemplate"/>
		<div class="content">
			<g:if test="${companyLocations.size()>0}">
				<table id="table-list" class="display" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>
								${message(code: 'default.field.locationName')}
							</th>
							<th>
								${message(code: 'default.field.companyName')}
							</th>
							<th>
								${message(code: 'default.field.companyIntegrator')}
							</th>
							<th>
								${message(code: 'default.field.stateOrProvince')}
							</th>
							<%--					<th class="width10 sorter ">${message(code: 'default.field.activeSkin')}</th>--%>
							<th>
								${message(code: 'default.field.enabled')}
							</th>
							<th>
								${message(code: 'default.table.action')}
							</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>
								${message(code: 'default.field.locationName')}
							</th>
							<th>
								${message(code: 'default.field.companyName')}
							</th>
							<th>
								${message(code: 'default.field.companyIntegrator')}
							</th>
							<th>
								${message(code: 'default.field.stateOrProvince')}
							</th>
							<%--					<th class="width10 sorter ">${message(code: 'default.field.activeSkin')}</th>--%>
							<th>
								${message(code: 'default.field.enabled')}
							</th>
							<th>
								${message(code: 'default.table.action')}
							</th>
						</tr>
					</tfoot>
					<tbody>
						<g:each in="${companyLocations}" status="i" var="location">
							<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
								<td><sec:ifAnyGranted roles="ROLE_INTEGRATOR, ROLE_HELP_DESK">
										<g:link action="edit" id="${location.id}">
											${location.name.encodeAsHTML()}
										</g:link>
									</sec:ifAnyGranted> 
									<sec:ifNotGranted roles="ROLE_INTEGRATOR, ROLE_HELP_DESK">
										${location.name.encodeAsHTML()}
									</sec:ifNotGranted>
								</td>
								<td>
									${location.company.name.encodeAsHTML()}
								</td>
								<td>
									${location.company?.companyWideIntegrator?.companyName?.encodeAsHTML()}
								</td>
								<td><g:if test="${location.state}">
										${g.message(code:"state.code." + location.state.name)}
									</g:if> <g:else>
										${location.stateName.encodeAsHTML()}
									</g:else></td>
								<%--						<td class="breakWord widthTdRow15">--%>
								<%--								<g:if test="${location.activeSkin}">--%>
								<%--									${location.activeSkin.encodeAsHTML()}--%>
								<%--								</g:if>--%>
								<%--								<g:else>--%>
								<%--									<g:message code="default.field.empty" />--%>
								<%--								</g:else>	--%>
								<%--						</td>--%>
								<td><g:if test="${location.enable == true}">
										<g:message code="default.field.yes" />
									</g:if> <g:else>
										<g:message code="default.field.no" />
									</g:else>
								<td><g:link action="show" id="${location.id}"
										class="action-icons view"
										" title="${message(code:'default.action.show')}"></g:link> 
									
									<sec:ifAnyGranted roles="ROLE_INTEGRATOR, ROLE_HELP_DESK">
										<g:link action="edit" id="${location.id}"
											class="action-icons edit"
											title="${message(code:'default.action.edit')}"></g:link>
										<g:link controller="box" action="register"
											params="${['location':location.id]}"
											class="action-icons addBox"
											title="${message(code:'default.box.register')}"></g:link>
										<g:link controller="exxtractorsUser" action="create" id="${location.id}"
                                            class="action-icons users-add"
                                            title="${message(code:'create.exxtractor.user')}"></g:link>	
									</sec:ifAnyGranted>
									
									<g:if test="${location.boxes?.size() > 0}">
										<g:link controller="box" action="index"
											class="action-icons boxInfo" id="${location.id}"
											title="${ message(code:'default.box.box') + "(" + location.boxes?.size() + ")" }"></g:link>
									</g:if>
								</td>
							</tr>
						</g:each>
					</tbody>
				</table>
			</g:if>
		</div>
	</g:render>
</body>
</html>