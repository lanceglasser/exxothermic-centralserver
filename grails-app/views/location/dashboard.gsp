<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main" />
		
		<link rel="stylesheet" type="text/css"
			href="${resource(dir: 'themes/default/css', file: 'widgets.css')}">  
		
		<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=AIzaSyB_3H-gp22pdmUYCfpFZG04crqtTDE8nwk"></script> 
    	<script src="${resource(dir: 'themes/default/js', file: 'google_maps.js')}"></script>
    	<script src="${resource(dir: 'themes/default/js', file: 'application.js')}"></script>
    	
		<script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1','packages':['corechart']}]}"></script>
		
		<script src="${resource(dir: 'themes/default/js/widget', file: 'jquery.exxo.widget.js')}"></script>
		
		<!-- Gridster Start -->
		<script src="${resource(dir: 'themes/default/js', file: 'jquery.draggable.js')}"></script>
		<script src="${resource(dir: 'themes/default/js', file: 'jquery.coords.js')}"></script>
		<script src="${resource(dir: 'themes/default/js/widget', file: 'jquery.collision.js')}"></script>
		<script src="${resource(dir: 'themes/default/js/widget', file: 'utils.js')}"></script>
		<script src="${resource(dir: 'themes/default/js/widget', file: 'jquery.gridster.js')}"></script>
		<script src="${resource(dir: 'themes/default/js/widget', file: 'jquery.gridster.extras.js')}"></script>
		
		<link rel="stylesheet" type="text/css"
			href="${resource(dir: 'themes/default/css', file: 'jquery.gridster.css')}">
		<!-- Gridster End -->
		
		<link rel="stylesheet" type="text/css"
			href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
		<link rel="stylesheet" type="text/css"
			href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
		<script
			src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
		<script
			src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
		<script
			src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_user_dashboard.js')}"></script>
		<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_app_demo.js')}"></script>
		
		<g:render template="/widget/dashboardGeneralConfig"></g:render>
		
		<script>
			<sec:ifAnyGranted roles="ROLE_ADMIN">
			Exxo.UI.vars["can-edit-location"] = false;
			</sec:ifAnyGranted>
			<sec:ifNotGranted roles="ROLE_ADMIN">
			Exxo.UI.vars["can-edit-location"] = true;
			</sec:ifNotGranted>
			Exxo.module = "index";
			Exxo.UI.vars["dashboard-name"] = "locationDashboard";
			Exxo.UI.vars["location-id"] = ${ location.id };
			Exxo.UI.translates['dashboard-titles-ref'] = "${ message(code:'location.dashboard.titles.ref')}";
		</script>
		
	</head>
	<body>
		<section class="introp">
	  		<div class="intro-body">
	    		<div class="container full-width">
	      			<div class="row">
						<div class="col-md-12" id="internal-content">
						  	
						  	<div class="col-md-12 dashboard-header-line">						  			
						  		<div id="crumbs">
										<ul class="left">
											<li>
												<g:link controller="home" action="index" >
													<img class="title-icon" src="${resource(dir: 'themes/default/img/icons', file: 'home-white.png')}" alt="Home"/>
												</g:link>
											</li>
											<li>
												<g:link controller="company" action="dashboard" id="${location.company.id}">
													${ message(code:'dashboard.title', args:[message(code:'default.field.companyName')]) }
												</g:link>
											</li>											
											<li>
												<a href="" class="current">${ message(code:'dashboard.title', args:[message(code:'default.field.location')]) }</a>
											</li>											
										</ul>
										<span class="left add-widget circle" title="${ message(code:'add.widget') }"></span>
										<span id="how-use-widgets-btn" class="left help-widget circle"  title="${ message(code:'how.use.widgets') }"></span>
								</div>
						  		
						  		<span class="clearfix"></span>
						  	</div>
						  	<span class="clearfix"></span>
						  	
						  	<div style="width: 1410px; position: relative;">
						  	<!-- Renders the Main Dashboard for Current User -->
							<user:locationDashboard />
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</body>
</html>
