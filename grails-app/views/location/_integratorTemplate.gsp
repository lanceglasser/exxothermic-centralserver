<div class="col-xs-4 text-to-right">
	<label for="commercialIntegrator">
		<div class="questionToolTip" title="${message(code:'tooltip.select.systemIntegrator')}"></div>	
		<g:message code="default.field.commercialntegrator" />
	</label>
</div>					
<div class="col-xs-8 text-to-left">
	<g:select name="commercialIntegrator.id" id="commercialIntegrator" from="${integrators}" optionKey="id" optionValue="companyName"  
				value="${location.commercialIntegrator?.id}" noSelection="['':'']"  placeholder="Select an Integrator" />
	<span class="liten-up"> <g:message code="default.note.location.integrator" /></span>
</div>				
<span class="clearfix"></span>	

<script type="text/javascript">
$("#commercialIntegrator").select2({	      
	allowClear: true
});

$('.questionToolTip').tooltipster({
	position: 'right',
	maxWidth: 210 
});     
</script>
