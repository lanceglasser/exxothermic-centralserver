<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.updateSoftwareVersion" /></title>	
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_form_show.js')}"></script>
<script>
	Exxo.module = "software-show";
</script>
</head>
<body>
	<g:render template="/layouts/internalContentTemplate">
		<h1>
			${boxSoftware.name.encodeAsHTML()}
		</h1>
		<g:render template="/layouts/messagesAndErrorsTemplate"
			model="[entity: user]" />
		<div class="content form-info">
			<!-- Left Column -->
			<div class="col-xs-6 resize-formleft">
				<custom:editField name="name" value="${boxSoftware.name ? boxSoftware.name.encodeAsHTML() : ''}"
					label="default.field.name" />
				<custom:editField name="versionSoftware" value="${boxSoftware.versionSoftware ? boxSoftware.versionSoftware.encodeAsHTML() : ''}"
					label="default.field.version" />
				<custom:editField name="versionLabel" value="${boxSoftware.versionSoftware ? boxSoftware.versionSoftware.encodeAsHTML() : ''}"
					label="default.field.versionLabel" />
				<custom:editField name="description" value="${boxSoftware.description ? boxSoftware.description.encodeAsHTML() : ''}"
					label="default.field.description" />
			</div>
			<!-- Right Column -->
			<div class="col-xs-6 resize-formright">
				<custom:editField name="file" value="${boxSoftware.fileNameOrigin ? boxSoftware.fileNameOrigin.encodeAsHTML() : ''}"
					label="default.field.file" />
				<custom:editField name="checksum" value="${boxSoftware.checksum ? boxSoftware.checksum.encodeAsHTML() : ''}"
					label="default.field.checksum" />
				<custom:editField name="enabled"
					value="${(boxSoftware.enable == true) ? message(code:"default.field.yes") : message(code:"default.field.no")}"
					label="default.field.enabled"/>
				<custom:editField name="architectures" value="${boxSoftware.getArchitecturesList()}"
                    label="apply.to" />
			</div>
			<span class="clearfix"></span>

			<!-- Buttons -->			
			<g:link action="listAll" controller="boxSoftware" id="${boxSoftware.id}">
				<input type="button" class="form-btn btn-primary"
					value="${message(code:'default.field.buttonBack')}" />
			</g:link>
		</div>
	</g:render>
</body>
</html>