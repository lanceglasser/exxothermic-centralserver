<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.edit.updateSoftwareVersion" /></title>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_utils.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_constants.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_box_register_software.js')}"></script>
<script>
	Exxo.module = "box-register";
	Exxo.UI.translates[Exxo.Constants.ERROR_FILE_INVALID] = "${ message(code:'invalid.software.version.message', args:['7GB']) }";
	Exxo.UI.vars['filename'] = "${boxSoftware?.fileName}";
	Exxo.UI.translates['architectures.min.error'] = "${ message(code:'architectures.min.error')}";
</script>
</head>
<body>
<g:render template="/layouts/internalContentTemplate">
	<h1>
		<g:message code="default.title.edit.updateSoftwareVersion" />
	</h1>
	
	<div class="content">
		<g:render template="/layouts/messagesAndErrorsTemplate" model="[entity: boxSoftware]"/>

		<g:uploadForm action="saveEdit" name="editSoftwareUpdateForm" class="exxo-form">
		
 			<g:hiddenField name="id" value="${boxSoftware?.id}" />
			<g:hiddenField name="version" value="${boxSoftware?.version}" />
			<g:hiddenField name="checksum" value="${boxSoftware?.checksum}" />
			<g:hiddenField name="offset" value="${offset}" />
			
			<g:render template="registerTemplate"></g:render>
			
		</g:uploadForm>
	</div>
</g:render>
</body>
</html>