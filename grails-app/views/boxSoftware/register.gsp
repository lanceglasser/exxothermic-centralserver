<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.box.registerSoftwareVersion" /></title>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_utils.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_constants.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_validation.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_box_register_software.js')}"></script>
<script>
	Exxo.module = "box-register";
	Exxo.UI.translates[Exxo.Constants.ERROR_FILE_INVALID] = "${ message(code:'invalid.software.version.message', args:['7GB']) }";
	Exxo.UI.vars['filename'] = "${boxSoftware?.fileName}";
	Exxo.UI.translates['architectures.min.error'] = "${ message(code:'architectures.min.error')}";
	Exxo.UI.translates['invalid.version.label'] = "${ message(code:'invalid.version.label')}";
</script>
</head>
<body>
<g:render template="/layouts/internalContentTemplate">
	<h1>
		<g:message code="default.box.registerSoftwareVersion" />
	</h1>
	
	<div class="content">
		<g:render template="/layouts/messagesAndErrorsTemplate" model="[entity: boxSoftware]"/>

		<g:uploadForm action="uploadSoftwareUpdate" name="uploadSoftwareUpdateForm" class="exxo-form">
 
			<g:render template="registerTemplate"></g:render>
			
		</g:uploadForm>
	</div>
</g:render>
</body>
</html>