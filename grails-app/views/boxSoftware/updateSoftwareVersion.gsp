<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.softwareUpdate" /></title>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_form_show.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_box_software_update.js')}"></script>
<script>
	Exxo.module = "box-software-update";
	$(document).ready(function() {
	   $("#fields-container").createShowForm({
			title: '<g:message code="default.title.softwareUpdate" />',
			columns: [
				[
					{label: '<g:message code="default.box.information" />', value: ''},
					{useMainText: '${box.serial != null}', label: '<g:message code="default.field.serial" />', value: '${box.serial?.encodeAsHTML()}'},
					{useMainText: '${box.name != null}', label: '<g:message code="default.field.name" />', value: '${box.name?.encodeAsHTML()}'},

					{label: '<g:message code="default.old.version.information" />', value: ''},
					{useMainText: '${box.softwareVersion != null}', label: '<g:message code="default.field.version" />', value: '${box.softwareVersion?.versionSoftware?.encodeAsHTML()}'},
					{useMainText: '${box.softwareVersion != null}', label: '<g:message code="default.field.versionLabel" />', value: '${box.softwareVersion?.versionSoftwareLabel?.encodeAsHTML()}'},
					{useMainText: '${box.softwareVersion != null}', label: '<g:message code="default.field.name" />', value: '${box.softwareVersion?.name?.encodeAsHTML()}'}
					
				],
				[
					{label: '<g:message code="new.version.information" />', value: ''},
					{useMainText: '${boxSoftware.versionSoftware != null}', label: '<g:message code="default.field.versionNew" />', value: '${boxSoftware?.versionSoftware?.encodeAsHTML()}'},
					{useMainText: '${boxSoftware.versionSoftwareLabel != null}', label: '<g:message code="default.field.versionLabelNew" />', value: '${boxSoftware?.versionSoftwareLabel?.encodeAsHTML()}'},
					{useMainText: '${boxSoftware.name != null}', label: '<g:message code="default.field.nameNew" />', value: '${boxSoftware?.name?.encodeAsHTML()}'}
				]
			],
			texts: {
				yes: 	'<g:message code="default.field.yes" />',
				no: 	'<g:message code="default.field.no" />',
				empty: 	'<g:message code="default.field.empty" />'
			},
			buttons: [
				'<g:submitButton class="form-btn btn-primary"	name="save" id="save" value="${message(code:'default.field.buttonApply')}" />',
				'<g:link action="listAll" controller="box" class="back-button"><span class="form-btn btn-primary">${message(code:'default.field.buttonCancel')}</span></g:link>'
			]
		   });
	});
</script>
</head>
<body>
	<section class="introp">
		<div class="intro-body">
			<div class="container full-width">
				<div class="row">
					<div class="col-md-12" id="internal-content">
						<g:if test="${flash.error}">
			        		<div class="alert alert-error">  
							  <a class="close" data-dismiss="alert">×</a>  
							  ${flash.error} 
							  <g:hasErrors bean="${box}">
							  		<ul>
										<g:eachError bean="${box}" var="error">
											<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
										</g:eachError>
									</ul>
							  </g:hasErrors>	
							    ${flash.error=null}
							</div> 
						</g:if>          
			      		 <g:if test="${flash.success}">
			        		<div class="alert alert-success">  
							  <a class="close" data-dismiss="alert">×</a>  
							  ${flash.success} 
							  ${flash.success=null} 
							</div>
			      		</g:if>
						<g:form action="applyUpdateSoftwareVersion" name="applyUpdateSoftwareVersionForm"  class="exxo-form">
							<g:hiddenField name="boxId" value="${box.id}" />
							<g:hiddenField name="boxSoftwareId" value="${boxSoftware.id}" />
							
							<div id="fields-container">
							
							</div>
							
							<div class="content">
								<div class="prop" style="visibility:hidden" name="divImageUpdateProgress" id="divImageUpdateProgress">
									<div class="name"><g:message code="default.box.software.updating"/></div>
									<div class="value imageUpdateProgress"></div>
								</div>
					        </div>
						</g:form>
					</div>
				</div>
			</div>
		</div>
	</section>
</body>
</html>