<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title><g:message code="default.title.softwareUpdate" /></title>
		<script src="${resource(dir: 'js/pluggins/tablesorter', file: 'jquery.tablesorter.min.js')}"></script>
		<script src="${resource(dir: 'js/pages/common', file: 'tables.sorter.js')}"></script>
		
	</head>
	<body>
	 
			<h2><g:message code="default.title.updateSoftwareVersion" /></h2>
	 
			<g:if test="${flash.error}">
	        		<div class="alert alert-error">  
					  <a class="close" data-dismiss="alert">×</a>  
					  ${flash.error} 
					</div> 
			</g:if>
			 <g:if test="${flash.success}">
	        		<div class="alert alert-success">  
					  <a class="close" data-dismiss="alert">×</a>  
					  ${flash.success} 
					  ${flash.success=null} 
					</div> 
      		</g:if>      		
	</body>
</html>