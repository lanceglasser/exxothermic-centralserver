

<!-- Left Column -->
<div class="col-xs-6 resize-formleft">
	<!-- Name  -->				
	<custom:editField type="text" name="name" required="${true}" value="${boxSoftware.name}" 
		label="default.field.name" max="50"/>
	
	<!-- Version  -->				
	<custom:editField type="text" name="versionSoftware" required="${true}" value="${boxSoftware.versionSoftware}" 
		label="default.field.version" max="10" note="${ message(code:'default.field.example') } 1"/>
	
	<!-- Version Label -->				
	<custom:editField type="text" name="versionSoftwareLabel" required="${true}" value="${boxSoftware.versionSoftwareLabel}"
		label="default.field.versionLabel" max="20" note="${ message(code:'default.field.example') } ${message(code: 'default.box.software.versionExample')}" />
	
	<!-- Image Client Association -->
	<div class="col-xs-4 text-to-right">		
		<label class="control-label" for="type"><g:message code="default.field.afiliate" /> *</label>
	</div>					
	<div class="col-xs-8 text-to-left">
		<g:select id="affiliate" name='affiliate.id' value="${boxSoftware?.affiliate?.id}"
		    noSelection="${['null':message(code:'default.field.all')]}"
		    from='${mycentralserver.app.Affiliate.list()}'
		    optionKey="id" optionValue="name">
		</g:select>
	
	</div>	
	<span class="clearfix"></span>	
	
	<!-- Image Type -->            
    <div class="col-xs-4 text-to-right">        
        <label class="control-label" for="type"><g:message code="apply.to" /> *</label>
    </div>                  
    <div class="col-xs-8 text-to-left">
        <div class="well-architectures">
            <input id="associatedArchitectures" name="associatedArchitectures" type="hidden" value="">
            <ul class="list-group checked-list-box">
              <g:each in="${mycentralserver.utils.enumeration.Architecture.values()}" status="i" var="arch">
	              <li class="list-group-item architecture" 
	                ${boxSoftware.isArchAssociated(arch.getValue())? 'data-checked="true"':''} 
	                value="${arch.getValue()}">
	                ${arch}</li>
              </g:each>
            </ul>
        </div>
        <span class="liten-up">${ message(code:'software.version.apply.to.msg') }</span>
    </div>  
    <span class="clearfix"></span>
</div>

<!-- Right Column -->
<div class="col-xs-6 resize-formright">
	<!-- Description  -->				
	<custom:editField type="textarea" name="description" required="${true}" value="${boxSoftware.description}" 
		label="default.field.description" max="255" cssclass="input-textarea"/>
	
	<!-- File Selector -->
	<g:render template="/layouts/tags/fileUploadSelectorTemplate" 
		model="[
			id:				'file',
			label:			message(code:'default.field.file')
			]"
	/>
		
	<!-- Supported:Only for Edit Page -->
	<g:if test="${params.action=='edit' || params.action=='saveEdit'}">
		<custom:editField type="checkbox" name="isSupported" required="${false}" value="${boxSoftware.isSupported}" 
			label="default.field.supported" cssclass="input-checkbox" tooltip="${message(code:'tooltip.boxSoftware.isSupported')}"/>
	</g:if>
			
	<!-- Enabled -->
	<custom:editField type="checkbox" name="enable" required="${false}" value="${boxSoftware.enable}" 
		label="default.field.enabled" cssclass="input-checkbox" tooltip="${message(code:'tooltip.boxSoftware.enable')}"/>
	
</div>
<span class="clearfix"></span>

<!-- Buttons -->
<g:submitButton class="form-btn btn-primary" name="register" value="${message(code:'default.field.buttonSave')}" />
											
<g:link action="listAll" controller="boxSoftware" class="back-button">        
   <span class="form-btn btn-primary">
      ${message(code:'default.field.buttonCancel')}
   </span>
</g:link>
