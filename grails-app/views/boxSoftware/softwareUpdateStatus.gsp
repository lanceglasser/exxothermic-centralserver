<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.softwareUpdate" /></title>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_form_show.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_box_software_update.js')}"></script>
<script>
    Exxo.module = "box-software-update";
    var boxUpdatedMessage = "${message(code:'default.box.software.updated')}";
    var doneMessage = "${message(code:'default.field.updateDone')}";
    var baseURL = "${createLink(uri: '', absolute : 'true')}";
    var pageUpdated = ${controlDetail?.status == 1? 'false':'true'};
    Exxo.UI.translates["status.msg.2"] = "${message(code:'box.software.upgrade.control.status.2')}";
    Exxo.UI.translates["status.msg.3"] = "${message(code:'box.software.upgrade.control.status.3')}";
</script>
</head>
<body>
    <section class="introp">
        <div class="intro-body">
            <div class="container full-width">
                <div class="row">
                    <div class="col-md-12" id="internal-content">

                        <h1>
                            <g:message code="default.title.box.software.update" />
                        </h1>

                        <g:if test="${controlDetail}">
                            <g:if test="${ controlDetail.status == 1 }">
                                <h2>
                                    <div id="messageUpdate">
                                        <g:message code="default.box.software.updating" />
                                    </div>
                                </h2>
                            </g:if>
                            <table class='table table-striped'>
                                <thead>
                                    <tr>
                                        <th>
                                            ${message(code:'default.field.serial')}
                                        </th>
                                        <th>
                                            ${message(code:'updating.to.version')}
                                        </th>
                                        <th>
                                            ${message(code:'started.at')}
                                        </th>
                                        <th>
                                            ${message(code:'default.field.status')}
                                        </th>
                                        <th class="hideOnProgress"
                                            ${ controlDetail.status == 1? 'style="display: none;"':'' }>
                                            ${message(code:'finished.at')}
                                        </th>
                                        <th class="hideOnProgress"
                                            ${ controlDetail.status == 1? 'style="display: none;"':'' }>
                                            ${message(code:'default.field.errorDescription')}
                                        </th>
                                    </tr>
                                </thead>
                                <tbody style="">
                                    <tr id="serial" class="rowsText">
                                        <g:field type="hidden" id="serialNumber" name="serialNumber" value="${serial}" />
                                        <td class="widthTdRow15"><g:link controller="box" action="listAll"
                                                params="[serialToFilter: serial]">
                                                ${serial}
                                            </g:link></td>
                                        <td class="widthTdRow15">
                                            ${controlDetail.versionLabel}
                                        </td>
                                        <td class="widthTdRow15">
                                            ${formatDate(format:'MM-dd-yyyy HH:mm:ss', date:controlDetail.startedAt)}
                                        </td>
                                        <td class="widthTdRow15"><span id="status_${serial}"> ${message(code:'box.software.upgrade.control.status.'+controlDetail.status)}
                                        </span></td>
                                        <td id="finishedAt" class="widthTdRow15 hideOnProgress"
                                            ${controlDetail.status == 1? ' style="display: none;"':'' }>
                                            ${controlDetail.status != 1? formatDate(format:'MM-dd-yyyy HH:mm:ss', date:controlDetail.finishedAt):''}
                                        </td>
                                        <td id="description" class="widthTdRow40 hideOnProgress"
                                            ${ controlDetail.status == 1? ' style="display: none;"':'' }><g:if
                                                test="${ controlDetail.status == 3 }">
                                                ${message(code:messageErrorDescriptionHash?.get(controlDetail.errorCode))}
                                            </g:if> <g:else>
                                                ${controlDetail.msg}
                                            </g:else></td>
                                    </tr>
                                </tbody>
                            </table>
                        </g:if>
                        <g:else>
                            <div class="col-md-8 col-md-offset-2" style="margin-top: 25px;">
                                <div id="child-salute">
                                    <div class="message">
                                        <p>
                                            ${ message(code:'box.software.upgrade.control.no.record', args:[serial]) }
                                        </p>
                                        <p>
                                            <g:message code="box.software.upgrade.control.no.record.msg.1" />
                                        </p>
                                        <g:link controller="box" action="listAll">
                                            <g:message code="default.box.list" />
                                        </g:link>
                                    </div>
                                </div>
                            </div>
                        </g:else>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
</html>