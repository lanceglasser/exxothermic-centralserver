<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.softwareUpdate" /></title>
<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
<script src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
<script>
	Exxo.module = "page-list";
	Exxo.UI.vars["page-code"] = "software-list";
	Exxo.Tables.loadPaginationInitValuesFromString('<custom:paginationInfo code="software-list"/>');
</script>
</head>
<body>
<section class="introp">
  <div class="intro-body">
    <div class="container full-width">
      <div class="row">
        <div class="col-md-12" id="internal-content">
          	<h1><g:message code="default.title.updateSoftwareVersion" /></h1>
			<g:render template="/layouts/messagesAndErrorsTemplate" />
		<div class="content">
			<g:if test="${listSoftwareVersion}">
				<table id="table-list" class="display" cellspacing="0" width="100%">
	              <thead>
	                <tr>
						<th>${message(code: 'default.field.name')}</th>
						<th>${message(code: 'default.field.type')}</th>
						<th>${message(code: 'default.field.affiliate')}</th>
						<th>${message(code: 'default.field.version')}</th>
						<th>${message(code: 'default.field.versionLabel')}</th>
						<th>${message(code: 'default.field.description')}</th>
						<th>${message(code: 'default.field.file')}</th>
						<th>${message(code: 'default.field.supported')}</th>
						<th>${message(code: 'default.table.action')}</th>
	                </tr>
	              </thead>
	              <tbody>
	              	<g:each in="${listSoftwareVersion}" status="i" var="boxSoftware">
						<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
							<td>
								<sec:ifAnyGranted roles="ROLE_ADMIN">
									<g:if test="${boxSoftware.isSupported == true}">								
										<g:link action="edit" id="${boxSoftware.id}">${boxSoftware.name.encodeAsHTML()}</g:link>
									</g:if>
									<g:else>
										${boxSoftware.name.encodeAsHTML()}
									</g:else>	
								</sec:ifAnyGranted>
								<sec:ifNotGranted  roles="ROLE_ADMIN">
									${boxSoftware.name.encodeAsHTML()}
								</sec:ifNotGranted>
							</td>
							<td>${boxSoftware.getArchitecturesList() }</td>
							<td>${boxSoftware.affiliate? boxSoftware.affiliate?.name?.encodeAsHTML():message(code:'default.field.all')}</td>								
							<td>${boxSoftware.versionSoftware.encodeAsHTML()}</td>
							<td>${boxSoftware.versionSoftwareLabel.encodeAsHTML()}</td>	
							<td>${(boxSoftware.description != null && boxSoftware.description.size() < 50)?boxSoftware.description.encodeAsHTML():( (boxSoftware.description.substring(0, 50)+ "...").encodeAsHTML())}</td>							
							<td>${boxSoftware.fileNameOrigin}</td>	
							 
							
							<td>
							  <g:if test="${boxSoftware.isSupported  == true}">
								 <g:message code="default.field.yes" />
							  </g:if> 
							  <g:else>
								<g:message code="default.field.no" />
							  </g:else>
						   </td>
					
							<td>									
								<g:link action="show" id="${boxSoftware.id}" class="action-icons view" title="${message(code:'default.action.show')}"></g:link>	
								<sec:ifAnyGranted roles="ROLE_ADMIN">
									<g:if test="${boxSoftware.isSupported == true}">
										<g:link action="edit" id="${boxSoftware.id}" class="action-icons edit" title="${message(code:'default.action.edit')}"></g:link>
									</g:if>
								</sec:ifAnyGranted>  								 
							</td>
						</tr>
					</g:each>
	              </tbody>
	            </table>
			</g:if>            
        </div>
      </div>
    </div>
  </div>
</section>
<div id="confirm-reset-dialog" title="${message(code:'mycentralserver.user.User.resetPassword.title')}"  style="display:none;">
  ${message(code:'mycentralserver.user.User.resetPassword')}
</div>
</body>
</html>