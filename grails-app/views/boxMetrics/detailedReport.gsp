<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="usage.report.title" /></title>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
<script
	src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
<script
	src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
<script
	src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_metrics_report.js')}"></script>
	
<script>
	Exxo.module = "box-metrics-report";
	Exxo.UI.vars["page-code"] = "box-metrics-result";
	Exxo.Tables.loadPaginationInitValuesFromString('<custom:paginationInfo code="box-metrics-result"/>');
	Exxo.UI.urls["get-data-url"] = "${g.createLink(controller: 'boxMetrics', action: 'getDataAjax', absolute: true)}";
	Exxo.UI.urls["get-chart-data-url"] = "${g.createLink(controller: 'boxMetrics', action: 'getChartsDataAjax', absolute: true)}";
	Exxo.UI.urls["get-hour-clients-data-url"] = "${g.createLink(controller: 'boxMetrics', action: 'getHourClientsDataAjax', absolute: true)}";
	Exxo.UI.urls["get-daily-clients-data-url"] = "${g.createLink(controller: 'boxMetrics', action: 'getDailyClientsDataAjax', absolute: true)}";
	Exxo.UI.urls["get-hour-clients-chart-data-url"] = "${g.createLink(controller: 'boxMetrics', action: 'getHourClientsChartsDataAjax', absolute: true)}";
	Exxo.UI.urls["gen-file-url"] = "${g.createLink(controller: 'boxMetrics', action: 'generateMetricsReportFile', absolute: true)}";

	google.load("visualization", "1", {packages:["corechart"]});
	Exxo.UI.translates["chart-time-title"] = "${ message(code:'consume.time.per.hour')}";
	Exxo.UI.translates["chart-max-title"] = "${ message(code:'max.sessions.per.hour')}";
	Exxo.UI.translates["chart-total-title"] = "${ message(code:'total.sessions.per.hour')}";
	Exxo.UI.translates["chart-hourly-title"] = "${ message(code:'client.sessions.per.hour')}";
	Exxo.UI.translates["chart-daily-title"] = "${ message(code:'client.sessions.per.day')}";
	Exxo.UI.translates["total-day"] = "${ message(code:'total.day')}";	
	Exxo.UI.translates["hours"] = "${ message(code:'schedule.hours') }";
	Exxo.UI.translates["days"] = "${ message(code:'client.sessions.per.day.info') }";
</script>
</head>
<body>
	<section class="introp">
		<div class="intro-body">
			<div class="container full-width">
				<div class="row">
					<div class="col-md-12" id="internal-content">
						<h1>
							<g:message code="usage.report.title" />
						</h1>
						<g:render template="/layouts/messagesAndErrorsTemplate" />
						<div class="content">
							<g:form controller="boxMetrics" action="getData" name="getDataForm" class="exxo-form">
							<div class="col-md-12 form-bordered" style="padding-bottom: 25px;">
								
								<!-- Start Date  -->
								<div class="col-xs-6 resize-formleft">
									<custom:editField type="date" name="startDate" required="${true}" value="${ formatDate(format:'MM/dd/yyyy', date:new java.util.Date()) }" 
										minusDays="-7" label="start.date" leftColumnPosition="left" note="${ message(code:'schedule.date.format') }"/>
								</div>
								
								<!-- End Date  -->
								<div class="col-xs-6 resize-formright">
									<custom:editField type="date" name="endDate" required="${true}" value="${ formatDate(format:'MM/dd/yyyy', date:new java.util.Date()) }" 
										label="end.date" leftColumnPosition="left"/>
								</div>
								<span class='clearfix'></span>
								
								<!-- Location -->
								<div class="col-xs-6 resize-formleft">
									<div class="col-xs-4 text-to-left">
										<label for="location">
											${ message(code:'default.field.location') } *
										</label>
									</div>
									<div class="col-xs-8 text-to-left">
										<select id="location" name="location">
											<option value="0" selected="selected">${ message(code:'default.field.all') }</option>
											<g:each in="${locations}" status="i" var="location">
											<option value="${ location.id }" ${ location.id == locationId? 'selected="selected"':'' }>${ location.encodeAsHTML() }</option>
											</g:each>
										</select>
									</div>
									<span class='clearfix'></span>
								</div>		
								
							</div>
							<!-- Buttons -->
									<g:submitButton class="form-btn btn-primary" name="save" value="${message(code:'default.table.sSearch')}" />
									<a href="" id="genFileButton">        
								       <span class="form-btn btn-primary"> ${message(code:'generate.file')} </span>       
							    	</a>
							</g:form>	
								<span class='clearfix'></span>
							<span class='clearfix'></span>
							<div id="result-data-container" style="display: none;">							
								<br/>
							<!-- Start of Tabs -->
							<ul id="tabs">
							    <li><a id="mainTab" href="#tab1">${ message(code:'usage.report.tab') }</a></li>
							    <li><a href="#tab2">${ message(code:'clients.report.tab') }</a></li>
							</ul>
							<div id="tab1" class="tab-section usageReportTabs">	
								<div id="chartsContainerCombo1">
								
									<span class="liten-up stringLabel">${ message(code:'data.usage.report.charts.note') }</span>
									<span class="clearfix"></span>
									
									<!-- Charts Date  -->
									<div class="col-xs-6 resize-formleft">
										<span id="startDateDiv">
							            <div class="col-xs-4 text-to-left">
							              <label for="startDate" class="stringLabel"> ${ message(code:'show.charts.of') }: </label>
							            </div>
							            <div class="col-xs-8 text-to-left"><select id="chartsDays"></select></div>
										</span>
										<span class="clearfix"></span>
									</div>
									<span class="clearfix"></span>
									
									<br/>
									<div id="chartsContainer">
										<div class="streaminChart col-xs-4 resize-formleft">
											<div id="chart_div"></div>
										</div>
										<div class="streaminChart col-xs-4 resize-formleft">
											<div id="chart_sessions_total_div"></div>
										</div>
										<div class="streaminChart col-xs-4 resize-formleft">
											<div id="chart_sessions_max_div"></div>
										</div>
									</div>
									
									<span class='clearfix'></span>
								</div> <!-- Charts Container End -->
								<div id="chartsNoData" style="display: none;">
									<div class="alert alert-warning">
										${ message(code:'no.data.for.day') }
									</div>
								</div>
								<span class='clearfix'></span>
								
								<br/>
								<div id="tableContainer" style="display: none;">
									<p class="muted"> ${ message(code:'clients.report.filter.note') } </p>
									
									<table id="table-result" class="display" cellspacing="0" width="100%">
								        <thead>
								            <tr>
								                <th>${ message(code:'default.field.location') }</th>
								                <th>${ message(code:'exxtractor.name', args:[message(code:'exxtractor')]) }</th>
								                <th>${ message(code:'exxtractor.serial', args:[message(code:'exxtractor')]) }</th>
								                <th>${ message(code:'date') }</th>
								                <th>${ message(code:'port') }</th>
								                <th>${ message(code:'channel.name') }</th>						                
								                <th>${ message(code:'hour') }</th>
								                <th>${ message(code:'time.in.seconds') }</th>
								                <th>${ message(code:'sessions.max') }</th>
								                <th>${ message(code:'sessions.total') }</th>
								            </tr>
								        </thead>
								 
								        <tfoot>
								            <tr>
								                <th>${ message(code:'default.field.location') }</th>
								                <th>${ message(code:'exxtractor.name', args:[message(code:'exxtractor')]) }</th>
								                <th>${ message(code:'exxtractor.serial', args:[message(code:'exxtractor')]) }</th>
								                <th>${ message(code:'date') }</th>
								                <th>${ message(code:'port') }</th>
								                <th>${ message(code:'channel.name') }</th>						                
								                <th>${ message(code:'hour') }</th>
								                <th>${ message(code:'time.in.seconds') }</th>
								                <th>${ message(code:'sessions.max') }</th>
								                <th>${ message(code:'sessions.total') }</th>
								            </tr>
								        </tfoot>
								    </table>
								</div> <!-- Table Container End -->
							</div> <!-- end of streamReport tab-->
							<div id="tab2" class="tab-section usageReportTabs">	
								<div id="chartsContainerCombo" style="/*display: none;*/">
								
									<span class="liten-up stringLabel">${ message(code:'data.clients.report.charts.note') }</span>
									<span class="clearfix"></span>
									
									<!-- Charts Date  -->
									<div class="col-xs-6 resize-formleft">
										<span id="startDateDiv">
							            <div class="col-xs-4 text-to-left">
							              <label for="startDate" class="stringLabel"> ${ message(code:'show.charts.of') }: </label>
							            </div>
							            <div class="col-xs-8 text-to-left"><select id="chartsDays2"></select></div>
										</span>
										<span class="clearfix"></span>
									</div>
									<span class="clearfix"></span>
									
									<br/>
									<div id="chartsContainer2">
										<div id="chartsHour" class="clientChart col-lg-6">
											<div id="chartsNoDataHour" style="display: none;">
												<div class="alert alert-warning">
													${ message(code:'no.data.for.day') }
												</div>
											</div>
											<div id="chart_hour_clients"></div>
										</div>
										<div class="clientChart col-lg-6">
											<div id="chartsNoDataDaily" style="display: none;">
												<div class="alert alert-warning">
													${ message(code:'no.data.for.day') }
												</div>
											</div>
											<div id="chart_daily_clients"></div>
										</div>
									</div>
									
									<span class='clearfix'></span>
								</div> <!-- Charts Container End -->
								
								<span class='clearfix'></span>
								
								<br/>
								<div id="tableContainer2" style="display: none;">
									<p class="muted"> ${ message(code:'clients.report.filter.note') } </p>
									<br/>
									
									<div class="clients-table-container col-lg-6">
									<span class="liten-up stringLabel"  style="
									    font-size: 18px !important;
									    margin-bottom: 5px;
									">${ message(code:'client.per.hour.table.title') }</span>
										<table id="table-result-hour" class="display" cellspacing="0" width="100%">
									        <thead>
									            <tr>
									                <th>${ message(code:'default.field.location') }</th>
									                <th>${ message(code:'exxtractor.name', args:[message(code:'exxtractor')]) }</th>
									                <th>${ message(code:'exxtractor.serial', args:[message(code:'exxtractor')]) }</th>
									                <th>${ message(code:'date') }</th>	
									                <th>${ message(code:'hour') }</th>				
									                <th>${ message(code:'clients.total') }</th>
									            </tr>
									        </thead>
									 
									        <tfoot>
									            <tr>
									                <th>${ message(code:'default.field.location') }</th>
									                <th>${ message(code:'exxtractor.name', args:[message(code:'exxtractor')]) }</th>
									                <th>${ message(code:'exxtractor.serial', args:[message(code:'exxtractor')]) }</th>
									                <th>${ message(code:'date') }</th>	
									                <th>${ message(code:'hour') }</th>					
									                <th>${ message(code:'clients.total') }</th>
									            </tr>
									        </tfoot>
									    </table>
									 </div>
									 <div class="clients-table-container col-lg-6">									 
									 <span class="liten-up stringLabel"  style="
									    font-size: 18px !important;
									    margin-bottom: 5px;
									">${ message(code:'client.per.day.table.title') }</span>
									    <table id="table-result-daily" class="display" cellspacing="0" width="100%">
									        <thead>
									            <tr>
									                <th>${ message(code:'default.field.location') }</th>
									                <th>${ message(code:'exxtractor.name', args:[message(code:'exxtractor')]) }</th>
									                <th>${ message(code:'exxtractor.serial', args:[message(code:'exxtractor')]) }</th>
									                <th>${ message(code:'date') }</th>
									                <th>${ message(code:'clients.total') }</th>
									            </tr>
									        </thead>									 
									        <tfoot>
									            <tr>
									                <th>${ message(code:'default.field.location') }</th>
									                <th>${ message(code:'exxtractor.name', args:[message(code:'exxtractor')]) }</th>
									                <th>${ message(code:'exxtractor.serial', args:[message(code:'exxtractor')]) }</th>
									                <th>${ message(code:'date') }</th>
									                <th>${ message(code:'clients.total') }</th>
									            </tr>
									        </tfoot>
									    </table>
									 </div>
								</div> <!-- Table Container End -->
							</div> <!-- end of tab2 ClientReports tab-->
							
							
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</body>
</html>