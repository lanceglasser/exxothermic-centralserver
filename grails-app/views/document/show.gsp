<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.Documents" /></title>
<script
	src="${resource(dir: 'themes/default/js', file: 'jquery.gdocsviewer.min.js')}"></script>
<script type="text/javascript" src="${resource(dir: 'themes/default/js', file: 'jquery.media.js?v0.92')}"></script>
<script src="${resource(dir: 'themes/default/js/', file: 'bootstrap.js')}"></script>
<script src="${resource(dir: 'themes/default/js/', file: 'bootstrap.file-input.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_document.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_utils.js')}"></script>
<script>
	Exxo.module = "document-create";
	Exxo.UI.translates['closeButton'] = "${message(code:'default.field.buttonClose')}";
</script>
</head>
<body>
<g:render template="/layouts/internalContentTemplate">
	<h1> <g:message code="default.title.Documents" /> </h1>
	
	<div class="content form-info">
		<g:render template="/layouts/messagesAndErrorsTemplate" model="[entity: entity]"/>

			<g:hiddenField name="offset" value="${offset}" />
			
		 	<div class="col-xs-6 resize-formleft">
			 	<custom:editField name="name" value="${document?.name}" label="default.field.title" />
				<custom:editField  name="type" value="${document.type.name}" label="default.field.type" />
				<custom:editField  name="file" value="${document?.filename}" label="default.field.file" />	
				<span id="fileDiv">
		            <div class="col-xs-4 text-to-right"></div>
		            <div class="col-xs-8 text-to-left">
		            	<a href="${ document.url }" class="view-pdf">${ message(code:'default.action.show') }</a>
		           	</div>
				</span>		
				<custom:editField  name="expirationDate" value="${formatDate(format:'MM/dd/yyyy', date:document?.expirationDate)}" 
					label="default.field.expirationDate" />
			</div>
		<span class="clearfix"></span>
		<g:link action="listAll" controller="document">        
	       <span class="form-btn btn-primary"> ${message(code:'default.field.buttonBack')}</span>       
	  	</g:link>
	 </div>
</g:render>
<div id="pdf-viewer" title="${message(code:'document.pdf.viewer')}" style="display:none;">
  <!--  <a href="" id="pdf-viewer-link"></a>  -->
  <!-- <object id="pdf-object" data="" type="application/pdf" width="100%" height="480px"> </object> -->
  <!-- <iframe id="pdf-iframe" width="790px" height="470px" src="" > </iframe> -->
</div>
</body>
</html>