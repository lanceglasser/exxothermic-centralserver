<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.Documents" /></title>
<script>
	Exxo.module = "empty-list";
</script>
</head>
</head>
<body>
	<section class="introp">
			<div class="intro-body">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2" id="home-salute">
							<div id="child-salute">
								<div class="message">
									<p><g:message code="documents.empty.list"/></p>
									<g:link controller="document" action="create"><g:message code="menu.documents.create"/></g:link>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
</body>
</html>