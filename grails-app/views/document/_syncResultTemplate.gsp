<!-- Information with the status of every transaction -->
<g:if test="${jsonTransactionStatus}" >
<div id="syncResultContainer">
	<br/>
     			<legend>
     				<g:message code="default.title.statusSendDocument" />
	</legend>      			
     			<table class="display dataTable" cellspacing="0" width="100%" role="grid" aria-describedby="table-list_info" style="width: 100%;">     
     				<thead>    				
    					<tr>
    						<th>${message(code:'default.field.serial')}</th>
    						<th>${message(code:'default.field.status')}</th>
    						<th>${message(code:'default.field.errorDescription')}</th>	          						
    						<th>${message(code:'default.title.Documents')}</th>  						
    					</tr>
   					</thead>	
   					<tbody> <!-- style="text-align='center'" -->
      				<g:each in="${jsonTransactionStatus?.documentsStatus}" status="i" var="record">
      					<tr id=""  class="${(i % 2) == 0 ? 'even' : 'odd'}">
      						<td class="widthTdRow10">		          								
      							${record.serialNumber}
      						</td>
      						<td class="widthTdRow15">		          							
      							${message(code:'boxes.sync.result.status.'+record.successful)}
      						</td>
      						<td class="widthTdRow40">
      							${message(code:messageErrorDescriptionHash.get(record.errorCode))}
      						</td>
      						<td class="widthTdRow40">
      							<g:each in="${record.documentIds}" var="docId">
									${documentsHashInfo.get(docId.toString())},
     							</g:each>
      						</td>
      					</tr>	
      				</g:each>
     				</tbody>
     			</table>
</div>
    
       </g:if>