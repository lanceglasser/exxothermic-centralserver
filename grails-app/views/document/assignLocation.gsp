<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.Documents" /></title>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_custom_button_assign.js')}"></script>
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
<script
	src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
<script
	src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
<script>
	Exxo.module = "button-assign-location";
	Exxo.UI.vars['defaultButtonId'] = "${documentId}";
</script>
</head>
<body>
	<section class="introp">
		<div class="intro-body">
			<div class="container full-width">
				<div class="row">
					<div class="col-md-12" id="internal-content">
						<h1>
							${ message(code:'assign.general', args:[message(code:'default.home.documents')]) }
						</h1>
						<p class="muted"> <g:message code="default.assign.location.note" /></p>
						<sec:ifAnyGranted roles="ROLE_OWNER, ROLE_OWNER_STAFF">
						<p class="muted"><b><g:message code="associated.with.premium.companies.only" /></b></p>
						</sec:ifAnyGranted>
						
						<div class="content ">

							<g:render template="/layouts/messagesAndErrorsTemplate" />
							
							<g:form action="saveLocations" name="createForm" id="createForm" class="exxo-form">
							<div class="col-xs-12 resize-formleft">
								<div class="col-xs-3">
									<label class="control-label" for="user">
										<g:message code="default.home.manageDocument" />*
									</label>
								</div>
								<div class="col-xs-9">											
									<g:select name="buttons" id="buttons" from="${documentList}" optionKey="id" optionValue="name" value="${ documentId }" 
										onchange="Exxo.Button.Assign.clearScrean();${remoteFunction(
								            action:'ajaxAssignLocation', 
								            params:'\'buttonId=\' + escape(this.value)',
											update : 'infor')}"/>
								</div>
							</div>
							<span class="clearfix"></span>							
							
							<div class="col-xs-12 resize-formleft">
								<div class="col-xs-3">
								</div>
								<div class="col-xs-9">
									<div class="exxo-table-container-full-width" id="infor"></div>
								</div>
							</div>
							<span class="clearfix"></span>							
														
							<div class="col-xs-12 resize-formleft">
								<div class="col-xs-3">
								</div>
								<div class="col-xs-9">
									<input type="hidden" id="locationsToSave" name="locationsToSave" value=""/>
									<g:submitButton class="form-btn btn-primary" name="save" value="${message(code:'default.field.buttonSave')}" />					
									
									<g:link action="listAll" controller="document" class="back-button">        
								       <span class="form-btn btn-primary">
								          ${message(code:'default.field.buttonCancel')}
								       </span>       
								    </g:link>
								</div>
							</div>
							<span class="clearfix"></span>
							</g:form>
							
							<!-- Sync Result -->
							<g:render template="/document/syncResultTemplate" model="[jsonTransactionStatus: jsonTransactionStatus]" />
							
						</div> <!-- End of Document -->
					</div>
				</div>
			</div>
		</div>
	</section>
</body>
</html>