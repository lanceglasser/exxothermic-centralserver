<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.Documents" /></title>
<script src="${resource(dir: 'themes/default/js', file: 'jquery.gdocsviewer.min.js')}"></script>
<script src="${resource(dir: 'themes/default/js/', file: 'bootstrap.js')}"></script>
<script src="${resource(dir: 'themes/default/js/', file: 'bootstrap.file-input.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_document.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_utils.js')}"></script>
<script>
	Exxo.module = "document-create";
	Exxo.UI.translates['closeButton'] = "${ message(code:'default.field.buttonClose') }";
	Exxo.UI.translates[Exxo.Constants.ERROR_FILE_INVALID] = "${ message(code:'invalid.document.message', args:['5MB']) }";
	Exxo.UI.translates[Exxo.Constants.ERROR_FILE_SIZE] = "${ message(code:'invalid.document.message', args:['5MB']) }";
	Exxo.UI.vars['filename'] = "${document?.filename}";
</script>
</head>
<body>
<g:render template="/layouts/internalContentTemplate">
	<h1>
		<g:message code="default.title.Documents" />
	</h1>
	
	<div class="content">
		<g:render template="formTemplate" model="[formAction: 'update']"></g:render>		
	</div>
</g:render>
<div id="pdf-viewer" title="${message(code:'document.pdf.viewer')}" style="display:none;">
</div>
</body>
</html>