

		<g:render template="/layouts/messagesAndErrorsTemplate" model="[entity: document]"/>
		<g:uploadForm  action="${ formAction }" name="createForm"  class="exxo-form" >
		 	<div class="col-xs-6 resize-formleft">
		 	<g:hiddenField name="id" value="${document?.id}" />
			<g:hiddenField name="version" value="${document?.version}" />
			
			<div class="col-xs-4 text-to-right">
				<label for="type"><g:message code="default.field.category" /></label>
			</div>
			<div class="col-xs-8 text-to-left">
				<g:select id="type" name='type.id' value="${document?.type?.id}"
				    from='${type}'
				    optionKey="id" optionValue="name">
				</g:select>
			</div>
			<span class="clearfix"></span>
				
		 	<custom:editField type="text" name="name" required="${true}" value="${document?.name}" label="default.field.title" />
			
			<!-- File Selector -->
			<g:render template="/layouts/tags/fileUploadSelectorTemplate" 
				model="[
					id:				'file',
					label:			message(code:'default.field.file'),
					note:			message(code: 'invalid.document.message', args:['5MB']),
					accept:			'application/pdf'
					]"
			/>
			
			<g:if test="${ document?.url != null && document?.url != '' }">
			<div class="col-xs-4 text-to-right">
			</div>
			<div class="col-xs-8 text-to-left">
				<a href="${ document.url }" class="view-pdf">${ message(code:'preview.current.document') }</a>
			</div>
			<span class="clearfix"></span>
			</g:if>
			
			<custom:editField type="date" note="${ message(code: 'schedule.date.format')}" name="expirationDate" required="${false}" value="${formatDate(format:'MM/dd/yyyy', date:document?.expirationDate)}" label="default.field.expirationDate" />

	<br/>
	<!-- "helpDiv" -->
	<div id="helpDiv" >
	            <div class="col-xs-4 text-to-right">
	              <label for="title">${message(code:'default.field.content.help.label')}</label>
	            </div>
	            <div class="col-xs-8 text-to-left">
		            <a href="#" class="view-imageHelp">
		            	<img class="floatLeft" alt="No Image" src="${ resource(dir: 'themes/default/img', file: 'Help-Support.png') }">
		            </a>
		            <div class="noteHelp" >
			            <span class="liten-up floatLeft">
		            	${message(code:'default.field.content.help.tab.note')}                         
		            	</span>
	            	</div>
	            </div>
	</div>

          	<g:submitButton class="form-btn btn-primary" name="save" value="${message(code:'default.field.buttonSave')}" />
			<g:link action="listAll" controller="document" class="back-button">        
		       <span class="form-btn btn-primary"> ${message(code:'default.field.buttonCancel')}</span>       
		  	</g:link>
		 </g:uploadForm>
	
	
	<span class="clearfix"></span>
<div id="imageHelp-viewer" title="${message(code:'default.field.content.help.title')}" style="display:none;">
<article class="tabs">

	<section id="tab1">
		<h2 id="tab1"><a href="#tab1">${message(code:'default.field.document.doc.tab')}</a></h2>
		<div class="featuredHelp" >   
	   		<span>Info:	${message(code:'default.field.document.doc')}
	   		<br/>
	   		<img src="${ resource(dir: 'themes/default/img', file: 'doc1.png') }">	
	   		</span>
		</div>
		<div class="helpArrow" >   
	   		<span>${message(code:'default.field.document.doc.selection')}
	   		<br/>
	   		<img src="${ resource(dir: 'themes/default/img', file: 'arrow-right-icon.png') }">	
	   		</span>
	   		
		</div>
		
		<div class="dialogHelp" >   
	   		<span>Info:	${message(code:'default.field.document.pdf')}
	   		<br/>
	   		<img src="${ resource(dir: 'themes/default/img', file: 'doc2.png') }">	
	   		</span>
		</div>
		
	</section>	

</article>
</div>
	
	