<g:set var="tableId" value="${(tableId != null)? tableId:'table-list'}" />
<g:set var="locationId" value="${(locationId != null)? locationId:0}" />

<table id="${tableId}" class="display" cellspacing="0" width="100%">
	<thead>
    	<tr>			     
			<th>${message(code: 'default.field.name')}</th>
			<th>${message(code: 'default.field.category')}</th>	
			<th>${message(code: 'preview.files')}</th>					    							   				
			<th>${message(code: 'default.table.action')}</th>		
  		</tr>
	</thead>
	<tfoot>
  		<tr>
   			<th>${message(code: 'default.field.name')}</th>
			<th>${message(code: 'default.field.category')}</th>	
			<th>${message(code: 'preview.files')}</th>						    							   				
			<th>${message(code: 'default.table.action')}</th>	
  		</tr>
	</tfoot>
	<tbody>
	<g:each in="${documentsList}" status="i" var="entity">
		<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
			<td>
				<sec:ifAnyGranted roles="ROLE_HELP_DESK">
				<g:link controller="document" action="edit" id="${entity.id}">${entity.name.encodeAsHTML()}</g:link>
				</sec:ifAnyGranted>
				<sec:ifNotGranted roles="ROLE_HELP_DESK">
				<g:if test="${ ""+entity.createdBy.id == ""+sec.loggedInUserInfo(field:'id')  }">
				<g:link controller="document" action="edit" id="${entity.id}">${entity.name.encodeAsHTML()}</g:link>
				</g:if>
				<g:else>
				${entity.name.encodeAsHTML()}
				</g:else>
				</sec:ifNotGranted>
			</td>
			<td>${ entity.type.name }</td>
			<td><a href="${ entity.url }" class="view-pdf">${ message(code:'default.action.show') }</a></td>					
			<td>	
				<g:link controller="document" action="show" id="${entity.id}" class="action-icons view" title="${message(code:'default.action.show')}"></g:link>

				<sec:ifAnyGranted roles="ROLE_HELP_DESK">
					<g:link controller="document" action="edit" id="${entity.id}"  class="action-icons edit" title="${message(code:'default.action.edit')}"></g:link>
					<g:if test="${ locationId != 0 }">
						<g:link controller="document" action="unassignOfLocation" id="${entity.id}" params="[locId: locationId]"
							class="action-icons unassign" title="${message(code:'unassign.coontent.of.location')}"></g:link>						
					</g:if>
					<g:link controller="document" action="delete" id="${entity.id}"  class="action-icons delete" title="${message(code:'default.action.delete')}"></g:link>
				</sec:ifAnyGranted>
				<sec:ifNotGranted roles="ROLE_HELP_DESK">
					<g:if test="${ ""+entity.createdBy.id == ""+sec.loggedInUserInfo(field:'id')  }">
					<g:link controller="document" action="edit" id="${entity.id}"  class="action-icons edit" title="${message(code:'default.action.edit')}"></g:link>
					<g:if test="${ locationId != 0 }">
						<g:link controller="document" action="unassignOfLocation" id="${entity.id}" params="[locId: locationId]"
							class="action-icons unassign" title="${message(code:'unassign.coontent.of.location')}"></g:link>						
					</g:if>
					<g:link controller="document" action="delete" id="${entity.id}"  class="action-icons delete" title="${message(code:'default.action.delete')}"></g:link>
					</g:if>
				</sec:ifNotGranted>
			</td>
		</tr>
	</g:each>
  </tbody>
</table>