<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main" />
		<title><g:message code="default.title.listLocation" /></title>
		<script>
			Exxo.module = "index";
		</script>
	</head>
	<body>
		<section class="introp">
			<div class="intro-body">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2" id="home-salute">
							<div id="child-salute">
								<div class="message">
									<g:if test="${noButtons}">
										<p><g:message code="default.no.document.toDisplay"/></p>
									</g:if>
									<g:else>	 
										<p><g:message code="default.no.document.toManage"/> </p>			
								 	</g:else>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</body>
</html>
