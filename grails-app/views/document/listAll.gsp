<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.Documents" /></title>
<script
	src="${resource(dir: 'themes/default/js', file: 'jquery.gdocsviewer.min.js')}"></script>
<script type="text/javascript" src="${resource(dir: 'themes/default/js', file: 'jquery.media.js?v0.92')}"></script>
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
<script
	src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
<script
	src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
<script
	src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_app_skin_list.js')}"></script>
<script>
	Exxo.module = "app-skin-list";
	Exxo.UI.vars["page-code"] = "document-list";
	Exxo.Tables.loadPaginationInitValuesFromString('<custom:paginationInfo code="document-list"/>');
	Exxo.UI.translates['okButton'] = "${message(code:'default.field.buttonOk')}";
	Exxo.UI.translates['cancelButton'] = "${message(code:'default.field.buttonCancel')}";
	Exxo.UI.translates['disableMessage'] = "${message(code:'mycentralserver.custombuttons.CustomButton.isDisabled')}";
	Exxo.UI.translates['closeButton'] = "${message(code:'default.field.buttonClose')}";
</script>
</head>
</head>
<body>
<g:render template="/layouts/internalContentTemplate">
	<h1>
		<g:message code="menu.documents.list" />
	</h1>
	
	<div class="content">
		<g:render template="/layouts/messagesAndErrorsTemplate" />
		
           <table id="table-list" class="display" cellspacing="0" width="100%">
             <thead>
               <tr>			     
		     		<th>${message(code: 'default.field.title')}</th>
		     		<th>${message(code: 'default.field.category')}</th>	
	     			<th>${message(code: 'preview.files')}</th>					    							   				
		     		<th>${message(code: 'default.table.action')}</th>		
               </tr>
             </thead>
             <tfoot>
               <tr>
                	<th>${message(code: 'default.field.name')}</th>
                	<th>${message(code: 'default.field.category')}</th>	
		    		<th>${message(code: 'preview.files')}</th>						    							   				
		     		<th>${message(code: 'default.table.action')}</th>	
               </tr>
             </tfoot>
             <tbody>
             	<g:each in="${list}" status="i" var="entity">
				<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					<td>
						<sec:ifAnyGranted roles="ROLE_HELP_DESK,ROLE_ADMIN">
							<g:link action="edit" id="${entity.id}">${entity.name.encodeAsHTML()}</g:link>
						</sec:ifAnyGranted>
						<sec:ifNotGranted roles="ROLE_HELP_DESK,ROLE_ADMIN">
							<g:if test="${ ""+entity.createdBy.id == ""+sec.loggedInUserInfo(field:'id')  }">
								<g:link action="edit" id="${entity.id}">${entity.name.encodeAsHTML()}</g:link>
							</g:if>
							<g:else>
								${entity.name.encodeAsHTML()}
							</g:else>
						</sec:ifNotGranted>
					</td>
					<td>${ entity.type.name }</td>	
					<td><a href="${ entity.url }" class="view-pdf">${ message(code:'default.action.show') }</a></td>					
					<td>	
						<g:link action="show" id="${entity.id}" class="action-icons view" title="${message(code:'default.action.show')}"></g:link>
						
						<sec:ifAnyGranted roles="ROLE_HELP_DESK,ROLE_ADMIN">
							<g:link action="edit" id="${entity.id}"  class="action-icons edit" title="${message(code:'default.action.edit')}"></g:link>
							<g:link action="delete" id="${entity.id}"  class="action-icons delete" title="${message(code:'default.action.delete')}"></g:link>
						</sec:ifAnyGranted>
						<sec:ifNotGranted roles="ROLE_HELP_DESK,ROLE_ADMIN">
							<g:if test="${ ""+entity.createdBy.id == ""+sec.loggedInUserInfo(field:'id')  }">
								<g:link action="edit" id="${entity.id}"  class="action-icons edit" title="${message(code:'default.action.edit')}"></g:link>
								<g:link action="delete" id="${entity.id}"  class="action-icons delete" title="${message(code:'default.action.delete')}"></g:link>
							</g:if>
						</sec:ifNotGranted>
					</td>
				</tr>
			</g:each>
             </tbody>
           </table>
	</div>
</g:render>
<div id="confirm-delete-dialog" title="${message(code:'default.confirm.title')}" style="display:none;">
  ${message(code:'document.confirm.delete')}
</div>
<div id="pdf-viewer" title="${message(code:'document.pdf.viewer')}" style="display:none;">
  <!--  <a href="" id="pdf-viewer-link"></a>  -->
  <!-- <object id="pdf-object" data="" type="application/pdf" width="100%" height="480px"> </object> -->
  <!-- <iframe id="pdf-iframe" width="790px" height="470px" src="" > </iframe> -->
</div>
</body>
</html>