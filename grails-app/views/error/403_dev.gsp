<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>ExXothermic Central Server | Error Page</title>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link rel="shortcut icon" type="image/png" href="assets/img/favicon.ico">
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="${resource(dir: 'themes/default/css', file: '404.css')}" rel="stylesheet">
<script src="${resource(dir: 'themes/default/js', file: 'modernizr.custom.js')}"></script>
</head>
<body>
<div class="container">
  <div class="row">
    <div class="span12">
      <div class="hero-unit center">
          <h1>You are not authorized</h1>
          <br />
          <p><strong>Stay calm! You are not authorized to see the page you were looking for.</strong></p>
          <p>Try to use our <a href="${g.createLink(controller: 'home', absolute: true)}">HOME PAGE</a> and start your journey from the beginning.</p>
        </div>
    </div>
  </div>
</div>
<footer> <a href="http://www.exxothermic.com/privacy.html" target="_blank"><g:message code="home.privacy" /></a> | <a href="http://www.exxothermic.com/terms.html" target="_blank"><g:message code="home.terms" /></a><br>
    <g:message code="application.copyright" args='["2018"]' /> | <g:message code="application.version" /> ${grailsApplication.metadata['app.version']}
</footer>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script> 
</body>
</html>
