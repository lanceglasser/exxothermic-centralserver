<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.BoxManufacturer" /></title>
<script>
	Exxo.module = "box-manufacturer-show";
</script>
</head>
<body>
	<section class="introp">
		<div class="intro-body">
			<div class="container full-width">
				<div class="row">
					<div class="col-md-12" id="internal-content">
						<h1>
							${manufacturer.serialNumber.encodeAsHTML()}
						</h1>
						
						<div class="content form-info">
							
							<!-- Left Column -->
							<div class="col-xs-6 resize-formleft">
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="default.field.serialNumber" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${manufacturer.serialNumber}">
									<span class="field-value">${manufacturer.serialNumber.encodeAsHTML()}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty" /></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="default.field.certificateFileName" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${manufacturer.certificateFileName}">
									<span class="field-value">${manufacturer.certificateFileName.encodeAsHTML()}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty" /></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="default.field.physicalSerial" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${manufacturer.physicalSerial}">
									<span class="field-value">${manufacturer.physicalSerial.encodeAsHTML()}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty" /></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="default.field.deviceTypeName" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${manufacturer.deviceType?.name}">
									<span class="field-value">${manufacturer.deviceType?.name.encodeAsHTML()}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty" /></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="default.field.machineModel" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${manufacturer.deviceType?.machineModel}">
									<span class="field-value">${manufacturer.deviceType?.machineModel.encodeAsHTML()}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty" /></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
							</div>
							
							<!-- Right Column -->
							<div class="col-xs-6 resize-formright">
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="default.field.machineBrand" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${manufacturer.deviceType?.machineBrand}">
									<span class="field-value">${manufacturer.deviceType.machineBrand?.value.encodeAsHTML()}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty" /></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="default.field.processorBrand" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${manufacturer.deviceType?.processorBrand}">
									<span class="field-value">${manufacturer.deviceType.processorBrand?.value.encodeAsHTML()}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty" /></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="default.field.memory" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${manufacturer.deviceType?.memory}">
									<span class="field-value">${manufacturer.deviceType.memory?.value.encodeAsHTML()}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty" /></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="default.field.memoryType" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${manufacturer.deviceType?.memoryType}">
									<span class="field-value">${manufacturer.deviceType.memoryType?.value.encodeAsHTML()}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty" /></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="default.field.createdBy" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${manufacturer.createdBy}">
									<span class="field-value">${manufacturer.createdBy.firstName.encodeAsHTML() + " "  + manufacturer.createdBy.lastName.encodeAsHTML()}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty" /></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
							</div>
							<span class="clearfix"></span>
							
							<!-- Buttons -->
							<g:link action="listAll" controller="boxManufacturer"
								params="[offset: offset]">
								<input type="button" class="form-btn btn-primary"
									value="${message(code:'default.field.buttonBack')}" />
							</g:link>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</body>
</html>