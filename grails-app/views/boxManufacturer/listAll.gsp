<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.listManufacturer" /></title>
<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
<script src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
<script>
	Exxo.module = "page-list";
	Exxo.UI.vars["page-code"] = "certificates-list";
	Exxo.Tables.loadPaginationInitValuesFromString('<custom:paginationInfo code="certificates-list"/>');
</script>
</head>
<body>
<section class="introp">
  <div class="intro-body">
    <div class="container full-width">
      <div class="row">
        <div class="col-md-12" id="internal-content">
          	<h1><g:message code="default.manufacturer.list" /></h1>
			<g:if test="${flash.error}">
				<div class="alert alert-error">
					<a class="close" data-dismiss="alert">×</a>
					${flash.error}
					${flash.error=null}
				</div>
			</g:if>
			<g:if test="${flash.success}">
	        		<div class="alert alert-success">  
					  <a class="close" data-dismiss="alert">×</a>  
					  ${flash.success} 
					  ${flash.success=null}
					</div> 
      		</g:if>
		<div class="content">
            <table id="table-list" class="display" cellspacing="0" width="100%">
              <thead>
                <tr>
					<th>${message(code: 'default.field.serialNumber')}</th>				
					<th>${message(code: 'default.field.certificateFileName')}</th>
					<th>${message(code: 'default.field.physicalSerial')}</th>					
					<th>${message(code: 'default.field.macAddress')}</th>
					<th>${message(code: 'default.table.action')}</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                	<th>${message(code: 'default.field.serialNumber')}</th>				
					<th>${message(code: 'default.field.certificateFileName')}</th>
					<th>${message(code: 'default.field.physicalSerial')}</th>					
					<th>${message(code: 'default.field.macAddress')}</th>
					<th>${message(code: 'default.table.action')}</th>
                </tr>
              </tfoot>
              <tbody>
              	<g:each in="${manufacturers}" status="i" var="manufacturer">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<td><g:link action="edit" id="${manufacturer.id}" params="[offset: offset]">${manufacturer.serialNumber.encodeAsHTML()} </g:link></td>
						<td>${manufacturer.certificateFileName.encodeAsHTML()}</td>
						<td>${manufacturer.physicalSerial?.encodeAsHTML()}</td>
						<td>${manufacturer.getMacAddress().encodeAsHTML()}</td>
						<td>
							<g:link action="show" id="${manufacturer.id}" class="action-icons view" title="${message(code:'default.action.show')}" params="[offset: offset]"></g:link> 
							<g:link action="edit" id="${manufacturer.id}" class="action-icons edit" title="${message(code:'default.action.edit')}" params="[offset: offset]"></g:link>  
						</td>
					</tr>
				</g:each>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</body>
</html>