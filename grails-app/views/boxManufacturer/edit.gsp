<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.updateCertificate" /></title>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_box_manufacturer_edit.js')}"></script>
<script>
	Exxo.module = "box-manufacturer-edit";
	Exxo.UI.urls["loadStatesByCountryUrl"] = "${g.createLink(controller: 'common', action: 'statebycountry', absolute: true)}";
</script>
</head>
<body>
	<section class="introp">
		<div class="intro-body">
			<div class="container full-width">
				<div class="row">
					<div class="col-md-12" id="internal-content">
						<h1>
							<g:message code="default.title.updateCertificate" />
						</h1>
						<div class="content">
							<g:if test="${flash.error}">
								<div class="alert alert-error">
									<a class="close" data-dismiss="alert">×</a>
									${flash.error}
									<g:hasErrors bean="${manufacturer}">
										<ul>
											<g:eachError bean="${manufacturer}" var="error">
												<li
													<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${flash.error}"</g:if>><g:message
														error="${error}" /></li>
											</g:eachError>
										</ul>
									</g:hasErrors>
									<g:hasErrors bean="${manufacturer.deviceType}">
										<ul>
											<g:eachError bean="${manufacturer.deviceType}" var="error">
												<li
													<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
														error="${error}" /></li>
											</g:eachError>
										</ul>
									</g:hasErrors>
								</div>
								${flash.error=null}
							</g:if>
							<g:if test="${flash.success}">
								<div class="alert alert-success">
									<a class="close" data-dismiss="alert">×</a>
									${flash.success}
								</div>
								${flash.success=null}
							</g:if>
							
							<g:form action="update" id="${params.id}" name="updateForm" class="exxo-form">
								<g:hiddenField name="id" value="${manufacturer?.id}" />
								<g:hiddenField name="version" value="${manufacturer?.version}" />
								<g:hiddenField name="offset" value="${offset}" />
		 						
								<!-- Column Left -->
								<div class="col-xs-6 resize-formleft">
									<div class="col-xs-4 text-to-right">
										<label for="serialNumber">
											<g:message code="default.field.serialNumber" />
										</label>
									</div>
									<div class="col-xs-8 text-to-left">
										<g:field type="text" readonly="true"
											name="serialNumber" maxlength="50" required="required"
											value="${manufacturer?.serialNumber}" />
									</div>
									<span class="clearfix"></span>
									
									<div class="col-xs-4 text-to-right">
										<label for="certificateFileName">
											<g:message code="default.field.certificateFileName" />
										</label>
									</div>
									<div class="col-xs-8 text-to-left">
										<g:field type="text" readonly="true"
											name="certificateFileName" maxlength="100"
											value="${manufacturer?.certificateFileName}" />
									</div>
									<span class="clearfix"></span>
									
									<div class="col-xs-4 text-to-right">
										<label for="physicalSerial">
											<g:message code="default.field.physicalSerial" />*
										</label>
									</div>
									<div class="col-xs-8 text-to-left">
										<g:textField required="required" class="required"
											name="physicalSerial" maxlength="100"
											value="${manufacturer?.physicalSerial}" />
									</div>
									<span class="clearfix"></span>
									
									<div class="col-xs-4 text-to-right">
										<label for="name">
											<g:message code="default.field.deviceTypeName" />*
										</label>
									</div>
									<div class="col-xs-8 text-to-left">
										<g:textField required="required" class="required" name="name"
											maxlength="100" value="${manufacturer.deviceType?.name}" />
									</div>
									<span class="clearfix"></span>
									
									<div class="col-xs-4 text-to-right">
										<label for="machineModel">
											<g:message code="default.field.machineModel" />*
										</label>
									</div>
									<div class="col-xs-8 text-to-left">
										<g:textField required="required" class="required"
											name="machineModel" maxlength="100"
											value="${manufacturer.deviceType?.machineModel}" />
									</div>
									<span class="clearfix"></span>
									
								</div>
								
								<!-- Column Right -->
								<div class="col-xs-6 resize-formright">
									<div class="col-xs-4 text-to-right">
										<label for="machineBrand">
											<g:message code="default.field.machineBrand" />
										</label>
									</div>
									<div class="col-xs-8 text-to-left">
										<g:select name="machineBrand.id" id="machineBrand"
											from="${machineBrands}" optionKey="id" optionValue="value"
											value="${manufacturer.deviceType?.machineBrand?.id}"
											noSelection="['':'']"
											placeholder="${message(code:'default.field.selectMachineBrand')}" />
									</div>
									<span class="clearfix"></span>
									
									<div class="col-xs-4 text-to-right">
										<label for="processorBrand">
											<g:message code="default.field.processorBrand" />*
										</label>
									</div>
									<div class="col-xs-8 text-to-left">
										<g:select name="processorBrand.id" id="processorBrand"
											from="${processorBrands}" optionKey="id" optionValue="value"
											value="${manufacturer.deviceType?.processorBrand?.id}"
											noSelection="['':'']"
											placeholder="${message(code:'default.field.selectMachineBrand')}" />
									</div>
									<span class="clearfix"></span>
									
									<div class="col-xs-4 text-to-right">
										<label for="memory">
											<g:message code="default.field.memory" />*
										</label>
									</div>
									<div class="col-xs-8 text-to-left">
										<g:select name="memory.id" id="memory" from="${memories}"
											optionKey="id" optionValue="value"
											value="${manufacturer.deviceType?.memory?.id}"
											noSelection="['':'']"
											placeholder="${message(code:'default.field.selectMemory')}" />
									</div>
									<span class="clearfix"></span>
									
									<div class="col-xs-4 text-to-right">
										<label for="memoryType">
											<g:message code="default.field.memoryType" />*
										</label>
									</div>
									<div class="col-xs-8 text-to-left">
										<g:select name="memoryType.id" id="memoryType"
											from="${memoryTypes}" optionKey="id" optionValue="value"
											value="${manufacturer.deviceType?.memoryType?.id}"
											noSelection="['':'']"
											placeholder="${message(code:'default.field.selectMemoryType')}" />
									</div>
									<span class="clearfix"></span>
								</div>
								<span class="clearfix"></span>
							
								<!-- Buttons -->
								<g:submitButton class="form-btn btn-primary" name="saves"
									value="${message(code:'default.field.buttonSave')}" />
				
								<g:link action="listAll" controller="boxManufacturer" class="back-button">
									<span class="form-btn btn-primary"> ${message(code:'default.field.buttonCancel')}
									</span>
								</g:link>
								
							</g:form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

</body>
</html>