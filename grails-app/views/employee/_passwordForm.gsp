	<g:form action="changepassword"  class="form-horizontal">
		<g:hiddenField name="id" value="${user?.id}" />
		 <fieldset>  
		 			 
			 <legend><g:message code="default.title.changePassword" /> - ${user?.firstName?.encodeAsHTML()}  ${user?.lastName?.encodeAsHTML()}</legend>
			 
	         <g:if test="${flash.error}">
	        		<div class="alert alert-error">  
						 <a class="close" data-dismiss="alert">×</a>
						  ${flash.error} 				 					   
							<g:hasErrors bean="${user}">    						  				  				
						  		<ul>
									<g:eachError bean="${user}" var="error">
									
										<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
									</g:eachError>
								</ul>
							</g:hasErrors>  	
						${flash.error=null}
					</div>
			 </g:if>
      		 <g:if test="${flash.success}">
	        		<div class="alert alert-success">  
					  <a class="close" data-dismiss="alert">×</a>  
					  ${flash.success} 
					   ${flash.success=null} 
					</div> 
			</g:if>	
			
			<div class="control-group">
				<label class="control-label" for="newPassword" ><g:message code="default.field.newpassword" />*</label>
				<div class="controls">
					<g:passwordField class="input-xlarge" required="required" name="newPassword"/>					
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" for="confirmPassword" ><g:message code="default.field.confirmPassword" />*</label>
				<div class="controls">
					<g:passwordField class="input-xlarge" required="required" name="confirmPassword"/>
				</div>
			</div>
			<div class="form-actions">
				<g:submitButton class="btn btn-primary" name="submit" value="${message(code:'default.field.buttonSave')}" />
				
				<g:if test="${redirectAction == 'changeUserPassword'}">
			 	 
					<g:link action="listAll" controller="employee" class="back-button">        
				       <span class="btn btn-primary">
				          ${message(code:'default.field.buttonCancel')}
				       </span>       
			    	</g:link>
			    	
			 	</g:if>
			 	<g:else>				
					<g:link action="index" controller="home" class="back-button">        
				       <span class="btn btn-primary">
				          ${message(code:'default.field.buttonCancel')}
				       </span>       
			    	</g:link>
				</g:else>	
			</div> 
			 </fieldset>	  
		</g:form>