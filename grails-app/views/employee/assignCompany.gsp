<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.assignCompany" /></title>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_employee_asign.js')}"></script>
<script>
	Exxo.module = "employee-asign";
</script>
</head>
<body>
	<section class="introp">
		<div class="intro-body">
			<div class="container full-width">
				<div class="row">
					<div class="col-md-12" id="internal-content">
						<h1>
							<g:message code="default.title.assignCompany" />
						</h1>
						<g:form action="associateCompanies" name="createForm">
						<div class="content exxo-form">
							
							<g:render template="/layouts/messagesAndErrorsTemplate" model="[entity: company, secEntity: user]"/>
											
							<div class="col-xs-6 resize-formleft">
								<div class="col-xs-3">
									<label class="control-label" for="user">
										<g:message code="default.field.name" /> *
									</label>
								</div>
								<div class="col-xs-9">
									<g:select name="user"  from="${usersList}" optionKey="id"  optionValue="${{it.firstName + " " + it.lastName}}" required="required" value="${user.id}" 
									onchange="Exxo.showSpinner();${remoteFunction(
									            action:'ajaxAssignCompany', 
									            params:'\'userId=\' + escape(this.value)',
												update : 'infor')}"/>	
								</div>
								<span class="clearfix"></span>
							</div>
							<span class="clearfix"></span>
							
							<div class="col-xs-6 resize-formleft">
								<div class="control-group table" id="infor"></div>
							</div>
							<span class="clearfix"></span>
											 		
							<div class="col-xs-6">
								<g:submitButton class="form-btn btn-primary"
									name="associateCompanies"
									value="${message(code:'default.field.buttonSave')}" />		
							
								<g:link action="listAll" class="back-button">        
							       <span class="form-btn btn-primary">
							          ${message(code:'default.field.buttonCancel')}
							       </span>       
							    </g:link> 	
							</div>
							<span class="clearfix"></span>
						</div>
						</g:form>
					</div>
				</div>
			</div>
		</div>
	</section>

</body>
</html>