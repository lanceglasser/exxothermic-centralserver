<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.user" /></title>
<script>
	Exxo.module = "employee-show";
</script>
</head>
<body>
	<section class="intro">
		<div class="intro-body">
			<div class="container full-width">
				<div class="row">
					<div class="col-md-12" id="internal-content">
						<h1>
							${user.firstName.encodeAsHTML() + " " + user.lastName.encodeAsHTML()}
						</h1>

						<div class="content form-info">
							<div class="col-xs-6 resize-formleft">
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="field.email" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${user.email}">
									<span class="field-value">${user.email.encodeAsHTML()}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty" /></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="user.firstName.label" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${user.firstName}">
									<span class="field-value">${user.firstName.encodeAsHTML()}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty"/></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="user.lastName.label" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${user.lastName}">
									<span class="field-value">${user.lastName.encodeAsHTML()}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty"/></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="default.field.employeeFor" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${user.employeers() && user.employeers().size() > 0}">
									<span class="field-value">${user.employeers().name.encodeAsHTML()}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty"/></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="field.phone.number" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${user.phoneNumber}">
									<span class="field-value">${user.phoneNumber.encodeAsHTML()}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty"/></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
							</div>
							<div class="col-xs-6 resize-formright">
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="user.lastUpdatedBy.label" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${user.lastUpdatedBy}">
									<span class="field-value">${user?.lastUpdatedBy?.firstName?.encodeAsHTML() + " " + user?.lastUpdatedBy?.lastName?.encodeAsHTML()}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty" /></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="default.field.accountExpired" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${user.accountExpired == true}">
									<span class="field-value"><g:message code="default.field.yes" /></span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.no" /></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="default.field.accountLocked" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${user.accountLocked == true}">
									<span class="field-value"><g:message code="default.field.yes" /></span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.no" /></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="default.field.passwordExpired" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${user.passwordExpired == true}">
									<span class="field-value"><g:message code="default.field.yes" /></span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.no" /></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="default.field.enabled" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${user.enabled == true}">
									<span class="field-value"><g:message code="default.field.yes" /></span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.no" /></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
							</div>
							<span class="clearfix"></span>
							
							<div class="col-xs-12">								
							<g:if test="${user.employeers().size >0 }">
								<g:link action="listAll" class="back-button"> 
									<input type="button" class="form-btn btn-primary" value="${message(code:'default.field.buttonBack')}"/> 
								</g:link>
							</g:if>
							<g:else>
								<g:link action="listAllNonAssign" class="back-button"> 
									<input type="button" class="form-btn btn-primary" value="${message(code:'default.field.buttonBack')}"/> 
								</g:link>
							</g:else>
							</div>
							<span class="clearfix"></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</body>
</html>