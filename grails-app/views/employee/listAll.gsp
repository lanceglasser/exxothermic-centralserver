<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.listCompanyUser" /></title>
<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
<script src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_employee_list.js')}"></script>
<script>
	Exxo.module = "employee-list";
	Exxo.UI.vars["page-code"] = "employee-list";
	Exxo.Tables.loadPaginationInitValuesFromString('<custom:paginationInfo code="employee-list"/>');
	Exxo.UI.translates['okButton'] = "${message(code:'default.field.buttonOk')}";
	Exxo.UI.translates['cancelButton'] = "${message(code:'default.field.buttonCancel')}";
	Exxo.UI.translates['disableMessage'] = "${message(code:'mycentralserver.user.User.resetPassword')}";
</script>
</head>
<body>
<section class="introp">
  <div class="intro-body">
    <div class="container full-width">
      <div class="row">
        <div class="col-md-12" id="internal-content">
          <h1><g:message code="default.title.listCompanyUser" /></h1>
			<g:if test="${flash.error}">
	       		<div class="alert alert-error">  
				  <a class="close" data-dismiss="alert">×</a>  
				  ${flash.error} 
				  ${flash.error=null} 
				</div> 
			</g:if>
			
			<g:if test="${flash.success}">
	       		<div class="alert alert-success">  
				  <a class="close" data-dismiss="alert">×</a>  
				  ${flash.success} 
				  ${flash.success=null} 
				</div> 
	    	</g:if>
		<div class="content">
			<g:if test="${employeeList.size()>0}">
            <table id="table-list" class="display" cellspacing="0" width="100%">
              <thead>
                <tr>			     
			     <th>${message(code: 'user.email.label')}</th>
			     <th>${message(code: 'roles')}</th>
			     <th>${message(code: 'employee.companies')}</th>
			     <th>${message(code: 'user.firstName.label')}</th>
			     <th>${message(code: 'user.lastName.label')}</th>							    							   
			     <th>${message(code: 'default.field.enabled')}</th>							     
			     <th>${message(code: 'default.table.action')}</th>	
                </tr>
              </thead>
              <tbody>
              	<g:each in="${employeeList}" status="i" var="employeeInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<td><g:link action="edit" id="${employeeInstance.user?.id}">${employeeInstance.user?.email?.encodeAsHTML()}</g:link></td>
						<td>
							<user:listUserRoles user="${ employeeInstance.user }" />
						</td>
						<td>${employeeInstance.user.employeers().name.toString().replace("[", "").replace("]","").encodeAsHTML()}</td>	
						<td>${employeeInstance.user?.firstName?.encodeAsHTML()}</td>
						<td>${employeeInstance.user?.lastName?.encodeAsHTML()}</td>
						 						
						<td>
							<g:if test="${employeeInstance.user?.enabled == true}">
								<g:message code="default.field.yes" />
							</g:if>
							<g:else>
								<g:message code="default.field.no" />
							</g:else>
						</td>									
						<td>										
							<g:link action="show" id="${employeeInstance.user?.id}"class="action-icons view" title="${message(code:'default.action.show')}" params="[offset: offset]" ></g:link>
							<g:link action="edit" id="${employeeInstance.user?.id}"  class="action-icons edit" title="${message(code:'default.action.edit')}"  params="[offset: offset]"></g:link>  					
							<g:link action="assignCompany" id="${employeeInstance.user?.id}" class="action-icons associate" title="${message(code:'default.action.updateCompany')}" params="[offset: offset]"></g:link>					 										 									
							<g:link action="deleteRelation" id="${employeeInstance.id}" class="action-icons delete" title="${message(code:'default.action.deleteRelation')}" params="[offset: offset]"></g:link> 																			
							<g:link action="resetUserPassword" id="${employeeInstance.user?.id}" class="action-icons changePassword" title="${message(code:'default.action.resetPassword')}" params="[offset: offset]"></g:link>
							<sec:ifAnyGranted roles="ROLE_HELP_DESK">
								<g:link action="changeRole" id="${employeeInstance.user?.id}"  class="action-icons change-role" title="${message(code:'default.action.change.role')}"></g:link>
							</sec:ifAnyGranted> 
						</td>
					</tr>
				</g:each>
              </tbody>
            </table>
            </g:if>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div id="confirm-reset-dialog" title="${message(code:'mycentralserver.user.User.resetPassword.title')}"  style="display:none;min-height: 50px;">
  <div class="row">
  <div style="height: 50px;">${message(code:'mycentralserver.user.User.resetPassword')}<br/></div>
  </div>
</div>
<div id="confirm-role-change-dialog" title="${message(code:'mycentralserver.confirm.user.change.role.title')}"  style="display:none;">
  ${message(code:'mycentralserver.confirm.user.change.role')}
</div>
</body>
</html>