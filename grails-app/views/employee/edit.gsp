<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.updateEmployee" /></title>
<script>
	Exxo.module = "employee-edit";
</script>
</head>
<body>
<g:render template="/layouts/internalContentTemplate">
	<h1>
		<g:message code="default.title.updateEmployee" />
	</h1>
	<div class="content">
		<g:render template="/layouts/messagesAndErrorsTemplate" model="[entity: user]"/>

		<g:form action="update" name="updateForm" class="exxo-form">
			<g:hiddenField name="id" value="${user.id}" />
			<g:hiddenField name="version" value="${user.version}" />
			
			<!-- Left Column -->
			<div class="col-xs-6 resize-formleft">								
				<custom:editField type="text" name="firstName" required="${true}" value="${user?.firstName}" 
					label="default.field.firstName" max="50" placeholder="${g.message(code: "default.field.firstName")}" />
					
				<custom:editField type="text" name="lastName" required="${true}" value="${user?.lastName}" 
					label="default.field.lastName" max="50" placeholder="${g.message(code: "default.field.lastName")}" />
				
				<custom:editField type="email" name="email" disabled="${true}" required="${true}" value="${user?.email}" 
					label="field.email" max="50" placeholder="${g.message(code: "field.email")}" />
				
				<custom:editField type="phoneNumber" name="phoneNumber" required="${false}" value="${user?.phoneNumber}" 
					label="field.phone.number" max="50" note="${message(code: "default.field.example")} ${message(code: "field.phone.number.format")}" 
						placeholder="${message(code: "field.phone.number")} ${message(code: "default.field.example")} ${message(code: "field.phone.number.format")}" />
				
				<custom:editField type="text" name="lastUpdatedBy" disabled="${true}" required="${true}" value="${user.lastUpdatedBy?.firstName + " " + user?.lastUpdatedBy?.lastName}" 
					label="user.lastUpdatedBy.label" />
				
				<custom:editField type="checkbox" name="enabled" disabled="${true}" required="${false}" value="${user.enabled}" 
					label="default.field.enabled" cssclass="input-checkbox"/>
			</div>
			
			<!-- Right Column -->
			<div class="col-xs-6 resize-formright">
			</div>
			<span class="clearfix"></span>
			
			<!-- Buttons -->
			<div class="col-xs-6">
				<g:submitButton class="form-btn btn-primary"
					name="save"
					value="${message(code:'default.field.buttonSave')}" />
					
				<g:if test="${user.employeers().size >0 }">
				 	<g:link action="listAll" controller="employee" class="back-button">        
				       <span class="form-btn btn-primary">
				          ${message(code:'default.field.buttonCancel')}
				       </span>       
				    </g:link>
				</g:if>
				<g:else>
					<g:link action="listAllNonAssign" controller="employee" class="back-button">        
				       <span class="form-btn btn-primary">
				          ${message(code:'default.field.buttonCancel')}
				       </span>       
				    </g:link>
				</g:else>
			</div>
			<span class="clearfix"></span>
		</g:form>
	</div>
</g:render>
</body>
</html>