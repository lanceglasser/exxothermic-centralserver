<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.newEmployee" /></title>
<script>
	Exxo.module = "employee";
</script>
<script
	src="${resource(dir: 'themes/default/js/exxo/', file: 'exxo_employee_form.js')}"></script>
</head>
<body>
	<g:render template="/layouts/internalContentTemplate">
		<h1>
			<g:message code="default.title.newEmployee" />
		</h1>
		<div class="content">

			<g:render template="/layouts/messagesAndErrorsTemplate"
				model="[entity: user]" />

			<g:form action="save" name="createForm" class="exxo-form">
				<!-- Left Column -->
				<div class="col-xs-6 resize-formleft">
					<custom:editField type="text" name="firstName" required="${true}"
						value="${user?.firstName}" label="default.field.firstName"
						max="50"
						placeholder="${g.message(code: "default.field.firstName")}" />

					<custom:editField type="text" name="lastName" required="${true}"
						value="${user?.lastName}" label="default.field.lastName" max="50"
						placeholder="${g.message(code: "default.field.lastName")}" />

					<custom:editField type="email" name="email" required="${true}"
						value="${user?.email}" label="field.email" max="50"
						placeholder="${g.message(code: "field.email")}" />

					<custom:editField type="phoneNumber" name="phoneNumber"
						required="${false}" value="${user?.phoneNumber}"
						label="field.phone.number" max="50"
						note="${message(code: "default.field.example")} ${message(code: "field.phone.number.format")}"
						placeholder="${message(code: "field.phone.number")} ${message(code: "default.field.example")} ${message(code: "field.phone.number.format")}" />

					<sec:ifAnyGranted roles="ROLE_INTEGRATOR, ROLE_HELP_DESK">
						<div class="col-xs-4 text-to-right">
							<label for="userType"> ${ message(code:'default.field.type') }
								*
							</label>
						</div>
						<div class="col-xs-8 text-to-left">
							<select id="userRoles" name="userRoles">
								<option
									value="${ mycentralserver.utils.RoleEnum.INTEGRATOR_STAFF.value }"
									${ role?.role == mycentralserver.utils.RoleEnum.INTEGRATOR_STAFF.value? 'selected="selected"':'' }>
									${ message(code:'user.role.'+mycentralserver.utils.RoleEnum.INTEGRATOR_STAFF.value) }
								</option>
								<option
									value="${ mycentralserver.utils.RoleEnum.OWNER_STAFF.value }"
									${ role?.role == mycentralserver.utils.RoleEnum.OWNER_STAFF.value? 'selected="selected"':'' }>
									${ message(code:'user.role.'+mycentralserver.utils.RoleEnum.OWNER_STAFF.value) }
								</option>
							</select>
						</div>
					</sec:ifAnyGranted>

				</div>

				<!-- Right Column -->
				<div class="col-xs-6 resize-formright"></div>
				<span class="clearfix"></span>

				<!-- Buttons -->
				<div class="col-xs-6">
					<g:submitButton class="form-btn btn-primary" name="save"
						value="${message(code:'default.field.buttonSave')}" />
					<g:link action="listAll" controller="employee" class="back-button">
						<span class="form-btn btn-primary"> ${message(code:'default.field.buttonCancel')}
						</span>
					</g:link>
				</div>
				<span class="clearfix"></span>
			</g:form>
		</div>
	</g:render>
</body>
</html>