
	<div class="col-xs-3">
		<label class="control-label" for="email"><g:message code="field.email" /> *</label>
	</div>
	<div class="col-xs-9">
		<g:field type="email" disabled="true" name="email"   value="${user?.email?.encodeAsHTML()}"/>		
	</div>
	<span class="clearfix"></span>

	<div class="col-xs-3">
		<label for="employeers"><g:message code="default.field.employeeFor" /> *</label>
	</div>
	<div class="col-xs-9">
		<div class="exxo-table-container-full-width">
			<table class='table input-table'>
				<g:each in="${employeers}" status="i" var="companyInstance">
					
					<g:if test="${(i % 2) == 0}">
						<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					</g:if>									
						  <td>
							<g:checkBox checked="${user.employeers().contains(companyInstance)}"  name="employeers" value="${companyInstance.id}" /> ${companyInstance?.name?.encodeAsHTML()}
						   </td>
						   
						  <g:if test="${(i % 2) != 0}">									 		 
							</tr>
						  </g:if>								  		
	
				 
					<g:if test="${(employeers.size()-1) == i}">
						</tr>		
					</g:if> 							
				</g:each>	
			</table>							
		</div>
	</div>
<span class="clearfix"></span>
<script>
Exxo.hideSpinner();
</script>