<g:set var="tableId" value="${(tableId != null)? tableId:'table-list'}" />
<g:set var="locationId" value="${(locationId != null)? locationId:0}" />

<table id="${tableId}" class="display" cellspacing="0" width="100%">
	<thead>
		<tr>
			<g:if test="${ orderEnabled }">
			<th>
				${message(code: 'default.location.contents.order')}
			</th>
			 </g:if>
			<th>
				${message(code: 'default.field.contentName')}
			</th>
			<th>
				${message(code: 'default.field.type')}
			</th>
			<th>
				${message(code: 'url')}
			</th>
			<th>
				${message(code: 'preview.files')}
			</th>
			<th>
				${message(code: 'default.field.expiration')}
			</th>
			<th>
				${message(code: 'default.field.expired')}
			</th>
			<th>
				${message(code: 'default.field.enabled')}
			</th>
			<th>
				${message(code: 'default.table.action')}
			</th>
		</tr>
	</thead>
	<tbody>
		<g:each in="${contentsList}" status="i" var="contentInstance">
            <g:set var="contentController" value="${ contentInstance.type == "offer"? 'offer':'content' }"/>
			<tr contentId="${contentInstance.id}" class="${(i % 2) == 0 ? 'even' : 'odd'}">
				
				<g:if test="${orderEnabled}">
					<td class="moveRows">
						<div class="hide">
							<a href="#" class="up"><img alt="no image" src="${resource(dir: 'themes/default/img', file: 'arrowUp.png')}"></a>					
	            			<a href="#" class="down"><img alt="no image" src="${resource(dir: 'themes/default/img', file: 'arrowDown.png')}"></a>
	            		</div>
					</td>
				 </g:if>				
				<td>
					<sec:ifAnyGranted roles="ROLE_HELP_DESK">
						<g:link controller="${contentController}" action="edit" id="${contentInstance.id}">
							${contentInstance.title.encodeAsHTML()}
						</g:link>
					</sec:ifAnyGranted>
					<sec:ifNotGranted roles="ROLE_HELP_DESK">
						<g:if test="${contentInstance.affiliate}">
							${contentInstance.title.encodeAsHTML()}
						</g:if>
						<g:else>
							<g:link controller="${contentController}" action="edit" id="${contentInstance.id}">
								${contentInstance.title.encodeAsHTML()}
							</g:link>
						</g:else>
					</sec:ifNotGranted>
				</td>
				<td>
					${message(code: 'content.message.type.' + contentInstance.type.encodeAsHTML())} 
				</td>
				<td>
					${contentInstance.url}	
				</td>
				<td>
					<g:if test="${contentInstance.featuredImage}">
						<a href="${contentInstance.featuredImage?.encodeAsHTML()}" data-lightbox="image-${ contentInstance.id }" 
							data-title="${message(code: 'featured.image')}">
							<!-- If there is a banner image, use this as thumbnail -->
							<g:if test="${contentInstance.featuredImage}">
							<img src="${contentInstance.featuredImage?.encodeAsHTML()}" width="64px" height="28px"/>
							</g:if>	
						</a>
					</g:if>
					
					<g:if test="${contentInstance.dialogImage}">
						<a href="${contentInstance.dialogImage?.encodeAsHTML()}" data-lightbox="image-${ contentInstance.id }" 
							data-title="${message(code: 'dialog.image')}">
							<!-- If there is not a banner image, use this as thumbnail -->
                            <g:if test="${!contentInstance.featuredImage}">
                            <img src="${contentInstance.dialogImage?.encodeAsHTML()}" width="32px" height="25px"/>
                            </g:if>	
						</a>	
					</g:if>
					
					<!-- Tablet Images -->
					<br/>
					<g:if test="${contentInstance.featuredImageTablet}">
						<a href="${contentInstance.featuredImageTablet?.encodeAsHTML()}" data-lightbox="image-${ contentInstance.id }" 
							data-title="${message(code: 'featured.image.tablet')}"></a>
					</g:if>
					
					<g:if test="${contentInstance.dialogImageTablet}">
						<a href="${contentInstance.dialogImageTablet?.encodeAsHTML()}" data-lightbox="image-${ contentInstance.id }" 
							data-title="${message(code: 'dialog.image.tablet')}"></a>	
					</g:if>
				</td>
				<td>
					<g:formatDate format="MM-dd-yyyy" date="${contentInstance.schedule?.endDate }"/>
				</td>				
				<td>
					<g:set var="today" value="${new Date()}" />
					<g:if test="${contentInstance.schedule?.endDate  == null}">
						<g:set var="expirationDate" value="${new Date() + 1}" />
					</g:if> <g:else>
						<g:set var="expirationDate" value="${contentInstance.schedule.endDate }" />
					</g:else>
					
					<g:if test="${expirationDate <= today}">
						<g:message code="default.field.yes" />
					</g:if> <g:else>
						<g:message code="default.field.no" />
					</g:else>
				</td>			
				<td><g:if test="${contentInstance.enabled == true}">
						<g:message code="default.field.yes" />
					</g:if> <g:else>
						<g:message code="default.field.no" />
					</g:else></td>				
				<td>
					<g:link controller="${contentController}" action="show" id="${contentInstance.id}"
						class="action-icons view"
						title="${message(code:'default.action.show')}">
					</g:link> 
					
					<sec:ifAnyGranted roles="ROLE_HELP_DESK">
						<g:link controller="${contentController}" action="edit"
							id="${contentInstance.id}" class="action-icons edit"
							title="${message(code:'default.action.edit')}">
						</g:link>
						<g:if test="${contentInstance.enabled}">
							<g:link controller="${contentController}" action="assignLocation" id="${contentInstance.id}"
								buttonEnable="true" class="action-icons viewLocation"
								title="${message(code:'default.action.management')}">
							</g:link>
						</g:if>
						<g:else>
							<g:link controller="${contentController}" action="assignLocation" id="${contentInstance.id}"
								buttonEnable="false" class="action-icons viewLocation-disable"
								title="${message(code:'default.action.management')}">
							</g:link>
						</g:else>
						<g:if test="${ locationId != 0 }">
							<g:link controller="${contentController}" action="unassignOfLocation" id="${contentInstance.id}" params="[locId: locationId]"
								class="action-icons unassign" title="${message(code:'unassign.coontent.of.location')}"></g:link>						
						</g:if>
						<g:link controller="${contentController}" action="delete" id="${contentInstance.id}"  class="action-icons delete" title="${message(code:'default.action.delete')}"></g:link>
					</sec:ifAnyGranted>
					<sec:ifNotGranted roles="ROLE_HELP_DESK">
						<g:if test="${contentInstance.affiliate == null}">
							<g:link controller="${contentController}" action="edit"
								id="${contentInstance.id}" class="action-icons edit"
								title="${message(code:'default.action.edit')}">
							</g:link>
							<g:if test="${contentInstance.enabled}">
								<g:link controller="${contentController}" action="assignLocation" id="${contentInstance.id}"
									buttonEnable="true" class="action-icons viewLocation"
									title="${message(code:'default.action.management')}">
								</g:link>
							</g:if>
							<g:else>
								<g:link controller="${contentController}" action="assignLocation" id="${contentInstance.id}"
									buttonEnable="false" class="action-icons viewLocation-disable"
									title="${message(code:'default.action.management')}">
								</g:link>
							</g:else>
						</g:if>
						<g:if test="${ locationId != 0 }">
							<g:link controller="${contentController}" action="unassignOfLocation" id="${contentInstance.id}" params="[locId: locationId]"
								class="action-icons unassign" title="${message(code:'unassign.coontent.of.location')}"></g:link>						
						</g:if>
						<g:if test="${contentInstance.affiliate == null}">
							<g:link controller="${contentController}" action="delete" id="${contentInstance.id}"  class="action-icons delete" title="${message(code:'default.action.delete')}"></g:link>
						</g:if>
					</sec:ifNotGranted>
				</td>
			</tr>
		</g:each>
	</tbody>
</table>