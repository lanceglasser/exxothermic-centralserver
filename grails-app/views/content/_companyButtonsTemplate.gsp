<g:each var="company" in="${companies}">
	<g:if test="${company.locations.size() > 0}">
	     <div class="company">
			<div class="header">
				<label>
					<g:checkBox name="allCompany_${company.id}" class="cbCompany" companyId="${company.id}"  value="${false}" /> 
					${company.name.encodeAsHTML()}
				</label>
				
				<div class="minus" id="minus_${company.id}" companyId="${company.id}"></div>
				<div class="plus" id="plus_${company.id}" companyId="${company.id}"></div>
				<span class="rowHelp"></span>
				<div class="clearfix"></div>
			</div>
			<div class="locationContainer" id="locationContainer_${company.id}" name="locationContainer_${company.id}">
				<g:each var="location" in="${company.locations.sort{a,b-> a.name.compareTo(b.name)}}" status="i">
					<g:if test="${location.enable}"> 
						<div class="location ${(i % 2) == 0 ? ' even' : ' odd'}"><label for="location_${location.id}">
								<g:checkBox name="location_${location.id}" class="cbLocation cb_${company.id}" locationId="${location.id}" companyId="${company.id}"  value="${false}" />${location.name.encodeAsHTML()}
							</label>
						</div>
						<div class="clearfix"></div>
					 </g:if>
				</g:each>
			</div>		
		</div>  
	</g:if>
	
</g:each>
<script>
	selectedLocations = "${selectedLocations}";
	Exxo.hideSpinner();
	//initializeEvents();
</script>
<script src="${resource(dir: 'js/pages/customButton', file: 'manageButtons.js')}"></script>