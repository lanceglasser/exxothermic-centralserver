<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.home.manageContent" /></title>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_custom_button_assign.js')}"></script>
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
<script
	src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
<script
	src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
<script>
	Exxo.module = "button-assign-location";
	Exxo.UI.vars['defaultButtonId'] = "${contentId}";
</script>
</head>
<body>
	<section class="introp">
		<div class="intro-body">
			<div class="container full-width">
				<div class="row">
					<div class="col-md-12" id="internal-content">
						<h1>
							${ message(code:'assign.general', args:[message(code:'default.home.banners')]) }
						</h1>
						<p class="muted"> <g:message code="default.assign.location.note" /></p>
						<sec:ifAnyGranted roles="ROLE_OWNER, ROLE_OWNER_STAFF">
						<p class="muted"><b><g:message code="associated.with.premium.companies.only" /></b></p>
						</sec:ifAnyGranted>

						<div class="content ">

							<g:render template="/layouts/messagesAndErrorsTemplate" />

							<g:form action="saveLocations" name="createForm" id="createForm" class="exxo-form">
							
							<div class="col-xs-12">
                                <div class="col-xs-3">
                                    <label class="control-label" for="user">
                                        <g:message code="default.home.manageContent" /> *
                                    </label>
                                </div>
                                <div class="col-xs-9">
                                    <div>
                                        <div class="col-xs-12">
		                                    <g:select name="buttons" from="${contentList}" optionKey="id" optionValue="title"
		                                        onchange="Exxo.Button.Assign.clearScrean();${remoteFunction(
		                                            action:'loadAssociatedLocations',
		                                            params:'\'buttonId=\' + escape(this.value)',
		                                            update : 'infor')}"/>
		                                </div>
		                                <span class="clearfix"></span>
                                    </div>
                                    <div>
                                        <div class="col-xs-12">
                                            <div class="exxo-table-container-full-width" id="infor"></div>
                                        </div>
                                        <span class="clearfix"></span>
                                    </div>
                                    <div>
                                        <div class="col-xs-12">
                                            <input type="hidden" id="locationsToSave" name="locationsToSave" value=""/>
		                                    <g:submitButton class="form-btn btn-primary" name="save" value="${message(code:'default.field.buttonSave')}"  onSubmit="Exxo.showSpinner();" />
		
		                                    <g:link action="listAll" controller="content" class="back-button">
		                                       <span class="form-btn btn-primary">
		                                          ${message(code:'default.field.buttonCancel')}
		                                       </span>
		                                    </g:link>
                                        </div>
                                        <span class="clearfix"></span>
                                    </div>
                                </div>
                            </div>
                            <span class="clearfix"></span>
							</g:form>

							<!-- Synchronize Button -->
							<g:form action="syncronizeContent" name="syncForm" id="syncForm" class="exxo-form">
								<div class="col-xs-12">
									<div class="col-xs-3">
									</div>
									<div class="col-xs-9">
										<input type="hidden" id="customButtonId" name="customButtonId" value="${ contentId }"/>
										<g:submitButton name="sync" class="form-btn btn-primary" value="${message(code:'default.field.buttonSyncronize')}" />
									</div>
								</div>
								<span class="clearfix"></span>
							</g:form>

							<!-- Information with the status of every transaction -->
							<g:render template="/content/contentsSyncResultTemplate" model="[jsonTransactionStatus: jsonTransactionStatus]" />

						</div> <!-- End of Content -->
					</div>
				</div>
			</div>
		</div>
	</section>
</body>
</html>