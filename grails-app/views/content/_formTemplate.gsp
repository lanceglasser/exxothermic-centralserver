<g:set var="isOffer" value="${(isOffer != null)? isOffer:false}" />
<g:set var="currentAppTab" value="${(isOffer != null)? 'OFFERS':'AUDIO'}" />
<script type="text/javascript">
	Exxo.UI.vars['is-offer'] = ${(isOffer)? 'true':'false'};
	Exxo.UI.vars['dialog-img'] = "${content?.dialogImage? content.dialogImage:''}"; 
	Exxo.UI.vars['banner-img'] = "${content?.featuredImage? content.featuredImage:''}";
	Exxo.UI.translates['select'] = "${message(code:'select')}";
	Exxo.UI.translates["of next day"] = "${message(code:'of.next.day')}";
	Exxo.UI.translates["schedule.time.final"] = "${message(code:'schedule.time.final')}";
</script>
<div class="col-xs-12">
	<div class="col-xs-4 resize-formleft">
		
		<h1>${ title }</h1>
		
		<p class="muted">
			${ message(code:'app.theme.help.text.2') }
		</p>
		<p class="muted">
			${ message(code:'banner.help.text.1') }
		</p>
		
		<div id="helpDiv" style="height: 30px; vertical-align: middle; line-height: 30px;" >
            <a href="#" class="view-imageHelp" cat="app" template="${(isOffer)? 'offer':'banner'}">
            	<img class="floatLeft" alt="No Image" src="${ resource(dir: 'themes/default/img', file: 'Help-Support.png') }">
            </a>
            <div class="noteHelp" >
	            <span class="liten-up floatLeft">
            	${message(code:'default.field.content.help.tab.note')}                         
            	</span>
           	</div>
		</div>
	</div>
	<div class="col-xs-8 resize-formleft">
		<!-- App Example Container -->
		<g:render template="/layouts/tags/appViewExampleTemplate" model="[offer: isOffer, currentAppTab: (isOffer)? 'OFFERS':'AUDIO']"/>
		<span class="clearfix"></span>
	</div>
</div>

<span class="clearfix"></span>
<br/>
<hr/>

<div class="col-xs-12">
<!-- Left Column -->
<div class="col-xs-6 resize-formleft">
	<!-- Title -->
	<custom:editField type="text" name="title"  max="100" required="${true}" value="${content.title}"
		label="title" />
	
	<g:if test="${ !isOffer }">
		<!-- Type of Welcome Message -->
		<div class="col-xs-4 text-to-right">
			<label for="type"><g:message code="default.field.type" /> *</label>
		</div>
		<div class="col-xs-8 text-to-left">
			<select name="type.id" id="type" tabindex="-1" title="Type" class="select2-offscreen">
				<option value="image" ${ content.type == 'image'? 'selected="selected"':'' }>${ message(code:'content.message.type.image') }</option>
				<option value="text" ${ content.type == 'text'? 'selected="selected"':'' }>${ message(code:'content.message.type.text') }</option>
			</select>
		</div>
		<span class="clearfix"></span>
	</g:if>	
	
	<!-- Description -->
	<span id="descriptionDiv">
	    <div class="col-xs-4 text-to-right">
	      <label for="description"> ${ message(code:'default.field.text') }
	      	<span id="descRequired" style="display: none;"> *</span>
	      </label>
	    </div>
	    <div class="col-xs-8 text-to-left">
	    	<textarea id="description" name="description" 
                rows="20" cols="25" maxlength="400">${ content.description? content.description:''}</textarea>
	    	<g:if test="${ !isOffer }">
	    		<span class="liten-up">${ message(code:'description.required.note') }</span>
	    	</g:if>
	    </div>
	</span>
	
	<!-- Color -->
	<custom:editField type="colorSelect" name="color" required="${true}" value="${content.color}" 
		label="default.field.customeButtons.channelColor"  number="1" note="${ message(code: 'default.contents.color.note')  }"/>
	<g:if test="${ isOffer }">
		<!-- isFile -->
		<custom:editField type="checkbox" name="isFile" required="${false}" value="${content.isFile}"
			label="default.field.isfile" cssclass="input-checkbox" tooltip="tooltip.customButton.isfile"/>
		<br/>
		<!-- RadioGroup -->		
		<span id="radioDiv" >
		            <div class="col-xs-4 text-to-right">
		              <label for="referenceValue"> ${ message(code: 'default.field.offer.referenceType')  }</label>
		            </div>
		            <div class="col-xs-8 text-to-left">
		            	<span id="radioLabelUrl" class="radioLabel" >URL</span> 
		            		<input type="radio" name="referenceType" 
		            			<g:if test="${ content?.referenceType == 'URL'}">
		            				 checked="checked"
		            			</g:if>
		            			value="URL" id="radioURL" />			
						<span id="radioLabelPdf" class="radioLabel" >PDF</span> 
							<input type="radio" name="referenceType"  
								<g:if test="${ content?.referenceType != 'URL'}">
		            				 checked="checked"
		            			</g:if>
		            			 value="PDF" id="radioPDF" />
	
					</div>
		</span>
		<span class='clearfix'></span>
		<!-- File Selector -->
		<g:render template="/layouts/tags/fileUploadSelectorTemplate" 
			model="[
				id:				'file',
				label:			message(code:'default.field.file'),
				note:			message(code: 'invalid.document.message', args:['5MB']),
				accept:			'application/pdf'
				]"
		/>
		
		<g:if test="${ content?.referenceType == 'PDF' && content?.url != null && content?.url != '' }">
			<div id="filePreview">	
				<div class="col-xs-4 text-to-right">
				</div>
				<div class="col-xs-8 text-to-left">
					<a href="${ content.url }" class="view-pdf">${ message(code:'preview.current.document') }</a>
				</div>
				<span class="clearfix"></span>
			</div>
		</g:if>
	</g:if>	
	
	<!-- Url -->
	<g:if test="${ content?.referenceType == 'URL' || !isOffer}">			   
		<g:set var="urlField" value="${content.url}" />
	</g:if>
	<g:else>
		<g:set var="urlField" value="" />
	</g:else>
	
	<custom:editField type="url" name="urlReference" required="${false}" value="${urlField}"
		label="url"
		note="${ message(code: 'default.field.example') + " " + message(code: 'default.customButtons.context.example') }"/>
		
	<!-- Enable -->
	<custom:editField type="checkbox" name="enabled" required="${false}" value="${content.enabled}"
		label="default.field.enabled" cssclass="input-checkbox" tooltip="tooltip.customButton.featured"/>
		
	<!-- Scheduler -->
	<div class="col-xs-12 text-to-left">
		<div class="col-xs-4 text-to-right">
			<label for="myschedule"><g:message code="schedule.label" /> *</label>
		</div>
		<div class="col-xs-8 text-to-left">
			<g:if test="${content.schedule}">
				<custom:scheduler name="content" days="${content.schedule.days}" hours="${content.schedule.hours}" start="${content.schedule.startDate}" end="${content.schedule.endDate}"/>
			</g:if>
			<g:else>
				<custom:scheduler name="content"/>
			</g:else>
		</div>
	</div>
	
	<sec:ifAnyGranted roles="ROLE_HELP_DESK, ROLE_ADMIN">
	
		<div id="default-options-container" style="display: none;">
			<div class="col-xs-4 text-to-right">
			</div>
			<div class="col-xs-8 text-to-left">
				<span class="liten-up" style="text-align: justify;">
					${ message(code:'default.content.for.locations.explain') }
				</span>
			</div>
			<span class="clearfix"></span>
		
		
			<!-- isDefaultOfLocation -->
			<custom:editField type="checkbox" name="isDefaultOfLocation" required="${false}" value="${content.isDefaultOfLocation}"
				label="isDefaultOfLocation" cssclass="input-checkbox" tooltip="isDefaultOfLocation.tooltip"/>
				
			<!-- Associate Affiliate -->
			<div id="select-default-client">
				<div class="col-xs-4 text-to-right">
					<label for="affiliate">
						<div class="questionToolTip" title="${ message(code:'default.content.client.tooltip') }"></div>
						<g:message code="default.field.affiliate" />
					</label>
				</div>
				<div class="col-xs-8 text-to-left">
					<g:select name="affiliate.id" id="affiliate" 
							from="${affiliates}" optionKey="id" optionValue="name"
							value="${content.affiliate?.id}"
							noSelection="${['null':'']}" />
				</div>
				<span class="clearfix"></span>
			</div>
		</div>
	</sec:ifAnyGranted>
</div>
<!-- End of Left Column -->
<!-- Right Column -->
<div class="col-xs-6 resize-formright">
	<!-- Featured -->
	<g:if test="${ isOffer }">
	<custom:editField type="checkbox" name="featured" required="${false}" value="${content.featured}"
		label="default.field.featured" cssclass="input-checkbox" tooltip="tooltip.customButton.featured"/>
	</g:if>
	
	<!-- Feature Image -->	
	<span id="featuredImageObject">
		<g:render template="/layouts/tags/imageUploadSelectorTemplate" 
			model="[
				id:				'featuredImageFile',
				ref:			'1',
				demoId:			'app-skin-background-img',
				demoWidth:		'270',
				demoHeight:		'118',
				demoId2:		'gen-dialog-img',
				demoWidth2:		'270',
				demoHeight2:	'118',
				demoId3:		isOffer? 'offer-gen-thumbnail-img':'',
				demoWidth3:		'90',
				demoHeight3:	'46',
				imgValue: 		content.featuredImage,
				label:			message(code:'featured.image'),
				cropImage:		true,
				minWidth: 		mycentralserver.utils.Constants.FEATURED_CONTENT_IMAGE_WIDTH,
				minHeight: 		mycentralserver.utils.Constants.FEATURED_CONTENT_IMAGE_HEIGHT,
				cropWidth:		mycentralserver.utils.Constants.FEATURED_CONTENT_CROP_WIDTH,
				cropHeight:		mycentralserver.utils.Constants.FEATURED_CONTENT_CROP_HEIGHT
				]"
		/>
		
		<!-- GenDialogImages -->
		<custom:editField type="checkbox" name="genDialogImages" required="${false}" value="${content.genDialogImages}"
			label="auto.generate.dialog.images" cssclass="input-checkbox"
			rightColumnPosition="right" leftColumnPosition="right" leftColumnSize="10"/>
		
		<!-- Feature Image for Tablet -->
		<div id="dialogImagesContainer" style="${ content.genDialogImages? 'display: none;':''}">
			<g:render template="/layouts/tags/imageUploadSelectorTemplate" 
				model="[
					id:				'dialogImageFile',
					ref:			'3',
					imgValue: 		content.dialogImage,
					label:			message(code:'dialog.image'),
					demoId:			(isOffer)? 'offer-demo-thumbnail-img':'',
					demoWidth:		'90',
					demoHeight:		'60',
					demoId2:		'dialog-img',
					demoWidth2:		'270',
					demoHeight2:	'210',
					cropImage:		true,
					minWidth: 		mycentralserver.utils.Constants.DIALOG_CONTENT_IMAGE_WIDTH,
					minHeight: 		mycentralserver.utils.Constants.DIALOG_CONTENT_IMAGE_HEIGHT,
					cropWidth:		mycentralserver.utils.Constants.DIALOG_CONTENT_CROP_WIDTH,
					cropHeight:		mycentralserver.utils.Constants.DIALOG_CONTENT_CROP_HEIGHT
					]"
			/>
			
			<!-- GenFeaturedImgForTablets -->
			<custom:editField type="checkbox" name="genDialogImgForTablets" required="${false}" value="${content.genDialogImgForTablets}"
				label="auto.generate.image.for.tablets" cssclass="input-checkbox" 
				rightColumnPosition="right" leftColumnPosition="right" leftColumnSize="10"/>
					
			<!-- Dialog Image for Tablet -->
			<span id="dialogImageTabletObject" style="${ content.genDialogImgForTablets? 'display: none;':''}">				
				<g:render template="/layouts/tags/imageUploadSelectorTemplate" 
					model="[
						id:				'dialogImageFileTablet',
						ref:			'4',
						imgValue: 		content.dialogImageTablet,
						label:			message(code:'dialog.image.tablet'),
						cropImage:		true,
						minWidth: 		mycentralserver.utils.Constants.DIALOG_CONTENT_IMAGE_TABLET_WIDTH,
						minHeight: 		mycentralserver.utils.Constants.DIALOG_CONTENT_IMAGE_TABLET_HEIGHT,
						cropWidth:		mycentralserver.utils.Constants.DIALOG_CONTENT_CROP_TABLET_WIDTH,
						cropHeight:		mycentralserver.utils.Constants.DIALOG_CONTENT_CROP_TABLET_HEIGHT
						]"
				/>
			</span>
			
			<custom:editField type="dialogGenExample" name="dialogGenExample" required="${false}" value="" label=""
				src="${resource(dir: 'themes/default/img', file: '640x500To924x462.png')}" visible="${ content.genDialogImgForTablets }"/>
			
		</div>
		
		<custom:editField type="dialogImgsGenExample" name="dialogImgsGenExample" required="${false}" value="" label=""
			src="${resource(dir: 'themes/default/img', file: '640x280To640x500.png')}" visible="${ content.genDialogImages }"/>
				
	</span>
</div>
<span class="clearfix"></span>

</div>