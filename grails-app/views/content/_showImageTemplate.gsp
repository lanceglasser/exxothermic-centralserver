<div class="col-xs-6 text-to-right">
	<label>
		${ label }
	</label>
</div>				
<div class="col-xs-6 text-to-left">
	<g:if test="${ image }">
		<a href="${image?.encodeAsHTML()}" data-lightbox="image-${ id }" 
						data-title="${ note }">
			<i><img class="ad-background-img" src="${image}"></i>
			<span class="liten-up">${ message(code:'preview.image.on.click.note') }</span>				
		</a>
	</g:if>
	<g:else>
		---
	</g:else>
</div>
<span class="clearfix"></span>