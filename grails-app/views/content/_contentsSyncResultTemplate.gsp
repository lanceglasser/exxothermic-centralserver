<g:if test="${jsonTransactionStatus}">
	<div id="syncResultContainer">
		<br />
		<br />
		<legend>
			<g:message code="default.title.statusSendCustomButton" />
		</legend>
		<table id="table-list" class="display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>
						${message(code:'default.field.serial')}
					</th>
					<th>
						${message(code:'default.field.status')}
					</th>
					<th>
						${message(code:'default.field.errorDescription')}
					</th>
					<th>
						${message(code:'contents')}
					</th>
					<th>
						${message(code:'default.field.content.sent')}						
					</th>
					<th>
						${message(code:'default.field.content.limit')}
					</th>					
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th>
						${message(code:'default.field.serial')}
					</th>
					<th>
						${message(code:'default.field.status')}
					</th>
					<th>
						${message(code:'default.field.errorDescription')}
					</th>
					<th>
						${message(code:'contents')}
					</th>
					<th>
						${message(code:'default.field.content.sent')}						
					</th>
					<th>
						${message(code:'default.field.content.limit')}
					</th>
				</tr>
			</tfoot>
			<tbody>
				<g:each in="${jsonTransactionStatus?.contentsStatus}" status="i"
					var="record">
					<tr id="${record.channelNum}"
						class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<td>
							${record.serialNumber}
						</td>
						<td>
							${message(code:'boxes.sync.result.status.'+record.successful)}
						</td>
						<td>
							${message(code:messageErrorDescriptionHash.get(record.errorCode))}
						</td>
						<td>
							<ul>
								<g:each in="${record.contentIds}" var="buttonId">
									<il>
									${contentsHashInfo.get(buttonId)}&#44;&#32;</il>
								</g:each>
							</ul>
						</td>
						<td>
							<g:if test="${ record.successful}">
								${record.contentIds.size()}
							</g:if>
							<g:else>
								0 
							</g:else>
						</td>
						<td>
							${boxLocationData.get(record.serialNumber).contentsLimit}
						</td>
					</tr>
				</g:each>
			</tbody>
		</table>
	</div>
</g:if>