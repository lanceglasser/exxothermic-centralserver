<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title><g:message code="default.title.listContent" /></title>
		<link rel="stylesheet" type="text/css"
			href="${resource(dir: 'themes/default/css', file: 'lightbox.css')}">
		<script
			src="${resource(dir: 'themes/default/js', file: 'lightbox.min.js')}"></script>
		<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
		<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
		<script src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
		
		<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
		
		<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_content_list.js')}"></script>  
		<script>
			Exxo.module = "content-list";
			Exxo.UI.vars["page-code"] = "content-list";
			Exxo.Tables.loadPaginationInitValuesFromString('<custom:paginationInfo code="content-list"/>');
			Exxo.UI.translates['okButton'] = "${message(code:'default.field.buttonOk')}";
			Exxo.UI.translates['cancelButton'] = "${message(code:'default.field.buttonCancel')}";
			Exxo.UI.translates['disableMessage'] = "${message(code:'mycentralserver.custombuttons.CustomButton.isDisabled')}";
		</script>
		</head>		
	</head>
	<body>
		<section class="introp">
		  <div class="intro-body">
		    <div class="container full-width">
		      <div class="row">
		        <div class="col-md-12" id="internal-content">
		          	<h1>
		          		<g:message code="default.title.listContent" />
		          		<p class="muted"> ${ message(code:'click.on.thumbnails.for.preview') }</p>
		          	</h1>
					
					<g:render template="/layouts/messagesAndErrorsTemplate" />
					
					<div class="content">
						<g:if test="${contentsList.size()>0}">
			            	<g:render template="/content/contentsTableTemplate" model="[contentsList: contentsList]"/>
			            </g:if>
		            
		            	<!-- Information with the status of every transaction -->
						<g:render template="/content/contentsSyncResultTemplate" model="[jsonTransactionStatus: jsonTransactionStatus]" />
							
		          	</div>
		        </div>
		      </div>
		    </div>
		  </div>
		</section>	
    <div id="content-disable-dialog" title="${message(code:'mycentralserver.custombuttons.CustomButton.isDisabled.title')}">
	  <p>${message(code:'mycentralserver.custombuttons.CustomButton.isDisabled')}</p>
	</div>
<div id="confirm-delete-dialog" title="${message(code:'default.confirm.title')}" style="display:none;">
  ${message(code:'general.object.confirm.delete.male', args:[message(code:'banner')])}
</div>
<div id="confirm-unassign-dialog" title="${message(code:'default.confirm.title')}" style="display:none;">
${ message(code:'confirm.unassign.content.of.location') }
</div>
	</body>
</html>