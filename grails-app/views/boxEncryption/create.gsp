<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title><g:message code="default.title.registerBox" /></title>
		<link rel="stylesheet" href="${resource(dir: 'css/pluggins/select2', file: 'select2.css')}" type="text/css">
		<script src="${resource(dir: 'js/pluggins/select2', file: 'select2.js')}"></script>
		<script src="${resource(dir: 'js/pages/box', file: 'register.js')}"></script>
	</head>
	<body>
		<g:form action="save" name="registerBoxForm" class="form-horizontal">
		 <fieldset>  
          <legend><g:message code="default.title.registerPublicKey" /></legend>
          
          
          <g:if test="${flash.error}">
	        		<div class="alert alert-error">  
					  <a class="close" data-dismiss="alert">×</a>  
					  ${flash.error} 
					  <g:hasErrors bean="${boxEncryption}">
					  		<ul>
								<g:eachError bean="${boxEncryption}" var="error">
									<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
								</g:eachError>
							</ul>
					  </g:hasErrors>	
					    ${flash.error=null}
					</div> 

      	 </g:if>          
      		 <g:if test="${flash.success}">
	        		<div class="alert alert-success">  
					  <a class="close" data-dismiss="alert">×</a>  
					  ${flash.success} 
					  ${flash.success=null} 
					</div> 

      		</g:if>
			
			
          	<div class="control-group">
				<label class="control-label" for="serial"  ><g:message code="default.field.serial" />*</label>
				<div class="controls">
					<g:textField  class="input-xlarge"  required="required" maxlength="50" name="serial" value="${boxEncryption.serial}" />
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" for="name"  ><g:message code="default.field.publicKeyExponent" />*</label>
				<div class="controls">
					<g:textField  class="input-xlarge"  required="required" maxlength="50" name="publicKeyExponent" value="" />
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" for="name"  ><g:message code="default.field.publicKeyModule" />*</label>
				<div class="controls">
					<g:textField  class="input-xlarge"  required="required" maxlength="50" name="publicKeyModule" value="" />
				</div>
			</div>			
		
			<div class="form-actions">
				<g:submitButton class="btn btn-primary" name="register" value="${message(code:'default.field.buttonRegister')}" />
				
				<g:link action="index" controller="home" class="back-button">        
			       <span class="btn btn-primary">
			          ${message(code:'default.field.buttonCancel')}
			       </span>       
		    	</g:link>	
			</div>  
          </fieldset>
          </g:form>          
	</body>
</html>
