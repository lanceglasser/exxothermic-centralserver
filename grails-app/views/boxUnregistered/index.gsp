<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main" />
		<title><g:message code="default.title.unregisteredBox" /></title>
		<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
		<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
		<script src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
		<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
		<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_box_list.js')}"></script>
		<script>
			Exxo.module = "box-list";
			Exxo.UI.vars["page-code"] = "exxtractors-unregistered-list";
			Exxo.Tables.loadPaginationInitValuesFromString('<custom:paginationInfo code="exxtractors-unregistered-list"/>');
			Exxo.UI.translates['okButton'] = "${message(code:'default.field.buttonOk')}";
			Exxo.UI.translates['cancelButton'] = "${message(code:'default.field.buttonCancel')}";
			Exxo.UI.translates['disableMessage'] = "${message(code:'mycentralserver.box.resetBox')}";
		</script>
		</head>
	<body>
	<section class="introp">
	  <div class="intro-body">
	    <div class="container full-width">
	      <div class="row">
	        <div class="col-md-12" id="internal-content">
	          <h1><g:message code="default.title.unregisteredBox" /></h1>
				<g:render template="/layouts/messagesAndErrorsTemplate"/>
			<div class="content">
				<g:if test="${boxes.size()>0}">
	            <table id="table-list" class="display" cellspacing="0" width="100%">
	              <thead>
	                <tr>
						<th>${message(code: 'default.field.serial')}</th>								
						<th>${message(code: 'default.field.version')}</th>
						<th>${message(code: 'default.field.updateDate')}</th>
						<th>${message(code: 'default.table.action')}</th>
	                </tr>
	              </thead>
	              <tbody>
	              	<g:each in="${boxes}" status="i" var="boxInstance">
						<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
							<td>${boxInstance.serial}</td>
							<td>
								${boxInstance.versionSoftwareLabel? boxInstance.versionSoftwareLabel.encodeAsHTML()
									:message(code:'default.field.empty')}
							</td>
		
							<td>
								<g:if test="${boxInstance.connected() == true}">
									<i class="state stateConnected"></i>
									<span class="greenText">
										${boxInstance.lastConnected(message(code:'default.text.messageFormat'))}
									</span>
								</g:if> <g:else>
									<i class="state stateDisconnected"></i>
									<span class="redText">
										${boxInstance.lastConnected(message(code:'default.text.messageFormat'))}
									</span>
								</g:else>
							</td>
		
							<td>		
								<sec:ifAnyGranted roles="ROLE_ADMIN, ROLE_HELP_DESK">							
									<g:link action="removeBox" id="${boxInstance.id}" class="action-icons delete" title="${message(code:'default.action.delete')}"></g:link>
									<g:if test="${boxInstance.connected() == true}">
										<g:link controller="box" action="listLogFiles"
			                               id="${boxInstance.id}" class="action-icons listInformation"
			                               title="${message(code:'default.action.show.logFile')}">
			                            </g:link>
		                            </g:if>
								</sec:ifAnyGranted> 													
							</td>
						</tr>
					</g:each>
	              </tbody>
	            </table>
	            </g:if>
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	</section>
	<div id="confirm-retire-dialog"
		title="${message(code:'mycentralserver.box.retireBox.title')}"
		style="display: none;">
		${message(code:'mycentralserver.box.retireBox')}
	</div>
	
	<div id="confirm-reset-dialog"
		title="${message(code:'mycentralserver.box.resetBox.title')}"
		style="display: none;">
		${message(code:'mycentralserver.box.resetBox')}
	</div>
	</body>
</html>