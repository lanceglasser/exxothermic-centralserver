<!-- Left Column -->
<div class="col-xs-6 resize-formleft">

	<!-- Name  -->
	<custom:editField type="text" name="name" required="${true}"
		value="${entity.name}" label="default.field.name" max="250" />
		
	<!-- Manufacturer  -->
	<custom:editField type="text" name="manufacturer" required="${true}"
		value="${entity.manufacturer}" label="manufacturer" max="250" />
		
	<!-- Build Fingerprint  -->
	<custom:editField type="text" name="buildFingerprint" required="${true}"
		value="${entity.buildFingerprint}" label="buildFingerprint" max="250"/>
		
	<!-- useSmallerPackages -->
	<custom:editField type="checkbox" name="useSmallerPackages" required="${false}" value="${entity.useSmallerPackages}"
		label="useSmallerPackages" cssclass="input-checkbox" tooltip="tooltip.useSmallerPackages"/>
			
	<br />
	
	<!--  Buttons  -->
	<g:submitButton class="form-btn btn-primary" name="save"
		value="${message(code:'default.field.buttonSave')}" />
	<g:link action="index" controller="device" class="back-button">
		<span class="form-btn btn-primary"> ${message(code:'default.field.buttonCancel')}
		</span>
	</g:link>
	
	<span class="clearfix"></span>
</div>