<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<script>
	Exxo.module = "device-create";
</script>
</head>
<body>
<g:render template="/layouts/internalContentTemplate">
	<h1>
		<g:message code="menu.device.create" />
	</h1>
	
	<div class="content">
		<g:render template="/layouts/messagesAndErrorsTemplate" model="[entity: entity]"/>
		
		<g:uploadForm action="save" id="createForm" name="createForm" class="exxo-form">
			<g:render template="form"></g:render>
		</g:uploadForm>
	</div>
</g:render>
</body>
</html>