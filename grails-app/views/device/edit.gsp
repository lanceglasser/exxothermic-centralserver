<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<script>
	Exxo.module = "device-create";
</script>
</head>
<body>
<g:render template="/layouts/internalContentTemplate">
	<h1>
		<g:message code="update.device.title" />
	</h1>
	
	<div class="content">
		<g:render template="/layouts/messagesAndErrorsTemplate" model="[entity: entity]"/>
		
		<g:uploadForm action="update" name="createForm" class="exxo-form">
			<g:hiddenField name="id" value="${entity?.id}" />
			<g:render template="form"></g:render>
		</g:uploadForm>
	</div>
</g:render>
</body>
</html>