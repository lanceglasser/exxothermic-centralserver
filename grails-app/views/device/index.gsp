<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
<script
	src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
<script
	src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
<script
	src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_content_list.js')}"></script>
<script>
	Exxo.module = "devices-list";
	Exxo.UI.vars["page-code"] = "devices-list";
	Exxo.Tables.loadPaginationInitValuesFromString('<custom:paginationInfo code="doc-categories-list"/>');
	Exxo.UI.translates['okButton'] = "${message(code:'default.field.buttonOk')}";
	Exxo.UI.translates['cancelButton'] = "${message(code:'default.field.buttonCancel')}";
	Exxo.UI.translates['disableMessage'] = "${message(code:'mycentralserver.custombuttons.CustomButton.isDisabled')}";
</script>
</head>
</head>
<body>
	<g:render template="/layouts/internalContentTemplate">
		<h1>
			<g:message code="devices.list.title" />
		</h1>

		<div class="content">
			<g:render template="/layouts/messagesAndErrorsTemplate" />

			<table id="table-list" class="display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>
							${message(code: 'default.field.name')}
						</th>
						<th>
							${message(code: 'manufacturer')}
						</th>
						<th>
							${message(code: 'buildFingerprint')}
						</th>
						<th>
							${ message(code:'useSmallerPackages') }
						</th>
						<th>
							${message(code: 'default.table.action')}
						</th>
					</tr>
				</thead>
				<tbody>
					<g:each in="${devices}" status="i" var="entity">
						<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
							<td><g:link action="edit" id="${entity.id}">
									${entity.name.encodeAsHTML()}
								</g:link></td>
							<td>${ entity.manufacturer.encodeAsHTML() }</td>
							<td>${ entity.buildFingerprint.encodeAsHTML() }</td>
							<td>
								<g:if test="${ entity.useSmallerPackages == true}">
									<g:message code="default.field.yes" />
								</g:if> <g:else>
									<g:message code="default.field.no" />
								</g:else>
							</td>
							<td><g:link action="show" id="${entity.id}"
									class="action-icons view"
									title="${message(code:'default.action.show')}"></g:link> <g:link
									action="edit" id="${entity.id}" class="action-icons edit"
									title="${message(code:'default.action.edit')}"></g:link> <g:link
									action="delete" id="${entity.id}" class="action-icons delete"
									title="${message(code:'default.action.delete')}"></g:link></td>
						</tr>
					</g:each>
				</tbody>
			</table>
		</div>
	</g:render>
	<div id="confirm-delete-dialog"
		title="${message(code:'default.confirm.title')}"
		style="display: none;">
		${message(code:'general.object.confirm.delete.male', args:[message(code:'device')])}
	</div>
</body>
</html>