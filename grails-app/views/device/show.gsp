<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<script>
	Exxo.module = "device-show";
</script>

</head>
<body>
	<g:render template="/layouts/internalContentTemplate">
		<h1>
			${entity.name?.encodeAsHTML()}
		</h1>

		<div class="content form-info">
			<g:render template="/layouts/messagesAndErrorsTemplate"
				model="[entity: entity]" />

			<!-- Left Column -->
			<div class="col-xs-8 resize-formleft">
				<custom:editField name="name"
					value="${entity.name ? entity.name.encodeAsHTML() : ''}"
					label="default.field.name" leftColumnSize="6" />
			</div>

			<div class="col-xs-8 resize-formleft">
				<custom:editField name="manufacturer"
					value="${entity.manufacturer ? entity.manufacturer.encodeAsHTML() : ''}"
					label="manufacturer" leftColumnSize="6" />
			</div>

			<div class="col-xs-8 resize-formleft">
				<custom:editField name="buildFingerprint"
					value="${entity.buildFingerprint ? entity.buildFingerprint.encodeAsHTML() : ''}"
					label="buildFingerprint" leftColumnSize="6" />
			</div>
			
			<!-- useSmallerPackages -->
			<div class="col-xs-8 resize-formleft">
				<custom:editField name="useSmallerPackages"
					value="${(entity.useSmallerPackages == true) ? message(code:"default.field.yes") : message(code:"default.field.no")}"
					label="useSmallerPackages" leftColumnSize="6"/>
			</div>
			
			<span class="clearfix"></span>

			<!-- Buttons -->
			<g:link action="index" controller="device"
				class="form-btn btn-primary">
				${message(code:'default.field.buttonBack')}
			</g:link>
		</div>
	</g:render>
</body>
</html>