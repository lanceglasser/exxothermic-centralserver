<g:if test="${jsonTransactionStatus}">
	<div id="syncResultContainer">
		<br />
		<br />
		<legend>
			<g:message code="default.title.statusSendCustomButton" />
		</legend>
		<table id="table-list" class="display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>
						${message(code:'default.field.serial')}
					</th>
					<th>
						${message(code:'default.field.status')}
					</th>
					<th>
						${message(code:'default.field.errorDescription')}
					</th>
					<th>
						${message(code:'offers')}
					</th>
					<th>
						${message(code:'contents')}
					</th>
					<th>
						${message(code:'documents')}
					</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th>
						${message(code:'default.field.serial')}
					</th>
					<th>
						${message(code:'default.field.status')}
					</th>
					<th>
						${message(code:'default.field.errorDescription')}
					</th>
					<th>
						${message(code:'offers')}
					</th>
					<th>
						${message(code:'contents')}
					</th>
					<th>
						${message(code:'documents')}
					</th>
				</tr>
			</tfoot>
			<tbody>
				<g:each in="${jsonTransactionStatus?.contentsStatus}" status="i"
					var="record">
					<tr id="${record.channelNum}"
						class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<td>
							${record.serialNumber}
						</td>
						<td>
							${message(code:'boxes.sync.result.status.'+record.successful)}
						</td>
						<td>
							${message(code:messageErrorDescriptionHash.get(record.errorCode))}
						</td>
						<td>
							<ul style="padding-left: 0;">
								<g:each in="${record.contentIds}" var="buttonId">
									<il>${contentsHashInfo.get(buttonId)}, </il>
								</g:each>
							</ul>
						</td>
						<td>
							<ul style="padding-left: 0;">
								<g:each in="${record.contents}" var="contentId">
									<il>
									${contentsHashInfo.get(contentId)}, </il>
								</g:each>
							</ul>
						</td>
						<td>
							<ul style="padding-left: 0;">
								<g:each in="${record.documents}" var="documentId">
									<il>${documentsHashInfo.get(documentId.toString())?
										documentsHashInfo.get(documentId.toString()):documentsHashInfo.get('O'+documentId.toString())}, </il>
								</g:each>
							</ul>
						</td>
					</tr>
				</g:each>
			</tbody>
		</table>
	</div>
</g:if>