<!-- Left Column -->
<div class="col-xs-6 resize-formleft">
	<!-- Type -->
	<custom:editField name="type"
			value="${message(code: 'content.message.type.' + entity.type.encodeAsHTML())}"
			label="default.field.type" leftColumnSize="6" />
	
	<!-- Enabled -->		
	<custom:editField name="enabled"
		value="${(entity.enabled == true) ? message(code:"default.field.yes") : message(code:"default.field.no")}"
		label="default.field.enabled" leftColumnSize="6"/>
	
	<!-- Description -->
	<custom:editField name="type"
		value="${ entity.description != null? entity.description.encodeAsHTML():'- - - -' }"
		label="default.field.description" leftColumnSize="6" />
			
	<g:if test="${ entity.type.encodeAsHTML() == 'text' }">
	</g:if>
	<g:else>
		<g:if test="${ entity.type.encodeAsHTML() == 'image' }">
			<!-- Url -->
			<custom:editField name="url"
				value="${ entity.url.encodeAsHTML() }"
				label="url" leftColumnSize="6" />
				
			<!-- Color -->
			<div class="col-xs-6 text-to-right">
				<label>
					<g:message code="default.field.customeButtons.channelColor" />
				</label>
			</div>				
			<div class="col-xs-6 text-to-left">			
				<i class="channel-color" style="background-color: ${entity.color}"></i>
			</div>
			<span class="clearfix"></span>
					
		</g:if>
		<g:else>
			<g:if test="${ entity.featured }">
				<!-- Url -->
				<custom:editField name="url"
					value="${ entity.url.encodeAsHTML() }"
					label="url" leftColumnSize="6" />
			</g:if>
							
			<!-- Color -->
			<div class="col-xs-6 text-to-right">
				<label>
					<g:message code="default.field.customeButtons.channelColor" />
				</label>
			</div>				
			<div class="col-xs-6 text-to-left">			
				<i class="channel-color" style="background-color: ${entity.color}"></i>
			</div>
			<span class="clearfix"></span>
			
		</g:else>
		
	</g:else>
	
	<!-- Scheduler -->
	<div class="col-xs-12 text-to-left">
		<div class="col-xs-6 text-to-right">
			<label for="myschedule"><g:message code="scheduler" /></label>
		</div>
		<div class="col-xs-6 text-to-left">
			<g:if test="${entity.schedule}">
				<custom:scheduler name="content" allowEdit="false" days="${entity.schedule.days}" hours="${entity.schedule.hours}" start="${entity.schedule.startDate}" end="${entity.schedule.endDate}"/>
			</g:if>
			<g:else>
				<custom:scheduler name="content"/>
			</g:else>
		</div>
	</div>
	
</div>
<!-- Right Column -->
<div class="col-xs-6 resize-formright">
	<g:if test="${ entity.type.encodeAsHTML() == 'text' }">
	</g:if>
	<g:else>
		<g:if test="${ entity.type.encodeAsHTML() == 'image' }">
			<div class="col-xs-6 text-to-right">
                <label>
                    ${message(code:'phones.images')}
                </label>
            </div>
            <span class="clearfix"></span>
            
			<g:render template="/content/showImageTemplate" 
				model="[
					label: 	message(code:'featured'),
					id: 	'featured',
					image: 	entity.featuredImage,
					note:	message(code:'featured.image')
					]"/>
			
			<g:render template="/content/showImageTemplate" 
				model="[
					label: 	message(code:'dialog'),
					id: 	'featured',
					image: 	entity.dialogImage,
					note:	message(code:'dialog.image')
					]"/>
			
			<div class="col-xs-6 text-to-right">
				<label>
					${message(code:'tablets.images')}
				</label>
			</div>
			<span class="clearfix"></span>
			<g:render template="/content/showImageTemplate" 
				model="[
					label: 	message(code:'featured'),
					id: 	'featured',
					image: 	entity.featuredImageTablet,
					note:	message(code:'featured.image.tablet')
					]"/>
			
			<g:render template="/content/showImageTemplate" 
				model="[
					label: 	message(code:'dialog'),
					id: 	'featured',
					image: 	entity.dialogImageTablet,
					note:	message(code:'dialog.image.tablet')
					]"/>	
		</g:if>
		<g:else>
			<!-- Featured -->		
			<custom:editField name="featured"
				value="${(entity.featured == true) ? message(code:"default.field.yes") : message(code:"default.field.no")}"
				label="default.field.featured" leftColumnSize="6"/>
			
			<div class="col-xs-6 text-to-right">
                <label>
                    ${message(code:'phones.images')}
                </label>
            </div>
            <span class="clearfix"></span>
            			
			<g:if test="${ entity.featured }">
				<g:render template="/content/showImageTemplate" 
					model="[
						label: 	message(code:'featured'),
						id: 	'featured',
						image: 	entity.featuredImage,
						note:	message(code:'featured.image')
						]"/>
			</g:if>
			
			<g:render template="/content/showImageTemplate" 
				model="[
					label: 	message(code:'dialog'),
					id: 	'featured',
					image: 	entity.dialogImage,
					note:	message(code:'dialog.image')
					]"/>
			
			<div class="col-xs-6 text-to-right">
				<label>
					${message(code:'tablets.images')}
				</label>
			</div>
			<span class="clearfix"></span>
			
			<g:if test="${ entity.featured }">
				<g:render template="/content/showImageTemplate" 
					model="[
						label: 	message(code:'featured'),
						id: 	'featured',
						image: 	entity.featuredImageTablet,
						note:	message(code:'featured.image.tablet')
						]"/>
			</g:if>
			
			<g:render template="/content/showImageTemplate" 
				model="[
					label: 	message(code:'dialog'),
					id: 	'featured',
					image: 	entity.dialogImageTablet,
					note:	message(code:'dialog.image.tablet')
					]"/>
		</g:else>
		
	</g:else>
</div>
<span class="clearfix"></span>