<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.listContent" /></title>
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'lightbox.css')}">
<script
	src="${resource(dir: 'themes/default/js', file: 'lightbox.min.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_custom_button_create.js')}"></script>
<script> 
    Exxo.UI.translates["of next day"] = "${message(code:'of.next.day')}";
    Exxo.UI.translates["schedule.time.final"] = "${message(code:'schedule.time.final')}";
    Exxo.UI.vars["timeConfiguration"] = "${ (content?.schedule?.hours)? content?.schedule?.hours:''}";
	Exxo.module = "content-show";
</script>
</head>
<body>
<g:render template="/layouts/internalContentTemplate">
	<h1>
		${content.title?.encodeAsHTML()}
	</h1>
	
	<div class="content form-info">
	
		<g:render template="/layouts/messagesAndErrorsTemplate" model="[entity: content]"/>
		
		<g:render template="/offer/showTemplate" model="[entity: content]"/>

		<!-- Buttons -->
		<g:link action="listAll" controller="offer" class="form-btn btn-primary"> 
			${message(code:'default.field.buttonBack')}
		</g:link>
	</div>
</g:render>
</body>
</html>