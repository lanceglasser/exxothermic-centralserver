<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.customeButtons.updateContent" /></title>
<!-- Image Crop Required Files -->
<link rel="stylesheet" href="${resource(dir: 'css/jcrop', file: 'jquery.Jcrop.css')}" type="text/css">
<script src="${resource(dir: 'js/jcrop', file: 'jquery.Jcrop.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_images_upload.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_utils.js')}"></script>

<script src="${resource(dir: 'themes/default/js', file: 'jquery.ptTimeSelect.js')}"></script>
<link rel="stylesheet" href="${resource(dir: 'themes/default/css', file: 'jquery.ptTimeSelect.css')}" type="text/css">

<link rel="stylesheet" href="${resource(dir: 'css/pluggins/bootstrap-colorpicker', file: 'bootstrap-colorpicker.css')}" type="text/css">
<link rel="stylesheet" href="${resource(dir: 'css/jcrop', file: 'demos.css')}" type="text/css">
<script src="${resource(dir: 'js/pluggins/bootstrap-colorpicker', file: 'bootstrap-colorpicker.js')}"></script>
<script src="${resource(dir: 'js/pluggins/bootstrap-colorpicker', file: 'docs.js')}"></script>

<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_custom_button_create.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_app_demo.js')}"></script>

<script>
	Exxo.module = "custom-button-edit";
	Exxo.UI.translates['closeButton'] = "${ message(code:'default.field.buttonClose') }";
	Exxo.UI.translates[Exxo.Constants.ERROR_FILE_INVALID] = "${message(code:'file.invalid.error')}";
	Exxo.UI.translates[Exxo.Constants.ERROR_FILE_SIZE] = "${message(code:'file.big.error')}";

	var TYPE_IMAGE		=  "${mycentralserver.utils.Constants.CONTENT_TYPE_IMAGE}";
	var TYPE_MESSAGE 	=  "${mycentralserver.utils.Constants.CONTENT_TYPE_TEXT}";
	var TYPE_OFFER		=  "${mycentralserver.utils.Constants.CONTENT_TYPE_OFFER}";
	var TYPE_SELECTED	=  "${content.type}";

	Exxo.UI.vars['filename'] =  "${content?.filename}";
	Exxo.UI.vars['banner-background-color'] = "${ content.color != ""? content.color:'#354cf4' }";
	
</script>
</head>
<body>
<g:render template="/layouts/internalContentTemplate">
	<h1>
		<g:message code="update.offer" />
	</h1>
	
	<div class="content">
		<g:render template="/layouts/messagesAndErrorsTemplate" model="[entity: content]"/>
		<g:uploadForm action="update" name="updateForm" class="exxo-form" >
			<g:hiddenField name="id" value="${content?.id}" />
			<g:hiddenField name="version" value="${content?.version}" />
			
			<g:render template="/content/formTemplate" model="[content: content, isOffer:true]"/>
						
			<!-- Buttons -->
			<g:submitButton class="form-btn btn-primary" name="update" value="${message(code:'default.field.buttonSave')}"/>
			
			<g:link action="listAll" controller="offer" class="back-button">        
		       <span class="form-btn btn-primary">
		          ${message(code:'default.field.buttonCancel')}
		       </span>       
		    </g:link>
		</g:uploadForm>
	</div>
	
</g:render>
</body>
</html>