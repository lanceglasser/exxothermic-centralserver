<%@ page contentType="text/html"%>

<!DOCTYPE html>
<html>
<head>
</head>
<body>
    ${message(code:'default.home.forgot.password.email.hello', args:[user.firstName])}
    ${message(code:'default.home.forgot.password.email.part.1')}
    <g:createLink controller="login" action="resetMyPassword"
        params="[token: user.resetPasswordToken]" absolute="true" />
    ${message(code:'default.home.forgot.password.email.part.2')}
    <g:message code="default.home.forgot.password.email.part.3" />
    <p>
        ${ message(code:'thanks') },
    </p>
    ${ session.affiliate.signature }
</body>
</html>