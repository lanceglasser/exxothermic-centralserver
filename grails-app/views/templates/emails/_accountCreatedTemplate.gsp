<%@ page contentType="text/html"%>

<!DOCTYPE html>
<html>
<head>
</head>
<body>
    <p> 
        ${ message(code:'email.create.account.1', args:[user.email, password]) }
        <a href="<g:createLink controller="home" action="index" absolute="true"/>"><g:message code="here" /></a>
    </p>
    <p>${ message(code:'thanks') },</p>
    <p>${ session.affiliate.signature }</p>
</body>
</html>