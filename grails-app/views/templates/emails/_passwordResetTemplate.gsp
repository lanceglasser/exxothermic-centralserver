<%@ page contentType="text/html"%>

<!DOCTYPE html>
<html>
<head>
</head>
<body>
    ${message(code:'default.home.forgot.password.email.hello', args:[user.firstName])}
    ${message(code:'default.home.forgot.password.email.part.5', args:[password])}
    <g:message code="default.home.forgot.password.email.part.6" />
    <g:message code="default.home.forgot.password.email.part.7" />
    <g:createLink controller="home" action="index" absolute="true" />
    <p>
        ${ message(code:'thanks') },
    </p>
    ${ session.affiliate.signature }
</body>
</html>