<%@ page contentType="text/html"%>

<!DOCTYPE html>
<html>
<head>
</head>
<body>
    <p>
        ${ message(code:'box.register.email.msg', args:[box.serial, box.location, user]) }
    </p>
    <p>${ message(code:'thanks') },</p>
    <p>${ session.affiliate.signature }</p>
</body>
</html>