<%@ page contentType="text/html"%>

<!DOCTYPE html>
<html>
<head>
</head>
<body>
    <p>
        ${ message(code:'email.password.updated.1', args:[password]) }
    </p>
    <g:message code="default.home.forgot.password.email.part.7" />
    <g:createLink controller="home" action="index" absolute="true" />
    <p>
        ${ message(code:'thanks') },
    </p>
    ${ session.affiliate.signature }
</body>
</html>