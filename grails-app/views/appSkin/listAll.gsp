<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.listSkin" /></title>
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'lightbox.css')}">
<script
	src="${resource(dir: 'themes/default/js', file: 'lightbox.min.js')}"></script>

<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
<script
	src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
<script
	src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
<script
	src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_app_skin_list.js')}"></script>
<script>
	Exxo.module = "app-skin-list";
	Exxo.UI.vars["page-code"] = "skins-list";
	Exxo.Tables.loadPaginationInitValuesFromString('<custom:paginationInfo code="skins-list"/>');
	Exxo.UI.translates['okButton'] = "${message(code:'default.field.buttonOk')}";
	Exxo.UI.translates['cancelButton'] = "${message(code:'default.field.buttonCancel')}";
	Exxo.UI.translates['disableMessage'] = "${message(code:'mycentralserver.custombuttons.CustomButton.isDisabled')}";
</script>
</head>
</head>
<body>
	<g:render template="/layouts/internalContentTemplate">
		<h1>
			<g:message code="default.skin.list" />
			<p class="muted"> ${ message(code:'click.on.thumbnails.for.preview') }</p>
		</h1>

		<div class="content">
			<g:render template="/layouts/messagesAndErrorsTemplate" />

			<table id="table-list" class="display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>
							${message(code: 'default.field.name')}
						</th>
						<th>
							${message(code: 'default.field.title')}
						</th>
						<th>
							${message(code: 'preview.files')}
						</th>
						<th>
							${message(code: 'default.table.action')}
						</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>
							${ message(code: 'default.field.name') }
						</th>
						<th>
							${message(code: 'default.field.title')}
						</th>
						<th>
							${message(code: 'preview.files')}
						</th>
						<th>
							${message(code: 'default.table.action')}
						</th>
					</tr>
				</tfoot>
				<tbody>
					<g:each in="${list}" status="i" var="entity">
						<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
							<td><g:link action="edit" id="${entity.id}">
									${entity.name.encodeAsHTML()}
								</g:link></td>
							<td>
								${entity.title?.encodeAsHTML()}
							</td>
							<td>
							<g:if test="${entity.backgroundImageUrl}">
								<a href="${entity.backgroundImageUrl?.encodeAsHTML()}"
									data-lightbox="image-${ entity.id }"
									data-title="${message(code: 'default.field.ads.background.image')}">
										<img src="${entity.backgroundImageUrl?.encodeAsHTML()}" width="64px" height="28px"/>
								</a>
							</g:if>
							
							<g:if test="${entity.dialogImageUrl}">
								<a href="${entity.dialogImageUrl?.encodeAsHTML()}"
									data-lightbox="image-${ entity.id }"
									data-title="${message(code: 'dialog.image')}">
									<!-- If there is not a dialog image, use this as thumbnail -->
									<g:if test="${!entity.backgroundImageUrl}">
									<img src="${entity.dialogImageUrl?.encodeAsHTML()}" width="32px" height="25px"/>
									</g:if>	
										
								</a>
							</g:if>
							
							<g:if test="${entity.backgroundImageUrlTablet}">
								<a href="${entity.backgroundImageUrlTablet?.encodeAsHTML()}"
									data-lightbox="image-${ entity.id }"
									data-title="${message(code: 'default.field.ads.background.image.tablet')}">
								</a>
							</g:if>
							
							<g:if test="${entity.tabletDialogImageUrl}">
								<a href="${entity.tabletDialogImageUrl?.encodeAsHTML()}"
									data-lightbox="image-${ entity.id }"
									data-title="${message(code: 'dialog.image.tablet')}">
								</a>
							</g:if>
							</td>
							<td>
								<g:link action="show" id="${entity.id}"
									class="action-icons view"
									title="${message(code:'default.action.show')}">
								</g:link>
								<g:link action="edit" id="${entity.id}"
									class="action-icons edit"
									title="${message(code:'default.action.edit')}"></g:link>
								<sec:ifNotGranted roles="ROLE_ADMIN">	
								<g:link action="assignLocation" id="${entity.id}"
									buttonEnable="true" class="action-icons viewLocation"
									title="${message(code:'default.action.management')}"></g:link>
								</sec:ifNotGranted>
								<g:link action="delete" id="${entity.id}"
									class="action-icons delete"
									title="${message(code:'default.action.delete')}"></g:link>
							</td>
						</tr>
					</g:each>
				</tbody>
			</table>

			<!-- Information with the status of every transaction -->
			<g:render template="/layouts/tags/boxesUpdateResultTemplate"
				model="[boxesSyncResult: boxesSyncResult]" />
		</div>
	</g:render>
	<div id="confirm-delete-dialog"
		title="${message(code:'default.confirm.title')}"
		style="display: none;">
		${message(code:'default.app.skin.confirm.delete')}
	</div>
</body>
</html>