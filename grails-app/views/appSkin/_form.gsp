<script type="text/javascript">
	Exxo.UI.vars['back-img'] = "${entity?.backgroundImageUrl? entity.backgroundImageUrl:''}"; 
	Exxo.UI.vars['dialog-img'] = "${entity?.dialogImageUrl? entity.dialogImageUrl:''}";
</script>
<div class="col-xs-12">
	<div class="col-xs-4 resize-formleft">
		
		<h1>${ title }</h1>
		
		<p class="muted">
			${ message(code:'app.theme.help.text.1') }
		</p>
		
		<p class="muted">
			${ message(code:'app.theme.help.text.2') }
		</p>
		
		<p class="muted">
			${ message(code:'app.theme.help.text.3') }
		</p>
		
		<div id="helpDiv" style="height: 30px; vertical-align: middle; line-height: 30px;" >
            <a href="#" class="view-imageHelp" cat="app" template="appThemeSettings">
            	<img class="floatLeft" alt="No Image" src="${ resource(dir: 'themes/default/img', file: 'Help-Support.png') }">
            </a>
            <div class="noteHelp" >
	            <span class="liten-up floatLeft">
            	${message(code:'default.field.content.help.tab.note')}                         
            	</span>
           	</div>
		</div>
	</div>
	<div class="col-xs-8 resize-formright">
		<!-- App Example Container -->
		<g:render template="/layouts/tags/appViewExampleTemplate" model="[appTheme: entity]"/>
		<span class="clearfix"></span>
	</div>
</div>

<span class="clearfix"></span>
<br/>
<hr/>

<!-- Left Column -->
<div class="col-xs-6 resize-formleft">
	
	<!-- Name  -->				
	<custom:editField type="text" name="name" required="${true}" value="${entity.name}" 
		label="default.field.name" />
	
	<!-- Title  -->				
	<custom:editField type="text" name="title" max="20" required="${true}" value="${entity.title}" 
		label="default.field.title" />
	
	<!-- Primary Color -->
	<custom:editField type="colorSelect" name="primaryColor" required="${true}" value="${entity.primaryColor}" 
		label="default.field.main.color"  number="1"/>
	
	<!-- Secondary Color -->
	<custom:editField type="colorSelect" name="secondaryColor" required="${true}" value="${entity.secondaryColor}" 
		label="default.field.secondary.color" number="2" />
		
	<!-- ChannelInfoEnable  -->
	<custom:editField type="checkbox" name="channelInfoEnable" required="${false}" value="${entity.channelInfoEnable}" 
		label="app.skin.field.enable.channel.info" cssclass="input-checkbox"/>
</div>

<!-- Right Column -->
<div class="col-xs-6 resize-formright">
<sec:ifAnyGranted roles="ROLE_ADMIN, ROLE_HELP_DESK, ROLE_INTEGRATOR_STAFF, ROLE_INTEGRATOR">
	<!-- Start of Tabs -->
	<ul id="tabs">
	    <li><a href="#background">${ message(code:'default.field.ads.background.image') }</a></li>
	    <li><a href="#dialog">${ message(code:'text.dialog.image') }</a></li>
	</ul>
	 
	<div id="background" class="tab-section">
		<div class="col-sm-12">
    		<div class="form-help-alert">
				<div class="questionToolTip" title="${ message(code:'app.theme.background.image') }"></div>
				${ message(code:'app.theme.background.image') }
			</div>
		</div>
		<span class="clearfix"></span>
	
	    	<!-- Ads Background Image -->
			<g:render template="/layouts/tags/imageUploadSelectorTemplate" 
				model="[
					id:				'bgImageUrl',
					ref:			'1',
					demoId:			'app-skin-background-img',
					demoWidth:		'270',
					demoHeight:		'118',
					imgValue: 		entity.backgroundImageUrl,
					label:			message(code:'phone.image'),
					cropImage:		true,
					minWidth: 		mycentralserver.utils.Constants.FEATURED_CONTENT_IMAGE_WIDTH,
					minHeight: 		mycentralserver.utils.Constants.FEATURED_CONTENT_IMAGE_HEIGHT
					]"
			/>
			
			<!-- GenFeaturedImgForTablets -->
			<custom:editField type="checkbox" name="genFeaturedImgForTablets" required="${false}" value="${entity.genFeaturedImgForTablets}"
				label="auto.generate.image.for.tablets" cssclass="input-checkbox" 
				rightColumnPosition="right" leftColumnPosition="right" leftColumnSize="10"/>
			
			<!-- Ads Background Image Tablets -->
			<div id="tabletsFeatureImageContainer" style="${ entity.genFeaturedImgForTablets? 'display: none;':''}">
			<g:render template="/layouts/tags/imageUploadSelectorTemplate" 
				model="[
					id:				'bgImageUrlTablet',
					ref:			'2',					
					imgValue: 		entity.backgroundImageUrlTablet,
					label:			message(code:'tablets.image'),
					cropImage:		true,
					minWidth: 		mycentralserver.utils.Constants.FEATURED_CONTENT_IMAGE_TABLET_WIDTH,
					minHeight: 		mycentralserver.utils.Constants.FEATURED_CONTENT_IMAGE_TABLET_HEIGHT
					]"
			/>
			</div>
			
			<custom:editField type="featureGenExample" name="featureGenExample" required="${false}" value="" label=""
					src="${resource(dir: 'themes/default/img', file: '640x280To640x500.png')}" visible="${ entity.genFeaturedImgForTablets}"/>
	</div>
	
	<div id="dialog" class="tab-section">
    	<div class="col-sm-12">
    		<div class="form-help-alert">
    		  <div class="questionToolTip" title="${ message(code:'app.skin.dialog.note') }"></div>
				${ message(code:'app.skin.dialog.note') }
			</div>
		</div>
		<span class="clearfix"></span>
		
		<!-- dialogImageUrl y tabletDialogImageUrl -->
		<g:render template="/layouts/tags/imageUploadSelectorTemplate" 
			model="[
				id:				'dialogImageUrlFile',
				ref:			'3',
				demoId:			'dialog-img',
				demoWidth:		'270',
				demoHeight:		'210',
				imgValue: 		entity.dialogImageUrl,
				label:			message(code:'phone.image'),
				cropImage:		true,
				minWidth: 		mycentralserver.utils.Constants.TEXT_CONTENT_BACKGROUND_WIDTH,
				minHeight: 		mycentralserver.utils.Constants.TEXT_CONTENT_BACKGROUND_HEIGHT
				]"
		/>
		
		<!-- GenFeaturedImgForTablets -->
		<custom:editField type="checkbox" name="genDialogImgForTablets" required="${false}" value="${entity.genDialogImgForTablets}"
			label="auto.generate.image.for.tablets" cssclass="input-checkbox" 
			rightColumnPosition="right" leftColumnPosition="right" leftColumnSize="10"/>
		
		<div id="dialogImageTabletObject" style="${ entity.genDialogImgForTablets? 'display: none;':''}">
			<g:render template="/layouts/tags/imageUploadSelectorTemplate" 
				model="[
					id:				'tabletDialogImageUrlFile',
					ref:			'4',
					imgValue: 		entity.tabletDialogImageUrl,
					label:			message(code:'tablets.image'),
					cropImage:		true,
					minWidth: 		mycentralserver.utils.Constants.TEXT_CONTENT_BACKGROUND_TABLET_WIDTH,
					minHeight: 		mycentralserver.utils.Constants.TEXT_CONTENT_BACKGROUND_TABLET_HEIGHT
					]"
			/>
		</div>		
		
		<custom:editField type="dialogGenExample" name="dialogGenExample" required="${false}" value="" label=""
				src="${resource(dir: 'themes/default/img', file: '640x500To924x462.png')}" visible="${ entity.genDialogImgForTablets }"/>
	</div>
	</sec:ifAnyGranted>
</div>
<span class="clearfix"></span>
<div class="col-xs-12">
<!--  Buttons  -->
<g:submitButton class="form-btn btn-primary" name="save" value="${message(code:'default.field.buttonSave')}" />
<g:link action="listAll" controller="appSkin" class="back-button">        
   <span class="form-btn btn-primary">
      ${message(code:'default.field.buttonCancel')}
   </span>       
</g:link>
</div>