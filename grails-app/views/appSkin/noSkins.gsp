<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.listSkin" /></title>
<script>
	Exxo.module = "index";
</script>
</head>
</head>
<body>
	<section class="introp">
			<div class="intro-body">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2" id="home-salute">
							<div id="child-salute">
								<div class="message">
									<p><g:message code="default.skins.dont.exists"/></p>
									<g:link controller="appSkin" action="create"><g:message code="default.title.newAppSkin"/></g:link>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
</body>
</html>