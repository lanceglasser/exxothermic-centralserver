<!-- Left Column -->
<div class="col-xs-6 resize-formleft">
	<custom:editField name="name"
		value="${entity.name ? entity.name.encodeAsHTML() : ''}"
		label="default.field.name" leftColumnSize="6"/>
		
	<custom:editField name="title"
		value="${entity.title ? entity.title.encodeAsHTML() : ''}"
		label="default.field.title" leftColumnSize="6" />
	
	<div class="col-xs-6 text-to-right">
		<label>
			<g:message code="default.field.main.color" />
		</label>
	</div>				
	<div class="col-xs-6 text-to-left">			
		<i class="channel-color" style="background-color: ${entity.primaryColor}"></i>
	</div>
	<span class="clearfix"></span>	
	
	<div class="col-xs-6 text-to-right">
		<label>
			<g:message code="default.field.secondary.color" />
		</label>
	</div>				
	<div class="col-xs-6 text-to-left">			
		<i class="channel-color" style="background-color: ${entity.secondaryColor}"></i>
	</div>
	<span class="clearfix"></span>
	
	<custom:editField name="channelInfoEnable"
		value="${(entity.channelInfoEnable == true) ? message(code:"default.field.yes") : message(code:"default.field.no")}"
		label="app.skin.field.enable.channel.info" leftColumnSize="6"/>
		
	<div class="col-xs-6 text-to-right">
		<label>
			<g:message code="default.field.ads.background.image" />
		</label>
	</div>				
	<div class="col-xs-6 text-to-left">
		<g:if test="${ entity.backgroundImageUrl }">
			
			<a href="${entity.backgroundImageUrl?.encodeAsHTML()}" data-lightbox="image-${ entity.id }" 
							data-title="${message(code: 'default.field.ads.background.image')}">
				<i><img class="ad-background-img" src="${entity.backgroundImageUrl}"></i>
				<span class="liten-up">${ message(code:'preview.image.on.click.note') }</span>				
			</a>
					
		</g:if>
		<g:else>
			---
		</g:else>
	</div>
	<span class="clearfix"></span>
	
	<div class="col-xs-6 text-to-right">
		<label>
			<g:message code="default.field.ads.background.image.tablet" />
		</label>
	</div>				
	<div class="col-xs-6 text-to-left">
		<g:if test="${ entity.backgroundImageUrlTablet }">
			
			<a href="${entity.backgroundImageUrlTablet?.encodeAsHTML()}" data-lightbox="image-${ entity.id }" 
							data-title="${message(code: 'default.field.ads.background.image.tablet')}">
				<i><img class="ad-background-img" src="${entity.backgroundImageUrlTablet}"></i>
				<span class="liten-up">${ message(code:'preview.image.on.click.note') }</span>				
			</a>
					
		</g:if>
		<g:else>
			---
		</g:else>
	</div>
	<span class="clearfix"></span>
	
	<!-- Dialog Image -->
	<div class="col-xs-6 text-to-right">
		<label>
			<g:message code="dialog.image" />
		</label>
	</div>				
	<div class="col-xs-6 text-to-left">
		<g:if test="${ entity.dialogImageUrl }">
			
			<a href="${entity.dialogImageUrl?.encodeAsHTML()}" data-lightbox="image-${ entity.id }" 
							data-title="${message(code: 'dialog.image')}">
				<i><img class="ad-background-img" src="${entity.dialogImageUrl}"></i>
				<span class="liten-up">${ message(code:'preview.image.on.click.note') }</span>				
			</a>
					
		</g:if>
		<g:else>
			---
		</g:else>
	</div>
	<span class="clearfix"></span>
	
	<!-- Tablets Dialog Image -->
	<div class="col-xs-6 text-to-right">
		<label>
			<g:message code="dialog.image.tablet" />
		</label>
	</div>				
	<div class="col-xs-6 text-to-left">
		<g:if test="${ entity.tabletDialogImageUrl }">
			
			<a href="${entity.tabletDialogImageUrl?.encodeAsHTML()}" data-lightbox="image-${ entity.id }" 
							data-title="${message(code: 'dialog.image.tablet')}">
				<i><img class="ad-background-img" src="${entity.tabletDialogImageUrl}"></i>
				<span class="liten-up">${ message(code:'preview.image.on.click.note') }</span>				
			</a>
					
		</g:if>
		<g:else>
			---
		</g:else>
	</div>
	<span class="clearfix"></span>
	
</div>
<!-- Right Column -->
<div class="col-xs-6 resize-formright">
</div>
<span class="clearfix"></span>