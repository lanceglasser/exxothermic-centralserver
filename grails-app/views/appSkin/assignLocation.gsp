<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.assignLocation" /></title>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_app_skin_create.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_images_upload.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_utils.js')}"></script>
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
<script
	src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
<script
	src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
<script>
	Exxo.module = "skin-associate";
</script>
</head>
<body>
<g:render template="/layouts/internalContentTemplate">
	<h1>
		<g:message code="default.title.assignSkin" />
	</h1>
	<p class="muted"> <g:message code="assign.skin.note" /></p>
	<sec:ifAnyGranted roles="ROLE_OWNER, ROLE_OWNER_STAFF">
	<p class="muted"><b><g:message code="associated.with.premium.companies.only" /></b></p>
	</sec:ifAnyGranted>
	
	<div class="content">
		<g:render template="/layouts/messagesAndErrorsTemplate" model="[entity: appSkin]"/>
		
		<g:form action="assign" name="createForm" id="createForm" class="exxo-form">
			<input type="hidden" id="locationsToSave" name="locationsToSave" value=""/>
			<div class="col-xs-12 resize-formleft">
				<div class="col-xs-2">
					<label class="control-label" for="user">
						<g:message code="default.field.appSkin" /> *
					</label>
				</div>
				<div class="col-xs-10">
					<g:select name="activeSkin" id="activeSkin"  from="${skins}" style="width:50%" optionKey="id" optionValue="name" value="${appSkin.id}" 
							onchange="Exxo.Skin.Create.clearScrean();${remoteFunction(
					            action:'ajaxAssignSkin', 
					            params:'\'skinId=\' + escape(this.value)',
								update : 'infor')}"/>
				</div>
			</div>
			<span class="clearfix"></span>							
			
			<div class="col-xs-12 resize-formleft">
				<div class="col-xs-2">
				</div>
				<div class="col-xs-10">
					<div class="exxo-table-container" id="infor"></div>
				</div>
			</div>
			<span class="clearfix"></span>							
										
			<div class="col-xs-12 resize-formleft">
				<div class="col-xs-2">
				</div>
				<div class="col-xs-10 text-to-left">
					<input type="hidden" id="locations" name="locations" value=""/>
					<g:submitButton class="form-btn btn-primary" name="assign"
						value="${message(code:'default.field.buttonSave')}" />
					
					<g:link controller="appSkin" action="listAll" class="back-button">        
				       <span class="form-btn btn-primary">
				          ${message(code:'default.field.buttonCancel')}
				       </span>       
				    </g:link>
				</div>
			</div>
			<span class="clearfix"></span>
		</g:form>
				
		<!-- Information with the status of every transaction -->
		<g:render template="/layouts/tags/boxesUpdateResultTemplate" model="[boxesSyncResult: boxesSyncResult]"/>
	</div>
</g:render>
</body>
</html>