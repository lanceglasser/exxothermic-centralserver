<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.home.skinApp" /></title>
<link rel="stylesheet" href="${resource(dir: 'css/pluggins/bootstrap-colorpicker', file: 'bootstrap-colorpicker.css')}" type="text/css">
<link rel="stylesheet" href="${resource(dir: 'css/jcrop', file: 'jquery.Jcrop.css')}" type="text/css">
<link rel="stylesheet" href="${resource(dir: 'themes/default/css', file: 'video-js.min.css')}" type="text/css">		
<script src="${resource(dir: 'js/pluggins/bootstrap-colorpicker', file: 'bootstrap-colorpicker.js')}"></script>
<script src="${resource(dir: 'js/pluggins/bootstrap-colorpicker', file: 'docs.js')}"></script>
<script src="${resource(dir: 'js/jcrop', file: 'jquery.Jcrop.js')}"></script>
<script src="${resource(dir: 'themes/default/js', file: 'video.js')}"></script>

<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_utils.js')}"></script>
<script>
	Exxo.module = "skin-create";
	//videojs.options.flash.swf = "video-js.swf";
</script>

</head>
<body>
<g:render template="/layouts/internalContentTemplate">
	<h1>
		${entity.name?.encodeAsHTML()}
	</h1>
	
	<div class="content form-info">
		<g:render template="/layouts/messagesAndErrorsTemplate" model="[entity: entity]"/>
		
		<!-- Left Column -->
		<div class="col-xs-8 resize-formleft">
			<custom:editField name="name"
				value="${entity.name ? entity.name.encodeAsHTML() : ''}"
				label="default.field.name" leftColumnSize="6"/>						
		</div>
		
		<div class="col-xs-8 resize-formleft">
			<custom:editField name="createdBy"
				value="${entity.createdBy ? entity.createdBy.encodeAsHTML() : ''}"
				label="default.field.createdBy" leftColumnSize="6"/>						
		</div>
		<div class="col-xs-8 resize-formleft">
			<custom:editField name="dateCreated"
				value="${entity.dateCreated ? entity.dateCreated.encodeAsHTML() : ''}"
				label="default.field.dateCreated" leftColumnSize="6"/>						
		</div>
		<div class="col-xs-8 resize-formleft">
			<custom:editField name="lastUpdatedBy"
				value="${entity.lastUpdatedBy ? entity.lastUpdatedBy.encodeAsHTML() : ''}"
				label="default.field.lastUpdatedBy" leftColumnSize="6"/>						
		</div>
		<div class="col-xs-8 resize-formleft">
			<custom:editField name="lastUpdated"
				value="${entity.lastUpdated ? entity.lastUpdated.encodeAsHTML() : ''}"
				label="default.field.lastUpdated" leftColumnSize="6"/>						
		</div>
		
		
		<span class="clearfix"></span>

		<!-- Buttons -->
		<g:link action="listAll" controller="docCategory" class="form-btn btn-primary" params="[offset: offset]"> 
			${message(code:'default.field.buttonBack')}
		</g:link>
	</div>
</g:render>


</body>
</html>