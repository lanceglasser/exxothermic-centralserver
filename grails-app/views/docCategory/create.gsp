<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.newAppSkin" /></title>
<link rel="stylesheet" href="${resource(dir: 'css/pluggins/bootstrap-colorpicker', file: 'bootstrap-colorpicker.css')}" type="text/css">
<link rel="stylesheet" href="${resource(dir: 'css/jcrop', file: 'jquery.Jcrop.css')}" type="text/css">		
<link rel="stylesheet" href="${resource(dir: 'css/jcrop', file: 'demos.css')}" type="text/css">	
<script src="${resource(dir: 'js/pluggins/bootstrap-colorpicker', file: 'bootstrap-colorpicker.js')}"></script>
<script src="${resource(dir: 'js/pluggins/bootstrap-colorpicker', file: 'docs.js')}"></script>
<script src="${resource(dir: 'js/jcrop', file: 'jquery.Jcrop.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_app_skin_create.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_utils.js')}"></script>
<script>
	Exxo.module = "skin-create";
	var dimensionsImageError = "${message(code:'customButton.image.dimension.error')}";
	var invalidImageError = "${message(code:'image.invalid.error')}";
	var tooBigImageError = "${message(code:'image.big.error')}";
	//Exxo.UI.vars["welcomeType"] = "";
</script>
</head>
<body>
<g:render template="/layouts/internalContentTemplate">
	<h1>
		<g:message code="menu.doc.categories.create" />
	</h1>
	
	<div class="content">
		<g:render template="/layouts/messagesAndErrorsTemplate" model="[entity: entity]"/>
		
		<g:uploadForm action="save" id="createForm" name="createForm" class="exxo-form">
			<g:hiddenField name="adsBackgroundImageUpdated" value="1" />
			<g:hiddenField name="wImgUpd2" value="0" />
			<g:hiddenField name="wImgUpd3" value="0" />
			<g:hiddenField name="wImgUpd4" value="0" />
			<g:render template="form"></g:render>
		</g:uploadForm>
	</div>
</g:render>
</body>
</html>