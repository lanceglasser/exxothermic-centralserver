<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="doc.categories.title" /></title>
<script>
	Exxo.module = "index";
</script>
</head>
</head>
<body>
	<section class="introp">
			<div class="intro-body">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2" id="home-salute">
							<div id="child-salute">
								<div class="message">
									<p><g:message code="doc.categories.empty.list"/></p>
									<g:link controller="docCategory" action="create"><g:message code="menu.doc.categories.create"/></g:link>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
</body>
</html>