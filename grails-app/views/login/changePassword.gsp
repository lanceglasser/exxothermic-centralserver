<html>
<head>
	<meta name='layout' content='anonymous' />
	<title><g:message code="springSecurity.login.title" /></title>
	<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_login.js')}"></script>
	<script>
		Exxo.module = "login";
		var validationDetail = "${message(code:'password.validation.description')}";
		var confirmationFails = "${message(code:'password.validation.confirmation.fail')}";
		Exxo.UI.vars['validationDetail'] = "${message(code:'password.validation.description')}";
		Exxo.UI.vars['confirmationFails'] = "${message(code:'password.validation.confirmation.fail')}";
	</script> 
</head>

<body>

	<!-- Create div first for Login Form-->
	<div id="first">
		<form action="doChangePassword" controller="login" method="post">
			<h1>${message(code: 'default.home.ChangePassword')}</h1>
			<g:if test="${flash.success}">
				<div class="alert alert-success">
					<a class="close" data-dismiss="alert">×</a>
					${flash.success}
				</div>
				${flash.success=null}
			</g:if>
			<g:if test='${flash.message}'>
				<div class="alert">
					<a class="close" data-dismiss="alert">×</a>
					${flash.message}
				</div>
			</g:if>
			<g:if test="${flash.error}">
        		<div class="alert alert-error">  
				  <a class="close" data-dismiss="alert">×</a>  
				  ${flash.error} 
				  <g:hasErrors bean="${user}">
			  		<ul>
						<g:eachError bean="${user}" var="error">
							<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
						</g:eachError>
					</ul>
				  </g:hasErrors>				
				</div> 
				 ${flash.error=null}
      		</g:if>
			
			<input type="email" id="username" name="username" placeholder="${message(code: 'field.email')}" required="required"/>
			<input type="password" id="password" name="password" value="" placeholder="${message(code: 'field.password')}" required="required"/>
			
			<input type="password" id="newPassword" name="newPassword" value="" placeholder="${message(code: 'default.field.password.new')}" required="required"/>
			
			<input type="password" id="confirmPassword" name="confirmPassword" value="" placeholder="${message(code: 'default.field.password.confirm')}" required="required"/>
			
			<input type="submit" class="form-btn btn-primary" value="${message(code: 'default.home.change')}" />
			<g:link action="listAll" controller="location" >        
		       <span class="form-btn btn-primary">
		          ${message(code:'default.field.buttonCancel')}
		       </span>       
	    	</g:link>
		</form>
	</div>
</body>
</html>
