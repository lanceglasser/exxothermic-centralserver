<html>
<head>
	<meta name='layout' content='anonymous' />
	<title><g:message code="springSecurity.login.title" /></title>
	<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_login.js')}"></script>
	<script>
		Exxo.module = "login";
	</script> 
</head>

<body>
	<div id="first">
		<form action="<g:createLink controller="login" action="doResetPassword"/>" method="post" id="forgot-form">
			<h1>
				<g:message code="forgot.password.title" />
			</h1>
			
			<g:render template="/layouts/messagesAndErrorsTemplate" model="[entity: user]"/>
			
			<p>
				<g:message code="forgot.password.subtitle" />
			</p>
			<input type="email" id="forgot-name" name="forgot-name" required="required"
				placeholder="${message(code: 'field.email', default: 'Enter your email')}" />
			<input type="submit" class="form-btn btn-primary"
				value="${message(code: 'forgot.password.button.reset', default: 'Reset')}" />
		</form>
		<hr>
		<p id="three">
			<g:link controller="home" action="index" class="signin">
				<g:message code="home.AlreadyhaveAnAccount" />
			</g:link>
		</p>
	</div>
</body>
</html>
