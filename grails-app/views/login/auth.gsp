<html>
<head>
	<meta name='layout' content='anonymous' />
	<title><g:message code="springSecurity.login.title" /></title>
	<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_login.js')}"></script>
	<script>
		Exxo.module = "login";
	</script> 
</head>

<g:set var="bodyClass" value="${(session.theme == "default")? 'mye-body-background':''}" />

<body class="${ bodyClass }">

	<!-- Create div first for Login Form-->
	<div id="first">
		<form action="${postUrl}" method="post">
			<h1>${message(code: 'signin.title')}</h1>
			<h2>${message(code: 'signin.subtitle')}</h2>
			<p>${message(code: 'signin.message')}</p>
			<g:if test="${flash.success}">
				<div class="alert alert-success">
					<a class="close" data-dismiss="alert">×</a>
					${flash.success}
				</div>
				${flash.success=null}
			</g:if>
			<g:if test='${flash.message}'>
				<div class="alert">
					<a class="close" data-dismiss="alert">×</a>
					${flash.message}
				</div>
			</g:if>
			<input type="text" id="username" name="j_username" placeholder="${message(code: 'field.email')}" required="required"/> 
			<input type="password" id="password" name="j_password" placeholder="${message(code: 'field.password')}" required="required"/>
			<div class="skin skin-square">
				<input tabindex="9" type="checkbox" id="remember_me" name='${rememberMeParameter}'
					<g:if test='${hasCookie}'>checked="true"</g:if>
				/>
				<label for="square-checkbox-1">${message(code: 'signin.remember.me')}<br>
				</label>
				<p id="one">
					<a href="#" id="forgot">${message(code: 'signin.forgot.password')}</a>
				</p>
			</div>
			<input type="submit" id="login" class="btn-primary" value="${message(code: 'signin')}" />
			<hr>
		</form>
	</div>
	<div id="third">
		<form action="<g:createLink controller="login" action="doResetPassword"/>" method="post" id="forgot-form">
			<h1>
				<g:message code="forgot.password.title" />
			</h1>
			<p>
				<g:message code="forgot.password.subtitle" />
			</p>
			<input type="email" id="forgot-name" name="forgot-name" required="required"
				placeholder="${message(code: 'field.email', default: 'Enter your email')}" />
			<input type="submit" class="form-btn btn-primary"
				value="${message(code: 'forgot.password.button.reset', default: 'Reset')}" />
		</form>
		<hr>
		<p id="three">
			<a href="#" id="backtologin" class="signin">
				${message(code: 'home.AlreadyhaveAnAccount')}
			</a>
		</p>
	</div>
</body>
</html>
