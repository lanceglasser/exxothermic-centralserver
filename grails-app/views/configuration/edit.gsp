<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<link rel="stylesheet" href="${resource(dir: 'css/pluggins/select2', file: 'select2.css')}" type="text/css">
		<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
		<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
		
		<script src="${resource(dir: 'js/pluggins/select2', file: 'select2.js')}"></script>	
		<script src="${resource(dir: 'js/pages/user', file: 'selects.js')}"></script>
		
		<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_configuration.js')}"></script>	
		<script> 
			Exxo.module = "configuration-edit";
			Exxo.UI.vars["configuration-type"] = ${ configuration.type }; 
			Exxo.UI.vars["configuration-list-type"] = ${ mycentralserver.utils.Constants.CONFIGURATION_TYPE_LIST };
		</script>	
	</head>
	<body>
		<g:render template="/layouts/internalContentTemplate">
			<g:form action="update" name="updateForm" class="exxo-form" >
				<g:hiddenField name="id" value="${configuration.id}" />			
				<h1><g:message code="configuration.edit" /></h1>
				<div class="content">
					<g:render template="/layouts/messagesAndErrorsTemplate"
						model="[entity: configuration]" />
						
     		 		<!-- Left Column -->
					<div class="col-xs-6 resize-formleft">
						
						<!-- Code -->
						<span id="codeDiv">
				            <div class="col-xs-4 text-to-right">
				              <label for="model"> ${ message(code:'default.field.code')} </label>
				            </div>
				            <div class="col-xs-8 text-to-left exxo-label">
				             	${configuration.code} 
				            </div>
						</span>
						<span class="clearfix"></span>
						
						<!-- Description  -->				
						<custom:editField type="textarea" name="description" required="${true}" value="${configuration.description}" 
							label="default.field.description" />
			
						<!-- Print Configuration Form -->
						<configuration:renderConfigForm configuration="${ configuration }"/>
						
						<br/>
						<!--  Buttons  -->
						<g:submitButton class="form-btn btn-primary" name="save" value="${message(code:'default.field.buttonSave')}" />
						<g:link controller="configuration" class="back-button">        
						   <span class="form-btn btn-primary">
						      ${message(code:'default.field.buttonCancel')}
						   </span>       
						</g:link>					
					</div>

     		 		<span class="clearfix"></span>
				</div>				 
			</g:form>
		</g:render>	
	</body>
</html>

