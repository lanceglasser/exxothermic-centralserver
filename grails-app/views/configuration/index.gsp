<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.listCompany" /></title>
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
<script
	src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
<script
	src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
<script>
	Exxo.module = "page-list";
	Exxo.UI.vars["page-code"] = "configuration-list";
	Exxo.Tables
			.loadPaginationInitValuesFromString('<custom:paginationInfo code="configuration-list"/>');
</script>
</head>
<body>
	<section class="introp">
		<div class="intro-body">
			<div class="container full-width">
				<div class="row">
					<div class="col-md-12" id="internal-content">
						<h1>
							<g:message code="configurations.list.title" />
						</h1>
						<g:render template="/layouts/messagesAndErrorsTemplate" />
						<div class="content">
							<table id="table-list" class="display" cellspacing="0" width="100%">
					              <thead>
					                <tr>
								     <th>
											${message(code: 'default.field.code')}
										</th>
										<th style="width: 250px;">
											${message(code: 'default.field.description')}
										</th>
										<th>
											${message(code: 'default.text.currentValue')}
										</th>
										<th>${message(code: 'default.table.action')}</th>
								    </tr>
					              </thead>			
					              <tbody>
			                        <g:each in="${configurations}" status="i" var="conf">
									<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
										<td>
											${ conf.code }
										</td>
										<td>
											${ conf.description }
										</td>

										<td>
											${ conf.value }
										</td>
										<td><g:link controller="configuration" action="edit"
												id="${conf.id}" class="action-icons edit"
												title="${message(code:'default.action.edit')}"></g:link></td>
									</tr>
								</g:each>
				              </tbody>
				            </table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div id="confirm-reset-dialog"
		title="${message(code:'mycentralserver.user.User.resetPassword.title')}"
		style="display: none;">
		${message(code:'mycentralserver.user.User.resetPassword')}
	</div>
</body>
</html>