<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main" />
		<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
		<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
		<script src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
		<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
		<script
			src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_software.upgrade.job.js')}"></script>
		<script>
			Exxo.module = "box-upgrade-job-show";
			Exxo.UI.vars["page-code"] = "box-upgrade-job-list";
			Exxo.Tables.loadPaginationInitValuesFromString('<custom:paginationInfo code="box-upgrade-job-list"/>');
			Exxo.UI.translates['okButton'] = "${message(code:'default.field.buttonOk')}";
			Exxo.UI.translates['cancelButton'] = "${message(code:'default.field.buttonCancel')}";
            <g:if test="${job.status != 30}">
            // Reload the page every 1 minute for update of the information when the job is not completed
            setTimeout(function(){
                window.location.reload(1);
             }, 60*1000);
            </g:if>
		</script>
		</head>
	<body>
	<section class="intro">
	  <div class="intro-body">
	    <div class="container full-width">
	      <div class="row">
	        <div class="col-md-12" id="internal-content">
	          <h1> ${ message(code:'box.software.upgrade.job') }: ${ job.description.encodeAsHTML() } </h1>
	          <g:render template="/layouts/messagesAndErrorsTemplate"/>
	          
	          
			  <div class="content form-info">
			  	<!-- Left Column -->
				<div class="col-xs-6 resize-formleft">
					<custom:editField name="status"
						value="${ message(code:'box.job.status.' + job.status)}"
						label="status" leftColumnSize="6"/>
					
					<custom:editField name="jobs-status"
						value="${ message(code:'total.completed.detail', args:[job.completedBoxes, job.totalBoxes])}"
						label="total.completed" leftColumnSize="6"/>
				</div>
				<!-- Right Column -->
				<div class="col-xs-6 resize-formright">
					<custom:editField name="createdBy"
						value="${job.createdBy.encodeAsHTML()}"
						label="default.field.createdBy" leftColumnSize="6"/>
					
					<custom:editField name="dateCreated"
						value="${ formatDate(format:'MM-dd-yyyy hh:mm:ss', dat: job.dateCreated )}"
						label="default.field.dateCreated" leftColumnSize="6"/>
					
					<custom:editField name="dateCreated"
						value="${ formatDate(format:'MM-dd-yyyy hh:mm:ss', dat: job.lastUpdated )}"
						label="default.field.lastUpdated" leftColumnSize="6"/>
				</div>
				<span class="clearfix"></span>
			  	<br/><br/>
			  	<h2>${ message(code:'exxtractos.upgrade.jobs.detail') }</h2>
				<table id="table-list" class="display" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th> ${message(code: 'default.field.serial')} </th>
							<th> ${message(code: 'status')} </th>
							<th> ${ message(code:'steps') + ' ' + message(code: 'total.completed')} </th>
							<th> ${message(code: 'upgrade.to.version')} </th>
							<th> ${message(code: 'default.table.action')} </th>
						</tr>
					</thead>
					<tbody>
						<g:each in="${job.boxUpgradeJobs}" status="i" var="entity">
							<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
								<td>
									<g:link controller="boxSoftwareUpgradeJob" action="getStepsDetailAjax" 
										id="${entity.id}" class="view-details">
										${entity.box.serial.encodeAsHTML()}
									</g:link>
								</td>
								<td>
									${ message(code:'box.job.status.' + entity.status)}
								</td>
								<td>
									${ message(code:'total.completed.detail', args:[entity.completedSteps, entity.totalSteps])}
								</td>
								<td>
									${ entity.boxSoftware.versionSoftware }
								</td>
								<td>
									<g:link controller="boxSoftwareUpgradeJob" action="getStepsDetailAjax" id="${entity.id}"
										class="action-icons view view-details"
										title="${message(code:'default.action.show')}">
									</g:link>
								</td>
							</tr>
						</g:each>
					</tbody>
				</table>
				
				<!--  Buttons  -->
				<g:link action="list" controller="boxSoftwareUpgradeJob">        
				   <span class="form-btn btn-primary">
				      ${message(code:'default.field.buttonBack')}
				   </span>       
				</g:link>

	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	</section>
	<div id="steps-details" title="${message(code:'software.job.steps.detail.title')}" style="display:none;">
		<p>${ message(code:'software.job.steps.detail.title') }: <span id="serial-container"></span></p>
		<table id="internal-content" class="display dataTable">
		<thead>
		<tr>
			<th style="text-align: center;">${ message(code:'upgrade.to.version') }</th>
			<th>${ message(code:'default.field.status') }</th>
			<th>${ message(code:'default.field.lastUpdated') }</th>
		</tr>
		</thead>
		<tbody id="steps-tbody-list">
		</tbody>
		</table>
	</div>
	</body>
</html>