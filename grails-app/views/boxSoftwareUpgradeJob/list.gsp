<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main" />
		<title><g:message code="default.title.Box" /></title>
		<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
		<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
		<script src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
		<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
		<script
			src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_software.upgrade.job.js')}"></script>
		<script>
			Exxo.module = "box-upgrade-job-list";
			Exxo.UI.vars["page-code"] = "box-upgrade-job-list";
			Exxo.Tables.loadPaginationInitValuesFromString('<custom:paginationInfo code="box-upgrade-job-list"/>');
			Exxo.UI.translates['okButton'] = "${message(code:'default.field.buttonOk')}";
			Exxo.UI.translates['cancelButton'] = "${message(code:'default.field.buttonCancel')}";
		</script>
		</head>
	<body>
	<section class="intro">
	  <div class="intro-body">
	    <div class="container full-width">
	      <div class="row">
	        <div class="col-md-12" id="internal-content">
	          <h1> ${ message(code:'generic.list', args:[message(code:'box.software.upgrade.job')]) } </h1>
	          <g:render template="/layouts/messagesAndErrorsTemplate"/>
	          
			  <div class="content">
				<table id="table-list" class="display" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th> ${message(code: 'description')} </th>
							<th> ${message(code: 'status')} </th>
							<th> ${message(code: 'total.completed')} </th>
							<th> ${message(code: 'default.table.action')} </th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th> ${message(code: 'description')} </th>
							<th> ${message(code: 'status')} </th>
							<th> ${message(code: 'total.completed')} </th>
							<th> ${message(code: 'default.table.action')} </th>
						</tr>
					</tfoot>
					<tbody>
						<g:each in="${jobs}" status="i" var="entity">
							<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
								<td>
									<g:link controller="boxSoftwareUpgradeJob" action="show" id="${entity.id}">
										${entity.description.encodeAsHTML()}
									</g:link>
								</td>
								<td>
									${ message(code:'box.job.status.' + entity.status)}
								</td>
								<td>
									${ message(code:'total.completed.detail', args:[entity.completedBoxes, entity.totalBoxes])}
								</td>
								<td>
									<g:link controller="boxSoftwareUpgradeJob" action="show" id="${entity.id}"
										class="action-icons view"
										title="${message(code:'default.action.show')}">
									</g:link>
								</td>
							</tr>
						</g:each>
					</tbody>
				</table>
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	</section>	
	</body>
</html>