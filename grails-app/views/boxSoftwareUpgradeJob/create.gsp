<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
<script
	src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
<script
	src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
<script
	src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_software.upgrade.job.js')}"></script>
	
<script>
	Exxo.module = "software-upgrade-job";
	Exxo.UI.urls["get-exxtractors-url"] = "${g.createLink(controller: 'boxSoftwareUpgradeJob', action: 'getExXtractorsToUpgradeAjax', absolute: true)}";
	Exxo.UI.urls["job-show-url"] = "${g.createLink(controller: 'boxSoftwareUpgradeJob', action: 'show', absolute: true)}";
	Exxo.UI.urls["create-job-url"] = "${g.createLink(controller: 'boxSoftwareUpgradeJob', action: 'createJobAjax', absolute: true)}";
	Exxo.UI.translates['okButton'] = "${message(code:'default.field.buttonOk')}";
	Exxo.UI.translates['cancelButton'] = "${message(code:'default.field.buttonCancel')}";
	Exxo.UI.translates['at-least-one-error'] = "${ message(code:'software.upgrade.at.least.one.error') }";
</script>
</head>
<body>
	<section class="intro">
		<div class="intro-body">
			<div class="container full-width">
				<div class="row">
					<div class="col-md-12" id="internal-content">
						<h1>
							${ message(code:'generic.create', args:[message(code:'box.software.upgrade.job')]) }
						</h1>
						<g:render template="/layouts/messagesAndErrorsTemplate" />
						<div class="content">
							<g:form action="getExXtractorsAjax" name="getDataForm" class="exxo-form">
							<div class="col-md-12" style="padding-bottom: 0px;">								
								<!-- Location -->
								<div class="col-xs-6 resize-formleft">
									<div class="col-xs-4 text-to-left">
										<label for="location">
											${ message(code:'default.field.location') } *
										</label>
									</div>
									<div class="col-xs-8 text-to-left">
										<select id="location" name="location">
											<option value="0" selected="selected">${ message(code:'default.field.all') }</option>
											<g:each in="${locations}" status="i" var="location">
											<option value="${ location.id }" ${ location.id == locationId? 'selected="selected"':'' }>${ location.encodeAsHTML() }</option>
											</g:each>
										</select>
									</div>
									<span class='clearfix'></span>
								</div>		
								
								<!-- Buttons -->						
								<div class="col-xs-6 resize-formright">
									<div class="col-xs-4 text-to-left">
									</div>
									<div class="col-xs-8 text-to-left">
										<a href="" id="getExXtractorsButton">        
									       <span class="form-btn btn-primary" style="margin: 5px 0; width: 255px;"> ${message(code:'get.exxtractors.to.upgrade')} </span>       
								    	</a>
									</div>
									<span class='clearfix'></span>
									
								</div>
								<span class='clearfix'></span>
							</div>
							
							<span class='clearfix'></span>
							<div>
								<br/>
								<div id="tableContainer" style="display: none;">
									<p class="muted"> ${ message(code:'create.software.upgrade.job.note') } </p>
									
									<table id="table-result" class="display" cellspacing="0" width="100%">
								        <thead>
								            <tr>
								            	<th></th>
								                <th>${ message(code:'default.field.location') }</th>
								                <th>${ message(code:'exxtractor.name', args:[message(code:'exxtractor')]) }</th>
								                <th>${ message(code:'exxtractor.serial', args:[message(code:'exxtractor')]) }</th>
								                <th>${ message(code:'current.version') }</th>
								                <th>${ message(code:'upgrade.to.version') }</th>
								                <th>${ message(code:'check.to.upgrade') }</th>
								            </tr>
								        </thead>
								 
								        <tfoot>
								            <tr>
								            	<th></th>
								                <th>${ message(code:'default.field.location') }</th>
								                <th>${ message(code:'exxtractor.name', args:[message(code:'exxtractor')]) }</th>
								                <th>${ message(code:'exxtractor.serial', args:[message(code:'exxtractor')]) }</th>
								                <th>${ message(code:'current.version') }</th>
								                <th>${ message(code:'upgrade.to.version') }</th>
								                <th>${ message(code:'check.to.upgrade') }</th>
								            </tr>
								        </tfoot>
								    </table>
								    
								    <!-- Buttons -->						
									<div class="col-xs-12 resize-formright">
										<div class="col-xs-4 text-to-left">
										</div>
										<div class="col-xs-8 text-to-left">
											<a href="" id="createJobButton">        
										       <span class="form-btn btn-primary" style="width: 225px;"> ${message(code:'create.job')} </span>       
									    	</a>
									    	<a href="" class="back-button">        
										       <span class="form-btn btn-primary"> ${message(code:'default.field.buttonCancel')} </span>       
									    	</a>
										</div>
										<span class='clearfix'></span>
									</div>
									<span class='clearfix'></span>
								</div> <!-- Table Container End -->							
							</div>
							
							
							</g:form>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div id="confirm-job" title="${message(code:'default.confirm.title')}" style="display:none;">
		<p>${ message(code:'confirm.software.upgrade.creation') }</p>
		<table id="internal-content" class="display dataTable">
		<thead>
		<tr><th>${ message(code:'default.field.serial') }</th><th>${ message(code:'upgrade.to.version') }</th></tr>
		</thead>
		<tbody id="tbody-list">
		
		</tbody>
		</table>
	</div>
</body>
</html>