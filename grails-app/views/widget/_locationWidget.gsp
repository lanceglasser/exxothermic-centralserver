<g:render template="/widget/baseWidget" model="[widgetId: "location-widget", widgetTitle:message(code:'location.information', args:[message(code:'default.field.location')])]">
	<div id="widgetLocation" class="display" >
		<div id="location-loading" class="loading-img widget-loading-container">
			<img class="widget-loading-icon" alt="Loading" src="${resource(dir: 'themes/default/img', file: 'ajax-loader.gif')}">
		</div>
		<div id="location-info" style="display: none;">
			<div class="col-md-12" id="nameDiv">
				<div class="col-md-6 title">${ message(code:'default.field.location') }: <span class="field-value"></span></div>
				<span class="col-md-6">
					<sec:ifAnyGranted roles="ROLE_INTEGRATOR, ROLE_HELP_DESK">
						<a id="location-edit-btn" class="align-right" href="#" style="margin-left: 15px;"> ${ message(code:'edit.information') } </a>							
					</sec:ifAnyGranted>
					<a id="location-extra-btn" class="align-right" href="#" style="margin-left: 15px;"> ${ message(code:'extra.information') } </a>
					<a id="app-preview-btn" class="align-right" href="#" style="margin-left: 15px;"> ${ message(code:'app.preview') } </a>
				</span>
				<span class="clearfix"></span>
			</div>
			<span class="clearfix"></span>
			<div class="col-md-12">
				<div class="col-md-2 logo">
					<img id="location-logo" src="" width="100px" height="100px"/>
					<div class="clearer"></div>
					<sec:ifNotGranted roles="ROLE_ADMIN">
						<a id="change-logo-btn" href="#" style="display: none;">${ message(code:'change.logo') }</a>
						<span id="notLogoChangeMsg" style="display: none; font-size: 8px;">${ message(code:'logo.change.not.allow.because.company.set') }</span>
					</sec:ifNotGranted>
				</div>
				<div class="col-md-10">
					<div class="col-md-6 resize-formleft">
						
						<span id="companyDiv">
				            <div class="col-xs-6 text-to-right">
				              <label for="company"> ${ message(code:'default.field.company') } </label>
				            </div>
				            <div class="col-xs-6 text-to-left">
				            	<a id="company-link" class="field-value" href="#"></a>
				            </div>
						</span>
						<span class="clearfix"></span>
						<custom:editField name="commercialIntegrator" value="" label="default.field.commercialntegrator" leftColumnSize="6" />
						<custom:editField name="type" value="" label="default.field.typeOfEstablishment" leftColumnSize="6" />
						<custom:editField name="useCompanyLogo" value="" label="location.use.company.logo" leftColumnSize="6" />
					</div>
					<div class="col-md-6 resize-formleft">
						<custom:editField name="timezone" value="" label="default.field.timezone" />
						<custom:editField name="contentsLimit" value="" label="default.field.contentsLimit" />
						<custom:editField name="enabled" value="" label="default.field.enabled" />
						<custom:editField name="address" value="" label="default.field.address" />
						<custom:editField name="poc" value="" label="point.of.contact" />
					</div>
					<span class="clearfix"></span>
				</div>
				<span class="clearfix"></span>
			</div>
		</div>
	</div> <!-- End of widgetCompany div -->
	
	<div id="upload-logo-window" title="${message(code:'upload.logo', args:[message(code:'default.field.location')])}" style="display:none;">
		<g:uploadForm  name="uploadLocationLogo" action="uploadFile" class="exxo-form" >
			<input type="hidden" id="imageId" name="imageId" value="locationLogo"/>
			<input type="hidden" id="locationId" name="locationId" value="${ location? location.id:0 }"/>
			
			<g:render template="/layouts/tags/imageUploadSelectorTemplate" 
				model="[
					id:				'bgLogoUrl',
					imgValue: 		null,
					label:			message(code:'location.logo'),
					cropImage:		true,
					minWidth: 		mycentralserver.utils.Constants.COMPANY_LOGO_WIDTH,
					minHeight: 		mycentralserver.utils.Constants.COMPANY_LOGO_HEIGHT
					]"
			/>
		</g:uploadForm>
	</div>
	<div id="extra-location-info-window" title="${ message(code:'extra.information') }" style="display:none;">
		<div class="col-md-12">
			<div class="col-md-6 resize-formleft">			
				<custom:editField name="name2" value="" label="field.name" leftColumnSize="6" />
				<custom:editField name="maxOccupancy" value="" label="default.field.maxOccupancy" leftColumnSize="6" />
				<custom:editField name="numberOfTv" value="" label="default.field.numberOfTv" leftColumnSize="6" />
				<custom:editField name="numberOfTvWithExxothermicDevice" value="" label="default.field.numberOfTvWithExxothermiceDevice" leftColumnSize="6" />
				<custom:editField name="createdBy" value="" label="default.field.createdBy"  leftColumnSize="6"/>
			</div>
			<div class="col-md-6 resize-formleft">
				<span id="gpsLocationDiv">
		            <div class="col-xs-6 text-to-right">
		              <label for="gpsLocation"> ${ message(code:'gps.location') } </label>
		            </div>
		            <div class="col-xs-6 text-to-left"><span class="field-value" style="font-size: 10px;"></span> </div>
				</span>
				<span class="clearfix"></span>
				<div class="col-md-12">
					<div id="map-canvas"  style="width: 100%; height: 300px;"></div>
				</div>
			</div>
			<span class="clearfix"></span>
		</div>
		<span class="clearfix"></span>
	</div>
	<div id="app-preview-window" title="${ message(code:'app.preview') }" style="display:none; background-color: #F2F2F2;">
		<div class="col-md-12">
			<!-- App Example Container -->
			<custom:appPreview location="${ location }" />
			<span class="clearfix"></span>
		</div>
		<span class="clearfix"></span>
	</div>
</g:render>