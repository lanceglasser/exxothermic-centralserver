
<div id="${ widgetId }-conf" class="widget-config-window exxo-form" title="${ message(code:'configuration') }" style="display:none;">
	
	<div class="col-md-12">
	
		<g:if test="${ enableAutoRefresh }">
			<script>
				Exxo.UI.translates['widget-refresh-min-max-error'] = "${ message(code:'widgets.refresh.time.minimum.error')}";
			</script>
			<!-- Enable Auto Refresh -->
			<custom:editField type="checkbox" name="${ widgetId }-refreshStatus" required="${false}" value="${false}" leftColumnSize="6"
				label="enable.auto.refresh" cssclass="input-checkbox" tooltip="tooltip.auto.refresh"/>
			
			<!-- Refresh interval time in minutes -->
			<custom:editField type="number" name="${ widgetId }-refreshTime" value="5" leftColumnSize="6"
				label="auto.refresh.interval" cssclass="onlyNumbers" min="3" max="8" step="1" tooltip="tooltip.auto.refresh.interval"/>
		</g:if>
		
			
		<!-- 
		<div class="col-md-6 resize-formleft">			
			<custom:editField name="name2" value="" label="field.name" leftColumnSize="6" />
		</div>
		<div class="col-md-6 resize-formleft">
			
		</div>
		<span class="clearfix"></span>
		 -->
	</div>
	<span class="clearfix"></span>
	
	<div class="col-md-12">
		${body()}
		<span class="clearfix"></span>
	</div>
	<span class="clearfix"></span>
</div>