<g:render template="/widget/baseWidget" model="[widgetId: "appSkin-widget", widgetTitle:message(code:'default.home.manageSkin')]">
	<table id="widgetAppSkinsTable" class="dataTable display widgetDataTable">
		<thead>
			<tr>
				<th>${ message(code:'default.field.name') }</th>
				<th>${ message(code:'default.field.title') }</th>
				<th>${ message(code:'preview.files') }</th>				
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</g:render>
<script>
	Exxo.UI.urls["settings-edit-url"] = "${g.createLink(controller: 'appSkin', action: 'edit', absolute: true)}/";
</script>