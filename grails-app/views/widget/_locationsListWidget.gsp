<g:render template="/widget/baseWidget" model="[widgetId: "locations-widget", widgetTitle:message(code:'default.home.locations')]">
	<table id="widgetLocationsTable" class="dataTable display widgetDataTable">
		<thead>
			<tr>
				<th>${ message(code:'default.field.name') }</th>
				<th>${ message(code:'default.field.enabled') }</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</g:render>