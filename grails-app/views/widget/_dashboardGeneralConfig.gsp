<!-- Image Crop Required Files -->
<link rel="stylesheet" href="${resource(dir: 'css/jcrop', file: 'jquery.Jcrop.css')}" type="text/css">
<script src="${resource(dir: 'js/jcrop', file: 'jquery.Jcrop.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_images_upload.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_utils.js')}"></script>

<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_widget_company.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_widget_location.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_widget_locations.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_widget_exxtractors.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_widget_banners.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_widget_app_skin.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_widget_offers.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_widget_documents.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_widget_employees.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_widget_stats.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_widget_welcome_ad.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_widget_wa_app_theme.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_widget_shortcuts.js')}"></script>


<script>
	Exxo.module = "index";	
	Exxo.UI.translates['addButton'] = "${ message(code:'default.field.buttonAddOther') }";
	Exxo.UI.translates['cancelButton'] = "${ message(code:'default.field.buttonCancel') }";
	Exxo.UI.translates['uploadButton'] = "${ message(code:'default.button.upload') }";
	Exxo.UI.translates['closeButton'] = "${ message(code:'default.field.buttonClose') }";
	Exxo.UI.translates['saveButton'] = "${ message(code:'default.field.buttonSave') }";
	Exxo.UI.translates['registerButton'] = "${ message(code:'default.field.buttonRegister') }";
	Exxo.UI.translates['WidgetConfigIcon'] = "${ message(code:'configuration') }";
	Exxo.UI.translates['select-image-before-save'] = "${message(code:'select.image.before.save')}";
	Exxo.UI.translates['WidgetListIcon'] = "${ message(code:'icon.list.title') }";
	Exxo.UI.translates['WidgetRefreshIcon'] = "${ message(code:'icon.refresh.title') }";
	Exxo.UI.translates['WidgetMinIcon'] = "${ message(code:'icon.min.title') }";
	Exxo.UI.translates['WidgetMaxIcon'] = "${ message(code:'icon.max.title') }";
	Exxo.UI.translates['WidgetDelIcon'] = "${ message(code:'icon.del.title') }";
	Exxo.UI.translates['dashboard-titles-ref'] = "${ message(code:'general.dashboard.titles.ref')}";
	Exxo.UI.translates['remove'] = "${ message(code:'remove')}";
	Exxo.UI.translates['widget-stats-info-msg'] = "${ message(code:'widget.stats.info.msg', args:[message(code:'venue.servers')]) }";
	Exxo.UI.translates['widget-shortcuts-info-msg'] = "${ message(code:'widget.shortcuts.info.msg') }";
	Exxo.UI.translates['widget-companies-list-info-msg'] = "${ message(code:'widget.companies.list.info.msg') }";
	Exxo.UI.translates['widget-locations-list-info-msg'] = "${ message(code:'widget.locations.list.info.msg',args:[message(code:'venue.servers')]) }";
	Exxo.UI.translates['widget-exxtractors-list-info-msg'] = "${ message(code:'widget.exxtractors.list.info.msg',args:[message(code:'venue.servers')]) }";
	Exxo.UI.translates['widget-banners-list-info-msg'] = "${ message(code:'widget.banners.list.info.msg') }";
	Exxo.UI.translates['widget-offers-list-info-msg'] = "${ message(code:'widget.offers.list.info.msg') }";
	Exxo.UI.translates['widget-documents-list-info-msg'] = "${ message(code:'widget.documents.list.info.msg') }";
	Exxo.UI.translates['widget-app-theme-list-info-msg'] = "${ message(code:'widget.app.theme.settings.list.info.msg') }";
	Exxo.UI.translates['widget-wa-list-info-msg'] = "${ message(code:'widget.welcome.ad.list.info.msg') }";
	Exxo.UI.translates['widget-app-theme-wa-info-msg'] = "${ message(code:'widget.app.theme.and.wa.info.msg') }";
	Exxo.UI.translates['widget-employees-list-info-msg'] = "${ message(code:'widget.employees.list.info.msg') }";
	Exxo.UI.translates['widget-company-info-msg'] = "${ message(code:'widget.company.info.msg') }";
	Exxo.UI.translates['widget-location-info-msg'] = "${ message(code:'widget.location.info.msg') }";
	Exxo.UI.translates['widget-welcome-ad-type-image'] = "${ message(code:'welcome.message.type.IMAGE') }";
	Exxo.UI.translates['widget-welcome-ad-type-video'] = "${ message(code:'welcome.message.type.VIDEO') }";
	Exxo.UI.translates["no-more-widgets-to-add"] = "${ message(code:'no-more-widgets-to-add')}";
			
	Exxo.UI.urls["widget-get-data-url"] = "${g.createLink(controller: 'widget', action: 'getListDataAjax', absolute: true)}";
	Exxo.UI.urls["widget-get-detailed-data-url"] = "${g.createLink(controller: 'widget', action: 'getDataAjax', absolute: true)}";
	Exxo.UI.urls["widget-save-config-url"] = "${g.createLink(controller: 'widget', action: 'saveDashboardConfigAjax', absolute: true)}";
	Exxo.UI.urls['companies-list'] = "${g.createLink(controller: 'company', action: 'index', absolute: true)}";	
	Exxo.UI.urls['locations-list'] = "${g.createLink(controller: 'location', action: 'listAll', absolute: true)}";
	Exxo.UI.urls['employees-list'] = "${g.createLink(controller: 'employee', action: 'listAll', absolute: true)}";
	Exxo.UI.urls['exxtractors-list'] = "${g.createLink(controller: 'box', action: 'listAll', absolute: true)}";
	Exxo.UI.urls['banners-list'] = "${g.createLink(controller: 'content', action: 'listAll', absolute: true)}";
	Exxo.UI.urls['offers-list'] = "${g.createLink(controller: 'offer', action: 'listAll', absolute: true)}";
	Exxo.UI.urls['documents-list'] = "${g.createLink(controller: 'document', action: 'listAll', absolute: true)}";
	Exxo.UI.urls['app-theme-list'] = "${g.createLink(controller: 'appSkin', action: 'listAll', absolute: true)}";
	Exxo.UI.urls['welcome-ad-list'] = "${g.createLink(controller: 'welcomeAd', action: 'listAll', absolute: true)}";	
	Exxo.UI.urls["company-dashboard-url"] = "${g.createLink(controller: 'company', action: 'dashboard', absolute: true)}/";
	Exxo.UI.urls["location-dashboard-url"] = "${g.createLink(controller: 'location', action: 'dashboard', absolute: true)}/";
	Exxo.UI.urls["employee-show-url"] = "${g.createLink(controller: 'employee', action: 'show', absolute: true)}/";
	Exxo.UI.urls["edit-location-url"] = "${g.createLink(controller: 'location', action: 'edit', absolute: true)}/";
	Exxo.UI.urls["upload-file"] = "${g.createLink(controller: 'widget', action: 'uploadFile', absolute: true)}";
	Exxo.UI.urls["register-exx"] = "${g.createLink(controller: 'box', action: 'registerAjax', absolute: true)}";
	Exxo.UI.urls["get-widget-html"] = "${g.createLink(controller: 'widget', action: 'getWidgetHtml', absolute: true)}";
	
	Exxo.UI.vars["company-id"] = 0;
	Exxo.UI.vars["location-id"] = 0;
</script>

<div id="temp-add-widget" style="display: none;"></div>