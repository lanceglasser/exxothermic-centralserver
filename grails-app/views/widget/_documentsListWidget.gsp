<g:render template="/widget/baseWidget" model="[widgetId: "documents-widget", widgetTitle:message(code:'default.home.documents')]">
	<table id="widgetDocumentsTable" class="dataTable display widgetDataTable">
		<thead>
			<tr>
				<th>${ message(code:'default.field.name') }</th>
				<th>${ message(code:'default.field.category') }</th>
				<th>${ message(code:'default.field.expirationDate') }</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</g:render>
<script>
	Exxo.UI.urls["documents-edit-url"] = "${g.createLink(controller: 'document', action: 'edit', absolute: true)}/";
	Exxo.UI.vars["can-edit-documents"] = false;
	<sec:ifNotGranted roles="ROLE_ADMIN">
	Exxo.UI.vars["can-edit-documents"] = true;
	</sec:ifNotGranted>	
</script>