<g:render template="/widget/baseWidget" model="[widgetId: "waAppTheme-widget", widgetTitle:message(code:'wa.theme')]">
	<div id="widgetLocation" class="display" >
	
		<div id="waAppTheme-loading" style="display: none;" class="loading-img widget-loading-container">
			<img class="widget-loading-icon" alt="Loading" src="${resource(dir: 'themes/default/img', file: 'ajax-loader.gif')}">
		</div>
		<!-- welcome ad section -->
		<div id="welcome-ad-info" style="display: none;">
		
			<div class="col-md-12" id="nameDiv">
				<div class="col-md-12 title">${ message(code:'default.home.welcome.ad') }: 
					<span id="wName" class="field-value"></span>
				</div>
				<span class="clearfix"></span>
			</div>
			<span class="clearfix"></span>
			
			<br/>			
			<div class="col-md-12" id="wa-info-container">
				<div class="col-md-4">					
					<img id="smallImageUrl" src="" width="80px"/>
					<span id="wa-video-preview-msg">${ message(code:'wa.app.theme.widget.no.preview') }</span>
				</div>
				<div class="col-md-8">
					<div class="col-md-12 resize-formleft">						
						<span id="welcomeAdDiv">     
				            <div class="col-xs-12 text-to-left">
				              <label id="skipEnable"> <!-- content here with JS --></label>
				            </div>
				            <span class="clearfix"></span>
				            
				            <div class="col-xs-12 text-to-left">
				              <label id="skipTime"> <!-- content here with JS --> </label>
				            </div>
				            <span class="clearfix"></span>
				            
				            <div class="col-xs-12 text-to-left">
				              <label for="welcomeType">${ message(code:'default.field.type') }: </label>
				              <span id="welcomeType" class="field-value"><!-- content here with JS --></span>
				            </div>
				            
				            <span class="clearfix"></span>
						</span>
						<span class="clearfix"></span>
						
					</div>					
					<span class="clearfix"></span>
				</div>
								
				<span class="clearfix"></span>
				
			</div>
		</div>
		
		
		<!-- app theme section -->	
		<br/>			
		<div id="app-theme-info" style="display: none;">								
		
			<div class="col-md-12" id="nameDiv">
				<div class="col-md-12 title"><br/>${ message(code:'default.field.appSkin') }: <span id="aName" class="field-value"></span></div>				
				<span class="clearfix"></span>
			</div>
			
			<span class="clearfix"></span>
			<div class="col-md-12">
				<div class="col-md-4 logo">
					<img id="backgroundImageUrl" src="" width="120px" height="60px"/>
					<span id="aps-video-preview-msg">${ message(code:'wa.app.theme.widget.no.preview') }</span>
					<span class="clearfix"></span>
				</div>
				<div class="col-md-8">
					<div class="col-md-12 resize-formleft">
						
						<span id="appThemeDiv">
							
							<div class="col-xs-4 text-to-left">
				              <label for="title"> ${ message(code:'default.field.title') } </label>
				            </div>
				            <div class="col-xs-8 text-to-right">
				            	<span id="title" class="field-value"></span>
				            </div>
				            <span class="clearfix"></span>
				            
				            <div class="col-xs-8 text-to-left">
				              <label for="primaryColor"> ${ message(code:'default.field.main.color') } </label>
				            </div>
				            <div class="col-xs-4 text-to-right">
				            	<i id="primaryColor" class="channel-color" style="display: inline-block;"></i>
				            </div>
				            <span class="clearfix"></span>
				            <div class="col-xs-8 text-to-left">
				              <label for="secondaryColor"> ${ message(code:'default.field.secondary.color') } </label>
				            </div>
				            <div class="col-xs-4 text-to-right">
				            	<i id="secondaryColor" class="channel-color"  style="display: inline-block;"></i>
				            </div>
				            <span class="clearfix"></span>
						</span>
						<span class="clearfix"></span>
						
					</div>					
					<span class="clearfix"></span>
				</div>
				<span class="clearfix"></span>
			</div>
		</div>
	</div>
</g:render>