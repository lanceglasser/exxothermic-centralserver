<g:render template="/widget/baseWidget" model="[widgetId: "company-widget", widgetTitle:message(code:'company.information')]">
	<div id="widgetCompany" class="display" >
		<div id="company-loading" class="loading-img widget-loading-container">
			<img class="widget-loading-icon" alt="Loading" src="${resource(dir: 'themes/default/img', file: 'ajax-loader.gif')}">
		</div>
		<div id="company-info" style="display: none;">
			<div class="col-md-12" id="nameDiv">
				<div class="col-md-6 title">${ message(code:'company') }: <span class="field-value"></span></div>
				<span class="col-md-6">
					<sec:ifAnyGranted  roles="ROLE_INTEGRATOR, ROLE_HELP_DESK">
						<a id="company-edit-btn" class="align-right" href="#">${ message(code:'edit.information') }</a>							
					</sec:ifAnyGranted>
				</span>
				<div class="clearer"></div>
			</div>
			<div class="clearer"></div>
			<div class="col-md-12">
				<div class="col-md-2 logo">
					<img id="company-logo" src="" width="150px" height="150px"/>
					<div class="clearer"></div>
					<sec:ifNotGranted roles="ROLE_ADMIN">
						<a id="change-logo-btn" href="#">${ message(code:'change.logo') }</a>
					</sec:ifNotGranted>
				</div>
				<div class="col-md-10">
					<div class="col-md-6 resize-formleft">
						<custom:editField name="webSite" value="" label="default.field.webSite" leftColumnSize="6" />
						<custom:editField name="phoneNumber" value="" label="field.phone.number" leftColumnSize="6" />
						<custom:editField name="companyWideIntegrator" value="" label="default.field.worldIntegrator" leftColumnSize="6" />
						<custom:editField name="typeOfCompany" value="" label="type" leftColumnSize="6" />
						<custom:editField name="typeOfEstablishment" value="" label="default.field.typeOfEstablishment" leftColumnSize="6" />
					</div>
					<div class="col-md-6 resize-formleft">
						<custom:editField name="timezone" value="" label="default.field.timezone" />
						<custom:editField name="zipCode" value="" label="default.field.zipCode" />
						<custom:editField name="enabled" value="" label="default.field.enabled" />
						<custom:editField name="address" value="" label="default.field.address" />
						<custom:editField name="poc" value="" label="point.of.contact" />
					</div>
					<div class="clearer"></div>
				</div>
				<div class="clearer"></div>
			</div>
		</div>
	</div> <!-- End of widgetCompany div -->
	
	<div id="upload-logo-window" title="${message(code:'upload.logo', args:[message(code:'company')])}" style="display:none;">
		<g:uploadForm  name="uploadLogo" action="uploadFile" class="exxo-form" >
			<input type="hidden" id="imageId" name="imageId" value="companyLogo"/>
			<input type="hidden" id="companyId" name="companyId" value="${ company? company.id:0 }"/>
			
			<g:render template="/layouts/tags/imageUploadSelectorTemplate" 
				model="[
					id:				'bgLogoUrl',
					imgValue: 		null,
					label:			message(code:'company.logo'),
					cropImage:		true,
					minWidth: 		mycentralserver.utils.Constants.COMPANY_LOGO_WIDTH,
					minHeight: 		mycentralserver.utils.Constants.COMPANY_LOGO_HEIGHT
					]"
			/>
		</g:uploadForm>
	</div>
</g:render>