<g:render template="/widget/baseWidget" model="[widgetId: "banners-widget", widgetTitle:message(code:'default.home.banners')]">
	<table id="widgetBannersTable" class="dataTable display widgetDataTable">
		<thead>
			<tr>
				<th>${ message(code:'default.field.name') }</th>
				<th>${ message(code:'preview.files') }</th>
				<th>${ message(code:'default.field.enabled') }</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</g:render>
<script>
	Exxo.UI.urls["banners-edit-url"] = "${g.createLink(controller: 'content', action: 'edit', absolute: true)}/";
	Exxo.UI.vars["can-edit-banners"] = false;
	<sec:ifNotGranted roles="ROLE_ADMIN">
	Exxo.UI.vars["can-edit-banners"] = true;
	</sec:ifNotGranted>	
</script>