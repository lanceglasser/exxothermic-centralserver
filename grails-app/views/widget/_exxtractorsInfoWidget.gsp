<g:render template="/widget/baseWidget" model="[widgetId: "exxtractors-info-widget", widgetTitle:message(code:'venue.servers.stats', args:[message(code:'venue.servers')])]">
	<div id="widgetExxtractorsInfo" class="display" >
		<div id="exxtractors-info-loading" class="loading-img widget-loading-container">
			<img class="widget-loading-icon" alt="Loading" src="${resource(dir: 'themes/default/img', file: 'ajax-loader.gif')}">
		</div>
		<div id="exxtractors-info-container" style="display: none;">
			<div class="col-md-12" style="padding-right: 0px; padding-left: 0px;">
				<div class="col-md-6" style="padding-right: 0px; padding-left: 0px;">
					<h2>${ message(code:'some.status', args:[message(code:'connectivity')]) }</h2>
					<div id="connectivityStatusContainer" class="col-md-12" style="padding-right: 0px; padding-left: 0px;">
					</div>
					<span class="clearfix"></span>
				</div>
				<div class="col-md-6" style="padding-right: 0px; padding-left: 0px;">
					<h2>${ message(code:'some.status', args:[message(code:'software.version')]) }</h2>
					<div id="softwareVersionStatusContainer" class="col-md-12" style="padding-right: 0px; padding-left: 0px;">
					</div>
					<span class="clearfix"></span>
				</div>
				<span class="clearfix"></span>
			</div>
			<span class="clearfix"></span>
			<div class="col-md-12" style="padding-right: 0px; padding-left: 0px;">
				<span class="unsupportedNote">
					${ message(code:'unsupported.exxtractors', args:[message(code:'venue.servers')]) }: <span id="unsupported-div"></span></span>
				<span class="unversionedNote">
					${ message(code:'unversioned.exxtractors', args:[message(code:'venue.servers')]) }: <span id="unversioned-div"></span></span>
				</span>
			</div>
			<span class="clearfix"></span>
		</div>
	</div>
	<g:render template="/widget/baseConfigWidget" 
		model="[widgetId: "exxtractors-info-widget", widgetTitle:message(code:'venue.servers.stats', args:[message(code:'venue.servers')]),
			enableAutoRefresh:true]">
	
	</g:render>	
</g:render>