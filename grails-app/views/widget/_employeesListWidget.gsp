<g:render template="/widget/baseWidget" model="[widgetId: "employees-widget", widgetTitle:message(code:'default.home.employees')]">
	<table id="widgetEmployeesTable" class="dataTable display widgetDataTable">
		<thead>
			<tr>
				<th>${ message(code: 'default.field.enabled') }</th>
				<th>${ message(code:'default.field.name') }</th>
				<th>${ message(code:'field.email') }</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</g:render>