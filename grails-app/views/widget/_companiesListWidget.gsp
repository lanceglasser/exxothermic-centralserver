<g:render template="/widget/baseWidget" model="[widgetId: "companies-widget", widgetTitle:message(code:'default.title.listCompany')]">
	<table id="widgetCompaniesTable" class="dataTable display widgetDataTable">
		<thead>
			<tr>
				<th>${ message(code:'default.field.name') }</th>
				<th>${ message(code:'default.field.enabled') }</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</g:render>