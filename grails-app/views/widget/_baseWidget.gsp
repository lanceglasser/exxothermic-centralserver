<li id="${ widgetId }" widget-title="${ widgetTitle }"
	data-row="${ row }" data-col="${ col }" data-sizex="${ sizex }" data-sizey="${ sizey }" status="${ status }"
	maximizeHeight="${ maxH }" maximizeX="${ maxX }" maximizeY="${ maxY }" refreshStatus="${ refreshStatus }" refreshTime="${ refreshTime }"
	>
	${body()}
</li>