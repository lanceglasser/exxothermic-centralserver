<g:render template="/widget/baseWidget" model="[widgetId: "shortcuts-widget", widgetTitle:message(code:'shortcuts')]">
	<div id="widgetShortcuts" class="display" >
		<div id="loading-div" class="loading-img widget-loading-container">
			<img class="widget-loading-icon" alt="Loading" src="${resource(dir: 'themes/default/img', file: 'ajax-loader.gif')}">
		</div>
		<div id="main-content" style="display: none;">
		
		</div>		
	</div>
	
	<g:render template="/widget/baseConfigWidget" model="[widgetId: "shortcuts-widget", widgetTitle:message(code:'shortcuts')]">
		<div id="loading-shortcuts-list-div" class="loading-img widget-loading-container">
			<img class="widget-loading-icon" alt="Loading" src="${resource(dir: 'themes/default/img', file: 'ajax-loader.gif')}">
		</div>
		<div id="shortcuts-list">
			<span class="instruction">${ message(code:'shortcuts.selection.message', args:[mycentralserver.utils.Constants.MAXIMUM_SHORTCUTS]) }</span>
			<span class="clearfix"></span>
			<div id="shortcuts-list-container">
			
			</div>
		</div>
	</g:render>
	
	<div id="reg-exx-window" class="modal-form" title="${ message(code:'register.to.this.location') }" style="display:none;">
	
		<div class="col-md-12 content resize-formleft">
			<g:form action="" name="reg-exx-form" id="reg-exx-form" class="exxo-form">
				
				<div class="alert alert-warning">	
					<g:message code="default.box.register.note" />
				</div>
				<div id="reg-exx-error" class="alert alert-error" style="display: none;">
				</div>
				
				<g:if test="${ location }">
					<input type="hidden" id="location" name="location.id" value="${ location.id }"/>
				</g:if>
				
				<!-- Serial Number  -->
				<custom:editField type="text" name="serial" leftColumnSize="3" required="${true}" value="" max="50"
					label="default.field.serial" autocomplete="off" note="${ message(code: 'box.serial.help') }" cssclass="uppercase"/>
					
				<!-- Name : Optional  -->				
				<custom:editField type="text" name="name" leftColumnSize="3" required="${false}" value="" max="50"
					label="${ message(code: 'default.field.name') + " " + message(code: 'default.field.optionaL') }" 
					note="${ message(code: 'default.box.name.note') }"/>
			</g:form>
		</div>
		<span class="clearfix"></span>
	</div>
	
	<script>
		Exxo.UI.vars["max-shortcuts"] = ${ mycentralserver.utils.Constants.MAXIMUM_SHORTCUTS };
		Exxo.UI.translates['min-shortcuts-error'] = "${ message(code:'shortcuts.minimum.error.message')}";
		Exxo.UI.translates['max-shortcuts-error'] = "${ message(code:'shortcuts.maximum.error.message', args:[mycentralserver.utils.Constants.MAXIMUM_SHORTCUTS])}";
	</script>
</g:render>