<g:render template="/widget/baseWidget" model="[widgetId: "exxtractors-widget", widgetTitle:message(code:'venue.servers')]">
	<table id="widgetExxtractorsTable" class="dataTable display widgetDataTable">
		<thead>
			<tr>
				<th>${ message(code:'default.field.online') }</th>
				<th>${ message(code:'default.field.name') }</th>
				<th>${ message(code:'default.field.location') }</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</g:render>
<script>
	Exxo.UI.urls["exttractors-edit-url"] = "${g.createLink(controller: 'box', action: 'edit', absolute: true)}/";
</script>