<g:render template="/widget/baseWidget" model="[widgetId: "welcomeList-widget", widgetTitle:message(code:'welcome.ad.menu.manage')]">
	<table id="widgetWelcomeListTable" class="dataTable display widgetDataTable">
		<thead>
			<tr>
				<th>${ message(code:'default.field.name') }</th>
				<th>${ message(code:'default.field.type') }</th>
				<th>${ message(code:'preview.files') }</th>				
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</g:render>
<script>
Exxo.UI.translates['download'] = "${ message(code:'default.action.download') }";
Exxo.UI.urls["wa-edit-url"] = "${g.createLink(controller: 'welcomeAd', action: 'edit', absolute: true)}/";
</script>