<g:render template="/widget/baseWidget" model="[widgetId: "offers-widget", widgetTitle:message(code:'default.home.offers')]">
	<table id="widgetOffersTable" class="dataTable display widgetDataTable">
		<thead>
			<tr>
				<th>${ message(code:'default.field.name') }</th>
				<th>${ message(code:'preview.files') }</th>
				<th>${ message(code:'default.field.enabled') }</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</g:render>
<script>
	Exxo.UI.urls["offers-edit-url"] = "${g.createLink(controller: 'offer', action: 'edit', absolute: true)}/";
	Exxo.UI.vars["can-edit-offers"] = false;
	<sec:ifNotGranted roles="ROLE_ADMIN">
	Exxo.UI.vars["can-edit-offers"] = true;
	</sec:ifNotGranted>	
</script>