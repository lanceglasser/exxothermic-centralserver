<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main" />

		<link rel="stylesheet" type="text/css"
			href="${resource(dir: 'themes/default/css', file: 'widgets.css')}">  
		
		<script src="${resource(dir: 'themes/default/js/widget', file: 'jquery.exxo.widget.js')}"></script>
		
		<script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1','packages':['corechart']}]}"></script>
		
		<!-- Gridster Start -->
		<script src="${resource(dir: 'themes/default/js', file: 'jquery.draggable.js')}"></script>
		<script src="${resource(dir: 'themes/default/js', file: 'jquery.coords.js')}"></script>
		<script src="${resource(dir: 'themes/default/js/widget', file: 'jquery.collision.js')}"></script>
		<script src="${resource(dir: 'themes/default/js/widget', file: 'utils.js')}"></script>
		<script src="${resource(dir: 'themes/default/js/widget', file: 'jquery.gridster.js')}"></script>
		<script src="${resource(dir: 'themes/default/js/widget', file: 'jquery.gridster.extras.js')}"></script>
		
		<link rel="stylesheet" type="text/css"
			href="${resource(dir: 'themes/default/css', file: 'jquery.gridster.css')}">
		<!-- Gridster End -->
		
		<link rel="stylesheet" type="text/css"
			href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
		<link rel="stylesheet" type="text/css"
			href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
		<script
			src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
		<script
			src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
		<script
			src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_user_dashboard.js')}"></script>
		
			
		<g:render template="/widget/dashboardGeneralConfig"></g:render>
		
		<script>
			<sec:ifAnyGranted roles="ROLE_ADMIN">
			Exxo.UI.vars["can-edit-company"] = false;
			</sec:ifAnyGranted>
			<sec:ifNotGranted roles="ROLE_ADMIN">
			Exxo.UI.vars["can-edit-company"] = true;
			</sec:ifNotGranted>
			Exxo.module = "index";
			Exxo.UI.vars["dashboard-name"] = "companyDashboard";
			Exxo.UI.vars["company-id"] = ${ company.id };
			Exxo.UI.urls["edit-company-url"] = "${g.createLink(controller: 'company', action: 'edit', absolute: true)}/";
			Exxo.UI.urls["edit-integrator-url"] = "${g.createLink(controller: 'companyIntegrator', action: 'edit', absolute: true)}/";
			Exxo.UI.translates['dashboard-titles-ref'] = "${ message(code:'company.dashboard.titles.ref')}";
		</script>
		
	</head>
	<body>
		<section class="introp">
	  		<div class="intro-body">
	    		<div class="container full-width">
	      			<div class="row">
						<div class="col-md-12" id="internal-content">
						  	
						  	<div class="col-md-12 dashboard-header-line">
						  		<div id="crumbs">
										<ul class="left">
											<li>
												<g:link controller="home" action="index" >
													<img class="title-icon" src="${resource(dir: 'themes/default/img/icons', file: 'home-white.png')}" alt="Home"/>
												</g:link>
											</li>
											<li><a href="" class="current">${ message(code:'dashboard.title', args:[message(code:'default.field.companyName')]) }</a></li>											
										</ul>
										
										<span class="left add-widget circle" title="${ message(code:'add.widget') }"></span>
										<span id="how-use-widgets-btn" class="left help-widget circle"  title="${ message(code:'how.use.widgets') }"></span>
								</div>	
						  		
						  		<span class="clearfix"></span>
						  	</div>
						  	<span class="clearfix"></span>
						  	
							
						  	<!-- Renders the Main Dashboard for Current User -->
						  	<div style="width: 1500px; position: relative;">
							<user:companyDashboard />
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</body>
</html>
