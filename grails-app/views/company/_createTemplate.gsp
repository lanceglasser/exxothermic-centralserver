<g:render template="/layouts/messagesAndErrorsTemplate" model="[entity: company]"/>

<g:uploadForm action="save" name="wizardForm" class="exxo-form">
	<div class="col-xs-6 resize-formleft">
		<custom:editField type="text" name="name" required="${true}" value="${company.name}" label="default.field.companyName" 
		  tooltip="${message(code:'tooltip.company.name')}" max="50"/>
		<custom:editField type="url" name="webSite" value="${company.webSite}" label="default.field.webSite" max="100"/>
		<custom:editField type="phoneNumber" name="phoneNumber" max="20" value="${company.phoneNumber}" label="field.phone.number" />
		
		<div class="col-xs-4 text-to-right">
			<label for="type">
				<div class="questionToolTip"
					title="${message(code:'tooltip.typeOfEstablishment')}"></div> <g:message
					code="default.field.typeOfEstablishment" />*
			</label>
		</div>
		<div class="col-xs-8 text-to-left">
			<custom:selectWithOptGroupCompanyType id="type"
				from="${typesCompany}" value="${company.type?.code}" />
		</div>
		<span class="clearfix"></span>

		<div id="controlCompanyOtherType">
			<custom:editField type="text" name="otherType" required="${true}" value="${company.otherType}" label="default.field.specify" max="20"/>
		</div>
		
		<g:if test="${comingFromWizard == false}">
		<!-- Logo Image -->
		<g:render template="/layouts/tags/imageUploadSelectorTemplate" 
			model="[
				id:				'bgLogoUrl',
				imgValue: 		company.logoUrl,
				label:			message(code:'company.logo'),
				cropImage:		true,
				minWidth: 		mycentralserver.utils.Constants.COMPANY_LOGO_WIDTH,
				minHeight: 		mycentralserver.utils.Constants.COMPANY_LOGO_HEIGHT
				]"
		/>
		</g:if>
	</div>
	<div class="col-xs-6 resize-formright">
		<!-- Level of Service -->
		<div class="col-xs-4 text-to-right">
			<label for="levelOfService"><g:message code="company.level.of.service" />*</label>
		</div>
		<div class="col-xs-8 text-to-left">
			<select name="levelOfService" id="levelOfService">
				<option value="1" ${ company.levelOfService == 1? 'selected:"selected"':'' }> ${ message(code:'company.level.of.service.1') }</option>
				<option value="2" ${ company.levelOfService == 2? 'selected:"selected"':'' }> ${ message(code:'company.level.of.service.2') }</option>
			</select>
		</div>
		<span class="clearfix"></span>
		
		<div class="col-xs-4 text-to-right">
			<label for="country"><g:message code="default.field.country" />*</label>
		</div>
		<div class="col-xs-8 text-to-left">
			<g:select name="country.id" id="country" from="${countries}"
				optionKey="id"
				optionValue="${ {shortName->g.message(code:'country.code.'+shortName) } }"
				value="${company.country?.id}" />
		</div>
		<span class="clearfix"></span>

		<g:if test="${company.state}">
			<div id="stateDiv">
		</g:if>
		<g:else>
			<div id="stateDiv" style="display: none;">
		</g:else>
		<div class="col-xs-4 text-to-right">
			<label for="state"><g:message
					code="default.field.stateOrProvince" />*</label>
		</div>
		<div class="col-xs-8 text-to-left">
			<g:select name="state.id" id="state" from="${states}" optionKey="id"
				optionValue="${ {name->g.message(code:'state.code.'+name) } }"
				value="${company.state?.id}" />
		</div>
		<span class="clearfix"></span>
		</div>

		<g:if test="${company.state}">
			<div id="stateNameDiv" style="display: none;">
		</g:if>
		<g:else>
			<div id="stateNameDiv">
		</g:else>
			<custom:editField type="text" name="stateName" value="${company.stateName}" label="default.field.stateOrProvince" />
		</div>

		<custom:editField type="text" name="city" max="100" required="${true}" value="${company.city}" label="default.field.city" />
		
		<div class="col-xs-4 text-to-right">
		  <label for="type"><g:message code="default.field.timezone" /> *</label>
		</div>
		<div class="col-xs-8 text-to-left">
		  <custom:timeZoneSelect name="timezone" value="${company.timezone}" />
		</div>
		<span class="clearfix"></span>
		
		<custom:editField type="zipCode" name="zipCode" required="${true}" value="${company.zipCode}" label="default.field.zipCode" tooltip="${message(code:'tooltip.zipCode')}" cssclass="onlyNumbers"/>
				
		<custom:editField type="textarea" name="address" min="7" required="${true}" value="${company.address}" label="default.field.address" max="100"/>
			
		<g:if test="${comingFromWizard == false}">
			<custom:editField type="checkbox" name="enable" value="${company.enable}" cssclass="input-checkbox" label="default.field.enabled" tooltip="${message(code:'tooltip.company.enable')}" />
		</g:if>
	</div>
	<span class="clearfix"></span>

	<g:if test="${comingFromWizard == false}">
		<g:submitButton class="form-btn btn-primary" name="save"
			value="${message(code:'default.field.buttonSave')}" />

		<g:link action="index" controller="company" class="back-button">
			<span class="form-btn btn-primary"> ${message(code:'default.field.buttonCancel')}
			</span>
		</g:link>
	</g:if>
	<g:else>
		<g:if test="${company.id != null}">
			<g:submitButton disabled="true" name="continue"
				value="${message(code:'default.field.buttonContinue')}"
				class="form-btn btn-primary" />
		</g:if>
		<g:else>
			<g:submitButton name="continue"
				value="${message(code:'default.field.buttonContinue')}"
				class="form-btn btn-primary" />
		</g:else>
		<af:ajaxButton name="cancel"
			value="${message(code:'default.field.buttonCancel')}"
			afterSuccess="onPage();" class="form-btn btn-primary" />

	</g:else>
</g:uploadForm>