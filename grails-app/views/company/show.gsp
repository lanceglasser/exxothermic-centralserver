<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.Company" /></title>
<script>
	Exxo.module = "company-show";
</script>
</head>
<body>
	<section class="intro">
		<div class="intro-body">
			<div class="container full-width">
				<div class="row">
					<div class="col-md-12" id="internal-content">
						<h1>
							${company?.name.encodeAsHTML()}
						</h1>

						<div class="content form-info">
							<!-- Left Column -->
							<div class="col-xs-6 resize-formleft">
								<custom:editField name="levelOfService" value="${message(code:"company.level.of.service." + company.levelOfService)}" label="company.level.of.service" />
								<custom:editField name="webSite" value="${company.webSite ? company.webSite : message(code:"default.field.empty")}" label="default.field.webSite" />
								<custom:editField name="phoneNumber" value="${company.phoneNumber ? company.phoneNumber : message(code:"default.field.empty")}" label="field.phone.number" />
								<custom:editField name="companyWideIntegrator" value="${company.companyWideIntegrator ? company.companyWideIntegrator?.companyName.encodeAsHTML() : message(code:"default.field.empty")}" label="default.field.worldIntegrator" />
								<custom:editField name="typeOfCompany" value="${company.typeOfCompany.name ? company.typeOfCompany.name : message(code:"default.field.empty")}" label="default.field.typeOfCompany" />
								<custom:editField name="typeOfEstablishment" value="${company.type?.name ? message(code:"typeOfEstablishment." + company.type.code.encodeAsHTML()) : message(code:"default.field.empty")}" label="default.field.typeOfEstablishment" />
								<g:if test="${company.otherType}">
									<custom:editField name="otherType" value="${company.otherType}" label="default.field.specify" />
								</g:if>
								<div class="col-xs-4 text-to-right">
									<label>
										<g:message code="company.logo" />
									</label>
								</div>				
								<div class="col-xs-8 text-to-left">
									<g:if test="${ company.logoUrl && company.logoUrl.trim() != "" }">
										<i><img class="ad-background-img" src="${company.logoUrl}" style="width: 100px; height: 100px;"></i>
									</g:if>
									<g:else>
										---
									</g:else>
								</div>
								<span class="clearfix"></span>
							</div>
							<!-- Right Column -->
							<div class="col-xs-6 resize-formright">
								<custom:editField name="pocEmail" value="${company.pocEmail}" label="poc.email" />
								<custom:editField name="pocPhone" value="${company.pocPhone}" label="poc.phone" />
								<custom:editField name="pocFirstName" value="${company.pocFirstName}" label="poc.firstname" />
								<custom:editField name="pocLastName" value="${company.pocLastName}" label="poc.lastname" />
								<custom:editField name="pocAccountCreated" value="${company.pocAccountCreated == true ? message(code:"default.field.yes") : message(code:"default.field.no")}" label="poc.account.created" />
							
								<custom:editField name="country" value="${company.state ? message(code:"country.code." + company.state.country.shortName.encodeAsHTML()) : message(code:"country.code." + company.country.shortName.encodeAsHTML())}" label="default.field.country" />

								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="default.field.stateOrProvince" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
									<g:if test="${company.state}">
										<span class="field-value">${g.message(code:"state.code." + company.state.name)}</span>
									</g:if>
									<g:else>
										<g:if test="${company.stateName}">
											<span class="field-value">${company.stateName.encodeAsHTML()}</span>
										</g:if>
										<g:else>
											<span class="field-value"><g:message code="default.field.empty" /></span>
										</g:else>
									</g:else>
								</div>
								<span class="clearfix"></span>
								
								<custom:editField name="city" value="${company.city ? company.city : message(code:"default.field.empty")}" label="default.field.city" />
								<custom:editField name="timezone" value="${company.timezone ? company.timezone : message(code:"default.field.empty")}" label="default.field.timezone" />
								<custom:editField name="zipCode" value="${company.zipCode ? company.zipCode : message(code:"default.field.empty")}" label="default.field.zipCode" />
								<custom:editField name="address" value="${company.address ? company.address.encodeAsHTML() : message(code:"default.field.empty")}" label="default.field.address" />
								<custom:editField name="enabled" value="${company.enable == true ? message(code:"default.field.yes") : message(code:"default.field.no")}" label="default.field.enabled" />
							</div>
							<span class="clearfix"></span>
							<!-- Buttons -->
							<div class="col-xs-12">
								<g:link action="index" controller="company" class="back-button"> 
									<input type="button" class="form-btn btn-primary" value="${message(code:'default.field.buttonBack')}"/> 
								</g:link>
							</div>
							<span class="clearfix"></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</body>
</html>