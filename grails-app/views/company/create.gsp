<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.newCompany" /></title>
<!-- Image Crop Required Files -->
<link rel="stylesheet" href="${resource(dir: 'css/jcrop', file: 'jquery.Jcrop.css')}" type="text/css">
<script src="${resource(dir: 'js/jcrop', file: 'jquery.Jcrop.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_images_upload.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_utils.js')}"></script>

<!-- Current Page Required Files -->
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_company_create.js')}"></script>


<script>
	Exxo.module = "company-create";
	Exxo.UI.urls["loadStatesByCountryUrl"] = "${g.createLink(controller: 'common', action: 'statebycountry', absolute: true)}";
	Exxo.Company.Create.defaultTimezone = 'UTC';
	var dimensionsImageError = "${message(code:'customButton.image.dimension.error')}";
	var invalidImageError = "${message(code:'image.invalid.error')}";
	var tooBigImageError = "${message(code:'image.big.error')}";
	Exxo.UI.translates[Exxo.Constants.ERROR_FILE_INVALID] = "${message(code:'file.invalid.error')}";
	Exxo.UI.translates[Exxo.Constants.ERROR_FILE_SIZE] = "${message(code:'file.big.error')}";

	Exxo.UI.translates['zipcode-title-msg'] = "${message(code:'default.invalid.string.lenght', args:['X1', 'X2'])}";
</script>
</head>
<body>
	<section class="introp">
		<div class="intro-body">
			<div class="container full-width">
				<div class="row">
					<div class="col-md-12" id="internal-content">
						<h1 id="responsive-header">
							<g:message code="default.title.newCompany" />
						</h1>
						<div class="content">
							<g:render template="createTemplate"></g:render>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</body>
</html>