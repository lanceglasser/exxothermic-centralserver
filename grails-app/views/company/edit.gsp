<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.updateCompany" /></title>
<!-- Image Crop Required Files -->
<link rel="stylesheet" href="${resource(dir: 'css/jcrop', file: 'jquery.Jcrop.css')}" type="text/css">
<script src="${resource(dir: 'js/jcrop', file: 'jquery.Jcrop.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_images_upload.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_utils.js')}"></script>

<!-- Current Page Required Files -->
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_company_create.js')}"></script>
<script>
	Exxo.module = "company-create";
	Exxo.UI.urls["loadStatesByCountryUrl"] = "${g.createLink(controller: 'common', action: 'statebycountry', absolute: true)}";
	Exxo.Company.Create.defaultTimezone = '${(company.timezone!=null)? company.timezone:'UTC'}';
	var dimensionsImageError = "${message(code:'customButton.image.dimension.error')}";
	var invalidImageError = "${message(code:'image.invalid.error')}";
	var tooBigImageError = "${message(code:'image.big.error')}";
	Exxo.UI.translates[Exxo.Constants.ERROR_FILE_INVALID] = "${message(code:'file.invalid.error')}";
	Exxo.UI.translates[Exxo.Constants.ERROR_FILE_SIZE] = "${message(code:'file.big.error')}";
	Exxo.UI.vars['company.zip.code']="${company.zipCode}";
	Exxo.UI.translates['zipcode-title-msg'] = "${message(code:'default.invalid.string.lenght', args:['X1', 'X2'])}";
</script>
</head>
<body>
	<section class="introp">
		<div class="intro-body">
			<div class="container full-width">
				<div class="row">
					<div class="col-md-12" id="internal-content">
						<h1>
							<g:message code="default.title.updateCompany" />
						</h1>
						<div class="content">
							<g:render template="/layouts/messagesAndErrorsTemplate" model="[entity: company]"/>
				      		
							<g:uploadForm action="update" id="${params.id}" name="updateForm" class="exxo-form">
								<g:hiddenField name="id" value="${company?.id}" />
								<g:hiddenField name="version" value="${company?.version}" />
								<g:hiddenField name="offset" value="${offset}" />
																
								<div class="col-xs-6 resize-formleft">
								
									<custom:editField type="text" name="name" required="${true}" value="${company?.name}" label="default.field.companyName" tooltip="${message(code:'tooltip.company.name')}" 
									   max="50"/>
									<custom:editField type="url" name="webSite" value="${company?.webSite}" label="default.field.webSite" max="100"/>
									<custom:editField type="phoneNumber" name="phoneNumber" value="${company.phoneNumber}" label="field.phone.number" max="20" />									

                                    <g:set var="integratorId" value="${0}"/>
                                    <g:if test="${company.companyWideIntegrator}">
                                        <g:set var="integratorId" value="${company.companyWideIntegrator.id}"/>
                                    </g:if>
                                    <sec:ifAnyGranted roles="ROLE_HELP_DESK">
                                    <!-- Allows to set the Integrator company -->
                                    <div class="col-xs-4 text-to-right">
                                        <label for="companyIntegrator"><g:message code="default.field.worldIntegrator" /></label>
                                    </div>
                                    <div class="col-xs-8 text-to-left">
                                        <g:select name="companyIntegrator" id="companyIntegrator"  from="${integrators}"
                                            noSelection="${['0':message(code:'without.integrator')]}"
                                            optionKey="id" optionValue="companyName" value="${integratorId}"/>
                                    </div>
                                    <span class="clearfix"></span>
                                    </sec:ifAnyGranted>
                                    <sec:ifNotGranted roles="ROLE_HELP_DESK">
                                    <input type="hidden" name="companyIntegrator" id="companyIntegrator" value="${integratorId}"/>
                                    <g:if test="${company.companyWideIntegrator}">
                                        <custom:editField type="text" name="name" disabled="${true}" value="${company.companyWideIntegrator.companyName}" label="default.field.worldIntegrator" />
                                    </g:if>
                                    </sec:ifNotGranted>
									
							
									<div class="col-xs-4 text-to-right">
										<label for="type">
											<div class="questionToolTip"
												title="${message(code:'tooltip.typeOfEstablishment')}"></div> 
											<g:message code="default.field.typeOfEstablishment" />*
										</label>
									</div>
									<div class="col-xs-8 text-to-left">
										<custom:selectWithOptGroupCompanyType id="type"
											from="${typesCompany}" value="${company.type?.code}" />
									</div>
									<span class="clearfix"></span>
							
									<div id="controlCompanyOtherType">
										<custom:editField type="text" name="otherType" required="${true}" value="${company.otherType}" label="default.field.specify" 
										  max="20"/>
									</div>
								
									<!-- Logo Image -->
									<g:render template="/layouts/tags/imageUploadSelectorTemplate" 
										model="[
											id:				'bgLogoUrl',
											imgValue: 		company.logoUrl,
											label:			message(code:'company.logo'),
											cropImage:		true,
											minWidth: 		mycentralserver.utils.Constants.COMPANY_LOGO_WIDTH,
											minHeight: 		mycentralserver.utils.Constants.COMPANY_LOGO_HEIGHT
											]"
									/>
								</div>
								
								<!-- Right Column -->
								<div class="col-xs-6 resize-formright">
									<custom:editField type="text" name="pocEmail" required="${false}" value="${company.pocEmail}" 
										label="poc.email" disabled="${ company.pocAccountCreated }"  max="255"/>
									<custom:editField type="text" name="pocFirstName" required="${false}" value="${company.pocFirstName}" 
										label="poc.firstname"  max="150"/>
									<custom:editField type="text" name="pocLastName" required="${false}" value="${company.pocLastName}" 
										label="poc.lastname"  max="150"/>
									<custom:editField type="text" name="pocPhone" required="${false}" value="${company.pocPhone}" 
										label="poc.phone"  max="50"/>
									<custom:editField type="checkbox" name="pocAccountCreated" required="${false}" 
										value="${company.pocAccountCreated}" label="poc.create.account" rightColumnPosition="right" 
										cssclass="input-checkbox" tooltip="poc.create.account.help" leftColumnSize="11"
										readonly="${ company.pocAccountCreated }" disabled="${ company.pocAccountCreated }"/>
									
									<div class="col-xs-12 text-to-right">
										<span class="liten-up full-width text-to-right">${ message(code:'poc.create.account.note') }</span>
									</div>
									<br><br>
									
									<!-- Level of Service -->
									<div class="col-xs-4 text-to-right">
										<label for="levelOfService"><g:message code="company.level.of.service" />*</label>
									</div>
									<div class="col-xs-8 text-to-left">
										<select name="levelOfService" id="levelOfService">
											<option value="1" ${ company.levelOfService == 1? 'selected="selected"':'' }> ${ message(code:'company.level.of.service.1') }</option>
											<option value="2" ${ company.levelOfService == 2? 'selected="selected"':'' }> ${ message(code:'company.level.of.service.2') }</option>
										</select>
									</div>
									<span class="clearfix"></span>
		
									<div class="col-xs-4 text-to-right">
										<label for="country"><g:message code="default.field.country" />*</label>
									</div>
									<div class="col-xs-8 text-to-left">
										<g:select name="country.id" id="country" from="${countries}"
											optionKey="id"
											optionValue="${ {shortName->g.message(code:'country.code.'+shortName) } }"
											value="${company.country?.id}" />
									</div>
									<span class="clearfix"></span>
							
									<g:if test="${company.state}">
										<div id="stateDiv">
									</g:if>
									<g:else>
										<div id="stateDiv" style="display: none;">
									</g:else>
									<div class="col-xs-4 text-to-right">
										<label for="state"><g:message
												code="default.field.stateOrProvince" />*</label>
									</div>
									<div class="col-xs-8 text-to-left">
										<g:select name="state.id" id="state" from="${states}" optionKey="id"
											optionValue="${ {name->g.message(code:'state.code.'+name) } }"
											value="${company.state?.id}" />
									</div>
									<span class="clearfix"></span>
									</div>
							
									<g:if test="${company.state}">
										<div id="stateNameDiv" style="display: none;">
									</g:if>
									<g:else>
										<div id="stateNameDiv">
									</g:else>
										<custom:editField type="text" name="stateName" value="${company.stateName}" label="default.field.stateOrProvince" />
									</div>
							
									<custom:editField type="text" name="city" max="100" required="${true}" value="${company.city}" label="default.field.city" />
									
									<div class="col-xs-4 text-to-right">
									  <label for="type"><g:message code="default.field.timezone" /> *</label>
									</div>
									<div class="col-xs-8 text-to-left">
									  <custom:timeZoneSelect name="timezone" value="${company.timezone}" />
									</div>
									<span class="clearfix"></span>
									
									<custom:editField type="zipCode" name="zipCode" required="${true}" value="${company.zipCode}" label="default.field.zipCode" tooltip="${message(code:'tooltip.zipCode')}" cssclass="onlyNumbers"/>
									<custom:editField type="textarea" name="address" min="7" required="${true}" value="${company.address}" label="default.field.address" max="100"/>
									<custom:editField type="checkbox" name="enable" value="${company.enable}" cssclass="input-checkbox" label="default.field.enabled" tooltip="${message(code:'tooltip.company.enable')}" />
								</div>
								<span class="clearfix"></span>
							
								<g:submitButton class="form-btn btn-primary"
									name="saves"
									value="${message(code:'default.field.buttonSave')}" />
									
								<g:link action="index" controller="company" params="[offset: offset]">        
							       <span class="form-btn btn-primary" class="back-button">
							          ${message(code:'default.field.buttonCancel')}
							       </span>       
							    </g:link>
							</g:uploadForm>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

</body>
</html>