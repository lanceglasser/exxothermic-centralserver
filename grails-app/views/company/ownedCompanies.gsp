<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.listCompany" /></title>
<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
<script src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_change_owner.js')}"></script>
<script>
	Exxo.module = "change-company-owner";
	Exxo.UI.vars["page-code"] = "change-company-owner";
	Exxo.UI.translates['changeButton'] = "${ message(code: 'change.btn') }";
	Exxo.Tables.loadPaginationInitValuesFromString('<custom:paginationInfo code="change-company-owner"/>');
	Exxo.UI.urls["change-company-url"] = 
		"${g.createLink(controller: 'company', action: 'changeCompanyOwner', absolute: true)}";
</script>
</head>
<body>
<section class="introp">
  <div class="intro-body">
    <div class="container full-width">
      <div class="row">
        <div class="col-md-12" id="internal-content">
          	<h1><g:message code="default.title.listCompany" /></h1>
			<g:render template="/layouts/messagesAndErrorsTemplate"/>
		<div class="content">
            <table id="table-list" class="display" cellspacing="0" width="100%">
              <thead>
                <tr>
                	<th>${message(code: 'default.field.companyName')}</th>
                	<th>${message(code: 'default.field.owner')}</th>						
					<th>${message(code: 'default.field.worldIntegrator')}</th>			
					<th>${message(code: 'default.field.stateOrProvince')}</th>												
					<th>${message(code: 'default.field.enabled')}</th>
					<th>${message(code: 'company.level.of.service')}</th>
					<th>${message(code: 'default.table.action')}</th>
                </tr>
              </thead>
              <tbody>
              	<g:each in="${companies}" status="i" var="companyInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<td id="company-${companyInstance.id}"> 
							${companyInstance.name.length()>50?(companyInstance.name[0..50]+" ...").encodeAsHTML():companyInstance.name.encodeAsHTML()}
						</td>
						
						<td id="owner-${companyInstance.id}">
							<g:if test="${companyInstance.owner}">
								 ${companyInstance.owner.encodeAsHTML()} 
							</g:if>
							<g:else>
								 <g:message code="default.field.empty" />
							</g:else>		
						</td>
							
						<td>
							<g:if test="${companyInstance.companyWideIntegrator?.companyName}">
								 ${companyInstance.companyWideIntegrator?.companyName.encodeAsHTML()} 
							</g:if>
							<g:else>
								 <g:message code="default.field.empty" />
							</g:else>		
						</td>
						
						<td>
							<g:if test="${companyInstance.state}">
								${g.message(code:"state.code." + companyInstance.state.name)}
							</g:if>
							<g:else>
								<g:if test="${companyInstance.stateName}">
									${companyInstance.stateName.encodeAsHTML()}
								</g:if>
								<g:else>
									<g:message code="default.field.empty" />
								</g:else>
							</g:else>								
						</td>
													
						<td>
							<g:if test="${companyInstance.enable == true}">
								<g:message code="default.field.yes" />
							</g:if>
							<g:else>
								<g:message code="default.field.no" />
							</g:else>
						</td>
						
						<td>
							${ message(code:'company.level.of.service.' + companyInstance.levelOfService) }
						</td>
													
						<td>							
							<g:if test="${companyInstance.typeOfCompany.name=='Integrator'}">
								<g:link  controller="companyIntegrator"  action="show" id="${companyInstance.id}" class="action-icons view" title="${message(code:'default.action.show')}"  ></g:link>							
							</g:if>								
							<g:else>									
								<g:link action="show" id="${companyInstance.id}" class="action-icons view" title="${message(code:'default.action.show')}"  ></g:link> 
							</g:else>
							
							<a id="btn-owner-${ companyInstance.id }" href="#" class="action-icons owner" title="Change Owner" rel="${ companyInstance.id }"
								 owner="${companyInstance.owner? companyInstance.owner.id:0}"></a>
						</td>
					</tr>
				</g:each>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div id="change-owner-dialog" title="${message(code:'change.company.owner')}"  style="display:none;">
  	<div class="col-xs-12 exxo-form content form-info">
  		<!-- Company -->
  		<span id="companyLabelDiv">
            <div class="col-xs-4 text-to-right">
              <label for="company"> <g:message code="default.field.companyName" /> </label>
            </div>
            <div class="col-xs-8 text-to-left"><span id="company" class="field-value">The Company Name</span> </div>
		</span>		
		<span class="clearfix"></span>
		
	  	<!-- Owner -->
		<div class="col-xs-4 text-to-right">
			<label for="owner"><g:message code="default.field.owner" /></label>
		</div>
		<div class="col-xs-8 text-to-left">
			<select name="owners" id="owners">
				<g:each in="${owners}" status="i" var="owner">
					<option value="${ owner.id }" ${ owner.id == 1? 'selected:"selected"':'' }> ${ owner }</option>
				</g:each>
			</select>
		</div>
		<span class="clearfix"></span>
  	</div>
</div>
</body>
</html>