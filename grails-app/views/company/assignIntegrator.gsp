<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.assignIntegrator" /></title>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_integrator_asign.js')}"></script>
<script>
	Exxo.module = "company-asign-integrator";
</script>
</head>
<body>
	<section class="intro">
		<div class="intro-body">
			<div class="container full-width">
				<div class="row">
					<div class="col-md-12" id="internal-content">
						<h1>
							<g:message code="default.title.assignIntegrator" />
						</h1>
						<g:form action="assign" name="createForm">
						<div class="content exxo-form">
							
							<g:render template="/layouts/messagesAndErrorsTemplate" model="[entity: company]"/>
														
							<div class="col-xs-12 resize-formleft">
								<div class="col-xs-3">
									<label class="control-label" for="user">
										<g:message code="default.field.integrator" /> *
									</label>
								</div>
								<div class="col-xs-9">									
									<g:select name="integrator" id="integrator"  from="${integrators}" style="width:50%" optionKey="id" 
										optionValue="companyName" value="${integratorId}"
										onchange="Exxo.showSpinner();${remoteFunction(
								            action:'showAssociatedCompanies', 
								            params:'\'integratorId=\' + escape(this.value)',
											update : 'infor')}"/>
								</div>
							</div>
							<span class="clearfix"></span>							
							
							<div class="col-xs-12 resize-formleft">
								<div class="col-xs-3">
									<label class="control-label" for="user">
										<g:message code="default.field.companies" /> *
									</label>
								</div>
								<div class="col-xs-9">
									<div class="exxo-table-container" id="infor"></div>
								</div>
							</div>
							<span class="clearfix"></span>
							
							<div class="col-xs-12 resize-formleft">
								<div class="col-xs-3">
								</div>
								<div class="col-xs-9 text-to-left">
									<g:submitButton class="form-btn btn-primary"
										name="associateCompanies"
										value="${message(code:'default.field.buttonSave')}" />		
								
									<g:link action="index" controller="home" class="back-button">        
								       <span class="form-btn btn-primary">
								          ${message(code:'default.field.buttonCancel')}
								       </span>       
								    </g:link>
								</div>
							</div>
							<span class="clearfix"></span>
						</div>
						</g:form>
					</div>
				</div>
			</div>
		</div>
	</section>

</body>
</html>