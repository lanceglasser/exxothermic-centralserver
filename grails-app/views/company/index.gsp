<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.listCompany" /></title>
<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
<script src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
<script>
	Exxo.module = "page-list";
	Exxo.UI.vars["page-code"] = "company-list";
	Exxo.Tables.loadPaginationInitValuesFromString('<custom:paginationInfo code="company-list"/>');
</script>
</head>
<body>
<section class="introp">
  <div class="intro-body">
    <div class="container full-width">
      <div class="row">
        <div class="col-md-12" id="internal-content">
          	<h1><g:message code="default.title.listCompany" /></h1>
			<g:render template="/layouts/messagesAndErrorsTemplate"/>
		<div class="content">
            <table id="table-list" class="display" cellspacing="0" width="100%">
              <thead>
                <tr>
                	<th>${message(code: 'default.field.companyName')}</th>						
					<th>${message(code: 'default.field.worldIntegrator')}</th>
					<th>${message(code: 'default.field.type')}</th>						
					<th>${message(code: 'default.field.stateOrProvince')}</th>												
					<th>${message(code: 'default.field.enabled')}</th>
					<th>${message(code: 'company.level.of.service')}</th>
					<th>${message(code: 'default.table.action')}</th>
                </tr>
              </thead>
              <tbody>
              	<g:each in="${companies}" status="i" var="companyInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<td> 
							<sec:ifAnyGranted  roles="ROLE_INTEGRATOR, ROLE_HELP_DESK">
								<g:if test="${companyInstance.typeOfCompany.name=='Integrator'}">
									<g:link  controller="companyIntegrator" action="editIntegratorCompany" id="${companyInstance.id}">${companyInstance.name.encodeAsHTML()}</g:link>
								</g:if>								
								<g:else>
									<g:link action="edit" id="${companyInstance.id}">${companyInstance.name.encodeAsHTML()}</g:link>
								</g:else>								
							</sec:ifAnyGranted>
							<sec:ifNotGranted  roles="ROLE_INTEGRATOR, ROLE_HELP_DESK">
								${companyInstance.name.length()>50?(companyInstance.name[0..50]+" ...").encodeAsHTML():companyInstance.name.encodeAsHTML()}
							</sec:ifNotGranted>
						</td>
							
						<td>
							<g:if test="${companyInstance.companyWideIntegrator?.companyName}">
								 ${companyInstance.companyWideIntegrator?.companyName.encodeAsHTML()} 
							</g:if>
							<g:else>
								 <g:message code="default.field.empty" />
							</g:else>		
						</td>
						
						<td>${companyInstance.typeOfCompany}</td>
						
						<td>
							<g:if test="${companyInstance.state}">
								${g.message(code:"state.code." + companyInstance.state.name)}
							</g:if>
							<g:else>
								${companyInstance.stateName.encodeAsHTML()}
							</g:else>								
						</td>
													
						<td>
							<g:if test="${companyInstance.enable == true}">
								<g:message code="default.field.yes" />
							</g:if>
							<g:else>
								<g:message code="default.field.no" />
							</g:else>
						</td>
						
						<td>
							${ message(code:'company.level.of.service.' + companyInstance.levelOfService) }
						</td>
							
													
						<td>							
							<g:if test="${companyInstance.typeOfCompany.name=='Integrator'}">
								 
								<g:link  controller="companyIntegrator"  action="show" id="${companyInstance.id}" class="action-icons view" title="${message(code:'default.action.show')}"  ></g:link>
								 									
							</g:if>
								
							<g:else>									
								<g:link action="show" id="${companyInstance.id}" class="action-icons view" title="${message(code:'default.action.show')}"  ></g:link> 
							</g:else>
							
							
							<!-- EDIT  --> 
							 
							<sec:ifAnyGranted  roles="ROLE_INTEGRATOR, ROLE_HELP_DESK">
								<g:if test="${companyInstance.typeOfCompany.name=='Integrator'}">
									<g:link  controller="companyIntegrator" action="edit" id="${companyInstance.id}" class="action-icons edit" title="${message(code:'default.action.edit')}"></g:link>
								</g:if>								
								<g:else>
									<g:link action="edit" id="${companyInstance.id}" title="${message(code:'default.action.edit')}" class="action-icons edit"></g:link>
								</g:else>								
							</sec:ifAnyGranted>
							
							
							<g:if test="${companyInstance.locations.size()>0}" >									 
								<g:link  controller="location" action="index" id="${companyInstance.id}" class="action-icons viewLocation" title="${message(code:'default.field.view.locations')}(${companyInstance.locations.size()})"></g:link> 
							</g:if>	
							<sec:ifAnyGranted  roles="ROLE_INTEGRATOR, ROLE_HELP_DESK">								 
								<g:link controller="location" action="create" params="${["company":companyInstance.id]}" class="action-icons addLocation" title="${message(code:'default.title.newLocation')}"  ></g:link>
							</sec:ifAnyGranted>		
						</td>
					</tr>
				</g:each>
              </tbody>
            </table>
            
            <br>
			<g:if test="${companiesAssigned}">
				
				<h1><g:message code="default.title.actingAsIntegrator" /></h1>
				
				<table id="table-list-two" class="display" cellspacing="0" width="100%">
	              <thead>
	                <tr>
						<th>${message(code: 'default.field.companyName')}</th>				
						<th>${message(code: 'default.field.type')}</th>	
						<th>${message(code: 'default.field.owner')}</th>												
						<th>${message(code: 'default.field.stateOrProvince')}</th>							
						<th>${message(code: 'default.field.enabled')}</th>
						<th>${message(code: 'default.table.action')}</th>
	                </tr>
	              </thead>
	              <tfoot>
	                <tr>
	                	<th>${message(code: 'default.field.companyName')}</th>				
						<th>${message(code: 'default.field.type')}</th>	
						<th>${message(code: 'default.field.owner')}</th>												
						<th>${message(code: 'default.field.stateOrProvince')}</th>							
						<th>${message(code: 'default.field.enabled')}</th>
						<th>${message(code: 'default.table.action')}</th>
	                </tr>
	              </tfoot>
	              <tbody>
	              	<g:each in="${companiesAssigned}" status="i" var="assignedInstance">
						<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
							<!-- Name -->
							<td>
								<sec:ifAnyGranted  roles="ROLE_OWNER, ROLE_INTEGRATOR, ROLE_HELP_DESK">
									<g:if test="${assignedInstance.typeOfCompany.name=='Integrator'}">
										<g:link  controller="companyIntegrator" action="edit" id="${assignedInstance.id}">${assignedInstance.name}</g:link>
									</g:if>								
									<g:else>
										<g:link action="edit" id="${assignedInstance.id}">${assignedInstance.name}</g:link>
									</g:else>								
								</sec:ifAnyGranted>							
								<sec:ifNotGranted  roles="ROLE_OWNER, ROLE_INTEGRATOR, ROLE_HELP_DESK">
									${assignedInstance.name}
								</sec:ifNotGranted>
							</td>
						 	 
							<td>${assignedInstance.typeOfCompany}</td>
							<td>${assignedInstance.owner}</td>
						 
							<td>${g.message(code:"state.code." + assignedInstance.state.name)}</td>								
							<td>
								<g:if test="${assignedInstance.enable == true}">
									<g:message code="default.field.yes" />
								</g:if>
								<g:else>
									<g:message code="default.field.no" />
								</g:else>
							</td>							
							<td>								
								<g:if test="${assignedInstance.typeOfCompany.name=='Integrator'}">										
									<g:link  controller="companyIntegrator"  action="show" id="${assignedInstance.id}" class="action-icons view" title="${message(code:'default.action.show')}"  ></g:link>									
								</g:if>										
								<g:else>										
									<g:link action="show" id="${assignedInstance.id}" class="action-icons view" title="${message(code:'default.action.show')}"  ></g:link>
								</g:else>	
									<g:link action="edit" id="${assignedInstance.id}"  class="action-icons edit" title="${message(code:'default.action.edit')}"  ></g:link>								
								<g:if test="${assignedInstance.locations.size()>0}" >										 
									<g:link  controller="location" action="index" id="${assignedInstance.id}" class="action-icons viewLocation" title="${message(code:'default.field.view.locations')}(${assignedInstance.locations.size()})"></g:link> 
								</g:if>
								<sec:ifNotGranted roles="ROLE_STAFF">										
									<g:link controller="location" action="create" params="${["company":assignedInstance.id]}" class="action-icons addLocation" title="${message(code:'default.title.newLocation')}"  ></g:link>
								</sec:ifNotGranted>
							</td>
						</tr>
					</g:each>
	              </tbody>
	            </table>
			</g:if>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div id="confirm-reset-dialog" title="${message(code:'mycentralserver.user.User.resetPassword.title')}"  style="display:none;">
  ${message(code:'mycentralserver.user.User.resetPassword')}
</div>
</body>
</html>