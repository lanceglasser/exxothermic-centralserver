<table class='table table-striped exxo-table'>
	<thead>
		<tr>
			<th class="width10"></th>
			<th class="widthTdRow40 sorter">${message(code: 'default.field.companyName')}</th>								
			<th class="restOfWidth sorter">${message(code: 'default.field.worldIntegrator')}</th>														
		</tr>
	</thead>
	<tbody>	
		<g:each in="${companies}" status="i" var="companyInstance">
			<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
				<td class="widthTdRow10"><g:checkBox checked="false" name="companies" id="company_${companyInstance.id}" value="${companyInstance.id}"/></td>
				<td class="widthTdRow40">${companyInstance.name.length()>50?companyInstance.name[0..50].encodeAsHTML()+" ...":companyInstance.name.encodeAsHTML()}</td>																						
				<td class="widthTdRowRestOfWidth">${companyInstance.companyWideIntegrator?.companyName?.encodeAsHTML()}</td>												
			</tr>
		</g:each>
	</tbody>
</table>
										
<script>
	Exxo.UI.vars['selectedCompanies'] = "${selectedCompanies}";
	Exxo.hideSpinner();
</script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_manage_companies_of_integrator.js')}"></script>