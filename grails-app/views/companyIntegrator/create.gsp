<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.newIntegrator" /></title>
<!-- Image Crop Required Files -->
<link rel="stylesheet" href="${resource(dir: 'css/jcrop', file: 'jquery.Jcrop.css')}" type="text/css">
<script src="${resource(dir: 'js/jcrop', file: 'jquery.Jcrop.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_images_upload.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_utils.js')}"></script>

<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_integrator_create.js')}"></script>
<script>
	Exxo.module = "integrator-create";
	Exxo.UI.urls["loadStatesByCountryUrl"] = "${g.createLink(controller: 'common', action: 'statebycountry', absolute: true)}";
	Exxo.UI.translates['zipcode-title-msg'] = "${message(code:'default.invalid.string.lenght', args:['X1', 'X2'])}";
</script>
</head>
<body>
	<section class="introp">
		<div class="intro-body">
			<div class="container full-width">
				<div class="row">
					<div class="col-md-12" id="internal-content">
						<h1>
							<g:message code="default.title.newIntegrator" />
						</h1>
						<div class="content">
							<g:render template="/layouts/messagesAndErrorsTemplate" 
								model="[entity: company, entity2: integrator]"/>
							
							<g:uploadForm action="save" name="createForm" class="exxo-form">
								<!-- Column Left -->
								<div class="col-xs-6 resize-formleft">
									<div class="col-xs-4 text-to-right">
										<label for="name">
											<g:message code="default.field.companyName" />*
										</label>
									</div>
									<div class="col-xs-8 text-to-left">
										<g:textField name="name" maxlength="50"
											required="required" class="required" value="${company.name}" />
									</div>
									<span class="clearfix"></span>
									
									<div class="col-xs-4 text-to-right">
										<label for="owner">
											<div class="questionToolTip" title="<g:message code="default.integrator.create.note" />"></div>
											<g:message code="default.field.company.owner" />*
										</label>
									</div>
									<div class="col-xs-8 text-to-left">
										<g:select name="owner.id" id="owner" value="${company.owner?.id}"
											from="${users}" optionKey="id" optionValue="firstNameAndLastName" />
									</div>
									<span class="clearfix"></span>
									
									<div class="col-xs-4 text-to-right">
										<label for="webSite">
											<g:message code="default.field.webSite" />
										</label>
									</div>
									<div class="col-xs-8 text-to-left">
										<g:field type="url" name="webSite"
											maxlength="100" value="${company.webSite}" />
									</div>
									<span class="clearfix"></span>
									
									<div class="col-xs-4 text-to-right">
										<label for="phoneNumber">
											<div class="questionToolTip" 
												title="<g:message code="default.field.example" /> <g:message code="field.phone.number.format" />">
											</div>
											<g:message code="field.phone.number" />
										</label>
									</div>
									<div class="col-xs-8 text-to-left">
										<input type="text" name="phoneNumber"
											id="phoneNumber" pattern="^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]*$"
											maxlength="20" value="${company.phoneNumber}"
											placeholder="<g:message code="default.field.example" /> <g:message code="field.phone.number.format" />">
											 <span class="liten-up">${message(code: "default.field.example")} ${message(code: "field.phone.number.format")}</span>
									</div>
									<span class="clearfix"></span>
									
									<!-- Level of Service -->
							        <div class="col-xs-4 text-to-right">
							            <label for="levelOfService"><g:message code="company.level.of.service" />*</label>
							        </div>
							        <div class="col-xs-8 text-to-left">
							            <select name="levelOfService" id="levelOfService">
							                <option value="1" ${ company.levelOfService == 1? 'selected:"selected"':'' }> ${ message(code:'company.level.of.service.1') }</option>
							                <option value="2" ${ company.levelOfService == 2? 'selected:"selected"':'' }> ${ message(code:'company.level.of.service.2') }</option>
							            </select>
							        </div>
							        <span class="clearfix"></span>
									
									<!-- Logo Image -->
									<g:render template="/layouts/tags/imageUploadSelectorTemplate" 
										model="[
											id:				'bgLogoUrl',
											imgValue: 		integrator.company?.logoUrl,
											label:			message(code:'company.logo'),
											cropImage:		true,
											minWidth: 		mycentralserver.utils.Constants.COMPANY_LOGO_WIDTH,
											minHeight: 		mycentralserver.utils.Constants.COMPANY_LOGO_HEIGHT
											]"
									/>
								</div>
								
								<!-- Column Right -->
								<div class="col-xs-6 resize-formright">
									<div class="col-xs-4 text-to-right">
										<label for="country">
											<g:message code="default.field.country" />*
										</label>
									</div>
									<div class="col-xs-8 text-to-left">
										<g:select name="country.id" id="country" from="${countries}" optionKey="id"  
											optionValue="${ {shortName->g.message(code:'country.code.'+shortName) } }" 
											value="${company.country?.id}"/>
									</div>
									<span class="clearfix"></span>
									
									<g:if test="${company.state}">
										<div id="stateDiv">
									</g:if>
									<g:else>
										<div id="stateDiv" style="display:none;">
									</g:else>									
									<div class="col-xs-4 text-to-right">
										<label for="state">
											<g:message code="default.field.stateOrProvince" />*
										</label>
									</div>
									<div class="col-xs-8 text-to-left">
										<g:select name="state.id" id="state" from="${states}" optionKey="id" 
										 optionValue="${ {name->g.message(code:'state.code.'+name) } }" value="${company.state?.id}" />
									</div>
									<span class="clearfix"></span>
									</div>
									
									<g:if test="${company.state}">
										<div id="stateNameDiv" style="display:none;">
									</g:if>
									<g:else>
										<div id="stateNameDiv">
									</g:else>
									<div class="col-xs-4 text-to-right">
										<label for="stateName">
											<g:message code="default.field.stateOrProvince" />
										</label>
									</div>
									<div class="col-xs-8 text-to-left">
										<g:textField name="stateName" maxlength="20"  value="${company.stateName}" />
									</div>
									<span class="clearfix"></span>
									</div>
									
									<custom:editField type="text" name="city" max="100" required="${true}" value="${company.city}" label="default.field.city" />		
									
									<div class="col-xs-4 text-to-right">
									  <label for="type"><g:message code="default.field.timezone" /> *</label>
									</div>
									<div class="col-xs-8 text-to-left">
									  <custom:timeZoneSelect name="timezone" value="${company.timezone}" />
									</div>
									<span class="clearfix"></span>
									
									<custom:editField type="zipCode" name="zipCode" required="${true}" value="${company.zipCode}" label="default.field.zipCode" tooltip="${message(code:'tooltip.zipCode')}" cssclass="onlyNumbers"/>
									<custom:editField type="textarea" name="address" min="7" required="${true}" value="${company.address}" label="default.field.address" />
										
									<h4>
										<g:message code="default.home.register.extraInformation" />
									</h4>
									<div class="col-xs-4 text-to-right">
										<label for="numberOfEmployees"><g:message code="default.field.numberOfEmployees" /></label>
									</div>
									<div class="col-xs-8 text-to-left">
										<g:select name="numberOfEmployees"
											from="${['1-5','6-12','13-25','more']}"
											value="${integrator.numberOfEmployees}" />
									</div>
									<span class="clearfix"></span>
									
                                    <custom:editField type="text" name="cediaNumber" required="${false}" 
                                        value="${integrator?.cediaNumber}" 
                                        label="default.field.cediaNumber" max="40"/>
                                        
                                    <custom:editField type="text" name="contractorLicense" required="${false}" 
                                        value="${integrator?.contractorLicense}"  tooltip="${message(code:'contractor.licence.number.help')}"
                                        label="default.field.contractorLicenseNumber" max="40"/>
                                        
                                    <custom:editField type="text" name="reseller" required="${false}" 
                                        value="${integrator?.reseller}" 
                                        label="default.field.reseller" max="40"/>
                                        
                                    <custom:editField type="tax" name="taxId" required="${false}" 
                                        value="${integrator?.taxId}" tooltip="${ message(code:'default.field.example')} 01-1234567"
                                        label="default.field.taxId" max="10"/>

                                    <custom:editField type="checkbox" name="enabled" required="${false}" value="${company.enable}"
        label="default.field.enabled" cssclass="input-checkbox" tooltip="tooltip.companyIntegrator.enable"/>

                                    <custom:editField type="checkbox" name="approved" required="${false}" value="${integrator.approved}"
        label="default.field.approved" cssclass="input-checkbox"/>
								</div>
								<span class="clearfix"></span>
							
								<!-- Buttons -->
								<g:submitButton class="form-btn btn-primary" name="saves"
									value="${message(code:'default.field.buttonSave')}" />
				
								<g:link action="index" controller="companyIntegrator" class="back-button">
									<span class="form-btn btn-primary"> ${message(code:'default.field.buttonCancel')}
									</span>
								</g:link>
							</g:uploadForm>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

</body>
</html>