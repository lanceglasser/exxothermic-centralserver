<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.listIntegrator" /></title>
<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
<script src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
<script>
	Exxo.module = "page-list";
	Exxo.UI.vars["page-code"] = "integrators-list";
	Exxo.Tables.loadPaginationInitValuesFromString('<custom:paginationInfo code="integrators-list"/>');
</script>
</head>
<body>
<section class="introp">
  <div class="intro-body">
    <div class="container full-width">
      <div class="row">
        <div class="col-md-12" id="internal-content">
          	<h1><g:message code="default.title.listIntegrator" /></h1>
			<g:render template="/layouts/messagesAndErrorsTemplate"/>
		<div class="content">
            <table id="table-list" class="display" cellspacing="0" width="100%">
              <thead>
                <tr>
					<th>${message(code: 'field.name')}</th>
					<th>${message(code: 'default.field.taxId')}</th>
					<th>${message(code: 'default.field.stateOrProvince')}</th>
					<th>${message(code: 'default.field.city')}</th>						
					<th>${message(code: 'default.field.approved')}</th>
					<th>${message(code: 'default.table.action')}</th>
                </tr>
              </thead>
              <tbody>
              	<g:each in="${integrators}" status="i" var="integratorInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<td><g:link action="edit" id="${integratorInstance.id}">${integratorInstance.company?.name}</g:link></td>							
						<td>${integratorInstance.taxId}</td>
						<td>
							<g:if test="${integratorInstance.company.state}">
								${g.message(code:"state.code." + integratorInstance.company.state.name)}
							</g:if>
							<g:else>
								${integratorInstance.company.stateName.encodeAsHTML()}
							</g:else>	
						</td>
						<td>${integratorInstance.company?.city}</td>									
						<td>
							<g:if test="${integratorInstance.approved == true}">
								<g:message code="default.field.yes" />
							</g:if>
							<g:else>
								<g:message code="default.field.no" />
							</g:else>
						</td>
						<td>								
							<g:link action="show" id="${integratorInstance.id}" class="action-icons view" title="${message(code:'default.action.show')}"  ></g:link>
							<g:link action="edit" id="${integratorInstance.id}" class="action-icons edit" title="${message(code:'default.action.edit')}"  ></g:link>  		
							<g:if test="${integratorInstance.approved}"> 										
								<g:link action="approve" id="${integratorInstance.id}" class="action-icons disapprove" title="${message(code:'default.action.disapprove')}"></g:link> 
							</g:if>
							<g:else>
								<g:link action="approve" id="${integratorInstance.id}" class="action-icons approve" title="${message(code:'default.action.approve')}"></g:link> 									
							</g:else> 																		
						</td>
					</tr>
				</g:each>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</body>
</html>