<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.Integrator" /></title>
<script>
	Exxo.module = "company-show";
</script>
</head>
<body>
	<section class="intro">
		<div class="intro-body">
			<div class="container full-width">
				<div class="row">
					<div class="col-md-12" id="internal-content">
						<h1>
							${integrator.company?.name}
						</h1>
						
						<div class="content form-info">
							<g:if test="${flash.error}">
				        		<div class="alert alert-error">  
								  <a class="close" data-dismiss="alert">×</a>  
								  ${flash.error} 
			 						<g:hasErrors bean="${integrator.company}">
								  		<ul>
											<g:eachError bean="${integrator.company}" var="error">
												<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
											</g:eachError>
										</ul>
								  </g:hasErrors>	
								  <g:hasErrors bean="${integrator}">
								  		<ul>
											<g:eachError bean="${integrator}" var="error">
												<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
											</g:eachError>
										</ul>
								  </g:hasErrors>				
								</div> 
								${flash.error=null} 
				      		</g:if>
				      		 <g:if test="${flash.success}">
					        		<div class="alert alert-success">  
									  <a class="close" data-dismiss="alert">×</a>  
									  ${flash.success} 
									</div> 
									 ${flash.success=null} 
				      		</g:if>
							<!-- Left Column -->
							<div class="col-xs-6 resize-formleft">
							     <custom:editField name="levelOfService" value="${message(code:"company.level.of.service." + integrator.company.levelOfService)}" label="company.level.of.service" />
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="default.field.owner" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${integrator.company?.owner}">
									<span class="field-value">${integrator.company?.owner}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty" /></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="default.field.webSite" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${integrator.company?.webSite}">
									<span class="field-value">${integrator.company?.webSite}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty"/></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
																
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="field.phone.number" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${integrator.company?.phoneNumber}">
									<span class="field-value">${integrator.company?.phoneNumber}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty"/></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="default.field.country" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
									<g:if test="${integrator.company.state}">
										<span class="field-value">${message(code:"country.code." + integrator.company.state.country.shortName.encodeAsHTML())}</span>
									</g:if>
									<g:else>
										<span class="field-value">${message(code:"country.code." + integrator.company.country.shortName.encodeAsHTML())}</span>
									</g:else>
								</div>
								<span class="clearfix"></span>
								
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="default.field.stateOrProvince" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
									<g:if test="${integrator.company.state}">
										<span class="field-value">${g.message(code:"state.code." + integrator.company.state.name)}</span>
									</g:if>
									<g:else>
										<g:if test="${integrator.company.stateName}">
											<span class="field-value">${integrator.company.stateName.encodeAsHTML()}</span>
										</g:if>
										<g:else>
											<span class="field-value"><g:message code="default.field.empty" /></span>
										</g:else>
									</g:else>
								</div>
								<span class="clearfix"></span>
								
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="default.field.city" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${integrator.company.city}">
									<span class="field-value">${integrator.company?.city}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty"/></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="default.field.zipCode" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${integrator.company?.zipCode}">
									<span class="field-value">${integrator.company?.zipCode}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty"/></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="default.field.address" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${integrator.company?.address}">
									<span class="field-value">${integrator.company?.address}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty" /></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
							</div>
							
							<!-- Right Column -->
							<div class="col-xs-6 resize-formright">
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="default.field.numberOfEmployees" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${integrator.numberOfEmployees}">
									<span class="field-value">${integrator.numberOfEmployees}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty" /></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="default.field.cediaNumber" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${integrator.cediaNumber}">
									<span class="field-value">${integrator.cediaNumber}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty" /></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="default.field.contractorLicenseNumber" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${integrator.contractorLicense}">
									<span class="field-value">${integrator.contractorLicense}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty" /></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="default.field.reseller" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${integrator.reseller}">
									<span class="field-value">${integrator.reseller}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty" /></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="default.field.taxId" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${integrator.company?.taxId}">
									<span class="field-value">${integrator.company?.taxId}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty" /></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
																
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="default.field.enabled" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${integrator.company?.enable == true}">
									<span class="field-value"><g:message code="default.field.yes" /></span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.no" /></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
								<div class="col-xs-4 text-to-right">
									<span class="field-label"><g:message code="default.field.approved" /></span>
								</div>
								<div class="col-xs-8 text-to-left">
								<g:if test="${integrator.approved == true}">
									<span class="field-value"><g:message code="default.field.yes" /></span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.no" /></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
							</div>
							<span class="clearfix"></span>
							<!-- Buttons -->
							<sec:ifAnyGranted roles="ROLE_ADMIN, ROLE_HELP_DESK">	
								<g:link action="index" controller="companyIntegrator"> <input type="button" class="form-btn btn-primary" value="${message(code:'default.field.buttonBack')}"/> </g:link>
							</sec:ifAnyGranted>				  			
							<sec:ifNotGranted roles="ROLE_ADMIN, ROLE_HELP_DESK">
								<g:link action="index" controller="company"> <input type="button" class="form-btn btn-primary" value="${message(code:'default.field.buttonBack')}"/> </g:link>
							</sec:ifNotGranted>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</body>
</html>