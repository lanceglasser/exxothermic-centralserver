<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.updateIntegrator" /></title>
<!-- Image Crop Required Files -->
<link rel="stylesheet" href="${resource(dir: 'css/jcrop', file: 'jquery.Jcrop.css')}" type="text/css">
<script src="${resource(dir: 'js/jcrop', file: 'jquery.Jcrop.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_images_upload.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_utils.js')}"></script>

<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_integrator_create.js')}"></script>
<script>
	Exxo.module = "integrator-create";
	Exxo.UI.urls["loadStatesByCountryUrl"] = "${g.createLink(controller: 'common', action: 'statebycountry', absolute: true)}";
	Exxo.UI.translates['zipcode-title-msg'] = "${message(code:'default.invalid.string.lenght', args:['X1', 'X2'])}";
</script>
</head>
<body>
	<section class="introp">
		<div class="intro-body">
			<div class="container full-width">
				<div class="row">
					<div class="col-md-12" id="internal-content">
						<h1>
							<g:message code="default.title.updateIntegrator" />
						</h1>
						<div class="content">
							<g:render template="/layouts/messagesAndErrorsTemplate" 
								model="[entity: integrator.company, entity2: integrator]"/>
							
							<g:uploadForm action="update" id="${integrator.id}" name="updateForm" class="exxo-form">
								<g:hiddenField name="id" value="${integrator.id}" />
		 						<g:hiddenField name="version" value="${integrator.version}" />
		 						<g:field  type="hidden" name="offset" value="${offset}" />
		 						
								<!-- Column Left -->
								<div class="col-xs-6 resize-formleft">
									<div class="col-xs-4 text-to-right">
										<label for="name">
											<g:message code="field.name" />*
										</label>
									</div>
									<div class="col-xs-8 text-to-left">
										<g:textField class="required" name="name"  maxlength="50" required="required" value="${integrator.company?.name}" />
									</div>
									<span class="clearfix"></span>
									
									<sec:ifAnyGranted roles="ROLE_ADMIN">
										<div class="col-xs-4 text-to-right">
											<label for="owner">
												<g:message code="default.field.owner" />*
											</label>
										</div>
										<div class="col-xs-8 text-to-left">
											<g:select name="owner.id" id="owner" value="${integrator.company?.owner?.id}" 
												from="${users}"  optionKey="id" optionValue="firstNameAndLastName" />
										</div>
										<span class="clearfix"></span>
									</sec:ifAnyGranted>
									
									<div class="col-xs-4 text-to-right">
										<label for="webSite">
											<g:message code="default.field.webSite" />
										</label>
									</div>
									<div class="col-xs-8 text-to-left">
										<g:field  type="url" name="webSite" maxlength="100" value="${integrator.company?.webSite}" />
									</div>
									<span class="clearfix"></span>
									
									<div class="col-xs-4 text-to-right">
										<label for="phoneNumber">
											<div class="questionToolTip" 
												title="<g:message code="default.field.example" /> <g:message code="field.phone.number.format" />">
											</div>
											<g:message code="field.phone.number" />
										</label>
									</div>
									<div class="col-xs-8 text-to-left">
										<input type="text" name="phoneNumber"
											id="phoneNumber" pattern="^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]*$"
											maxlength="20" value="${integrator.company?.phoneNumber}"
											placeholder="<g:message code="default.field.example" /> <g:message code="field.phone.number.format" />">
									</div>
									<span class="clearfix"></span>
									
									<!-- Level of Service -->
                                    <div class="col-xs-4 text-to-right">
                                        <label for="levelOfService"><g:message code="company.level.of.service" />*</label>
                                    </div>
                                    <div class="col-xs-8 text-to-left">
                                        <select name="levelOfService" id="levelOfService">
                                            <option value="1" ${ integrator.company.levelOfService == 1? 'selected="selected"':'' }> ${ message(code:'company.level.of.service.1') }</option>
                                            <option value="2" ${ integrator.company.levelOfService == 2? 'selected="selected"':'' }> ${ message(code:'company.level.of.service.2') }</option>
                                        </select>
                                    </div>
                                    <span class="clearfix"></span>
                                    
									<!-- Logo Image -->
									<g:render template="/layouts/tags/imageUploadSelectorTemplate" 
										model="[
											id:				'bgLogoUrl',
											imgValue: 		integrator.company?.logoUrl,
											label:			message(code:'company.logo'),
											cropImage:		true,
											minWidth: 		mycentralserver.utils.Constants.COMPANY_LOGO_WIDTH,
											minHeight: 		mycentralserver.utils.Constants.COMPANY_LOGO_HEIGHT
											]"
									/>
									
								</div>
								
								<!-- Column Right -->
								<div class="col-xs-6 resize-formright">
									<div class="col-xs-4 text-to-right">
										<label for="country">
											<g:message code="default.field.country" />*
										</label>
									</div>
									<div class="col-xs-8 text-to-left">
										<g:select name="country.id" id="country" from="${countries}" optionKey="id"  
											optionValue="${ {shortName->g.message(code:'country.code.'+shortName) } }" value="${integrator.company.country?.id}"/>
									</div>
									<span class="clearfix"></span>
									
									<g:if test="${integrator.company.state}">
										<div id="stateDiv">
									</g:if>
									<g:else>
										<div id="stateDiv" style="display:none;">
									</g:else>									
									<div class="col-xs-4 text-to-right">
										<label for="state">
											<g:message code="default.field.stateOrProvince" />*
										</label>
									</div>
									<div class="col-xs-8 text-to-left">
										<g:select name="state.id" id="state" from="${states}" optionKey="id" 
											optionValue="${ {name->g.message(code:'state.code.'+name) } }" value="${integrator.company.state?.id}" />
									</div>
									<span class="clearfix"></span>
									</div>
									
									<g:if test="${integrator.company.state}">
										<div id="stateNameDiv" style="display:none;">
									</g:if>
									<g:else>
										<div id="stateNameDiv">
									</g:else>
									<div class="col-xs-4 text-to-right">
										<label for="stateName">
											<g:message code="default.field.stateOrProvince" />
										</label>
									</div>
									<div class="col-xs-8 text-to-left">
										<g:textField class="input-xlarge" name="stateName" maxlength="20"  value="${integrator.company.stateName}" />
									</div>
									<span class="clearfix"></span>
									</div>
									
									<custom:editField type="text" name="city" max="100" required="${true}" value="${integrator.company?.city}" label="default.field.city" />		
									
									<div class="col-xs-4 text-to-right">
									  <label for="type"><g:message code="default.field.timezone" /> *</label>
									</div>
									<div class="col-xs-8 text-to-left">
									  <custom:timeZoneSelect name="timezone" value="${integrator.company?.timezone}" />
									</div>
									<span class="clearfix"></span>
									
									<custom:editField type="zipCode" name="zipCode" required="${true}" value="${integrator.company?.zipCode}" label="default.field.zipCode" tooltip="${message(code:'tooltip.zipCode')}" cssclass="onlyNumbers"/>
									<custom:editField type="textarea" name="address" min="7" required="${true}" value="${integrator.company?.address}" label="default.field.address" />
																		
									<h4>
										<g:message code="default.home.register.extraInformation" />
									</h4>
									<div class="col-xs-4 text-to-right">
										<label for="numberOfEmployees"><g:message code="default.field.numberOfEmployees" /></label>
									</div>
									<div class="col-xs-8 text-to-left">
										<g:select name="numberOfEmployees" from="${['1-5','6-12','13-25','more']}"
											value="${integrator.numberOfEmployees}" />
									</div>
									<span class="clearfix"></span>									
									
									<!-- cediaNumber -->
									<custom:editField type="text" name="cediaNumber" required="${false}" value="${integrator?.cediaNumber}"
											label="field.cedia.number" max="40"/>
											
									<!-- contractorLicense -->
									<custom:editField type="text" name="contractorLicense" required="${false}" value="${integrator?.contractorLicense}"
										label="field.contractor.license.number" tooltip="${message(code:'contractor.licence.number.help')}" max="40"/>
						
									<!-- reseller -->
									<custom:editField type="text" name="reseller" required="${false}" value="${integrator?.reseller}"
											label="field.reseller" max="40"/>
											
									<!-- taxId -->
									<custom:editField type="tax" name="taxId" required="${false}" value="${integrator?.taxId}" max="10" 
											label="field.tax.id" tooltip="${ message(code:'default.field.example') + ' 01-1234567' }"/>
									
									<!-- Enabled -->
									<custom:editField type="checkbox" name="enable" value="${integrator.company?.enable}" cssclass="input-checkbox" label="default.field.enabled" tooltip="${message(code:'tooltip.companyIntegrator.enable')}" />
									
								</div>
								<span class="clearfix"></span>
							
								<!-- Buttons -->
								<g:submitButton class="form-btn btn-primary" name="saves"
									value="${message(code:'default.field.buttonSave')}" />
								
								<sec:ifAnyGranted roles="ROLE_ADMIN, ROLE_HELP_DESK">		
									<g:link action="index" controller="companyIntegrator" class="back-button">        
								       <span class="form-btn btn-primary">
								          ${message(code:'default.field.buttonCancel')}
								       </span>       
								    </g:link>
							    </sec:ifAnyGranted>	
							    <sec:ifNotGranted roles="ROLE_ADMIN, ROLE_HELP_DESK">
							    	<g:link action="index" controller="home" class="back-button">        
							       <span class="form-btn btn-primary">
							          ${message(code:'default.field.buttonCancel')}
							       </span>       
							    </g:link>
							    </sec:ifNotGranted>
							</g:uploadForm>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

</body>
</html>