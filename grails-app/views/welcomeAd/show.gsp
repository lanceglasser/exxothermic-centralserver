<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.home.skinApp" /></title>
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'lightbox.css')}">
<script
	src="${resource(dir: 'themes/default/js', file: 'lightbox.min.js')}"></script>
<link rel="stylesheet" href="${resource(dir: 'css/pluggins/bootstrap-colorpicker', file: 'bootstrap-colorpicker.css')}" type="text/css">
<link rel="stylesheet" href="${resource(dir: 'css/jcrop', file: 'jquery.Jcrop.css')}" type="text/css">
<link rel="stylesheet" href="${resource(dir: 'themes/default/css', file: 'video-js.min.css')}" type="text/css">		
<script src="${resource(dir: 'js/pluggins/bootstrap-colorpicker', file: 'bootstrap-colorpicker.js')}"></script>
<script src="${resource(dir: 'js/pluggins/bootstrap-colorpicker', file: 'docs.js')}"></script>
<script src="${resource(dir: 'js/jcrop', file: 'jquery.Jcrop.js')}"></script>
<script src="${resource(dir: 'themes/default/js', file: 'video.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_welcome_ad.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_utils.js')}"></script>
<script>
	Exxo.module = "welcome-ad";
	//videojs.options.flash.swf = "video-js.swf";
</script>

</head>
<body>
<g:render template="/layouts/internalContentTemplate">
	<h1>
		${entity.name?.encodeAsHTML()}
	</h1>
	
	<div class="content form-info">
		<g:render template="/layouts/messagesAndErrorsTemplate" model="[entity: entity]"/>
		
		<g:render template="/welcomeAd/showTemplate" model="[entity: entity]"/>

		<!-- Buttons -->
		<g:link action="listAll" controller="welcomeAd" class="form-btn btn-primary"> 
			${message(code:'default.field.buttonBack')}
		</g:link>
	</div>
</g:render>


</body>
</html>