<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.listSkin" /></title>
<script>
	Exxo.module = "index";
</script>
</head>
</head>
<body>
	<section class="introp">
			<div class="intro-body">
				<div class="container">
					<div class="row">
						<g:render template="/layouts/messagesAndErrorsTemplate"/>
					
						<div class="col-md-8 col-md-offset-2" id="home-salute">
							<div id="child-salute">
								<div class="message">
									<p><g:message code="welcome.ad.empty.list"/></p>
									<g:link controller="welcomeAd" action="create"><g:message code="welcome.ad.menu.create"/></g:link>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
</body>
</html>