<!-- Left Column -->
<div class="col-xs-6 resize-formleft">
	<custom:editField name="name"
		value="${entity.name ? entity.name.encodeAsHTML() : ''}"
		label="default.field.name" leftColumnSize="6"/>
	
	<custom:editField name="skipEnable"
		value="${(entity.skipEnable == true) ? message(code:"default.field.yes") : message(code:"default.field.no")}"
		label="welcome.ad.field.skip.enable" leftColumnSize="6"/>
		
	<custom:editField name="skipTime"
		value="${entity.skipTime ? entity.skipTime.encodeAsHTML() : ''}"
		label="welcome.message.skip.seconds" leftColumnSize="6"/>
					
</div>
<!-- Right Column -->
<div class="col-xs-6 resize-formright">
				
	<custom:editField name="welcomeType"
		value="${entity.welcomeType ? message(code: "welcome.message.type." + entity.welcomeType.encodeAsHTML()) : ''}"
		label="welcome.message.type" leftColumnSize="6"/>				
	
	<g:if test="${ entity.welcomeType == mycentralserver.app.WelcomeAd.WelcomeType.IMAGE }">
		<div class="col-xs-6 text-to-right">
			<label>
				<g:message code="welcome.ad.files" />
			</label>
		</div>
		<div class="col-xs-6 text-to-left">	
			<g:if test="${entity.smallImageUrl}">
				<a href="${entity.smallImageUrl?.encodeAsHTML()}" data-lightbox="image-${ entity.id }" 
					data-title="${message(code: 'welcome.ad.image.init')} ${ message(code: 'small')}">${ message(code:'small') }</a>
			</g:if>
			<g:if test="${entity.largeImageUrl}">
				<a href="${entity.largeImageUrl?.encodeAsHTML()}" data-lightbox="image-${ entity.id }" 
					data-title="${message(code: 'welcome.ad.image.init')} ${ message(code: 'large')}">${ message(code:'large') }</a>
			</g:if>
		</div>
		<span class="clearfix"></span>
	</g:if>
	<g:else> <!-- It's a Video -->
		<div class="col-xs-6 text-to-right">
			<label>
				<g:message code="welcome.message.file" />
			</label>
		</div>
		<div class="col-xs-6 text-to-left">			
			<g:if test="${ entity.videoUrl }">
				<a href="${ entity.videoUrl }" target="_blank">${ message(code:'download')}</a>
				<!-- Video Preview Removed because of Browser incompatibility  			
				<a href="#" id="view-video">${ message(code:'default.action.show')}</a>		
				<div id="video" class="hide">
					<video id="video_player_1" class="video-js vjs-default-skin"
						controls="true" preload="false"
						width="640px" height="264px" data-setup=''>
					  <source src="${entity.videoUrl}" type='video/mp4' />
					  <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that 
					  	<a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
					  </p>
					</video>
				</div>
				-->
			</g:if>
			<g:else>
				---
			</g:else>
		</div>
		<span class="clearfix"></span>
	</g:else>
</div>
<span class="clearfix"></span>