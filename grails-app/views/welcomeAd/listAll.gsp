<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="welcome.ad.title" /></title>
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'lightbox.css')}">
<script
	src="${resource(dir: 'themes/default/js', file: 'lightbox.min.js')}"></script>
<script src="${resource(dir: 'themes/default/js', file: 'video.js')}"></script>
<link rel="stylesheet" href="${resource(dir: 'themes/default/css', file: 'video-js.min.css')}" type="text/css">		
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
<script
	src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
<script
	src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_welcome_ad.js')}"></script>
<script>
	Exxo.module = "welcome-ad";
	Exxo.UI.vars["page-code"] = "welcome-ad-list";
	Exxo.Tables.loadPaginationInitValuesFromString('<custom:paginationInfo code="welcome-ad-list"/>');
	Exxo.UI.translates['okButton'] = "${message(code:'default.field.buttonOk')}";
	Exxo.UI.translates['cancelButton'] = "${message(code:'default.field.buttonCancel')}";
	Exxo.UI.translates['disableMessage'] = "${message(code:'mycentralserver.custombuttons.CustomButton.isDisabled')}";
</script>
</head>
</head>
<body>
<g:render template="/layouts/internalContentTemplate">
	<h1>
		<g:message code="welcome.ad.title" />
		<p class="muted"> ${ message(code:'click.on.thumbnails.for.preview') }</p>
	</h1>
	
	<div class="content">
		<g:render template="/layouts/messagesAndErrorsTemplate" />
		
           <table id="table-list" class="display" cellspacing="0" width="100%">
             <thead>
               <tr>			     
			     <th>${message(code: 'default.field.name')}</th>
			     <th>${message(code: 'default.home.locations')}</th>			     
			     <th>${message(code: 'default.field.type')}</th>
			     <th>${message(code: 'preview.files')}</th>
			     <th>${message(code: 'welcome.ad.field.skip.enable')}</th>
			     <th>${message(code: 'default.table.action')}</th>		
               </tr>
             </thead>
             <tfoot>
               <tr>
                <th>${message(code: 'default.field.name')}</th>
                <th>${message(code: 'default.home.locations')}</th>
		     	<th>${message(code: 'default.field.type')}</th>
		     	<th>${message(code: 'preview.files')}</th>
		     	<th>${message(code: 'welcome.ad.field.skip.enable')}</th>
		     	<th>${message(code: 'default.table.action')}</th>
               </tr>
             </tfoot>
             <tbody>
             	<g:each in="${list}" status="i" var="entity">
				<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					<td><g:link action="edit" id="${entity.id}">${entity.name.encodeAsHTML()}</g:link></td>
					<td><g:link action="edit" id="${entity.id}">${entity.locations.name.toString().replace("[", "").replace("]","").encodeAsHTML()}</g:link></td>
					<td>${message(code: "welcome.message.type." + entity.welcomeType.encodeAsHTML())}</td>	
					<td> 
						<g:if test="${ entity.welcomeType.encodeAsHTML() == "IMAGE"}">
							<g:if test="${entity.smallImageUrl}">								
								<a href="${entity.smallImageUrl?.encodeAsHTML()}" data-lightbox="image-${ entity.id }" 
									data-title="${message(code: 'welcome.ad.image.init')} ${ message(code: 'small')}">
									<img src="${entity.smallImageUrl?.encodeAsHTML()}"
										width="32px" height="48px"/>
								</a>
							</g:if>
							<g:if test="${entity.largeImageUrl}">
								<a href="${entity.largeImageUrl?.encodeAsHTML()}" data-lightbox="image-${ entity.id }" 
									data-title="${message(code: 'welcome.ad.image.init')} ${ message(code: 'large')}"></a>
							</g:if>
						</g:if>
						<g:else>
							<g:if test="${ entity.videoUrl }">
								<a href="${ entity.videoUrl }" target="_blank">${ message(code:'download')}</a>	
							</g:if>
							<g:else>
								---
							</g:else>
						</g:else>
					</td>
					<td>
					<g:if test="${entity.skipEnable == true}">
						<g:message code="default.field.yes" />
					</g:if> 
					<g:else>
						<g:message code="default.field.no" />
					</g:else>
					</td>
					<td>	
						<g:link action="show" id="${entity.id}" class="action-icons view" title="${message(code:'default.action.show')}"></g:link>
						<sec:ifAnyGranted roles="ROLE_HELP_DESK">
							<g:link action="edit" id="${entity.id}"  class="action-icons edit" title="${message(code:'default.action.edit')}"></g:link>
							<g:link action="assignLocation" id="${entity.id}" buttonEnable="true" class="action-icons viewLocation" title="${message(code:'default.action.management')}"></g:link>
							<g:link action="delete" id="${entity.id}"  class="action-icons delete" title="${message(code:'default.action.delete')}"></g:link>
						</sec:ifAnyGranted>
						<sec:ifNotGranted roles="ROLE_HELP_DESK">
							<g:if test="${ ""+entity.createdBy.id == ""+sec.loggedInUserInfo(field:'id')  }">
								<g:link action="edit" id="${entity.id}"  class="action-icons edit" title="${message(code:'default.action.edit')}"></g:link>
								<sec:ifNotGranted roles="ROLE_ADMIN">
								<g:link action="assignLocation" id="${entity.id}" buttonEnable="true" class="action-icons viewLocation" title="${message(code:'default.action.management')}"></g:link>
								</sec:ifNotGranted>
								<g:link action="delete" id="${entity.id}"  class="action-icons delete" title="${message(code:'default.action.delete')}"></g:link>
							</g:if>
						</sec:ifNotGranted>
					</td>
				</tr>
			</g:each>
             </tbody>
           </table>
           
           <br/>
           <!-- Information with the status of every transaction -->
			<g:render template="/layouts/tags/boxesUpdateResultTemplate" model="[boxesSyncResult: boxesSyncResult]"/>
	</div>
</g:render>
<div id="confirm-delete-dialog" title="${message(code:'default.confirm.title')}" style="display:none;">
  ${message(code:'general.object.confirm.delete.male', args:[message(code:'welcome.ad')])}
</div>
</body>
</html>