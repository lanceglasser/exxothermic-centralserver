<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="welcome.ad.edit.title" /></title>
<!-- Image Crop Required Files -->
<link rel="stylesheet" href="${resource(dir: 'css/jcrop', file: 'jquery.Jcrop.css')}" type="text/css">
<script src="${resource(dir: 'js/jcrop', file: 'jquery.Jcrop.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_images_upload.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_utils.js')}"></script>

<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_welcome_ad.js')}"></script>
<script>
	Exxo.module = "welcome-ad";
	var dimensionsImageError = "${message(code:'customButton.image.dimension.error')}";
	var invalidImageError = "${message(code:'image.invalid.error')}";
	var tooBigImageError = "${message(code:'image.big.error')}";
	Exxo.UI.vars["welcomeType"] = "${ entity.welcomeType }";
	Exxo.UI.vars["videoFileName"] = "${ entity.videoFileName }";
	Exxo.UI.translates[Exxo.Constants.ERROR_FILE_INVALID] = "${message(code:'file.invalid.error')}";
	Exxo.UI.translates[Exxo.Constants.ERROR_FILE_SIZE] = "${message(code:'file.big.error')}";
</script>
</head>
<body>
<g:render template="/layouts/internalContentTemplate">
	<h1>
		<g:message code="welcome.ad.edit.title" />
	</h1>
	
	<div class="content">
		<g:render template="/layouts/messagesAndErrorsTemplate" model="[entity: entity]"/>
		
		<g:uploadForm action="update" id="${params.id}" name="createForm" class="exxo-form">
			<g:hiddenField name="id" value="${entity?.id}" />
			<g:hiddenField name="version" value="${entity?.version}" />
			
			<g:render template="form"></g:render>
		</g:uploadForm>
		
		<!-- Information with the status of every transaction -->
		<g:render template="/layouts/tags/boxesUpdateResultTemplate" model="[boxesSyncResult: boxesSyncResult]"/>
	</div>
</g:render>
</body>
</html>