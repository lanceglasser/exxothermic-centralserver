<g:hiddenField name="wVideoUpd" value="0" />
			
<!-- Left Column -->
<div class="col-xs-6 resize-formleft">
	
	<!-- Name  -->				
	<custom:editField type="text" name="name" required="${true}" value="${entity.name}" 
		label="default.field.name" max="255"/>
	
	<!-- Enabled -->
	<custom:editField type="checkbox" name="skipEnable" required="${false}" value="${entity.skipEnable}" 
		label="welcome.ad.field.skip.enable" cssclass="input-checkbox"/>
		
	<!-- Allow Skip After Seconds  -->				
	<custom:editField type="number" name="skipTime" required="${true}" value="${entity.skipTime}" 
		label="welcome.message.skip.seconds" cssclass="onlyNumbers" min="1" max="600"/>	
	
</div>

<!-- Right Column -->
<div class="col-xs-6 resize-formright">	
				
	<!-- Type of Welcome Message -->
	<div class="col-xs-4 text-to-right">
		<label for="type"><g:message code="welcome.message.type" /></label>
	</div>
	<div class="col-xs-8 text-to-left">
		<g:select name="welcomeType.id" id="welcomeType" 
				from="${mycentralserver.app.WelcomeAd.WelcomeType?.values()}"
				optionValue="${ {id->g.message(code:'welcome.message.type.'+id) } }"
				value="${entity.welcomeType}" />
	</div>
	<span class="clearfix"></span>
				
	<!-- Welcome Image -->
	<div id="VIDEO-SELECT-DIV" class="welcome-file hide">
		<g:render template="/layouts/tags/fileUploadSelectorTemplate" 
			model="[
				id:				'welcomeVideoUrl',
				label:			message(code:'welcome.message.file'),
				accept:			'video/mp4'
				]"
		/>
		<br/>
		<div class="col-xs-4 text-to-right">
			<label for="welcomeVideoUrl">${ message(code:'video.requirements') }</label>
		</div>
		<div class="col-xs-8 text-to-left">
			<div class="video-requirements">
				${ message(code:'video.requirements.detail') }
			</div>
		</div>
		<span class="clearfix"></span>
	</div>
	<div id="IMAGE-SELECT-DIV" class="welcome-file hide">
		<!-- Small Image -->
		<g:render template="/layouts/tags/imageUploadSelectorTemplate" 
			model="[
				id:				'smallImageUrlFile',
				imgValue: 		entity.smallImageUrl,
				label:			message(code:'welcome.message.file.1'),
				ref:			'1',
				cropImage:		true,
				minWidth: 		mycentralserver.utils.Constants.IMAGE_PHONE_3_5_WIDTH,
				minHeight: 		mycentralserver.utils.Constants.IMAGE_PHONE_3_5_HEIGHT
				]"
		/>
		
		<!-- GenFeaturedImgForTablets -->
		<custom:editField type="checkbox" name="genImgForTablets" required="${false}" value="${entity.genImgForTablets}"
			label="auto.generate.image.for.tablets" cssclass="input-checkbox" 
			rightColumnPosition="right" leftColumnPosition="right" leftColumnSize="10"/>
		
		<div id="tabletImageObject" style="${ entity.genImgForTablets? 'display: none;':''}">
			<!-- Large Image -->
			<g:render template="/layouts/tags/imageUploadSelectorTemplate" 
				model="[
					id:				'largeImageUrlFile',
					imgValue: 		entity.largeImageUrl,
					label:			message(code:'welcome.message.file.3'),
					ref:			'3',
					cropImage:		true,
					minWidth: 		mycentralserver.utils.Constants.IMAGE_TABLET_WIDTH,
					minHeight: 		mycentralserver.utils.Constants.IMAGE_TABLET_HEIGHT
					]"
			/>
		</div>
		
		<custom:editField type="waGenExample" name="waGenExample" required="${false}" value="" label=""
			src="${resource(dir: 'themes/default/img', file: '640x960To2048x1536.png')}" visible="${ entity.genImgForTablets }"/>
		<br/>	
		<!-- "helpDiv" -->
		<div id="helpDiv" >
		            <div class="col-xs-4 text-to-right">
		              <label for="title">${message(code:'default.field.welcomead.help.label')}</label>
		            </div>
		            <div class="col-xs-8 text-to-left">
			            <a href="#" class="view-imageHelp">
			            	<img class="floatLeft" alt="No Image" src="${ resource(dir: 'themes/default/img', file: 'Help-Support.png') }">
			            </a>
			            <div class="noteHelp" >
				            <span class="liten-up floatLeft">
			            	${message(code:'default.field.welcomead.help.tab.note')}                         
			            	</span>
		            	</div>
		            </div>
		</div>
			
	</div>
	
	<span class="clearfix"></span>	
</div>
<span class="clearfix"></span>

<!--  Buttons  -->
<g:submitButton class="form-btn btn-primary" name="save" value="${message(code:'default.field.buttonSave')}" />
<g:link action="listAll" controller="welcomeAd" class="back-button">        
   <span class="form-btn btn-primary">
      ${message(code:'default.field.buttonCancel')}
   </span>       
</g:link>


<span class="clearfix"></span>
<div id="imageHelp-viewer" title="${message(code:'default.field.welcomead.help.title')}" style="display:none;">
<article class="tabs">

	<section id="tab1" style="min-height: 355px;">
		<h2 id="tab1"><a href="#tab1">${message(code:'default.field.welcomead.help.tab.phone')}</a></h2>
		<div class=welcomePhoneHelp >   
	   		<span>Info:	${message(code:'default.field.welcomead.help.phone.desc')}</span>
	   		<br/>
	   		<div class="phone-container-vert">
				<div class="wa-up-fill"></div>
				<div class="skip-message">${ message(code:'wa.example.skip.text') }</div>
				<div class="wa-img"></div>
				<div class="wa-text">
				${ message(code:'wa.example.text') }
				</div>
			</div>
		</div>
	</section>
	
	<section id="tab2"  style="min-height: 355px;">
		<h2><a href="#tab2">${message(code:'default.field.welcomead.help.tab.tablet')}</a></h2>		
		<span class="welcomeTabletTitle">Info:	${message(code:'default.field.welcomead.help.tablet.desc')}</span>	   		
	   	<br/>
		<table>
			<tr style="text-align: center;">
				<td width="300px;">${message(code:'default.field.welcomead.help.tablet.landscape')}</td>
				<td>${message(code:'default.field.welcomead.help.tablet.portrait')}</td></tr>
			<tr>
				<td>
					<div class="tablet-container-hori">
						<div class="internal-content">
							<div class="skip-message">${ message(code:'wa.example.skip.text') }</div>
							<div class="wa-img"></div>
							<div class="wa-text">
							${ message(code:'wa.example.text') }
							</div>
						</div>
					</div>
				</td>
				<td>
					<div class="tablet-container-vert">
						<div class="skip-message">${ message(code:'wa.example.skip.text') }</div>
						<div class="wa-img"></div>
						<div class="wa-text">
						${ message(code:'wa.example.text') }
						</div>
					</div>
				</td>
			</tr>
		</table>
		<div class="clearer"></div>
	</section>

</article>
</div>