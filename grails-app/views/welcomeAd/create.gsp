<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="welcome.ad.menu.create" /></title>
<link rel="stylesheet" href="${resource(dir: 'css/pluggins/bootstrap-colorpicker', file: 'bootstrap-colorpicker.css')}" type="text/css">
<link rel="stylesheet" href="${resource(dir: 'css/jcrop', file: 'jquery.Jcrop.css')}" type="text/css">		
<link rel="stylesheet" href="${resource(dir: 'css/jcrop', file: 'demos.css')}" type="text/css">	
<script src="${resource(dir: 'js/pluggins/bootstrap-colorpicker', file: 'bootstrap-colorpicker.js')}"></script>
<script src="${resource(dir: 'js/pluggins/bootstrap-colorpicker', file: 'docs.js')}"></script>
<script src="${resource(dir: 'js/jcrop', file: 'jquery.Jcrop.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_welcome_ad.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_utils.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_images_upload.js')}"></script>
<script>
	Exxo.module = "welcome-ad";
	var dimensionsImageError = "${message(code:'customButton.image.dimension.error')}";
	var invalidImageError = "${message(code:'image.invalid.error')}";
	var tooBigImageError = "${message(code:'image.big.error')}";
	Exxo.UI.vars["welcomeType"] = "${ entity.welcomeType }";
	Exxo.UI.vars["videoFileName"] = "";
	Exxo.UI.translates[Exxo.Constants.ERROR_FILE_INVALID] = "${message(code:'file.invalid.error')}";
	Exxo.UI.translates[Exxo.Constants.ERROR_FILE_SIZE] = "${message(code:'file.big.error')}";
	Exxo.UI.translates['closeButton'] = "${message(code:'default.field.buttonClose')}";
</script>
</head>
<body>
<g:render template="/layouts/internalContentTemplate">
	<h1>
		<g:message code="welcome.ad.menu.create" />
	</h1>
	
	<div class="content">
		<g:render template="/layouts/messagesAndErrorsTemplate" model="[entity: entity]"/>
		
		<g:uploadForm action="save" id="createForm" name="createForm" class="exxo-form">
			
			<g:render template="form"></g:render>
			
		</g:uploadForm>
	</div>
</g:render>
</body>
</html>