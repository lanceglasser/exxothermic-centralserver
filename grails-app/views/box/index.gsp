<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title><g:message code="default.title.Box" /> - ${location?.name}</title>
		<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
		<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
		<script src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
		<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
		<script src="${resource(dir: 'js/pages/box', file: 'index.js')}"></script>
		<script>
			Exxo.module = "employee-list";
			Exxo.UI.translates['okButton'] = "${message(code:'default.field.buttonOk')}";
			Exxo.UI.translates['cancelButton'] = "${message(code:'default.field.buttonCancel')}";
			Exxo.UI.translates['disableMessage'] = "${message(code:'mycentralserver.box.resetBox')}";
		</script>
		
		
	</head>
	<body>
	<section class="introp">
		  <div class="intro-body">
		    <div class="container full-width">
		      <div class="row">
		        <div class="col-md-12" id="internal-content">
		          <h1><g:message code="default.title.Box" /> - ${location?.name}</h1>
					<g:render template="/layouts/messagesAndErrorsTemplate"/>
				<div class="content">
					<g:if test="${boxes.size()>0}">
						<g:render template="/box/boxesTableTemplate" model="[boxes: boxes]"/>
		            </g:if>
		          </div>
		        </div>
		      </div>
		    </div>
		  </div>
		</section>
			
	<div id="confirm-reset-dialog"
		title="${message(code:'mycentralserver.box.resetBox.title')}"
		style="display: none;">
		${message(code:'mycentralserver.box.resetBox')}
	</div>
	
	</body>
</html>