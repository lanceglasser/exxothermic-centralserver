<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.Box" /></title>
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
<script
	src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
<script
	src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_box_block.js')}"></script>
<script>
			Exxo.module = "box-block";
			Exxo.UI.urls["black-list-page"] = "${g.createLink(controller: 'box', action: 'blackList', absolute: true)}";
			Exxo.UI.translates['okButton'] = "${message(code:'default.field.buttonOk')}";
			Exxo.UI.translates['cancelButton'] = "${message(code:'default.field.buttonCancel')}";
			Exxo.UI.translates['disableMessage'] = "${message(code:'mycentralserver.box.resetBox')}";
		</script>
</head>
<body>
	<section class="introp">
		<div class="intro-body">
			<div class="container full-width">
				<div class="row">
					<div class="col-md-12" id="internal-content">
						<h1 id="responsive-header">
							<g:message code="block.venue.server" />
						</h1>
						<div class="content">
							<g:render template="blockTemplate"></g:render>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</body>
</html>