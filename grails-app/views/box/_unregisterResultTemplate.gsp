<g:if test="${unregisterResult}">
	<div id="syncResultContainer">
		<legend>
			<g:message code="unregister.process.results" />
		</legend>
		<table id="table-list" class="display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>${message(code:'default.field.serial')}</th>
					<th>${message(code:'default.field.name')}</th>
					<th>${message(code:'default.field.location')}</th>
					<th>${message(code:'default.field.status')}</th>
				</tr>
			</thead>
			<tbody>
				<g:each in="${unregisterResult}" status="i" var="record">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<td>${record[0]}</td>
						<td>${record[1]}</td>
						<td>${record[2]}</td>
						<td>${record[3]? message(code:'boxes.sync.result.status.true'):message(code:'boxes.sync.result.status.false')}</td>
					</tr>
				</g:each>
			</tbody>
		</table>
	</div>
</g:if>