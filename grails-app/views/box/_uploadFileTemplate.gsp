<!DOCTYPE html>
<html>
	<head>
		<title><g:message code="default.title.customeButtons.updateContent" /></title> 
		<!-- Image Crop Required Files -->
		<link rel="stylesheet" href="${resource(dir: 'css/jcrop', file: 'jquery.Jcrop.css')}" type="text/css">
		<script src="${resource(dir: 'js/jcrop', file: 'jquery.Jcrop.js')}"></script>
		<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_images_upload.js')}"></script>
		<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_utils.js')}"></script>
		<script src="${resource(dir: 'themes/default/js/exxo', file: 'upload.js')}"></script>
	</head>
	<body>	
		<g:uploadForm  name="myUpload" action="uploadFile" class="exxo-form" >
			 <legend>${message(code:'default.upload.image')}</legend>
			<!-- hidden crop params -->
			<g:hiddenField name="id" value="${channel?.id}" />
			<div style="text-align: right;">		 
			<fieldset> 
				<g:render template="/layouts/tags/imageUploadSelectorTemplate" 
						model="[
							id:				'file',
							imgValue: 		channel.imageURL,
							label:			message(code:'default.field.customeButtons.buttonImage'),
							cropImage:		true,
							minWidth: 		mycentralserver.utils.Constants.CHANNEL_IMAGE_SMALL_WIDTH,
							minHeight: 		mycentralserver.utils.Constants.CHANNEL_IMAGE_SMALL_HEIGHT]"
					/>
					
				<!--  BUTTONS  -->
				<div class="form-actions" style="margin-bottom: 0px;margin-top: 0px;padding-right: 14px;">
					<g:submitButton class="form-btn btn-primary" name="update" value="${message(code:'default.field.buttonSave')}"/>					 
					<button class="form-btn btn-primary" type="button"  value="${message(code:'default.field.buttonSave')}" onClick='$("#dialogPlaceholder").dialog("close");'>${message(code:'default.field.buttonClose')}</button>
				</div>  		
			</fieldset>
			</div>
		</g:uploadForm>
		
<!-- Ajax Flow Container -->
<div id="ajaxFlowWaitImg" class="ajaxFlow" style="display:none;">
	<span class="waitBackground"></span>
	<span class="waiter">
		<span class="wait">
			<span class="title">${ message(code:'please.wait') }</span>
			<span class="spinner"></span>
		</span>
	</span>
</div>

		<script lang="javascript">
				var dimensionsImageError = "${message(code:'small.image.dimensions.error')}";
				var invalidImageError = "${message(code:'image.invalid.error')}";
				var tooBigImageError = "${message(code:'image.big.error')}";
				Exxo.UI.urls['baseURL'] = '${createLink(uri: '', absolute : 'true')}';
				Exxo.UI.translates[Exxo.Constants.ERROR_FILE_INVALID] = "${message(code:'file.invalid.error')}";
				Exxo.UI.translates[Exxo.Constants.ERROR_FILE_SIZE] = "${message(code:'file.big.error')}";
				Exxo.UI.translates['select-image-before-save'] = "${message(code:'select.image.before.save')}";
		</script>
	</body>
</html>