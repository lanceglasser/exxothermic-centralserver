<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.editBox" /></title>
<!-- Includes for Slider -->
<link rel="stylesheet" href="${resource(dir: 'themes/default/js', file: 'nouislider.pips.css')}" type="text/css">
<link rel="stylesheet" href="${resource(dir: 'themes/default/js', file: 'nouislider.css')}" type="text/css">
<script src="${resource(dir: 'themes/default/js', file: 'nouislider.js')}"></script>
<script src="${resource(dir: 'themes/default/js', file: 'wNumb.js')}"></script>
<!-- Includes for Number Input Spinner -->
<link rel="stylesheet" href="${resource(dir: 'themes/default/css', file: 'jquery.bootstrap-touchspin.css')}" type="text/css">
<script src="${resource(dir: 'themes/default/js', file: 'jquery.bootstrap-touchspin.js')}"></script>

<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_box_edit.js')}"></script>
<!-- <script src="${resource(dir: 'js/util', file: 'util.js')}"></script> -->
<script src="${resource(dir: 'themes/default/js/exxo', file: 'util.js')}"></script>

<script>
	Exxo.module = "box-edit";
	Exxo.UI.translates['okButton'] = "${message(code:'default.field.buttonOk')}";
	Exxo.UI.translates['cancelButton'] = "${message(code:'default.field.buttonCancel')}";
	Exxo.UI.translates['emptyChannel'] = "${message(code:'mycentralserver.box.BoxChannel.channelLabel.blank')}";
	Exxo.UI.translates['regex']		   = "${message(code:'channel.field.validation.name.message')}";
	Exxo.UI.translates['regexDesc']    = "${message(code:'channel.field.validation.description.message')}";
	Exxo.UI.translates['channel-image-remove-error'] = "${message(code:'channel-images-remove-error')}";
	Exxo.UI.translates['no-channels-to-update'] = "${message(code:'no.channels.to.update')}";
	Exxo.UI.translates['unknown.error'] = "${ message(code:'') }";
	Exxo.UI.translates['confirm.audio.input.change'] = "${message(code:'confirm.audio.input.change')}";
	Exxo.UI.vars['serial-number'] = "${box?.serial}";
	Exxo.UI.vars['is-connected'] = ${ isConnected? "true":"false"};
</script>
</head>
<body>
<g:render template="/layouts/internalContentTemplate">
	<h1>
		<g:message code="default.title.editBox" />
	</h1>
	
	<div class="content">
		<g:render template="/layouts/messagesAndErrorsTemplate"/>
		
		<g:form action="update" id="${box.id}" name="form-box-config" class="exxo-form">
			<g:hiddenField name="id" value="${box?.id}" />
			<g:hiddenField name="version" value="${box?.version}" />
			<g:hiddenField name="sucessfulMessage" value="${message(code:'default.action.channel.ready')}" />
			<g:hiddenField name="failMessage" value="${message(code:'default.action.channel.error')}" />
			
			<!-- Left Column -->
			<div class="col-xs-6 resize-formleft">				
				<!-- Serial Number -->
				<span id="serialDiv">
		            <div class="col-xs-4 text-to-right">
		              <label for="model"> ${ message(code:'default.field.serial')} </label>
		            </div>
		            <div class="col-xs-8 text-to-left exxo-label">
		             	${box.serial} 
		            </div>
				</span>
				<span class="clearfix"></span>
				
				<!-- Version / Model -->
				<span id="modelDiv">
		            <div class="col-xs-4 text-to-right">
		              <label for="model"> ${message(code: 'default.field.version')} / ${message(code: 'Model')} </label>
		            </div>
		            <div class="col-xs-8 text-to-left exxo-label">
		             	${ box.getBoxVersion(message(code: 'default.field.empty')) } / ${(box.model && box.model != '')? box.model:'-'}
		            </div>
				</span>
				<span class="clearfix"></span>
				
				<!-- Bios -->
				<span id="biosDiv">
		            <div class="col-xs-4 text-to-right">
		              <label for="bios"> ${message(code: 'default.field.bios')} </label>
		            </div>
		            <div class="col-xs-8 text-to-left exxo-label">
		             	${ box.bios? box.bios:'-' }
		            </div>
				</span>
				<span class="clearfix"></span>
				
				<!-- Last Ip Connection -->
				<span id="biosDiv">
		            <div class="col-xs-4 text-to-right">
		              <label for="bios"> ${message(code: 'connection.last.ip_address')} </label>
		            </div>
		            <div class="col-xs-8 text-to-left exxo-label">
		             	${ boxIp? boxIp.ipAddress:'-' }
		            </div>
				</span>
				<span class="clearfix"></span>
				
				<!-- Most recent connection MAC address -->
				<span id="hwaddrDiv">
		            <div class="col-xs-4 text-to-right">
		              <label for="hwaddr"> ${message(code: 'connection.last.hardware_address')} </label>
		            </div>
		            <div class="col-xs-8 text-to-left exxo-label">
		             	${ boxIp ? boxIp.macAddress : message(code: 'connection.last.hardware_address.unavailable') }
		            </div>
				</span>
				<span class="clearfix"></span>

        <!-- LAN manager passcode -->
        <span id="lmpasscodeDiv">
          <div class="col-xs-4 text-to-right">
            <label for="lmpasscode"> ${message(code: 'management.admin.passcode')} </label>
          </div>
          <div class="col-xs-8 text-to-left exxo-label">
            <!-- TODO: retrieve actual passcode -->
            ${ boxManufacturer.managementPasscode ? boxManufacturer.managementPasscode
                                                  : management.admin.passcode.unavailable }
          </div>
        </span>
        <span class="clearfix"></span>

				<!-- Venue -->
				<span id="venueDiv">
		            <div class="col-xs-4 text-to-right">
		              <label for="venue"> ${message(code: 'default.field.location')} </label>
		            </div>
		            <div class="col-xs-8 text-to-left exxo-label">
		             	<g:link controller="location" action="dashboard" id="${box.location.id}">
		             		${box.location.name.encodeAsHTML()}
						</g:link>
		            </div>
				</span>
				<span class="clearfix"></span>
				
				<!-- Company -->
				<span id="companyDiv">
		            <div class="col-xs-4 text-to-right">
		              <label for="company"> ${message(code: 'default.field.company')} </label>
		            </div>
		            <div class="col-xs-8 text-to-left exxo-label">
		             	<g:link controller="company" action="dashboard" id="${box.location.company.id}">
		             		${ box.location.company.encodeAsHTML() }
						</g:link>
		            </div>
				</span>
				<span class="clearfix"></span>
											
				<!-- Name -->			
				<custom:editField type="text" name="name" required="${false}" value="${box.name}" 
					label="${message(code: 'default.field.name') + " " + message(code: 'default.field.optionaL')}"/>
				
				<!-- AudioCardGeneration and JumperConfiguration can be change by HelpDesk Users -->
				<box:renderAudioCardInfoSelect isEditPage="true" box="${ box }" labelCssClass="exxo-label"/>
        
				<!-- PA -->
				<g:if test="${canBePA}">
	                <custom:editField type="checkbox" name="pa" required="${false}" value="${box.pa}"
	                    label="default.field.pa" cssclass="input-checkbox"  disabled="${ isConnected? "false":"true" }"
	                    note="${ message(code: 'default.pa.note') } / ${ message(code:'pa.edit.when.connect.only') }"/>
				</g:if>
			</div>
			
			<!-- Right Column -->
			<div class="col-xs-6 resize-formright">
				<g:if test="${box.connected()}">
					<!-- Print Box Configurations -->
					<box:renderConfigs box="${ box }" locale="${ message(code:'curent.session.locale') }"/>
					<span class="clearfix"></span>
				</g:if>
			</div>
			<span class="clearfix"></span>
									    
		    <!-- Buttons -->
		    <g:submitButton class="form-btn btn-primary" name="register"
				value="${message(code:'default.field.buttonSave')}" />
				
			<g:link action="listAll" controller="box" class="back-button">        
		       <span class="form-btn btn-primary">
		          ${message(code:'default.field.buttonCancel')}
		       </span>       
	    	</g:link>
		</g:form>
	
	    <g:set var="disabledText" value="${ isConnected? '':' disabled="disabled"' }" />
	    
	    <g:if test="${isConnected}">
	    <!-- Container with special options -->
		<sec:access expression="hasAnyRole('ROLE_HELP_DESK')">
		   <div class="box-special-options">
		       <h1>
		           ${ message(code:'box.special.actions', args:[message(code:'default.box.box')]) }
		       </h1>
		       <div>
		           <g:link controller="box" action="sendActionToServer" class="back-button send-action-btn" btn-action="DTVSet">
		              <span class="form-btn btn-primary">
		                 ${message(code:'box.action.dtv.set')}
		              </span>
		           </g:link>
		           <span class="message">${message(code:'box.action.dtv.set.message', args:[message(code:'default.box.box'), message(code:'box.action.dtv.set')])}</span>
		       </div>
		       <div>
                   <g:link controller="box" action="sendActionToServer" class="back-button send-action-btn" btn-action="StartSSHTunnel">
                      <span class="form-btn btn-primary">
                         ${message(code:'box.action.startSSHTunnel')}
                      </span>
                   </g:link>
                   <span class="message">${message(code:'box.action.startSSHTunnel.message', args:[message(code:'default.box.box'), message(code:'box.action.startSSHTunnel')])}</span>
               </div>
               <div>
                   <g:link controller="box" action="sendActionToServer" class="back-button send-action-btn" btn-action="StopSSHTunnel">
                      <span class="form-btn btn-primary">
                         ${message(code:'box.action.stopSSHTunnel')}
                      </span>
                   </g:link>
                   <span class="message">${message(code:'box.action.stopSSHTunnel.message', args:[message(code:'default.box.box'), message(code:'box.action.stopSSHTunnel')])}</span>
               </div>
		   </div>
		</sec:access>
	    </g:if>

	    <g:uploadForm action="updateChannels" name="updateChannels" class="exxo-form">
	        <div id='channelscontent'>
	           <br/>
	        <g:if test="${isConnected}">
	           <div id="process" class="alert alert-warning" style="display: none;">
                   <a class="close" data-dismiss="alert">×</a>
                   <div id="alert-warnMessage">
                       ${message(code:'default.action.process')}
                   </div>
               </div>
               <br/>
               <div id="containerSuccessMessage">
                   <!-- See the utils.js this html is created by the javaScript -->
                   <div id="ready" class="alert alert-success" style="display: none;">
                       <a class="close" data-dismiss="alert">×</a>
                       <div id="alert-sucessMessage">
                           ${message(code:'default.action.channel.ready')}
                       </div>
                       <div class='channels'></div>
                   </div>
               </div>

               <div id="containerFailMessage">
                   <!-- See the utils.js this html is created by the javaScript -->
                   <div id="error" class="alert alert-error" style="display: none;">
                       <a class="close" data-dismiss="alert">×</a>
                       <div id="alert-errorMessage">
                           ${message(code:'default.action.channel.error')}
                           ${flash.error}
                           ${flash.error=null}
                       </div>
                       <div class='channels'></div>
                   </div>
               </div>
	        </g:if>
	        
	        <g:if test="${listChannels}">
	            <br/>
	            <h1>
	                <g:message code="default.title.channels" />
	            </h1>
	            <p class="muted">${message(code:'box.channels.description.message')}, ${message(code:'box.edit.channels.order.message')}</p>
	            <g:if test="${isConnected}">
	            <p class="muted">${ message(code:'channels.image.update') }</p>
	            </g:if>
	            <g:else>
                <p class="muted">${ message(code:'disconnected.server.channels.info') }</p>
	            </g:else>
	            
	            <table id="channels-table" class='table table-striped'>
	                <thead>
	                    <tr>
	                        <th style="text-align: center;">${message(code:'enable') }</th> 
	                        <th>${message(code:'default.field.image')}</th>
	                        <th>${message(code:'large.image')}</th>
	                        <th>${message(code:'default.field.channelLabel')}</th>
	                        <th>${message(code:'default.field.description')}</th>
	                        <th>${message(code:'gain.level')}</th>
                            <g:if test="${ canSetChannelsDelay }">
                            <th style="width: 150px">${message(code:'delay')} (${message(code:'milliseconds')})</th>
                            </g:if>
	                    </tr>
	                </thead>
	                <tbody style="">
	                    <g:hiddenField name="serialNumber" value="${box?.serial}" />
	
	                    <g:each in="${listChannels}" var="channel" status="currentId">
	                        <g:if test="${!channel.isPa}">
	                            <tr id="${channel.channelNumber}" class="${(currentId % 2) == 0 ? '' : 'odd'}">
	                                <!-- Enable Column -->
	                                <td style="text-align: left; vertical-align: middle;">
	                                    <custom:editField type="checkbox" name="enableForApp" required="${false}" value="${channel.enabledForApp}"
	                                          cssclass="input-checkbox" disabled="${ isConnected? "false":"true" }"/>
	                                    <g:field type="hidden" name="oldEnableForApp" value="${channel.enabledForApp}" />
	                                </td>
	                                
	                                
	                                <g:field type="hidden" id="channel" name="channel"
	                                    required="" value="${channel.channelNumber}" />
	                                <g:if test="${channel.channelPort}">
	
	                                    <td style="text-align: left; vertical-align: middle;">
	                                        <g:field type="hidden" id="oldImageURL"
	                                            name="oldImageURL" value="${channel.imageURL}" /> 
	                                        <g:field type="hidden" id="imageURL" name="imageURL"
	                                            value="${channel.imageURL}" />
	                                        <div id="demo_${channel.channelNumber}"
	                                            class="small-image-frame left">
	                                            <img id="target_${channel.channelNumber}"
	                                                src="${channel.imageURL}"
	                                                name="target_${channel.channelNumber}"
	                                                style="width: 100%; height: 100%;${channel.imageURL? '':' display: none'}" />
	                                        </div> 
	                                        <g:if test="${isConnected}">
	                                        <div class="channel-images-btns">
                                                <g:link channel-id="${channel.id}" class="action-icons softwareUpdate btn-upload-image"
                                                    title="${message(code:'default.button.upload')}">
                                                </g:link>                                                       
                                                <g:link action="deleteChannelImage" channel-id="${channel.id}" class="action-icons delete btn-del-image"
                                                    title="${message(code:'remove')}" image-type="small" channel-number="${channel.channelNumber}">
                                                </g:link>
                                            </div>
	                                        </g:if>
	                                        <span class="clearfix"></span>
	                                    </td>
	                                </g:if>
	                                <g:else>
	                                    <td style="text-align: center">
	                                    </td>
	                                </g:else>
	                                <g:if test="${channel.channelPort}">
	                                    <td style="text-align: left; vertical-align: middle;">
	                                        <g:field type="hidden" id="oldLargeImageURL"
	                                            name="oldLargeImageURL"
	                                            value="${channel.largeImageURL}" /> <g:field
	                                            type="hidden" id="largeImageURL" name="largeImageURL"
	                                            value="${channel.largeImageURL}" />
	                                        <div id="largeImageDemo_${channel.channelNumber}"
	                                            class="large-image-frame left">
	                                            <img id="largeImageTarget_${channel.channelNumber}"
	                                                src="${channel.largeImageURL}"
	                                                name="largeImageTarget_${channel.channelNumber}"
	                                                style="width: 100%; height: 100%;${channel.largeImageURL? '':' display: none'}" />
	                                        </div>
	                                        <g:if test="${isConnected}">
	                                        <div class="channel-images-btns">
                                                <g:link channel-id="${channel.id}" class="action-icons softwareUpdate btn-upload-large-image"
                                                    title="${message(code:'default.button.upload')}">
                                                </g:link>                                                       
                                                <g:link action="deleteChannelImage" channel-id="${channel.id}" class="action-icons delete btn-del-image"
                                                    title="${message(code:'remove')}" image-type="large" channel-number="${channel.channelNumber}">
                                                </g:link>
                                            </div>
                                            </g:if>
	                                        <span class="clearfix"></span>  
	                                    </td>
	                                </g:if>
	                                <g:else>
	                                    <td style="text-align: center"></td>
	                                </g:else>
	
	                                <td style="vertical-align: middle;">
	                                    <g:field type="text" class="input-small" id="label"
	                                        name="label" required="" disabled="${ !isConnected }"
	                                        maxlength="13" 
	                                        value="${channel.getChannelLabel()}" /> <g:field
	                                        type="hidden" id="oldlabel" name="oldlabel"
	                                        required="" value="${channel.getChannelLabel()}" />
	                                </td>
	                                <td style="vertical-align: middle;"><g:textArea
	                                        maxlength="100" id="description" class="input-large"  disabled="${ !isConnected }"
	                                        name="description" value="${channel.getChannelDescription()}" />
	                                    <g:field type="hidden" id="oldDescription"
	                                        name="oldDescription" required=""
	                                        value="${channel.getChannelDescription()}" />
	                                </td>
	                                <td style="text-align: left; vertical-align: top; padding-left: 15px;">
	                                    <g:field type="hidden" id="gain-${channel.id}"
	                                            name="gain" value="${channel.gain}" />
	                                    <g:field type="hidden" name="oldGain" value="${channel.gain}" />
	                                    <div class="gainSelect" style="width: 60%;"
	                                        rel="${channel.id}" level="${channel.gain}"></div>
	                                </td>
                                    <td style="vertical-align: middle;">
                                        <g:field type="hidden" name="oldDelay" value="${channel.delay}" />
                                        <g:if test="${ canSetChannelsDelay }">
                                        <input type="text" name="delay" value="${channel.delay}" min="0" max="3000" step="10"
                                            class="numberspinner onlyNumbers" ${ isConnected? '':'disabled="disabled"'}/>
                                        </g:if>
                                        <g:else>
                                        <g:field type="hidden" name="delay" value="${channel.delay}" />
                                        </g:else>
                                    </td>
	                            </tr>
	                        </g:if>
	                    </g:each>
	                </tbody>
	            </table>
	        </g:if>
                    
	        <g:if test="${isConnected}">
	           <!-- JS -->
               <g:link action="setChannel" name="urlLinkToSyncronize">
                   <span class="form-btn btn-primary" id="btnSyncronize"
                       name="btnSyncronize"> ${message(code:'default.field.buttonSave')}
                   </span>
               </g:link>
            </g:if>
            
            </div> <!-- channelscontent -->
		</g:uploadForm>
	</div>
</g:render>
<div id="confirm-delete-dialog" title="${message(code:'default.confirm.title')}" style="display:none;">
  ${message(code:'general.object.confirm.delete.female', args:[message(code:'default.field.image')])}
</div>
<!-- Div for the Image Upload -->
<div id="dialogPlaceholder" style="display: none;" title="${ message(code:'default.upload.image') }"></div>
</body>
</html>
