<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main" />
		<title><g:message code="default.title.Box.logFile" /> - ${location?.name}</title>
		<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
		<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
		<script src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
		<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
		<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_box_list.js')}"></script>
		<script>
			Exxo.module = "box-list";
			Exxo.UI.translates['okButton'] = "${message(code:'default.field.buttonOk')}";
			Exxo.UI.translates['cancelButton'] = "${message(code:'default.field.buttonCancel')}";
			Exxo.UI.translates['disableMessage'] = "${message(code:'mycentralserver.box.resetBox')}";
		</script>
		</head>
	<body>
	<section class="introp">
	  <div class="intro-body">
	    <div class="container full-width">
	      <div class="row">
	        <div class="col-md-12" id="internal-content">
	          <h1><g:message code="default.title.Box.logFile" /></h1>
				<g:if test="${flash.error}">
					<div class="alert alert-error">
						<a class="close" data-dismiss="alert">×</a>
						${flash.error}
					</div>
				</g:if>
				<g:if test="${flash.success}">
					<div class="alert alert-success">
						<a class="close" data-dismiss="alert">×</a>
						${flash.success}
						${flash.success=null}
					</div>
				</g:if>
			<div class="content">
				<g:if test="${jsonWithLogFile?.data?.size() > 0}">
	            <table id="table-list" class="display" cellspacing="0" width="100%">
	              <thead>
	                <tr>
				     	<th>${message(code: 'default.field.name')}</th>
						<th>${message(code: 'default.table.action')}</th>
	                </tr>
	              </thead>
	              <tfoot>
	                <tr>
	                 	<th>${message(code: 'default.field.name')}</th>
						<th>${message(code: 'default.table.action')}</th>		
	                </tr>
	              </tfoot>
	              <tbody>
	              	<g:each in="${jsonWithLogFile?.data?}" status="i" var="fileName">
						<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
							<td>
								<g:if test="${fileName}">
									${fileName}
								</g:if> <g:else>
									<g:message code="default.field.empty" />
								</g:else></td>
							<td>							
								<g:link
									action="downloadLogFile" id="${boxId}" params="[file:fileName]"
									class="action-icons download"
									title="${message(code:'default.action.download')}"
									target="_blank">
								</g:link>
								<g:link action="deleteLogFile" id="${boxId}"
									params="[file:fileName]" class="action-icons deleteLog"
									title="${message(code:'default.action.delete')}"></g:link>
							</td>
						</tr>
					</g:each>
	              </tbody>
	            </table>
	            </g:if>
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	</section>
	</body>
</html>