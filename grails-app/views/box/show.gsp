<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.Box" /></title>
<script>
	Exxo.module = "box-show";
</script>
</head>
<body>
	<section class="intro">
		<div class="intro-body">
			<div class="container full-width">
				<div class="row">
					<div class="col-md-12" id="internal-content">
						<h1>
							${box.serial.encodeAsHTML()}
						</h1>

						<div class="content form-info">
							<!-- Left Column -->
							<div class="col-xs-6 resize-formleft">
								<div class="col-xs-6 text-to-right">
									<span class="field-label"><g:message code="default.field.serial" /></span>
								</div>
								<div class="col-xs-6 text-to-left">
								<g:if test="${box.serial}">
									<span class="field-value">${box.serial.encodeAsHTML()}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty" /></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
								<div class="col-xs-6 text-to-right">
									<span class="field-label"><g:message code="Model" /></span>
								</div>
								<div class="col-xs-6 text-to-left">
								<g:if test="${box.model && box.model != ''}">
									<span class="field-value">${box.model.encodeAsHTML()}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty" /></span>
								</g:else>
								</div>
								
								<div class="col-xs-6 text-to-right">
									<span class="field-label"><g:message code="default.field.bios" /></span>
								</div>
								<div class="col-xs-6 text-to-left">
								<g:if test="${box.bios && box.bios != ''}">
									<span class="field-value">${box.bios.encodeAsHTML()}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty" /></span>
								</g:else>
								</div>
								
								<span class="clearfix"></span>
								
								<div class="col-xs-6 text-to-right">
									<span class="field-label"><g:message code="default.field.location" /></span>
								</div>
								<div class="col-xs-6 text-to-left">
								<g:if test="${box.location?.name}">
									<span class="field-value">${box.location?.name.encodeAsHTML()}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty"/></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
								<div class="col-xs-6 text-to-right">
									<span class="field-label"><g:message code="default.field.name" /></span>
								</div>
								<div class="col-xs-6 text-to-left">
								<g:if test="${box.name}">
									<span class="field-value">${box.name}</span>
								</g:if>
								<g:else>
									<span class="field-value"><g:message code="default.field.empty"/></span>
								</g:else>
								</div>
								<span class="clearfix"></span>
								
								<!-- AudioCardGeneration and JumperConfiguration -->
                                <box:renderAudioCardInfoSelect readOnly="true" box="${ box }" leftSize="6"/>
                
								<g:if test="${canBePA}">
				                    <div class="col-xs-6 text-to-right">
	                                    <span class="field-label"><g:message code="default.field.pa" /></span>
	                                </div>
	                                <div class="col-xs-6 text-to-left">
	                                <g:if test="${box.pa == true}">
	                                    <span class="field-value"><g:message code="default.field.yes"/></span>
	                                </g:if>
	                                <g:else>
	                                    <span class="field-value"><g:message code="default.field.no"/></span>
	                                </g:else>
	                                </div>
	                                <span class="clearfix"></span>
				                </g:if>
							</div>
							<!-- Right Column -->
							<div class="col-xs-6 resize-formright">
								<!-- Print Box Configurations -->
								<box:renderConfigs box="${ box }" locale="${ message(code:'curent.session.locale') }" readOnly="true"/>
							</div>
							<span class="clearfix"></span>
							
							<!-- Buttons -->
							<g:link action="listAll" id="${box.location.id}" controller="box" params="[offset: offset]">
							    <input type="button" class="form-btn btn-primary" value="${message(code:'default.field.buttonBack')}"/>
							 </g:link>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</body>
</html>