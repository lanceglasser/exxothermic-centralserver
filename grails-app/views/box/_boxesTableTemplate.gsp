<table id="box-list" class="display" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>
				${message(code: 'default.field.serial')} / ${message(code: 'default.field.location')}
			</th>
			<th>
				${message(code: 'default.field.name')}
			</th>
			<th>
				${message(code: 'default.field.pa')}
			</th>
			<th>
				${message(code: 'default.field.version')} / ${message(code: 'Model')}
			</th>
			<th>
				${message(code: 'default.field.updateDate')}
			</th>
			<th style="display:none;">
				lastUpdate
			</th>
			<th>
				${message(code: 'default.table.action')}
			</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<th>
				${message(code: 'default.field.serial')} / ${message(code: 'default.field.location')}
			</th>
			<th>
				${message(code: 'default.field.name')}
			</th>
			<th>
				${message(code: 'default.field.pa')}
			</th>
			<th>
				${message(code: 'Model')} / ${message(code: 'default.field.version')}
			</th>
			<th>
				${message(code: 'default.field.updateDate')}
			</th>
			<th style="display:none;">
				lastUpdate
			</th>
			<th>
				${message(code: 'default.table.action')}
			</th>
		</tr>
	</tfoot>
	<tbody>
		<g:each in="${boxes}" status="i" var="boxInstance">
			<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
				<td><sec:ifAnyGranted roles="ROLE_ADMIN">
						${boxInstance.serial} / ${boxInstance.location.name.encodeAsHTML()}
					</sec:ifAnyGranted> <sec:ifNotGranted roles="ROLE_ADMIN">
						<g:link controller="box" action="edit" id="${boxInstance.id}">
							${boxInstance.serial} / ${boxInstance.location.name.encodeAsHTML()}
						</g:link>
					</sec:ifNotGranted></td>
				<td><g:if test="${boxInstance.name}">
						${boxInstance.name.encodeAsHTML()}
					</g:if> <g:else>
						<g:message code="default.field.empty" />
					</g:else></td>

				<td><g:if test="${boxInstance.pa}">
						<g:message code="default.field.yes" />
					</g:if> <g:else>
						<g:message code="default.field.no" />
					</g:else></td>

				<td>
					${ boxInstance.getBoxVersion(message(code: 'default.field.empty')) } / ${(boxInstance.model && boxInstance.model != '')? boxInstance.model:'-'} 
				</td>

				<td><g:if test="${boxInstance.connected() == true}">
						<i class="state stateConnected"></i>
						<span class="greenText" title="${boxInstance.status.description}">
							${boxInstance.lastConnected(message(code:'default.text.messageFormat'))}
						</span>
					</g:if> <g:else>
						<i class="state stateDisconnected"></i>
						<span class="redText" title="${boxInstance.status.description}">
							${boxInstance.lastConnected(message(code:'default.text.messageFormat'))}
						</span>
					</g:else>
				</td>
				<td style="display:none;">
					${boxInstance.lastUpdated}				
				</td>
				<td>
			        <g:link controller="box" action="show" id="${boxInstance.id}"
			           class="action-icons view"
					   title="${message(code:'default.action.show')}">
					</g:link>
					<sec:ifNotGranted roles="ROLE_ADMIN">
						<g:link controller="box" action="edit" id="${boxInstance.id}"
					       class="action-icons edit"
						   title="${message(code:'default.action.edit')}">
						</g:link>
				    </sec:ifNotGranted>
				    <g:if test="${boxInstance.connected() == true}">
						<sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_HELP_DESK">
							<g:link controller="box" action="listLogFiles"
							   id="${boxInstance.id}" class="action-icons listInformation"
							   title="${message(code:'default.action.show.logFile')}">
							</g:link>
						</sec:ifAnyGranted>

						<g:link controller="box" controller="boxSoftware"
							action="updateSoftwareVersion" id="${boxInstance.id}"
							class="action-icons softwareUpdate"
							title="${message(code:'default.box.updateSoftwareVersion')}"></g:link>
							
						<g:link controller="box" action="remoteReset"
                            id="${boxInstance.id}" class="action-icons reset"
                            title="${message(code:'default.remreset.button')}"></g:link>

					</g:if>
					<sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_HELP_DESK">
						<g:link controller="box" action="retireBox" id="${boxInstance.id}"
							class="action-icons delete"
							title="${message(code:'default.box.retire')}"></g:link>
					</sec:ifAnyGranted></td>
			</tr>
		</g:each>
	</tbody>
</table>