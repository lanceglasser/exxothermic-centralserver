<g:render template="/layouts/messagesAndErrorsTemplate" model="[entity: box]"/>

<p class="muted"> <g:message code="box.block.note" /> </p>

<g:form action="doBlock" name="blockForm" id="blockForm" class="exxo-form">
	<!-- Left Column -->
	<div class="col-sm-8 col-md-8 resize-formleft">
		
		<div class="alert alert-error" id="divError" style="display: none;">
			<a class="close" data-dismiss="alert">×</a>
			<span id="errorMsg"></span>
		</div>
	
		<!-- Serial Number  -->
		<div class="col-xs-5 text-to-right">
			<label for="box">
				${ message(code:'default.field.serial') } *
			</label>
		</div>
		<div class="col-xs-7 text-to-left">
			<select id="box" name="box">
				<g:each in="${boxes}" status="i" var="box">
				<option value="${ box.id }" >${ box.serial + ' - ' + box.name }</option>
				</g:each>
			</select>
		</div>
		<span class='clearfix'></span>
		
		<!-- Reason : Optional  -->				
		<custom:editField type="text" name="reason" leftColumnSize="5" required="${false}" value="" max="150"
			label="${ message(code: 'reason') + " " + message(code: 'default.field.optionaL') }" 
			note="${ message(code: 'block.reason.note') }"/>
	</div>
	<span class="clearfix"></span>

	<!-- Buttons -->
	
	<div class="col-xs-10 col-sm-6 col-md-8 col-lg-8">
		<g:submitButton class="form-btn btn-primary" name="doBlock" value="${message(code:'btn.block')}" />			
		<g:link action="index" controller="home" class="back-button"  >        
	       <span class="form-btn btn-primary">
	          ${message(code:'default.field.buttonCancel')}
	       </span>       
    	</g:link>
	</div>	
</g:form>