<table id="exxtractorsTable" class="display" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>
				${message(code: 'default.field.serial')} / ${message(code: 'default.field.location')}
			</th>
			<th>
				${message(code: 'default.field.name')}
			</th>
			<th>
				${message(code: 'default.field.pa')}
			</th>
			<th>
				${message(code: 'default.field.version')} / ${message(code: 'Model')}
			</th>
			<th>
				${message(code: 'default.field.updateDate')}
			</th>
			<th>
				${message(code: 'default.table.action')}
			</th>
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>