<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.registerBox" /></title>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_box_register.js')}"></script>
<script>
	Exxo.module = "box-register";
</script>
</head>
<body>
	<section class="introp">
		<div class="intro-body">
			<div class="container full-width">
				<div class="row">
					<div class="col-md-12" id="internal-content">
						<h1 id="responsive-header">
							<g:message code="default.title.registerBox" />
						</h1>
						<div class="content">
							<g:render template="registerTemplate"></g:render>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</body>
</html>