<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main" />
		<title><g:message code="default.title.listLocation" /></title>
		<script>
			Exxo.module = "index";
		</script>
	</head>
	<body>
		<section class="introp">
			<div class="intro-body">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2" id="home-salute">
							<div id="child-salute">
								<div class="message">
									<sec:ifNotGranted  roles="ROLE_ADMIN">
										<p><g:message code="default.box.dont.exists"/></p>
										<g:link controller="box" action="register">
												<g:message code="default.box.register"/>
										</g:link>
									</sec:ifNotGranted>
											
									<sec:ifAnyGranted roles="ROLE_ADMIN">
										<p><g:message code="default.box.dont.exists"/> </p>		
									</sec:ifAnyGranted>
								</div>
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-12" id="internal-content">
							<!-- Information with the status of every unregister process -->
							<g:render template="/box/unregisterResultTemplate" model="[unregisterResult: unregisterResult]" />
						</div>
					</div>
				</div>
			</div>
		</section>
	</body>
</html>
