<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.box.unregister" /></title>
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
<script
	src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
<script
	src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_box_unregister.js')}"></script>
<script>
	Exxo.module = "box-unregister";
	var boxes = new Array();
	<g:each in="${boxes}" status="i" var="box">
		var box = {
				id:${box.id}, 
				name:"${box.name.encodeAsHTML()}", 
				serial:"${box.serial.encodeAsHTML()}",
				model: "${box.getBoxVersion(message(code: 'default.field.empty')) } / ${(box.model && box.model != '')? box.model:'-'}",
				venueid:${box.location.id},
				venue:"${box.location.name.encodeAsHTML()}",
				statusDescription:"${box.status.description}",
				connected: ${box.connected()},
				lastSeen: "${box.lastConnected(message(code:'default.text.messageFormat'))}"			
				};
		boxes.push(box);
	</g:each>
	Exxo.UI.vars["all-boxes"] = boxes;
	Exxo.UI.vars["current-boxes"] = boxes;
</script>
</head>
<body>
	<section class="introp">
		<div class="intro-body">
			<div class="container full-width">
				<div class="row">
					<div class="col-md-12" id="internal-content">
						<h1>
							<g:message code="default.box.unregister" />
						</h1>
						<g:render template="/layouts/messagesAndErrorsTemplate"/>
									
						<div class="col-xs-12 resize-formleft exxo-form form-horizontal">
							<div class="col-md-6">
								<div class="col-md-6 resize-formright">
									<label class="control-label" for="boxes">
										<g:message code="default.field.location" />
									</label>
								</div>
								<div class="col-md-6 resize-formleft">
									<select id="location" name="location">
										<option value="0" selected="selected">${ message(code:'default.field.all') }</option>
										<g:each in="${locations}" status="i" var="location">
										<option value="${ location.id }" ${ location.id == locationId? 'selected="selected"':'' }>${ location.encodeAsHTML() }</option>
										</g:each>
									</select>
								</div>
								<span class="clearfix"></span>
							</div>
							<div class="col-md-6 resize-formleft" style="text-align: left;">
								<label class="control-label" for="boxes" style="width: 70px; display: inline-block; margin-right: 10px;">
									<g:message code="default.field.text" />
								</label>
								<input type="text" id="textToSearch" name="textToSearch" style="display: inline-block; width: 150px;">
							</div>
							<span class="clearfix"></span>
						</div>
						<span class="clearfix"></span>
						
						<div class="col-xs-12 resize-formleft exxo-form form-horizontal">
							<div class="col-xs-3">
							</div>
							<div class="col-xs-9">
								<p class="muted">${ message(code:'unregister.servers.filter.note') }</p>
							</div>
							<span class="clearfix"></span>
						</div>
						<span class="clearfix"></span>
							
						<g:form action="applyUnregister" name="unregisterForm" class="form-horizontal">
						<div class="content exxo-form">
							<div class="col-xs-12 resize-formleft">
								<div class="col-xs-3">
									<label class="control-label" for="boxes">
										<g:message code="default.title.Box" /> *
									</label>
								</div>
								<div class="col-xs-9">
									<div class="exxo-table-container" style="width: 100%;">
										<table id="boxes-table" class='table table-striped exxo-table'>
											<thead>
												<tr>
													<th></th>
													<th>${message(code: 'default.field.serial')}</th>
													<th>${message(code: 'default.field.name')}</th>
													<th>${message(code: 'default.field.version')} / ${message(code: 'Model')}</th>
													<th>${message(code: 'default.field.location')}</th>
													<th>${message(code: 'default.field.updateDate')}</th>														
												</tr>
											</thead>
											<tbody>
												<g:each in="${boxes}" status="i" var="box">
													<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
														<td>
														<g:checkBox checked="false" name="boxes" value="${box.id}" />
													   </td>
													   <td> ${box.serial.encodeAsHTML()}</td>
													   <td> ${box.name.encodeAsHTML()}</td>
													   <td> ${box.getBoxVersion(message(code: 'default.field.empty')) } / ${(box.model && box.model != '')? box.model:'-'} </td>
													   <td> ${box.location.name.encodeAsHTML()}</td>
													   <td>
													   <g:if test="${box.connected() == true}">
															<i class="state stateConnected"></i>
															<span class="greenText" title="${box.status.description}">
																${box.lastConnected(message(code:'default.text.messageFormat'))}
															</span>
														</g:if> <g:else>
															<i class="state stateDisconnected"></i>
															<span class="redText" title="${box.status.description}">
																${box.lastConnected(message(code:'default.text.messageFormat'))}
															</span>
														</g:else>
													</td>
													</tr>
												</g:each>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<span class="clearfix"></span>
							
							<div class="col-xs-12 resize-formleft">
								<div class="col-xs-3">
								</div>
								<div class="col-xs-9 text-to-left">
								    
								    <g:submitButton class="form-btn btn-primary" name="applyUnregister" value="${message(code:'default.field.buttonUnregister')}" />
						
									<g:link action="index" controller="home" class="back-button"  >        
								       <span class="form-btn btn-primary">
								          ${message(code:'default.field.buttonCancel')}
								       </span>       
							    	</g:link>
								</div>
							</div>
							<span class="clearfix"></span>
						</div>
						</g:form>
						
						<!-- Information with the status of every unregister process -->
						<g:render template="/box/unregisterResultTemplate" model="[unregisterResult: unregisterResult]" />
						
					</div>
				</div>
			</div>
		</div>
	</section>

</body>
</html>