<g:render template="/layouts/messagesAndErrorsTemplate" model="[entity: box]"/>

<p class="muted"> <g:message code="default.box.register.note" /> </p>

<g:form action="${comingFromWizard == false ? 'doregister' : 'pages'}" name="wizardForm" id="wizardForm" class="exxo-form">  
	<!-- Left Column -->
	<div class="col-sm-8 col-md-8 resize-formleft">
		
		<!-- Serial Number  -->				
		<custom:editField type="text" name="serial" leftColumnSize="5" required="${true}" value="${box.serial}" max="50"
			label="default.field.serial" autocomplete="off" note="${ message(code: 'box.serial.help') }" cssclass="uppercase"/>
				
		<div class="col-xs-12 col-sm-6 col-md-5 text-right-sm text-right-md">
			<label  for="location">
				<g:message code="default.field.location" />*
			</label>
		</div>	
		<div class="col-xs-12 col-sm-6 col-md-7 text-to-left">
			<custom:selectWithOptGroupCompany id="location" from="${company}" value="${(locationId)?locationId:box.location?.id}"   />
		</div>
		<span class="clearfix"></span>
		
		<!-- Name : Optional  -->				
		<custom:editField type="text" name="name" leftColumnSize="5" required="${false}" value="${box.name}" max="50"
			label="${ message(code: 'default.field.name') + " " + message(code: 'default.field.optionaL') }" 
			note="${ message(code: 'default.box.name.note') }"/>
						
	</div>
	
	<!-- Right Column -->
	<div class="col-xs-6 resize-formright">
		
	</div>
	<span class="clearfix"></span>

	<!-- Buttons -->
	
	<div class="col-xs-10 col-sm-6 col-md-8 col-lg-8">
		<g:if test="${comingFromWizard == false}">
			<g:submitButton class="form-btn btn-primary" name="register" value="${message(code:'default.field.buttonRegister')}" />
			
			<g:link action="index" controller="home" class="back-button"  >        
		       <span class="form-btn btn-primary">
		          ${message(code:'default.field.buttonCancel')}
		       </span>       
	    	</g:link>	
	    </g:if>
	    <g:else>		    	 
	    	<g:submitButton name="continue" value="${message(code:'default.field.buttonRegister')}"  class="form-btn btn-primary" /> 
	    	<af:ajaxButton name="cancel" value="${message(code:'default.field.buttonCancel')}" afterSuccess="onPage();" class="form-btn btn-primary" />	   
		</g:else>
	</div>	
</g:form>