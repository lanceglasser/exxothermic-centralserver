<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main" />
		<title><g:message code="default.title.Box" /></title>
		<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
		<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
		<script src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
		<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_box_list.js')}"></script>
        <script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
		<script>
			Exxo.module = "box-list-server";
			Exxo.UI.vars["page-code"] = "exxtractors-list";
            Exxo.Tables.loadPaginationInitValuesFromString('<custom:paginationInfo code="exxtractors-list"/>');
			var serialToFilter = "${ serialToFilter? serialToFilter:'' }";
			Exxo.UI.vars["tableFilter"] = (serialToFilter == "")? Exxo.UI.vars["tableFilter"]:serialToFilter;
			Exxo.UI.vars["is-admin"] = false;
			<sec:ifAnyGranted roles="ROLE_ADMIN">
			Exxo.UI.vars["is-admin"] = true;
            </sec:ifAnyGranted>
            Exxo.UI.vars["is-admin-or-helpdesk"] = false;
            <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_HELP_DESK">
            Exxo.UI.vars["is-admin-or-helpdesk"] = true;
            </sec:ifAnyGranted>
			Exxo.UI.translates['okButton'] = "${message(code:'default.field.buttonOk')}";
			Exxo.UI.translates['cancelButton'] = "${message(code:'default.field.buttonCancel')}";
			Exxo.UI.translates['disableMessage'] = "${message(code:'mycentralserver.box.resetBox')}";
			Exxo.UI.translates['registerBox'] = "${message(code:'default.box.register')}";
            Exxo.UI.translates['emptyText'] = "${message(code:'default.field.empty')}";
			
			Exxo.UI.urls["exxtractors-edit-url"] = "${g.createLink(controller: 'box', action: 'edit', absolute: true)}/";
			Exxo.UI.urls['registerBoxURL'] = '${createLink(action: 'register')}';
			Exxo.UI.urls["exxtractors-get-list-url"] = "${g.createLink(controller: 'widget', action: 'getListDataAjax', absolute: true)}";
			Exxo.UI.urls["widget-get-data-url"] = "${g.createLink(controller: 'widget', action: 'getListDataAjax', absolute: true)}";

            // Base action links
            Exxo.UI.vars["action-show-link"] = '<g:link controller="box" action="show" id="BOX_ID"
                       class="action-icons view" title="${message(code:'default.action.show')}"> </g:link>';
            Exxo.UI.vars["action-edit-link"] = '<g:link controller="box" action="edit" id="BOX_ID"
                class="action-icons edit" title="${message(code:'default.action.edit')}"></g:link>';
            Exxo.UI.vars["action-logs-link"] = '<g:link controller="box" action="listLogFiles"
                id="BOX_ID" class="action-icons listInformation" title="${message(code:'default.action.show.logFile')}"></g:link>';
            Exxo.UI.vars["action-software-link"] = '<g:link controller="box" controller="boxSoftware" action="updateSoftwareVersion" 
                id="BOX_ID" class="action-icons softwareUpdate" title="${message(code:'default.box.updateSoftwareVersion')}"></g:link>';
            Exxo.UI.vars["action-reset-link"] = '<g:link controller="box" action="remoteReset"
                id="BOX_ID" class="action-icons reset" title="${message(code:'default.remreset.button')}"></g:link>';
            Exxo.UI.vars["action-retire-link"] = '<g:link controller="box" action="retireBox" id="BOX_ID"
                class="action-icons delete" title="${message(code:'default.box.retire')}"></g:link>';
		</script>
		</head>
	<body>
	<section class="introp">
	  <div class="intro-body">
	    <div class="container full-width">
	      <div class="row">
	        <div class="col-md-12" id="internal-content">
	          <h1><g:message code="default.title.Box" /></h1>
	          <g:render template="/layouts/messagesAndErrorsTemplate"/>
	          
			<div class="content">
            	<g:render template="/box/boxesTableFromServerTemplate" model="[boxes: boxes]"/>
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	</section>	
	<div id="confirm-retire-dialog" title="${message(code:'mycentralserver.box.retireBox.title')}" style="display: none;">
		${message(code:'mycentralserver.box.retireBox')}
	</div>
		
	<div id="confirm-reset-dialog" title="${message(code:'mycentralserver.box.resetBox.title')}" style="display: none;">
		${message(code:'mycentralserver.box.resetBox')}
	</div>

	<div id="confirm-factory-reset-dialog" title="${message(code:'default.confirm.title')}" style="display: none;">
		${message(code:'default.reset.warning')}
	</div>
	</body>
</html>