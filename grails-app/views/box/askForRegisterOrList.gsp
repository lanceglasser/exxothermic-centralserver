<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main" />
		<title><g:message code="default.title.listLocation" /></title>
		<script>
			Exxo.module = "index";
		</script>
	</head>
	<body>
		<section class="introp">
			<div class="intro-body">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2" id="home-salute">
							<div id="child-salute">
								<div class="message">
									<g:if test="${flash.error}">
						        		<div class="alert alert-error">  
										  <a class="close" data-dismiss="alert">×</a>  
										  ${flash.error} 
										  <g:hasErrors bean="${box}">
										  		<ul>
													<g:eachError bean="${box}" var="error">
														<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
													</g:eachError>
												</ul>
										  </g:hasErrors>
										</div> 
										${flash.error=null} 
						      		</g:if>
						      		 <g:if test="${flash.success}">
						        		<div class="alert alert-success">  
										  <a class="close" data-dismiss="alert">×</a>  
										  ${flash.success} 
										</div> 
										${flash.success=null} 
						      		</g:if>
						      		
		      						<p><g:message code="default.box.add.more" /></p>
		      						
									<sec:ifNotGranted  roles="ROLE_ADMIN"> 
										<g:link controller="box" action="register" params="${["location":location]}"> 
											<input type="button" class="form-btn btn-primary" value="${message(code:'default.field.buttonAddOther')}"/> 
										</g:link>
									</sec:ifNotGranted>
									
									<g:link controller="box" action="listAll"> <input type="button" class="form-btn btn-primary" value="${message(code:'default.field.buttonDone')}"/> </g:link>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</body>
</html>
