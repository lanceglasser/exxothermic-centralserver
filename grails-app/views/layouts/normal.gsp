<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<title><g:layoutTitle default="ExXothermic Cloud Service"/></title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		
		
		
		<link rel="shortcut icon" href="${resource(dir: 'themes/default/img', file: 'favicon.ico')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.css')}" type="text/css">		
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'style.css')}" type="text/css">		
		<link rel="stylesheet" href="${resource(dir: 'themes/' + session.theme + '/css', file: 'style.css')}" type="text/css">
		<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
		<script src="${resource(dir: 'js', file: 'bootstrap.js')}"></script>
		<g:layoutHead/>
		<r:layoutResources />
	</head>
	<body>
	
	     <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">

                    <a class="brand" href="#"><g:message code="default.service.title"/></a>
						 
                        <p class="navbar-text pull-right">
	                        <g:if test="${params.action =='register' ||  params.action =='registerAccount'}"> 
	                        	 <g:message code="default.home.AlreadyhaveAnAccount"/> -  <g:link controller="login" action="auth"  class="navbar-link"><g:message code="default.home.signIn"/></g:link>
	                        </g:if>
	                        <g:if test="${params.action=='auth'}">
	                        	<g:message code="default.home.IWantTo"/> <g:link controller="home" action="register"  class="navbar-link"><g:message code="default.home.Register"/></g:link>
	                        </g:if>
                        </p>
                        
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
       
	
        <div class="container-fluid">
            <div class="row rower-over">
                <div class="span3">
                    <div class="well sidebar-nav">
                        <ul class="nav nav-list">
 	
                            <li>
                                <a href="${message(code:'whoWeAre.link.url')}"> <g:message code="default.home.whoWeAre"/></a>
                            </li>                            
                            <li>
                                <a href="${message(code:'buyAMyBox.link.url')}"> <g:message code="default.home.buyAMyBox"/></a>
                            </li>                            
                            <li>
                                <a href="${message(code:'downloadMyApp.link.url')}" TARGET="_blank"><g:message code="default.home.downloadMyApp"/></a>
                            </li>                            
                            <li>
                                <a href="${message(code:'faq.link.url')}"><g:message code="default.home.faq"/></a>
                            </li>
                            <li>
                                <a href="${message(code:'support.link.url')}"><g:message code="default.home.support"/></a>
                            </li>                            
                            </br>                            
                        </ul>
                    </div>
                    <!--/.well -->
                </div>
                <!--/span-->
                <div class="span9">
           				<g:layoutBody/>                                                
                  </div>
            </div>
            <!--/row-->
            <hr>
             <footer >
            	<div>
            		 <a href="http://www.exxothermic.com/privacy.html"><g:message code="default.home.privacy"/></a>             		 
            		 | <a href="http://www.exxothermic.com/terms.html"><g:message code="default.home.terms"/></a>
            		 <p> <g:message code="default.application.copyright" args='["2018"]' /> | <g:message code="default.application.version"/> ${grailsApplication.metadata['app.version']} </p>
            	</div>                
            </footer>
        </div>
		

		
		<g:javascript library="application"/>
		<r:layoutResources />
	</body>
</html>