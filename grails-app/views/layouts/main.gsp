<!DOCTYPE html>
<%@page import="grails.plugins.springsecurity.SpringSecurityService"%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="${resource(dir: 'themes/' + session.theme + '/img', file: 'favicon.ico')}" type="image/x-icon">
<link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
<link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">
<title>${ message(code:'web.site.title') }</title>
<script src="${resource(dir: 'themes/default/js', file: 'jquery.min.js')}"></script>
<link rel="stylesheet" href="${resource(dir: 'css/ui-lightness', file: 'jquery-ui-1.10.4.custom.css')}" type="text/css">
<!-- Bootstrap Core CSS -->
<link rel="stylesheet" href="${resource(dir: 'themes/default/css', file: 'bootstrap.min.css')}" type="text/css">
<!-- Fonts -->
<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'showhide.css')}">
<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href='http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

<!-- Custom Style for Themes -->
<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'style.css')}"/>
<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/' + session.theme + '/css', file: 'theme.css')}"/>

<link rel="stylesheet" type="text/css"
			href="${resource(dir: 'themes/default/css', file: 'helpCenter.css')}">
			
<!-- Custom Theme CSS -->
<link href="${resource(dir: 'themes/default/css', file: 'grayscale.css')}" rel="stylesheet">

<!-- Slider CSS -->
<link rel="stylesheet" href="${resource(dir: 'themes/default/css', file: 'superslides.css')}">
<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'component.css')}" />
<script src="${resource(dir: 'themes/default/js', file: 'modernizr.custom.js')}"></script>
<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'wizzard.css')}" />
<script src="${resource(dir: 'js', file: 'jquery-ui-1.10.4.custom.min.js')}"></script>
<script src="${resource(dir: 'js', file: 'jquery.tooltipster.min.js')}"></script>

<!-- Bootbox Messages -->
<link href="${resource(dir: 'themes/default/css', file: 'messi.css')}" rel="stylesheet">
<script src="${resource(dir: 'themes/default/js', file: 'messi.js')}"></script>

<script src="${resource(dir: 'themes/default/js', file: 'application.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo/', file: 'exxo_main.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo/', file: 'exxo_constants.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo/', file: 'exxo_dashboard.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo/', file: 'exxo_help_center.js')}"></script>
<script>

	Exxo.UI.translates["lengthMenu"]	= "${message(code:'default.table.lengthMenu')}";
	Exxo.UI.translates["zeroRecords"]	= "${message(code:'default.table.zeroRecords')}";
	Exxo.UI.translates["info"]			= "${message(code:'default.table.info')}";
	Exxo.UI.translates["infoEmpty"]		= "${message(code:'default.table.infoEmpty')}";
	Exxo.UI.translates["infoFiltered"]	= "${message(code:'default.table.infoFiltered')}";
	Exxo.UI.translates["sSearch"]		= "${message(code:'default.table.sSearch')}";

	Exxo.UI.translates['cancelButton'] = "${ message(code:'default.field.buttonCancel') }";
	Exxo.UI.translates['uploadButton'] = "${ message(code:'default.button.upload') }";
	Exxo.UI.translates['closeButton'] = "${ message(code:'default.field.buttonClose') }";
	Exxo.UI.translates['saveButton'] = "${ message(code:'default.field.buttonSave') }";
	//alert(Exxo.UI.translates.lengthMenu);

	/*
	default.table.lengthMenu=Display _MENU_ records per page
	default.table.zeroRecords=No records found
	default.table.info=Showing page _PAGE_ of _PAGES_
	default.table.infoEmpty=No records available
	default.table.infoFiltered=(filtered from _MAX_ total records)
	default.table.sSearch=Search
	*/
</script>

<g:layoutHead/>
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">
<div class="container-menu">
  <ul id="gn-menu" class="gn-menu-main">
    <li class="gn-trigger float-left"> <a class="gn-icon gn-icon-menu"><span>Menu</span></a>
      <nav class="gn-menu-wrapper gn-open-part">
        <div class="gn-scroller">
          <ul class="goo-collapsible">
          	<!-- General Configurations Start: Only for Admin/Help Desk -->
			<sec:ifAnyGranted roles="ROLE_ADMIN, ROLE_HELP_DESK">
				<li class="dropdown">
					<a class="mGeneral" href="#"><g:message code="menu.general.options" /></a>
					<ul>
						<li class=" ${params.controller=='affiliate' && params.action=='establishments ' ?'active':''} list">
							<g:link class="gn-icon comm-list" controller="affiliate" action="establishments">
								<g:message code="menu.affiliates.establishment" />
							</g:link>
						</li>
						<sec:ifAnyGranted roles="ROLE_ADMIN">
						<li class=" ${params.controller=='configuration' && params.action=='index ' ?'active':''} list">
							<g:link class="gn-icon comm-list" controller="configuration" action="">
								<g:message code="configurations" />
							</g:link>
						</li>
						</sec:ifAnyGranted>
					</ul>
				</li>
				<sec:ifAnyGranted roles="ROLE_ADMIN">
				<li class="dropdown">
					<a class="mDevices" href="#"><g:message code="devices" /></a>
					<ul>
						<li class=" ${params.controller=='device' && params.action=='index ' ?'active':''} list">
							<g:link class="gn-icon comm-list" controller="device" action="">
								<g:message code="devices.list.title" />
							</g:link>
						</li>
						<li class=" ${params.controller=='device' && params.action=='create' ?'active':''} list">
							<g:link class="gn-icon comm-list" controller="device" action="create">
								<g:message code="menu.device.create" />
							</g:link>
						</li>
					</ul>
				</li>
				</sec:ifAnyGranted>

				<li class="dropdown">					
					<a class="mPartner" href="#"><g:message code="menu.affiliates.manage" /></a>
					<ul>
						<li class=" ${params.controller=='affiliate' && params.action=='stores' ?'active':''} list">
							<g:link class="gn-icon comm-list" controller="affiliate" action="listAll">
								<g:message code="menu.affiliates.stores" />
							</g:link>
						</li>
						<li class=" ${params.controller=='affiliate' && params.action=='stores' ?'active':''} list">
							<g:link class="gn-icon comm-list" controller="affiliate" action="create">
								<g:message code="menu.affiliates.create" />
							</g:link>
						</li>						
					</ul>
				</li>
				
			</sec:ifAnyGranted>
			<!-- General Configurations End: Only for Admin/Help Desk -->
			
			<!-- Wizard remove for now 
          	<sec:ifAnyGranted roles="ROLE_INTEGRATOR">
				<li class="titles">
					<div class="menuHeaderIcon">
						<div class="menuHeaderToolTip" title="${ message(code:'menu.header.help.wizard') }"></div>
					</div>
					<g:message code="default.home.wizard" />
				</li>
				<li class=" ${params.controller=='wizard' && params.action=='index' ?'active':''} ">
					<g:link controller="wizard" action="index" class="gn-icon wizard">
						<g:message code="default.wizard.start" />
					</g:link>
				</li>
			</sec:ifAnyGranted>
			 -->
			 
			<sec:ifAnyGranted roles="ROLE_ADMIN">
				<li class="dropdown">
					<a class="mUsers" href="#"><g:message code="default.home.manageUsers" /></a>
					<ul>
						<li class=" ${params.controller=='user' && params.action=='listAll' ?'active':''} ">
							<g:link class="gn-icon admin" controller="user" action="listAll">
								<g:message code="default.user.list" />
							</g:link>
						</li>
						<li class=" ${params.controller=='user' && params.action=='create' ?'active':''}">
							<g:link class="gn-icon create-ad" controller="user" action="create">
								<g:message code="default.users.new" />									
							</g:link>
						</li>
					</ul>
				</li>				
			</sec:ifAnyGranted>
			
			<sec:ifAnyGranted roles="ROLE_OWNER, ROLE_INTEGRATOR, ROLE_HELP_DESK">
				<li class="dropdown">
					<a class="mUsers" href="#"><g:message code="default.home.manageEmployees" /></a>
					<ul>
							<li class=" ${params.controller=='employee' && params.action=='listAll' ?'active':''} ">
								<g:link class="gn-icon admin" controller="employee" action="listAll">
									<g:message code="default.employees.list" />
								</g:link>
							</li>
							<li class=" ${params.controller=='employee' && params.action=='listAllNonAssign' ?'active':''} ">
								<g:link class="gn-icon admin" controller="employee"
									action="listAllNonAssign">
									<g:message code="default.employees.listnonAssign" />
								</g:link>
							</li>
							<li class=" ${params.controller=='employee' && params.action=='create' ?'active':''}">
								<g:link class="gn-icon create-ad" controller="employee" action="create">
									<g:message code="default.employees.new" />
								</g:link>
							</li>
							<li	class=" ${params.controller=='employee' && params.action=='assignCompany' ?'active':''} assign">
								<g:link class="gn-icon assign-com" controller="employee" action="assignCompany">
									<g:message code="default.title.assignCompany" />
								</g:link>
							</li>	
					</ul>
				</li>
						
			</sec:ifAnyGranted>
			
			<!-- 
			<sec:ifAnyGranted roles="ROLE_ADMIN">						
				<li class="titles"><g:message code="default.home.manageCertificates" /></li>
				<li class=" ${params.controller=='boxManufacturer' && params.action=='listAll' ?'active':'srgfsf'} ">
					<g:link class="gn-icon comm-list" controller="boxManufacturer" action="listAll">
						<g:message code="default.manufacturer.list" />								
					</g:link>
				</li>
			</sec:ifAnyGranted>
			 -->
			 
			<sec:ifAnyGranted roles="ROLE_OWNER, ROLE_INTEGRATOR, ROLE_HELP_DESK, ROLE_ADMIN">
				<li class="dropdown">					
					<a class="mCompany" href="#"><g:message code="default.home.manageCompanies" /></a>
					<ul>
						<li class=" ${params.controller=='company' && params.action=='index' ?'active':''} ">
							<g:link class="gn-icon comp-list" controller="company" action="index">
								<g:message code="default.companies.list" />
							</g:link>
						</li>
						<sec:ifAnyGranted roles="ROLE_INTEGRATOR, ROLE_HELP_DESK">
							<li class=" ${params.controller=='company' && params.action=='create' ?'active':''}">
								<g:link class="gn-icon create-company" controller="company" action="create">
									<g:message code="default.companies.new" />
								</g:link>
							</li>
						</sec:ifAnyGranted>
						<sec:ifAnyGranted roles="ROLE_HELP_DESK">							
								<li class=" ${params.controller=='company' && params.action=='assignIntegrator' ?'active':''}">
									<g:link class="gn-icon asign-inter" controller="company" action="assignIntegrator">
										<g:message code="default.companies.assign" />
									</g:link>
								</li>	
								<li class=" ${params.controller=='company' && params.action=='ownedCompanies' ?'active':''}">
									<g:link class="gn-icon asign-inter" controller="company" action="ownedCompanies">
										<g:message code="companies.assign.owner" />
									</g:link>
								</li>	
						</sec:ifAnyGranted>
					</ul>
				</li>
				
			</sec:ifAnyGranted>
			
			<sec:ifAnyGranted roles="ROLE_ADMIN, ROLE_HELP_DESK">
				<li class="dropdown">
					<a class="mCompany" href="#"><g:message code="default.home.manageIntegrators" /></a>
					<ul>
						<li class=" ${params.controller=='companyIntegrator' && params.action=='index' ?'active':''} ">
							<g:link class="gn-icon comp-list" controller="companyIntegrator" action="index">
								<g:message code="default.companies.listIntegrators" />
							</g:link>
						</li>
						<li class=" ${params.controller=='companyIntegrator' && params.action=='create' ?'active':''}">
							<g:link class="gn-icon create-company" controller="companyIntegrator" action="create">
								<g:message code="default.integrator.new" />
							</g:link>
						</li>	
					</ul>
				</li>
				
			</sec:ifAnyGranted>
			
			<li class="dropdown">
				<a class="mLocation" href="#"><g:message code="default.home.manageLocations" /></a>
				<ul>
					<li class=" ${params.controller=='location' && params.action=='listall' ?'active':''}">
						<g:link class="gn-icon location-list" controller="location" action="listAll">
							<g:message code="default.location.list" />
						</g:link>
					</li>
						
					<sec:ifAnyGranted roles="ROLE_INTEGRATOR, ROLE_HELP_DESK">
						<li class=" ${params.controller=='location' && params.action=='create' ?'active':''}">
							<g:link class="gn-icon cretate-loc" controller="Location" action="create">
								<g:message code="default.companies.newLocation" />
							</g:link>
						</li>
						<li class=" ${params.controller=='exxtractorsUser' && params.action=='create' ?'active':''}">
                            <g:link controller="exxtractorsUser" action="create" class="gn-icon cretate-loc" >
                                <g:message code="create.exxtractor.user" />
                            </g:link>
                        </li>
						
						<%--<li class=" ${params.controller=='location' && params.action=='assignSkin' ?'active':''}">
							<g:link  controller="Location" action="assignSkin" class="assignIntegrator">
								<g:message code="default.title.assignSkin" />
							</g:link>
						</li> --%>
					</sec:ifAnyGranted>
				</ul>
			</li>
			
			<!-- MY BOX  Management -->
			<li class="dropdown">
				<a class="mBox" href="#"><g:message code="default.home.manageMybox" /></a>
				<ul>
					<li class=" ${params.controller=='box' && params.action=='listAll' ?'active':''} list">
						<g:link class="gn-icon esxxt-list" controller="box" action="listAll">
							<g:message code="default.box.list" />
						</g:link>
					</li>
					
					<!-- Other ExXtractors Options -->					
					<sec:ifAnyGranted roles="ROLE_HELP_DESK,ROLE_INTEGRATOR,ROLE_INTEGRATOR_STAFF">
					<li class=" ${params.controller=='box' && params.action=='register' ?'active':''}">
						<g:link class="gn-icon reg-ext" controller="box" action="register">
							<g:message code="default.box.register" />
						</g:link>
					</li>
					</sec:ifAnyGranted>
					
					<sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_HELP_DESK">
						<li	class=" ${params.controller=='box' && params.action=='blackList' ?'active':''} list">
							<g:link class="gn-icon esxxt-list" controller="box" action="blackList">
								<g:message code="default.title.Box.blackList" />
							</g:link>
						</li>
						
						<li class=" ${params.controller=='boxUnregistered' && params.action=='index' ?'active':''}">
							<g:link class="gn-icon esxxt-list" controller="boxUnregistered" action="index">
								<g:message code="default.box.unregisterList" />
							</g:link>
						</li>	
					</sec:ifAnyGranted>
					 
					<sec:ifAnyGranted roles="ROLE_HELP_DESK,ROLE_INTEGRATOR,ROLE_INTEGRATOR_STAFF">
						<li class=" ${params.controller=='box' && params.action=='unregister' ?'active':''}">
							<g:link class="gn-icon unreg-ext" controller="box" action="unregister">
								<g:message code="default.box.unregister" />
							</g:link>
						</li>
						<li class=" ${params.controller=='boxMetrics' && params.action=='report' ?'active':''}">
							<g:link class="gn-icon unreg-ext" controller="boxMetrics" action="report">
								<g:message code="usage.report.title" />
							</g:link>
						</li>
						<li class=" ${params.controller=='boxMetrics' && params.action=='report' ?'active':''}">
							<g:link class="gn-icon unreg-ext" controller="boxMetrics" action="tables">
								<g:message code="usage.report.title.tables" />
							</g:link>
						</li>
					</sec:ifAnyGranted>
					
					<sec:ifAnyGranted roles="ROLE_ADMIN">
						<li class=" ${params.controller=='box' && params.action=='report' ?'active':''}">
							<g:link class="gn-icon unreg-ext" controller="box" action="block">
								<g:message code="box.move.black.list" />
							</g:link>
						</li>
					</sec:ifAnyGranted>
				</ul>
			</li>
			
			
			<!-- Box Software Management Options -->
			<sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_HELP_DESK,ROLE_INTEGRATOR,ROLE_INTEGRATOR_STAFF">
				<li class="dropdown">					
					<a class="mBoxSoftware" href="#">${ message(code:'software.version') }</a>
					<ul>
						<li class=" ${params.controller=='boxSoftware' && params.action=='listAll' ?'active':''} list">
							<g:link class="gn-icon soft-vers" controller="boxSoftware" action="listAll">
								<g:message code="default.box.listSoftwareVersion" />
							</g:link>
						</li>
						
						<sec:ifAnyGranted roles="ROLE_ADMIN">
							<li class=" ${params.controller=='boxSoftwareUpgradeJob' && params.action=='list' ?'active':''} create">
								<g:link class="gn-icon soft-vers" controller="boxSoftwareUpgradeJob" action="list">
									${ message(code:'generic.list', args:[message(code:'box.software.upgrade.job')])}
								</g:link>
							</li>
							<li class=" ${params.controller=='boxSoftwareUpgradeJob' && params.action=='list' ?'active':''} create">
								<g:link class="gn-icon soft-vers" controller="boxSoftwareUpgradeJob" action="create">
									${ message(code:'generic.create', args:[message(code:'box.software.upgrade.job')])}
								</g:link>
							</li>
							<li class=" ${params.controller=='boxSoftware' && params.action=='register' ?'active':''} create">
								<g:link class="gn-icon soft-vers" controller="boxSoftware" action="register">
									<g:message code="default.box.registerSoftwareVersion" />
								</g:link>
							</li>
						</sec:ifAnyGranted>
					</ul>
				</li>
			</sec:ifAnyGranted>
			 <!-- End of Box Software Management Options -->
			
			
			<!-- Session Crete Contents -->
			<sec:ifNotGranted roles="ROLE_ADMIN">
				<li class="dropdown">
					<a class="mContent" href="#"><g:message code="default.home.manageContent" /></a>
					<ul>
						<li class=" ${params.controller=='content' && params.action=='listAll' ?'active':''} list">
							<g:link class="gn-icon comm-list" controller="content" action="listAll">
								<g:message code="default.home.manageContent.listAll" />
							</g:link>
						</li>
						<li class=" ${params.controller=='content' && params.action=='create' ?'active':''} create">
							<g:link class="gn-icon create-cont" controller="content" action="create">
								<g:message code="default.home.createContent" />
							</g:link>
						</li>
						<li class=" ${params.controller=='content' && params.action=='assignLocation' ?'active':''} create">
							<g:link class="gn-icon manage-con" controller="content" action="assignLocation">
								${ message(code:'assign.general', args:[message(code:'default.home.banners')]) }
							</g:link>
						</li>	
					</ul>
				
				</li>
				
			</sec:ifNotGranted>
			
			<!-- Manage Offers -->
			<sec:ifNotGranted roles="ROLE_ADMIN">
				<li class="dropdown">
					<a class="mOffer" href="#"><g:message code="default.home.manageOffer" /></a>
					<ul>
						<li class=" ${params.controller=='offer' && params.action=='listAll' ?'active':''} list">
							<g:link class="gn-icon comm-list" controller="offer" action="listAll">
								<g:message code="default.home.manageOffers.listAll" />
							</g:link>
						</li>
						<li class=" ${params.controller=='offer' && params.action=='create' ?'active':''} create">
							<g:link class="gn-icon create-cont" controller="offer" action="create">
								<g:message code="default.home.createOffer" />
							</g:link>
						</li>
						<li class=" ${params.controller=='offer' && params.action=='assignLocation' ?'active':''} create">
							<g:link class="gn-icon manage-con" controller="offer" action="assignLocation">
								${ message(code:'assign.general', args:[message(code:'default.home.offers')]) }
							</g:link>
						</li>	
					</ul>
				</li>
				
			</sec:ifNotGranted>
			
			<!-- Documents Options Start: Only for Admin -->
			<sec:ifNotGranted roles="ROLE_ADMIN">
				<li class="dropdown">
					<a class="mDocument" href="#"><g:message code="menu.documents.manage" /></a>
					<ul>
						<li class=" ${params.controller=='document' && params.action=='listAll' ?'active':''} list">
							<g:link class="gn-icon comm-list" controller="document" action="listAll">
								<g:message code="menu.documents.list" />
							</g:link>
						</li>
						<li class=" ${params.controller=='document' && params.action=='create' ?'active':''} create">
							<g:link class="gn-icon create-cont" controller="document" action="create">
								<g:message code="menu.documents.create" />
							</g:link>
						</li>
						
						<li class=" ${params.controller=='document' && params.action=='assignLocation' ?'active':''} create">
							<g:link class="gn-icon manage-con" controller="document" action="assignLocation">
								${ message(code:'assign.general', args:[message(code:'default.home.documents')]) }
							</g:link>
						</li>	
					</ul>
				</li>
				
			</sec:ifNotGranted>
			<!-- Documents Options End -->
			
			<!-- Docs Categories Options Start: Only for Admin -->
			<sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_HELP_DESK,ROLE_INTEGRATOR">
				<li class="dropdown">
					<a class="mDocCategory" href="#"><g:message code="menu.doc.categories.manage" /></a>
					<ul>
						<li class=" ${params.controller=='docCategory' && params.action=='listAll' ?'active':''} list">
							<g:link class="gn-icon comm-list" controller="docCategory" action="listAll">
								<g:message code="menu.doc.categories.list" />
							</g:link>
						</li>
						<li class=" ${params.controller=='docCategory' && params.action=='create' ?'active':''} create">
							<g:link class="gn-icon create-cont" controller="docCategory" action="create">
								<g:message code="menu.doc.categories.create" />
							</g:link>
						</li>	
					</ul>
				</li>
				
			</sec:ifAnyGranted>
			<!-- Docs Categories Options End -->
			
			<!-- Skins Management Options Start -->
				<li class="dropdown">
						<a class="mAppSkin" href="#"><g:message code="default.home.manageSkin" /></a>
						<ul>
							<li class=" ${params.controller=='appSkin' && params.action=='listAll' ?'active':''} list">
								<g:link class="gn-icon comm-list" controller="appSkin" action="listAll">
									<g:message code="default.skin.list" />
								</g:link>
							</li>
							<li class=" ${params.controller=='appSkin' && params.action=='create' ?'active':''} create">
								<g:link class="gn-icon create-cont" controller="appSkin" action="create">
									<g:message code="default.title.newAppSkin" />
								</g:link>
							</li>
							<sec:ifNotGranted roles="ROLE_ADMIN">
							<li class=" ${params.controller=='appSkin' && params.action=='assignLocation' ?'active':''} create">
								<g:link class="gn-icon manage-con" controller="appSkin" action="assignLocation">
									<g:message code="default.title.assignSkin" />
								</g:link>
							</li>
							</sec:ifNotGranted>		
						</ul>
				</li>
				
			<!-- Skins Management Options End -->
			
			<!-- Welcome Ads Management Options Start -->
			<li class="dropdown">
						<a class="mWelcomeAd" href="#"><g:message code="welcome.ad.menu.manage" /></a>
						<ul>							
							<li class=" ${params.controller=='welcomeAd' && params.action=='listAll' ?'active':''} list">
								<g:link class="gn-icon comm-list" controller="welcomeAd" action="listAll">
									<g:message code="welcome.ad.menu.list" />
								</g:link>
							</li>
							<li class=" ${params.controller=='welcomeAd' && params.action=='create' ?'active':''} create">
								<g:link class="gn-icon create-cont" controller="welcomeAd" action="create">
									<g:message code="welcome.ad.menu.create" />
								</g:link>
							</li>
							<sec:ifNotGranted roles="ROLE_ADMIN">
							<li class=" ${params.controller=='welcomeAd' && params.action=='assignLocation' ?'active':''} create">
								<g:link class="gn-icon manage-con" controller="welcomeAd" action="assignLocation">
									<g:message code="welcome.ad.menu.assign" />
								</g:link>
							</li>
							</sec:ifNotGranted>		
						</ul>
			</li>
			<!-- Welcome Ads Management Options End -->
			
			<li class="dropdown">
				<a class="mLogout" href="#"><g:message code="default.home.logout" /></a>
				<ul>
					<li class="open-close-menu">
						<g:link class="gn-icon asign-inter" controller="logout" action="index">
							<g:message code="default.home.logout" />
						</g:link>
					</li>	
				</ul>
			</li>
			
          </ul>
        </div>
        <!-- /gn-scroller --> 
      </nav>
    </li>
    <li class="logo_infoapp">${ message(code:'top.bar.company.title') }</li>    
    <li class="icono-onoff" id="btn-logout">Logout</li>
    <li class="icon-home" id="btn-homepage">${ message(code:'home') }</li>
    <li class="user-data">
		<g:message code="default.home.LoggedInAs" /> 
		<span class="under" id="btn-my-account"><user:getCurrentUserName /></span>
		&nbsp;&nbsp;&nbsp;|
    </li>
  </ul>
</div>
<!-- /container -->


<g:layoutBody/>

<!-- Ajax Flow Container -->
<div id="ajaxFlowWait" class="ajaxFlow" style="display:none;">
	<span class="waitBackground"></span>
	<span class="waiter">
		<span class="wait">
			<span class="title">${ message(code:'please.wait') }</span>
			<span class="spinner"></span>
		</span>
	</span>
</div>

<g:if test="${ session.maintenanceMsg && session.maintenanceMsg != "" }">
	<div class="warning-site-message-on-main">
		<div class="alert alert-maintance">
			<a class="close" data-dismiss="alert">×</a>
			${ session.maintenanceMsg }
		</div>
	</div>
</g:if>

<footer id="azul"> 
	<a href="${ message(code:'privacy.policy.url') }" target="_blank"><g:message code="home.privacy" /></a> | 
	<a href="${ message(code:'terms.and.conditions.url') }" target="_blank"><g:message code="home.terms" /></a><br>
	<g:message code="application.copyright" args='["2018"]' /> | <g:message code="application.version" /> ${grailsApplication.metadata['app.version']}
</footer>

<!-- Div for the Help Center Window -->
<div id="helpDialogPlaceHolder" style="display: none;" title="${ message(code:'help.center') }"></div>

<!-- Div for the Add Widget Window -->
<div id="addWidgetDialogPlaceHolder" style="display: none;" title="${ message(code:'add.widget') }">
<!-- 
<div class="widget-row">
	<div class="texts">
		<div class="title">Locations List</div>
		<div class="description">This widget includes a table with the list of Locations related to the current dashboard</div>
	</div>
	
	<div class="btn-div"> 
		<a href="#">        
		   <span class="form-btn btn-primary" style="width: 150px;">
		      Agregar
		   </span>       
		</a>
	</div>
	<span class="clearfix"></span>
</div>

-->

</div>

<!-- Dialog Alerts -->
<div id="dialog-alerts-info" title="Info">
</div>

<!-- Core JavaScript Files 
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>  --> 
<!-- FullWidth Slider JavaScript -->
<script src="${resource(dir: 'themes/default/js', file: 'jquery.easing.1.3.js')}"></script>
<script src="${resource(dir: 'themes/default/js', file: 'jquery.animate-enhanced.min.js')}"></script>

<script src="${resource(dir: 'themes/default/js', file: 'showhide.js')}"></script>
<script src="${resource(dir: 'themes/default/js', file: 'select2.js')}"></script>
<script src="${resource(dir: 'themes/default/js', file: 'icheck.js?v=1.0.2')}"></script>
<script src="${resource(dir: 'themes/default/js', file: 'jquery.magnific-popup.js')}"></script> 
<script src="${resource(dir: 'js', file: 'jquery.tooltipster.min.js')}"></script>

<script lang="javascript">
	Exxo.UI.urls['logout'] = '<g:createLink controller="logout" action="index"/>';
	Exxo.UI.urls['baseURL'] = '${createLink(uri: '', absolute : 'true')}';
	Exxo.UI.urls["my_account_url"] = '<g:createLink controller="user" action="myaccount"/>';
	Exxo.UI.urls['savePaginationInfo'] = '<g:createLink controller="session" action="savePaginationDetail"/>';
</script>
<script src="${resource(dir: 'themes/default/js', file: 'classie.js')}"></script> 
<script src="${resource(dir: 'themes/default/js', file: 'gnmenu.js')}"></script> 
<script>
	new gnMenu( document.getElementById( 'gn-menu' ) );
</script>

</body>
</html>
