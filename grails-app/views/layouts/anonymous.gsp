<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="${resource(dir: 'themes/' + session.theme + '/img', file: 'favicon.ico')}" type="image/x-icon">
<title>${ message(code:'web.site.title') }</title>
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script
	src="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>

<script
	src="${resource(dir: 'themes/default/js', file: 'perfect-scrollbar.js')}"></script>

<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/js', file: 'perfect-scrollbar.css')}">
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'bootstrap.min.css')}">
<link
	href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'grayscale.css')}">
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'showhide.css')}">
<link rel="stylesheet" type="text/css"
	href="${resource(dir: 'themes/default/css', file: 'alerts.css')}">
<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'wizzard.css')}" />	
<!-- Custom Style for Themes -->
<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'style.css')}"/>
<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/' + session.theme + '/css', file: 'theme.css')}"/>

<!--  Default Javascript Files -->
<script
	src="${resource(dir: 'themes/default/js', file: 'jquery.easing.1.3.js')}"></script>
<script
	src="${resource(dir: 'themes/default/js', file: 'jquery.animate-enhanced.min.js')}"></script>
<script
	src="${resource(dir: 'themes/default/js', file: 'icheck.js?v=1.0.2')}"></script>
<script
	src="${resource(dir: 'themes/default/js', file: 'custom.min.js?v=1.0.2')}"></script>
<script src="${resource(dir: 'themes/default/js', file: 'select2.js')}"></script>

<!-- Bootbox Messages -->
<link href="${resource(dir: 'themes/default/css', file: 'messi.css')}" rel="stylesheet">
<script src="${resource(dir: 'themes/default/js', file: 'messi.js')}"></script>

<!-- Exxo JS Files -->
<script src="${resource(dir: 'themes/default/js', file: 'application.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_main.js')}"></script>

<script>
    Exxo.UI.translates['cancelButton'] = "${ message(code:'default.field.buttonCancel') }";
    Exxo.UI.translates['uploadButton'] = "${ message(code:'default.button.upload') }";
    Exxo.UI.translates['closeButton'] = "${ message(code:'default.field.buttonClose') }";
    Exxo.UI.translates['saveButton'] = "${ message(code:'default.field.buttonSave') }";
</script>	
<g:layoutHead/>
</head>

<body id="body-nobars" class="${pageProperty( name:'body.class' )}">
<g:if test="${ session.maintenanceMsg && session.maintenanceMsg != "" }">
	<div class="warning-site-message">
		<div class="alert alert-maintance">
			<a class="close" data-dismiss="alert">×</a>
			${ session.maintenanceMsg }
		</div>
	</div>
</g:if>

<section class="intro intro-cont">
	
		<div class="container">
			<div class="row rower-over">
				<div class="col-sm-12 col-md-12 col-md-offset-12 margin-center">
					<div id="main">
						<img class="name-app"
							src="${resource(dir: 'themes/' + session.theme + '/img', file: 'logo-login-app.png')}"
							width="122" height="123" alt="MYE Entertainment App">
							
						<g:layoutBody/>
						
					</div>
				</div>
			</div>
		</div>
	
</section>
<nav class="navbar-custom navbar-fixed-top nav-under" role="navigation">
	<div class="container">
		<div class="navbar-header page-scroll">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-main-collapse">
				<i class="fa fa-bars"></i>
			</button>
		</div>
		<div
			class="navbar-collapse navbar-right">
			<ul class="nav navbar-nav">
				<li class="hidden"><a href="#page-top"></a></li>
				<li class="page-scroll"><a
					href="${message(code:'whoWeAre.link.url')}" target="_blank"><g:message
							code="home.whoWeAre" />&nbsp;|</a></li>
				<li class="page-scroll"><a
					href="${message(code:'buyAMyBox.link.url')}" target="_blank"><g:message
							code="home.buyAMyBox" />&nbsp;|</a></li>
				<li class="page-scroll"><a
					href="${message(code:'downloadMyApp.link.url')}" target="_blank"><g:message
							code="home.downloadMyApp" />&nbsp;|</a>
				<li class="page-scroll"><a
					href="${message(code:'faq.link.url')}" target="_blank"><g:message
							code="home.faq" />&nbsp;|</a>
				<li class="page-scroll"><a
					href="${message(code:'support.link.url')}" target="_blank"><g:message
							code="home.support" /></a></li>
			</ul>
		</div>
	</div>
</nav>
<!-- Ajax Flow Container -->
<div id="ajaxFlowWait" class="ajaxFlow" style="display:none;">
    <span class="waitBackground"></span>
    <span class="waiter">
        <span class="wait">
            <span class="title">${ message(code:'please.wait') }</span>
            <span class="spinner"></span>
        </span>
    </span>
</div>		
<footer>
	<a href="${ message(code:'privacy.policy.url') }" target="_blank"><g:message
			code="home.privacy" /></a> | <a
		href="${ message(code:'terms.and.conditions.url') }" target="_blank"><g:message
			code="home.terms" /></a><br>
    <g:message code="application.copyright" args='["2018"]' />
	|
	<g:message code="application.version" />
	${grailsApplication.metadata['app.version']}
</footer>

</body>
</html>
