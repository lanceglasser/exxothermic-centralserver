<g:if test="${flash.success}">
	<div class="alert alert-success">
		<a class="close" data-dismiss="alert">×</a>
		${flash.success}
	</div>
	${flash.success=null}
</g:if>
<g:if test="${flash.info}">
	<div class="alert alert-success">
		<a class="close" data-dismiss="alert">×</a>
		${flash.info}
	</div>
	${flash.info=null}
</g:if>
<g:if test="${flash.warn}">
	<div class="alert alert-warning">
		<a class="close" data-dismiss="alert">×</a>
		${flash.warn}
	</div>
	${flash.warn=null}
</g:if>

<div class="alert alert-error" id="divError" style="display:${flash.error || errorMessages? 'block;':'none;'}">
	<a class="close" data-dismiss="alert">×</a>
	<span id="errorMessageContainer">
	<g:if test="${flash.error || errorMessages}">
		${flash.error}
		${errorMessages}
		<g:hasErrors bean="${entity}">
			<ul>
				<g:eachError bean="${entity}" var="error">
					<li
						<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>>
						<g:message error="${error}" /></li>
				</g:eachError>
			</ul>
		</g:hasErrors>
		<g:hasErrors bean="${entity2}">
			<ul>
				<g:eachError bean="${entity2}" var="error">
					<li
						<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>>
						<g:message error="${error}" /></li>
				</g:eachError>
			</ul>
		</g:hasErrors>
		${flash.error=null}
	</g:if>
	</span>
</div>