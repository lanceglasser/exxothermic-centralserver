
<select name="${id}.id" id="${id}">
	<g:each in="${company}" var="comp">
		<g:if test="${comp.locations.size() > 0 }">
			<optgroup label="${comp?.name?.encodeAsHTML()}**">
				<g:each in="${comp.locations}" var="loc">
				    <g:if test="${allowedLocations != null}">
				    	<g:if test="${allowedLocations.any { it == (loc) }}">
				    		<g:if test="${loc.id==value.toInteger()}">
								<option  value="${loc.id}" selected > ${loc?.name?.encodeAsHTML()}</option>						
							</g:if> 
							<g:else>
								<option value="${loc.id}" > ${loc?.name?.encodeAsHTML()}</option>
							</g:else>
				    	</g:if>				    	 
				    </g:if>	
				    <g:else>				    	 
						<g:if test="${loc.id==value.toInteger()}">
							<option  value="${loc.id}" selected > ${loc?.name?.encodeAsHTML()}</option>						
						</g:if> 
						<g:else>
							<option value="${loc.id}" > ${loc?.name?.encodeAsHTML()}</option>
						</g:else>
					</g:else>	
				</g:each>
			</optgroup>
		</g:if>
	</g:each>
</select>