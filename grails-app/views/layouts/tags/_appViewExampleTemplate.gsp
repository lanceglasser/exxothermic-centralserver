<g:set var="currentAppTab" value="${(currentAppTab)? currentAppTab:'AUDIO'}" />
<g:set var="offer" value="${(offer != null)? offer:false}" />
<g:set var="isFullPreview" value="${(isFullPreview != null)? isFullPreview:false}" />

<script
			src="${resource(dir: 'themes/default/js', file: 'jssor.js')}"></script>
<script
			src="${resource(dir: 'themes/default/js', file: 'jssor.slider.js')}"></script>
<script type="text/javascript">
	<g:if test="${ isFullPreview }">
	Exxo.UI.vars['main-color'] = "${ appTheme?.primaryColor != ""? appTheme?.primaryColor:'#354cf4' }";
	Exxo.UI.vars['second-color'] = "${ appTheme?.secondaryColor!= ""? appTheme?.secondaryColor:'#354cf4'}";
	</g:if>
</script>


<div class="col-xs-12" style="padding: 0px;">
	<g:if test="${ isFullPreview }">
		<div class="col-xs-6" style="padding-left: 0px;">
			<p>${ message(code:'full.app.preview.note') }</p>
			<p>${ message(code:'full.app.preview.note.2') }</p>
			<p>${ message(code:'full.app.preview.note.3') }</p>
			<p>${ message(code:'full.app.preview.note.4') }</p>
		</div>
	</g:if>
	
	<div id="app-main-view" class="col-xs-6" style="padding-left: 0px; min-width: 286px;">
		<div class="phone-app">
			<div class="phone-header"></div>
			<div class="app-header main-color">
				<span id="app-title">${ appTheme?.title }</span>
			</div>
			<div class="ads-container main-color">
				<div id="app-skin-background" style="width: 270px; height:118px; overflow: hidden; background-color: #000;">
					<div id="slider_banners_container" style="position: relative; top: 0px; left: 0px; width: 270px;
					        height: 118px;">
					        <!-- Slides Container -->
					        <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 270px; height: 118px;
					            overflow: hidden;">
					            
					<g:if test="${ isFullPreview }">
						<g:if test="${ banners.size() == 0 }">
							<div></div>
						</g:if>
						<g:each in="${banners}" status="i" var="banner">
							<g:if test="${ banner.type == "text" }">							
								<div class="general-banner-container text-banner-container" style="position: relative;"
									type="text" desc="${ banner.description }" url="${ banner.channelContent }">
									<img id="app-skin-background-img" alt="" width="270px" height="118px;"
										src="${appTheme?.backgroundImageUrl? appTheme.backgroundImageUrl:''}"
										style="display:none;" />
									<div class="company-icon mycircle"></div>
									<div class="text-banner-msg-box">${ banner.description }</div>
								</div>
							</g:if>
							<g:else>
							<g:if test="${ banner.type == "image" }">
								<div class="general-banner-container" type="image" title="${ banner.channelLabel }"
									desc="${ banner.description }" dialog="${ banner.dialogImage }" url="${ banner.channelContent }"
									style="background-color: #${ banner.channelColor}">
									<img alt="" width="270px" height="118px;"
											src="${(banner.imageURL != "")? banner.imageURL:''}"/>
								</div>
							</g:if>
							<g:else>
								<div class="general-banner-container" type="offer" title="${ banner.channelLabel }"
									desc="${ banner.description }" dialog="${ banner.dialogImage }" url="${ banner.channelContent }"
									hours="${ banner.schedule.hours }" days="${ banner.schedule.days }"
									expiration="${ banner.expirationText }"
									style="background-color: #${ banner.channelColor}">
									<img alt="" width="270px" height="118px;"
											src="${(banner.imageURL != "")? banner.imageURL:''}"/>
								</div>
							</g:else>
							<!-- Is a Offer Banner -->
							</g:else>
						</g:each>
					</g:if>
					<g:else> <!-- It's not the Full Preview -->
					<img id="app-skin-background-img" alt="" width="270px" height="118px;"
						src="${appTheme?.backgroundImageUrl? appTheme.backgroundImageUrl:''}"
						style="display:none;" />
					<div class="text-banner-container" style="position: relative; display: none;">
						<div class="company-icon mycircle"></div>
						<div class="text-banner-msg-box">My text message</div>
					</div>
					</g:else>
						</div>
					    <a style="display: none" href="http://www.jssor.com">jQuery Slider</a>
					</div>
				</div>
			</div>
			<div class="app-tabs">
				<div class="tab ${ currentAppTab == 'AUDIO'? 'current':'' }">
					<span>AUDIO</span>
				</div>
				<div class="tab ${ currentAppTab == 'INFORMATION'? 'current':'' }">
					<span>INFORMATION</span>
				</div>
				<div class="tab ${ currentAppTab == 'OFFERS'? 'current':'' }">
					<span>OFFERS</span>
				</div>
				<div class="clearer"></div>
			</div>
			<div class="tabs-separator"> </div>
			<div id="APP-DEMO-DIV-AUDIO" class="channels-container tab-container" ${ currentAppTab == 'AUDIO'? '':'style="display: none;"' }>
				<div>
					<!-- 
					<div class="channel">
						<div class="name"> Audio 1</div>
						<div class="info"></div>
						<div class="info-icon"></div>
						<div class="clearer"></div>
					</div>
					 -->
					<div class="channel" style="background-color: #5785D8; color: #fff;">
						<div class="name" style="color: #fff;"> Audio 1</div>
						<div class="info-icon"></div>
						<div class="pause-button"></div>
						<div class="clearer"></div>
					</div>
					<div class="channel">
						<div class="name"> Audio 2</div>
						<div class="info"></div>
						<div class="info-icon"></div>
						<div class="clearer"></div>
					</div>
				</div>
				
			</div>
			
			<div id="APP-DEMO-DIV-INFORMATION" class="docs-container tab-container" ${ currentAppTab == 'INFORMATION'? '':'style="display: none;"' }>
				<g:if test="${ isFullPreview }">
					<g:each in="${docCategories}" status="i" var="docCat">
						<div class="header"><span>${ docCat.key }</span></div>
						<g:each in="${docCat.value}" var="doc">
						   <div class="doc" style="cursor: pointer;" ref="${ doc.url }"><span>
						   	${ org.apache.commons.lang3.StringEscapeUtils.unescapeJava(doc.name) }</span></div>
						</g:each>
						<div class="cat-separator"></div>
					</g:each>
				</g:if>
				<g:else>
					<div class="header"><span>General</span></div>
					<div class="doc"><span>Gym's Rules</span></div>
					<div class="doc"><span>Features</span></div>
					<div class="cat-separator"></div>
					<div class="example" style="display: none;">
						<div class="header"><span>Schedules</span></div>
						<div class="doc"><span>January</span></div>
					</div>
					<div class="demo" style="display: none;">					
						<div class="header"><span>Offers</span></div>
						<div class="doc"><span class="doc-title"></span></div>
					</div>
				</g:else>
			</div>
			
			<div id="APP-DEMO-DIV-OFFERS" class="offers-container tab-container" ${ currentAppTab == 'OFFERS'? '':'style="display: none;"' }>
				<g:if test="${ isFullPreview }">
					<g:each in="${offers}" var="offer">					   	
					   	<div class="offer" title="${ offer.title }" desc="${ offer.description }" dialog="${ offer.dialogImage }"
					   		hours="${ offer.schedule.hours }" days="${ offer.schedule.days }" url="${ offer.url }">
							<div class="offer-thumbnail" style="background-color: #${ offer.backgroundColor}; cursor: pointer;">
								<g:if test="${ offer.dialogImage != ""}">
									<img alt="" width="90px" height="60px;" src="${ offer.dialogImage }">
								</g:if>
							</div>
							<div class="info">
								<span class="title">${ org.apache.commons.lang3.StringEscapeUtils.unescapeJava(offer.title) }</span>
								<span class="expiration">
									Expires: <custom:expirationDate expiration="${ offer.schedule.expirationDate }"
													locationTimeZone="${ location?.timezone }"/>
								</span>
							</div>
							<div class="clearer"></div>
						</div>
					</g:each>
				</g:if>
				<g:else>
					<g:if test="${ !offer }">
					<div class="offer">
						<div class="offer-thumbnail"></div>
						<div class="info">
							<span class="title">CHRISTMAS'S OFFER</span>
							<span class="expiration">Expires: 12/31/2015</span>
						</div>
						<div class="clearer"></div>
					</div>
					
					<div class="offer">
						<div class="offer-thumbnail"></div>
						<div class="info">
							<span class="title">MONDAY'S OFFER</span>
							<span class="expiration">Expires: Never</span>
						</div>
						<div class="clearer"></div>
					</div>
					</g:if>
					<g:else>
					<div id="offer-demo" class="offer">
						<div class="offer-thumbnail-img-div offer-thumbnail" style="display: none; overflow: hidden;">
							<img id="offer-demo-thumbnail-img" alt="" width="90px" height="60px;" src="" style="display:none;">
						</div>
						
						<div class="gen-offer-thumbnail-img-div offer-thumbnail" style="display: none; padding-top: 7px;" >
							<div style="width: 90px; height: 46px; overflow: hidden;">
								<img id="offer-gen-thumbnail-img" alt="" width="90px" height="46px;" src="">
							</div>
						</div>
						
						<div class="info">
							<span class="title"></span>
							<span class="expiration"></span>
						</div>
						<div class="clearer"></div>
					</div>
					</g:else>
				</g:else>
				
				
			</div>
			
			<div class="play-controls main-color-part">
				<div class="channel-thumbnail"> </div>
				<div class="channel-text"> Now Playing Audio 1</div>
				<div class="skip-next-button"></div>
				<div class="pause-button"></div>
				<div class="skip-prev-button"></div>
				<div class="clearer"></div>
			</div>
		</div>
		<div class="clearer"></div>
		<div class="phone-app-demo-note"> ${ message(code:'app.demo.note') } </div>
	</div>
	
	<div id="app-detail-view" class="col-xs-6" style="padding-left: 0px; ${ isFullPreview? 'display: none;':'' }">
		<!-- Offer or Banner Detail View -->
		<div class="phone-app">
			<div class="phone-header"></div>
			<div class="dialog-app-header">
				<span class="title"></span>
			</div>
			<div class="dialog-img-container main-color">
				<div class="dialog-img-div" style="display: none;">
					<img id="dialog-img" alt="" width="100%" height="210px;"
						src="" style="display:none;" />
				</div>
				
				<div class="gen-dialog-img-div" style="display: none;" >
					<div style="width: 270px; height: 118px; overflow: hidden;">
						<img id="gen-dialog-img" alt="" width="270px" height="118px;" src="">
					</div>
				</div>
			</div>
			<div style="background-color: rgb(208, 208, 208); height: 173px; overflow: hidden;">
				<span id="dialog-description"></span>
				<div style="background-color: rgb(208, 208, 208); height: 149px; padding-top: 15px;">				
					<span id="dialog-view-expiration"></span>
					<span id="dialog-view-available" style="display: none;"></span>
					<span id="dialog-view-more-btn" style="display:none; ${ (offer || isFullPreview)? '':'margin-top: 20px;'}">MORE INFO</span>
				</div>
			</div>
						
		</div>
		<div class="clearer"></div>
		<div class="phone-app-demo-note"> ${ message(code:'app.demo.note.2') } </div>
	</div>
	<div class="clearer"></div>
</div>



