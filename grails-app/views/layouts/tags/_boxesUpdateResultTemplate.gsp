<g:if test="${boxesSyncResult != null && boxesSyncResult.getTotalCounter() > 0}">
<div id="syncResultContainer">
	<br />
	<br />
	<legend>
		<g:message code="boxes.sync.results" />
	</legend>
	<table id="table-list-two" class="display" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>
					${message(code:'default.field.serial')}
				</th>
				<th>
					${message(code:'default.field.status')}
				</th>
				<th>
					${message(code:'default.field.errorDescription')}
				</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th>
					${message(code:'default.field.serial')}
				</th>
				<th>
					${message(code:'default.field.status')}
				</th>
				<th>
					${message(code:'default.field.errorDescription')}
				</th>
			</tr>
		</tfoot>
		<tbody>
			<g:each in="${boxesSyncResult.getBoxUpdatesList()}" status="i"
				var="record">
				<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					<td class="widthTdRow10">
						${record.getSerial()}
					</td>
					<td class="widthTdRow10">
						${message(code:'boxes.sync.result.status.'+record.getStatus())}
					</td>
					<td class="widthTdRow10">
						${message(code:messageErrorDescriptionHash.get(record.getDescription()))}
					</td>
				</tr>
			</g:each>
		</tbody>
	</table>
</div>
</g:if>