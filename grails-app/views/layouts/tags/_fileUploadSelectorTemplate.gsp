<!-- Template: imageUploadSelectorTemplate -->
<br/>
<span id="${id}Div">
<g:set var="leftColumnSize" value="${(leftColumnSize != null)? leftColumnSize:4}" />
<g:set var="ref" value="${(ref != null)? ref:''}" />

<div class="col-sm-${ leftColumnSize } text-to-right">
	<label for="${ id }">
		<g:if test="${tooltip}">
			<span class="tooltipImage" >
   				<span>
   					${tooltip}
   				</span>
			</span>
		</g:if>
		${ label } *
	</label>
</div>
<div class="col-xs-${ 12 - leftColumnSize } text-to-left">
	<div id="errorDiv${ref}" class="alert alert-error"  style="display:none;"></div>
	
	<div id="uploads">
		<div class="color-wrapper fakeupload">
			<div class="input">
				<input type="text" id="fakeupload${ref}" name="fakeupload${ref}" tabindex="-1">
			</div>
			<div class="color-box">
			</div>
		</div>
		<input type="hidden" id="${ id }-updated" name="${ id }-updated" value="0"/>
		<input type="file" ${accept? 'accept="' + accept + '"':''} class="realupload" id="${ id }" name="${ id }"
			ref="${ ref }" tabindex="-1"
			>
		<g:if test="${ note != null && note.trim() != '' }">
			<span class="liten-up">${ note }</span>
		</g:if>
	</div>
</div>
</span>
<span class="clearfix"></span>