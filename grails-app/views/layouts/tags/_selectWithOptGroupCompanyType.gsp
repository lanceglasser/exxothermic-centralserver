
<select name="${id}.id" id="${id}">
	<g:each in="${types}" var="type">
		<g:if test="${type.catalog.size()>0 &&  type.enable}">
			
			
			<g:if test="${type.catalog.size()==1}">
					<g:each in="${type.catalog}" var="subtype">
						<g:if test="${subtype.code==value}">
							<option id="${subtype.code}" value="${subtype.id}" selected >${message(code:"typeOfEstablishment." + subtype.code.encodeAsHTML())}</option>
						</g:if>
						<g:else>
							<option id="${subtype.code}" value="${subtype.id}" >${message(code:"typeOfEstablishment." + subtype.code.encodeAsHTML())}</option>
						</g:else>
					</g:each>
			</g:if>
			<g:else>
				<optgroup label="${message(code:"typeOfEstablishment." + type.code.encodeAsHTML())}">
					<g:each in="${type.catalog}" var="subtype">
						<g:if test="${subtype.code==value}">
							<option id="${subtype.code}" value="${subtype.id}" selected >${message(code:"typeOfEstablishment." + subtype.code.encodeAsHTML())}</option>
						</g:if>
						<g:else>
							<option id="${subtype.code}" value="${subtype.id}" >${message(code:"typeOfEstablishment." + subtype.code.encodeAsHTML())}</option>
						</g:else>
					</g:each>
				</optgroup>
			</g:else>
		</g:if>
		<
	</g:each>
</select>