<!-- Template: imageUploadSelectorTemplate -->
<br/>
<g:set var="leftColumnSize" value="${(leftColumnSize != null)? leftColumnSize:4}" />
<g:set var="cropImage" value="${(cropImage != null)? cropImage:false}" />
<g:set var="ref" value="${(ref != null)? ref:''}" />

<g:set var="demoId" value="${(demoId != null)? demoId:''}" />
<g:set var="demoWidth" value="${(demoWidth != null)? demoWidth:0}" />
<g:set var="demoHeight" value="${(demoHeight != null)? demoHeight:0}" />

<g:set var="demoId2" value="${(demoId2 != null)? demoId2:''}" />
<g:set var="demoWidth2" value="${(demoWidth2 != null)? demoWidth2:0}" />
<g:set var="demoHeight2" value="${(demoHeight2 != null)? demoHeight2:0}" />

<g:set var="demoId3" value="${(demoId3 != null)? demoId3:''}" />
<g:set var="demoWidth3" value="${(demoWidth3 != null)? demoWidth3:0}" />
<g:set var="demoHeight3" value="${(demoHeight3 != null)? demoHeight3:0}" />

<g:set var="minWidth" value="${(minWidth != null)? minWidth:10}" />
<g:set var="minHeight" value="${(minHeight != null)? minHeight:10}" />

<g:set var="cropHeight" value="${(cropHeight != null)? cropHeight:10}" />
<g:set var="cropWidth" value="${(cropWidth != null)? cropWidth:10}" />

<g:set var="maxWidth" value="${(maxWidth != null)? maxWidth:mycentralserver.utils.Constants.IMAGE_TO_CROP_MAX_WIDTH}" />
<g:set var="maxHeight" value="${(maxHeight != null)? maxHeight:mycentralserver.utils.Constants.IMAGE_TO_CROP_MAX_HEIGHT}" />

<g:set var="maxSize" value="${(maxSize != null)? maxSize:
	mycentralserver.utils.Utils.getFileHumanSize(mycentralserver.utils.Constants.IMAGE_TO_CROP_MAX_SIZE)}" />

<g:set var="fileSizeError" value="${ message(code:'file.image.size.restriction', args:[maxSize]) }" />

<g:set var="sizeError" value="${(minWidth != maxWidth || minHeight != maxHeight)? 
	message(code:'file.image.restrictions.with.min',args:[minWidth,minHeight,maxWidth,maxHeight]):
	message(code:'file.image.restrictions',args:[minWidth,minHeight])}" />

<g:set var="note" value="${(minWidth != maxWidth || minHeight != maxHeight)? 
	message(code:'file.image.note.with.min',args:[minWidth,minHeight,maxWidth,maxHeight,maxSize,cropWidth,cropHeight]):
	message(code:'file.image.note',args:[minWidth,minHeight,maxSize])}" />


<div class="col-sm-${ leftColumnSize } text-to-right">

	<label for="${ id }">
		<g:if test="${tooltipImage}">
<span class="tooltipImage" >   
   		<span>Info:	${ ImageInfo }
   		<img src="${tooltipImageResource}">   
   		</span>
	</span>
</g:if>
	${ label }
	</label>
	
</div>
<div class="col-xs-${ 12 - leftColumnSize } text-to-left">
	<div id="errorDiv${ref}" class="alert alert-error"  style="display:none;"></div>
	<g:if test="${imgValue}">
		<div id="demo${ref}" >
	</g:if>
	<g:else>
		<div id="demo${ref}" style="display:none;" >
	</g:else>
		<div id="targetImgDiv${ref}">
			<g:if test="${ imgValue }">
				<img id="target${ref}" style="width:300px; height:auto; padding: 5px ; border: 1px #bbb solid;" src="${imgValue}"/>
			</g:if>
		</div>
	  	<g:if test="${ cropImage != null && cropImage }">
	  		<!-- Crop Ads Background Image Values -->
			<input type="hidden" id="x-${id}" name="x-${id}" />
			<input type="hidden" id="y-${id}" name="y-${id}" />
			<input type="hidden" id="w-${id}" name="w-${id}" />
			<input type="hidden" id="h-${id}" name="h-${id}" />
			
			<div class="preview-pane" id="preview-pane-${id}" style="display:none;" >
			    <div class="preview-container" >
                    <g:if test="${ imgValue }">
			      	<img  id="preview${ref}" class="jcrop-preview" src="${imgValue}" alt="Preview"  />
                    </g:if>
                    <g:else>
                    <img  id="preview${ref}" class="jcrop-preview" alt="Preview"  />
                    </g:else>
			    </div>
			</div>
		</g:if>
	</div>
	<div id="uploads">
		<div class="color-wrapper fakeupload">
			<div class="input">
				<input type="text" id="fakeupload${ref}" name="fakeupload${ref}" tabindex="-1" class="notab">
			</div>
			<div class="color-box">
			</div>
		</div>
		<input type="hidden" id="${ id }-updated" name="${ id }-updated" value="0"/>
		<input type="file" class="imgUploadSelector notab" id="${ id }" name="${ id }"  tabindex="-1"
			ref="${ ref }"
			demoId="${ demoId }"
			demoWidth="${ demoWidth }"
			demoHeight="${ demoHeight }"
			d2id="${ demoId2 }"
			d2w="${ demoWidth2 }"
			d2h="${ demoHeight2 }"
			d3id="${ demoId3 }"
			d3w="${ demoWidth3 }"
			d3h="${ demoHeight3 }"
			maxWidth="${ maxWidth }"
			minWidth="${ minWidth }"
			maxHeight="${ maxHeight }"
			minHeight="${ minHeight }"
			maxSize="${ maxSize }"
			fileSizeError="${ fileSizeError }"
			sizeError="${ sizeError }"
			cropImage="${ (cropImage != null && cropImage)? 'true':'false' }">
		<g:if test="${ note != null && note.trim() != '' }">
			<span class="liten-up">${ note }</span>
		</g:if>
	</div>
</div>
<span class="clearfix"></span>