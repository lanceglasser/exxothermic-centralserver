
<select name="${id}.id" id="${id}">
	<g:each in="${types}" var="type">
		<g:if test="${type.value.get('isSubgroup')==true}">
			<optgroup label="${message(code:"typeOfEstablishment." + type.key.encodeAsHTML())}">
				<g:each in="${type.value.get('subtypes')}" var="subtype">
					<g:if test="${subtype.code==value}">
						<option id="${subtype.code}" value="${subtype.id}" selected >${message(code:"typeOfEstablishment." + subtype.code.encodeAsHTML())}</option>
					</g:if>
					<g:else>
						<option id="${subtype.code}" value="${subtype.id}" >${message(code:"typeOfEstablishment." + subtype.code.encodeAsHTML())}</option>
					</g:else>
				</g:each>
			</optgroup>
		</g:if>
		<g:else>
			<g:each in="${type.value.get('subtypes')}" var="subtype">
				<g:if test="${subtype.code==value}">
					<option id="${subtype.code}" value="${subtype.id}" selected >${message(code:"typeOfEstablishment." + subtype.code.encodeAsHTML())}</option>
				</g:if>
				<g:else>
					<option id="${subtype.code}" value="${subtype.id}" >${message(code:"typeOfEstablishment." + subtype.code.encodeAsHTML())}</option>
				</g:else>
			</g:each>
		</g:else>
		
		
	</g:each>
</select>