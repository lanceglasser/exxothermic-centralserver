<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<%@page import="grails.plugins.springsecurity.SpringSecurityService"%>
<% thisYear = Calendar.getInstance().get(Calendar.YEAR) %>


<html lang="en" class="no-js">
<!--<![endif]--><head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="ExXothermic Cloud Service" /></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.css')}" type="text/css">
		
		<link rel="stylesheet" href="${resource(dir: 'css/redmond', file: 'jquery-ui-1.10.3.custom.css')}" type="text/css">
		
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'style.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'tooltipster.css')}" type="text/css">
		
		<link rel="stylesheet" media="screen and (max-width:480px)" href="${resource(dir: 'css', file: 'mobile.css')}" type="text/css">
		<!-- mobile -->
		<link rel="stylesheet" media="screen and (min-width:481px)"
			href="${resource(dir: 'css', file: 'pc.css')}" type="text/css">
		<!-- PC -->
		
		<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
		
		
		<script src="${resource(dir: 'js', file: 'jquery-ui-1.10.3.custom.min.js')}"></script>
		<script src="${resource(dir: 'js', file: 'jquery.tooltipster.min.js')}"></script>
		<script src="${resource(dir: 'js', file: 'bootstrap.js')}"></script>
		<script src="${resource(dir: 'js/pluggins/bootbox', file: 'bootbox.js')}"></script>
			
		<g:layoutHead />
		<r:layoutResources />
		
		<script>
			$(document).ready(function() {
				$('.questionToolTip').tooltipster({
					position : 'right',
					maxWidth : 210
				});
			});
		</script>
	</head>
	<body>
	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container-fluid">

				<g:link controller="home" action="index" class="brand">ExXothermic Cloud Service</g:link>

				<ul class="nav">

					<li class=" ${params.controller=='home'?'active':''} home"><g:link
							controller="home" action="index">
							<g:message code="default.home.home" />
						</g:link></li>

					<sec:ifAnyGranted
						roles="ROLE_OWNER, ROLE_INTEGRATOR, ROLE_HELP_DESK">
						<li
							class=" ${params.controller=='company'?'active':''} listCompany">
							<g:link controller="company" action="index">
								<g:message code="default.home.companies" />
							</g:link>
						</li>
					</sec:ifAnyGranted>

					<sec:ifAnyGranted roles="ROLE_ADMIN">
						<li
							class=" ${params.controller=='companyIntegrator'?'active':''} listCompany">
							<g:link controller="companyIntegrator" action="index">
								<g:message code="default.home.companiesIntegrators" />
							</g:link>
						</li>
					</sec:ifAnyGranted>
					 
					<li
						class=" ${params.controller=='location'?'active':''} listLocation">
						<g:link controller="location" action="listAll">
							<g:message code="default.home.locations" />
						</g:link>
					</li>
				
					<sec:ifAnyGranted roles="ROLE_OWNER, ROLE_INTEGRATOR">
						<li class=" ${params.controller=='employee'?'active':''} listUser">
							<g:link controller="employee" action="listAll">
								<g:message code="default.home.employees" />
							</g:link>
						</li>
					</sec:ifAnyGranted>

					<sec:ifAnyGranted roles="ROLE_ADMIN">
						<li class=" ${params.controller=='user'?'active':''} listUser">
							<g:link controller="user" action="listAll">
								<g:message code="default.home.users" />
							</g:link>
						</li>
					</sec:ifAnyGranted>

				</ul>


				<p class="navbar-text pull-right">
					<g:message code="default.home.LoggedInAs" />
					<g:link controller="user" action="myaccount" class="navbar-link">
						<user:getCurrentUserName />
					</g:link>
					|
					<g:link controller="logout" class="navbar-link logout"
						title="${message(code:'default.home.logout')}"></g:link>
				</p>

			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row">			 
			<div class="span9">
					<div class="leftDimension" style="width:950px;">
						<g:layoutBody />
					</div>
			</div>
		</div>
		<!--/row-->
		<hr>
		<footer>
			<div>
				<a href="http://www.exxothermic.com/privacy.html"><g:message
						code="default.home.privacy" /></a> | <a
					href="http://www.exxothermic.com/terms.html"><g:message
						code="default.home.terms" /></a>
				<p>
					<g:message code="default.application.copyright" args='["2018"]' /> |
					<g:message code="default.application.version" />
					${grailsApplication.metadata['app.version']}
				</p>

			</div>

		</footer>
	 
	</div>
	<g:javascript>var baseURL = '${createLink(uri: '', absolute : 'true')}';</g:javascript>


	<g:javascript library="application" />
	<r:layoutResources />
</body>
</html>
	