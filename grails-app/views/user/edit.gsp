<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title><g:message code="default.title.updateUser" /></title> 
		<link rel="stylesheet" href="${resource(dir: 'css/pluggins/select2', file: 'select2.css')}" type="text/css">
		<script src="${resource(dir: 'js/pluggins/select2', file: 'select2.js')}"></script>	
		<script src="${resource(dir: 'js/pages/user', file: 'selects.js')}"></script>
		<script> Exxo.module = "user-edit"; </script>	
	</head>
	<body>
		<g:render template="/layouts/internalContentTemplate">
			<g:form action="update" name="updateForm" class="exxo-form" >
				<g:hiddenField name="id" value="${user.id}" />
				<g:hiddenField name="version" value="${user.version}" />
				<g:hiddenField name="offset" value="${offset}" />
			
					<h1><g:message code="default.title.updateUser" /></h1>
					<div class="content">
						<g:render template="/layouts/messagesAndErrorsTemplate"
							model="[entity: user, secondEntity: userRole]" />
		      		
	     		 		<g:render template="userCommonFieldsTemplate">
	     		 			<custom:editField type="text" name="location" disabled="${true}" required="${false}" value="${user.lastUpdatedBy ? user.lastUpdatedBy.firstName + " " + user.lastUpdatedBy.lastName : ""}" label="user.lastUpdatedBy.label" />
	     		 			
	     		 			<g:submitButton class="form-btn btn-primary" name="save" value="${message(code:'default.field.buttonSave')}" />			
							<g:link action="listAll" controller="user" class="back-button">        
						       <span class="form-btn btn-primary"> ${message(code:'default.field.buttonCancel')}</span>       
					  		</g:link> 
				  		
	     		 		</g:render>
	     		 		
	     		 		<span class="clearfix"></span>
				</div>				 
			</g:form>
		</g:render>	
	</body>
</html>

