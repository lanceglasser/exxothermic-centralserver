<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title><g:message code="default.title.newUser" /></title>
		<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_user_create.js')}"></script>
		<script> Exxo.module = "user-create"; </script>	
	</head>
	<body>
	
		<g:render template="/layouts/internalContentTemplate">
			<g:form action="save" name="createForm"  class="exxo-form" >

				<h1><g:message code="default.title.newUser" /></h1>
				<div class="content">		     		     
	         		<g:render template="/layouts/messagesAndErrorsTemplate"
						model="[entity: user]" />
	     		     		
	     		    <g:render template="userCommonFieldsTemplate">
	     		    	<div class="col-xs-4 text-to-right">
	              			<label for="rol"><g:message code="default.field.roleName" />*</label>
	            		</div>
	            		<div class="col-xs-8 text-to-left">										 
	            			<g:select name="role.id" id="role" from="${roles}" optionKey="id" optionValue="description"  required="true"/>
	            		</div>
	            		<span class='clearfix'></span>
	            		
	            		<g:submitButton class="form-btn btn-primary" name="save" value="${message(code:'default.field.buttonSave')}" />
						<g:link action="index" controller="home" class="back-button">        
					       <span class="form-btn btn-primary"> ${message(code:'default.field.buttonCancel')}</span>       
					  	</g:link>
	            		
	     		    </g:render>
				  </div>
			</g:form>
		</g:render>		
	</body>
</html>
