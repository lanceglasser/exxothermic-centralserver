<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.myAccount" /></title>
<script> 
	Exxo.module = "user-account";
	Exxo.UI.vars['validationDetail'] = "${message(code:'password.validation.description')}";
	console.debug("Message = " + Exxo.UI.vars['validationDetail']);
	Exxo.UI.vars['confirmationFails'] = "${message(code:'password.validation.confirmation.fail')}";
</script>
</head>
<body>
	<g:render template="/layouts/internalContentTemplate">
		<g:form action="updatemyaccount" class="exxo-form">

			<h1>
				<g:message code="default.title.myAccount" />
			</h1>
			<div class="content">
				<g:render template="/layouts/messagesAndErrorsTemplate"
					model="[entity: user]" />

				<!-- Column Left -->
				<div class="col-xs-6 resize-formleft">
					<custom:editField type="email" name="email" disabled="${true}"
						value="${user.email}" label="field.email" />
					<custom:editField type="text" name="firstName" required="${true}"
						value="${user.firstName}" label="default.field.firstName" />
					<custom:editField type="text" name="lastName" required="${true}"
						value="${user?.lastName}" label="default.field.lastName" />
				</div>

				<!-- Column right -->
				<div class="col-xs-6 resize-formright"></div>
				<span class='clearfix'></span>

				<g:submitButton class="form-btn btn-primary" name="register"
					value="${message(code:'default.field.buttonSave')}" />
				<g:link action="index" controller="home" class="back-button">
					<span class="form-btn btn-primary"> ${message(code:'default.field.buttonCancel')}
					</span>
				</g:link>
			</div>
		</g:form>

		<br />
		<g:render template="passwordForm" />
	</g:render>
</body>
</html>