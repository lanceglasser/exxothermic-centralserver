<section class="intro">
	<div class="intro-body">
		<div class="container full-width">
			<div class="row">
				<div class="col-md-12" id="internal-content">
					${body()}
				</div>
			</div>
		</div>
	</div>
</section>