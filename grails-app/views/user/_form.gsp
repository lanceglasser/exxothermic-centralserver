<%@ page import="mycentralserver.user.User" %>



<div class="fieldcontain ${hasErrors(bean: userInformationInstance, field: 'email', 'error')} required">
	<label for="email">
		<g:message code="user.email.label"/>
		<span class="required-indicator">*</span>
	</label>
	<g:field type="email" name="email" required="" value="${userInformationInstance?.email}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInformationInstance, field: 'password', 'error')} required">
	<label for="password">
		<g:message code="userInformation.password.label" default="Password" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="password" required="" value="${userInformationInstance?.password}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInformationInstance, field: 'firstName', 'error')} required">
	<label for="firstName">
		<g:message code="userInformation.firstName.label" default="First Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="firstName" maxlength="100" required="" value="${userInformationInstance?.firstName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInformationInstance, field: 'lastName', 'error')} required">
	<label for="lastName">
		<g:message code="userInformation.lastName.label" default="Last Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="lastName" maxlength="100" required="" value="${userInformationInstance?.lastName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInformationInstance, field: 'phoneNumber', 'error')} ">
	<label for="phoneNumber">
		<g:message code="userInformation.phoneNumber.label" default="Phone Number" />
		
	</label>
	<g:textField name="phoneNumber" pattern="${userInformationInstance.constraints.phoneNumber.matches}" value="${userInformationInstance?.phoneNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInformationInstance, field: 'lastUpdatedBy', 'error')} ">
	<label for="lastUpdatedBy">
		<g:message code="userInformation.lastUpdatedBy.label" default="Last Updated By" />
		
	</label>
	<g:select id="lastUpdatedBy" name="lastUpdatedBy.id" from="${mycentralserver.user.User.list()}" optionKey="id" value="${userInformationInstance?.lastUpdatedBy?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInformationInstance, field: 'createdBy', 'error')} ">
	<label for="createdBy">
		<g:message code="userInformation.createdBy.label" default="Created By" />
		
	</label>
	<g:select id="createdBy" name="createdBy.id" from="${mycentralserver.user.User.list()}" optionKey="id" value="${userInformationInstance?.createdBy?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInformationInstance, field: 'accountExpired', 'error')} ">
	<label for="accountExpired">
		<g:message code="userInformation.accountExpired.label" default="Account Expired" />
		
	</label>
	<g:checkBox name="accountExpired" value="${userInformationInstance?.accountExpired}" />
</div>

<div class="fieldcontain ${hasErrors(bean: userInformationInstance, field: 'accountLocked', 'error')} ">
	<label for="accountLocked">
		<g:message code="userInformation.accountLocked.label" default="Account Locked" />
		
	</label>
	<g:checkBox name="accountLocked" value="${userInformationInstance?.accountLocked}" />
</div>

<div class="fieldcontain ${hasErrors(bean: userInformationInstance, field: 'createdDate', 'error')} required">
	<label for="createdDate">
		<g:message code="userInformation.createdDate.label" default="Created Date" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="createdDate" precision="day"  value="${userInformationInstance?.createdDate}"  />
</div>

<div class="fieldcontain ${hasErrors(bean: userInformationInstance, field: 'enabled', 'error')} ">
	<label for="enabled">
		<g:message code="userInformation.enabled.label" default="Enabled" />
		
	</label>
	<g:checkBox name="enabled" value="${userInformationInstance?.enabled}" />
</div>

<div class="fieldcontain ${hasErrors(bean: userInformationInstance, field: 'lastLogin', 'error')} required">
	<label for="lastLogin">
		<g:message code="userInformation.lastLogin.label" default="Last Login" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="lastLogin" precision="day"  value="${userInformationInstance?.lastLogin}"  />
</div>

<div class="fieldcontain ${hasErrors(bean: userInformationInstance, field: 'passwordExpired', 'error')} ">
	<label for="passwordExpired">
		<g:message code="userInformation.passwordExpired.label" default="Password Expired" />
		
	</label>
	<g:checkBox name="passwordExpired" value="${userInformationInstance?.passwordExpired}" />
</div>

<div class="fieldcontain ${hasErrors(bean: userInformationInstance, field: 'user', 'error')} required">
	<label for="user">
		<g:message code="userInformation.user.label" default="User" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="user" name="user.id" from="${mycentralserver.user.User.list()}" optionKey="id" required="" value="${userInformationInstance?.user?.id}" class="many-to-one"/>
</div>

