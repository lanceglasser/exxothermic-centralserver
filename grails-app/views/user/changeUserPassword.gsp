<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title><g:message code="default.title.changePassword" /></title>
	</head>
	<body>
		<g:render template="passwordForm"/>
	</body>
</html>