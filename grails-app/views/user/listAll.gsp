<!DOCTYPE html>
<html>
	<head>	
		<meta name="layout" content="main"/>
		<title><g:message code="default.title.listUser" /></title>
		<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
		<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
		<script src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
		<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
		<script src="${resource(dir: 'js/pages/user', file: 'listAll.js')}"></script>  
		<script>
			Exxo.module = "users-list";
			Exxo.UI.vars["page-code"] = "users-list";
			Exxo.Tables.loadPaginationInitValuesFromString('<custom:paginationInfo code="users-list"/>');
			Exxo.UI.translates['okButton'] = "${message(code:'default.field.buttonOk')}";
			Exxo.UI.translates['cancelButton'] = "${message(code:'default.field.buttonCancel')}";
			Exxo.UI.translates['disableMessage'] = "${message(code:'mycentralserver.user.User.resetPassword')}";
		</script>		
	</head>
	<body>	
		<section class="introp">
		  <div class="intro-body">
		    <div class="container full-width">
		      <div class="row">
		        <div class="col-md-12" id="internal-content">
		          <h1><g:message code="default.title.listUser" /></h1>
					<g:if test="${flash.error}">
			       		<div class="alert alert-error">  
						  <a class="close" data-dismiss="alert">×</a>  
						  ${flash.error} 
							   <g:hasErrors bean="${user}">    						  				  				
							  		<ul>
										<g:eachError bean="${user}" var="error">
										
											<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
										</g:eachError>
									</ul>
								</g:hasErrors>  	
						  ${flash.error=null} 
						</div> 
					</g:if>
					
					<g:if test="${flash.success}">
			       		<div class="alert alert-success">  
						  <a class="close" data-dismiss="alert">×</a>  
						  ${flash.success} 
						  ${flash.success=null} 
						</div> 
			    	</g:if>
				<div class="content">
					<g:if test="${userList.size()>0}">
		            <table id="table-list" class="display" cellspacing="0" width="100%">
		              <thead>
		                <tr>			     
					     <th>${message(code: 'user.email.label')}</th>
					     <th>${message(code: 'roles')}</th>
					     <th>${message(code: 'user.firstName.label')}</th>
					     <th>${message(code: 'user.lastName.label')}</th>
					     <th>${message(code: 'user.phoneNumber.label')}</th>							    							   							    
					     <th>${message(code: 'default.field.enabled')}</th>	
					     <th>${message(code: 'default.table.action')}</th>
		                </tr>
		              </thead>
		              <tbody>
		              	<g:each in="${userList}" status="i" var="userInstance">
							<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
								<td>															  									
										<g:link action="edit" id="${userInstance.id}" params="[offset: offset]">${userInstance.email.encodeAsHTML()}</g:link>
												
								</td>
								<td>
									<user:listUserRoles user="${ userInstance }" />
								</td>
								<td>${fieldValue(bean: userInstance, field: "firstName")}</td>	
								<td>${fieldValue(bean: userInstance, field: "lastName")}</td>												 				
								<td>
									<g:if test="${userInstance.phoneNumber}">
										<div class="value">${userInstance.phoneNumber.encodeAsHTML()}</div>
									</g:if>
									<g:else>
										<div class="value"><g:message code="default.field.empty" /></div>
									</g:else>
								</td>				
							<td>
								<g:if test="${userInstance.enabled == true}">
									<g:message code="default.field.yes" />
								</g:if>
								<g:else>
									<g:message code="default.field.no" />
								</g:else>						
							<td>				
								<g:link action="show" id="${userInstance.id}" class="action-icons view" params="[offset: offset]" title="${message(code:'default.action.show')}"  ></g:link>
									 
									<g:link action="edit" id="${userInstance.id}"  class="action-icons edit" title="${message(code:'default.action.edit')}"  params="[offset: offset]"></g:link>
									<g:if test="${userInstance.enabled}"> 										
										<g:link action="disabled" id="${userInstance.id}" params="[offset: offset]" class="action-icons block" title="${message(code:'default.action.disabled')}"></g:link> 
									</g:if>
									<g:else>
										<g:link action="disabled" id="${userInstance.id}" params="[offset: offset]" class="action-icons unblock" title="${message(code:'default.action.enabled')}" ></g:link> 
									</g:else>	
									<g:link action="resetPassword" id="${userInstance.id}" params="[offset: offset]" class="action-icons changePassword"" title="${message(code:'default.action.resetPassword')}"></g:link> 
												
							</td>
						</tr>
					</g:each>
	              </tbody>
	            </table>
	            </g:if>
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	</section>
	<div id="confirm-reset-dialog" title="${message(code:'mycentralserver.user.User.resetPassword.title')}" style="display:none;">
	  ${message(code:'mycentralserver.user.User.resetPassword')}
	</div>
   		
	</body>
</html>
