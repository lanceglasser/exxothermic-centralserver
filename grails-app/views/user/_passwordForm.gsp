<h1><g:message code="default.title.changePassword" /></h1>
<div class="content">
	<g:form action="changepassword"  class="exxo-form">
		<g:hiddenField name="id" value="${user?.id}" />		
   
		<!-- Column Left -->
		<div class="col-xs-6 resize-formleft">
			<custom:editField type="password" name="newPassword" required="${true}" value="" label="default.field.newpassword" />
			<custom:editField type="password" name="confirmPassword" required="${true}" value="" label="default.field.confirmPassword" />
		</div>
 					
		<!-- Column right -->
		<div class="col-xs-6 resize-formright"></div>
  		<span class='clearfix'></span>	
	     		    
		<g:submitButton class="form-btn btn-primary" name="submit" value="${message(code:'default.field.buttonSave')}" />				
		<g:link action="index" controller="home" class="back-button">        
	       <span class="form-btn btn-primary"> ${message(code:'default.field.buttonCancel')} </span>       
    	</g:link>	
					 
	</g:form>
	<script lang="javascript">
		var validationDetail = "${message(code:'password.validation.description')}";
		var confirmationFails = "${message(code:'password.validation.confirmation.fail')}";
	</script>
</div>