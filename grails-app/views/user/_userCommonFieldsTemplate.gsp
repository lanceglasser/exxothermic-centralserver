<!-- Column Left -->
 <div class="col-xs-6 resize-formleft">
	<custom:editField type="text" name="firstName" required="${true}" max="50" value="${user?.firstName}" label="default.field.firstName" />
	<custom:editField type="text" name="lastName" required="${true}" max="50" value="${user?.lastName}" label="default.field.lastName" />
	<custom:editField type="email" name="email" required="${true}" max="150" value="${user?.email}" label="user.email.label" />
	<custom:editField type="phoneNumber" name="phoneNumber" required="${true}" max="35" value="${user?.phoneNumber}" label="default.field.phoneNumber" note="${message(code:'default.field.example') + '+1 111 222 3333'}" />
	${body()}
</div>