<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.user" /></title>
<script> Exxo.module = "user-show"; </script>
</head>
<body>
	<g:render template="/layouts/internalContentTemplate">
		<h1>
			${user.firstName.encodeAsHTML() + " " + user.lastName.encodeAsHTML()}
		</h1>
		<g:render template="/layouts/messagesAndErrorsTemplate"
			model="[entity: user]" />
		<div class="content form-info">
			<!-- Left Column -->
			<div class="col-xs-6 resize-formleft">
				<custom:editField name="email"
					value="${user.email ? user.email.encodeAsHTML() : ''}"
					label="user.email.label" />
				<custom:editField name="firstName"
					value="${user.firstName ? user.firstName.encodeAsHTML() : ''}"
					label="user.firstName.label" />
				<custom:editField name="lastName"
					value="${user.lastName ? user.lastName.encodeAsHTML() : ''}"
					label="user.lastName.label" />
				<custom:editField name="phoneNumber"
					value="${user.phoneNumber ? user.phoneNumber.encodeAsHTML() : ''}"
					label="default.field.phoneNumber" />
				<custom:editField name="lastUpdatedBy"
					value="${user.lastUpdatedBy ? user?.lastUpdatedBy?.firstName?.encodeAsHTML() + " " + user?.lastUpdatedBy?.lastName?.encodeAsHTML() : ''}"
					label="user.lastUpdatedBy.label" />
			</div>
			<!-- Right Column -->
			<div class="col-xs-6 resize-formright">
				<custom:editField name="accountExpired"
					value="${(user.accountExpired == true) ? message(code:"default.field.yes") : message(code:"default.field.no")}"
					label="default.field.accountExpired" />
				<custom:editField name="accountLocked"
					value="${(user.accountLocked == true) ? message(code:"default.field.yes") : message(code:"default.field.no")}"
					label="default.field.accountLocked" />
				<custom:editField name="passwordExpired"
					value="${(user.passwordExpired == true) ? message(code:"default.field.yes") : message(code:"default.field.no")}"
					label="password.expired" />
				<custom:editField name="enabled"
					value="${(user.enabled == true) ? message(code:"default.field.yes") : message(code:"default.field.no")}"
					label="default.field.enabled" />
			</div>
			<span class="clearfix"></span>

			<!-- Buttons -->
			<g:link action="listAll" controller="user" params="[offset: offset]">
				<input type="button" class="form-btn btn-primary"
					value="${message(code:'default.field.buttonBack')}" />
			</g:link>
		</div>
	</g:render>


</body>
</html>

