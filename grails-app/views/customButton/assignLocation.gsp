<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.home.manageContent" /></title>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_custom_button_assign.js')}"></script>
<script>
	Exxo.module = "button-assign-location";
	Exxo.UI.vars['defaultButtonId'] = "${buttonId}";
</script>
</head>
<body>
	<section class="intro">
		<div class="intro-body">
			<div class="container full-width">
				<div class="row">
					<div class="col-md-12" id="internal-content">
						<h1>
							<g:message code="default.home.manageContent" />
						</h1>
						<p class="muted"> <g:message code="default.assign.location.note" /></p>
						
						
						<div class="content ">

							<g:if test="${flash.error}">
				        		<div class="alert alert-warning">  
									<a class="close" data-dismiss="alert">×</a>  
									${flash.error} 
									${flash.error=null} 
								</div>
							</g:if>
				      		
				      		 <g:if test="${flash.success}">
				        		<div class="alert alert-success">  
								  <a class="close" data-dismiss="alert">×</a>  
								  ${flash.success} 
								  ${flash.success=null}
								</div>
				      		</g:if>
							<g:form action="saveLocations" name="createForm" id="createForm" class="exxo-form">
							<div class="col-xs-12 resize-formleft">
								<div class="col-xs-3">
									<label class="control-label" for="user">
										<g:message code="default.home.manageContent" />*
									</label>
								</div>
								<div class="col-xs-9">
									<g:select name="buttons"  from="${buttonsList}" optionKey="id" optionValue="${{it.name.encodeAsHTML()}}" required="required"
										onchange="${remoteFunction(
								            action:'ajaxAssignLocation', 
								            params:'\'buttonId=\' + escape(this.value)',
											update : 'infor')}"/>
								</div>
							</div>
							<span class="clearfix"></span>							
							
							<div class="col-xs-12 resize-formleft">
								<div class="col-xs-3">
								</div>
								<div class="col-xs-9">
									<div class="exxo-table-container-full-width" id="infor"></div>
								</div>
							</div>
							<span class="clearfix"></span>							
														
							<div class="col-xs-12 resize-formleft">
								<div class="col-xs-3">
								</div>
								<div class="col-xs-9">
									<input type="hidden" id="locationsToSave" name="locationsToSave" value=""/>
									<g:submitButton class="form-btn btn-primary" name="save" value="${message(code:'default.field.buttonSave')}" />					
									
									<input type="button" id="resetButton" name="resetButton" class="form-btn btn-primary" value="${message(code:'default.field.buttonReset')}"/>	
									
									<g:link action="index" controller="home">        
								       <span class="form-btn btn-primary">
								          ${message(code:'default.field.buttonCancel')}
								       </span>       
								    </g:link>
								</div>
							</div>
							<span class="clearfix"></span>
							</g:form>
							
							<!-- Synchronize Button -->
							<g:form action="syncronizeCustomButtons" name="createForm" id="createForm" class="exxo-form">
								<div class="col-xs-12 resize-formleft">
									<div class="col-xs-3">
									</div>
									<div class="col-xs-9">
										<input type="hidden" id="customButtonId" name="customButtonId" value=""/>
										<g:submitButton class="form-btn btn-primary" name="save" value="${message(code:'default.field.buttonSyncronize')}" />
									</div>
								</div>
								<span class="clearfix"></span>
							</g:form>
							
							<!-- Information with the status of every transaction -->
							<g:if test="${jsonTransactionStatus}" >
			         			<legend>
			         				<g:message code="default.title.statusSendCustomButton" />
								</legend>      			
			         			<table class='table table-striped'>      
			         				<thead>    				
				       					<tr>
				       						<th>${message(code:'default.field.serial')}</th>
				       						<th>${message(code:'default.field.status')}</th>
				       						<th>${message(code:'default.field.errorDescription')}</th>	          						
				       						<th>${message(code:'default.field.customButton')}</th>
				       						<!-- <th class="sorter">${message(code:'default.field.sendAgain')}</th> -->      						
				       					</tr>
			       					</thead>	
			       					<tbody> <!-- style="text-align='center'" -->
			          				<g:each in="${jsonTransactionStatus?.customButtonsStatus}" status="i" var="record">
			          					<tr id="${record.channelNum}"  class="${(i % 2) == 0 ? 'even' : 'odd'}">
			          						<td class="widthTdRow10">		          								
			          							${record.serialNumber}
			          						</td>
			          						<td class="widthTdRow15">		          							
			          							${record.successful}
			          						</td>
			          						<td class="widthTdRow40">
			          							${message(code:messageErrorDescriptionHash.get(record.errorCode))}
			          						</td>
			          						<td class="widthTdRow40">
			          							<ul>
			          								<g:each in="${record.customBottonIds}" var="buttonId">
														<il>${customButtonsHashInfo.get(buttonId)}&#44;&#32;</il>
			          								</g:each>
			          							</ul>
			          						</td>
			          					</tr>	
			          				</g:each>
			         				</tbody>
			         			</table>    
				          </g:if>
						</div> <!-- End of Content -->
					</div>
				</div>
			</div>
		</div>
	</section>
</body>
</html>