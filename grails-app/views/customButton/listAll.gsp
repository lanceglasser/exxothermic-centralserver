<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title><g:message code="default.title.listContent" /></title>
		<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables.css')}">
		<link rel="stylesheet" type="text/css" href="${resource(dir: 'themes/default/css', file: 'jquery.dataTables_themeroller.css')}">
		<script src="${resource(dir: 'themes/default/js', file: 'jquery.dataTables.js')}"></script>
		<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_table_list.js')}"></script>
		<script src="${resource(dir: 'js/pages/customButton', file: 'listAll.js')}"></script>  
		<script>
			Exxo.module = "content-list";
			Exxo.UI.translates['okButton'] = "${message(code:'default.field.buttonOk')}";
			Exxo.UI.translates['cancelButton'] = "${message(code:'default.field.buttonCancel')}";
			Exxo.UI.translates['disableMessage'] = "${message(code:'mycentralserver.custombuttons.CustomButton.isDisabled')}";
		</script>
		</head>		
	</head>
	<body>
		<section class="intro">
		  <div class="intro-body">
		    <div class="container full-width">
		      <div class="row">
		        <div class="col-md-12" id="internal-content">
		          <h1><g:message code="default.title.listContent" /></h1>
					<g:if test="${flash.error}">
			       		<div class="alert alert-error">  
						  <a class="close" data-dismiss="alert">×</a>  
						  ${flash.error} 
						  ${flash.error=null} 
						</div> 
					</g:if>
					
					<g:if test="${flash.success}">
			       		<div class="alert alert-success">  
						  <a class="close" data-dismiss="alert">×</a>  
						  ${flash.success} 
						  ${flash.success=null} 
						</div> 
			    	</g:if>
				<div class="content">
					<g:if test="${buttonsList.size()>0}">
		            <table id="table-list" class="display" cellspacing="0" width="100%">
		              <thead>
		                <tr>			     
					     <th>${message(code: 'default.field.contentName')}</th>
					     <th>${message(code: 'default.field.customeButtons.channelLabel')}</th>
					     <th>${message(code: 'default.field.customeButtons.channelContent')}</th>
					     <th>${message(code: 'default.field.enabled')}</th>							    							   				
					     <th>${message(code: 'default.table.action')}</th>		
		                </tr>
		              </thead>
		              <tfoot>
		                <tr>
		                 <th>${message(code: 'default.field.contentName')}</th>
					     <th>${message(code: 'default.field.customeButtons.channelLabel')}</th>
					     <th>${message(code: 'default.field.customeButtons.channelContent')}</th>
					     <th>${message(code: 'default.field.enabled')}</th>							    							   				
					     <th>${message(code: 'default.table.action')}</th>
		                </tr>
		              </tfoot>
		              <tbody>
		              	<g:each in="${buttonsList}" status="i" var="customButtonInstance">
							<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
								<td><g:link action="edit" id="${customButtonInstance.id}">${customButtonInstance.name.encodeAsHTML()}</g:link></td>
								<td>${customButtonInstance.channelLabel.encodeAsHTML()}</td>	
								<td>${customButtonInstance.channelContent}</td>
								<td>
									<g:if test="${customButtonInstance.enabled == true}">
										<g:message code="default.field.yes" />
									</g:if> 
									<g:else>
										<g:message code="default.field.no" />
									</g:else>							
								</td>											
								<td>	
									<g:link action="show" id="${customButtonInstance.id}" class="action-icons view" title="${message(code:'default.action.show')}"  params="[offset: offset]"></g:link>
									<g:link action="edit" id="${customButtonInstance.id}"  class="action-icons edit" title="${message(code:'default.action.edit')}"  params="[offset: offset]"></g:link>  
									<g:if test="${customButtonInstance.enabled}">
										<g:link action="assignLocation" id="${customButtonInstance.id}" buttonEnable="true" class="action-icons viewLocation" title="${message(code:'default.action.management')}"  params="[offset: offset]"></g:link>
									</g:if>	
									<g:else>
										<g:link action="assignLocation" id="${customButtonInstance.id}" buttonEnable="false" class="action-icons viewLocation-disable" title="${message(code:'default.action.management')}"  params="[offset: offset]"></g:link>
									</g:else>
								</td>
							</tr>
						</g:each>
		              </tbody>
		            </table>
		            </g:if>
		            
		            <!-- Information with the status of every transaction -->
					<g:if test="${jsonTransactionStatus}" >	          	
		         			<legend>
		         				<g:message code="default.title.statusSendCustomButton" />
							</legend>      					         			    
		         			<table id="table-list2" class="display" cellspacing="0" width="100%">     				
		       					<tr>		       			 
		       						<th>${message(code: 'default.field.serial')}</th>
								    <th>${message(code: 'default.field.status')}</th>
								    <th>${message(code: 'default.field.errorDescription')}</th>
								    <th>${message(code: 'default.field.customButton')}</th>							    							   											 	           					
		       					</tr>	
		       					<tbody style="text-align='center'">
			          				<g:each in="${jsonTransactionStatus?.customButtonsStatus}" var="record">
			          					<tr id="${record.channelNum}" >
			          						<td class="">${record.serialNumber}</td>
			          						<td class="">${record.successful}</td>
			          						<td class=""></td>
			          						<td class="">
			          							<ul>
			          								<g:each in="${record.customBottonIds}" var="buttonId">
														<li>${customButtonsHashInfo.get(buttonId)}&#44;&#32;</li>
			          								</g:each>
			          							</ul>
			          						</td>			          					
			          					</tr>	
			          				</g:each>
		         				</tbody>
		         			</table>	          			  			     	
		         	 </g:if>
		          </div>
		        </div>
		      </div>
		    </div>
		  </div>
		</section>	
    <div id="content-disable-dialog" title="${message(code:'mycentralserver.custombuttons.CustomButton.isDisabled.title')}">
	  <p>${message(code:'mycentralserver.custombuttons.CustomButton.isDisabled')}</p>
	</div>
	</body>
</html>