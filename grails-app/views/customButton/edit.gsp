<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.customeButtons.updateContent" /></title> 
<link rel="stylesheet" href="${resource(dir: 'css/pluggins/bootstrap-colorpicker', file: 'bootstrap-colorpicker.css')}" type="text/css">
<link rel="stylesheet" href="${resource(dir: 'css/jcrop', file: 'jquery.Jcrop.css')}" type="text/css">		
<link rel="stylesheet" href="${resource(dir: 'css/jcrop', file: 'demos.css')}" type="text/css">	
<script src="${resource(dir: 'js/pluggins/bootstrap-colorpicker', file: 'bootstrap-colorpicker.js')}"></script>
<script src="${resource(dir: 'js/pluggins/bootstrap-colorpicker', file: 'docs.js')}"></script>
<script src="${resource(dir: 'js/jcrop', file: 'jquery.Jcrop.js')}"></script>
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_custom_button_create.js')}"></script>
<script>
	Exxo.module = "custom-button-create";
	var dimensionsImageError = "${message(code:'customButton.image.dimension.error')}";
	var invalidImageError = "${message(code:'image.invalid.error')}";
	var tooBigImageError = "${message(code:'image.big.error')}";
</script>
        
<style type="text/css">
/* Apply these styles only when #preview-pane has
   been placed within the Jcrop widget */
.jcrop-holder .preview-pane {
  display: block;
  position: absolute;
  z-index: 2000;
  top: 10px;
  right: -240px;
  padding: 6px;
  border: 1px rgba(0,0,0,.4) solid;
  background-color: white;

  -webkit-border-radius: 6px;
  -moz-border-radius: 6px;
  border-radius: 6px;

  -webkit-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
}

/* The Javascript code will set the aspect ratio of the crop
   area based on the size of the thumbnail preview,
   specified here */
.preview-pane .preview-container {
  width: 150px;
  height: 120px;
  overflow: hidden;
}

</style>
</head>
<body>
<g:render template="/layouts/internalContentTemplate">
	<h1>
		<g:message code="default.title.customeButtons.updateContent" />
	</h1>
	
	<div class="content">
		<g:render template="/layouts/messagesAndErrorsTemplate" model="[entity: customButton]"/>
		<g:uploadForm action="update" name="updateForm" class="exxo-form" >
			<g:hiddenField name="id" value="${customButton?.id}" />
			<g:hiddenField name="version" value="${customButton?.version}" />
			<g:hiddenField name="offset" value="${offset}" />
			<!-- hidden crop params -->
	       	<input type="hidden" id="x" name="x" />
	       	<input type="hidden" id="y" name="y" />	        	 
	       	<input type="hidden" id="w" name="w" value="150"  />
			<input type="hidden" id="h" name="h" value="120"/>
			
			<!-- Left Column -->
			<div class="col-xs-6 resize-formleft">
				<!-- Content Name  -->				
				<custom:editField type="text" name="name" required="${true}" value="${customButton.name}" 
					label="default.field.contentName" />
													
				<!-- Channel Image -->
				<div class="col-xs-4 text-to-right">
					<label for="buttonImage"><g:message code="default.field.customeButtons.buttonImage" /></label>
				</div>
				<div class="col-xs-8 text-to-left">
					<div id="errorDiv" class="alert alert-error"  style="display:none;"></div>
					<g:if test="${customButton.imageURL}">
						<div id="demo" >
					</g:if>
					<g:else>
						<div id="demo" style="display:none;" >
					</g:else>
						<img id="target" style="width:300px; height:auto; padding: 5px ; border: 1px #bbb solid;" src="${customButton.imageURL}"/>							  
					  	<div  class="preview-pane" id="preview-pane" style="display:none;" >
						    <div class="preview-container" >											      
						      	<img  id="preview" class="jcrop-preview" src="${customButton.imageURL}" alt="Preview"  />
						    </div>
						</div>
					</div>
					<div id="uploads">					 
							<div class="fakeupload">
								<input class="input-xlarge" type="text"  id="fakeupload" name="fakeupload" >
							</div>
							<input type="file" onchange="fileSelectHandler($(this).val());" class="realupload" id="file" name="file" >								 							  							 
					</div>
				</div>
				<span class="clearfix"></span>
				<div class="col-xs-12 text-to-left">
					<div class="col-xs-4 text-to-right">
						<label for="myschedule"><g:message code="schedule.label" /></label>
					</div>
					<div class="col-xs-8 text-to-left">
					<g:if test="${customButton.schedule}">
						<custom:scheduler name="content" days="${customButton.schedule.days}" hours="${customButton.schedule.hours}" start="${customButton.schedule.startDate}" end="${customButton.schedule.endDate}"/>
					</g:if>
					<g:else>
						<custom:scheduler name="content"/>
					</g:else>
					</div>
				</div>
	
			</div>
			
			<!-- Right Column -->
			<div class="col-xs-6 resize-formright">
				<!-- Channel Label -->				
				<custom:editField type="text" name="channelLabel" required="${true}" value="${customButton.channelLabel}" 
					label="default.field.customeButtons.channelLabel" />
					
				<!-- Channel Color -->
				<div class="col-xs-4 text-to-right">
					<label for="channelColor">
						<g:message code="default.field.customeButtons.channelColor" />*
					</label>
				</div>
				<div class="col-xs-8 text-to-left">
					<div class="input-append color" data-color="${customButton.channelColor}" data-color-format="hex" id="cp3" data-colorpicker-guid="1">							
						<input type="text" maxlength="10" required="required" class="input-xlarge" name="channelColor" id="channelColor" value="${customButton.channelColor}">							
						<span class="add-on add-on-fix">
							<i class="input-color-selector-box" style="width: 33px; height: 28px; background-color: ${customButton.channelColor};">
							</i>
						</span>
					</div>
				</div>
				<span class="clearfix"></span>
				
				<!-- Channel Content -->			
				<custom:editField type="text" name="channelContent" required="${true}" value="${customButton.channelContent}" 
					label="default.field.customeButtons.channelContent" 
					note="${ message(code: 'default.field.example') + " " + message(code: 'default.customButtons.context.example') }"/>
									
				<!-- Enable -->
				<custom:editField type="checkbox" name="enabled" required="${false}" value="${customButton.enabled}" 
					label="default.field.enabled" cssclass="input-checkbox" tooltip="tooltip.customButton.enable"/>
			</div>
			<span class="clearfix"></span>
			
			<!-- Buttons -->
			<g:submitButton class="form-btn btn-primary" name="update" value="${message(code:'default.field.buttonSave')}" onClick="showSpinner();"/>
			
			<g:link action="listAll" controller="customButton" params="[offset: offset]">        
		       <span class="form-btn btn-primary">
		          ${message(code:'default.field.buttonCancel')}
		       </span>       
		    </g:link>
		</g:uploadForm>
	</div>
</g:render>
</body>
</html>