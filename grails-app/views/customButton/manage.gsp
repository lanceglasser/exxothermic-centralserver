<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title><g:message code="default.title.Location" /></title>
		<link rel="stylesheet" href="${resource(dir: 'css/pluggins/select2', file: 'select2.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'manageButtons.css')}" type="text/css">
		<script src="${resource(dir: 'js/pluggins/select2', file: 'select2.js')}"></script>		
		<script src="${resource(dir: 'js/pages/customButton', file: 'manageButtons.js')}"></script>
		<script>
			var selectedLocations = "${selectedLocations}";
		</script>
	</head>
	<body>
	 
          <legend><g:message code="default.title.assingLocationsToButton" /></legend>					
			

			<div class="prop">
				<div class="name">${customButton.name} - ${selectedLocations}</div>
				<div class="clear"></div>				
			</div>
			
			<div class="vertical-scroll height-200">
			<g:each var="company" in="${companies}">
				<g:if test="${company.locations.size() > 0}">
				     <div class="company">
						<div class="header">
							<label for="allCompany_${company.id}">
								<g:checkBox name="allCompany_${company.id}" class="cbCompany" companyId="${company.id}"  value="${false}" /> 
								${company}
							</label>
						</div>
						<div class="locationsContainer" id="locationContainer_${company.id}" name="locationContainer_${company.id}">
							<g:each var="location" in="${company.locations}">
								<div class="location">
								<label for="allCompany_${location.id}">
									<g:checkBox name="location_${location.id}" class="cbLocation cb_${company.id}" locationId="${location.id}" companyId="${company.id}"  value="${false}" /> 
									${location}
								</label>
							</div>
							</g:each>
						</div>		
					</div>  
				</g:if>
				
			</g:each> 
		  
		  	</div>
			<br>
			<div class="prop">
				<div class="name">&nbsp;</div>
				
				<g:form action="saveLocations" name="createForm" id="createForm" class="form-horizontal">
				<div class="form-actions" >
					<g:link action="listAll" controller="customButton">        
				       <span class="btn btn-primary">
				          ${message(code:'default.field.buttonCancel')}
				       </span>       
				    </g:link>
				    
				    <input type="button" id="resetButton" name="resetButton" class="btn btn-primary" value="${message(code:'default.field.buttonReset')}"/>	
				    
				    <g:submitButton class="btn btn-primary" name="save" value="${message(code:'default.field.buttonSave')}" />
				</div>	
				</g:form>			  			
			</div>	
	</body>
</html>

