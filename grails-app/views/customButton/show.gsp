<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main" />
<title><g:message code="default.title.updateSoftwareVersion" /></title>	
<script src="${resource(dir: 'themes/default/js/exxo', file: 'exxo_form_show.js')}"></script>
<script>
	Exxo.module = "software-show";
	$(document).ready(function() {
	   $("#internal-content").createShowForm({
			title: '${customButton.name.encodeAsHTML()}',
			columns: [
				[
					{useMainText: '${customButton.channelLabel != null}', label: '<g:message code="default.field.customeButtons.channelLabel" />', value: '${customButton.channelLabel?.encodeAsHTML()}'},
					{useMainText: '${customButton.channelColor != null}', label: '<g:message code="default.field.customeButtons.channelColor" />', value: '<i class="channel-color" style="background-color: ${customButton.channelColor}"></i>'},
					{useMainText: '${customButton.channelContent != null}', label: '<g:message code="default.field.customeButtons.channelContent" />', value: '${customButton.channelContent}'},
					{label: '<g:message code="default.field.enabled" />', value: '${customButton.enabled == true}', type: 'bool'}
				],
				[
					
				]
			],
			texts: {
				yes: 	'<g:message code="default.field.yes" />',
				no: 	'<g:message code="default.field.no" />',
				empty: 	'<g:message code="default.field.empty" />'
			},
			buttons: [
				'<g:link action="listAll" controller="customButton" params="[offset: offset]"> <input type="button" class="btn btn-primary" value="${message(code:'default.field.buttonBack')}" /> </g:link>'
			]
		   });
	});
</script>
</head>
<body>
	<section class="intro">
		<div class="intro-body">
			<div class="container full-width">
				<div class="row">
					<div class="col-md-12" id="internal-content">
					</div>
				</div>
			</div>
		</div>
	</section>
</body>
</html>