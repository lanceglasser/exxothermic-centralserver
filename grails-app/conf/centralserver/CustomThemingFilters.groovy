package centralserver

import mycentralserver.app.Affiliate
import mycentralserver.generaldomains.Configuration;
import mycentralserver.utils.Constants;
import mycentralserver.utils.PartnerEnum;

class CustomThemingFilters {

    def translatorService;

    def filters = {
        all(uri:'/**') {
            before = {
                String theme = Constants.DEFAULT_THEME;

                String domain = request.getHeader(Constants.HEADER_HOST)?.trim() ?: '';
                session.domain = domain;

                try {
                    Configuration config = Configuration.findByCode(Constants.BOX_CONF_CODE_MAINTENANCE_MSG);
                    session.maintenanceMsg = (config)? config.value:null;

                    def affiliates = Affiliate.getAffiliateForDomain(domain).list();
                    Affiliate affiliate =  null;
                    if(affiliates.size() > 0){
                        affiliate = affiliates[0];
                    }

                    if ( affiliate ) {
                        theme = affiliate.theme;
                        session.affiliate = affiliate;
                    } else {
                        Affiliate myeAffiliate =  Affiliate.findByCode(PartnerEnum.MYE.getCode());
                        session.affiliate = myeAffiliate;
                        theme = myeAffiliate.theme;
                    }
                } catch(Exception ex) {
                    // General exception keep it for grails unexpected exceptions
                    ex.printStackTrace();
                }

                session.theme = theme;
            }

            after = { model ->
            }
        }
        all(controller:'*', action:'*') {
            before = {
            }
            after = { Map model ->
                response.setHeader("Cache-control", "no-cache, no-store");
                response.setHeader("Pragma", "no-cache");
                response.setHeader("Expires", "-1");
            }
            afterView = { Exception e ->
            }
        }
    }
}