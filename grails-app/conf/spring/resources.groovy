 
 
import org.springframework.security.core.session.SessionRegistryImpl
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.security.web.authentication.session.ConcurrentSessionControlStrategy
import org.springframework.security.web.session.ConcurrentSessionFilter

beans = {
  sessionRegistry(SessionRegistryImpl)

  concurrencyFilter(ConcurrentSessionFilter) {
	sessionRegistry = sessionRegistry
	logoutHandlers = [ref("rememberMeServices"), ref("securityContextLogoutHandler")]
	expiredUrl='/login/concurrentSession'
  }
  concurrentSessionControlStrategy(ConcurrentSessionControlStrategy, sessionRegistry) {
	alwaysCreateSession = true
	exceptionIfMaximumExceeded = false
	maximumSessions = 1
  }
  // Better not to override the bean but to just set the strategy in Bootstrap.groovy (Details  below)
  /*
  authenticationProcessingFilter(UsernamePasswordAuthenticationFilter) {
	sessionAuthenticationStrategy=concurrentSessionControlStrategy
	authenticationManager=ref("authenticationManager")
  }
  */
}
 

