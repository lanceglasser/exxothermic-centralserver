dataSource {
    pooled = true
    driverClassName = "org.h2.Driver"
    username = "sa"
    password = ""
	logSql = false
}
hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
}
// environment specific settings
environments {
    development {
        dataSource {
            //dbCreate = "create-drop" // one of 'create', 'create-drop', 'update', 'validate', ''
            //url = "jdbc:h2:mem:devDb;MVCC=TRUE;LOCK_TIMEOUT=10000"
			driverClassName = "com.mysql.jdbc.Driver"
			dialect = "org.hibernate.dialect.MySQL5InnoDBDialect"
			dbCreate = "update" // one of 'create', 'create-drop', 'update', 'validate', ''			
			url = "jdbc:mysql://localhost/mycentralserver4?useUnicode=yes&characterEncoding=UTF-8"
			username = "root"
			password = "root"
			//url = "jdbc:mysql://184.106.136.95:3306/exxo_staging?useUnicode=yes&characterEncoding=UTF-8"
			//username = "cserver"
			//password = "Rz2ExaKvpQz751L"
        }
    }
	staging {
		dataSource {			
			driverClassName = "com.mysql.jdbc.Driver"
			dialect = "org.hibernate.dialect.MySQL5InnoDBDialect"
			dbCreate = "update" // one of 'create', 'create-drop', 'update', 'validate', ''
			url = "jdbc:mysql://localhost/exxo_staging?useUnicode=yes&characterEncoding=UTF-8"
			username = "root"
			password = "rk73MGT8G46A3"
		}
	}
    test {
        dataSource {
            dbCreate = "update"
			url = "jdbc:mysql://localhost/mycentralserver?useUnicode=yes&characterEncoding=UTF-8"
			username = "root"
			password = "root"
        }
    }
    production {
        dataSource {
            dbCreate = "update"
			driverClassName = "com.mysql.jdbc.Driver"
			dialect = "org.hibernate.dialect.MySQL5InnoDBDialect"
            url = "jdbc:mysql://localhost/exxothermic_central?useUnicode=yes&characterEncoding=UTF-8"
			username = "cserver"
			password = "Rz2ExaKvpQz751L"

			
            pooled = true
            properties {
               maxActive = -1
               minEvictableIdleTimeMillis=1800000
               timeBetweenEvictionRunsMillis=1800000
               numTestsPerEvictionRun=3
               testOnBorrow=true
               testWhileIdle=true
               testOnReturn=true
               validationQuery="SELECT 1"
            }
        }
    }
}
