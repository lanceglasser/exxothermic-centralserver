/**
 * Global URL Mapping definition of the Application
 * 
 * @author Cecropia Solutions
 * @since 08/18/2014
 */
class UrlMappings {
    static mappings = {
        "/$controller/$action?/$id?"{ 
            constraints { 
                // apply constraints here
            } 
        }
        "/api/box/validate"(controller:"api", action:"validateBoxConnection")
        "/"(controller:"home")
        "500"(controller:"error")
        "404"(controller:"error", action:"notfound")
        "403"(controller:"error", action:"notauthorized")
    }
}