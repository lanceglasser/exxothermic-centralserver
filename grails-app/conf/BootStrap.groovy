
import java.awt.GraphicsConfiguration.DefaultBufferCapabilities;
import java.util.Date;

import mycentralserver.box.Box
import mycentralserver.box.BoxDeviceType
import mycentralserver.box.BoxManufacturer
import mycentralserver.box.BoxCertificate;
import mycentralserver.box.BoxStatus;
import mycentralserver.box.BoxUnregistered
import mycentralserver.box.software.BoxSoftware;
import mycentralserver.generaldomains.*;
import mycentralserver.user.*;
import mycentralserver.company.Company
import mycentralserver.company.CompanyCatalogSubType;
import mycentralserver.company.CompanyCatalogType;
import mycentralserver.company.CompanyLocation
import org.codehaus.groovy.grails.plugins.springsecurity.*;
//import org.codehaus.groovy.grails.plugins.springsecurity.SecurityFilterPosition

class BootStrap {
	def springSecurityService
	//manage 1 login per user
	def authenticationManager
	def concurrentSessionController
	def securityContextPersistenceFilter
	def userService

	def init = { ServletContext->

		TimeZone.setDefault(TimeZone.getTimeZone("UTC"))

		SpringSecurityUtils.clientRegisterFilter('concurrencyFilter', SecurityFilterPosition.CONCURRENT_SESSION_FILTER)

		if (Role.count() ==0) {

			new Role(authority:"ROLE_OWNER", description:"Owner").save()
			new Role(authority:"ROLE_OWNER_STAFF", description:"Owner Employee").save()
			new Role(authority:"ROLE_ADMIN", description:"Administrator").save()
			new Role(authority:"ROLE_INTEGRATOR", description:"Integrator").save()
			new Role(authority:"ROLE_INTEGRATOR_STAFF", description:"Integrator Employee").save()
			new Role(authority:"ROLE_HELP_DESK", description:"Help Desk").save()
			
			new CompanyType(name:"Integrator").save()
			new CompanyType(name:"Owner").save()

			def roleAdmin=Role.findByAuthority("ROLE_ADMIN")?:new Role(authority:"ROLE_ADMIN").save()
			def user=User.findByEmail("admin@exxothermic.com")
			if (!user){

				def dateExpirationPass = userService.getPassswordExpirationDateForNewAccount()
				user=new User(email:'admin@exxothermic.com',firstName:'Admin',lastName:'User',enabled:true,password:'Password1', passwordExpirationDate:dateExpirationPass)

				if(user.validate())
				{
					log.info "creating admin user"
					user.save(flush:true)
					UserRole.create user,roleAdmin
				}
			}

			createCompanyCatalogTypes()
			//createCountryAndState()
			createDefaultUserCompanyAndLocation()
			createUsers()
			createBoxStatus()
			createTestCompanyAndLocation()
			createTestUnregisteredBoxes()
			createSoftwareVersionUnsupported()
			createManufacturer()
			createBoxCertifications()
		}

	}


	private createTestCompanyAndLocation()
	{

	}

	private createDefaultUserCompanyAndLocation()
	{

		def roleHelpDesk=Role.findByAuthority("ROLE_HELP_DESK")?:new Role(authority:"ROLE_HELP_DESK").save()
		def user=User.findByEmail("helpdesk@exxothermic.com")

		if(!user)
		{
			def dateExpirationPass = userService.getPasswordExpirationDateForExistAccount()
			user=new User(email:'helpdesk@exxothermic.com',firstName:'Help',lastName:'Desk',enabled:true,password:'Password1', passwordExpirationDate:dateExpirationPass)

			if(user.validate())
			{
				log.info "creating help user"
				user.save(flush:true)
				UserRole.create user,roleHelpDesk
			}
			else
			{
				log.info ("\n\n\n\n Error with the master account boostrap")
				user.errors.each{err-> log.info err }
			}
		}

		def exxo=new Company(id:0,name:"exxothermic",owner:user,enable:true,address:"Exxothermic direction",webSite:"http://wwww.exxothermic.com", phoneNumber:"+44 1234-1234")
		def type=CompanyCatalogSubType.findById (1)
		def typeOfCompany= CompanyType.findByName("Owner")
		exxo.type=type
		exxo.typeOfCompany=typeOfCompany
		def state=State.findById(1)
		exxo.taxId="01-1234567"
		exxo.state=state
		exxo.city="Area 51"
		exxo.createdBy=user


		exxo.zipCode="50611"
		if (!Company.findById(1))
		{
			if(exxo.validate()  )
			{

				log.info "		creating company admin user"
				exxo.save(flush:true)

				def location= new CompanyLocation()
				location.name="Dark Moon"
				location.address="Next to the force"

				location.type=type
				location.city="Area 51"
				location.company=exxo
				location.state=state
				//by default the administrator of the location will be the owner of the company
				location.administrator=user
				location.createdBy=user

				if(location.validate())
				{
					location.save(flush:true)
					log.info "			creating location admin user"

				}
				else
				{
					log.info ("\n\n\n\n Error with the master location boostrap")
					location.errors.each{err-> log.info err }
				}
			}
			else
			{
				log.info ("\n\n\n\n Error with the master company boostrap")
				exxo.errors.each{err-> log.info err }
			}
		}
	}

	private createTestUnregisteredBoxes()
	{
		def boxSerialList=[
			"123asd12312",
			"zdfsew324assa342",
			"qwertyuio",
			"asdfghjkl",
			"zxcvbbnm"
		]
		def now=new Date()
		boxSerialList.each { serial->
			if (!BoxUnregistered.findBySerial(serial))
			{
				def box=new BoxUnregistered();
				box.serial= serial
				box.date=now

				if(box.save())
				{
					log.info "creating  unregistered box  ${serial}"
				}
				else
				{
					log.info "problem with  unregistered box  ${serial}"
				}
			}
		}


	}


	private createBoxStatus()
	{
		def boxStatusList=[
			"Connected",
			"Registered",
			"Error",
			"Disconnected",
			"Updating"
		]
		int idx=0
		boxStatusList.each { name->
			def box=new BoxStatus(id:idx,description:name)
			box.id = idx;
			idx++;
			log.info "creating box status  ${name} "+idx+" "+box.id
			box.save(flush:true)
		}
	}

	private createCountryAndState()
	{
		def USA=[
			'AL',
			'AK',
			'AZ',
			'AR',
			'CA',
			'CO',
			'CT',
			'DE',
			'FL',
			'GA',
			'HI',
			'ID',
			'IL',
			'IN',
			'IA',
			'KS',
			'KY',
			'LA',
			'ME',
			'MD',
			'MA',
			'MI',
			'MN',
			'MS',
			'MO',
			'MT',
			'NE',
			'NV',
			'NH',
			'NJ',
			'NM',
			'NY',
			'NC',
			'ND',
			'OH',
			'OK',
			'OR',
			'PA',
			'RI',
			'SC',
			'SD',
			'TN',
			'TX',
			'UT',
			'VT',
			'VA',
			'WA',
			'WV',
			'WI',
			'WY'
		]

		def CANADA= [
			'Ontario',
			'Quebec',
			'BritishColumbia',
			'Alberta',
			'Manitoba',
			'Saskatchewan',
			'NovaScotia',
			'NewBrunswick',
			'Newfoundland',
			'PrinceEdwardIsland',
			'NorthwestTerritories',
			'Yukon',
			'Nunavut'
		]
		def countryList=['United States of America':[shortName:'USA',statesList:USA],'Canada':[shortName:'CAN',statesList:CANADA]]
		def countries=Country.list()?:[]
		if(!countries)
		{
			int countryId=0
			int stateId=0
			int cityId=0
			countryList.each {name, data->
				def c=new Country(id:countryId,name:name,shortName:data.shortName)
				countryId++
				if(c.validate())
				{
					log.info "creating country   ${name}"
					c.save(flush:true)
					data.statesList.each{ stateName->

						def state=new State(id:stateId,name:stateName,country:c)
						stateId++
						if(state.validate())
						{
							log.info "          creating state   ${stateName}"
							state.save(flush:true)

						}
						else
						{
							log.info ("\n\n\n\n Error with the state boostrap for ${name}")
							c.errors.each{err-> log.info err }
						}

					}
				}
				else
				{
					log.info ("\n\n\n\n Error with the country boostrap for ${name}")
					c.errors.each{err-> log.info err }
				}
			}
		}



	}



	private createUsers()
	{
		def usersTest= [
			'test1@test.com':[firstName:'Ramiro',lastName:'Solano'],
			'test2@test.com':[firstName:'Rolando',lastName:'Solano'],
			'test3@test.com':[firstName:'Ramira',lastName:'Solano'],
			'test4@test.com':[firstName:'Romulo',lastName:'Solano']
		]

		def dateExpirationPass 	= userService.getPasswordExpirationDateForExistAccount()
		def roleOwner			= Role.findByAuthority("ROLE_OWNER")?:new Role(authority:"ROLE_OWNER").save()

		usersTest.each{ email,profileAttrs->
			def user=new User(
					email:email,
					password:'password',
					passwordExpirationDate:dateExpirationPass,
					firstName:profileAttrs.firstName,
					lastName:profileAttrs.lastName,
					enabled:true)

			if (!User.findByEmail(email))
			{
				if(user.validate()  )
				{

					log.info "creating user with email ${email}"
					user.save(flush:true)
					UserRole.create user,roleOwner

				}
				else
				{
					log.info ("\n\n\n\n Error with the account boostrap for ${email}")
					user.errors.each{err-> log.info err }
				}
			}
		}

	}

	private createManufacturer()
	{
		//Create Catalog Categories
		def catalogCategoriesList= [
			"BRANDS",
			"MEMORY",
			"MEMORY_TYPES"
		]
		catalogCategoriesList.each { name->
			def category=new CatalogCategory(name:name, dateCreated:new Date(),createdBy:User.findByEmail('admin@exxothermic.com'))

			if(category.validate()  )
			{
				category.save(flush:true)
				log.info "creating Catalog Category status  ${name} "+" "+category.id
			}else
			{
				log.info ("\n\n\n\n Error with the Catalog Category boostrap for ${name}")
				category.errors.each{err-> log.info err }
			}
		}//End Create Catalog Categories

		//Create Catalog
		def catalogList= [
			'Brand1':[dateCreated:new Date(),catalogCategory:CatalogCategory.findById(1)],
			'Brand2':[dateCreated:new Date(),catalogCategory:CatalogCategory.findById(1)],
			'1GB':[dateCreated:new Date(),catalogCategory:CatalogCategory.findById(2)],
			'2GB':[dateCreated:new Date(),catalogCategory:CatalogCategory.findById(2)],
			'512MB':[dateCreated:new Date(),catalogCategory:CatalogCategory.findById(2)],
			'1.5GB':[dateCreated:new Date(),catalogCategory:CatalogCategory.findById(2)],
			'DDR2':[dateCreated:new Date(),catalogCategory:CatalogCategory.findById(3)],
			'DDR3':[dateCreated:new Date(),catalogCategory:CatalogCategory.findById(3)]
		]
		catalogList.each { value, profileAttrs->
			def catalog=new Catalog(value:value, dateCreated:profileAttrs.dateCreated,catalogCategory:profileAttrs.catalogCategory,
									createdBy:User.findByEmail('admin@exxothermic.com'))

			if(catalog.validate()  )
			{
				catalog.save(flush:true)
				log.info "creating Catalog status  ${value} "+" "+catalog.id
			}else
			{
				log.info ("\n\n\n\n Error with the Catalog boostrap for ${value}")
				catalog.errors.each{err-> log.info err }
			}
		}//End Create Catalog

		//Create Device Type
		def deviceTypeList= [
			'Name1':[dateCreated:new Date(),machineModel:'Model1',machineBrand:Catalog.findById(1),processorBrand:Catalog.findById(2),memory:Catalog.findById(4),memoryType:Catalog.findById(8)],
			'Name2':[dateCreated:new Date(),machineModel:'Model2',machineBrand:Catalog.findById(2),processorBrand:Catalog.findById(1),memory:Catalog.findById(4),memoryType:Catalog.findById(8)],
			'Name3':[dateCreated:new Date(),machineModel:'Model3',machineBrand:null,processorBrand:null,memory:null,memoryType:null],
			'Name4':[dateCreated:new Date(),machineModel:'Model4',machineBrand:null,processorBrand:null,memory:null,memoryType:null]
		]
		deviceTypeList.each { name, profileAttrs->
			def deviceType=new BoxDeviceType(name:name, dateCreated:profileAttrs.dateCreated,machineModel:profileAttrs.machineModel,
			machineBrand:profileAttrs.machineBrand,processorBrand:profileAttrs.processorBrand,
			memory:profileAttrs.memory,memoryType:profileAttrs.memoryType,createdBy:User.findByEmail('admin@exxothermic.com'))
			if(deviceType.validate()  )
			{
				log.info "creating Box Device Type status  ${name} "+" "+deviceType.id
				deviceType.save(flush:true)
			}else
			{
				log.info ("\n\n\n\n Error with the Box Device Type boostrap for ${name}")
				deviceType.errors.each{err-> log.info err }
			}
		}//End Device Type

		//Create Box Manufacturer
		def boxManufacturerList= [
			'serialNumber':[dateCreated:new Date(),certificateLocation:'location1',certificateFileName:'FileName1',physicalSerial:"physicalSerial1",deviceType:BoxDeviceType.findById(1)],
			'serialNumber1':[dateCreated:new Date(),certificateLocation:'location2',certificateFileName:'FileName2',physicalSerial:null,deviceType:null]
		]
		boxManufacturerList.each { serialNumber, profileAttrs->
			def boxManufacturer=new BoxManufacturer(serialNumber:serialNumber, dateCreated:profileAttrs.dateCreated,certificateLocation:profileAttrs.certificateLocation,
			certificateFileName:profileAttrs.certificateFileName,physicalSerial:profileAttrs.physicalSerial,
			deviceType:profileAttrs.deviceType,createdBy:User.findByEmail('admin@exxothermic.com'))
			if(boxManufacturer.validate()  )
			{
				boxManufacturer.save(flush:true)
				log.info "creating Box Manufacturer status  ${serialNumber} "+" "+boxManufacturer.id
			}else
			{
				log.info ("\n\n\n\n Error with the Box Manufacturer boostrap for ${serialNumber}")
				boxManufacturer.errors.each{err-> log.info err }
			}
		}//End Box Manufacturer

	}// End createManufacturer

	private createCompanyCatalogTypes()
	{
		def companyTypes=CompanyCatalogType.list()?:[]
		if (!companyTypes)
		{
			def companyTypeList=[
				'Bar': 	  [subTypes:['Sports Bar'], code:'sportsBar'],
				'Restaurant':[subTypes:['Restaurant'], code:'restaurant'],
				'Fitness Center':[subTypes:['Fitness Center'],code:'fitnessCenter'],
				'Airport':	  [subTypes:['Airport'],code:'airport'],
				'Hotel':	  [subTypes:[
						'Bar',
						'Restaurant',
						'Fitness center'
					],code:'airport'],
				'Hospital':  [subTypes:['Hospital'],code:'hospital'],
				'Back of Hall':[subTypes:['Back of Hall'],code:'hall'],
				'Museum':	[subTypes:['Museum'],code:'museum'],
				'Transit':	[subTypes:[
						'Bus',
						'Train',
						'Airplane',
						'Waiting room'
					],code:'transit'],
				'Education':[subTypes:['Education'],code:'education'],
				'Distance learning':[subTypes:['Distance learning'],code:'distanceLearning'],
				'Mall':	[subTypes:['Mall'],code:'mall'],
				'Digital signage':	[subTypes:['Digital signage'],code:'digitalSignage'],
				'Waiting room':	[subTypes:['Waiting room'],code:'waitingRoom'],
				'Other':			[subTypes:['Other'],code:'other']
			]


			companyTypeList.each{name,data->

				def subType = data.subTypes
				def code  = data.code
				def companyType = new CompanyCatalogType(name:name,description:name, code:code)

				if(companyType.validate())
				{
					log.info "creating company type  ${name}"
					companyType.save(flush:true)


					subType.each {nameSubType->
						def companysubType=new CompanyCatalogSubType(name:nameSubType,description:nameSubType,type:companyType, code:code)

						if(companysubType.validate())
						{
							log.info "creating company sub type  ${nameSubType}"
							companysubType.save(flush:true)

						}
						else
						{
							log.info ("\n\n\n\n Error with the company type boostrap for ${name}")
							companysubType.errors.each{err-> log.info err }
						}
					}
				}
				else
				{
					log.info ("\n\n\n\n Error with the company type boostrap for ${name}")
					companyType.errors.each{err-> log.info err }
				}

			}
		}
	}

	/** this method insert the Software Version unsupported in mycentralserver
	 * 
	 * @return
	 */

	private createSoftwareVersionUnsupported() {
		print "[START] createSoftwareVersionUnsupported"
		BoxSoftware boxSoftware
		def user = getAdminUser()
		print "createSoftwareVersionUnsupported getAnakinUser"
		int versionSoftware = 1
		def boxSoftwareVersionLabelList=[
			"0.80",
			"0.81",
			"0.82",
			"0.83",
			"0.84",
			"0.85",
			"0.86",
			"0.87",
			"0.88",
			"0.89",
			"0.90"
		]
		boxSoftwareVersionLabelList.each { versionLabel->

			if (!BoxSoftware.findByVersionSoftwareLabel(versionLabel)) {
				boxSoftware = new BoxSoftware(	name: versionLabel,
				versionSoftware:versionSoftware, 			//unique
				versionSoftwareLabel:versionLabel,			//unique
				description:( versionLabel +" unsupported"),
				fileName:"-" + versionSoftware + "-", //unique
				fileNameOrigin:"--",
				pathLocation:"---",
				checksum:"--",
				enable:true,
				isSupported:false,
				createdBy:user,
				lastUpdatedBy:user)
				if(boxSoftware.validate()) {
					boxSoftware.save()
					log.info " Created the unsupported software version " + versionLabel
				} else {
					log.error ("\n\n\n\n Error unsupported software version" + versionLabel)
					boxSoftware.errors.each{err-> log.info err }
				}
			}
			versionSoftware++
		}
		print "[END] createSoftwareVersionUnsupported"
	} // END createSoftwareVersionUnsupported
	
	private createBoxCertifications() {
		
		def certifications = [
			"x509":[ certificationVersion: "v3", description:"certification type x.509 version 3", encryption:"MD5WithRSAEncryption", label:"x509v3"],			
			]
		BoxCertificate certification
		
		certifications.each { type , attribute  ->
			certification = new BoxCertificate( type: type, 
												   certificateVersion: attribute.certificationVersion, 
												   description: attribute.description,
												   encryption: attribute.encryption,
												   label: attribute.label);
			if(certification.validate()) {
				certification.save();
			} else {
				log.error ("\n\n\n\n Error created the certification version" )
				certification.errors.each{err-> log.info err }
			}
		}
	} // END createBoxCertifications
	
	private getAdminUser() {
		return User.findByEmail("admin@exxothermic.com")
	}

	def destroy = {
	}
}
