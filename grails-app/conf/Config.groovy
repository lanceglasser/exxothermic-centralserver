import org.apache.log4j.*;

// locations to search for config files that get merged into the main config;
// config files can be ConfigSlurper scripts, Java properties files, or classes
// in the classpath in ConfigSlurper format

// grails.config.locations = [ "classpath:${appName}-config.properties",
//                             "classpath:${appName}-config.groovy",
//                             "file:${userHome}/.grails/${appName}-config.properties",
//                             "file:${userHome}/.grails/${appName}-config.groovy"]

// if (System.properties["${appName}.config.location"]) {
//    grails.config.locations << "file:" + System.properties["${appName}.config.location"]
// }

grails.project.groupId = appName // change this to alter the default package name and Maven publishing destination
grails.mime.file.extensions = true // enables the parsing of file extensions from URLs into the request format
grails.mime.use.accept.header = false
grails.mime.types = [
	all:           '*/*',
	atom:          'application/atom+xml',
	css:           'text/css',
	csv:           'text/csv',
	html:          [
		'text/html',
		'application/xhtml+xml'
	],
	js:            'text/javascript',
	json:          [
		'application/json',
		'text/json'
	],
	multipartForm: 'multipart/form-data',
	rss:           'application/rss+xml',
	text:          'text/plain',
	xml:           [
		'text/xml',
		'application/xml']
]

// URL Mapping Cache Max Size, defaults to 5000
//grails.urlmapping.cache.maxsize = 1000

// What URL patterns should be processed by the resources plugin
grails.resources.adhoc.patterns = [
	'/images/*',
	'/css/*',
	'/js/*',
	'/plugins/*'
]

// The default codec used to encode data with ${}
grails.views.default.codec = "none" // none, html, base64
grails.views.gsp.encoding = "UTF-8"
grails.converters.encoding = "UTF-8"
// enable Sitemesh preprocessing of GSP pages
grails.views.gsp.sitemesh.preprocess = true
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []
// whether to disable processing of multi part requests
grails.web.disable.multipart=false

// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']



// configure auto-caching of queries by default (if false you can cache individual queries with 'cache: true')
grails.hibernate.cache.queries = false
// define the legth of auto generated offers code.
offersCodesLetterLength = 3
offersCodesNumberLength = 2
// fb authentition URLs
URLfacebookAuthApp  = "https://graph.facebook.com/app?access_token="
URLfacebookAuthMe 	= "https://graph.facebook.com/me?access_token="
facebookAppId		= "816354635081530"
appName				= "AudioEverywhere"

environments {

    development {
        grails.logging.jul.usebridge = true
        restUrlComunication= "http://localhost:8080/central-server/RestService/"
        //restUrlComunication= "http://184.106.136.95/central-server/RestService/"
        application.path.base = "mycentralserver"
        baseUrlMyBoxSoftwareUpdates= "http://localhost/mycentralserver/file/downloadSoftwareVersion?id="
        basePathMyBoxSoftwareUpdate = "C:\\cecropiaSolutions\\softwareVersions\\"
        basePathMyAppSkinImageFiles = "/usr/share/skinImages/"
        basePathKeys = "C:\\keys\\"
        basePathCustomButtonTempFiles = "/usr/share/customButtonTemp"
        basePathCustomButtonImagesFiles = "/usr/share/customButtonImages"
        basePathContentTempFiles = "temp"
        basePathContentImagesFiles = "softwareVersions"
        basePathChannelTempFiles = "usr/share/channelTemp"
        basePathChannelImagesFiles = "usr/share/channelImages"
        rackspaceUser= "laglasser"
        rackspaceImageContainer = "StagingImagesContainer"
        rackspaceDocumentContainer = "StagingDocumentsContainer"
        apiKey= "e28ad30648c3d8ecc8fa920e612f1afa"
        basePathMyBoxLogFiles ="C:\\cecropiaSolutions\\myBox\\logs\\"
        basePathImageFiles = "C:\\cecropiaSolutions\\ExXtractorImages\\"
        basePathMyBoxMetricsFiles = "C:\\cecropiaSolutions\\myBox\\metrics\\"
        basePathPublicKeyFiles  = "/usr/share/apache-tomcat-7.0.37/publicKeyFiles/"
        urlServiceUploadLogFile = "http://localhost/mycentralserver/gears/uploadLogFile"
        urlServiceUploadMetricsFile = "http://localhost/mycentralserver/boxMetrics/uploadFile"
        timeToWait = [3, 5, 8, 14, 15]
        passwordValidDays=60
        encryptionKey="23E54ACAB9031BEB1321FFD3125FBA1AB0000000000000000000000000000000"
        encryptionIv="2354ACABBB1321FFD100000000000000"
        grails.mail.default.from="Support Central Server <selimdiaz@gmail.com>"
        maxDateForValidations="12/31/2020"
        // This is the ID of the Venue on Database and must be set after the creation of the record
        wifiAudioVenueId = 69
    }
    devserver {
        grails.logging.jul.usebridge = true
        restUrlComunication= "http://localhost:8080/central-server/RestService/"
        application.path.base = "mycentralserver"
        baseUrlMyBoxSoftwareUpdates= "http://172.28.0.109/mycentralserver/file/downloadSoftwareVersion?id="
        basePathMyBoxSoftwareUpdate = "/usr/share/uploadFiles/"
        basePathMyAppSkinImageFiles = "/usr/share/skinImages/"
        basePathKeys = "/usr/share/keys/"
        basePathCustomButtonTempFiles = "/usr/share/customButtonTemp"
        basePathCustomButtonImagesFiles = "/usr/share/customButtonImages"
        basePathContentTempFiles = "/usr/share/contentTemp"
        basePathContentImagesFiles = "/usr/share/contentImages"
        basePathChannelTempFiles = "usr/share/channelTemp"
        basePathChannelImagesFiles = "usr/share/channelImages"
        rackspaceUser= "laglasser"
        rackspaceImageContainer = "StagingImagesContainer"
        rackspaceDocumentContainer = "StagingDocumentsContainer"
        apiKey= "e28ad30648c3d8ecc8fa920e612f1afa"
        basePathImageFiles = "/usr/share/ExXtractorImages/"
        basePathMyBoxLogFiles ="/uploadFiles/myBox/logs/"
        basePathMyBoxMetricsFiles = "/uploadFiles/myBox/metrics/"
        basePathPublicKeyFiles  = "/var/lib/tomcat7/publicKeyFiles/"
        urlServiceUploadLogFile = "http://172.28.0.109/mycentralserver/gears/uploadLogFile"
        urlServiceUploadMetricsFile = "http://172.28.0.109/mycentralserver/boxMetrics/uploadFile"
        timeToWait = [3, 5, 8, 14, 15]
        passwordValidDays=60
        encryptionKey="23E54ACAB9031BEB1321FFD3125FBA1AB0000000000000000000000000000000"
        encryptionIv="2354ACABBB1321FFD100000000000000"
        grails.mail.default.from="Support Central Server <support@audioeverywhere.com>"
        maxDateForValidations="12/31/2020"
        // This is the ID of the Venue on Database and must be set after the creation of the record
        wifiAudioVenueId = 69
    }
    staging {
        grails.logging.jul.usebridge = true
        restUrlComunication= "http://184.106.136.95/central-server/RestService/"
        application.path.base = "mycentralserver"
        baseUrlMyBoxSoftwareUpdates= "http://184.106.136.95/mycentralserver/file/downloadSoftwareVersion?id="
        basePathMyBoxSoftwareUpdate = "/usr/share/uploadFiles/"
        basePathMyAppSkinImageFiles = "/usr/share/skinImages/"
        basePathKeys = "/usr/share/keys/"
        basePathCustomButtonTempFiles = "/usr/share/customButtonTemp"
        basePathCustomButtonImagesFiles = "/usr/share/customButtonImages"
        basePathContentTempFiles = "/usr/share/contentTemp"
        basePathContentImagesFiles = "/usr/share/contentImages"
        basePathChannelTempFiles = "usr/share/channelTemp"
        basePathChannelImagesFiles = "usr/share/channelImages"
        rackspaceUser= "laglasser"
        rackspaceImageContainer = "StagingImagesContainer"
        rackspaceDocumentContainer = "StagingDocumentsContainer"
        apiKey= "e28ad30648c3d8ecc8fa920e612f1afa"
        basePathImageFiles = "/usr/share/ExXtractorImages/"
        basePathMyBoxLogFiles ="/uploadFiles/myBox/logs/"
        basePathMyBoxMetricsFiles = "/uploadFiles/myBox/metrics/"
        basePathPublicKeyFiles  = "/usr/share/apache-tomcat-7.0.37/publicKeyFiles/"
        urlServiceUploadLogFile = "http://184.106.136.95/mycentralserver/gears/uploadLogFile"
        urlServiceUploadMetricsFile = "http://184.106.136.95/mycentralserver/boxMetrics/uploadFile"
        timeToWait = [3, 5, 8, 14, 15]
        passwordValidDays=60
        encryptionKey="23E54ACAB9031BEB1321FFD3125FBA1AB0000000000000000000000000000000"
        encryptionIv="2354ACABBB1321FFD100000000000000"
        grails.mail.default.from="Support Central Server <support@audioeverywhere.com>"
        maxDateForValidations="12/31/2020"
        // This is the ID of the Venue on Database and must be set after the creation of the record
        wifiAudioVenueId = 83
    }
    production {
        grails.logging.jul.usebridge = false
        restUrlComunication= "http://gears.exxothermic.com/RestService/"
        application.path.base = ""
        baseUrlMyBoxSoftwareUpdates= "http://services.exxothermic.com/file/downloadSoftwareVersion?id="
        basePathMyBoxSoftwareUpdate = "/usr/share/uploadFiles/"
        basePathMyAppSkinImageFiles = "/usr/share/skinImages/"
        basePathKeys = "/usr/share/keys/"
        basePathCustomButtonTempFiles = "/usr/share/customButtonTemp"
        basePathCustomButtonImagesFiles = "/usr/share/customButtonImages"
        basePathContentTempFiles = "/usr/share/contentTemp"
        basePathContentImagesFiles = "/usr/share/contentImages"
        basePathChannelTempFiles = "usr/share/channelTemp"
        basePathChannelImagesFiles = "usr/share/channelImages"
        rackspaceUser= "laglasser"
        rackspaceImageContainer = "ImageContainer"
        rackspaceDocumentContainer = "ProdDocumentsContainer"
        apiKey= "e28ad30648c3d8ecc8fa920e612f1afa"
        basePathImageFiles = "/usr/share/ExXtractorImages/"
        basePathMyBoxLogFiles ="/uploadFiles/myBox/logs/"
        basePathMyBoxMetricsFiles = "/uploadFiles/myBox/metrics/"
        basePathPublicKeyFiles  = "/usr/share/apache-tomcat-7.0.37/publicKeyFiles/"
        urlServiceUploadLogFile = "http://services.exxothermic.com/gears/uploadLogFile"
        urlServiceUploadMetricsFile = "http://services.exxothermic.com/boxMetrics/uploadFile"
        timeToWait = [3, 5, 8, 14, 15]
        passwordValidDays=60
        encryptionKey="23E54ACAB9031BEB1321FFD3125FBA1AB0000000000000000000000000000000"
        encryptionIv="2354ACABBB1321FFD100000000000000"
        grails.mail.default.from="Support Central Server <support@audioeverywhere.com>"
        maxDateForValidations="12/31/2020"
        // This is the ID of the Venue on Database and must be set after the creation of the record
        wifiAudioVenueId = 472
    }
}
// log4j configuration
log4j = {
	
	appenders {
		'null' name:'stacktrace'
		console name:'stdout', layout:pattern(conversionPattern: '%d %-5r %-5p [%c] (%t:%x) %m%n')
		//rollingFile  name:'file', file: System.getProperty('catalina.base')==null?"logs/CustomerSupport.log":System.getProperty('catalina.base') +'/logs/CustomerSupport.log', maxFileSize: 524288000, layout:pattern(conversionPattern: '%d %-5r %-5p [%c] (%t:%x) %m%n')
		
		environments {
			development {
				rollingFile name: "servicesAppender", maxFileSize: "10MB", maxBackupIndex: 50,
							file: "/var/log/services/services.log", layout: pattern(conversionPattern: '%d %-5r %-5p [%c] (%t:%x) %m%n')
				
			}
            devserver {
                rollingFile name: "servicesAppender", maxFileSize: "10MB", maxBackupIndex: 100,
                file: "/var/logs/exxo/services.log", layout: pattern(conversionPattern: '%d %-5r %-5p [%c] (%t:%x) %m%n')
            }
			staging {
				rollingFile name: "servicesAppender", maxFileSize: "10MB", maxBackupIndex: 100,
							file: "/var/log/services/services.log", layout: pattern(conversionPattern: '%d %-5r %-5p [%c] (%t:%x) %m%n')
			}
			production {
				rollingFile name: "servicesAppender", maxFileSize: "10MB", maxBackupIndex: 100,
							file: "/var/log/services/services.log", layout: pattern(conversionPattern: '%d %-5r %-5p [%c] (%t:%x) %m%n')
			}
		}
		
	}
	
	root {
		error 'servicesAppender'
		info 'servicesAppender'
		additivity = true
	}

	error  	'org.codehaus.groovy.grails.web.servlet',  //  controllers
			'org.codehaus.groovy.grails.web.pages', //  GSP
			'org.codehaus.groovy.grails.web.sitemesh', //  layouts
			'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
			'org.codehaus.groovy.grails.web.mapping', // URL mapping
			'org.codehaus.groovy.grails.commons', // core / classloading
			'org.codehaus.groovy.grails.plugins', // plugins
			'org.springframework'

	warn   	'org.mortbay.log',
			'org.apache.catalina'
	
	info	'grails.app.*'
	
			
	environments {
		production {
			
		}
		staging {
			
		}
        devserver {
            
        }
		development {			
			root {
				error 	'servicesAppender', 'stdout'
				info 	'servicesAppender', 'stdout'
				additivity = true
			}
			info	'grails.app.*'
			// debug 	'org.hibernate.SQL'
			// trace 	'org.hibernate.type.descriptor.sql.BasicBinder'
		}
		test {
			root {
				error 'stdout'
				info 'stdout'
				additivity = true
			}
		}
	}
}

grails {
	/*mail {
	 host = "smtp.gmail.com"
	 port = 465
	 username = "exxothermiccs@gmail.com"
	 password = "exxothermiccs#1"
	 props = ["mail.smtp.auth":"true",
	 "mail.smtp.socketFactory.port":"465",
	 "mail.smtp.socketFactory.class":"javax.net.ssl.SSLSocketFactory",
	 "mail.smtp.socketFactory.fallback":"false"]
	 }*/
	/*mail {
		host = "mail.exxothermic.com"
		port = 26
		username = "support@audioeverywhere.com"
		password = "It'sWendy'sFault2"
		props = ["mail.smtp.port":"26"]

	}*/
  mail {
    host = "secure242.inmotionhosting.com"
    port = 465
    username = "noreply@audiocloudservices.com"
    password = "It'sWendy'sFault2"
    props = [
      "mail.smtp.auth": "true",
      "mail.smtp.socketFactory.port": "465",
      "mail.smtp.socketFactory.class": "javax.net.ssl.SSLSocketFactory",
      "mail.smtp.socketFactory.fallback": "false"
    ]
  }
}




// Added by the Spring Security Core plugin:
grails.plugins.springsecurity.userLookup.userDomainClassName = 'mycentralserver.user.User'
grails.plugins.springsecurity.userLookup.authorityJoinClassName = 'mycentralserver.user.UserRole'
grails.plugins.springsecurity.authority.className = 'mycentralserver.user.Role'
grails.plugins.springsecurity.userLookup.usernamePropertyName='email'
config.successHandler.defaultTargetUrl='/home/'
grails.plugins.springsecurity.successHandler.alwaysUseDefault = true

grails.plugins.springsecurity.rememberMe.cookieName="stackoverflow"
grails.plugins.springsecurity.rememberMe.key="_grails_"
grails.plugins.springsecurity.rememberMe.tokenValiditySeconds=5*60

grails.plugins.springsecurity.securityConfigType = "InterceptUrlMap"

grails.plugins.springsecurity.useHttpSessionEventPublisher = true


grails.plugins.springsecurity.interceptUrlMap = [
    '/user/myaccount/**':           ['IS_AUTHENTICATED_REMEMBERED'],
    '/user/changepassword/**':      ['IS_AUTHENTICATED_REMEMBERED'],
    '/user/updatemyaccount':        ['IS_AUTHENTICATED_REMEMBERED'],
    '/user/**':                     ['ROLE_ADMIN'],
    '/boxSoftware/register/**':     ['ROLE_ADMIN'],
    '/boxSoftware/edit/*':          ['ROLE_ADMIN'],
    '/boxSoftware/**':              ['IS_AUTHENTICATED_REMEMBERED'],
    '/employee/**':                 ['ROLE_OWNER', 'ROLE_INTEGRATOR', 'ROLE_HELP_DESK'],
    '/companyIntegrator/editIntegratorCompany/**':    ['ROLE_ADMIN', 'ROLE_INTEGRATOR', 'ROLE_HELP_DESK'],
    '/companyIntegrator/edit/**':   ['ROLE_ADMIN', 'ROLE_HELP_DESK'],
    '/companyIntegrator/show/**':   ['ROLE_ADMIN', 'ROLE_HELP_DESK'],
    '/companyIntegrator/update/**': ['ROLE_ADMIN', 'ROLE_HELP_DESK'],
    '/companyIntegrator/**':        ['ROLE_ADMIN', 'ROLE_HELP_DESK'],
    '/boxManufacturer/**':          ['ROLE_ADMIN'],
    '/device/**':                   ['ROLE_ADMIN'],
    '/docCategory/**':              ['ROLE_ADMIN', 'ROLE_HELP_DESK', 'ROLE_INTEGRATOR'],
    '/appSkin/**':                  ['ROLE_OWNER', 'ROLE_HELP_DESK', 'ROLE_INTEGRATOR', 'ROLE_OWNER_STAFF', 'ROLE_INTEGRATOR_STAFF', 'ROLE_ADMIN'],
    '/welcomeAd/**':                ['ROLE_OWNER', 'ROLE_HELP_DESK', 'ROLE_INTEGRATOR', 'ROLE_OWNER_STAFF', 'ROLE_INTEGRATOR_STAFF', 'ROLE_ADMIN'],
    '/company/create**':            ['ROLE_INTEGRATOR', 'ROLE_HELP_DESK'],
    '/company/save**':              ['ROLE_INTEGRATOR', 'ROLE_HELP_DESK'],
    '/company/assignIntegrator**':  ['ROLE_HELP_DESK'],
    '/company/ownedCompanies**':    ['ROLE_HELP_DESK'],
    '/company/changeCompanyOwner**':['ROLE_HELP_DESK'],
    '/company/assign':              ['ROLE_OWNER', 'ROLE_OWNER_STAFF', 'ROLE_HELP_DESK'],
    '/company/notcompany':          ['ROLE_OWNER', 'ROLE_HELP_DESK', 'ROLE_INTEGRATOR', 'ROLE_OWNER_STAFF', 'ROLE_INTEGRATOR_STAFF'],
    '/company/show/**':             ['ROLE_OWNER', 'ROLE_HELP_DESK', 'ROLE_INTEGRATOR', 'ROLE_ADMIN'],
    '/company/dashboard/**':        ['ROLE_OWNER', 'ROLE_HELP_DESK', 'ROLE_INTEGRATOR', 'ROLE_ADMIN'],
    '/company/index':               ['ROLE_OWNER', 'ROLE_HELP_DESK', 'ROLE_INTEGRATOR', 'ROLE_ADMIN'],
    '/company/**':                  ['ROLE_OWNER', 'ROLE_HELP_DESK', 'ROLE_INTEGRATOR'],
    '/exxtractorsUser/**':          ['ROLE_HELP_DESK', 'ROLE_INTEGRATOR'],
    '/boxSoftwareUpgradeJob/**':    ['ROLE_HELP_DESK', 'ROLE_ADMIN'],
    '/location/create**':           ['ROLE_INTEGRATOR', 'ROLE_HELP_DESK'],
    '/location/save**':             ['ROLE_INTEGRATOR', 'ROLE_HELP_DESK'],
    '/location/show/**':            ['IS_AUTHENTICATED_REMEMBERED'],
    '/location/dashboard/**':       ['IS_AUTHENTICATED_REMEMBERED'],
    '/location/index/**':           ['IS_AUTHENTICATED_REMEMBERED'],
    '/location/listAll':            ['IS_AUTHENTICATED_REMEMBERED'],
    '/location/**':                 ['ROLE_OWNER', 'ROLE_HELP_DESK', 'ROLE_INTEGRATOR', 'ROLE_OWNER_STAFF', 'ROLE_INTEGRATOR_STAFF'],
    '/customButton/**':             ['IS_AUTHENTICATED_REMEMBERED'],
    '/content/**':                  ['IS_AUTHENTICATED_REMEMBERED'],
    '/offer/**':                    ['IS_AUTHENTICATED_REMEMBERED'],
    '/document/**':                 ['IS_AUTHENTICATED_REMEMBERED'],
    '/boxUnregistered/**':          ['ROLE_HELP_DESK', 'ROLE_ADMIN'],
    '/affiliate/**':                ['ROLE_HELP_DESK', 'ROLE_ADMIN'],
    '/configuration/**':            ['ROLE_ADMIN'],
    '/boxMetrics/uploadFile':       ['IS_AUTHENTICATED_ANONYMOUSLY'],
    '/boxMetrics/**':               ['ROLE_HELP_DESK', 'ROLE_INTEGRATOR', 'ROLE_INTEGRATOR_STAFF'],
    '/box/blackList':               ['ROLE_HELP_DESK', 'ROLE_ADMIN'],
    '/box/removeBoxFromBlackList':  ['ROLE_HELP_DESK', 'ROLE_ADMIN'],
    '/box/retireBox/**':            ['ROLE_HELP_DESK', 'ROLE_ADMIN'],
    '/box/listLogFiles/**':         ['ROLE_HELP_DESK', 'ROLE_ADMIN'],
    '/box/downloadLogFile/**':      ['ROLE_HELP_DESK', 'ROLE_ADMIN'],
    '/box/deleteLogFile/**':        ['ROLE_HELP_DESK', 'ROLE_ADMIN'],
    '/box/register':                ['ROLE_HELP_DESK', 'ROLE_INTEGRATOR', 'ROLE_INTEGRATOR_STAFF'],
    '/box/block':                   ['ROLE_ADMIN'],
    '/box/**':                      ['IS_AUTHENTICATED_REMEMBERED'],
    '/js/**':                       ['IS_AUTHENTICATED_ANONYMOUSLY'],
    '/css/**':                      ['IS_AUTHENTICATED_ANONYMOUSLY'],
    '/images/**':                   ['IS_AUTHENTICATED_ANONYMOUSLY'],
    '/login/**':                    ['IS_AUTHENTICATED_ANONYMOUSLY'],
    '/logout/**':                   ['IS_AUTHENTICATED_REMEMBERED'],
    '/*':                           ['IS_AUTHENTICATED_REMEMBERED']
]
