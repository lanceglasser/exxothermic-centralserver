package mycentralserver



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(CommonController)
class CommonControllerTests {

    void testSomething() {
       fail "Implement me"
    }
}
