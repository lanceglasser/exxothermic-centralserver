package mycentralserver



import grails.test.mixin.*
import org.junit.*

import mycentralserver.custombuttons.CustomButton

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(CustomButton)
class CustomButtonsTests {

    void testSomething() {
       fail "Implement me"
    }
}
