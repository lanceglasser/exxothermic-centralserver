package mycentralserver



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(BoxMetricsService)
class BoxMetricsServiceTests {

    void testSomething() {
        fail "Implement me"
    }
}
