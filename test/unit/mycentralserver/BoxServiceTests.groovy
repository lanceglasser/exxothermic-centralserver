package mycentralserver



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(BoxService)
class BoxServiceTests {

    void testSomething() {
        fail "Implement me"
    }
}
