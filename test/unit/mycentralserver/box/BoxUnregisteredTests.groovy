package mycentralserver.box



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(BoxUnregistered)
class BoxUnregisteredTests {

    void testSomething() {
       fail "Implement me"
    }
}
