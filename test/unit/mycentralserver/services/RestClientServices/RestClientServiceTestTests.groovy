package mycentralserver.services.RestClientServices

import static org.junit.Assert.*
import org.junit.*
import grails.converters.JSON
import grails.test.mixin.*
import grails.test.mixin.support.*

import mycentralserver.RestClientService;
import mycentralserver.custombuttons.CustomButton;

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 * @TestMixin(GrailsUnitTestMixin)
 */
//
@TestFor(RestClientService)
class RestClientServiceTestTests {

    void setUp() {
        // Setup logic here
    }

    void tearDown() {
        // Tear down logic here
    }

    void testSomething() {
        HashMap<String, List<CustomButton>> mapJson = new HashMap<String, List<CustomButton>>()
		List<CustomButton>  lbuttons = new ArrayList<CustomButton>() 
		CustomButton cb = new CustomButton()
		cb.id = 0
		cb.name = "testButton"
		cb.channelColor = "#009977"
		cb.channelContent = "www.google.com"
		cb.channelLabel = "Label"
		
		lbuttons.add(cb);
		mapJson.put("123", lbuttons)
		def json = (mapJson as JSON) 
		assertNotNull(lbuttons);		 
    }
}
