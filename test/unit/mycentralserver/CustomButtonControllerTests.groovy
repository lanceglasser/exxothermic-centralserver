package mycentralserver



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(CustomButtonController)
class CustomButtonControllerTests {

    void testSomething() {
       fail "Implement me"
    }
}
