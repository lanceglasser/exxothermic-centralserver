package mycentralserver



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(BoxUnregisteredController)
class BoxUnregisteredControllerTests {

    void testSomething() {
       fail "Implement me"
    }
}
