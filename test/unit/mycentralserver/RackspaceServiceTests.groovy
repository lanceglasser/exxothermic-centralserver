package mycentralserver



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(RackspaceService)
class RackspaceServiceTests {

    void testSomething() {
        fail "Implement me"
    }
}
