package mycentralserver



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(GearsService)
class GearsServiceTests {

    void testSomething() {
        fail "Implement me"
    }
}
